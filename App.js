import React, { useState } from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import Reducer from './source/reducer/Reducer';

import AppPage from './source/page/AppPage';
import SamplePage from './source/page/SamplePage';
import AwesomePage from './source/page/AwesomePage';
import LanguagePage from './source/page/LanguagePage';
import ArrayPage from './source/page/ArrayPage';
import ConversionPage from './source/page/ConversionPage';
import NextPage from './source/page/NextPage';
import PrinterPage from './source/page/PrinterPage';
import NavisionPage from './source/page/NavisionPage';
import DevicePage from './source/page/DevicePage';
import CssPage from './source/page/CssPage';
import StylePage from './source/page/StylePage';
import OrderHistoryDetails from './source/page/OrderHistoryDetails';
import MainMenuPage from './source/page/MainMenuPage';
import LocalAuthPage from './source/page/LocalAuthPage';
import QueryLocalTablePage from './source/page/QueryLocalTablePage';
import RenderPage from './source/page/RenderPage';
import ShiftingPage from './source/page/ShiftingPage';
import RawCallPage from './source/page/RawCallPage';
import TestPage from './source/page/TestPage';
import ScaleImagePage from './source/page/ScaleImagePage';
import EditOnChangePage from './source/page/EditOnChangePage';
import FlatListEndReachedPage from './source/page/FlatListEndReachedPage';

// VANSALES
import PgSplash from './source/page/PgSplash';
import PgMainMenu from './source/page/PgMainMenu';
import PgCustomerListing from './source/page/PgCustomerListing';
import PgItemListing from './source/page/PgItemListing';
import PgCheckout from './source/page/PgCheckout';
import PgOrderList from './source/page/PgOrderList';
import PgOrderDetails from './source/page/PgOrderDetails';
import PgOrderHistory from './source/page/PgOrderHistory';
import PgOrderHistoryDetails from './source/page/PgOrderHistoryDetails';
import PgOrderItem from './source/page/PgOrderItem';
import PgSummary from './source/page/PgSummary';
import PgPayment from './source/page/PgPayment';
import PgSettings from './source/page/PgSettings';
import PgCashCollection from './source/page/PgCashCollection';
import PgSalesReturnLists from './source/page/PgSalesReturnLists';
import PgSalesReturnDetailsNew from './source/page/PgSalesReturnDetailsNew';
import PgSelectItem from './source/page/PgSelectItem';
import PgSalesReturnDetailsUpdate from './source/page/PgSalesReturnDetailsUpdate';
import PgStockTake from './source/page/PgStockTake';
import PgTransferStocks from './source/page/PgTransferStocks';
import PgTransferStockDetails from './source/page/PgTransferStockDetails';
import PgDailySalesReport from './source/page/PgDailySalesReport';
import PgLogin from './source/page/PgLogin';
import PgRegisterNAVConnectionInfo from './source/page/PgRegisterNAVConnectionInfo';

import Camera from './source/framework/plugin/Camera';

import PgStreamHome from './source/page/PgStreamHome';
import PgStreamStreaming from './source/page/PgStreamStreaming';
import PgStreamView from './source/page/PgStreamView';
import PgStreamExplore from './source/page/PgStreamExplore';
import PgStreamVideo from './source/page/PgStreamVideo';
import PgStreamFavorites from './source/page/PgStreamFavorites';
import PgStreamProfile from './source/page/PgStreamProfile';

import TabNavigator from './source/page/NavTabPage';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { SafeAreaProvider } from 'react-native-safe-area-context';

// Additonal reusable component 
// used in some functions
import { References } from './source/dependency/global/Variable';
import Canvas from 'react-native-canvas';

import { loadData } from './source/framework/hook/Data';

const store = createStore(
    Reducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const Stack = createStackNavigator();

const EmobiqApp = () => {
    const loading = loadData();

    // Wait until all the data are loaded
    if (loading) {
        return null;
    }

    return (
        <SafeAreaProvider>
            <Provider store={store}>
                <NavigationContainer>
                    <Stack.Navigator 
                    initialRouteName='PgStreamHome'
                    screenOptions={{
                        headerShown: false
                    }}
                    >
                        {/* EMLIVE */}
                        <Stack.Screen
                            name='PgStreamHome'
                            component={TabNavigator}
                        />
                        <Stack.Screen
                            name='PgStreamStreaming'
                            component={PgStreamStreaming}
                        />
                        <Stack.Screen
                            name='PgStreamView'
                            component={PgStreamView}
                        />
                        <Stack.Screen
                            name='PgStreamExplore'
                            component={PgStreamExplore}
                        />
                        <Stack.Screen
                            name='PgStreamVideo'
                            component={PgStreamVideo}
                        />
                        <Stack.Screen
                            name='PgStreamFavorites'
                            component={PgStreamFavorites}
                        />
                        <Stack.Screen
                            name='PgStreamProfile'
                            component={PgStreamProfile}
                        />

                        {/* VANSALES */}
                        <Stack.Screen
                            name='pgSplash'
                            component={PgSplash}
                        />
                        <Stack.Screen
                            name='pgMainMenu'
                            component={PgMainMenu}
                        />
                        <Stack.Screen
                            name='pgCustomerListing'
                            component={PgCustomerListing}
                        />
                        <Stack.Screen
                            name='pgItemListing'
                            component={PgItemListing}
                        />
                        <Stack.Screen
                            name='pgCheckout'
                            component={PgCheckout}
                        />
                        <Stack.Screen
                            name='pgOrderList'
                            component={PgOrderList}
                        />
                        <Stack.Screen
                            name='pgOrderDetails'
                            component={PgOrderDetails}
                        />
                        <Stack.Screen
                            name='pgOrderHistory'
                            component={PgOrderHistory}
                        />
                        <Stack.Screen
                            name='pgOrderHistoryDetails'
                            component={PgOrderHistoryDetails}
                        />
                        <Stack.Screen
                            name='pgOrderItem'
                            component={PgOrderItem}
                        />
                        <Stack.Screen
                            name='pgSummary'
                            component={PgSummary}
                        />
                        <Stack.Screen
                            name='pgPayment'
                            component={PgPayment}
                        />
                        <Stack.Screen
                            name='pgSettings'
                            component={PgSettings}
                        />
                        <Stack.Screen
                            name='pgCashCollection'
                            component={PgCashCollection}
                        />
                        <Stack.Screen
                            name='pgSalesReturnLists'
                            component={PgSalesReturnLists}
                        />
                        <Stack.Screen
                            name='pgSalesReturnDetailsNew'
                            component={PgSalesReturnDetailsNew}
                        />
                        <Stack.Screen
                            name='pgSelectItem'
                            component={PgSelectItem}
                        />
                        <Stack.Screen
                            name='pgSalesReturnDetailsUpdate'
                            component={PgSalesReturnDetailsUpdate}
                        />
                        <Stack.Screen
                            name='pgStockTake'
                            component={PgStockTake}
                        />
                        <Stack.Screen
                            name='pgTransferStocks'
                            component={PgTransferStocks}
                        />
                        <Stack.Screen
                            name='pgTransferStockDetails'
                            component={PgTransferStockDetails}
                        />
                        <Stack.Screen
                            name='pgDailySalesReport'
                            component={PgDailySalesReport}
                        />
                        <Stack.Screen
                            name='pgLogin'
                            component={PgLogin}
                        />
                        <Stack.Screen
                            name='pgRegisterNAVConnectionInfo'
                            component={PgRegisterNAVConnectionInfo}
                        />


                        {/* Pages */}
                        <Stack.Screen
                            name='AppPage'
                            component={AppPage}
                        />
                        <Stack.Screen
                            name='SamplePage'
                            component={SamplePage}
                        />
                        <Stack.Screen
                            name='AwesomePage'
                            component={AwesomePage}
                        />
                        <Stack.Screen
                            name='LanguagePage'
                            component={LanguagePage}
                        />
                        <Stack.Screen
                            name='ArrayPage'
                            component={ArrayPage}
                        />
                        <Stack.Screen
                            name='ConversionPage'
                            component={ConversionPage}
                        />
                        <Stack.Screen
                            name='NextPage'
                            component={NextPage}
                        />
                        <Stack.Screen
                            name='PrinterPage'
                            component={PrinterPage}
                        />
                        <Stack.Screen
                            name='DevicePage'
                            component={DevicePage}
                        />
                        <Stack.Screen
                            name='NavisionPage'
                            component={NavisionPage}
                        />
                        <Stack.Screen
                            name='CssPage'
                            component={CssPage}
                        />
                        <Stack.Screen
                            name='StylePage'
                            component={StylePage}
                        />
                        <Stack.Screen
                            name='OrderHistoryDetails'
                            component={OrderHistoryDetails}
                        />
                        <Stack.Screen
                            name='MainMenuPage'
                            component={MainMenuPage}
                        />
                        <Stack.Screen
                            name='LocalAuthPage'
                            component={LocalAuthPage}
                        />
                         <Stack.Screen
                            name='QueryLocalTablePage'
                            component={QueryLocalTablePage}
                        />
                        <Stack.Screen
                            name='RenderPage'
                            component={RenderPage}
                        />
                        <Stack.Screen
                            name='ShiftingPage'
                            component={ShiftingPage}
                        />
                         <Stack.Screen
                            name='RawCallPage'
                            component={RawCallPage}
                        />
                         <Stack.Screen
                            name='TestPage'
                            component={TestPage}
                        />
                          <Stack.Screen
                            name='ScaleImagePage'
                            component={ScaleImagePage}
                        />
                          <Stack.Screen
                            name='EditOnChangePage'
                            component={EditOnChangePage}
                        />

                          <Stack.Screen
                            name='FlatListEndReachedPage'
                            component={FlatListEndReachedPage}
                        />

                        {/* Plugins */}
                        <Stack.Screen
                            name='CameraPlugin'
                            component={Camera}
                        />

                        
                    </Stack.Navigator>
                </NavigationContainer>
                {/* Reusable components - bbCodeToCanvas, bbCodeToCanvasSync*/}
                <Canvas ref={(canvas) => References.canvas = canvas} style={{ display: 'none' }} />
            </Provider>
        </SafeAreaProvider>
    );
}

export default EmobiqApp;