export const StateName = 'pgTransferStocks';
export const State = {
	"lblModalMessage": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(111, 111, 110)",
			"fontSize": "16px"
		},
		"parent": "pnlModalBodyMessage"
	},
	"pnlMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"zIndex": "0"
		},
		"parent": "pnlHolder"
	},
	"pnlMainHeaderLeft": {
		"parent": "pnlMainHeader",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeft",
			"componentVisible": true
		},
		"style": {
			"width": "15%",
			"height": "100%",
			"float": "left"
		}
	},
	"pnlMainHeaderLeftTblCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTblCell",
			"componentVisible": true
		},
		"style": {
			"paddingLeft": "10px",
			"width": "100%",
			"height": "100%",
			"textAlign": "left",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlMainHeaderLeftTbl"
	},
	"pnlMainHeaderMiddle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddle",
			"componentVisible": true
		},
		"style": {
			"paddingLeft": "5%",
			"float": "left",
			"width": "65%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlMainHeaderMiddleTbl": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTbl",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddle"
	},
	"pnlMainHeaderMiddleTblCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTblCell",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%"
		},
		"parent": "pnlMainHeaderMiddleTbl"
	},
	"txtSearch": {
		"property": {
			"componentType": "edit",
			"componentName": "txtSearch",
			"type": "search",
			"backgroundAsset": "icon/_search.png",
			"placeHolder": "Search",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"border": "1px solid #D9D9D9",
			"background": "url('data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Capa_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 451 451\" style=\"enable-background:new 0 0 451 451;\" xml:space=\"preserve\" width=\"512px\" height=\"512px\"><g><path d=\"M447.05,428l-109.6-109.6c29.4-33.8,47.2-77.9,47.2-126.1C384.65,86.2,298.35,0,192.35,0C86.25,0,0.05,86.3,0.05,192.3   s86.3,192.3,192.3,192.3c48.2,0,92.3-17.8,126.1-47.2L428.05,447c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4   C452.25,441.8,452.25,433.2,447.05,428z M26.95,192.3c0-91.2,74.2-165.3,165.3-165.3c91.2,0,165.3,74.2,165.3,165.3   s-74.1,165.4-165.3,165.4C101.15,357.7,26.95,283.5,26.95,192.3z\" fill=\"#3c3c3b\"/></g></svg>') no-repeat",
			"backgroundColor": "rgb(255, 255, 255)",
			"backgroundSize": "16px",
			"width": "100%",
			"display": "inline",
			"height": "40px",
			"fontSize": "16px",
			"textAlign": "left",
			"backgroundPosition": "left .5rem center",
			"paddingLeft": "30px",
			"margin": "0px"
		},
		"parent": "pnlMainBodySearch"
	},
	"pnlModalCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModalTable"
	},
	"pnlHolder": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"backgroundColor": "rgb(245, 245, 245)"
		},
		"parent": "pgTransferStocks"
	},
	"pnlMainHeaderLeftTbl": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTbl",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderLeft"
	},
	"pnlMainHeaderRightTbl": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTbl",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"display": "table",
			"width": "100%"
		},
		"parent": "pnlMainHeaderRight"
	},
	"pnlMainHeaderRightTblCell": {
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"textAlign": "right",
			"height": "100%",
			"paddingRight": "10px"
		},
		"parent": "pnlMainHeaderRightTbl",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTblCell",
			"componentVisible": true
		}
	},
	"lblReturn": {
		"property": {
			"componentType": "label",
			"componentName": "lblReturn",
			"componentValue": "RETURN",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"fontWeight": "500"
		},
		"parent": "pnlMainHeaderRightTblCell"
	},
	"pnlMainBodySearch": {
		"parent": "pnlMainSearch",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodySearch",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "50px",
			"padding": "5px"
		}
	},
	"pnlModal": {
		"parent": "pnlHolder",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"height": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"top": "0px",
			"zIndex": "4",
			"width": "100%"
		}
	},
	"pnlModalBodyTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"marginBottom": "10px",
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	},
	"imgModalLoading": {
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		},
		"style": {
			"width": "50px",
			"height": "10px"
		},
		"parent": "pnlModalBodyLoading"
	},
	"pnlModalBodyMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"background": "#ffffff",
			"padding": "10px 0px",
			"marginRight": "5%",
			"width": "90%",
			"borderBottom": "1px solid #D9D9D9",
			"marginLeft": "5%"
		},
		"parent": "pnlModalCell"
	},
	"pnlModalBodyLoading": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlMainHeader": {
		"style": {
			"backgroundColor": "rgb(255, 69, 0)",
			"width": "100%",
			"height": "65px",
			"textAlign": "center",
			"paddingTop": "15px"
		},
		"parent": "pnlMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		}
	},
	"dtTransferLists": {
		"property": {
			"componentType": "datalist",
			"componentName": "dtTransferLists",
			"componentValue": "l_Transfer_Order_List",
			"autoLoad": true,
			"componentData": [],
			"componentVisible": true
		},
		"style": {
			"overflow": "auto"
		},
		"parent": "pnlBodyContent",
		"template": {
			"componentDataItemState": {
				"pnlDataContentTblCell1": {
					"parent": "pnlDataContentTbl",
					"property": {
						"componentType": "panel",
						"componentName": "pnlDataContentTblCell1",
						"componentVisible": true
					},
					"style": {
						"width": "calc(100% - 40px)",
						"display": "table-cell",
						"textAlign": "left"
					}
				},
				"pnlDataContentPostingDate": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlDataContentPostingDate",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"width": "100%"
					},
					"parent": "pnlDataContentTblCell1"
				},
				"pnlDataContentTblCell2": {
					"parent": "pnlDataContentTbl",
					"property": {
						"componentType": "panel",
						"componentName": "pnlDataContentTblCell2",
						"componentVisible": true
					},
					"style": {
						"textAlign": "center",
						"display": "table-cell",
						"verticalAlign": "middle",
						"width": "40px"
					}
				},
				"imgArrowDetails": {
					"property": {
						"componentType": "image",
						"componentName": "imgArrowDetails",
						"componentValue": require("../../asset/images/icon-go-gray (1).png"),
						"zoom": false,
						"componentVisible": true
					},
					"style": {
						"width": "30px"
					},
					"parent": "pnlDataContentTblCell2"
				},
				"lblDataContentToLocation_data": {
					"property": {
						"componentType": "label",
						"componentName": "lblDataContentToLocation_data",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(60, 60, 59)",
						"fontSize": "16px",
						"paddingTop": "2px",
						"display": "block",
						"fontWeight": "normal"
					},
					"parent": "pnlDataContentToLocation"
				},
				"lblDataContentPostingDate_data": {
					"property": {
						"componentType": "label",
						"componentName": "lblDataContentPostingDate_data",
						"componentVisible": true
					},
					"style": {
						"fontWeight": "normal",
						"paddingTop": "2px",
						"display": "block",
						"color": "rgb(60, 60, 59)",
						"fontSize": "16px"
					},
					"parent": "pnlDataContentPostingDate"
				},
				"pnlDataContentStatus": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlDataContentStatus",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"width": "100%"
					},
					"parent": "pnlDataContentTblCell1"
				},
				"pnlMainDataContent": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlMainDataContent",
						"componentVisible": true
					},
					"style": {
						"paddingBottom": "0px",
						"width": "100%",
						"backgroundColor": "rgb(255, 255, 255)",
						"padding": "5px",
						"textAlign": "left"
					}
				},
				"pnlDataContentTbl": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlDataContentTbl",
						"componentVisible": true
					},
					"style": {
						"display": "table",
						"border": "1px solid #3C3C3B",
						"borderRadius": "5px",
						"padding": "5px",
						"width": "100%",
						"boxShadow": "-1px 1px 1px #545454",
						"background": "url() no-repeat fixed",
						"backgroundColor": "rgb(255, 255, 255)"
					},
					"parent": "pnlMainDataContent"
				},
				"pnlDataContentNo": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlDataContentNo",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"width": "100%"
					},
					"parent": "pnlDataContentTblCell1"
				},
				"lblDataContentNo_data": {
					"parent": "pnlDataContentNo",
					"property": {
						"componentType": "label",
						"componentName": "lblDataContentNo_data",
						"field": "No",
						"componentVisible": true
					},
					"style": {
						"fontWeight": "bold",
						"color": "rgb(60, 60, 59)",
						"fontSize": "24px",
						"display": "block"
					}
				},
				"pnlDataContentToLocation": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlDataContentToLocation",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"width": "100%"
					},
					"parent": "pnlDataContentTblCell1"
				},
				"lblDataContentStatus_data": {
					"property": {
						"componentType": "label",
						"componentName": "lblDataContentStatus_data",
						"field": "Status",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"color": "rgb(60, 60, 59)",
						"fontSize": "14px",
						"fontWeight": "normal",
						"paddingTop": "2px"
					},
					"parent": "pnlDataContentStatus"
				}
			}
		}
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"width": "50%",
			"textAlign": "center",
			"padding": "10px 0px",
			"float": "none"
		},
		"parent": "pnlModalBodyButtons"
	},
	"lblStyle": {
		"property": {
			"componentType": "label",
			"componentName": "lblStyle",
			"componentValue": "N/A",
			"componentVisible": true
		},
		"parent": "pnlCSS"
	},
	"imgMenu": {
		"property": {
			"componentType": "image",
			"componentName": "imgMenu",
			"componentValue": require("../../asset/icon/icon-menu.png"),
			"componentVisible": true
		},
		"style": {
			"width": "35px",
			"height": "35px"
		},
		"parent": "pnlMainHeaderLeftTblCell"
	},
	"pnlMainSearch": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainSearch",
			"componentVisible": true
		},
		"style": {
			"height": "50px",
			"width": "100%",
			"textAlign": "center",
			"borderBottom": "1px solid #D9D9D9"
		},
		"parent": "pnlMain"
	},
	"pnlMainBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"overflow": "auto",
			"height": "calc(100% - 100px - 15px)"
		},
		"parent": "pnlMain"
	},
	"pgTransferStocks": {
		"property": {
			"componentType": "page",
			"componentName": "pgTransferStocks",
			"title": "Transfer Stocks",
			"componentVisible": true
		}
	},
	"lblMainTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle",
			"componentValue": "TRANSFER STOCKS",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500"
		},
		"parent": "pnlMainHeaderMiddleTblCell"
	},
	"pnlMainHeaderRight": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRight",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "20%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlModalBodyMessage": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"padding": "10px",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlModalBodyButtonPositive": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"float": "none",
			"width": "50%",
			"textAlign": "center",
			"padding": "10px 0px"
		},
		"parent": "pnlModalBodyButtons"
	},
	"lblModalNegative": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonNegative"
	},
	"pnlBodyContent": {
		"parent": "pnlBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyContent",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"width": "100%"
		}
	},
	"pnlModalTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModal"
	},
	"pnlModalBodyButtons": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "90%",
			"textAlign": "center",
			"background": "#ffffff",
			"marginLeft": "5%",
			"marginRight": "5%"
		},
		"parent": "pnlModalCell"
	},
	"pnlBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBody",
			"componentVisible": true
		},
		"style": {
			"height": "100%"
		},
		"parent": "pnlMainBody"
	},
	"lblModalTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "20px",
			"fontWeight": "bold"
		},
		"parent": "pnlModalBodyTitle"
	},
	"lblModalPositive": {
		"parent": "pnlModalBodyButtonPositive",
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		}
	},
	"pnlCSS": {
		"style": {
			"position": "fixed",
			"display": "none",
			"height": "100%",
			"top": "0px",
			"zIndex": "5",
			"width": "100%"
		},
		"parent": "pnlHolder",
		"property": {
			"componentType": "panel",
			"componentName": "pnlCSS",
			"componentVisible": false
		}
	}
};
