export const StateName = 'LanguagePage';
export const State = {
    lblFirst: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentUUID: 'abcde1',
            componentType: 'label',
            componentValue: 'Hello',
            componentVisible: true,
        }
    },
    btnChangeCaption: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentUUID: 'abcde2',
            componentType: 'button',
            componentValue: 'Change Caption',
            componentVisible: true,
        }
    },
    btnGetLanguage: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentUUID: 'abcde3',
            componentType: 'button',
            componentValue: 'Get Language',
            componentVisible: true,
        }
    },
    btnSetLanguage: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentUUID: 'abcde4',
            componentType: 'button',
            componentValue: 'Set Language - Console Log',
            componentVisible: true,
        }
    },
    btnLangaugeConvertData: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentUUID: 'abcde5',
            componentType: 'button',
            componentValue: 'Convert Language Data',
            componentVisible: true,
        }
    },
    btnGetLanguageList: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentUUID: 'abcde6',
            componentType: 'button',
            componentValue: 'Get Language List - Console Log',
            componentVisible: true,
        }
    },
    btnSave: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentUUID: 'abcde7',
            componentType: 'button',
            componentValue: 'Save data to Local Table',
            componentVisible: true,
        }
    },
    btnClear: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentUUID: 'abcde8',
            componentType: 'button',
            componentValue: 'Clear data',
            componentVisible: true,
        }
    }
}