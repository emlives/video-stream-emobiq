export const StateName = 'pgDailySalesReport';
export const State = {
	"pnlChequeCollectionForToday": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlChequeCollectionForToday",
			"componentVisible": true
		},
		"parent": "pnlCollectionSummaryDetails"
	},
	"pnlModalTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"display": "table",
			"width": "100%"
		},
		"parent": "pnlModal"
	},
	"pnlModalBodyButtonPositive": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"float": "none",
			"padding": "10px 0px",
			"width": "50%",
			"textAlign": "center"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlMainBodyDateSearchTblCell2": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyDateSearchTblCell2",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"width": "60px",
			"height": "40px",
			"backgroundColor": "rgb(84, 84, 84)"
		},
		"parent": "pnlMainBodyDateSearchTbl"
	},
	"pnlMainCollectionSummary": {
		"parent": "pnlBodyContent",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainCollectionSummary",
			"componentVisible": true
		},
		"style": {
			"boxShadow": "-3px 3px 3px #545454",
			"borderRadius": "10px",
			"marginBottom": "30px"
		}
	},
	"pnlBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBody",
			"componentVisible": true
		},
		"style": {
			"height": "100%"
		},
		"parent": "pnlMainBody"
	},
	"pnlSalesSummary": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSalesSummary",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgb(135, 135, 135)",
			"borderRadius": "10px 10px 0px 0px",
			"padding": "5px"
		},
		"parent": "pnlMainSalesSummary"
	},
	"lblCashCollectionForToday": {
		"property": {
			"componentType": "label",
			"componentName": "lblCashCollectionForToday",
			"componentValue": "Cash Collection for To-Day",
			"componentVisible": true
		},
		"style": {
			"width": "200px",
			"color": "rgb(60, 60, 59)",
			"fontWeight": "bold"
		},
		"parent": "pnlCashCollectionForToday"
	},
	"pnlMainHeaderLeftTbl": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTbl",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderLeft"
	},
	"pnlMainHeaderMiddle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddle",
			"componentVisible": true
		},
		"style": {
			"width": "65%",
			"height": "100%",
			"paddingLeft": "5%",
			"float": "left"
		},
		"parent": "pnlMainHeader"
	},
	"imgSearch": {
		"parent": "pnlMainBodyDateSearchTblCell2",
		"property": {
			"componentType": "image",
			"componentName": "imgSearch",
			"zoom": false,
			"componentValue": require("../../asset/icon/Search - White.png"),
			"componentVisible": true
		},
		"style": {
			"width": "30px",
			"padding": "0",
			"margin": "0"
		}
	},
	"pnlMainBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		},
		"style": {
			"overflow": "auto",
			"width": "100%",
			"height": "calc(100% - 60px - 65px)"
		},
		"parent": "pnlMain"
	},
	"pnlMainSalesSummary": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainSalesSummary",
			"componentVisible": true
		},
		"style": {
			"boxShadow": "-3px 3px 3px #545454",
			"borderRadius": "10px",
			"marginBottom": "20px"
		},
		"parent": "pnlBodyContent"
	},
	"lblTodayCreditSales_colon": {
		"property": {
			"componentType": "label",
			"componentName": "lblTodayCreditSales_colon",
			"componentValue": ":",
			"componentVisible": true
		},
		"style": {
			"width": "10px",
			"color": "rgb(60, 60, 59)",
			"textAlign": "center"
		},
		"parent": "pnlTodayCreditSales"
	},
	"lblTodayCreditSales_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblTodayCreditSales_data",
			"componentValue": "$ 0.00",
			"componentVisible": true
		},
		"style": {
			"width": "calc(100% - 210px)",
			"color": "rgb(60, 60, 59)"
		},
		"parent": "pnlTodayCreditSales"
	},
	"imgMenu": {
		"property": {
			"componentType": "image",
			"componentName": "imgMenu",
			"componentValue": require("../../asset/icon/icon-menu.png"),
			"componentVisible": true
		},
		"style": {
			"width": "35px",
			"height": "35px"
		},
		"parent": "pnlMainHeaderLeftTblCell"
	},
	"pnlMainHeaderRight": {
		"parent": "pnlMainHeader",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRight",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "20%",
			"height": "100%"
		}
	},
	"pnlMainHeaderRightTbl": {
		"parent": "pnlMainHeaderRight",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTbl",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		}
	},
	"pnlCollectionSummary": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCollectionSummary",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgb(135, 135, 135)",
			"borderRadius": "10px 10px 0px 0px",
			"padding": "5px"
		},
		"parent": "pnlMainCollectionSummary"
	},
	"lblModalNegative": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonNegative"
	},
	"pnlMainHeaderMiddleTbl": {
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table"
		},
		"parent": "pnlMainHeaderMiddle",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTbl",
			"componentVisible": true
		}
	},
	"pnlMainHeaderMiddleTblCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTblCell",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlMainHeaderMiddleTbl"
	},
	"pnlCollectionSummaryDetails": {
		"style": {
			"minHeight": "50px",
			"padding": "10px",
			"minWidth": "40px"
		},
		"parent": "pnlMainCollectionSummary",
		"property": {
			"componentType": "panel",
			"componentName": "pnlCollectionSummaryDetails",
			"componentVisible": true
		}
	},
	"pgDailySalesReport": {
		"property": {
			"componentType": "page",
			"componentName": "pgDailySalesReport",
			"title": "Daily Sales Report",
			"componentVisible": true
		}
	},
	"lblTodayCashSales": {
		"property": {
			"componentType": "label",
			"componentName": "lblTodayCashSales",
			"componentValue": "To-Day Cash Sales",
			"componentVisible": true
		},
		"style": {
			"width": "200px",
			"color": "rgb(60, 60, 59)",
			"fontWeight": "bold"
		},
		"parent": "pnlTodayCashSales"
	},
	"pnlModalCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModalTable"
	},
	"lblTotalSalesToDay": {
		"property": {
			"componentType": "label",
			"componentName": "lblTotalSalesToDay",
			"componentValue": "Total Sales for To-Day",
			"componentVisible": true
		},
		"style": {
			"width": "200px",
			"color": "rgb(60, 60, 59)",
			"fontSize": "18px",
			"fontWeight": "bold"
		},
		"parent": "pnlTotalSalesToDay"
	},
	"lblTotalSalesToDay_colon": {
		"property": {
			"componentType": "label",
			"componentName": "lblTotalSalesToDay_colon",
			"componentValue": ":",
			"componentVisible": true
		},
		"style": {
			"width": "10px",
			"color": "rgb(60, 60, 59)",
			"fontSize": "18px",
			"textAlign": "center"
		},
		"parent": "pnlTotalSalesToDay"
	},
	"lblTotalSalesToDay_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblTotalSalesToDay_data",
			"componentValue": "$ 0.00",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(0, 153, 0)",
			"fontSize": "18px",
			"width": "calc(100% - 210px)"
		},
		"parent": "pnlTotalSalesToDay"
	},
	"lblCashCollectionForToday_colon": {
		"property": {
			"componentType": "label",
			"componentName": "lblCashCollectionForToday_colon",
			"componentValue": ":",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"width": "10px",
			"color": "rgb(60, 60, 59)"
		},
		"parent": "pnlCashCollectionForToday"
	},
	"pnlModalBodyTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"lblModalMessage": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(111, 111, 110)",
			"fontSize": "16px"
		},
		"parent": "pnlModalBodyMessage"
	},
	"edtDateSearch": {
		"property": {
			"componentType": "edit",
			"componentName": "edtDateSearch",
			"type": "date",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"margin": "0px"
		},
		"parent": "pnlMainBodyDateSearchTblCell1"
	},
	"lblSalesSummary": {
		"parent": "pnlSalesSummary",
		"property": {
			"componentType": "label",
			"componentName": "lblSalesSummary",
			"componentValue": "Sales Summary",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 215, 0)",
			"fontSize": "18px",
			"fontWeight": "bold"
		}
	},
	"lblStyle": {
		"property": {
			"componentType": "label",
			"componentName": "lblStyle",
			"componentValue": "N/A",
			"componentVisible": true
		},
		"parent": "pnlCSS"
	},
	"pnlHolder": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"backgroundColor": "rgb(245, 245, 245)"
		},
		"parent": "pgDailySalesReport"
	},
	"pnlTotalSalesToDay": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlTotalSalesToDay",
			"componentVisible": true
		},
		"parent": "pnlSalesSummaryDetails"
	},
	"pnlMainHeaderLeftTblCell": {
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%",
			"textAlign": "left",
			"paddingLeft": "10px"
		},
		"parent": "pnlMainHeaderLeftTbl",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTblCell",
			"componentVisible": true
		}
	},
	"lblTodayCashSales_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblTodayCashSales_data",
			"componentValue": "$ 0.00",
			"componentVisible": true
		},
		"style": {
			"width": "calc(100% - 210px)",
			"color": "rgb(60, 60, 59)"
		},
		"parent": "pnlTodayCashSales"
	},
	"pnlMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"zIndex": "0",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlMainHeader": {
		"parent": "pnlMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"paddingTop": "15px",
			"width": "100%",
			"height": "65px",
			"backgroundColor": "rgb(255, 69, 0)"
		}
	},
	"pnlTodayCashSales": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlTodayCashSales",
			"componentVisible": true
		},
		"parent": "pnlSalesSummaryDetails"
	},
	"lblChequeCollectionForToday_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblChequeCollectionForToday_data",
			"componentValue": "$ 0.00",
			"componentVisible": true
		},
		"style": {
			"width": "calc(100% - 210px)",
			"color": "rgb(60, 60, 59)"
		},
		"parent": "pnlChequeCollectionForToday"
	},
	"pnlModalBodyMessage": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"padding": "10px",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlMainBodyDateSearchTbl": {
		"style": {
			"display": "table",
			"width": "100%"
		},
		"parent": "pnlMainSearch",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyDateSearchTbl",
			"componentVisible": true
		}
	},
	"pnlModal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"zIndex": "4",
			"width": "100%",
			"height": "100%",
			"top": "0px",
			"background": "rgba(0, 0, 0, 0.60)"
		},
		"parent": "pnlHolder"
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"padding": "10px 0px",
			"display": "inline-block",
			"float": "none",
			"width": "50%",
			"textAlign": "center"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlCSS": {
		"parent": "pnlHolder",
		"property": {
			"componentType": "panel",
			"componentName": "pnlCSS",
			"componentVisible": false
		},
		"style": {
			"height": "100%",
			"top": "0px",
			"zIndex": "5",
			"position": "fixed",
			"display": "none",
			"width": "100%"
		}
	},
	"pnlSalesSummaryDetails": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSalesSummaryDetails",
			"componentVisible": true
		},
		"style": {
			"minWidth": "40px",
			"minHeight": "50px",
			"padding": "10px"
		},
		"parent": "pnlMainSalesSummary"
	},
	"pnlTodayCreditSales": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlTodayCreditSales",
			"componentVisible": true
		},
		"parent": "pnlSalesSummaryDetails"
	},
	"lblTodayCreditSales": {
		"property": {
			"componentType": "label",
			"componentName": "lblTodayCreditSales",
			"componentValue": "To-Day Credit Sales",
			"componentVisible": true
		},
		"style": {
			"width": "200px",
			"color": "rgb(60, 60, 59)",
			"fontWeight": "bold"
		},
		"parent": "pnlTodayCreditSales"
	},
	"lblCollectionSummary": {
		"property": {
			"componentType": "label",
			"componentName": "lblCollectionSummary",
			"componentValue": "Collection Summary",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"color": "rgb(255, 215, 0)",
			"fontSize": "18px"
		},
		"parent": "pnlCollectionSummary"
	},
	"imgModalLoading": {
		"style": {
			"width": "50px",
			"height": "10px"
		},
		"parent": "pnlModalBodyLoading",
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		}
	},
	"pnlMainBodyDateSearchTblCell1": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyDateSearchTblCell1",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"height": "50px"
		},
		"parent": "pnlMainBodyDateSearchTbl"
	},
	"pnlBodyContent": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyContent",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"width": "100%",
			"padding": "10px"
		},
		"parent": "pnlBody"
	},
	"lblModalTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontSize": "20px",
			"fontWeight": "bold",
			"width": "100%"
		},
		"parent": "pnlModalBodyTitle"
	},
	"pnlModalBodyButtons": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"background": "#ffffff",
			"marginLeft": "5%",
			"marginRight": "5%",
			"width": "90%",
			"textAlign": "center"
		},
		"parent": "pnlModalCell"
	},
	"lblModalPositive": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonPositive"
	},
	"pnlMainHeaderLeft": {
		"parent": "pnlMainHeader",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeft",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		}
	},
	"pnlMainSearch": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainSearch",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"padding": "5px",
			"height": "60px",
			"borderBottom": "1px solid #D9D9D9"
		},
		"parent": "pnlMain"
	},
	"pnlCashCollectionForToday": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCashCollectionForToday",
			"componentVisible": true
		},
		"parent": "pnlCollectionSummaryDetails"
	},
	"lblCashCollectionForToday_data": {
		"parent": "pnlCashCollectionForToday",
		"property": {
			"componentType": "label",
			"componentName": "lblCashCollectionForToday_data",
			"componentValue": "$ 0.00",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"width": "calc(100% - 210px)"
		}
	},
	"pnlModalBodyMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		},
		"style": {
			"background": "#ffffff",
			"padding": "10px 0px",
			"marginLeft": "5%",
			"marginRight": "5%",
			"width": "90%",
			"borderBottom": "1px solid #D9D9D9",
			"textAlign": "center"
		},
		"parent": "pnlModalCell"
	},
	"lblMainTitle": {
		"parent": "pnlMainHeaderMiddleTblCell",
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle",
			"componentValue": "DAILY SALES REPORT",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500"
		}
	},
	"pnlMainHeaderRightTblCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTblCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRightTbl"
	},
	"lblChequeCollectionForToday": {
		"property": {
			"componentType": "label",
			"componentName": "lblChequeCollectionForToday",
			"componentValue": "Cheque Collection for To-Day",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"width": "200px",
			"color": "rgb(60, 60, 59)"
		},
		"parent": "pnlChequeCollectionForToday"
	},
	"lblChequeCollectionForToday_colon": {
		"parent": "pnlChequeCollectionForToday",
		"property": {
			"componentType": "label",
			"componentName": "lblChequeCollectionForToday_colon",
			"componentValue": ":",
			"componentVisible": true
		},
		"style": {
			"width": "10px",
			"color": "rgb(60, 60, 59)",
			"textAlign": "center"
		}
	},
	"pnlModalBodyLoading": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	},
	"lblReturn": {
		"property": {
			"componentType": "label",
			"componentName": "lblReturn",
			"componentValue": "RETURN",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"fontWeight": "500",
			"display": "none"
		},
		"parent": "pnlMainHeaderRightTblCell"
	},
	"lblTodayCashSales_colon": {
		"property": {
			"componentType": "label",
			"componentName": "lblTodayCashSales_colon",
			"componentValue": ":",
			"componentVisible": true
		},
		"style": {
			"width": "10px",
			"color": "rgb(60, 60, 59)",
			"textAlign": "center"
		},
		"parent": "pnlTodayCashSales"
	}
};
