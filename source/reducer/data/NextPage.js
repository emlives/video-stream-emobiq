export const StateName = 'NextPage';
export const State = {
    btnSamplePage: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentValue: 'Sample page',
            componentType: 'button',
        },
    },
}
