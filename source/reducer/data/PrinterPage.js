export const StateName = 'PrinterPage';
export const State = {
    page: {
        style: {
            flex: 1,
                backgroundColor: 'white',
        },
        property: {
            componentDisabled: false,
        }
    },
    footer:{
        style: {
            height:100,
        },
    },
    canvasMessage:{
        "property": {
			"componentType": "label",
			"componentName": "canvasMessage",
            "componentValue": "1. Canvas is initialized. Because image is too big, it will not be displayed here. Go ahead and scan (and connect) to printer XXRAJ174303023",
			"componentVisible": true
		},
		"style": {
            width: '80%',
            alignSelf: 'center',
			fontSize: "14px",
            margin: 8,
            marginTop: 20,
            lineHeight: 20,
		},
    },
    pgTitle:{
        "property": {
			"componentType": "label",
			"componentName": "title",
            "componentValue": "Printer Demo",
			"componentVisible": true
		},
		"style": {
            width: '80%',
            alignSelf: 'center',
			fontSize: "20px",
            margin: 8,
            marginTop: 20,
            lineHeight: 20,
		},
    },
    btnSetupCanvas:{
        style: {
			width: '90%',
            borderRadius: 8,
            margin: 15,
            alignSelf: 'center',
            backgroundColor: 'black',
            color: 'white'
        },
        property: {
			componentValue: '1. Setup Canvas',
			componentVisible: true,
		}
    },
    btnScanPeripherals:{
        style: {
			width: '90%',
            borderRadius: 8,
            margin: 15,
            alignSelf: 'center',
            backgroundColor: 'black',
            color: 'white'
        },
        property: {
			componentValue: '2. Scan Peripherals',
			componentVisible: true,
		}
    },
    btnPrint:{
        style: {
			width: '90%',
            borderRadius: 8,
            margin: 15,
            alignSelf: 'center',
            backgroundColor: 'black',
            color: 'white'
        },
        property: {
			componentValue: '3. Print Canvas',
			componentVisible: true,
		}

    },
    cmbPrinter: {
		property: {
            componentUUID: "Wb5Z53yffN2IP-rLsysT8",
			componentType: "combobox",
			componentName: "cmbPrinter",
			componentVisible: true
		},
	},
    
}