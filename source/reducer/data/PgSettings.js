export const StateName = 'pgSettings';
export const State = {
	"lblUsername": {
		"property": {
			"componentType": "label",
			"componentName": "lblUsername",
			"componentValue": "Service URL",
			"componentVisible": true
		},
		"style": {
			"marginLeft": "5px",
			"color": "rgb(60, 60, 59)",
			"fontSize": "14px",
			"fontWeight": "bold"
		},
		"parent": "pnlUsernameCaption"
	},
	"lblModalMessage": {
		"style": {
			"color": "rgb(111, 111, 110)",
			"fontSize": "16px",
			"width": "100%"
		},
		"parent": "pnlModalBodyMessage",
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		}
	},
	"lblModalNegative": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		},
		"style": {
			"fontSize": "20px",
			"color": "rgb(74, 134, 237)"
		},
		"parent": "pnlModalBodyButtonNegative"
	},
	"pnlMainHeaderLeftCell": {
		"parent": "pnlMainHeaderLeftTable",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"paddingLeft": "10px",
			"width": "100%",
			"height": "100%",
			"textAlign": "left"
		}
	},
	"pnlMainHeaderRightTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRight"
	},
	"lblSave": {
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500"
		},
		"parent": "pnlMainHeaderRightCell",
		"property": {
			"componentType": "label",
			"componentName": "lblSave",
			"componentValue": "SAVE",
			"componentVisible": true
		}
	},
	"Panel92": {
		"style": {
			"minWidth": "40px",
			"minHeight": "50px"
		},
		"parent": "pnlMain",
		"property": {
			"componentType": "panel",
			"componentName": "Panel92",
			"componentVisible": true
		}
	},
	"lblMainTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle",
			"componentValue": "SETTINGS",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "500",
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px"
		},
		"parent": "pnlMainHeaderMiddleCell"
	},
	"pnlUsernameCaption442310": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUsernameCaption442310",
			"componentVisible": true
		},
		"style": {
			"textAlign": "left",
			"paddingBottom": "2.5px"
		},
		"parent": "pnlMainUsername462"
	},
	"pnlMainPassword": {
		"parent": "pnlMainBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainPassword",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"paddingBottom": "2%",
			"width": "100%",
			"paddingTop": "2%",
			"paddingLeft": "5%",
			"paddingRight": "5%"
		}
	},
	"pnlMainPassword387": {
		"style": {
			"paddingTop": "2%",
			"paddingBottom": "2%",
			"width": "100%",
			"textAlign": "center",
			"paddingLeft": "5%",
			"paddingRight": "5%"
		},
		"parent": "pnlMainBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainPassword387",
			"componentVisible": true
		}
	},
	"txtCompany": {
		"property": {
			"componentType": "edit",
			"componentName": "txtCompany",
			"type": "text",
			"componentVisible": true
		},
		"style": {
			"height": "50px",
			"boxShadow": "none",
			"color": "rgb(0, 0, 0)",
			"width": "100%",
			"border": "1px solid #3C3C3B",
			"backgroundColor": "rgba(255, 255, 255, 0)",
			"margin": "0px",
			"fontSize": "14px"
		},
		"parent": "pnlPasswordText420574"
	},
	"imgModalLoading": {
		"style": {
			"width": "50px",
			"height": "10px"
		},
		"parent": "pnlModalBodyLoading",
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		}
	},
	"pnlMainHeaderLeft": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeft",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"PrinterOption": {
		"property": {
			"componentType": "label",
			"componentName": "PrinterOption",
			"componentValue": "DEFAULT PRINTER",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "12pt",
			"backgroundColor": "rgb(71, 65, 65)",
			"fontWeight": "bold",
			"marginBottom": "10px"
		},
		"parent": "Panel597"
	},
	"cmbPrinter": {
		"property": {
			"componentType": "combobox",
			"componentName": "cmbPrinter",
			"componentVisible": true
		},
		"parent": "Panel597"
	},
	"pnlPasswordCaption934195": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlPasswordCaption934195",
			"componentVisible": true
		},
		"style": {
			"textAlign": "left",
			"paddingBottom": "2.5px"
		},
		"parent": "pnlMainPassword387"
	},
	"pnlMainHeaderRightCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightCell",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"display": "table-cell",
			"width": "100%",
			"verticalAlign": "middle",
			"textAlign": "right"
		},
		"parent": "pnlMainHeaderRightTable"
	},
	"pnlPasswordCaption": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlPasswordCaption",
			"componentVisible": true
		},
		"style": {
			"textAlign": "left",
			"paddingBottom": "2.5px"
		},
		"parent": "pnlMainPassword"
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"float": "none",
			"display": "inline-block",
			"width": "50%",
			"textAlign": "center",
			"padding": "10px 0px"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlMainUsername462": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainUsername462",
			"componentVisible": true
		},
		"style": {
			"paddingBottom": "2%",
			"width": "100%",
			"textAlign": "center",
			"paddingTop": "2%",
			"paddingLeft": "5%",
			"paddingRight": "5%"
		},
		"parent": "pnlMainBody"
	},
	"pnlPasswordText": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlPasswordText",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "2.5px"
		},
		"parent": "pnlMainPassword"
	},
	"lblPassword529": {
		"parent": "pnlPasswordCaption934195",
		"property": {
			"componentType": "label",
			"componentName": "lblPassword529",
			"componentValue": "Default Company",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontSize": "14px",
			"marginLeft": "5px",
			"fontWeight": "bold",
			"backgroundColor": "rgba(0, 0, 0, 0)"
		}
	},
	"pnlMain4444792": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain4444792",
			"componentVisible": true
		},
		"style": {
			"display": "none",
			"textAlign": "center",
			"width": "100%"
		},
		"parent": "pnlMainBody"
	},
	"pnlModalBodyTitle": {
		"style": {
			"marginBottom": "10px",
			"width": "100%"
		},
		"parent": "pnlModalBodyMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		}
	},
	"pnlMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"zIndex": "0",
			"width": "100%",
			"backgroundColor": "rgb(245, 245, 245)"
		},
		"parent": "pgSettings"
	},
	"pnlMainHeaderRight": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRight",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%",
			"paddingRight": "10px"
		},
		"parent": "pnlMainHeader"
	},
	"Language938127": {
		"property": {
			"componentType": "label",
			"componentName": "Language938127",
			"componentValue": "DEFAULT LANGUAGE",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"fontSize": "12pt",
			"backgroundColor": "rgb(71, 65, 65)",
			"color": "rgb(255, 255, 255)",
			"fontWeight": "bold",
			"marginBottom": "10px"
		},
		"parent": "Panel597690"
	},
	"pnlHeaderTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHeaderTitle",
			"componentVisible": true
		},
		"parent": "pnlHeader"
	},
	"txtURL": {
		"property": {
			"componentType": "edit",
			"componentName": "txtURL",
			"placeHolder": "Username",
			"componentVisible": true
		},
		"style": {
			"height": "50px",
			"color": "rgb(0, 0, 0)",
			"width": "100%",
			"fontSize": "14px",
			"backgroundColor": "rgba(255, 255, 255, 0)",
			"border": "1px solid #3C3C3B"
		},
		"parent": "pnlUsernameText"
	},
	"imgFingerprint609317": {
		"parent": "pnlMain4444792",
		"property": {
			"componentType": "image",
			"componentName": "imgFingerprint609317",
			"componentValue": require("../../asset/icon/icon-fingerprint-white.png"),
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"width": "70px",
			"height": "70px"
		}
	},
	"pnlModalBodyButtonPositive": {
		"style": {
			"display": "inline-block",
			"width": "50%",
			"textAlign": "center",
			"padding": "10px 0px",
			"float": "none"
		},
		"parent": "pnlModalBodyButtons",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		}
	},
	"imgBack": {
		"property": {
			"componentType": "image",
			"componentName": "imgBack",
			"componentValue": require("../../asset/icon/icon-menu.png"),
			"componentVisible": true
		},
		"style": {
			"width": "35px",
			"height": "35px"
		},
		"parent": "pnlMainHeaderLeftCell"
	},
	"pnlMainHeaderMiddle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddle",
			"componentVisible": true
		},
		"style": {
			"width": "70%",
			"height": "100%",
			"float": "left"
		},
		"parent": "pnlMainHeader"
	},
	"pnlMainHeaderMiddleTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddle"
	},
	"cmbLanguage270628": {
		"property": {
			"componentType": "combobox",
			"componentName": "cmbLanguage270628",
			"componentVisible": true,
			"componentOptions": [{"label": "XXRAJ174303023", "value": "B0:91:22:EF:1C:A2"}, 
    		{"label": "QLN320A3", "value": "AC:3F:A4:0C:6F:BF"}]
		},
		"parent": "Panel597690"
	},
	"lblSubTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblSubTitle",
			"componentValue": "CONNECTOR",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "12pt",
			"textAlign": "left",
			"width": "100%",
			"fontWeight": "bold",
			"backgroundColor": "rgb(71, 65, 65)"
		},
		"parent": "pnlHeaderTitle"
	},
	"pnlUsernameText": {
		"parent": "pnlMainUsername",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUsernameText",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "2.5px"
		}
	},
	"lblModalTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "18px",
			"fontWeight": "bold"
		},
		"parent": "pnlModalBodyTitle"
	},
	"pnlUsernameCaption": {
		"parent": "pnlMainUsername",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUsernameCaption",
			"componentVisible": true
		},
		"style": {
			"textAlign": "left",
			"paddingBottom": "2.5px"
		}
	},
	"lblPassword": {
		"property": {
			"componentType": "label",
			"componentName": "lblPassword",
			"componentValue": "Password",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgba(0, 0, 0, 0)",
			"color": "rgb(60, 60, 59)",
			"fontWeight": "bold",
			"marginLeft": "5px",
			"fontSize": "14px"
		},
		"parent": "pnlPasswordCaption"
	},
	"lblTouchSensor270401": {
		"property": {
			"componentType": "label",
			"componentName": "lblTouchSensor270401",
			"componentValue": "N/A",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "14px",
			"fontWeight": "bold",
			"marginLeft": "5px"
		},
		"parent": "pnlMain4444792"
	},
	"pnlModal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"height": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"position": "fixed",
			"top": "0px",
			"zIndex": "3",
			"width": "100%"
		},
		"parent": "pnlMain"
	},
	"pnlMainHeaderMiddleCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleCell",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlMainHeaderMiddleTable"
	},
	"pnlModalBodyMain": {
		"style": {
			"textAlign": "center",
			"background": "#ffffff",
			"borderBottom": "1px solid #D9D9D9",
			"marginLeft": "5%",
			"marginRight": "5%",
			"width": "90%",
			"padding": "10px 0px"
		},
		"parent": "pnlModalCell",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		}
	},
	"txtPassword": {
		"property": {
			"componentType": "edit",
			"componentName": "txtPassword",
			"type": "password",
			"componentVisible": true
		},
		"style": {
			"height": "50px",
			"boxShadow": "none",
			"color": "rgb(0, 0, 0)",
			"margin": "0px",
			"width": "100%",
			"fontSize": "14px",
			"border": "1px solid #3C3C3B",
			"backgroundColor": "rgba(255, 255, 255, 0)"
		},
		"parent": "pnlPasswordText"
	},
	"pnlModalBodyLoading": {
		"parent": "pnlModalBodyMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		}
	},
	"lblModalPositive": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonPositive"
	},
	"Panel597": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel597",
			"componentVisible": true
		},
		"style": {
			"paddingLeft": "5%",
			"paddingRight": "5%"
		},
		"parent": "Panel92"
	},
	"pnlMainBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"backgroundColor": "rgba(0, 0, 0, 0)"
		},
		"parent": "Panel92"
	},
	"pnlPasswordText420574": {
		"parent": "pnlMainPassword387",
		"property": {
			"componentType": "panel",
			"componentName": "pnlPasswordText420574",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "2.5px"
		}
	},
	"pnlModalTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table"
		},
		"parent": "pnlModal"
	},
	"pgSettings": {
		"property": {
			"componentType": "page",
			"componentName": "pgSettings",
			"title": "Settings",
			"componentVisible": true
		}
	},
	"pnlModalBodyMessage": {
		"parent": "pnlModalBodyMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		}
	},
	"pnlUsernameText24294": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUsernameText24294",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "2.5px"
		},
		"parent": "pnlMainUsername462"
	},
	"pnlModalBodyButtons": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"background": "#ffffff",
			"marginLeft": "5%",
			"marginRight": "5%",
			"width": "90%",
			"textAlign": "center"
		},
		"parent": "pnlModalCell"
	},
	"pnlMainHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"paddingTop": "15px",
			"overflow": "auto",
			"height": "65px",
			"backgroundColor": "rgb(255, 69, 0)",
			"width": "100%"
		},
		"parent": "pnlMain"
	},
	"pnlMainHeaderLeftTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderLeft"
	},
	"pnlMainUsername": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainUsername",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "2%",
			"paddingLeft": "5%",
			"paddingRight": "5%",
			"width": "100%",
			"textAlign": "center",
			"paddingBottom": "2%"
		},
		"parent": "pnlMainBody"
	},
	"lblUsername898": {
		"property": {
			"componentType": "label",
			"componentName": "lblUsername898",
			"componentValue": "Username",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"marginLeft": "5px",
			"fontSize": "14px",
			"fontWeight": "bold"
		},
		"parent": "pnlUsernameCaption442310"
	},
	"Panel597690": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel597690",
			"componentVisible": true
		},
		"style": {
			"paddingLeft": "5%",
			"paddingRight": "5%"
		},
		"parent": "Panel92"
	},
	"pnlHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHeader",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"paddingLeft": "5%",
			"paddingRight": "5%"
		},
		"parent": "pnlMainBody"
	},
	"txtUsername": {
		"property": {
			"componentType": "edit",
			"componentName": "txtUsername",
			"placeHolder": "Username",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(0, 0, 0)",
			"fontSize": "14px",
			"backgroundColor": "rgba(255, 255, 255, 0)",
			"height": "50px",
			"border": "1px solid #3C3C3B"
		},
		"parent": "pnlUsernameText24294"
	},
	"pnlModalCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModalTable"
	}
};
