export const StateName = 'RenderPage';
export const State = {
    labelFirstName: {
        property: {
            componentUUID: 'RP1',
            componentType: 'label',
            componentValue: 'Kevin'
        }
    },
    labelLastName: {
        property: {
            componentUUID: 'RP2',
            componentType: 'label',
            componentValue: 'Tan'
        }
    },
    buttonChangeName: {
        property: {
            componentUUID: 'RP3',
            componentType: 'button',
            componentValue: 'Change Name'
        }
    },
    buttonGoToPage: {
        property: {
            componentUUID: 'RP4',
            componentType: 'button',
            componentValue: 'Go To Page'
        }
    }
}