export const StateName = 'pgSalesReturnDetailsNew';
export const State = {
	"pnlUpdateQtyContent": {
		"style": {
			"display": "block",
			"width": "90%",
			"textAlign": "center",
			"background": "#ffffff",
			"marginLeft": "5%",
			"marginRight": "5%"
		},
		"parent": "pnlUpdateQtyTableCell",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContent",
			"componentVisible": true
		}
	},
	"imgUpdateQtyItemImage": {
		"property": {
			"componentType": "image",
			"componentName": "imgUpdateQtyItemImage",
			"componentValue": require("../../asset/images/default.png"),
			"componentVisible": true
		},
		"style": {
			"width": "100px",
			"height": "100px"
		},
		"parent": "pnlUpdateQtyItemImage"
	},
	"lblUpdateQtyItemUnitPrice": {
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateQtyItemUnitPrice",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "normal",
			"textAlign": "left",
			"display": "block",
			"color": "rgb(0, 153, 0)",
			"fontSize": "20px"
		},
		"parent": "pnlUpdateQtyItemUnitPrice"
	},
	"pnlUpdateQtyContentInput": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentInput",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgb(71, 65, 65)",
			"padding": "5%",
			"paddingBottom": "20px"
		},
		"parent": "pnlUpdateQtyContentMainInput"
	},
	"lblUpdateQtyUpdate": {
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateQtyUpdate",
			"componentValue": "UPDATE",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px"
		},
		"parent": "pnlUpdateQtyFooterBtnTableCell"
	},
	"imgBack": {
		"property": {
			"componentType": "image",
			"componentName": "imgBack",
			"componentValue": require("../../asset/icon/icon-back.png"),
			"componentVisible": true
		},
		"style": {
			"width": "35px",
			"height": "35px"
		},
		"parent": "pnlMainHeaderLeftCell"
	},
	"pnlMainHeaderMiddle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddle",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "70%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlMainBodyDetails1872": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyDetails1872",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"overflow": "auto",
			"paddingTop": "5px",
			"borderTop": "0px solid #FFF"
		},
		"parent": "pnlMainBodyDetails"
	},
	"pnlUpdateQtyContentInputQtyTableCell3": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentInputQtyTableCell3",
			"componentVisible": true
		},
		"style": {
			"width": "50px",
			"backgroundColor": "rgb(255, 69, 0)",
			"display": "table-cell",
			"height": "40px"
		},
		"parent": "pnlUpdateQtyContentInputQtyTable"
	},
	"imgPlus": {
		"style": {
			"float": "left",
			"width": "50px",
			"height": "40px",
			"border": "1px solid #FFF"
		},
		"parent": "pnlUpdateQtyContentInputQtyTableCell3",
		"property": {
			"componentType": "image",
			"componentName": "imgPlus",
			"componentValue": require("../../asset/plus-white.png"),
			"componentVisible": true
		}
	},
	"pnlSelectReasonCode": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSelectReasonCode",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"top": "0px",
			"zIndex": "2",
			"background": "rgba(0, 0, 0, 0.60)",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlUpdateQtyContentHeader387": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentHeader387",
			"componentVisible": true
		},
		"style": {
			"padding": "3%",
			"zIndex": "1",
			"minWidth": "40px",
			"minHeight": "50px"
		},
		"parent": "Panel513530"
	},
	"pnlMainHeaderMiddleTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTable",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table"
		},
		"parent": "pnlMainHeaderMiddle"
	},
	"pnlMainBodyItemsCO2656": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyItemsCO2656",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		},
		"parent": "pnlMainBodyItems84"
	},
	"Panel513": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel513",
			"componentVisible": true
		},
		"style": {
			"minWidth": "40px",
			"minHeight": "50px",
			"paddingTop": "2%"
		},
		"parent": "pnlUpdateQtyContent"
	},
	"lblUpdateQtyItemName": {
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateQtyItemName",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontWeight": "normal",
			"textAlign": "left",
			"display": "block",
			"fontSize": "16px"
		},
		"parent": "pnlUpdateQtyItemName"
	},
	"pnlUpdateQtyContentMainInput": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentMainInput",
			"componentVisible": true
		},
		"style": {
			"minWidth": "40px",
			"minHeight": "50px"
		},
		"parent": "Panel513"
	},
	"lblUpdateQtyContentInputUOM": {
		"style": {
			"color": "rgb(255, 255, 255)",
			"textAlign": "center",
			"display": "block",
			"fontSize": "16px",
			"fontWeight": "bold"
		},
		"parent": "pnlUpdateQtyContentInputUOM",
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateQtyContentInputUOM",
			"componentVisible": true
		}
	},
	"pnlModalBodyButtons": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"background": "#ffffff",
			"marginLeft": "5%",
			"width": "90%",
			"marginRight": "5%"
		},
		"parent": "pnlModalCell"
	},
	"lblModalNegative": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonNegative"
	},
	"pnlUpdateQtyContentInputQtyTableCell2": {
		"style": {
			"width": "calc(100% - 120px)",
			"display": "table-cell",
			"height": "40px"
		},
		"parent": "pnlUpdateQtyContentInputQtyTable",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentInputQtyTableCell2",
			"componentVisible": true
		}
	},
	"txtQuantity": {
		"property": {
			"componentType": "edit",
			"componentName": "txtQuantity",
			"type": "number",
			"componentValue": "0",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"border": "1px solid #FFF",
			"width": "100%",
			"height": "40px",
			"fontSize": "24px",
			"fontWeight": "normal",
			"float": "left",
			"color": "rgb(33, 33, 33)"
		},
		"parent": "pnlUpdateQtyContentInputQtyTableCell2"
	},
	"Panel513530": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel513530",
			"componentVisible": true
		},
		"style": {
			"minHeight": "50px",
			"minWidth": "40px"
		},
		"parent": "pnlUpdateQtyContent277"
	},
	"pnlMainHeaderRightTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRight"
	},
	"pnlMainBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		},
		"style": {
			"overflow": "auto",
			"width": "100%",
			"height": "calc(100% - 50px - 40px - 0px - 15px)"
		},
		"parent": "pnlMain"
	},
	"dtSales_Return_Lines": {
		"property": {
			"componentType": "datalist",
			"componentName": "dtSales_Return_Lines",
			"componentValue": "l_ Sales_Return_Lines",
			"autoLoad": false,
			"componentData": [],
			"componentVisible": true
		},
		"parent": "pnlMainBodyItemsCO2656",
		"template": {
			"componentDataItemState": {
				"Label335234361": {
					"style": {
						"color": "rgb(255, 255, 255)",
						"fontSize": "16px"
					},
					"parent": "Panel489504",
					"property": {
						"componentType": "label",
						"componentName": "Label335234361",
						"componentValue": "REASON",
						"componentVisible": true
					}
				},
				"Label335785391": {
					"property": {
						"componentType": "label",
						"componentName": "Label335785391",
						"componentValue": "REMOVE",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(255, 255, 255)",
						"fontSize": "16px"
					},
					"parent": "Panel489850"
				},
				"pnlProductBodyMiddle": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle",
						"componentVisible": true
					},
					"style": {
						"verticalAlign": "top",
						"width": "calc(100% - 110px)",
						"paddingLeft": "10px",
						"display": "table-cell"
					},
					"parent": "pnlProduct"
				},
				"lblItemName": {
					"style": {
						"textAlign": "left",
						"color": "rgb(111, 111, 110)",
						"fontSize": "20px",
						"fontWeight": "normal",
						"display": "block"
					},
					"parent": "pnlProductBodyMiddle1",
					"property": {
						"componentType": "label",
						"componentName": "lblItemName",
						"field": "Description",
						"componentVisible": true
					}
				},
				"lblPrice": {
					"style": {
						"fontSize": "18px",
						"fontWeight": "bold",
						"paddingTop": "2px",
						"color": "rgb(0, 153, 0)",
						"textAlign": "left",
						"display": "block",
						"marginTop": "10px"
					},
					"parent": "pnlProductBodyMiddle1",
					"property": {
						"componentType": "label",
						"componentName": "lblPrice",
						"componentVisible": true
					}
				},
				"Panel233": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel233",
						"componentVisible": true
					},
					"style": {
						"width": "100%",
						"height": "30px",
						"backgroundColor": "rgb(71, 65, 65)",
						"borderRadius": "0px 0px 5px 5px",
						"display": "table"
					},
					"parent": "Panel17"
				},
				"pnlProduct": {
					"style": {
						"position": "relative",
						"textAlign": "left",
						"backgroundColor": "rgb(255, 255, 255)",
						"borderRadius": "5px 5px 0px 0px",
						"width": "100%",
						"display": "table",
						"padding": "5px",
						"paddingLeft": "0px"
					},
					"parent": "Panel17",
					"property": {
						"componentType": "panel",
						"componentName": "pnlProduct",
						"componentVisible": true
					}
				},
				"pnlProductBodyMiddle1": {
					"style": {
						"width": "100%"
					},
					"parent": "pnlProductBodyMiddle",
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle1",
						"componentVisible": true
					}
				},
				"Label335": {
					"parent": "Panel489",
					"property": {
						"componentType": "label",
						"componentName": "Label335",
						"componentValue": "EDIT QTY",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(255, 255, 255)",
						"fontSize": "16px"
					}
				},
				"Panel489850": {
					"style": {
						"display": "table-cell",
						"verticalAlign": "middle",
						"width": "33%",
						"textAlign": "center"
					},
					"parent": "Panel233",
					"property": {
						"componentType": "panel",
						"componentName": "Panel489850",
						"componentVisible": true
					}
				},
				"pnlProductBodyLeft": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyLeft",
						"componentVisible": true
					},
					"style": {
						"verticalAlign": "top",
						"textAlign": "center",
						"display": "table-cell",
						"width": "110px"
					},
					"parent": "pnlProduct"
				},
				"Panel489": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel489",
						"componentVisible": true
					},
					"style": {
						"display": "table-cell",
						"verticalAlign": "middle",
						"width": "33%",
						"textAlign": "center"
					},
					"parent": "Panel233"
				},
				"Panel489504": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel489504",
						"componentVisible": true
					},
					"style": {
						"borderLeft": "2px solid #FFF",
						"borderRight": "2px solid #FFF",
						"verticalAlign": "middle",
						"textAlign": "center",
						"display": "table-cell",
						"width": "33%"
					},
					"parent": "Panel233"
				},
				"lblReasonCode_data": {
					"property": {
						"componentType": "label",
						"componentName": "lblReasonCode_data",
						"field": "Return_Reason_Code",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(153, 0, 0)",
						"fontSize": "16px"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"Panel17": {
					"style": {
						"display": "table",
						"width": "100%",
						"boxShadow": "-1px 1px 1px #545454",
						"border": "1px solid #3C3C3B",
						"borderRadius": "5px"
					},
					"property": {
						"componentType": "panel",
						"componentName": "Panel17",
						"componentVisible": true
					}
				},
				"imgItem": {
					"style": {
						"width": "100px",
						"height": "90px"
					},
					"parent": "pnlProductBodyLeft",
					"property": {
						"componentType": "image",
						"componentName": "imgItem",
						"componentValue": require("../../asset/images/default.png"),
						"componentVisible": true
					}
				},
				"lblItemCode": {
					"property": {
						"componentType": "label",
						"componentName": "lblItemCode",
						"field": "No",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(60, 60, 59)",
						"fontSize": "24px",
						"fontWeight": "bold"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"lblDiscCO": {
					"property": {
						"componentType": "label",
						"componentName": "lblDiscCO",
						"componentValue": "Disc",
						"field": "disc",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(60, 60, 59)",
						"fontWeight": "normal",
						"display": "none",
						"fontSize": "16px"
					},
					"parent": "pnlProductBodyMiddle1"
				}
			}
		}
	},
	"pnlUpdateQtyTable": {
		"parent": "pnlUpdateQty",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		}
	},
	"pnlUpdateQtyContentInputUOM": {
		"style": {
			"textAlign": "left"
		},
		"parent": "pnlUpdateQtyContentInput",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentInputUOM",
			"componentVisible": true
		}
	},
	"pnlUpdateQtyFooterBtnTableCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyFooterBtnTableCell",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlUpdateQtyFooterBtnTable"
	},
	"pnlUpdateQtyItemImage874": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyItemImage874",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"position": "relative",
			"display": "block",
			"height": "50px"
		},
		"parent": "pnlUpdateQtyContentHeader387"
	},
	"pnlHolder": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"backgroundColor": "rgb(245, 245, 245)"
		},
		"parent": "pgSalesReturnDetailsNew"
	},
	"pnlBodyContentHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyContentHeader",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlBodyMainContent"
	},
	"pnlModalCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlModalTable"
	},
	"pnlUpdateQtyContentInputQtyTable": {
		"style": {
			"height": "40px",
			"width": "100%",
			"display": "table"
		},
		"parent": "pnlUpdateQtyContentInputQty",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentInputQtyTable",
			"componentVisible": true
		}
	},
	"pnlUpdateQtyFooterBtnTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyFooterBtnTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "40px"
		},
		"parent": "pnlUpdateQtyFooterBtn"
	},
	"Panel960": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel960",
			"componentVisible": true
		},
		"style": {
			"width": "50%",
			"textAlign": "center",
			"borderRight": "1px solid #FFF",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlUpdateQtyFooterBtn704"
	},
	"lblCustomerCodeName": {
		"property": {
			"componentType": "label",
			"componentName": "lblCustomerCodeName",
			"field": "code",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"textAlign": "center",
			"color": "rgb(255, 215, 0)",
			"fontWeight": "bold",
			"display": "block"
		},
		"parent": "pnlBodyContentHeader"
	},
	"pnlUpdateQtyTableCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyTableCell",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%",
			"display": "table-cell"
		},
		"parent": "pnlUpdateQtyTable"
	},
	"pnlUpdateQtyItemNo": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyItemNo",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"width": "100%",
			"marginTop": "10px"
		},
		"parent": "pnlUpdateQtyContentHeader"
	},
	"pnlUpdateQtyContentInputQty": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentInputQty",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlUpdateQtyContentInput"
	},
	"cmbReasonCode": {
		"parent": "pnlUpdateQtyItemImage874",
		"property": {
			"componentType": "combobox",
			"componentName": "cmbReasonCode",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px"
		}
	},
	"pnlUpdateQtyFooterBtn704": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyFooterBtn704",
			"componentVisible": true
		},
		"style": {
			"height": "40px",
			"backgroundColor": "rgb(255, 69, 0)",
			"borderRadius": "0px 0px 10px 10px",
			"width": "100%",
			"textAlign": "center",
			"display": "table"
		},
		"parent": "pnlUpdateQtyContent277"
	},
	"pnlMainHeaderLeft": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeft",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"float": "left",
			"width": "15%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlMainHeaderMiddleCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddleTable"
	},
	"pnlMainBodyDetails": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyDetails",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"padding": "10px",
			"display": "none",
			"height": "60px",
			"backgroundColor": "rgb(84, 84, 84)"
		},
		"parent": "pnlMain"
	},
	"pnlModalBodyButtonPositive": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "50%",
			"padding": "10px 0px",
			"display": "inline-block",
			"float": "none",
			"textAlign": "center"
		},
		"parent": "pnlModalBodyButtons"
	},
	"lblModalPositive": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonPositive"
	},
	"Label750": {
		"property": {
			"componentType": "label",
			"componentName": "Label750",
			"componentValue": "CANCEL",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"fontWeight": "500"
		},
		"parent": "Panel561"
	},
	"pnlMainHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		},
		"style": {
			"height": "65px",
			"textAlign": "center",
			"backgroundColor": "rgb(255, 69, 0)",
			"width": "100%",
			"paddingTop": "15"
		},
		"parent": "pnlMain"
	},
	"lblOrderDate": {
		"property": {
			"componentType": "label",
			"componentName": "lblOrderDate",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "normal",
			"fontSize": "16px",
			"textAlign": "center",
			"display": "block",
			"color": "rgb(255, 255, 255)"
		},
		"parent": "pnlBodyContentHeader"
	},
	"pnlMainBodyHeaderCaption": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyHeaderCaption",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"backgroundColor": "rgb(135, 135, 135)"
		},
		"parent": "pnlMainBody"
	},
	"imgMinus": {
		"style": {
			"float": "left",
			"width": "50px",
			"height": "40px",
			"border": "1px solid #FFF"
		},
		"parent": "pnlUpdateQtyContentInputQtyTableCell1",
		"property": {
			"componentType": "image",
			"componentName": "imgMinus",
			"componentValue": require("../../asset/minus-white.png"),
			"componentVisible": true
		}
	},
	"Label9": {
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"fontWeight": "500"
		},
		"parent": "Panel960",
		"property": {
			"componentType": "label",
			"componentName": "Label9",
			"componentValue": "OK",
			"componentVisible": true
		}
	},
	"lblDataTotalAmountExclGST363": {
		"property": {
			"componentType": "label",
			"componentName": "lblDataTotalAmountExclGST363",
			"componentValue": "$ 0.00",
			"componentVisible": true
		},
		"style": {
			"float": "right",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "bolder"
		},
		"parent": "pnlMainBodyDetails1872"
	},
	"pnlMainFooterTbl361": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterTbl361",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainFooter"
	},
	"imgUpdateQtyClose": {
		"property": {
			"componentType": "image",
			"componentName": "imgUpdateQtyClose",
			"componentValue": require("../../asset/icon/icon-close-gray.png"),
			"componentVisible": true
		},
		"style": {
			"zIndex": "2",
			"float": "right",
			"height": "25px",
			"position": "absolute",
			"right": "7%",
			"width": "25px"
		},
		"parent": "Panel513"
	},
	"Label285": {
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontSize": "18px",
			"fontWeight": "500"
		},
		"parent": "Panel401",
		"property": {
			"componentType": "label",
			"componentName": "Label285",
			"componentValue": "Select Return Reason",
			"componentVisible": true
		}
	},
	"pgSalesReturnDetailsNew": {
		"property": {
			"componentType": "page",
			"componentName": "pgSalesReturnDetailsNew",
			"title": "Sales Return Details New",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgba(0, 0, 0, 0)"
		}
	},
	"pnlMainHeaderLeftTable": {
		"parent": "pnlMainHeaderLeft",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTable",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"display": "table",
			"width": "100%"
		}
	},
	"lblTotalAmountExclGST237": {
		"property": {
			"componentType": "label",
			"componentName": "lblTotalAmountExclGST237",
			"componentValue": "Total Excl. GST",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "bolder"
		},
		"parent": "pnlMainBodyDetails1872"
	},
	"pnlUpdateQtyTableCell515": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyTableCell515",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlUpdateQtyTable992753"
	},
	"pnlMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"zIndex": "0",
			"width": "100%"
		},
		"parent": "pnlHolder"
	},
	"lblAddress": {
		"property": {
			"componentType": "label",
			"componentName": "lblAddress",
			"field": "name",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"fontSize": "16px",
			"fontWeight": "normal",
			"float": "initial",
			"color": "rgb(255, 255, 255)",
			"display": "block"
		},
		"parent": "pnlBodyContentHeader"
	},
	"lblModalTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"fontWeight": "bold",
			"width": "100%",
			"color": "rgb(60, 60, 59)"
		},
		"parent": "pnlModalBodyTitle"
	},
	"lblStyle744": {
		"property": {
			"componentType": "label",
			"componentName": "lblStyle744",
			"componentValue": "N/A",
			"componentVisible": true
		},
		"parent": "pnlCSS"
	},
	"pnlUpdateQtyItemName": {
		"style": {
			"position": "relative",
			"width": "100%"
		},
		"parent": "pnlUpdateQtyContentHeader",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyItemName",
			"componentVisible": true
		}
	},
	"pnlMainHeaderRightCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightCell",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%"
		},
		"parent": "pnlMainHeaderRightTable"
	},
	"lblItems509": {
		"style": {
			"color": "rgb(255, 215, 0)",
			"fontWeight": "bold",
			"paddingBottom": "5px",
			"width": "calc(100% - 35px)",
			"paddingTop": "5px",
			"padding": "10px",
			"display": "inline-block",
			"fontSize": "16px"
		},
		"parent": "pnlMainBodyHeaderCaption",
		"property": {
			"componentType": "label",
			"componentName": "lblItems509",
			"componentValue": "ITEMS",
			"componentVisible": true
		}
	},
	"pnlModalTable": {
		"style": {
			"height": "100%",
			"display": "table",
			"width": "100%"
		},
		"parent": "pnlModal",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		}
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"padding": "10px 0px",
			"width": "50%",
			"display": "inline-block",
			"float": "none"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlCSS": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCSS",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"width": "100%",
			"height": "100%",
			"display": "none",
			"top": "0px",
			"zIndex": "4"
		},
		"parent": "pnlHolder"
	},
	"pnlUpdateQtyContentHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentHeader",
			"componentVisible": true
		},
		"style": {
			"zIndex": "1",
			"minWidth": "40px",
			"minHeight": "50px",
			"padding": "3%"
		},
		"parent": "Panel513"
	},
	"pnlUpdateQtyItemUnitPrice": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyItemUnitPrice",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginTop": "20px"
		},
		"parent": "pnlUpdateQtyContentHeader"
	},
	"btnUpdateQtyUpdate": {
		"style": {
			"width": "100%",
			"height": "40px",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"backgroundColor": "rgb(255, 69, 0)",
			"padding": "8px",
			"fontWeight": "bold",
			"margin": "0px",
			"display": "none"
		},
		"parent": "pnlUpdateQtyFooterBtn",
		"property": {
			"componentType": "fbutton",
			"componentName": "btnUpdateQtyUpdate",
			"componentValue": "UPDATE",
			"componentVisible": true
		}
	},
	"pnlMainFooter": {
		"parent": "pnlMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooter",
			"componentVisible": true
		},
		"style": {
			"height": "40px",
			"backgroundColor": "rgb(255, 69, 0)",
			"width": "100%"
		}
	},
	"pnlModalBodyMessage": {
		"style": {
			"width": "100%",
			"padding": "10px",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		}
	},
	"lblModalMessage": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(111, 111, 110)",
			"fontSize": "18px"
		},
		"parent": "pnlModalBodyMessage"
	},
	"pnlUpdateQtyContent277": {
		"style": {
			"display": "block",
			"marginLeft": "5%",
			"borderRadius": "10px",
			"marginRight": "5%",
			"width": "90%",
			"textAlign": "center",
			"background": "#ffffff"
		},
		"parent": "pnlUpdateQtyTableCell515",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContent277",
			"componentVisible": true
		}
	},
	"pnlMainBodyItems84": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyItems84",
			"addClass": "pnlMainBodyHeader",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		},
		"parent": "pnlMainBody"
	},
	"pnlModalBodyLoading": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlUpdateQty": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQty",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"width": "100%",
			"height": "100%",
			"top": "0px",
			"zIndex": "2",
			"background": "rgba(0, 0, 0, 0.60)"
		},
		"parent": "pnlHolder"
	},
	"pnlUpdateQtyTable992753": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyTable992753",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table"
		},
		"parent": "pnlSelectReasonCode"
	},
	"Panel561": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel561",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"width": "50%",
			"verticalAlign": "middle",
			"textAlign": "center",
			"borderLeft": "1px solid #FFF"
		},
		"parent": "pnlUpdateQtyFooterBtn704"
	},
	"lblMainTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle",
			"componentValue": "SALES RETURN DETAILS",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500"
		},
		"parent": "pnlMainHeaderMiddleCell"
	},
	"pnlMainHeaderRight": {
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRight",
			"componentVisible": true
		}
	},
	"pnlBodyMainContent": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyMainContent",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"padding": "10px"
		},
		"parent": "pnlMainBodyHeader"
	},
	"pnlUpdateQtyItemImage": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyItemImage",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"width": "100%",
			"height": "100px",
			"display": "block"
		},
		"parent": "pnlUpdateQtyContentHeader"
	},
	"lblUpdateQtyItemNo": {
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateQtyItemNo",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "normal",
			"textAlign": "left",
			"display": "block",
			"color": "rgb(60, 60, 59)",
			"fontSize": "24px"
		},
		"parent": "pnlUpdateQtyItemNo"
	},
	"pnlUpdateQtyContentInputQtyTableCell1": {
		"parent": "pnlUpdateQtyContentInputQtyTable",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentInputQtyTableCell1",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"width": "50px",
			"height": "40px",
			"backgroundColor": "rgb(255, 69, 0)"
		}
	},
	"pnlMainBodyHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyHeader",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"backgroundColor": "rgb(84, 84, 84)"
		},
		"parent": "pnlMainBody"
	},
	"pnlModalBodyMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		},
		"style": {
			"borderBottom": "1px solid #D9D9D9",
			"padding": "10px 0px",
			"marginLeft": "5%",
			"width": "90%",
			"textAlign": "center",
			"background": "#ffffff",
			"marginRight": "5%"
		},
		"parent": "pnlModalCell"
	},
	"pnlModalBodyTitle": {
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		}
	},
	"lblBtnOrder72": {
		"property": {
			"componentType": "label",
			"componentName": "lblBtnOrder72",
			"componentValue": "SUBMIT",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"fontWeight": "bold",
			"display": "block",
			"color": "rgb(255, 255, 255)"
		},
		"parent": "pnlMainFooterTblCell1865"
	},
	"pnlModal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"top": "0px",
			"width": "100%",
			"zIndex": "3",
			"height": "100%",
			"background": "rgba(0, 0, 0, 0.60)"
		},
		"parent": "pnlHolder"
	},
	"imgModalLoading": {
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		},
		"style": {
			"width": "50px",
			"height": "10px"
		},
		"parent": "pnlModalBodyLoading"
	},
	"pnlUpdateQtyFooterBtn": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyFooterBtn",
			"componentVisible": true
		},
		"style": {
			"height": "40px",
			"backgroundColor": "rgb(255, 69, 0)",
			"display": "block",
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlUpdateQtyContent"
	},
	"Panel401": {
		"parent": "Panel513530",
		"property": {
			"componentType": "panel",
			"componentName": "Panel401",
			"componentVisible": true
		},
		"style": {
			"padding": "10px"
		}
	},
	"pnlMainHeaderLeftCell": {
		"parent": "pnlMainHeaderLeftTable",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftCell",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"textAlign": "left",
			"paddingLeft": "10px",
			"display": "table-cell",
			"verticalAlign": "middle"
		}
	},
	"Image553564": {
		"style": {
			"width": "30px"
		},
		"parent": "pnlMainBodyHeaderCaption",
		"property": {
			"componentType": "image",
			"componentName": "Image553564",
			"zoom": false,
			"componentValue": require("../../asset/plus-white.png"),
			"componentVisible": true
		}
	},
	"pnlMainFooterTblCell1865": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterTblCell1865",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"backgroundColor": "rgba(0, 0, 0, 0)",
			"display": "table-cell",
			"verticalAlign": "middle",
			"textAlign": "center"
		},
		"parent": "pnlMainFooterTbl361"
	}
};
