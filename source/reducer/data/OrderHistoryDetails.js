export const StateName = 'OrderHistoryDetails';
export const State = {
    page: {
        style:{
            color:"black"  
        }
    },
    pnlHolder: {
        style: {
            "overflow": "auto",
            "width": "100%",
            "height": "100%",
            "padding": "",
        },
        property:{
            componentName: "pnlHolder",
        }
    },
    pnlMain: {
        style: {
            "zIndex": "0",
            "width": "100%",
            "height": "100%",
            "background": "",
            "backgroundSize": "",
            "padding": "",
            "margin": "",
        },
        property:{
            componentName: "pnlMain",
        }
    },
    pnlMainHeader: {
        style: {
            "overflow": "auto",
            "width": "100%",
            "height": "65px",
            "textAlign": "center",
            "background": "",
            "backgroundColor": "rgb(255, 69, 0)",
            "padding": "",
            "paddingTop": "15px",
            "display": "inline"
        },
        property:{
            componentName: "pnlMainHeader",
        }
    },
    pnlMainHeaderLeft: {
        style: {
            "float": "left",
            "width": "15%",
            "height": "100%",
            "textAlign": "left",
            "padding": "",
            "paddingLeft": "10px",
        },
        property:{
            componentName: "pnlMainHeaderLeft",
        }
    },
    pnlMainHeaderLeftTable: {
        style: {
            "display": "table",
            "width": "100%",
            "height": "100%",
            "padding": "",
        },
        property:{
            componentName: "pnlMainHeaderLeftTable",
        }
    },
    pnlMainHeaderLeftCell: {
        style: {
            "display": "table-cell",
            "verticalAlign": "middle",
            "width": "100%",
            "height": "100%",
            "padding": "",
        },
        property:{
            componentName: "pnlMainHeaderLeftCell",
        }
    },
    imgBack: {
        style: {
            width: "35px",
            height: "35px,"
        },
        property: {
            //componentValue: require('../../framework/core/asset/default.png'),
            componentValue: require('../../asset/icon/icon-back.png'),
            componentVisible: true,
            componentName: "imgBack",
        },
        componentType: 'image',
    },
    pnlMainHeaderMiddle: {
        style: {
            "float": "left",
            "width": "70%",
            "height": "100%",
            "padding": "",
        },
        property:{
            componentName: "pnlMainHeaderMiddle",
        }
    },
    pnlMainHeaderMiddleTable: {
        style: {
            "display": "table",
            "width": "100%",
            "height": "100%",
            "padding": "",
        },
        property:{
            componentName: "pnlMainHeaderMiddleTable",
        }
    },
    pnlMainHeaderMiddleCell: {
        style: {
            "display": "table-cell",
            "verticalAlign": "middle",
            "width": "100%",
            "height": "100%",
            "padding": "",
        },
        property:{
            componentName: "pnlMainHeaderMiddleCell",
        }
    },
    lblMainTitle: {
        style: {
            "width": "100%",
            "color": "rgb(255, 255, 255)",
            "fontSize": "20px",
            "fontWeight": "500",
        },
        property: {
            componentValue: 'INVOICE HISTORY DETAILS',
            componentVisible: true,
            componentName: "lblMainTitle"
        },
        componentType: 'label',
    },
    pnlMainHeaderRight233: {
        style: {
            "float": "left",
            "width": "15%",
            "height": "100%",
            "textAlign": "right",
            "padding": "",
            "paddingRight": "10px",
        },
        property:{
            componentName: "pnlMainHeaderRight233",
        }
    },
    pnlMainHeaderRightTable2: {
        style: {
            "display": "table",
            "width": "100%",
            "height": "100%",
            "padding": "",
        },
        property:{
            componentName: "pnlMainHeaderRightTable2",
        }
    },
    pnlMainHeaderRightCell877: {
        style: {
            "display": "table-cell",
            "verticalAlign": "middle",
            "width": "100%",
            "height": "100%",
            "padding": "",
        },
        property:{
            componentName: "pnlMainHeaderRightCell877",
        }
    },
    imgBack555407: {
        style: {
            "width": "35px",
            "height": "35px",
        },
        property: {
            //componentValue: require('../../framework/core/asset/default.png'),
            componentValue: require('../../asset/Printer_white.png'),
            componentVisible: true,
            componentName: "imgBack555407"
        },
        componentType: 'image',
    },
    pnlMainBody: {
        style: {
            "overflow": "auto",
            "width": "100%",
            "height": "calc(100% - 50px - 40px - 105px - 15px)",
            "background": "",
            "padding": "",
        },
        property:{
            componentName: "pnlMainBody",
        }
    },
    pnlMainBodyForm: {
        style: {
            "width": "100%",
            "backgroundColor": "rgb(84, 84, 84)",
            "borderBottom": "",
            "padding": "",
        },
        property:{
            componentName: "pnlMainBodyForm",
        }
    },
    pnlMainBodyFormCustomerCode: {
        style: {
            "width": "100%",
            "textAlign": "center",
            "padding": "10px",
        },
        property:{
            componentName: "pnlMainBodyFormCustomerCode",
        }
    },
    pnlMainBodyForm3LabelCustomerCode: {
        style: {
            "width": "100%",
            "textAlign": "center",
            "padding": "",
        },
        property:{
            componentName: "pnlMainBodyForm3LabelCustomerCode",
        }
    },
    lblOrderNo_data: {
        style: {
            "display": "block",
            "color": "rgb(255, 255, 255)",
            "fontSize": "20px",
            "fontWeight": "bold",
            "textAlign": "center",
            "textShadow": "",
            "border": "",
            "padding": "",
        },
        property: {
            componentValue: 'lblOrderNo_data',
            componentVisible: true,
            componentName: "lblOrderNo_data"
        },
        componentType: 'label',
    },
    lblCustomerCodeName_data: {
        style: {
            "display": "block",
            "color": "rgb(255, 215, 0)",
            "fontSize": "18px",
            "fontWeight": "bold",
            "textAlign": "center",
            "textShadow": "",
            "border": "",
            "padding": "",
        },
        property: {
            componentValue: 'lblCustomerCodeName_data',
            componentVisible: true,
            componentName: "lblCustomerCodeName_data"
        },
        componentType: 'label',
    },
    lblAddress_data: {
        style: {
            "display": "block",
            "float": "initial",
            "color": "rgb(255, 255, 255)",
            "fontSize": "16px",
            "fontWeight": "normal",
            "textAlign": "center",
            "textShadow": "",
            "border": "",
            "padding": "",
        },
        property: {
            componentValue: 'lblAddress_data',
            componentVisible: true,
            componentName: "lblAddress_data"
        },
        componentType: 'label',
    },
    lblOrderDate_data: {
        style: {
            "display": "block",
            "color": "rgb(255, 255, 255)",
            "fontSize": "16px",
            "fontWeight": "normal",
            "textAlign": "center",
            "textShadow": "",
            "border": "",
            "padding": "",
        },
        property: {
            componentValue: 'lblOrderDate_data',
            componentVisible: true,
            componentName: "lblOrderDate_data"
        },
        componentType: 'label',
    },
    pnlMainBodyItemsCO1: {
        style: {
            "width": "100%",
            "backgroundColor": "rgb(135, 135, 135)",
            "borderBottom": "",
            "padding": "",
            "marginBottom": "5px",
        },
        property:{
            componentName: "pnlMainBodyItemsC01",
        }
    },
    lblItems: {
        style: {
            "display": "inline-block",
            "width": "calc(100% - 48px)",
            "color": "rgb(255, 215, 0)",
            "fontSize": "18px",
            "fontWeight": "bold",
            "textShadow": "",
            "border": "",
            "padding": "10px",
            "paddingTop": "5px",
            "paddingBottom": "5px",
        },
        property: {
            componentValue: 'DETAILS',
            componentVisible: true,
            componentName: "lblItems"
        },
        componentType: 'label',
    },
    pnlMainBodyItemsCO1: {
        style: {
            "width": "100%",
            "backgroundColor": "rgb(135, 135, 135)",
            "borderBottom": "",
            "padding": "",
            "marginBottom": "5px",
        },
        property:{
            componentName: "pnlMainBodyItemsC01",
        }
    },
    btnInsert: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Insert',
        },
    },
    btnLoadData: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'LoadData',
        },
    },
    dlItem: {
        property: {
            autoLoad: true,
            componentType: 'dataList',
            componentName: 'dlItem',
            componentValue: 'l_OrderLines',
            componentData: [
                {
                    pnlProduct: {
                        style: {
                            "position": "relative",
                            "display": "table",
                            "width": "100%",
                            "boxShadow": "-1px 1px 1px #545454",
                            "textAlign": "left",
                            "backgroundColor": "rgb(255, 255, 255)",
                            "border": "1px solid #3C3C3B",
                            "borderBottom": "",
                            "borderRight": "",
                            "borderRadius": "5px",
                            "padding": "5px",
                        },
                        property:{
                            componentName: "pnlProduct",
                        }
                    },
                    pnlProductBodyLeft: {
                        style: {
                            "display": "table-cell",
                            "verticalAlign": "top",
                            "width": "110px",
                            "boxShadow": "",
                            "textAlign": "center",
                        },
                        property:{
                            componentName: "pnlProductBodyLeft",
                        }
                    },
                    imgItem: {
                        style: {
                            "width": "100px",
                            "height": "90px",
                        },
                        property: {
                            //componentValue: require('../../framework/core/asset/default.png'),
                            componentValue: require('../../framework/core/asset/default.png') ,
                            componentVisible: true,
                            componentName: "imgItem",
                        },
                        componentType: 'image',
                    },
                    pnlProductBodyMiddle: {
                        style: {
                            "display": "table-cell",
                            "verticalAlign": "top",
                            "width": "calc(100% - 110px - 50px)",
                            "boxShadow": "",
                            "background": "",
                            "padding": "",
                            "paddingLeft": "10px",
                        },
                        property:{
                            componentName: "pnlProductBodyMiddle",
                        }
                    },
                    pnlProductBodyMiddle1: {
                        style: {
                            "verticalAlign": "",
                            "width": "100%",
                            "padding": "",
                        },
                        property:{
                            componentName: "pnlProductBodyMiddle1",
                        }
                    },
                    lblItemCode_data: {
                        style: {
                            "color": "rgb(60, 60, 59)",
                            "fontSize": "24px",
                            "fontWeight": "bold",
                        },
                        property: {
                            componentType: 'label',
                            componentValue: 'A01-BADAM',
                            field: 'No',
                            componentName: 'lblItemCode_data',
                        }
                    },
                    lblItemName_data: {
                        style: {
                            "display": "block",
                            "color": "rgb(111, 111, 110)",
                            "fontSize": "20px",
                            "fontWeight": "normal",
                            "textAlign": "left",
                        },
                        property: {
                            componentType: 'label',
                            componentValue: 'Badam Nuts',
                            field: 'Description',
                            componentName: 'lblItemName_data',
                        }
                    },
                    lblItemsQtyUOM: {
                        style: {
                            "display": "block",
                            "color": "rgb(111, 111, 110)",
                            "fontSize": "16px",
                            "fontWeight": "normal",
                            "textAlign": "left",
                        },
                        property: {
                            componentType: 'label',
                            componentValue: '',
                            componentName: 'lblItemsQtyUOM',
                        }
                    },
                    lblPrice_data: {
                        style: {
                            "display": "block",
                            "color": "rgb(0, 153, 0)",
                            "fontSize": "18px",
                            "fontWeight": "bold",
                            "textAlign": "left",
                            "paddingTop": "2px",
                            "marginTop": "10px",
                        },
                        property: {
                            componentType: 'label',
                            componentValue: '',
                            componentName: 'lblPrice_data',
                        }
                    },
                    lblDiscCO: {
                        style: {
                            "display": "none",
                            "color": "rgb(60, 60, 59)",
                            "fontSize": "16px",
                            "fontWeight": "normal",
                        },
                        property: {
                            componentType: 'label',
                            componentValue: 'Disc',
                            field: 'disc',
                            componentName: 'lblDiscCO',
                        }
                    },
                },
            ]
        },
        template: {
            componentDataItemState: {
                pnlProduct: {
                    style: {
                        "position": "relative",
                        "display": "table",
                        "width": "100%",
                        "boxShadow": "-1px 1px 1px #545454",
                        "textAlign": "left",
                        "backgroundColor": "rgb(255, 255, 255)",
                        "border": "1px solid #3C3C3B",
                        "borderBottom": "",
                        "borderRight": "",
                        "borderRadius": "5px",
                        "padding": "5px",
                    },
                    property:{
                        componentName: "pnlProduct",
                    }
                },
                pnlProductBodyLeft: {
                    style: {
                        "display": "table-cell",
                        "verticalAlign": "top",
                        "width": "110px",
                        "boxShadow": "",
                        "textAlign": "center",
                    },
                    property:{
                        componentName: "pnlProductBodyLeft",
                    }
                },
                imgItem: {
                    style: {
                        "width": "100px",
                        "height": "90px",
                    },
                    property: {
                        //componentValue: require('../../framework/core/asset/default.png'),
                        componentValue: require('../../framework/core/asset/default.png') ,
                        componentVisible: true,
                        componentName: "imgItem",
                    },
                    componentType: 'image',
                },
                pnlProductBodyMiddle: {
                    style: {
                        "display": "table-cell",
                        "verticalAlign": "top",
                        "width": "calc(100% - 110px - 50px)",
                        "boxShadow": "",
                        "background": "",
                        "padding": "",
                        "paddingLeft": "10px",
                    },
                    property:{
                        componentName: "pnlProductBodyMiddle",
                    }
                },
                pnlProductBodyMiddle1: {
                    style: {
                        "verticalAlign": "",
                        "width": "100%",
                        "padding": "",
                    },
                    property:{
                        componentName: "pnlProductBodyMiddle1",
                    }
                },
                lblItemCode_data: {
                    style: {
                        "color": "rgb(60, 60, 59)",
                        "fontSize": "24px",
                        "fontWeight": "bold",
                    },
                    property: {
                        componentType: 'label',
                        componentValue: '',
                        field: 'No',
                        componentName: 'lblItemCode_data',
                    }
                },
                lblItemName_data: {
                    style: {
                        "display": "block",
                        "color": "rgb(111, 111, 110)",
                        "fontSize": "20px",
                        "fontWeight": "normal",
                        "textAlign": "left",
                    },
                    property: {
                        componentType: 'label',
                        componentValue: '',
                        field: 'Description',
                        componentName: 'lblItemName_data',
                    }
                },
                lblItemsQtyUOM: {
                    style: {
                        "display": "block",
                        "color": "rgb(111, 111, 110)",
                        "fontSize": "16px",
                        "fontWeight": "normal",
                        "textAlign": "left",
                    },
                    property: {
                        componentType: 'label',
                        componentValue: '',
                        componentName: 'lblItemsQtyUOM',
                    }
                },
                lblPrice_data: {
                    style: {
                        "display": "block",
                        "color": "rgb(0, 153, 0)",
                        "fontSize": "18px",
                        "fontWeight": "bold",
                        "textAlign": "left",
                        "paddingTop": "2px",
                        "marginTop": "10px",
                    },
                    property: {
                        componentType: 'label',
                        componentValue: '',
                        componentName: 'lblPrice_data',
                    }
                },
                lblDiscCO: {
                    style: {
                        "display": "none",
                        "color": "rgb(60, 60, 59)",
                        "fontSize": "16px",
                        "fontWeight": "normal",
                    },
                    property: {
                        componentType: 'label',
                        componentValue: 'Disc',
                        field: 'disc',
                        componentName: 'lblDiscCO',
                    }
                },
            },
        }
    },
    pnlMainBodyDetails: {
        style: {
            "width": "100%",
            "height": "105px",
            "backgroundColor": "rgb(84, 84, 84)",
            "borderBottom": "",
            "padding": "10px",
        },
        property:{
            componentName: "pnlMainBodyDetails",
        }
    },
    pnlSubTotal: {
        style: {
            "overflow": "auto",
            "width": "100%",
            "borderTop": "",
            "borderBottom": "",
            "padding": "",
        },
        property:{
            componentName: "pnlSubTotal",
        }
    },
    lblSubTotal: {
        style: {
            "overflow": "auto",
            "width": "100%",
            "borderTop": "",
            "borderBottom": "",
            "padding": "",
        },
        property: {
            componentType: 'label',
            componentValue: 'Subtotal',
            componentName: 'lblSubTotal',
        }
    },
    lblSubTotal_data: {
        style: {
            "float": "right",
            "color": "rgb(255, 255, 255)",
            "fontSize": "16px",
        },
        property: {
            componentType: 'label',
            componentValue: '$ 0.00',
            componentName: 'lblSubTotal_data',
        }
    },
    _hiddenComponentKey:[
    ],
    _dataListContent: {
        DataList467: [
            {
                Label439: {
                    style: {
                    },
                    property: {
                        componentValue: 'Hello',
                    },
                    componentType: 'label',
                },
            },
            {
                Label439: {
                    style: {
                    },
                    property: {
                        componentValue: 'World',
                    },
                    componentType: 'label',
                },
            },
        ],
    },
}

// Notes
// Figure out the display block + float left combination or just use flexDirection row in editor
// Since react native only supports display flex other than none, so for display block just change to flexDirection column, inline change to flexDirection row
