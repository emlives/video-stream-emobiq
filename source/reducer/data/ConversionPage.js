export const StateName = 'ConversionPage';
export const State = {
    lblResult: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'label',
            componentValue: 'Result',
            componentVisible: true,
        }
    },
    btnJoin: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Join',
            componentVisible: true,
        }
    },
    btnSplit: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Split - Console',
            componentVisible: true,
        }
    }
}