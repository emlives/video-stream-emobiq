export const StateName = 'NavisionPage';
export const State = {
    lblResult: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'label',
            componentValue: 'Result',
            componentVisible: true,
        }
    },
    btnCreateOrder: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Create Order',
            componentVisible: true,
        }
    },
    btnGetItemPrice: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Get Item Price',
            componentVisible: true,

        }
    },
    btnGetCompany: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Get Company',
            componentVisible: true,
        }
    },
    btnLoadData: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Load Data - Item List',
        },
    },
    btnLoadNext: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Load Next - Item List',
        },
    },
    dataList1: {
        property: {
            componentUUID: '123123',
            autoLoad: true,
            componentType: 'dataList',
            componentName: 'dataList1',
            componentValue: 'n_item_list',
            componentData: []
        },
        template: {
            componentDataItemState: {
                LabelKey: {
                    style: {
                        width: '100%',
                        color: 'rgb(111, 38, 38)',
                        textAlign: 'center',
                    },
                    property: {
                        componentType: 'label',
                        componentValue: '',
                        field: 'No',
                    },
                },
                LabelValue: {
                    style: {
                        width: '100%',
                        color: 'rgb(232, 38, 38)',
                        textAlign: 'center',
                    },
                    property: {
                        componentType: 'label',
                        componentValue: '',
                        field: 'Description',
                    },
                }
            },
        }
    }
}