export const StateName = 'pgLogin';
export const State = {
	"Label123": {
		"parent": "pnlMainFooterMsg",
		"property": {
			"componentUUID": "647hTRbVt5wgXsVHN2F-W",
			"componentType": "label",
			"componentName": "Label123",
			"componentValue": "Development | Version: ",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontSize": "14px",
			"textAlign": "right"
		}
	},
	"pnlModalBodyButtonPositive": {
		"property": {
			"componentUUID": "O2Zs0m5TJE3Q0rjG_W8YE",
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"componentVisible": true
		},
		"style": {
			"paddingRight": "0px",
			"left": "0px",
			"width": "50%",
			"paddingTop": "10px",
			"paddingBottom": "10px",
			"paddingLeft": "0px"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlWrapper": {
		"property": {
			"componentUUID": "lnSkTdeNFkJV7UShWnKVp",
			"componentType": "panel",
			"componentName": "pnlWrapper",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"overflow": "visible",
			"left": "0px",
			"width": "100%",
			"backgroundColor": "rgb(245, 245, 245)"
		},
		"parent": "pgLogin"
	},
	"pnlUsernameCaption": {
		"property": {
			"componentUUID": "plF2dV7cW8JydLyv8RezS",
			"componentType": "panel",
			"componentName": "pnlUsernameCaption",
			"componentVisible": true
		},
		"style": {
			"paddingBottom": "2.5px",
			"left": "0px",
			"justifyContent": "flex-start",
			"alignItems": "center",
			"width": "100%",
			"flexDirection": "row"
		},
		"parent": "pnlUsername"
	},
	"Label696418": {
		"property": {
			"componentUUID": "ulY8XGHREqE-wienS3gKU",
			"componentType": "label",
			"componentName": "Label696418",
			"componentValue": "Password",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"textAlign": "left",
			"marginLeft": "5px",
			"fontSize": "14px"
		},
		"parent": "pnlPasswordCaption"
	},
	"Label317": {
		"property": {
			"componentUUID": "-INH_yKBhw37j3C3NYNjp",
			"componentType": "label",
			"componentName": "Label317",
			"componentValue": "LOGIN",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)"
		},
		"parent": "pnlLoginButton"
	},
	"Image591": {
		"property": {
			"componentUUID": "hLmKX5A_GO33hyFpTwFr1",
			"componentType": "image",
			"componentName": "Image591",
			"componentValue": require("../../asset/icon/printer_3c3c3b.png"),
			"zoom": false,
			"componentVisible": true
		},
		"style": {
			"width": "30px",
			"height": "30px"
		},
		"parent": "pnlMainSettings"
	},
	"pnlMainLanguage": {
		"property": {
			"componentUUID": "ux9rOQ3VF2wiQRDWOuUpu",
			"componentType": "panel",
			"componentName": "pnlMainLanguage",
			"componentVisible": true
		},
		"style": {
			"marginTop": "1%",
			"flexDirection": "row",
			"left": "0px",
			"justifyContent": "center",
			"width": "100%"
		},
		"parent": "pnlMainBody"
	},
	"Label718": {
		"style": {
			"fontSize": "16px",
			"paddingRight": "5px"
		},
		"parent": "pnlMainLanguage",
		"property": {
			"componentUUID": "DchcGMBcUe5oMb53z7NTY",
			"componentType": "label",
			"componentName": "Label718",
			"componentValue": "English",
			"componentVisible": true
		}
	},
	"Label145": {
		"parent": "pnlMainFooterMsg",
		"property": {
			"componentUUID": "RctPGszKzqmOTjqesoKTc",
			"componentType": "label",
			"componentName": "Label145",
			"componentValue": "Data Version",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontSize": "14px",
			"textAlign": "left"
		}
	},
	"pnlUsername": {
		"style": {
			"paddingTop": "2%",
			"width": "100%",
			"paddingBottom": "2%",
			"paddingLeft": "10%",
			"paddingRight": "10%",
			"left": "0px"
		},
		"parent": "pnlMainBody",
		"property": {
			"componentUUID": "MjqDBEdMwQ4p0kJo_9gG_",
			"componentType": "panel",
			"componentName": "pnlUsername",
			"componentVisible": true
		}
	},
	"Label696": {
		"property": {
			"componentUUID": "sUejZIBmojPyXnaaZOqEG",
			"componentType": "label",
			"componentName": "Label696",
			"componentValue": "Username",
			"componentVisible": true
		},
		"style": {
			"fontSize": "14px",
			"fontWeight": "bold",
			"marginLeft": "5px",
			"textAlign": "left"
		},
		"parent": "pnlUsernameCaption"
	},
	"pnlUsername771": {
		"property": {
			"componentUUID": "tx8R4Vn3g46VA1Cw8v7-g",
			"componentType": "panel",
			"componentName": "pnlUsername771",
			"componentVisible": true
		},
		"style": {
			"left": "0px",
			"paddingTop": "2%",
			"paddingLeft": "10%",
			"paddingRight": "10%",
			"width": "100%",
			"paddingBottom": "2%"
		},
		"parent": "pnlMainBody"
	},
	"lblModalPositive": {
		"property": {
			"componentUUID": "8AHJr9t5b_khD7Ah6W91s",
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px",
			"textAlign": "center"
		},
		"parent": "pnlModalBodyButtonPositive"
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentUUID": "1EcYIDu1TxFiEoW_vfSe8",
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"componentVisible": true
		},
		"style": {
			"paddingBottom": "10px",
			"paddingLeft": "0px",
			"paddingRight": "0px",
			"width": "50%",
			"left": "0px",
			"paddingTop": "10px"
		},
		"parent": "pnlModalBodyButtons"
	},
	"imgModalLoading": {
		"property": {
			"componentUUID": "iTI6c3FwlMg2a3IO_GP-Z",
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"zoom": false,
			"componentVisible": true
		},
		"style": {
			"alignSelf": "center",
			"width": "50px",
			"height": "10px"
		},
		"parent": "pnlModalBodyLoading"
	},
	"pnlPrinter": {
		"property": {
			"componentUUID": "4mdSXCE9PH5cNT8cvjtLW",
			"componentType": "panel",
			"componentName": "pnlPrinter",
			"componentVisible": false
		},
		"style": {
			"position": "absolute",
			"width": "100%",
			"height": "100%",
			"justifyContent": "center",
			"top": "0px",
			"alignItems": "center",
			"backgroundColor": "rgba(0, 0, 0, 0.6)",
			"zIndex": "2"
		},
		"parent": "pnlWrapper"
	},
	"pnlPrinterMainBody": {
		"property": {
			"componentUUID": "pZoi_7vZ_WjtwPeWOxH2q",
			"componentType": "panel",
			"componentName": "pnlPrinterMainBody",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"position": "relative",
			"width": "90%",
			"backgroundColor": "rgb(255, 255, 255)"
		},
		"parent": "pnlPrinter"
	},
	"txtUsername": {
		"property": {
			"componentUUID": "4uUd1dL7ejodd43K738jq",
			"componentType": "edit",
			"componentName": "txtUsername",
			"componentValue": "MOBUSER10",
			"placeHolder": "Username",
			"componentVisible": true,
			"componentFunction": "gettxtUsernameValue"
		},
		"style": {
			"height": "50px",
			"width": "100%"
		},
		"parent": "pnlUsernameText"
	},
	"pnlMainFooterMsg": {
		"property": {
			"componentUUID": "wxMKr99fA1dx2x4kNoeNJ",
			"componentType": "panel",
			"componentName": "pnlMainFooterMsg",
			"componentVisible": true
		},
		"style": {
			"paddingBottom": "5px",
			"flexGrow": "1",
			"width": "100%",
			"alignItems": "flex-end",
			"paddingTop": "5px",
			"left": "0px",
			"flexDirection": "row",
			"justifyContent": "center"
		},
		"parent": "pnlMain"
	},
	"pnlModalBodyMessage": {
		"property": {
			"componentUUID": "73cKAnQ3I0VD9-REq2v87",
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"componentVisible": true
		},
		"style": {
			"left": "0px",
			"marginBottom": "10px",
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlMain": {
		"property": {
			"componentUUID": "2Hy_-Oz5wiLKKWy7ZB8Gf",
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"zIndex": "0",
			"flexShrink": "1",
			"left": "0px",
			"width": "100%",
			"height": "40px",
			"overflow": "scroll",
			"flexGrow": "1"
		},
		"parent": "pnlWrapper"
	},
	"pnlUsernameText": {
		"property": {
			"componentUUID": "TFfn37AuC3GEoPd-nASay",
			"componentType": "panel",
			"componentName": "pnlUsernameText",
			"componentVisible": true
		},
		"style": {
			"left": "0px",
			"width": "100%",
			"paddingTop": "2.5px"
		},
		"parent": "pnlUsername"
	},
	"Label81": {
		"property": {
			"componentUUID": "MiC8HXPuvlZ3w9jDVJyre",
			"componentType": "label",
			"componentName": "Label81",
			"componentValue": "中文",
			"componentVisible": true
		},
		"style": {
			"fontSize": "16px",
			"paddingLeft": "5px"
		},
		"parent": "pnlMainLanguage"
	},
	"pgLogin": {
		"property": {
			"componentUUID": "tj3UlQgzEcRvsMhjAXp5I",
			"componentType": "page",
			"componentName": "pgLogin",
			"title": "Login",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgb(245, 245, 245)"
		}
	},
	"Label371": {
		"property": {
			"componentUUID": "8ePMpCd0YfsAbmTr5ZI66",
			"componentType": "label",
			"componentName": "Label371",
			"componentValue": "Field Sales Apps",
			"componentVisible": true,
			"componentFunction": "getLabel371Value"
		},
		"style": {
			"fontWeight": "bold",
			"color": "rgb(60, 60, 59)",
			"fontSize": "20px"
		},
		"parent": "pnlHeader"
	},
	"pnlPasswordText": {
		"parent": "pnlUsername771",
		"property": {
			"componentUUID": "6aQFdA8aLLS6lQZCdcnEF",
			"componentType": "panel",
			"componentName": "pnlPasswordText",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"paddingTop": "2.5px",
			"left": "0px"
		}
	},
	"pnlModalBodyMain": {
		"property": {
			"componentUUID": "65gMQXme1O8HUJtCHaJrB",
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		},
		"style": {
			"left": "0px",
			"borderStyle": "solid",
			"marginLeft": "5%",
			"width": "90%",
			"borderWidth": "1px",
			"paddingLeft": "0px",
			"borderColor": "rgb(217, 217, 217)",
			"paddingBottom": "10px",
			"backgroundColor": "rgb(255, 255, 255)",
			"paddingTop": "10px",
			"paddingRight": "0px",
			"marginRight": "5%"
		},
		"parent": "pnlModal"
	},
	"lblModalMessage": {
		"property": {
			"componentUUID": "K5GghoLjmU2WYFU2VTiLe",
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(111, 111, 110)",
			"fontSize": "16px",
			"textAlign": "center"
		},
		"parent": "pnlModalBodyMessage"
	},
	"pnlSelectPrinter": {
		"property": {
			"componentUUID": "ApPv3rw8V6BSzOjDLEfYt",
			"componentType": "panel",
			"componentName": "pnlSelectPrinter",
			"componentVisible": true
		},
		"style": {
			"textAlign": "left",
			"paddingBottom": "10px",
			"paddingLeft": "10px",
			"paddingRight": "10px",
			"paddingTop": "10px"
		},
		"parent": "pnlPrinterMainBody"
	},
	"cmbPrinter": {
		"property": {
			"componentUUID": "IdCQ5URjA7CggrArzfeSx",
			"componentType": "combobox",
			"componentName": "cmbPrinter",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		},
		"parent": "pnlSelectPrinter"
	},
	"pnlPrinterFooterNavBtn": {
		"property": {
			"componentUUID": "IR-dbAyUBXDS5R-PUVP51",
			"componentType": "panel",
			"componentName": "pnlPrinterFooterNavBtn",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgb(240, 0, 0)",
			"paddingBottom": "1%",
			"height": "50px",
			"alignItems": "center",
			"textAlign": "center",
			"paddingTop": "1%",
			"justifyContent": "center"
		},
		"parent": "pnlPrinterMainBody"
	},
	"Image151": {
		"property": {
			"componentUUID": "V4f6xFYWCZaGGU2X5CFaI",
			"componentType": "image",
			"componentName": "Image151",
			"componentValue": require("../../asset/icon/_username_3c3c3b.png"),
			"zoom": false,
			"componentVisible": true
		},
		"style": {
			"width": "20px",
			"height": "20px"
		},
		"parent": "pnlUsernameCaption"
	},
	"pnlLoginButton": {
		"property": {
			"componentUUID": "idUF9a7QDztXJGwAiAurl",
			"componentType": "panel",
			"componentName": "pnlLoginButton",
			"componentVisible": true
		},
		"style": {
			"borderRadius": "10px",
			"left": "0px",
			"justifyContent": "center",
			"width": "100%",
			"backgroundColor": "rgb(255, 69, 0)",
			"alignItems": "center",
			"height": "50px"
		},
		"parent": "pnlLogin"
	},
	"pnlModal": {
		"property": {
			"componentUUID": "vORyPTV2Baeg5XxDLPeuI",
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"left": "0px",
			"justifyContent": "center",
			"width": "100%",
			"zIndex": "2",
			"backgroundColor": "rgba(0, 0, 0, 0.6)",
			"height": "100%",
			"position": "absolute",
			"overflow": "hidden",
			"alignItems": "center"
		},
		"parent": "pnlWrapper"
	},
	"lblPrinterFooterNavBtn": {
		"property": {
			"componentUUID": "xt0eN_awssVupWgHvrljI",
			"componentType": "label",
			"componentName": "lblPrinterFooterNavBtn",
			"componentValue": "OK",
			"componentVisible": true
		},
		"style": {
			"fontSize": "20px",
			"color": "rgb(255, 255, 255)",
			"fontWeight": "bold",
			"height": "35px"
		},
		"parent": "pnlPrinterFooterNavBtn"
	},
	"lblModalTitle": {
		"parent": "pnlModalBodyTitle",
		"property": {
			"componentUUID": "Y3bqBZ8RPJRF13DGZy8_D",
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"fontWeight": "bold",
			"textAlign": "center"
		}
	},
	"pnlModalBodyButtons": {
		"property": {
			"componentUUID": "cGc7rYOOQ8EUTfyOO02hb",
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"componentVisible": true
		},
		"style": {
			"flexDirection": "row",
			"justifyContent": "center",
			"width": "90%",
			"marginLeft": "5%",
			"marginRight": "5%",
			"left": "0px",
			"backgroundColor": "rgb(255, 255, 255)"
		},
		"parent": "pnlModal"
	},
	"pnlMainBody": {
		"property": {
			"componentUUID": "COfsI1UvtzG8O1hSd2iQb",
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		},
		"style": {
			"left": "0px",
			"width": "100%"
		},
		"parent": "pnlMain"
	},
	"Image151321": {
		"style": {
			"width": "20px",
			"height": "20px"
		},
		"parent": "pnlPasswordCaption",
		"property": {
			"componentUUID": "lWfwO-pAbuSjGp2eaojgX",
			"componentType": "image",
			"componentName": "Image151321",
			"componentValue": require("../../asset/icon/password_3c3c3b.png"),
			"zoom": false,
			"componentVisible": true
		}
	},
	"pnlModalBodyTitle": {
		"property": {
			"componentUUID": "0CDy_168R0QwLbz4qBYb1",
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"componentVisible": true
		},
		"style": {
			"left": "0px",
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"txtPassword": {
		"property": {
			"componentUUID": "lX2hiCz8JchqVz3hr89lW",
			"componentType": "edit",
			"componentName": "txtPassword",
			"placeHolder": "Password",
			"type": "password",
			"componentValue": "123456",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "50px"
		},
		"parent": "pnlPasswordText"
	},
	"pnlLogin": {
		"parent": "pnlMainBody",
		"property": {
			"componentUUID": "pkVCwGzWrHUdvpWgakXa8",
			"componentType": "panel",
			"componentName": "pnlLogin",
			"componentVisible": true
		},
		"style": {
			"paddingLeft": "10%",
			"left": "0px",
			"width": "100%",
			"paddingTop": "1%",
			"paddingBottom": "1%",
			"paddingRight": "10%",
			"marginTop": "50px"
		}
	},
	"pnlMainSettings": {
		"property": {
			"componentUUID": "3Ji-ZRvSOps73wsT2WkKJ",
			"componentType": "panel",
			"componentName": "pnlMainSettings",
			"componentVisible": true
		},
		"style": {
			"left": "0px",
			"flexDirection": "row",
			"alignItems": "center",
			"width": "100%",
			"paddingRight": "10%",
			"justifyContent": "flex-end"
		},
		"parent": "pnlMainBody"
	},
	"lblPrinter": {
		"style": {
			"color": "rgb(60, 60, 59)",
			"marginBottom": "0px",
			"marginRight": "10px",
			"fontSize": "16px",
			"marginTop": "10px"
		},
		"parent": "pnlMainSettings",
		"property": {
			"componentUUID": "Ewd-H4N8HkRSFOB4tu_LW",
			"componentType": "label",
			"componentName": "lblPrinter",
			"componentVisible": true,
			"componentValue": "Printer"
		}
	},
	"pnlModalBodyLoading": {
		"property": {
			"componentUUID": "1ujwHHnXoTxi513Ad_XYO",
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"componentVisible": true
		},
		"style": {
			"left": "0px",
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlHeader": {
		"style": {
			"left": "0px",
			"alignItems": "center",
			"paddingRight": "10px",
			"justifyContent": "center",
			"width": "100%",
			"paddingLeft": "10px",
			"paddingTop": "5px",
			"paddingBottom": "5px"
		},
		"parent": "pnlMainBody",
		"property": {
			"componentUUID": "-QGvP3dQe4NpoZYyaYA34",
			"componentType": "panel",
			"componentName": "pnlHeader",
			"componentVisible": true
		}
	},
	"Image63": {
		"property": {
			"componentUUID": "ZMLOVWrF-3A32QQc5-HhO",
			"componentType": "image",
			"componentName": "Image63",
			"componentValue": require("../../asset/images/OK365-Dark.png"),
			"zoom": false,
			"componentVisible": true
		},
		"style": {
			"width": "50%",
			"marginTop": "20px",
			"marginBottom": "20px"
		},
		"parent": "pnlHeader"
	},
	"pnlPasswordCaption": {
		"property": {
			"componentUUID": "y58Lem1f1iFvvqqlJ7rJv",
			"componentType": "panel",
			"componentName": "pnlPasswordCaption",
			"componentVisible": true
		},
		"style": {
			"left": "0px",
			"alignItems": "center",
			"paddingBottom": "2.5px",
			"flexDirection": "row",
			"justifyContent": "flex-start",
			"width": "100%"
		},
		"parent": "pnlUsername771"
	},
	"lblModalNegative": {
		"property": {
			"componentUUID": "sEscNILI-vAb1zTcLnQxo",
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px",
			"textAlign": "center"
		},
		"parent": "pnlModalBodyButtonNegative"
	},
	"lblSelectPrinter": {
		"property": {
			"componentUUID": "omgpm2piArKWET5wUc2Hc",
			"componentType": "label",
			"componentName": "lblSelectPrinter",
			"componentValue": "Select Printer",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"fontWeight": "bold",
			"textAlign": "center"
		},
		"parent": "pnlSelectPrinter"
	}
};
