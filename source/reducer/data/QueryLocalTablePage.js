export const StateName = 'QueryLocalTablePage';
export const State = {
    page: {
		style: {
			flex: 1,
            position: 'relative',
            backgroundColor: 'white'
		},
		property: {
			componentDisabled: false,
		}
	},
	lblTitle: {
		"property": {
			"componentType": "label",
			"componentName": "lblTitle",
            "componentValue": "Select / Delect / Update by Multi",
			"componentVisible": true
		},
		"style": {
            width: '84%',
            alignSelf: 'center',
			fontSize: "13px",
            margin: 8,
            marginTop: 20,
			color: 'grey',
			fontWeight: 'bold'
            // lineHeight: 20,
		},

	},
	btnSelectAll:{
        style: {
			width: '90%',
            borderRadius: 8,
            margin: 5,
			flex: 1,
            alignSelf: 'center',
			justifyContent: 'flex-end',
  			marginBottom: 36,
            backgroundColor: 'springgreen',
            color: 'black',
			fontWeight: 'bold'
        },
        property: {
			componentValue: 'Select All',
			componentVisible: true,
		}
    },
	btnDeleteByMulti:{
        style: {
			width: '90%',
            borderRadius: 8,
            margin: 5,
            alignSelf: 'center',
            backgroundColor: 'orangered',
            color: 'white',
			fontWeight: 'bold'
        },
        property: {
			componentValue: 'Delect By Multi',
			componentVisible: true,
		}
    },
	btnUpdateByMulti: {
		style: {
			width: '90%',
            borderRadius: 8,
            margin: 5,
            alignSelf: 'center',
            backgroundColor: 'orangered',
            color: 'white',
			fontWeight: 'bold'
        },
        property: {
			componentValue: 'Update By Multi',
			componentVisible: true,
		}
	},
	btnSelectByMulti:{
        style: {
			width: '90%',
            borderRadius: 8,
            margin: 5,
			marginTop: 10,
            alignSelf: 'center',
            backgroundColor: 'darkorange',
            color: 'white',
			fontWeight: 'bold'
        },
        property: {
			componentValue: 'Select By Multi',
			componentVisible: true,
		}
    },
}
