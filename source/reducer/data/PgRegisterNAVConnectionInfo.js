export const StateName = 'pgRegisterNAVConnectionInfo';
export const State = {
	"lblUsername": {
		"property": {
			"componentType": "label",
			"componentName": "lblUsername",
			"componentValue": "Service URL",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontWeight": "bold",
			"marginLeft": "5px",
			"fontSize": "14px"
		},
		"parent": "pnlUsernameCaption"
	},
	"txtPassword": {
		"property": {
			"componentType": "edit",
			"componentName": "txtPassword",
			// "componentValue": "HPkEyf5dVCbyoYX7v5I4jcERvBwqPf7G7fX+V0ZT8R4=",
			"type": "password",
			"componentVisible": true,
			"componentFunction": "getTxtPasswordValue"
		},
		"style": {
			"margin": "0px",
			"height": "50px",
			"backgroundColor": "rgba(255, 255, 255, 0)",
			"color": "rgb(0, 0, 0)",
			"fontSize": "14px",
			"width": "100%",
			"boxShadow": "none",
			"border": "1px solid #3C3C3B"
		},
		"parent": "pnlPasswordText"
	},
	"pnlModalBodyMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		},
		"style": {
			"width": "90%",
			"background": "#ffffff",
			"marginRight": "5%",
			"textAlign": "center",
			"borderBottom": "1px solid #D9D9D9",
			"padding": "10px 0px",
			"marginLeft": "5%"
		},
		"parent": "pnlModalCell"
	},
	"pnlModalBodyMessage": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "50%",
			"textAlign": "center",
			"display": "inline-block",
			"float": "none",
			"padding": "10px 0px"
		},
		"parent": "pnlModalBodyButtons"
	},
	"lblStyleCustom": {
		"property": {
			"componentType": "label",
			"componentName": "lblStyleCustom",
			"componentValue": "<style> ::placeholder { color: #ffffff; opacity: 1; } </style>",
			"componentVisible": true
		},
		"parent": "pnlCSS"
	},
	"pnlHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHeader",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"padding": "5px 10px"
		},
		"parent": "pnlMainBody"
	},
	"pnlModalBodyTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"marginBottom": "10px",
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	},
	"lblPassword": {
		"parent": "pnlPasswordCaption",
		"property": {
			"componentType": "label",
			"componentName": "lblPassword",
			"componentValue": "Password",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"marginLeft": "5px",
			"color": "rgb(60, 60, 59)",
			"fontSize": "14px",
			"backgroundColor": "rgba(0, 0, 0, 0)"
		}
	},
	"pnlPasswordText420574": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlPasswordText420574",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "2.5px"
		},
		"parent": "pnlMainPassword387"
	},
	"lblModalPositive": {
		"parent": "pnlModalBodyButtonPositive",
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		}
	},
	"lblTouchSensor270401": {
		"property": {
			"componentType": "label",
			"componentName": "lblTouchSensor270401",
			"componentValue": "N/A",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "14px",
			"marginLeft": "5px",
			"fontWeight": "bold"
		},
		"parent": "pnlMain4444792"
	},
	"pnlMainPassword387": {
		"style": {
			"width": "100%",
			"textAlign": "center",
			"padding": "10%",
			"paddingTop": "2%",
			"paddingBottom": "2%"
		},
		"parent": "pnlMainBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainPassword387",
			"componentVisible": true
		}
	},
	"pnlMainFooterMsg": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterMsg",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"backgroundColor": "rgba(0, 0, 0, 0)",
			"padding": "5px 10px"
		},
		"parent": "pnlMain"
	},
	"lblDataVersion": {
		"property": {
			"componentType": "label",
			"componentName": "lblDataVersion",
			"componentValue": "Data Version",
			"componentVisible": true
		},
		"style": {
			"marginLeft": "5px",
			"fontSize": "14px",
			"fontWeight": "normal",
			"color": "rgb(60, 60, 59)"
		},
		"parent": "pnlMainFooterMsg"
	},
	"pgRegisterNAVConnectionInfo": {
		"property": {
			"componentType": "page",
			"componentName": "pgRegisterNAVConnectionInfo",
			"title": "pgRegisterNAVConnectionInfo",
			"componentVisible": true
		}
	},
	"pnlModal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"overflow": "auto",
			"background": "rgba(0, 0, 0, 0.60)",
			"position": "fixed",
			"top": "0px",
			"zIndex": "1",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlWrapper"
	},
	"pnlModalBodyButtons": {
		"style": {
			"marginLeft": "5%",
			"marginRight": "5%",
			"width": "90%",
			"textAlign": "center",
			"background": "#ffffff"
		},
		"parent": "pnlModalCell",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"addClass": "hide",
			"componentVisible": true
		}
	},
	"lblSubTitle": {
		"parent": "pnlHeaderTitle",
		"property": {
			"componentType": "label",
			"componentName": "lblSubTitle",
			"componentValue": "CONNECTOR SETUP",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontSize": "20px",
			"fontWeight": "bold",
			"width": "100%"
		}
	},
	"lblUsername898": {
		"property": {
			"componentType": "label",
			"componentName": "lblUsername898",
			"componentValue": "Username",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"color": "rgb(60, 60, 59)",
			"fontSize": "14px",
			"marginLeft": "5px"
		},
		"parent": "pnlUsernameCaption442310"
	},
	"pnlLoginBtnTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlLoginBtnTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "50px"
		},
		"parent": "pnlMainLoginBtn"
	},
	"lblModalMessage": {
		"parent": "pnlModalBodyMessage",
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(111, 111, 110)",
			"fontSize": "16px"
		}
	},
	"lblStyle": {
		"property": {
			"componentType": "label",
			"componentName": "lblStyle",
			"componentValue": "N/A",
			"componentVisible": true
		},
		"parent": "pnlCSS"
	},
	"pnlWrapper": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlWrapper",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"backgroundColor": "rgb(245, 245, 245)"
		},
		"parent": "pgRegisterNAVConnectionInfo"
	},
	"pnlMain": {
		"parent": "pnlWrapper",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"zIndex": "0",
			"height": "100%",
			"backgroundColor": "rgba(0, 0, 0, 0)",
			"width": "100%",
			"background": "url() no-repeat fixed"
		}
	},
	"pnlHeaderLogo": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHeaderLogo",
			"componentVisible": true
		},
		"style": {
			"padding": "20px"
		},
		"parent": "pnlHeader"
	},
	"pnlLoginBtnTableCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlLoginBtnTableCell",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"backgroundColor": "rgb(255, 69, 0)",
			"border": "0px solid #000",
			"borderRadius": "10px",
			"display": "table-cell"
		},
		"parent": "pnlLoginBtnTable"
	},
	"pnlModalCell": {
		"style": {
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%",
			"display": "table-cell"
		},
		"parent": "pnlModalTable",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		}
	},
	"pnlUsernameCaption": {
		"style": {
			"textAlign": "left",
			"paddingBottom": "2.5px"
		},
		"parent": "pnlMainUsername",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUsernameCaption",
			"componentVisible": true
		}
	},
	"pnlUsernameText": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUsernameText",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "2.5px"
		},
		"parent": "pnlMainUsername"
	},
	"lblPassword529": {
		"style": {
			"marginLeft": "5px",
			"backgroundColor": "rgba(0, 0, 0, 0)",
			"color": "rgb(60, 60, 59)",
			"fontSize": "14px",
			"fontWeight": "bold"
		},
		"parent": "pnlPasswordCaption934195",
		"property": {
			"componentType": "label",
			"componentName": "lblPassword529",
			"componentValue": "Default Company",
			"componentVisible": true
		}
	},
	"pnlMainLoginBtn": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainLoginBtn",
			"componentVisible": true
		},
		"style": {
			"paddingBottom": "1%",
			"marginTop": "30px",
			"verticalAlign": "middle",
			"width": "100%",
			"padding": "20px",
			"paddingTop": "1%",
			"textAlign": "center"
		},
		"parent": "pnlMainBody"
	},
	"pnlModalBodyLoading": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	},
	"imgModalLoading": {
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		},
		"style": {
			"width": "50px",
			"height": "10px"
		},
		"parent": "pnlModalBodyLoading"
	},
	"lblVersion": {
		"property": {
			"componentType": "label",
			"componentName": "lblVersion",
			"componentValue": " Development | Version: ",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontWeight": "normal",
			"fontSize": "14px"
		},
		"parent": "pnlMainFooterMsg"
	},
	"pnlCSS": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCSS",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"display": "none",
			"top": "0px",
			"zIndex": "2",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlWrapper"
	},
	"pnlMainUsername462": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainUsername462",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"padding": "10%",
			"paddingTop": "2%",
			"paddingBottom": "2%"
		},
		"parent": "pnlMainBody"
	},
	"imgFingerprint609317": {
		"property": {
			"componentType": "image",
			"componentName": "imgFingerprint609317",
			"componentValue": require("../../asset/icon/icon-fingerprint-white.png"),
			"componentVisible": true
		},
		"style": {
			"height": "70px",
			"display": "inline-block",
			"width": "70px"
		},
		"parent": "pnlMain4444792"
	},
	"lblModalTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontSize": "18px",
			"fontWeight": "bold",
			"width": "100%"
		},
		"parent": "pnlModalBodyTitle"
	},
	"pnlModalBodyButtonPositive": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"float": "none",
			"width": "50%",
			"textAlign": "center",
			"padding": "10px 0px"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlPasswordCaption934195": {
		"style": {
			"textAlign": "left",
			"paddingBottom": "2.5px"
		},
		"parent": "pnlMainPassword387",
		"property": {
			"componentType": "panel",
			"componentName": "pnlPasswordCaption934195",
			"componentVisible": true
		}
	},
	"pnlMainUsername": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainUsername",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "2%",
			"paddingBottom": "2%",
			"width": "100%",
			"textAlign": "center",
			"padding": "10%"
		},
		"parent": "pnlMainBody"
	},
	"txtURL": {
		"parent": "pnlUsernameText",
		"property": {
			"componentType": "edit",
			"componentName": "txtURL",
			// "componentValue": "https://api.businesscentral.dynamics.com/v1.0/425096bb-f087-4c7d-ac68-7ac4ca13116d/Sandbox",
			"placeHolder": "Username",
			"componentVisible": true,
			"componentFunction": "getTxtURLValue"
		},
		"style": {
			"color": "rgb(0, 0, 0)",
			"fontSize": "14px",
			"width": "100%",
			"height": "50px",
			"backgroundColor": "rgba(255, 255, 255, 0)",
			"border": "1px solid #3C3C3B"
		}
	},
	"pnlMainPassword": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainPassword",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"padding": "10%",
			"paddingTop": "2%",
			"paddingBottom": "2%"
		},
		"parent": "pnlMainBody"
	},
	"pnlMainBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"minHeight": "calc(100% - 35px)",
			"color": "rgb(255, 255, 255)",
			"backgroundColor": "rgba(0, 0, 0, 0)"
		},
		"parent": "pnlMain"
	},
	"pnlHeaderTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHeaderTitle",
			"componentVisible": true
		},
		"parent": "pnlHeader"
	},
	"pnlUsernameText24294": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUsernameText24294",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "2.5px"
		},
		"parent": "pnlMainUsername462"
	},
	"txtUsername": {
		"property": {
			"componentType": "edit",
			"componentName": "txtUsername",
			"placeHolder": "Username",
			// "componentValue": "AJAY.BABU_MSC-CONSULTING.COM.SG",
			"componentVisible": true,
			"componentFunction": "getTxtUsernameValue"
		},
		"style": {
			"height": "50px",
			"backgroundColor": "rgba(255, 255, 255, 0)",
			"border": "1px solid #3C3C3B",
			"width": "100%",
			"color": "rgb(0, 0, 0)",
			"fontSize": "14px"
		},
		"parent": "pnlUsernameText24294"
	},
	"pnlPasswordCaption": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlPasswordCaption",
			"componentVisible": true
		},
		"style": {
			"textAlign": "left",
			"paddingBottom": "2.5px"
		},
		"parent": "pnlMainPassword"
	},
	"pnlMain4444792": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain4444792",
			"componentVisible": true
		},
		"style": {
			"display": "none",
			"width": "100%",
			"textAlign": "center",
			"padding": "5px 10px"
		},
		"parent": "pnlMainBody"
	},
	"txtCompany": {
		"property": {
			"componentType": "edit",
			"componentName": "txtCompany",
			"type": "text",
			// "componentValue": "Orangekloud",
			"componentVisible": true,
			"componentFunction": "getTxtCompanyValue"
		},
		"style": {
			"color": "rgb(0, 0, 0)",
			"fontSize": "14px",
			"backgroundColor": "rgba(255, 255, 255, 0)",
			"border": "1px solid #3C3C3B",
			"margin": "0px",
			"height": "50px",
			"boxShadow": "none",
			"width": "100%"
		},
		"parent": "pnlPasswordText420574"
	},
	"pnlModalTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModal"
	},
	"pnlUsernameCaption442310": {
		"parent": "pnlMainUsername462",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUsernameCaption442310",
			"componentVisible": true
		},
		"style": {
			"textAlign": "left",
			"paddingBottom": "2.5px"
		}
	},
	"pnlPasswordText": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlPasswordText",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "2.5px"
		},
		"parent": "pnlMainPassword"
	},
	"lblLogin": {
		"property": {
			"componentType": "label",
			"componentName": "lblLogin",
			"componentValue": "Save",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"verticalAlign": "middle",
			"fontWeight": "bold",
			"textAlign": "center",
			"display": "block"
		},
		"parent": "pnlLoginBtnTableCell"
	},
	"lblModalNegative": {
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonNegative",
		"property": {
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		}
	}
};
