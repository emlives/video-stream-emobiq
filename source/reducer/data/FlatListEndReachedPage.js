export const StateName = 'FlatListEndReachedPage';
export const State = {
	"DataList30": {
		"property": {
			"componentUUID": "PTaICnbjZ9-4gWIQJ0Jdj",
			"componentType": "datalist",
			"componentName": "DataList30",
			"componentValue": "l_abc",
			"componentData": [],
			"componentVisible": true
		},
		"style": {
			"overflow": "scroll",
			"flexGrow": "1",
			"flexShrink": "1",
			"height": "40px"
		},
		"parent": "pnlContent",
		"template": {
			"componentDataItemState": {
				"Panel676": {
					"property": {
						"componentUUID": "_r5iUXyryeWhq-J8wTsSJ",
						"componentType": "panel",
						"componentName": "Panel676",
						"componentVisible": true
					},
					"style": {
						"borderColor": "#AAAAAA",
						"paddingBottom": "5px",
						"paddingLeft": "5px",
						"paddingRight": "5px",
						"left": "0px",
						"justifyContent": "center",
						"width": "100%",
						"height": "100px",
						"borderWidth": "1px",
						"paddingTop": "5px",
						"flexDirection": "row",
						"borderStyle": "solid"
					}
				},
				"Panel847": {
					"style": {
						"left": "0px",
						"flexGrow": "1",
						"flexShrink": "1",
						"borderColor": "#AAAAAA",
						"backgroundColor": "rgb(255, 255, 255)",
						"borderStyle": "solid",
						"borderWidth": "1px"
					},
					"parent": "Panel676",
					"property": {
						"componentUUID": "oqGiGnUFLz7oeGjNmIeiU",
						"componentType": "panel",
						"componentName": "Panel847",
						"componentVisible": true
					}
				},
				"Label806": {
					"property": {
						"componentUUID": "xHi0SkDRpLNhes5pNP9Is",
						"componentType": "label",
						"componentName": "Label806",
						"field": "title",
						"componentValue": "Title",
						"componentVisible": true
					},
					"style": {
						"paddingBottom": "5px",
						"paddingLeft": "5px",
						"paddingRight": "5px",
						"paddingTop": "5px"
					},
					"parent": "Panel847"
				}
			}
		}
	},
	"FlatListEndReachedPage": {
		"property": {
			"componentUUID": "OnvBFFg2UtB5qQcyMfR7D",
			"componentType": "page",
			"componentName": "FlatListEndReachedPage",
			"title": "Test",
			"componentVisible": true
		}
	},
	"pnlContainer": {
		"property": {
			"componentUUID": "-E-7ictp76d6AcYVa6bc7",
			"componentType": "panel",
			"componentName": "pnlContainer",
			"componentVisible": true
		},
		"style": {
			"left": "0px",
			"width": "100%",
			"height": "100%",
			"backgroundColor": "rgb(198, 198, 253)"
		},
		"parent": "FlatListEndReachedPage"
	},
	"pnlHeader": {
		"style": {
			"left": "0px",
			"width": "100%",
			"height": "64px",
			"backgroundColor": "rgb(255, 227, 227)"
		},
		"parent": "pnlContainer",
		"property": {
			"componentUUID": "ol-NhzJAP3bq1Qs_D1fU_",
			"componentType": "panel",
			"componentName": "pnlHeader",
			"componentVisible": true
		}
	},
	"Panel309": {
		"parent": "pnlHeader",
		"property": {
			"componentUUID": "89xF_WDS1Vb7ua6CHjvXV",
			"componentType": "panel",
			"componentName": "Panel309",
			"componentVisible": true
		},
		"style": {
			"left": "0px",
			"width": "64px",
			"height": "64px",
			"backgroundColor": "rgb(255, 195, 195)"
		}
	},
	"pnlContent": {
		"property": {
			"componentUUID": "TGso4BrxNUkFLjdGOtuxC",
			"componentType": "panel",
			"componentName": "pnlContent",
			"componentVisible": true
		},
		"style": {
			"paddingLeft": "10px",
			"paddingRight": "10px",
			"left": "0px",
			"backgroundColor": "rgb(173, 255, 179)",
			"flexGrow": "1",
			"paddingBottom": "10px",
			"width": "100%",
			"borderStyle": "solid",
			"borderWidth": "1px",
			"flexShrink": "1",
			"paddingTop": "10px",
			"height": "40px",
			"borderColor": "#AAAAAA"
		},
		"parent": "pnlContainer"
	}
};
