export const StateName = 'ScaleImagePage';
export const State = {
    page: {
		style: {
			flex: 1,
            position: 'relative',
            backgroundColor: 'springgreen'
		},
		property: {
			componentDisabled: false,
		}
	},

    "pnlHolder": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		},
		"style": {
            "justifyContent": 'center',
            "alignItems": 'center',
			"width": "100%",
			"height": "50%",
            backgroundColor: 'yellow',
            borderWidth: 2,
            borderColor: 'blue'
		},
	},

    Image387: {
        style: {
            position: 'absolute',
            width: '70',
            height: '50%',
            backgroundColor: 'white',
            resizeMode: 'contain',
            borderWidth: 2,
            borderColor: 'red'
        },
        property: {
            componentValue: require("../../asset/images/OK365-Dark.png"),
            componentVisible: true,
        },
        componentType: 'image',
    },
    
  
}
