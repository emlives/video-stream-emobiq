export const StateName = 'pgOrderHistoryDetails';
export const State = {
	"lblMainTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle",
			"componentValue": "INVOICE HISTORY DETAILS",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500"
		},
		"parent": "pnlMainHeaderMiddleCell"
	},
	"pnlModal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"top": "0px",
			"zIndex": "3",
			"width": "100%",
			"height": "100%",
			"background": "rgba(0, 0, 0, 0.60)"
		},
		"parent": "pnlHolder"
	},
	"pnlModalBodyMessage": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"pgOrderHistoryDetails": {
		"property": {
			"componentType": "page",
			"componentName": "pgOrderHistoryDetails",
			"title": "OrderHistoryDetails",
			"componentVisible": true
		}
	},
	"pnlMainBodyItemsCO1": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyItemsCO1",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgb(135, 135, 135)",
			"marginBottom": "5px",
			"width": "100%"
		},
		"parent": "pnlMainBody"
	},
	"pnlDetailsDiscount": {
		"style": {
			"overflow": "auto",
			"width": "100%"
		},
		"parent": "pnlMainBodyDetails",
		"property": {
			"componentType": "panel",
			"componentName": "pnlDetailsDiscount",
			"componentVisible": true
		}
	},
	"lblGST_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblGST_data",
			"componentValue": "$ 0.00",
			"componentVisible": true
		},
		"style": {
			"float": "right",
			"color": "rgb(255, 255, 255)",
			"fontSize": "16px"
		},
		"parent": "pnlDetailsDiscount"
	},
	"pnlConfirmPaymentMethodDetailsBody264652": {
		"parent": "pnlConfirmPaymentMethodDetailsCell206351",
		"property": {
			"componentType": "panel",
			"componentName": "pnlConfirmPaymentMethodDetailsBody264652",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"width": "90%",
			"textAlign": "center",
			"background": "#ffffff",
			"margin": "5%"
		}
	},
	"pnlMainBodyItems": {
		"style": {
			"width": "100%"
		},
		"parent": "pnlMainBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyItems",
			"componentVisible": true
		}
	},
	"pnlMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"zIndex": "0",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"lblModalTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "14px",
			"fontWeight": "bold"
		},
		"parent": "pnlModalBodyTitle"
	},
	"pnlMainFooter": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooter",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "40px",
			"backgroundColor": "rgb(255, 69, 0)"
		},
		"parent": "pnlMain"
	},
	"pnlModalBodyLoading": {
		"parent": "pnlModalBodyMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		}
	},
	"Label894367403": {
		"property": {
			"componentType": "label",
			"componentName": "Label894367403",
			"componentValue": "OK",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "16px",
			"backgroundColor": "rgba(255, 255, 255, 0)"
		},
		"parent": "Panel37680600"
	},
	"pnlMainHeaderMiddle": {
		"parent": "pnlMainHeader",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddle",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "70%",
			"height": "100%"
		}
	},
	"pnlMainHeaderRightCell877": {
		"parent": "pnlMainHeaderRightTable2",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightCell877",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		}
	},
	"pnlMainBodyForm": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"backgroundColor": "rgb(84, 84, 84)"
		},
		"parent": "pnlMainBody"
	},
	"pnlMainBodyFormCustomerCode": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyFormCustomerCode",
			"componentVisible": true
		},
		"style": {
			"padding": "10px",
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyForm"
	},
	"pnlMainBodyDetails": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyDetails",
			"componentVisible": true
		},
		"style": {
			"padding": "10px",
			"width": "100%",
			"height": "105px",
			"backgroundColor": "rgb(84, 84, 84)"
		},
		"parent": "pnlMain"
	},
	"pnlMainFooterTbl": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterTbl",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainFooter"
	},
	"pnlMainHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		},
		"style": {
			"overflow": "auto",
			"height": "65px",
			"textAlign": "center",
			"paddingTop": "15px",
			"width": "100%",
			"backgroundColor": "rgb(255, 69, 0)"
		},
		"parent": "pnlMain"
	},
	"lblAddress_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblAddress_data",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"float": "initial",
			"color": "rgb(255, 255, 255)",
			"fontWeight": "normal",
			"fontSize": "16px",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyForm3LabelCustomerCode"
	},
	"lblOrderDate_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblOrderDate_data",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontWeight": "normal",
			"textAlign": "center",
			"display": "block",
			"fontSize": "16px"
		},
		"parent": "pnlMainBodyForm3LabelCustomerCode"
	},
	"lblSubTotal": {
		"property": {
			"componentType": "label",
			"componentName": "lblSubTotal",
			"componentValue": "Subtotal",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "16px"
		},
		"parent": "pnlSubtotal"
	},
	"lblTotalAmount_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblTotalAmount_data",
			"componentValue": "$ 0.00",
			"componentVisible": true
		},
		"style": {
			"float": "right",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "bolder"
		},
		"parent": "pnlMainBodyDetails1"
	},
	"pnlMainHeaderLeftTable": {
		"parent": "pnlMainHeaderLeft",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTable",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"display": "table",
			"width": "100%"
		}
	},
	"lblTotalAmountExclGST": {
		"property": {
			"componentType": "label",
			"componentName": "lblTotalAmountExclGST",
			"componentValue": "Total Amount",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "bolder",
			"float": "left"
		},
		"parent": "pnlMainBodyDetails1"
	},
	"lblModalNegative": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)"
		},
		"parent": "pnlModalBodyButtonNegative"
	},
	"Panel103885987": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel103885987",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100px",
			"padding": "10px",
			"display": "table"
		},
		"parent": "pnlConfirmPaymentMethodDetailsBody264652"
	},
	"pnlMainBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"overflow": "auto",
			"height": "calc(100% - 50px - 40px - 105px - 15px)"
		},
		"parent": "pnlMain"
	},
	"pnlMainBodyForm3LabelCustomerCode": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm3LabelCustomerCode",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyFormCustomerCode"
	},
	"pnlModalBodyButtons": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"background": "#ffffff",
			"width": "90%",
			"marginLeft": "5%",
			"marginRight": "5%"
		},
		"parent": "pnlModalCell"
	},
	"pnlModalBodyButtonPositive": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"float": "none",
			"textAlign": "center",
			"padding": "10px 0px",
			"width": "50%"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlCSS": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCSS",
			"componentVisible": false
		},
		"style": {
			"display": "none",
			"top": "0px",
			"height": "100%",
			"position": "fixed",
			"zIndex": "4",
			"width": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlMainFooterTblCell1": {
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"height": "100%",
			"textAlign": "center",
			"backgroundColor": "rgba(0, 0, 0, 0)"
		},
		"parent": "pnlMainFooterTbl",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterTblCell1",
			"componentVisible": true
		}
	},
	"pnlModalTable": {
		"parent": "pnlModal",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		}
	},
	"pnlModalCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModalTable"
	},
	"pnlPrinter": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlPrinter",
			"componentVisible": false
		},
		"style": {
			"position": "absolute",
			"zIndex": "2",
			"width": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"top": "0px",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlSubtotal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSubtotal",
			"componentVisible": true
		},
		"style": {
			"overflow": "auto",
			"width": "100%"
		},
		"parent": "pnlMainBodyDetails"
	},
	"lblGSTValue": {
		"property": {
			"componentType": "label",
			"componentName": "lblGSTValue",
			"componentVisible": true
		},
		"style": {
			"fontSize": "16px",
			"color": "rgb(255, 255, 255)"
		},
		"parent": "pnlDetailsDiscount"
	},
	"pnlMainBodyDetails1": {
		"parent": "pnlMainBodyDetails",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyDetails1",
			"componentVisible": true
		},
		"style": {
			"borderTop": "1px solid #FFF",
			"paddingTop": "5px",
			"width": "100%",
			"overflow": "auto"
		}
	},
	"lblModalMessage": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(111, 111, 110)",
			"fontSize": "14px"
		},
		"parent": "pnlModalBodyMessage"
	},
	"pnlMainHeaderLeft": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeft",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"float": "left",
			"textAlign": "left",
			"paddingLeft": "10px",
			"width": "15%"
		},
		"parent": "pnlMainHeader"
	},
	"lblItems": {
		"property": {
			"componentType": "label",
			"componentName": "lblItems",
			"componentValue": "DETAILS",
			"componentVisible": true
		},
		"style": {
			"padding": "10px",
			"display": "inline-block",
			"fontWeight": "bold",
			"width": "calc(100% - 48px)",
			"paddingTop": "5px",
			"color": "rgb(255, 215, 0)",
			"fontSize": "18px",
			"paddingBottom": "5px"
		},
		"parent": "pnlMainBodyItemsCO1"
	},
	"pnlMainBodypnlConfirmPaymentMethodDetailsSign105386": {
		"parent": "Panel103885987",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodypnlConfirmPaymentMethodDetailsSign105386",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"textAlign": "left"
		}
	},
	"pnlMainHeaderRightTable2": {
		"parent": "pnlMainHeaderRight233",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTable2",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		}
	},
	"pnlMainBodyItemsCO2": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyItemsCO2",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		},
		"parent": "pnlMainBodyItems"
	},
	"pnlConfirmPaymentMethodDetailsTable695203598148": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlConfirmPaymentMethodDetailsTable695203598148",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlPrinter"
	},
	"pnlMainHeaderRight233": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRight233",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"height": "100%",
			"paddingRight": "10px",
			"width": "15%",
			"textAlign": "right"
		},
		"parent": "pnlMainHeader"
	},
	"dlltem": {
		"property": {
			"componentType": "datalist",
			"componentName": "dlltem",
			"autoLoad": false,
			"componentValue": "l_OrderLines",
			"componentData": [],
			"componentVisible": true
		},
		"parent": "pnlMainBodyItemsCO2",
		"template": {
			"componentDataItemState": {
				"pnlProductBodyMiddle1": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle1",
						"componentVisible": true
					},
					"style": {
						"width": "100%"
					},
					"parent": "pnlProductBodyMiddle"
				},
				"lblItemCode_data": {
					"property": {
						"componentType": "label",
						"componentName": "lblItemCode_data",
						"field": "No",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(60, 60, 59)",
						"fontSize": "24px",
						"fontWeight": "bold"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"lblItemName_data": {
					"property": {
						"componentType": "label",
						"componentName": "lblItemName_data",
						"field": "Description",
						"componentVisible": true
					},
					"style": {
						"textAlign": "left",
						"color": "rgb(111, 111, 110)",
						"fontWeight": "normal",
						"display": "block",
						"fontSize": "20px"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"lblPrice_data": {
					"property": {
						"componentType": "label",
						"componentName": "lblPrice_data",
						"componentVisible": true
					},
					"style": {
						"fontSize": "18px",
						"textAlign": "left",
						"paddingTop": "2px",
						"color": "rgb(0, 153, 0)",
						"fontWeight": "bold",
						"display": "block",
						"marginTop": "10px"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"lblDiscCO": {
					"property": {
						"componentType": "label",
						"componentName": "lblDiscCO",
						"componentValue": "Disc",
						"field": "disc",
						"componentVisible": true
					},
					"style": {
						"fontSize": "16px",
						"fontWeight": "normal",
						"display": "none",
						"color": "rgb(60, 60, 59)"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"pnlProduct": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProduct",
						"componentVisible": true
					},
					"style": {
						"padding": "5px",
						"display": "table",
						"backgroundColor": "rgb(255, 255, 255)",
						"border": "1px solid #3C3C3B",
						"width": "100%",
						"boxShadow": "-1px 1px 1px #545454",
						"borderRadius": "5px",
						"position": "relative",
						"textAlign": "left"
					}
				},
				"pnlProductBodyLeft": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyLeft",
						"addClass": "hide",
						"componentVisible": true
					},
					"style": {
						"width": "110px",
						"verticalAlign": "top",
						"display": "table-cell",
						"textAlign": "center"
					},
					"parent": "pnlProduct"
				},
				"imgItem": {
					"property": {
						"componentType": "image",
						"componentName": "imgItem",
						"componentValue": require("../../asset/images/default.png"),
						"componentVisible": true
					},
					"style": {
						"width": "100px",
						"height": "90px"
					},
					"parent": "pnlProductBodyLeft"
				},
				"pnlProductBodyMiddle": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle",
						"componentVisible": true
					},
					"style": {
						"display": "table-cell",
						"verticalAlign": "top",
						"width": "calc(100% - 110px - 50px)",
						"paddingLeft": "10px"
					},
					"parent": "pnlProduct"
				},
				"lblItemsQtyUOM": {
					"style": {
						"display": "block",
						"fontSize": "16px",
						"textAlign": "left",
						"fontWeight": "normal",
						"color": "rgb(111, 111, 110)"
					},
					"parent": "pnlProductBodyMiddle1",
					"property": {
						"componentType": "label",
						"componentName": "lblItemsQtyUOM",
						"componentVisible": true
					}
				}
			}
		}
	},
	"pnlModalBodyTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"marginBottom": "10px",
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"float": "none",
			"textAlign": "center",
			"display": "inline-block",
			"width": "50%",
			"padding": "10px 0px"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlConfirmPaymentMethodDetailsCell206351": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlConfirmPaymentMethodDetailsCell206351",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlConfirmPaymentMethodDetailsTable695203598148"
	},
	"Panel37680600": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel37680600",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "50%",
			"textAlign": "center"
		},
		"parent": "pnlUpdateCartModalBody589675"
	},
	"imgBack": {
		"property": {
			"componentType": "image",
			"componentName": "imgBack",
			"componentValue": require("../../asset/icon/icon-back.png"),
			"componentVisible": true
		},
		"style": {
			"width": "35px",
			"height": "35px"
		},
		"parent": "pnlMainHeaderLeftCell"
	},
	"lblOrderNo_data": {
		"style": {
			"fontSize": "20px",
			"fontWeight": "bold",
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyForm3LabelCustomerCode",
		"property": {
			"componentType": "label",
			"componentName": "lblOrderNo_data",
			"componentVisible": true
		}
	},
	"lblCustomerCodeName_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblCustomerCodeName_data",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 215, 0)",
			"fontSize": "18px",
			"fontWeight": "bold",
			"display": "block",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyForm3LabelCustomerCode"
	},
	"imgModalLoading": {
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		},
		"style": {
			"height": "10px",
			"width": "50px"
		},
		"parent": "pnlModalBodyLoading"
	},
	"lblSelectPrinter": {
		"property": {
			"componentType": "label",
			"componentName": "lblSelectPrinter",
			"componentValue": "Select Printer",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"color": "rgb(60, 60, 59)",
			"fontSize": "18px"
		},
		"parent": "pnlMainBodypnlConfirmPaymentMethodDetailsSign105386"
	},
	"cmbPrinter": {
		"property": {
			"componentType": "combobox",
			"componentName": "cmbPrinter",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"border": "1px solid #3C3C3B"
		},
		"parent": "pnlMainBodypnlConfirmPaymentMethodDetailsSign105386"
	},
	"pnlUpdateCartModalBody589675": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBody589675",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "40px",
			"textAlign": "center",
			"backgroundColor": "rgb(255, 69, 0)",
			"paddingTop": "1%",
			"paddingBottom": "1%"
		},
		"parent": "pnlConfirmPaymentMethodDetailsBody264652"
	},
	"pnlMainHeaderLeftCell": {
		"parent": "pnlMainHeaderLeftTable",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftCell",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table-cell",
			"verticalAlign": "middle"
		}
	},
	"pnlMainHeaderMiddleTable": {
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table"
		},
		"parent": "pnlMainHeaderMiddle",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTable",
			"componentVisible": true
		}
	},
	"imgBack555407": {
		"property": {
			"componentType": "image",
			"componentName": "imgBack555407",
			"componentValue": require("../../asset/Printer_white.png"),
			"componentVisible": true
		},
		"style": {
			"height": "35px",
			"width": "35px"
		},
		"parent": "pnlMainHeaderRightCell877"
	},
	"lblModalPositive": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)"
		},
		"parent": "pnlModalBodyButtonPositive"
	},
	"lblStyle": {
		"property": {
			"componentType": "label",
			"componentName": "lblStyle",
			"componentValue": "N/A",
			"componentVisible": true
		},
		"parent": "pnlCSS"
	},
	"pnlHolder": {
		"style": {
			"overflow": "auto",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pgOrderHistoryDetails",
		"property": {
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		}
	},
	"pnlMainHeaderMiddleCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleCell",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%",
			"display": "table-cell"
		},
		"parent": "pnlMainHeaderMiddleTable"
	},
	"lblSubtotal_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblSubtotal_data",
			"componentValue": "$ 0.00",
			"componentVisible": true
		},
		"style": {
			"float": "right",
			"color": "rgb(255, 255, 255)",
			"fontSize": "16px"
		},
		"parent": "pnlSubtotal"
	},
	"lblBtnOrder": {
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "16px",
			"display": "block"
		},
		"parent": "pnlMainFooterTblCell1",
		"property": {
			"componentType": "label",
			"componentName": "lblBtnOrder",
			"componentValue": "RE-PRINT",
			"componentVisible": true
		}
	},
	"pnlModalBodyMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		},
		"style": {
			"borderBottom": "1px solid #D9D9D9",
			"padding": "10px 0px",
			"marginLeft": "5%",
			"marginRight": "5%",
			"textAlign": "center",
			"background": "#ffffff",
			"width": "90%"
		},
		"parent": "pnlModalCell"
	}
};
