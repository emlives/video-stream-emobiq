export const StateName = 'pgMainMenu';
export const State = {
	"pnlMainBodyCustomer": {
		"property": {
			"componentUUID": "n7fUCUBfhHMHVyAkRA0r0",
			"componentType": "panel",
			"componentName": "pnlMainBodyCustomer",
			"componentVisible": true
		},
		"style": {
			"justifyContent": "center",
			"alignItems": "center",
			"width": "100%",
			"backgroundColor": "rgb(71, 65, 65)",
			"paddingBottom": "5px",
			"left": "0px",
			"paddingTop": "5px",
			"marginBottom": "5px"
		},
		"parent": "pnlMainBody"
	},
	"Label856312": {
		"property": {
			"componentUUID": "qjAQc8BKhvFUQGqFrBL92",
			"componentType": "label",
			"componentName": "Label856312",
			"componentValue": "Sales Return",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"textAlign": "center",
			"color": "rgb(255, 54, 0)"
		},
		"parent": "pnlMainBody1Right3"
	},
	"Label8698259": {
		"property": {
			"componentUUID": "vCx39RCaD83GrE9xhRQyH",
			"componentType": "label",
			"componentName": "Label8698259",
			"componentValue": "Cash Collection",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"textAlign": "center"
		},
		"parent": "pnlMainBody1Left817519"
	},
	"pnlMainBody5": {
		"style": {
			"width": "100%",
			"paddingTop": "5px",
			"left": "0px",
			"paddingBottom": "5px",
			"paddingLeft": "5px",
			"paddingRight": "5px",
			"flexDirection": "row"
		},
		"parent": "pnlMainBody",
		"property": {
			"componentUUID": "T-dZbZhXbHNhpcZLjYpfC",
			"componentType": "panel",
			"componentName": "pnlMainBody5",
			"componentVisible": true
		}
	},
	"Image834936852715440": {
		"property": {
			"componentUUID": "sSng1CCTUdCkPQFkv07wQ",
			"componentType": "image",
			"componentName": "Image834936852715440",
			"zoom": false,
			"componentValue": require("../../asset/icon/_dailySalesReport.png"),
			"componentVisible": true
		},
		"style": {
			"width": "70px",
			"marginBottom": "10px"
		},
		"parent": "pnlMainBody1Left817519933542"
	},
	"pnlMainBody1Right3553423898": {
		"property": {
			"componentUUID": "68Y4-NvDwp1CH_75I_5iq",
			"componentType": "panel",
			"componentName": "pnlMainBody1Right3553423898",
			"componentVisible": true
		},
		"style": {
			"alignItems": "center",
			"paddingBottom": "5px",
			"paddingRight": "5px",
			"left": "0px",
			"justifyContent": "flex-start",
			"width": "50%",
			"paddingTop": "5px",
			"paddingLeft": "5px"
		},
		"parent": "pnlMainBody6"
	},
	"lblModalMessage": {
		"style": {
			"color": "rgb(111, 111, 110)",
			"fontSize": "16px",
			"textAlign": "center"
		},
		"parent": "pnlModalBodyMessage",
		"property": {
			"componentUUID": "GZ9ZFLbtbCK1TVK3pnJwy",
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		}
	},
	"pnlMainBody": {
		"parent": "pnlMain",
		"property": {
			"componentUUID": "-oulUPngmFNyIVJxl6fpZ",
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"overflow": "scroll",
			"left": "0px",
			"flexGrow": "1"
		}
	},
	"pnlMainBody1Right35534": {
		"property": {
			"componentUUID": "IV4YGldu_uEJ0EeLs1mMP",
			"componentType": "panel",
			"componentName": "pnlMainBody1Right35534",
			"componentVisible": true
		},
		"style": {
			"width": "50%",
			"paddingTop": "5px",
			"paddingLeft": "5px",
			"left": "0px",
			"justifyContent": "flex-start",
			"alignItems": "center",
			"paddingBottom": "5px",
			"paddingRight": "5px"
		},
		"parent": "pnlMainBody4"
	},
	"pnlMain": {
		"property": {
			"componentUUID": "zWkN5FlDwEOvi-NPp3V_I",
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"position": "absolute",
			"width": "100%",
			"height": "100%",
			"left": "0px",
			"zIndex": "0"
		},
		"parent": "pnlHolder"
	},
	"Image685": {
		"property": {
			"componentUUID": "zQdM2dUhnBA4tnWI3urmh",
			"componentType": "image",
			"componentName": "Image685",
			"zoom": false,
			"componentValue": require("../../asset/icon/_catalogue.png"),
			"componentVisible": true
		},
		"style": {
			"marginBottom": "10px",
			"width": "70px"
		},
		"parent": "pnlMainBody1Right"
	},
	"pnlMainBody1Left817": {
		"property": {
			"componentUUID": "zPbcnIHZGJKWZ4_JkhAEG",
			"componentType": "panel",
			"componentName": "pnlMainBody1Left817",
			"componentVisible": true
		},
		"style": {
			"paddingBottom": "5px",
			"left": "0px",
			"alignItems": "center",
			"width": "50%",
			"paddingLeft": "5px",
			"justifyContent": "flex-start",
			"paddingTop": "5px",
			"paddingRight": "5px"
		},
		"parent": "pnlMainBody2"
	},
	"pnlMainBody1Right3": {
		"property": {
			"componentUUID": "dGrRbu_HCQmOx6_pPJ-QM",
			"componentType": "panel",
			"componentName": "pnlMainBody1Right3",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "5px",
			"paddingLeft": "5px",
			"justifyContent": "flex-start",
			"alignItems": "center",
			"paddingBottom": "5px",
			"paddingRight": "5px",
			"left": "0px",
			"width": "50%"
		},
		"parent": "pnlMainBody2"
	},
	"pnlMainBody1Right3553423": {
		"property": {
			"componentUUID": "HU9B3QS9UDi0GnEWRelmM",
			"componentType": "panel",
			"componentName": "pnlMainBody1Right3553423",
			"componentVisible": true
		},
		"style": {
			"left": "0px",
			"justifyContent": "flex-start",
			"alignItems": "center",
			"width": "50%",
			"paddingLeft": "5px",
			"paddingTop": "5px",
			"paddingBottom": "5px",
			"paddingRight": "5px"
		},
		"parent": "pnlMainBody5"
	},
	"pnlModalBodyButtons": {
		"property": {
			"componentUUID": "9kioqqunBRCyIa4XEfHOq",
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"componentVisible": true
		},
		"style": {
			"justifyContent": "center",
			"width": "90%",
			"marginRight": "5%",
			"left": "0px",
			"flexDirection": "row",
			"backgroundColor": "rgb(255, 255, 255)",
			"marginLeft": "5%"
		},
		"parent": "pnlModal"
	},
	"pnlMainBody1": {
		"parent": "pnlMainBody",
		"property": {
			"componentUUID": "QusDfo1Bf6oOQEBzy3Xwg",
			"componentType": "panel",
			"componentName": "pnlMainBody1",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "5px",
			"paddingLeft": "5px",
			"flexDirection": "row",
			"width": "100%",
			"paddingBottom": "5px",
			"paddingRight": "5px",
			"left": "0px"
		}
	},
	"Label8698259147": {
		"property": {
			"componentUUID": "0wY7ALJV9C97JN65_zW_V",
			"componentType": "label",
			"componentName": "Label8698259147",
			"componentValue": "Transfer Stocks",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"textAlign": "center"
		},
		"parent": "pnlMainBody1Left817519933"
	},
	"Label8698259147853": {
		"property": {
			"componentUUID": "8VJbBD525vus6bOyeNt7C",
			"componentType": "label",
			"componentName": "Label8698259147853",
			"componentValue": "Daily Sales Report",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"textAlign": "center"
		},
		"parent": "pnlMainBody1Left817519933542"
	},
	"pnlModalBodyMessage": {
		"property": {
			"componentUUID": "eVGqsUJ2v0Q2aqb1bXoQO",
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"left": "0px",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"lblModalNegative": {
		"property": {
			"componentUUID": "XBeuQd2691MvxMoItPqJc",
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		},
		"style": {
			"fontSize": "20px",
			"textAlign": "center",
			"color": "rgb(74, 134, 237)"
		},
		"parent": "pnlModalBodyButtonNegative"
	},
	"Image834": {
		"parent": "pnlMainBody1Left",
		"property": {
			"componentUUID": "0IHeOJviSkN_RXlTfNKkK",
			"componentType": "image",
			"componentName": "Image834",
			"componentValue": require("../../asset/icon/_customer.png"),
			"zoom": false,
			"componentVisible": true
		},
		"style": {
			"marginBottom": "10px",
			"width": "70px"
		}
	},
	"Image834936": {
		"style": {
			"width": "70px",
			"marginBottom": "10px"
		},
		"parent": "pnlMainBody1Left817",
		"property": {
			"componentUUID": "0b7s-jVs8Bcunhfxn9F0E",
			"componentType": "image",
			"componentName": "Image834936",
			"zoom": false,
			"componentValue": require("../../asset/icon/_orderListing.png"),
			"componentVisible": true
		}
	},
	"Label869825": {
		"property": {
			"componentUUID": "opgmHLEx01ILnTcTAtMnP",
			"componentType": "label",
			"componentName": "Label869825",
			"componentValue": "Order Listing",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 54, 0)",
			"textAlign": "center",
			"fontSize": "18px"
		},
		"parent": "pnlMainBody1Left817"
	},
	"Image685644": {
		"style": {
			"width": "70px",
			"marginBottom": "10px"
		},
		"parent": "pnlMainBody1Right3",
		"property": {
			"componentUUID": "vlwjlF2Xpl1mS2SCyVpFS",
			"componentType": "image",
			"componentName": "Image685644",
			"componentValue": require("../../asset/icon/_salesReturn.png"),
			"zoom": false,
			"componentVisible": true
		}
	},
	"Image834936852715": {
		"style": {
			"width": "70px",
			"marginBottom": "10px"
		},
		"parent": "pnlMainBody1Left817519933",
		"property": {
			"componentUUID": "q_xbEReDXXCPUfhHP0jVc",
			"componentType": "image",
			"componentName": "Image834936852715",
			"componentValue": require("../../asset/icon/_transferStocks.png"),
			"zoom": false,
			"componentVisible": true
		}
	},
	"lblMainCustName": {
		"property": {
			"componentUUID": "eEpjO7mgrSUT8BYAew72I",
			"componentType": "label",
			"componentName": "lblMainCustName",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px"
		},
		"parent": "pnlMainBodyCustomer"
	},
	"lblMainCustCode": {
		"property": {
			"componentUUID": "xqJEO16N8ElXrrH_SNNgi",
			"componentType": "label",
			"componentName": "lblMainCustCode",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"fontSize": "20px",
			"color": "rgb(255, 215, 0)",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyCustomer"
	},
	"pnlMainBody1Left": {
		"property": {
			"componentUUID": "SqYYww8pWvkHdPz4SpzF8",
			"componentType": "panel",
			"componentName": "pnlMainBody1Left",
			"componentVisible": true
		},
		"style": {
			"left": "0px",
			"alignItems": "center",
			"paddingRight": "5px",
			"paddingLeft": "5px",
			"justifyContent": "flex-start",
			"paddingBottom": "5px",
			"width": "50%",
			"paddingTop": "5px"
		},
		"parent": "pnlMainBody1"
	},
	"lblLoginAs": {
		"style": {
			"color": "rgb(255, 255, 255)",
			"textAlign": "center",
			"fontSize": "12px"
		},
		"parent": "pnlHeaderMiddle",
		"property": {
			"componentUUID": "KK8BvvoVrvo9CdzgZtyv0",
			"componentType": "label",
			"componentName": "lblLoginAs",
			"componentVisible": true
		}
	},
	"pnlMainHeader": {
		"property": {
			"componentUUID": "QPRcBaHyLKt2mhgd2iSN3",
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		},
		"style": {
			"left": "0px",
			"width": "100%",
			"height": "65px",
			"paddingTop": "15px",
			"flexDirection": "row",
			"backgroundColor": "rgb(255, 69, 0)"
		},
		"parent": "pnlMain"
	},
	"pnlMainBody4": {
		"property": {
			"componentUUID": "1WCKC8Ph2JOLwRNiajVfA",
			"componentType": "panel",
			"componentName": "pnlMainBody4",
			"componentVisible": true
		},
		"style": {
			"paddingBottom": "5px",
			"paddingLeft": "5px",
			"paddingRight": "5px",
			"left": "0px",
			"flexDirection": "row",
			"paddingTop": "5px",
			"width": "100%"
		},
		"parent": "pnlMainBody"
	},
	"Label8698259147853876": {
		"property": {
			"componentUUID": "mpRJ1NemUOW6IiLpZzCmn",
			"componentType": "label",
			"componentName": "Label8698259147853876",
			"componentValue": "Download Items",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"textAlign": "center"
		},
		"parent": "pnlMainBody1Left817519933542897"
	},
	"Label856312352601777707": {
		"property": {
			"componentUUID": "e3KrrAbT2axTi4MKHSBwc",
			"componentType": "label",
			"componentName": "Label856312352601777707",
			"componentValue": "Settings",
			"componentVisible": false
		},
		"style": {
			"fontSize": "18px"
		},
		"parent": "pnlMainBody1Right3553423898"
	},
	"pnlModal": {
		"property": {
			"componentUUID": "XSrwnli-54Ped5IYY207S",
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"left": "0px",
			"height": "100%",
			"position": "absolute",
			"backgroundColor": "rgba(0, 0, 0, 0.6)",
			"overflow": "hidden",
			"zIndex": "1",
			"alignItems": "center",
			"width": "100%",
			"justifyContent": "center"
		},
		"parent": "pnlHolder"
	},
	"pnlModalBodyMain": {
		"property": {
			"componentUUID": "jDE3UkcpWPfd9Dkia11Yh",
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		},
		"style": {
			"paddingLeft": "0px",
			"paddingRight": "0px",
			"marginLeft": "5%",
			"left": "0px",
			"width": "90%",
			"backgroundColor": "rgb(255, 255, 255)",
			"borderStyle": "solid",
			"marginRight": "5%",
			"paddingBottom": "10px",
			"borderWidth": "1px",
			"borderColor": "rgb(217, 217, 217)",
			"paddingTop": "10px"
		},
		"parent": "pnlModal"
	},
	"pnlModalBodyTitle": {
		"property": {
			"componentUUID": "D52H3V5SDHmBzrmufp2kQ",
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"componentVisible": true
		},
		"style": {
			"marginBottom": "10px",
			"width": "100%",
			"left": "0px"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlHolder": {
		"property": {
			"componentUUID": "DztlGU0Bn6wIobnAKOFz3",
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"left": "0px",
			"width": "100%"
		},
		"parent": "pgMainMenu"
	},
	"Image834936852": {
		"property": {
			"componentUUID": "LjsYaRORP8z-C-_eCV-WH",
			"componentType": "image",
			"componentName": "Image834936852",
			"componentValue": require("../../asset/icon/_cashCollection.png"),
			"zoom": false,
			"componentVisible": true
		},
		"style": {
			"width": "70px",
			"marginBottom": "10px"
		},
		"parent": "pnlMainBody1Left817519"
	},
	"lblModalTitle": {
		"property": {
			"componentUUID": "HpSVILqalM5JfD5QdgqqI",
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"fontSize": "18px",
			"textAlign": "center"
		},
		"parent": "pnlModalBodyTitle"
	},
	"cmbRoute": {
		"property": {
			"componentUUID": "wzHXv74CvxKnDigvgIDhK",
			"componentType": "combobox",
			"componentName": "cmbRoute",
			"componentValue": "A",
			"idField": "route",
			"componentVisible": true
		},
		"style": {
			"display": "none",
			"width": "90%",
			"height": "40px",
			"fontSize": "16px"
		},
		"parent": "pnlMainBodyCustomer"
	},
	"pnlMainBody1Right": {
		"property": {
			"componentUUID": "AzmXT9U0MBqY5srSI4Ok1",
			"componentType": "panel",
			"componentName": "pnlMainBody1Right",
			"componentVisible": true
		},
		"style": {
			"paddingBottom": "5px",
			"paddingLeft": "5px",
			"left": "0px",
			"width": "50%",
			"paddingTop": "5px",
			"paddingRight": "5px",
			"justifyContent": "flex-start",
			"alignItems": "center"
		},
		"parent": "pnlMainBody1"
	},
	"Label856": {
		"property": {
			"componentUUID": "001D0dF6-9UxQ0tn11fxd",
			"componentType": "label",
			"componentName": "Label856",
			"componentValue": "Item Catalog",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"textAlign": "center"
		},
		"parent": "pnlMainBody1Right"
	},
	"Image484": {
		"property": {
			"componentUUID": "n_RQNJ1NZekFFBePtvEDQ",
			"componentType": "image",
			"componentName": "Image484",
			"zoom": false,
			"componentValue": require("../../asset/icon/icon-username-white.png"),
			"componentVisible": true
		},
		"style": {
			"width": "35px",
			"height": "35px"
		},
		"parent": "pnlHeaderLeft"
	},
	"pnlMainBody1Left817519933542897": {
		"property": {
			"componentUUID": "QGb49hFog3qW_zvszIkL9",
			"componentType": "panel",
			"componentName": "pnlMainBody1Left817519933542897",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "5px",
			"alignItems": "center",
			"paddingLeft": "5px",
			"paddingRight": "5px",
			"width": "50%",
			"paddingBottom": "5px",
			"left": "0px",
			"justifyContent": "flex-start"
		},
		"parent": "pnlMainBody6"
	},
	"Panel231": {
		"parent": "pnlMainBody1Right3553423898",
		"property": {
			"componentUUID": "GFDujnk04atYhN3CXG9PF",
			"componentType": "panel",
			"componentName": "Panel231",
			"componentVisible": true
		},
		"style": {
			"left": "0px",
			"width": "70px",
			"borderRadius": "35px",
			"height": "70px",
			"backgroundColor": "rgb(234, 234, 234)"
		}
	},
	"lblModalPositive": {
		"property": {
			"componentUUID": "IPW5m4YWpFCdnhypgcxcJ",
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"textAlign": "center",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonPositive"
	},
	"Image685644520": {
		"property": {
			"componentUUID": "IQntvlkNtNknRUZV1j7QG",
			"componentType": "image",
			"componentName": "Image685644520",
			"componentValue": require("../../asset/icon/_stockCount.png"),
			"zoom": false,
			"componentVisible": true
		},
		"style": {
			"width": "70px",
			"marginBottom": "10px"
		},
		"parent": "pnlMainBody1Right355"
	},
	"Image215": {
		"parent": "pnlHeaderRight",
		"property": {
			"componentUUID": "e0qp_FeydEF8IqClUK8EP",
			"componentType": "image",
			"componentName": "Image215",
			"zoom": false,
			"componentValue": require("../../asset/public/images/default.png"),
			"componentVisible": true
		},
		"style": {
			"width": "35px",
			"height": "35px"
		}
	},
	"lblPONumber383": {
		"property": {
			"componentUUID": "ZiIoOcapVb6fnaSOoizGm",
			"componentType": "label",
			"componentName": "lblPONumber383",
			"componentValue": "Route:",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontWeight": "normal",
			"display": "none",
			"fontSize": "16px",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyCustomer"
	},
	"pnlMainBody2": {
		"style": {
			"left": "0px",
			"width": "100%",
			"paddingBottom": "5px",
			"paddingRight": "5px",
			"flexDirection": "row",
			"paddingTop": "5px",
			"paddingLeft": "5px"
		},
		"parent": "pnlMainBody",
		"property": {
			"componentUUID": "F11kWi-6Hx01GVj3TWJ-o",
			"componentType": "panel",
			"componentName": "pnlMainBody2",
			"componentVisible": true
		}
	},
	"pnlMainBody1Left817519933": {
		"property": {
			"componentUUID": "i5U5W8n-vFBNiD4Y4NVNR",
			"componentType": "panel",
			"componentName": "pnlMainBody1Left817519933",
			"componentVisible": true
		},
		"style": {
			"paddingLeft": "5px",
			"paddingRight": "5px",
			"paddingTop": "5px",
			"justifyContent": "flex-start",
			"width": "50%",
			"paddingBottom": "5px",
			"left": "0px",
			"alignItems": "center"
		},
		"parent": "pnlMainBody4"
	},
	"Image685644520284730": {
		"property": {
			"componentUUID": "vW7lEGD6xTcjKcE5BIMgx",
			"componentType": "image",
			"componentName": "Image685644520284730",
			"componentValue": require("../../asset/icon/_setting.png"),
			"zoom": false,
			"componentVisible": true
		},
		"style": {
			"marginBottom": "10px",
			"width": "70px"
		},
		"parent": "pnlMainBody1Right3553423"
	},
	"pnlMainBody6": {
		"property": {
			"componentUUID": "1Luc2Rp3o410GcS23xgMd",
			"componentType": "panel",
			"componentName": "pnlMainBody6",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"paddingBottom": "5px",
			"paddingRight": "5px",
			"left": "0px",
			"paddingTop": "5px",
			"paddingLeft": "5px",
			"flexDirection": "row"
		},
		"parent": "pnlMainBody"
	},
	"pnlModalBodyLoading": {
		"style": {
			"left": "0px",
			"width": "100%"
		},
		"parent": "pnlModalBodyMain",
		"property": {
			"componentUUID": "aBXG7UHP8_HGSpe32Vbwa",
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"componentVisible": true
		}
	},
	"pnlHeaderRight": {
		"property": {
			"componentUUID": "DxK8otL-lUWoMrfN36m87",
			"componentType": "panel",
			"componentName": "pnlHeaderRight",
			"componentVisible": true
		},
		"style": {
			"alignItems": "center",
			"left": "0px",
			"justifyContent": "center",
			"width": "100px"
		},
		"parent": "pnlMainHeader"
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentUUID": "3OE9w9r9a2DVQ8-Idyrxt",
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"componentVisible": true
		},
		"style": {
			"width": "50%",
			"paddingBottom": "10px",
			"paddingRight": "0px",
			"left": "0px",
			"paddingTop": "10px",
			"paddingLeft": "0px"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlModalBodyButtonPositive": {
		"property": {
			"componentUUID": "QSfnfNRCRnKV18DWFM2II",
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "10px",
			"paddingBottom": "10px",
			"paddingLeft": "0px",
			"width": "50%",
			"left": "0px",
			"paddingRight": "0px"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlMainBody1Right355": {
		"property": {
			"componentUUID": "G2uv4Xln7_A8NDKn7Eokx",
			"componentType": "panel",
			"componentName": "pnlMainBody1Right355",
			"componentVisible": true
		},
		"style": {
			"paddingLeft": "5px",
			"left": "0px",
			"width": "50%",
			"paddingTop": "5px",
			"paddingRight": "5px",
			"justifyContent": "flex-start",
			"alignItems": "center",
			"paddingBottom": "5px"
		},
		"parent": "pnlMainBody3"
	},
	"Label856312352601777": {
		"property": {
			"componentUUID": "iwHGzZT22KItZPhdvDTTR",
			"componentType": "label",
			"componentName": "Label856312352601777",
			"componentValue": "Settings",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 54, 0)",
			"fontSize": "18px",
			"textAlign": "center"
		},
		"parent": "pnlMainBody1Right3553423"
	},
	"pgMainMenu": {
		"property": {
			"componentUUID": "_iCFVW8q1OIOBYyqWi2Ie",
			"componentType": "page",
			"componentName": "pgMainMenu",
			"title": "Main Menu",
			"componentVisible": true
		}
	},
	"pnlMainBody1Left817519": {
		"property": {
			"componentUUID": "QJdsak91_U7oqabIX66uA",
			"componentType": "panel",
			"componentName": "pnlMainBody1Left817519",
			"componentVisible": true
		},
		"style": {
			"width": "50%",
			"left": "0px",
			"paddingTop": "5px",
			"justifyContent": "flex-start",
			"paddingBottom": "5px",
			"paddingRight": "5px",
			"alignItems": "center",
			"paddingLeft": "5px"
		},
		"parent": "pnlMainBody3"
	},
	"Label856312352": {
		"property": {
			"componentUUID": "RB5bl5YHhkc4uy7kc7Jxs",
			"componentType": "label",
			"componentName": "Label856312352",
			"componentValue": "Stock Count",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"textAlign": "center"
		},
		"parent": "pnlMainBody1Right355"
	},
	"pnlMainBody1Left817519933542": {
		"property": {
			"componentUUID": "__Dr_xN9A5TH5eXNxNosH",
			"componentType": "panel",
			"componentName": "pnlMainBody1Left817519933542",
			"componentVisible": true
		},
		"style": {
			"paddingRight": "5px",
			"left": "0px",
			"width": "50%",
			"justifyContent": "flex-start",
			"alignItems": "center",
			"paddingTop": "5px",
			"paddingBottom": "5px",
			"paddingLeft": "5px"
		},
		"parent": "pnlMainBody5"
	},
	"Image834936852715440208": {
		"parent": "pnlMainBody1Left817519933542897",
		"property": {
			"componentUUID": "QqL3t8gZ3I39DAPiPwlF7",
			"componentType": "image",
			"componentName": "Image834936852715440208",
			"zoom": false,
			"componentValue": require("../../asset/ic_download.png"),
			"componentVisible": true
		},
		"style": {
			"width": "70px",
			"marginBottom": "10px"
		}
	},
	"pnlMainBody3": {
		"style": {
			"paddingBottom": "5px",
			"paddingLeft": "5px",
			"paddingRight": "5px",
			"left": "0px",
			"flexDirection": "row",
			"width": "100%",
			"paddingTop": "5px"
		},
		"parent": "pnlMainBody",
		"property": {
			"componentUUID": "yKsB7lOlShxEdJPGDcHjp",
			"componentType": "panel",
			"componentName": "pnlMainBody3",
			"componentVisible": true
		}
	},
	"pnlHeaderMiddle": {
		"property": {
			"componentUUID": "itO5cuvE0XeB5dsbnboiN",
			"componentType": "panel",
			"componentName": "pnlHeaderMiddle",
			"componentVisible": true
		},
		"style": {
			"left": "0px",
			"width": "100%",
			"justifyContent": "center",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"lblMainCustAddress": {
		"property": {
			"componentUUID": "E-FyqNe-cmR39a_Zag8Qd",
			"componentType": "label",
			"componentName": "lblMainCustAddress",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "14px",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyCustomer"
	},
	"Label856312352601": {
		"property": {
			"componentUUID": "lN05CE67VYNHWqT5Ture9",
			"componentType": "label",
			"componentName": "Label856312352601",
			"componentValue": "Invoice History",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"textAlign": "center"
		},
		"parent": "pnlMainBody1Right35534"
	},
	"pnlHeaderLeft": {
		"property": {
			"componentUUID": "IJeBF4wsAFU5Lu37jI7FZ",
			"componentType": "panel",
			"componentName": "pnlHeaderLeft",
			"componentVisible": true
		},
		"style": {
			"width": "100px",
			"justifyContent": "center",
			"alignItems": "center",
			"left": "0px"
		},
		"parent": "pnlMainHeader"
	},
	"Label869": {
		"property": {
			"componentUUID": "3NupUMFn5uUZJjZCsN8HM",
			"componentType": "label",
			"componentName": "Label869",
			"componentValue": "Customer",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 167, 0)",
			"fontSize": "18px",
			"textAlign": "center"
		},
		"parent": "pnlMainBody1Left"
	},
	"Image685644520284": {
		"style": {
			"marginBottom": "10px",
			"width": "70px"
		},
		"parent": "pnlMainBody1Right35534",
		"property": {
			"componentUUID": "LeImocxH2Qj5U0zBwECjo",
			"componentType": "image",
			"componentName": "Image685644520284",
			"componentValue": require("../../asset/icon/_invoiceHistory.png"),
			"zoom": false,
			"componentVisible": true
		}
	},
	"imgModalLoading": {
		"property": {
			"componentUUID": "XZL9skYJ_eg1Xhed8zfHJ",
			"componentType": "image",
			"componentName": "imgModalLoading",
			"zoom": false,
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		},
		"style": {
			"height": "10px",
			"alignSelf": "center",
			"width": "50px"
		},
		"parent": "pnlModalBodyLoading"
	},
	"lblTitle": {
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"textAlign": "center"
		},
		"parent": "pnlHeaderMiddle",
		"property": {
			"componentUUID": "c5zA_a1Do90Z-2rLpZTGL",
			"componentType": "label",
			"componentName": "lblTitle",
			"componentVisible": true
		}
	}
};
