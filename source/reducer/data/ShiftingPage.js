export const StateName = 'ShiftingPage';
export const State = {
    labelFirstName: {
        property: {
            componentUUID: 'SP1',
            componentType: 'label',
            componentValue: 'Empat'
        }
    },
    labelLastName: {
        property: {
            componentUUID: 'SP2',
            componentType: 'label',
            componentValue: 'Tan'
        }
    },
    buttonChangeName: {
        property: {
            componentUUID: 'SP3',
            componentType: 'button',
            componentValue: 'Change Name'
        }
    },
    buttonGoToPage: {
        property: {
            componentUUID: 'SP4',
            componentType: 'button',
            componentValue: 'Go To Page'
        }
    }
}