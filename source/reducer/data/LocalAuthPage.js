export const StateName = 'LocalAuthPage';
export const State = {
    page: {
        style: {
			flex: 1,
			backgroundColor: 'seashell',
			alignItems: 'center',
            justifyContent: 'center'
		},
		property: {
			componentDisabled: false,
		}
    },
    btnHasLocalAuth: {
		style: {
			marginTop: 50,
			width: '100%',
			color: 'springgreen',
		},
		property: {
			componentValue: 'Device Has Touch / Face AUTH ?',
			componentVisible: true,
		}
	},
    btnLocalAuth: {
		style: {
			marginTop: 50,
			width: '100%',
			color: 'springgreen',
		},
		property: {
			componentValue: 'AUTH with Touch / Face ID',
			componentVisible: true,
		}
	},
}