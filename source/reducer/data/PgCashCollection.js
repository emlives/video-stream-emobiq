export const StateName = 'pgCashCollection';
export const State = {
	"lblStyle174626795": {
		"property": {
			"componentType": "label",
			"componentName": "lblStyle174626795",
			"componentValue": "N/A",
			"componentVisible": true
		},
		"parent": "pnlCSS5"
	},
	"pnlMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"zIndex": "0",
			"width": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlPaymentMethod": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlPaymentMethod",
			"componentVisible": true
		},
		"style": {
			"marginTop": "10px",
			"marginBottom": "10px"
		},
		"parent": "Panel916681385"
	},
	"lblInvoiceAmount_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblInvoiceAmount_data",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(29, 133, 88)",
			"fontSize": "18px"
		},
		"parent": "pnlInvoiceAmount"
	},
	"pnlModalCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModalTable"
	},
	"pnlModalBodyTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"marginBottom": "10px",
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlMainBodyPrintContent": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyPrintContent",
			"componentVisible": true
		},
		"style": {
			"height": "100px",
			"padding": "10px",
			"display": "table",
			"width": "100%"
		},
		"parent": "pnlPrinterTableCellContent"
	},
	"pnlBtnPrintOK": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBtnPrintOK",
			"componentVisible": true
		},
		"style": {
			"width": "50%",
			"textAlign": "center",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "Panel896"
	},
	"pnlMainFooterTableCellSubmit": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterTableCellSubmit",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"width": "50%",
			"textAlign": "center",
			"borderRight": "1px solid #FFF",
			"color": "rgba(255, 255, 255, 0)",
			"display": "table-cell",
			"backgroundColor": "rgba(0, 0, 0, 0)"
		},
		"parent": "pnlMainFooterTable"
	},
	"pnlMainFooterTableCellSubmitPrint": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterTableCellSubmitPrint",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgba(0, 0, 0, 0)",
			"borderLeft": "1px solid #FFF",
			"display": "table-cell",
			"color": "rgba(255, 255, 255, 0)",
			"textAlign": "center",
			"verticalAlign": "middle",
			"width": "50%"
		},
		"parent": "pnlMainFooterTable"
	},
	"Panel63": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel63",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"display": "table",
			"width": "100%",
			"height": "40px",
			"borderRadius": "0px 0px 10px 10px",
			"backgroundColor": "rgb(255, 69, 0)"
		},
		"parent": "pnlUpdateCartModalBodyItemListing"
	},
	"lblCodeName_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblCodeName_data",
			"componentValue": "Code - Customer Name",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"paddingTop": "10px",
			"display": "block",
			"color": "rgb(255, 215, 0)",
			"fontSize": "16px",
			"fontWeight": "bold"
		},
		"parent": "pnlBodyContentHeader"
	},
	"Panel609": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel609",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"height": "40px",
			"textAlign": "center",
			"paddingTop": "8px",
			"float": "right",
			"verticalAlign": "middle",
			"width": "50px",
			"backgroundColor": "rgb(84, 84, 84)"
		},
		"parent": "pnlPaymentAmount"
	},
	"lblModalTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "18px",
			"fontWeight": "bold"
		},
		"parent": "pnlModalBodyTitle"
	},
	"imgModalLoading": {
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		},
		"style": {
			"width": "50px",
			"height": "10px"
		},
		"parent": "pnlModalBodyLoading"
	},
	"pnlMainHeader": {
		"parent": "pnlMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		},
		"style": {
			"overflow": "auto",
			"height": "65px",
			"backgroundColor": "rgb(255, 69, 0)",
			"paddingTop": "15px",
			"width": "100%",
			"textAlign": "center"
		}
	},
	"pnlMainHeaderLeftCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftCell",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"textAlign": "left",
			"display": "table-cell",
			"width": "100%",
			"height": "100%",
			"paddingLeft": "10px"
		},
		"parent": "pnlMainHeaderLeftTable"
	},
	"imgPrint": {
		"style": {
			"width": "35px",
			"height": "35px"
		},
		"parent": "pnlMainHeaderRightCell",
		"property": {
			"componentType": "image",
			"componentName": "imgPrint",
			"componentValue": require("../../asset/Printer_white.png"),
			"componentVisible": true
		}
	},
	"pnlPrinter": {
		"style": {
			"width": "100%",
			"height": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"position": "absolute",
			"top": "0px",
			"zIndex": "2"
		},
		"parent": "pnlHolder",
		"property": {
			"componentType": "panel",
			"componentName": "pnlPrinter",
			"componentVisible": false
		}
	},
	"lblChequeNumber": {
		"property": {
			"componentType": "label",
			"componentName": "lblChequeNumber",
			"componentValue": "Cheque Number",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(60, 60, 59)",
			"fontSize": "14px",
			"fontWeight": "bold"
		},
		"parent": "pnlChequeNumber"
	},
	"Panel434474168553131796": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel434474168553131796",
			"componentVisible": true
		},
		"style": {
			"marginTop": "30px",
			"marginBottom": "30px"
		},
		"parent": "Panel916681385"
	},
	"Panel845": {
		"parent": "pnlUpdateCartModalBodyItemListing",
		"property": {
			"componentType": "panel",
			"componentName": "Panel845",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"overflow": "auto",
			"height": "calc(100% - 89px)",
			"padding": "10px"
		}
	},
	"lblRemarks4921680694128548": {
		"property": {
			"componentType": "label",
			"componentName": "lblRemarks4921680694128548",
			"componentValue": "Click to select invoices",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"fontWeight": "bold",
			"textAlign": "center",
			"backgroundColor": "rgb(29, 133, 88)",
			"verticalAlign": "middle",
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"padding": "20px"
		},
		"parent": "Panel434474168553131796"
	},
	"pnlContentMainLines": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlContentMainLines",
			"componentVisible": true
		},
		"style": {
			"minWidth": "40px",
			"minHeight": "50px"
		},
		"parent": "pnlMainBody"
	},
	"lblBtnSubmit": {
		"property": {
			"componentType": "label",
			"componentName": "lblBtnSubmit",
			"componentValue": "SUBMIT",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"fontSize": "16px"
		},
		"parent": "pnlMainFooterTableCellSubmit"
	},
	"Panel936": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel936",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "50px",
			"backgroundColor": "rgb(255, 69, 0)",
			"position": "relative",
			"display": "table",
			"borderRadius": "10px 10px 0px 0px"
		},
		"parent": "pnlUpdateCartModalBodyItemListing"
	},
	"Label326355612": {
		"property": {
			"componentType": "label",
			"componentName": "Label326355612",
			"componentValue": "CANCEL",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "16px"
		},
		"parent": "Panel250797"
	},
	"pnlMainHeaderMiddleTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddle"
	},
	"pnlMainHeaderRightCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightCell",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%",
			"textAlign": "right",
			"paddingRight": "10px",
			"display": "table-cell"
		},
		"parent": "pnlMainHeaderRightTable"
	},
	"pnlPaymentAmount": {
		"style": {
			"marginTop": "10px",
			"marginBottom": "10px"
		},
		"parent": "Panel916681385",
		"property": {
			"componentType": "panel",
			"componentName": "pnlPaymentAmount",
			"componentVisible": true
		}
	},
	"pnlModalBodyLoading": {
		"style": {
			"width": "100%"
		},
		"parent": "pnlModalBodyMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		}
	},
	"lblInvoiceAmount": {
		"property": {
			"componentType": "label",
			"componentName": "lblInvoiceAmount",
			"componentValue": "Invoice Amount",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(60, 60, 59)",
			"fontSize": "14px",
			"fontWeight": "bold"
		},
		"parent": "pnlInvoiceAmount"
	},
	"lblModalMessage": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(111, 111, 110)",
			"fontSize": "16px"
		},
		"parent": "pnlModalBodyMessage"
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"width": "50%",
			"textAlign": "center",
			"padding": "10px 0px",
			"float": "none"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlPrinterTableCellContent": {
		"style": {
			"position": "relative",
			"background": "#ffffff",
			"margin": "5%",
			"width": "90%",
			"textAlign": "center",
			"borderRadius": "5px"
		},
		"parent": "pnlPrinterTableCell",
		"property": {
			"componentType": "panel",
			"componentName": "pnlPrinterTableCellContent",
			"componentVisible": true
		}
	},
	"lblSelectPrinter": {
		"parent": "pnlBodyPrintContent",
		"property": {
			"componentType": "label",
			"componentName": "lblSelectPrinter",
			"componentValue": "Select Printer",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontSize": "18px",
			"fontWeight": "bold"
		}
	},
	"pnlMainHeaderRight": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRight",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlBodyContentHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyContentHeader",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgb(84, 84, 84)"
		},
		"parent": "pnlMainBody"
	},
	"pnlInvoiceAmount": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlInvoiceAmount",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"marginTop": "10px",
			"marginBottom": "10px"
		},
		"parent": "Panel916681385"
	},
	"pnlModalBodyButtons": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "90%",
			"textAlign": "center",
			"background": "#ffffff",
			"marginLeft": "5%",
			"marginRight": "5%"
		},
		"parent": "pnlModalCell"
	},
	"pnlPrinterTableCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlPrinterTableCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"width": "100%",
			"height": "100%",
			"position": "relative",
			"verticalAlign": "middle"
		},
		"parent": "pnlPrinterTable"
	},
	"pnlChequeNumber": {
		"style": {
			"marginTop": "10px",
			"marginBottom": "10px"
		},
		"parent": "Panel916681385",
		"property": {
			"componentType": "panel",
			"componentName": "pnlChequeNumber",
			"addClass": "hide",
			"componentVisible": true
		}
	},
	"Panel250797": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel250797",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"width": "50%",
			"height": "100%",
			"verticalAlign": "middle",
			"textAlign": "center",
			"borderLeft": "1px solid #FFF"
		},
		"parent": "Panel63"
	},
	"pnlModalBodyMessage": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"edtPaymentAmount": {
		"parent": "pnlPaymentAmount",
		"property": {
			"componentType": "edit",
			"componentName": "edtPaymentAmount",
			"type": "number",
			"componentVisible": true
		},
		"style": {
			"width": "calc(100% - 50px)",
			"height": "40px",
			"fontSize": "18px",
			"border": "1px solid #3C3C3B"
		}
	},
	"lblBtnSubmitPrint": {
		"property": {
			"componentType": "label",
			"componentName": "lblBtnSubmitPrint",
			"componentValue": "SUBMIT & PRINT",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"fontSize": "16px"
		},
		"parent": "pnlMainFooterTableCellSubmitPrint"
	},
	"pnlCSS5": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCSS5",
			"componentVisible": false
		},
		"style": {
			"top": "0px",
			"zIndex": "4",
			"width": "100%",
			"height": "100%",
			"position": "fixed",
			"display": "none"
		},
		"parent": "pnlHolder"
	},
	"Label326": {
		"property": {
			"componentType": "label",
			"componentName": "Label326",
			"componentValue": "OK",
			"componentVisible": true
		},
		"style": {
			"fontSize": "16px",
			"color": "rgb(255, 255, 255)"
		},
		"parent": "Panel250"
	},
	"pnlModalBodyMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"background": "#ffffff",
			"width": "90%",
			"borderBottom": "1px solid #D9D9D9",
			"padding": "10px 0px",
			"marginLeft": "5%",
			"marginRight": "5%"
		},
		"parent": "pnlModalCell"
	},
	"pnlMainHeaderLeft": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeft",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlMainHeaderLeftTable": {
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table"
		},
		"parent": "pnlMainHeaderLeft",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTable",
			"componentVisible": true
		}
	},
	"pnlMainBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "calc(100% - 50px - 40px - 15px)"
		},
		"parent": "pnlMain"
	},
	"cmbPrinter": {
		"property": {
			"componentType": "combobox",
			"componentName": "cmbPrinter",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"border": "1px solid #3C3C3B"
		},
		"parent": "pnlBodyPrintContent"
	},
	"imgBack": {
		"property": {
			"componentType": "image",
			"componentName": "imgBack",
			"componentValue": require("../../asset/icon/icon-menu.png"),
			"componentVisible": true
		},
		"style": {
			"width": "35px",
			"height": "35px",
			"textAlign": "left"
		},
		"parent": "pnlMainHeaderLeftCell"
	},
	"pnlUpdateCartModalBodyItemListing": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyItemListing",
			"componentVisible": true
		},
		"style": {
			"borderRadius": "10px",
			"width": "90%",
			"background": "#ffffff",
			"textAlign": "center",
			"margin": "7.5% 5%",
			"position": "relative",
			"height": "90%"
		},
		"parent": "pnlModalOutstandingInvoices"
	},
	"pnlPrinterTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlPrinterTable",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"position": "relative",
			"display": "table",
			"width": "100%"
		},
		"parent": "pnlPrinter"
	},
	"Panel875": {
		"style": {
			"textAlign": "center",
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "Panel936",
		"property": {
			"componentType": "panel",
			"componentName": "Panel875",
			"componentVisible": true
		}
	},
	"pnlModal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"height": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"position": "fixed",
			"top": "0px",
			"zIndex": "3",
			"width": "100%"
		},
		"parent": "pnlHolder"
	},
	"lblPrintOK": {
		"property": {
			"componentType": "label",
			"componentName": "lblPrintOK",
			"componentValue": "OK",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "16px",
			"backgroundColor": "rgba(255, 255, 255, 0)"
		},
		"parent": "pnlBtnPrintOK"
	},
	"lblMainTitle": {
		"style": {
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500"
		},
		"parent": "pnlMainHeaderMiddleCell",
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle",
			"componentValue": "CASH COLLECTION",
			"componentVisible": true
		}
	},
	"Panel916681385": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel916681385",
			"componentVisible": true
		},
		"style": {
			"padding": "20px"
		},
		"parent": "pnlMainBody"
	},
	"pnlTotalOutstanding": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlTotalOutstanding",
			"componentVisible": true
		},
		"style": {
			"marginTop": "10px",
			"marginBottom": "10px"
		},
		"parent": "Panel916681385"
	},
	"lblTotalOutstanding_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblTotalOutstanding_data",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(139, 35, 35)",
			"fontSize": "18px"
		},
		"parent": "pnlTotalOutstanding"
	},
	"lblRemark": {
		"property": {
			"componentType": "label",
			"componentName": "lblRemark",
			"componentValue": "Remark",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(60, 60, 59)",
			"fontSize": "14px",
			"fontWeight": "bold"
		},
		"parent": "pnllblRemark"
	},
	"Label152": {
		"property": {
			"componentType": "label",
			"componentName": "Label152",
			"componentValue": "OUTSTANDING INVOICES",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"fontWeight": "bold"
		},
		"parent": "Panel875"
	},
	"Panel250": {
		"style": {
			"verticalAlign": "middle",
			"width": "50%",
			"textAlign": "center",
			"borderRight": "1px solid #FFF",
			"display": "table-cell",
			"height": "100%"
		},
		"parent": "Panel63",
		"property": {
			"componentType": "panel",
			"componentName": "Panel250",
			"componentVisible": true
		}
	},
	"lblModalPositive": {
		"parent": "pnlModalBodyButtonPositive",
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		}
	},
	"pgCashCollection": {
		"style": {
			"backgroundColor": "rgba(0, 0, 0, 0)"
		},
		"property": {
			"componentType": "page",
			"componentName": "pgCashCollection",
			"title": "Cash Collection",
			"componentVisible": true
		}
	},
	"pnlHolder": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		},
		"style": {
			"overflow": "auto",
			"width": "100%",
			"height": "100%",
			"backgroundColor": "rgb(245, 245, 245)"
		},
		"parent": "pgCashCollection"
	},
	"pnlMainHeaderRightTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRight"
	},
	"Panel896": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel896",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainFooterPrintOK"
	},
	"pnlMainHeaderMiddleCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddleTable"
	},
	"lblAddress_data": {
		"parent": "pnlBodyContentHeader",
		"property": {
			"componentType": "label",
			"componentName": "lblAddress_data",
			"componentValue": "Address",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"textAlign": "center",
			"paddingBottom": "10px"
		}
	},
	"cmbPaymentMethod": {
		"style": {
			"fontSize": "18px",
			"border": "1px solid #3C3C3B",
			"margin": "0",
			"width": "100%",
			"height": "40px"
		},
		"parent": "pnlPaymentMethod",
		"property": {
			"componentType": "combobox",
			"componentName": "cmbPaymentMethod",
			"componentVisible": true
		}
	},
	"Label103": {
		"property": {
			"componentType": "label",
			"componentName": "Label103",
			"componentValue": "FULL",
			"componentVisible": true
		},
		"style": {
			"width": "50px",
			"color": "rgb(255, 255, 255)",
			"fontWeight": "bold"
		},
		"parent": "Panel609"
	},
	"dtOK365_Customer_Ledger_Entries": {
		"property": {
			"componentType": "datalist",
			"componentName": "dtOK365_Customer_Ledger_Entries",
			"componentValue": "OK365_Customer_Ledger_Entries",
			"componentData": [],
			"componentVisible": true
		},
		"style": {
			"height": "100%"
		},
		"parent": "Panel845",
		"template": {
			"componentDataItemState": {
				"Label56676543": {
					"property": {
						"componentType": "label",
						"componentName": "Label56676543",
						"componentValue": ":",
						"componentVisible": true
					},
					"style": {
						"fontSize": "16px",
						"color": "rgb(60, 60, 59)",
						"fontWeight": "bold",
						"textAlign": "center",
						"width": "10px"
					},
					"parent": "Panel172940"
				},
				"Label432443510": {
					"property": {
						"componentType": "label",
						"componentName": "Label432443510",
						"componentValue": "Label",
						"componentVisible": true
					},
					"style": {
						"width": "calc(100% - 100px - 10px)",
						"color": "rgb(60, 60, 59)",
						"fontSize": "16px"
					},
					"parent": "Panel172940"
				},
				"Panel172954": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel172954",
						"componentVisible": true
					},
					"parent": "Panel806931"
				},
				"Label566587457": {
					"property": {
						"componentType": "label",
						"componentName": "Label566587457",
						"componentValue": ":",
						"componentVisible": true
					},
					"style": {
						"fontSize": "16px",
						"width": "10px",
						"color": "rgb(60, 60, 59)",
						"fontWeight": "bold",
						"textAlign": "center"
					},
					"parent": "Panel172954"
				},
				"Label432206691": {
					"property": {
						"componentType": "label",
						"componentName": "Label432206691",
						"componentValue": "Label",
						"componentVisible": true
					},
					"style": {
						"width": "calc(100% - 100px - 10px)",
						"color": "rgb(60, 60, 59)",
						"fontSize": "16px"
					},
					"parent": "Panel172954"
				},
				"Label566": {
					"property": {
						"componentType": "label",
						"componentName": "Label566",
						"componentValue": ":",
						"componentVisible": true
					},
					"style": {
						"width": "10px",
						"fontSize": "16px",
						"fontWeight": "bold",
						"color": "rgb(60, 60, 59)",
						"textAlign": "center"
					},
					"parent": "Panel172"
				},
				"Label80375663": {
					"property": {
						"componentType": "label",
						"componentName": "Label80375663",
						"componentValue": "Outstanding",
						"componentVisible": true
					},
					"style": {
						"width": "100px",
						"color": "rgb(60, 60, 59)",
						"fontSize": "16px",
						"fontWeight": "bold"
					},
					"parent": "Panel172940"
				},
				"Label80": {
					"property": {
						"componentType": "label",
						"componentName": "Label80",
						"componentValue": "Invoice No.",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(60, 60, 59)",
						"fontSize": "16px",
						"fontWeight": "bold",
						"width": "100px"
					},
					"parent": "Panel172"
				},
				"Label432": {
					"property": {
						"componentType": "label",
						"componentName": "Label432",
						"componentValue": "Label",
						"field": "Document_No",
						"componentVisible": true
					},
					"style": {
						"width": "calc(100% - 100px - 10px)",
						"color": "rgb(60, 60, 59)",
						"fontSize": "16px"
					},
					"parent": "Panel172"
				},
				"Label80535282": {
					"property": {
						"componentType": "label",
						"componentName": "Label80535282",
						"componentValue": "Posting Date",
						"componentVisible": true
					},
					"style": {
						"width": "100px",
						"color": "rgb(60, 60, 59)",
						"fontSize": "16px",
						"fontWeight": "bold"
					},
					"parent": "Panel172954"
				},
				"Panel806931": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel806931",
						"componentVisible": true
					},
					"style": {
						"minHeight": "50px",
						"textAlign": "left",
						"display": "table-cell",
						"minWidth": "40px"
					},
					"parent": "Panel803"
				},
				"Panel172": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel172",
						"componentVisible": true
					},
					"parent": "Panel806931"
				},
				"Checkbox892": {
					"property": {
						"componentType": "checkbox",
						"componentName": "Checkbox892",
						"idPrefix": "chk_",
						"idField": "Document_No",
						"componentVisible": true
					},
					"style": {
						"padding": "0",
						"margin": "0"
					},
					"parent": "Panel806"
				},
				"Panel172940": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel172940",
						"componentVisible": true
					},
					"parent": "Panel806931"
				},
				"Panel803": {
					"style": {
						"display": "table",
						"width": "100%",
						"boxShadow": "-1px 1px 1px #545454",
						"border": "1px solid #3C3C3B",
						"marginBottom": "5px",
						"borderRadius": "5px"
					},
					"property": {
						"componentType": "panel",
						"componentName": "Panel803",
						"componentVisible": true
					}
				},
				"Panel806": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel806",
						"componentVisible": true
					},
					"style": {
						"verticalAlign": "middle",
						"minWidth": "40px",
						"minHeight": "50px",
						"display": "table-cell",
						"width": "70px",
						"textAlign": "center"
					},
					"parent": "Panel803"
				}
			}
		}
	},
	"pnlModalTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModal"
	},
	"pnlModalBodyButtonPositive": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"float": "none",
			"width": "50%",
			"textAlign": "center",
			"padding": "10px 0px"
		},
		"parent": "pnlModalBodyButtons"
	},
	"lblTotalOutstanding": {
		"property": {
			"componentType": "label",
			"componentName": "lblTotalOutstanding",
			"componentValue": "Total Outstanding",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(60, 60, 59)",
			"fontSize": "14px",
			"fontWeight": "bold"
		},
		"parent": "pnlTotalOutstanding"
	},
	"pnllblRemark": {
		"property": {
			"componentType": "panel",
			"componentName": "pnllblRemark",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"marginTop": "20px",
			"marginBottom": "10px"
		},
		"parent": "Panel916681385"
	},
	"pnlMainFooter": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooter",
			"componentVisible": true
		},
		"style": {
			"height": "40px",
			"backgroundColor": "rgb(255, 69, 0)",
			"width": "100%"
		},
		"parent": "pnlMain"
	},
	"pnlBodyPrintContent": {
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyPrintContent",
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyPrintContent",
			"componentVisible": true
		}
	},
	"edtChequeNumber": {
		"property": {
			"componentType": "edit",
			"componentName": "edtChequeNumber",
			"type": "text",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"border": "1px solid #3C3C3B",
			"width": "100%",
			"height": "40px"
		},
		"parent": "pnlChequeNumber"
	},
	"pnlModalOutstandingInvoices": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalOutstandingInvoices",
			"componentVisible": false
		},
		"style": {
			"top": "0",
			"zIndex": "1",
			"height": "100%",
			"position": "fixed",
			"width": "100%",
			"background": "rgba(0, 0, 0, 0.60)"
		},
		"parent": "pnlHolder"
	},
	"lblModalNegative": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonNegative"
	},
	"edtRemark": {
		"property": {
			"componentType": "edit",
			"componentName": "edtRemark",
			"type": "text",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "40px",
			"fontSize": "18px",
			"border": "1px solid #3C3C3B"
		},
		"parent": "pnllblRemark"
	},
	"pnlMainFooterTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainFooter"
	},
	"pnlMainFooterPrintOK": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterPrintOK",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"backgroundColor": "rgb(255, 69, 0)",
			"paddingBottom": "1%",
			"width": "100%",
			"height": "40px",
			"borderRadius": "0px 0px 5px 5px ",
			"paddingTop": "1%"
		},
		"parent": "pnlPrinterTableCellContent"
	},
	"pnlMainHeaderMiddle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddle",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "70%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"lblPaymentMethod": {
		"parent": "pnlPaymentMethod",
		"property": {
			"componentType": "label",
			"componentName": "lblPaymentMethod",
			"componentValue": "Payment Method",
			"componentVisible": true
		},
		"style": {
			"fontSize": "14px",
			"fontWeight": "bold",
			"display": "block",
			"color": "rgb(60, 60, 59)"
		}
	},
	"lblPaymentAmount": {
		"property": {
			"componentType": "label",
			"componentName": "lblPaymentAmount",
			"componentValue": "Payment Amount",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"display": "block",
			"color": "rgb(60, 60, 59)",
			"fontSize": "14px"
		},
		"parent": "pnlPaymentAmount"
	}
};
