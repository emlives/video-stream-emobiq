export const StateName = 'pgOrderList';
export const State = {
	"pnlUpdateCartModalBody": {
		"parent": "pnlUpdateCartModalCell",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBody",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"background": "#ffffff",
			"marginLeft": "5%",
			"marginRight": "5%",
			"width": "90%",
			"padding": "10px"
		}
	},
	"lblCustomerName": {
		"property": {
			"componentType": "label",
			"componentName": "lblCustomerName",
			"componentValue": "Customer Name",
			"componentVisible": true
		},
		"style": {
			"width": "70%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"textAlign": "left",
			"fontWeight": "normal"
		},
		"parent": "pnlMainBodyForm1Label"
	},
	"pnlMainBodyForm1Input180830": {
		"style": {
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyForm1481",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm1Input180830",
			"componentVisible": true
		}
	},
	"pnlModalBodyMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		},
		"style": {
			"borderBottom": "1px solid #D9D9D9",
			"marginLeft": "5%",
			"width": "90%",
			"textAlign": "center",
			"background": "#ffffff",
			"padding": "10px 0px",
			"marginRight": "5%"
		},
		"parent": "pnlModalCell"
	},
	"lblModalMessage": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(111, 111, 110)",
			"fontSize": "16px",
			"width": "100%"
		},
		"parent": "pnlModalBodyMessage"
	},
	"lblModalNegative": {
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonNegative",
		"property": {
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		}
	},
	"pnlPending": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlPending",
			"componentVisible": true
		},
		"style": {
			"borderBottom": "2px solid #f22260",
			"width": "50%",
			"textAlign": "center",
			"display": "table-cell"
		},
		"parent": "pnlMainTabHeader"
	},
	"pnlUpdateCartModalCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlUpdateCartModalTable"
	},
	"imgSearchFilters": {
		"style": {
			"display": "none",
			"width": "30px",
			"height": "30px",
			"padding": "5px"
		},
		"parent": "pnlMainHeaderRightCell",
		"property": {
			"componentType": "image",
			"componentName": "imgSearchFilters",
			"componentValue": require("../../asset/images/icon-filter-gray.png"),
			"componentVisible": true
		}
	},
	"pnlMainHeaderLeftTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderLeft"
	},
	"pnlMainHeaderMiddleTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddle"
	},
	"txtDocumentDateFrom": {
		"property": {
			"componentType": "edit",
			"componentName": "txtDocumentDateFrom",
			"type": "date",
			"disabled": false,
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"padding": "0px",
			"fontSize": "16px",
			"border": "1px solid #D9D9D9",
			"height": "30px",
			"color": "rgb(0, 0, 0)",
			"width": "45%",
			"margin": "0px",
			"backgroundColor": "rgba(255, 255, 255, 0.98)"
		},
		"parent": "pnlMainBodyForm2Input739198"
	},
	"pnlModalTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModal"
	},
	"pnlMainBodyForm2Label": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm2Label",
			"componentVisible": true
		},
		"style": {
			"textAlign": "left",
			"width": "100%"
		},
		"parent": "pnlMainBodyForm2"
	},
	"pnlMainBodyForm2Input739198": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm2Input739198",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyForm2498"
	},
	"pnlMainBodyForm2498": {
		"parent": "pnlUpdateCartModalBody2",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm2498",
			"componentVisible": true
		},
		"style": {
			"marginBottom": "10px",
			"width": "100%",
			"textAlign": "center"
		}
	},
	"pnlHolder": {
		"style": {
			"height": "100%",
			"width": "100%"
		},
		"parent": "pgOrderList",
		"property": {
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		}
	},
	"lblOutletCode": {
		"parent": "pnlMainBodyForm1Label551333",
		"property": {
			"componentType": "label",
			"componentName": "lblOutletCode",
			"componentValue": "Outlet Code",
			"componentVisible": true
		},
		"style": {
			"textAlign": "left",
			"width": "70%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"fontWeight": "normal"
		}
	},
	"pnlUpdateCartModalBody2": {
		"parent": "pnlUpdateCartModalBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBody2",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"paddingTop": "20px",
			"marginBottom": "10px"
		}
	},
	"lblDeliveryDateFrom": {
		"property": {
			"componentType": "label",
			"componentName": "lblDeliveryDateFrom",
			"componentValue": "Req. Delivery Date",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "normal",
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyForm2Label"
	},
	"pnlMainHeaderRightTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRight"
	},
	"pnlMainBodySearch": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodySearch",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "50px",
			"padding": "5px"
		},
		"parent": "pnlMainSearch"
	},
	"cmbSalesperson": {
		"property": {
			"componentType": "combobox",
			"componentName": "cmbSalesperson",
			"componentVisible": true
		},
		"style": {
			"border": "1px solid #D9D9D9",
			"margin": "0px"
		},
		"parent": "pnlMainBodyForm1Input696752"
	},
	"pnlMainHeaderLeftCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"textAlign": "left",
			"paddingLeft": "10px",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderLeftTable"
	},
	"lblDocumentDate": {
		"property": {
			"componentType": "label",
			"componentName": "lblDocumentDate",
			"componentValue": "Document Date",
			"componentVisible": true
		},
		"style": {
			"textAlign": "left",
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"fontWeight": "normal"
		},
		"parent": "pnlMainBodyForm2Label820995"
	},
	"lblPending": {
		"property": {
			"componentType": "label",
			"componentName": "lblPending",
			"componentValue": "DRAFT",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "600",
			"color": "rgba(242, 34, 34, 0.95)",
			"fontSize": "10pt"
		},
		"parent": "pnlPending"
	},
	"pnlMainTabPending": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainTabPending",
			"componentVisible": true
		},
		"style": {
			"overflow": "auto",
			"width": "100%"
		},
		"parent": "pnlBody"
	},
	"pnlMainTabApproved": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainTabApproved",
			"componentVisible": true
		},
		"style": {
			"display": "none",
			"overflow": "auto",
			"width": "100%"
		},
		"parent": "pnlBody"
	},
	"lblCustomerName22": {
		"property": {
			"componentType": "label",
			"componentName": "lblCustomerName22",
			"componentValue": "Sales Employee",
			"componentVisible": true
		},
		"style": {
			"fontSize": "16px",
			"textAlign": "left",
			"color": "rgb(60, 60, 59)",
			"fontWeight": "normal"
		},
		"parent": "pnlMainBodyForm1Label554794"
	},
	"pnlModal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"top": "0px",
			"width": "100%",
			"height": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"zIndex": "4"
		},
		"parent": "pnlHolder"
	},
	"pnlModalBodyMessage": {
		"parent": "pnlModalBodyMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		}
	},
	"pnlMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"zIndex": "0",
			"width": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlMainHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgb(255, 69, 0)",
			"paddingTop": "15px",
			"width": "100%",
			"textAlign": "center",
			"height": "65px"
		},
		"parent": "pnlMain"
	},
	"txtDateTo": {
		"property": {
			"componentType": "edit",
			"componentName": "txtDateTo",
			"type": "date",
			"disabled": false,
			"componentVisible": true
		},
		"style": {
			"width": "45%",
			"display": "inline-block",
			"color": "rgb(0, 0, 0)",
			"fontSize": "16px",
			"border": "1px solid #D9D9D9",
			"backgroundColor": "rgba(255, 255, 255, 0.98)",
			"padding": "0px",
			"height": "30px",
			"margin": "0px"
		},
		"parent": "pnlMainBodyForm2Input"
	},
	"pnlModalBodyButtonPositive": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"float": "none",
			"width": "50%",
			"textAlign": "center",
			"padding": "10px 0px",
			"display": "inline-block"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlMainHeaderMiddle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddle",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "70%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlMainBodyForm1Label": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm1Label",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyForm1"
	},
	"dlDraft": {
		"property": {
			"componentType": "datalist",
			"componentName": "dlDraft",
			"componentValue": "l_orderlist",
			"autoLoad": false,
			"componentData": [],
			"componentVisible": true
		},
		"parent": "pnlMainTabPending",
		"template": {
			"componentDataItemState": {
				"lblOulet_data": {
					"property": {
						"componentType": "label",
						"componentName": "lblOulet_data",
						"field": "Sell_to_Customer_Name",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"verticalAlign": "top",
						"color": "rgb(111, 111, 110)",
						"fontWeight": "normal",
						"fontSize": "16px"
					},
					"parent": "pnlProductBodyMiddle3"
				},
				"lblDocTotal1": {
					"property": {
						"componentType": "label",
						"componentName": "lblDocTotal1",
						"componentValue": "Doc. Total:",
						"componentVisible": true
					},
					"style": {
						"fontSize": "16px",
						"fontWeight": "normal",
						"textAlign": "left",
						"display": "none",
						"width": "30%",
						"color": "rgb(111, 111, 110)"
					},
					"parent": "pnlProductBodyMiddle5"
				},
				"lblDocTotal_data": {
					"property": {
						"componentType": "label",
						"componentName": "lblDocTotal_data",
						"componentVisible": true
					},
					"style": {
						"fontSize": "18px",
						"display": "block",
						"color": "rgb(0, 153, 0)",
						"fontWeight": "bold"
					},
					"parent": "pnlProductBodyMiddle5"
				},
				"pnlProductBodyMiddle": {
					"style": {
						"textAlign": "left",
						"display": "table-cell",
						"width": "calc(100% - 40px)"
					},
					"parent": "pnlProductBody",
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle",
						"componentVisible": true
					}
				},
				"pnlProductBodyMiddle1": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle1",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"width": "100%"
					},
					"parent": "pnlProductBodyMiddle"
				},
				"pnlProductBodyMiddle2": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle2",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"overflow": "auto",
						"width": "100%"
					},
					"parent": "pnlProductBodyMiddle"
				},
				"pnlProductBodyMiddle3": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle3",
						"componentVisible": true
					},
					"style": {
						"display": "none",
						"verticalAlign": "top",
						"width": "100%"
					},
					"parent": "pnlProductBodyMiddle"
				},
				"Image9": {
					"property": {
						"componentType": "image",
						"componentName": "Image9",
						"componentValue": require("../../asset/images/icon-go-gray (1).png"),
						"zoom": false,
						"componentVisible": true
					},
					"style": {
						"width": "30px"
					},
					"parent": "Panel822"
				},
				"pnlProductBody": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBody",
						"componentVisible": true
					},
					"style": {
						"background": "url() no-repeat fixed",
						"backgroundColor": "rgb(255, 255, 255)",
						"borderRadius": "5px",
						"padding": "10px",
						"display": "table",
						"boxShadow": "-1px 1px 1px #545454",
						"border": "1px solid #3C3C3B",
						"width": "100%"
					},
					"parent": "pnlProduct"
				},
				"lblDocNumber_data": {
					"property": {
						"componentType": "label",
						"componentName": "lblDocNumber_data",
						"idPrefix": "num",
						"field": "No",
						"idField": "No",
						"componentVisible": true
					},
					"style": {
						"fontSize": "24px",
						"fontWeight": "bold",
						"display": "block",
						"color": "rgb(60, 60, 59)"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"lblDocumentDateAAA": {
					"property": {
						"componentType": "label",
						"componentName": "lblDocumentDateAAA",
						"componentValue": "Doc. Date:",
						"componentVisible": true
					},
					"style": {
						"textAlign": "left",
						"fontSize": "16px",
						"fontWeight": "normal",
						"display": "none",
						"width": "30%",
						"color": "rgb(111, 111, 110)"
					},
					"parent": "pnlProductBodyMiddle4141161"
				},
				"pnlProductBodyMiddle5": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle5",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"width": "100%",
						"marginTop": "10px"
					},
					"parent": "pnlProductBodyMiddle"
				},
				"lblDocumentDate_data4": {
					"property": {
						"componentType": "label",
						"componentName": "lblDocumentDate_data4",
						"componentVisible": true
					},
					"style": {
						"paddingTop": "2px",
						"display": "block",
						"color": "rgb(60, 60, 59)",
						"fontWeight": "normal",
						"fontSize": "16px"
					},
					"parent": "pnlProductBodyMiddle4141161"
				},
				"pnlProduct": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProduct",
						"componentVisible": true
					},
					"style": {
						"width": "100%",
						"paddingBottom": "0px",
						"textAlign": "left",
						"backgroundColor": "rgb(255, 255, 255)",
						"padding": "5px"
					}
				},
				"lblDocNumber1": {
					"style": {
						"display": "none",
						"width": "30%",
						"fontWeight": "normal",
						"textAlign": "left",
						"color": "rgb(111, 111, 110)",
						"fontSize": "16px"
					},
					"parent": "pnlProductBodyMiddle1",
					"property": {
						"componentType": "label",
						"componentName": "lblDocNumber1",
						"componentValue": "Doc. No:",
						"componentVisible": true
					}
				},
				"lblCustomer_data": {
					"property": {
						"componentType": "label",
						"componentName": "lblCustomer_data",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(60, 60, 59)",
						"display": "block",
						"fontSize": "18px",
						"fontWeight": "normal"
					},
					"parent": "pnlProductBodyMiddle2"
				},
				"lblOulet1": {
					"property": {
						"componentType": "label",
						"componentName": "lblOulet1",
						"componentValue": "Cust.Name:",
						"componentVisible": true
					},
					"style": {
						"display": "none",
						"color": "rgb(111, 111, 110)",
						"width": "30%",
						"fontSize": "16px",
						"fontWeight": "normal",
						"textAlign": "left"
					},
					"parent": "pnlProductBodyMiddle3"
				},
				"lblCustomer1": {
					"property": {
						"componentType": "label",
						"componentName": "lblCustomer1",
						"componentValue": "Cust. Code:",
						"componentVisible": true
					},
					"style": {
						"width": "30%",
						"color": "rgb(111, 111, 110)",
						"fontSize": "16px",
						"fontWeight": "normal",
						"display": "none",
						"float": "left",
						"textAlign": "left"
					},
					"parent": "pnlProductBodyMiddle2"
				},
				"pnlProductBodyMiddle4141161": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle4141161",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"width": "100%"
					},
					"parent": "pnlProductBodyMiddle"
				},
				"Panel822": {
					"parent": "pnlProductBody",
					"property": {
						"componentType": "panel",
						"componentName": "Panel822",
						"componentVisible": true
					},
					"style": {
						"display": "table-cell",
						"verticalAlign": "middle",
						"width": "40px",
						"textAlign": "center"
					}
				}
			}
		}
	},
	"txtOutletCode": {
		"style": {
			"border": "1px solid #D9D9D9",
			"padding": "0px",
			"margin": "0px",
			"width": "100%",
			"height": "30px",
			"fontSize": "16px"
		},
		"parent": "pnlMainBodyForm1Input180830",
		"property": {
			"componentType": "edit",
			"componentName": "txtOutletCode",
			"placeHolder": "Outlet Code",
			"type": "text",
			"disabled": true,
			"componentVisible": true
		}
	},
	"pnlMainBodyForm2Input": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm2Input",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyForm2"
	},
	"pnlUpdateCartModalBody5": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBody5",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"marginBottom": "10px",
			"marginTop": "20px"
		},
		"parent": "pnlUpdateCartModalBody"
	},
	"lblModalTitle": {
		"parent": "pnlModalBodyTitle",
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "20px",
			"fontWeight": "bold"
		}
	},
	"pnlMainHeaderLeft": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeft",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"imgBack": {
		"property": {
			"componentType": "image",
			"componentName": "imgBack",
			"componentValue": require("../../asset/icon/icon-menu.png"),
			"componentVisible": true
		},
		"style": {
			"height": "35px",
			"width": "35px"
		},
		"parent": "pnlMainHeaderLeftCell"
	},
	"pnlMainBodyForm1Label551333": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm1Label551333",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyForm1481"
	},
	"lblSelectOutletCode": {
		"property": {
			"componentType": "label",
			"componentName": "lblSelectOutletCode",
			"componentValue": "SELECT",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"textAlign": "right",
			"width": "30%",
			"fontWeight": "bold"
		},
		"parent": "pnlMainBodyForm1Label551333"
	},
	"txtDocumentDateTo": {
		"property": {
			"componentType": "edit",
			"componentName": "txtDocumentDateTo",
			"type": "date",
			"disabled": false,
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgba(255, 255, 255, 0.98)",
			"border": "1px solid #D9D9D9",
			"color": "rgb(0, 0, 0)",
			"width": "45%",
			"height": "30px",
			"fontSize": "16px",
			"margin": "0px",
			"display": "inline-block",
			"padding": "0px"
		},
		"parent": "pnlMainBodyForm2Input739198"
	},
	"pnlMainTabHeader": {
		"parent": "pnlMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainTabHeader",
			"componentVisible": true
		},
		"style": {
			"display": "none",
			"width": "100%",
			"height": "40px",
			"padding": "2% 2% 2% 2%"
		}
	},
	"pnlBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBody",
			"componentVisible": true
		},
		"style": {
			"height": "100%"
		},
		"parent": "pnlMainBody"
	},
	"imgUpdateCartClose": {
		"property": {
			"componentType": "image",
			"componentName": "imgUpdateCartClose",
			"componentValue": require("../../asset/icon/icon-close-gray.png"),
			"componentVisible": true
		},
		"style": {
			"height": "45px",
			"position": "absolute",
			"right": "0px",
			"width": "45px"
		},
		"parent": "pnlUpdateCartModalBody1"
	},
	"pnlMainBodyForm3": {
		"parent": "pnlUpdateCartModalBody2",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm3",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"marginBottom": "10px"
		}
	},
	"pnlMainBodyForm1Label554794": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm1Label554794",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyForm3"
	},
	"btnSearch": {
		"property": {
			"componentType": "fbutton",
			"componentName": "btnSearch",
			"componentValue": "Search",
			"componentVisible": true
		},
		"style": {
			"padding": "8px",
			"width": "80%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"backgroundColor": "rgb(242, 114, 34)",
			"fontWeight": "bold",
			"margin": "0px"
		},
		"parent": "pnlUpdateCartModalBody5"
	},
	"pnlCSS": {
		"parent": "pnlHolder",
		"property": {
			"componentType": "panel",
			"componentName": "pnlCSS",
			"componentVisible": false
		},
		"style": {
			"top": "0px",
			"position": "fixed",
			"display": "none",
			"width": "100%",
			"height": "100%",
			"zIndex": "5"
		}
	},
	"pnlMainSearch": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainSearch",
			"componentVisible": true
		},
		"style": {
			"height": "50px",
			"borderBottom": "1px solid #D9D9D9",
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlMain"
	},
	"pnlMainBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		},
		"style": {
			"height": "calc(100% - 100px - 15px)",
			"overflow": "auto",
			"width": "100%"
		},
		"parent": "pnlMain"
	},
	"imgUpdateCartImage": {
		"property": {
			"componentType": "image",
			"componentName": "imgUpdateCartImage",
			"componentValue": require("../../asset/images/default.png"),
			"componentVisible": true
		},
		"style": {
			"width": "150px",
			"display": "none"
		},
		"parent": "pnlUpdateCartModalBody1"
	},
	"lblSelectCustomer": {
		"property": {
			"componentType": "label",
			"componentName": "lblSelectCustomer",
			"componentValue": "SELECT",
			"componentVisible": true
		},
		"style": {
			"width": "30%",
			"textAlign": "right",
			"fontSize": "16px",
			"fontWeight": "bold",
			"color": "rgb(60, 60, 59)"
		},
		"parent": "pnlMainBodyForm1Label"
	},
	"lblDocumentDateTo851": {
		"property": {
			"componentType": "label",
			"componentName": "lblDocumentDateTo851",
			"componentValue": "To",
			"componentVisible": true
		},
		"style": {
			"width": "10%",
			"fontWeight": "normal",
			"display": "inline-block",
			"color": "rgb(60, 60, 59)",
			"fontSize": "12px",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyForm2Input739198"
	},
	"pnlMainBodyForm1Input696752": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm1Input696752",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"width": "100%"
		},
		"parent": "pnlMainBodyForm3"
	},
	"pnlModalBodyTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlModalBodyButtons": {
		"style": {
			"background": "#ffffff",
			"marginRight": "5%",
			"textAlign": "center",
			"marginLeft": "5%",
			"width": "90%"
		},
		"parent": "pnlModalCell",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"addClass": "hide",
			"componentVisible": true
		}
	},
	"dlDraft938": {
		"property": {
			"componentType": "datalist",
			"componentName": "dlDraft938",
			"componentValue": "l_salesDocument",
			"autoLoad": false,
			"componentData": [],
			"componentVisible": true
		},
		"style": {
			"paddingLeft": "10px",
			"paddingRight": "10px"
		},
		"parent": "pnlMainTabApproved",
		"template": {
			"componentDataItemState": {
				"pnlProductBodyMiddle196": {
					"style": {
						"display": "block",
						"width": "100%"
					},
					"parent": "pnlProductBodyMiddle331",
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle196",
						"componentVisible": true
					}
				},
				"lblDocNumber1450": {
					"style": {
						"width": "30%",
						"color": "rgb(111, 111, 110)",
						"fontSize": "16px",
						"fontWeight": "normal",
						"textAlign": "left"
					},
					"parent": "pnlProductBodyMiddle196",
					"property": {
						"componentType": "label",
						"componentName": "lblDocNumber1450",
						"componentValue": "Doc. Number:",
						"componentVisible": true
					}
				},
				"lblDocNumber_data680": {
					"property": {
						"componentType": "label",
						"componentName": "lblDocNumber_data680",
						"field": "docNUM",
						"componentVisible": true
					},
					"style": {
						"fontWeight": "normal",
						"textAlign": "right",
						"width": "70%",
						"fontSize": "16px",
						"color": "rgb(111, 111, 110)"
					},
					"parent": "pnlProductBodyMiddle196"
				},
				"lblDocTotal1390": {
					"property": {
						"componentType": "label",
						"componentName": "lblDocTotal1390",
						"componentValue": "Doc. Total:",
						"componentVisible": true
					},
					"style": {
						"width": "30%",
						"fontWeight": "normal",
						"textAlign": "left",
						"color": "rgb(111, 111, 110)",
						"fontSize": "16px"
					},
					"parent": "pnlProductBodyMiddle59"
				},
				"pnlProduct88": {
					"style": {
						"textAlign": "left",
						"backgroundColor": "rgb(255, 255, 255)",
						"width": "100%",
						"borderBottom": "1px solid #D9D9D9",
						"position": "relative"
					},
					"property": {
						"componentType": "panel",
						"componentName": "pnlProduct88",
						"componentVisible": true
					}
				},
				"pnlProductBodyMiddle2510": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle2510",
						"componentVisible": true
					},
					"style": {
						"width": "100%",
						"display": "block",
						"overflow": "auto"
					},
					"parent": "pnlProductBodyMiddle331"
				},
				"lblOulet_data91": {
					"parent": "pnlProductBodyMiddle3261",
					"property": {
						"componentType": "label",
						"componentName": "lblOulet_data91",
						"field": "name",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(111, 111, 110)",
						"textAlign": "right",
						"width": "70%",
						"fontSize": "16px",
						"fontWeight": "normal"
					}
				},
				"pnlProductBodyMiddle4141161528": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle4141161528",
						"componentVisible": true
					},
					"style": {
						"width": "100%",
						"display": "block"
					},
					"parent": "pnlProductBodyMiddle331"
				},
				"lblDocumentDate_data451308978962567": {
					"property": {
						"componentType": "label",
						"componentName": "lblDocumentDate_data451308978962567",
						"componentValue": "29/09/2018",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(111, 111, 110)",
						"fontSize": "16px",
						"fontWeight": "normal",
						"textAlign": "right",
						"width": "70%"
					},
					"parent": "pnlProductBodyMiddle4141161528"
				},
				"lblDocTotal_data861": {
					"style": {
						"textAlign": "right",
						"fontSize": "16px",
						"fontWeight": "normal",
						"width": "70%",
						"color": "rgb(111, 111, 110)"
					},
					"parent": "pnlProductBodyMiddle59",
					"property": {
						"componentType": "label",
						"componentName": "lblDocTotal_data861",
						"componentVisible": true
					}
				},
				"pnlProductBodyMiddle331": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle331",
						"componentVisible": true
					},
					"style": {
						"float": "left",
						"width": "100%",
						"textAlign": "left"
					},
					"parent": "pnlProductBody369"
				},
				"lblCustomer1140": {
					"style": {
						"color": "rgb(111, 111, 110)",
						"textAlign": "left",
						"width": "30%",
						"float": "left",
						"fontSize": "16px",
						"fontWeight": "normal"
					},
					"parent": "pnlProductBodyMiddle2510",
					"property": {
						"componentType": "label",
						"componentName": "lblCustomer1140",
						"componentValue": "Cust. Code:",
						"componentVisible": true
					}
				},
				"lblCustomer_data697": {
					"property": {
						"componentType": "label",
						"componentName": "lblCustomer_data697",
						"field": "code",
						"componentVisible": true
					},
					"style": {
						"width": "70%",
						"color": "rgb(111, 111, 110)",
						"fontWeight": "normal",
						"float": "right",
						"fontSize": "16px",
						"textAlign": "right"
					},
					"parent": "pnlProductBodyMiddle2510"
				},
				"lblOulet1902": {
					"parent": "pnlProductBodyMiddle3261",
					"property": {
						"componentType": "label",
						"componentName": "lblOulet1902",
						"componentValue": "Cust. Name:",
						"componentVisible": true
					},
					"style": {
						"width": "30%",
						"fontSize": "16px",
						"textAlign": "left",
						"color": "rgb(111, 111, 110)",
						"fontWeight": "normal"
					}
				},
				"lblDocumentDateAAA867": {
					"property": {
						"componentType": "label",
						"componentName": "lblDocumentDateAAA867",
						"componentValue": "Doc. Date",
						"componentVisible": true
					},
					"style": {
						"width": "30%",
						"fontSize": "16px",
						"color": "rgb(111, 111, 110)",
						"fontWeight": "normal",
						"textAlign": "left"
					},
					"parent": "pnlProductBodyMiddle4141161528"
				},
				"pnlProductBodyMiddle59": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle59",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"width": "100%"
					},
					"parent": "pnlProductBodyMiddle331"
				},
				"pnlProductBody369": {
					"style": {
						"padding": "5px",
						"width": "100%",
						"background": "url() no-repeat fixed",
						"overflow": "auto"
					},
					"parent": "pnlProduct88",
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBody369",
						"componentVisible": true
					}
				},
				"pnlProductBodyMiddle3261": {
					"parent": "pnlProductBodyMiddle331",
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle3261",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"width": "100%"
					}
				}
			}
		}
	},
	"pnlUpdateCartModal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModal",
			"componentVisible": false
		},
		"style": {
			"zIndex": "1",
			"background": "rgba(0, 0, 0, 0.60)",
			"top": "0px",
			"width": "100%",
			"height": "100%",
			"position": "fixed"
		},
		"parent": "pnlHolder"
	},
	"pnlMainHeaderRightCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRightTable"
	},
	"pnlMainBodyForm1Input": {
		"parent": "pnlMainBodyForm1",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm1Input",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center"
		}
	},
	"txtCustomerName": {
		"property": {
			"componentType": "edit",
			"componentName": "txtCustomerName",
			"disabled": true,
			"placeHolder": "Customer Name",
			"type": "text",
			"componentVisible": true
		},
		"style": {
			"height": "30px",
			"width": "100%",
			"fontSize": "16px",
			"border": "1px solid #D9D9D9",
			"padding": "0px",
			"margin": "0px"
		},
		"parent": "pnlMainBodyForm1Input"
	},
	"pnlMainBodyForm1481": {
		"parent": "pnlUpdateCartModalBody2",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm1481",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"marginBottom": "10px",
			"width": "100%"
		}
	},
	"pnlMainBodyForm2": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm2",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"marginBottom": "10px"
		},
		"parent": "pnlUpdateCartModalBody2"
	},
	"pnlModalCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModalTable"
	},
	"pgOrderList": {
		"property": {
			"componentType": "page",
			"componentName": "pgOrderList",
			"title": "OrderList",
			"componentVisible": true
		}
	},
	"pnlMainHeaderRight": {
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRight",
			"componentVisible": true
		}
	},
	"pnlModalBodyLoading": {
		"parent": "pnlModalBodyMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		}
	},
	"imgModalLoading": {
		"style": {
			"width": "50px",
			"height": "10px"
		},
		"parent": "pnlModalBodyLoading",
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		}
	},
	"pnlMainBodyForm2Label820995": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm2Label820995",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyForm2498"
	},
	"lblModalPositive": {
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonPositive",
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		}
	},
	"pnlUpdateCartModalBody1": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBody1",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "15px",
			"marginBottom": "10px",
			"position": "relative"
		},
		"parent": "pnlUpdateCartModalBody"
	},
	"txtDateFrom": {
		"property": {
			"componentType": "edit",
			"componentName": "txtDateFrom",
			"type": "date",
			"disabled": false,
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgba(255, 255, 255, 0.98)",
			"margin": "0px",
			"display": "inline-block",
			"width": "45%",
			"fontSize": "16px",
			"border": "1px solid #D9D9D9",
			"color": "rgb(0, 0, 0)",
			"padding": "0px",
			"height": "30px"
		},
		"parent": "pnlMainBodyForm2Input"
	},
	"pnlUpdateCartModalTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlUpdateCartModal"
	},
	"lblDocumentDateTo": {
		"style": {
			"fontWeight": "normal",
			"textAlign": "center",
			"display": "inline-block",
			"width": "10%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "12px"
		},
		"parent": "pnlMainBodyForm2Input",
		"property": {
			"componentType": "label",
			"componentName": "lblDocumentDateTo",
			"componentValue": "To",
			"componentVisible": true
		}
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "50%",
			"textAlign": "center",
			"padding": "10px 0px",
			"display": "inline-block",
			"float": "none"
		},
		"parent": "pnlModalBodyButtons"
	},
	"lblMainTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle",
			"componentValue": "ORDER LIST",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500"
		},
		"parent": "pnlMainHeaderMiddleCell"
	},
	"txtSearch": {
		"property": {
			"componentType": "edit",
			"componentName": "txtSearch",
			"type": "search",
			"backgroundAsset": "icon/_search.png",
			"placeHolder": "Search",
			"componentVisible": true
		},
		"style": {
			"background": "url('data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Capa_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 451 451\" style=\"enable-background:new 0 0 451 451;\" xml:space=\"preserve\" width=\"512px\" height=\"512px\"><g><path d=\"M447.05,428l-109.6-109.6c29.4-33.8,47.2-77.9,47.2-126.1C384.65,86.2,298.35,0,192.35,0C86.25,0,0.05,86.3,0.05,192.3   s86.3,192.3,192.3,192.3c48.2,0,92.3-17.8,126.1-47.2L428.05,447c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4   C452.25,441.8,452.25,433.2,447.05,428z M26.95,192.3c0-91.2,74.2-165.3,165.3-165.3c91.2,0,165.3,74.2,165.3,165.3   s-74.1,165.4-165.3,165.4C101.15,357.7,26.95,283.5,26.95,192.3z\" fill=\"#3c3c3b\"/></g></svg>') no-repeat",
			"paddingLeft": "30px",
			"color": "rgb(60, 60, 59)",
			"backgroundPosition": "left .5rem center",
			"backgroundSize": "16px",
			"backgroundColor": "rgb(255, 255, 255)",
			"border": "1px solid #D9D9D9",
			"fontSize": "16px",
			"textAlign": "left",
			"margin": "0px",
			"display": "inline",
			"width": "100%",
			"height": "40px"
		},
		"parent": "pnlMainBodySearch"
	},
	"lblStyle": {
		"property": {
			"componentType": "label",
			"componentName": "lblStyle",
			"componentValue": "N/A",
			"componentVisible": true
		},
		"parent": "pnlCSS"
	},
	"pnlMainHeaderMiddleCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleCell",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlMainHeaderMiddleTable"
	},
	"pnlMainBodyForm1": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm1",
			"componentVisible": true
		},
		"style": {
			"marginBottom": "10px",
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlUpdateCartModalBody2"
	}
};
