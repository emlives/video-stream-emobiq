export const StateName = 'pgSplash';
export const State = {
	"Panel308": {
		"property": {
			"componentUUID": "qGiW1DgUS9ZsdjevnFAOF",
			"componentType": "panel",
			"componentName": "Panel308",
			"componentVisible": true
		},
		"style": {
			"alignItems": "center",
			"minWidth": "40px",
			"justifyContent": "center",
			"minHeight": "50px",
			"backgroundColor": "rgb(59, 55, 45)",
			"left": "0px",
			"width": "100%"
		},
		"parent": "Panel81"
	},
	"lblLicensed376": {
		"style": {
			"color": "rgb(255, 255, 255)",
			"display": "block",
			"fontSize": "11px"
		},
		"parent": "Panel308",
		"property": {
			"componentUUID": "v5oflB4zGDP1T90ZqiPyW",
			"componentType": "label",
			"componentName": "lblLicensed376",
			"componentVisible": true,
			"componentFunction": "getLblLicensed376Value"
		}
	},
	"Image525": {
		"property": {
			"componentUUID": "_hBL9DMkQfsMKVxh6WHnZ",
			"componentType": "image",
			"componentName": "Image525",
			"componentValue": require("../../asset/images/logo-emobiq-power.png"),
			"zoom": false,
			"componentVisible": true
		},
		"style": {
			"alignSelf": "flex-end",
			"width": "50%"
		},
		"parent": "Panel607"
	},
	"pgSplash": {
		"property": {
			"componentUUID": "rDPwOeKu0Sm5ZhMj338Je",
			"componentType": "page",
			"componentName": "pgSplash",
			"title": "Splash",
			"componentVisible": true
		}
	},
	"Panel118": {
		"property": {
			"componentUUID": "oSg7X1VrCYE-GdXywWNLX",
			"componentType": "panel",
			"componentName": "Panel118",
			"componentVisible": true
		},
		"style": {
			"justifyContent": "center",
			"left": "0px",
			"alignItems": "center",
			"flexGrow": "1",
			"width": "100%"
		},
		"parent": "Panel81"
	},
	"Image246": {
		"property": {
			"componentUUID": "1_Z5jODj9kIBu6qFl6vzq",
			"componentType": "image",
			"componentName": "Image246",
			"componentValue": require("../../asset/images/OK365-Dark.png"),
			"zoom": false,
			"componentVisible": true
		},
		"style": {
			"width": "70%"
		},
		"parent": "Panel118"
	},
	"Label55660": {
		"property": {
			"componentUUID": "PzNLYmj773QrwweYeCQQP",
			"componentType": "label",
			"componentName": "Label55660",
			"componentVisible": true
		},
		"style": {
			"fontSize": "14px"
		},
		"parent": "Panel118"
	},
	"Panel607": {
		"property": {
			"componentUUID": "t6Yho8e4vijrQiIImJIp9",
			"componentType": "panel",
			"componentName": "Panel607",
			"componentVisible": true
		},
		"style": {
			"left": "0px",
			"backgroundColor": "rgb(237, 70, 47)",
			"width": "100%"
		},
		"parent": "Panel81"
	},
	"Panel81": {
		"property": {
			"componentUUID": "yLMth0ez9mSBHqCopBFis",
			"componentType": "panel",
			"componentName": "Panel81",
			"componentVisible": true
		},
		"style": {
			"left": "0px",
			"height": "100%",
			"backgroundColor": "rgb(255, 255, 255)",
			"width": "100%"
		},
		"parent": "pgSplash"
	}
};
