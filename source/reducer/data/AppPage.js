export const StateName = 'AppPage';
export const State = {
    lblResult: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'label',
            componentValue: 'Result',
            componentVisible: true,
            componentUUID: 'lblResult',
            componentName: 'lblResult'
        },
        attribute: {
            "caption": "Result",
        },
        elementAttribute: {
            
        }
    },
    btnInactivityTimeout: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Inactivity Timeout',
            componentVisible: true,
            componentUUID: 'btnInactivityTimeout',
            componentName: 'btnInactivityTimeout'
        },
        attribute: {
            "caption": "Inactivity Timeout",
        },
        elementAttribute: {
            
        }
    },
    btnGoToArrayPage: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Go To Array Page',
            componentVisible: true,
            componentUUID: 'btnGoToArrayPage',
            componentName: 'btnGoToArrayPage'
        },
        attribute: {
            "caption": "Go To Array Page",
        },
        elementAttribute: {
            
        }
    },
    btnGoToStylePage: {
        componentType: 'button',
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Go To Style Page',
            componentVisible: true,
            componentUUID: 'btnGoToStylePage',
            componentName: 'btnGoToStylePage'
        },
        attribute: {
            "caption": "Go To Style Page",
        },
        elementAttribute: {
            
        }
    },
    btnGoToCssPage: {
        componentType: 'button',
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Go To CSS Page',
            componentVisible: true,
            componentUUID: 'btnGoToCssPage',
            componentName: 'btnGoToCssPage'
        },
        attribute: {
            "caption": "Go To CSS Page",
        },
        elementAttribute: {
            
        }
    },
    btnGoToSamplePage: {
        componentType: 'button',
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Go To Sample Page',
            componentVisible: true,
            componentUUID: 'btnGoToSamplePage',
            componentName: 'btnGoToSamplePage'
        },
        attribute: {
            "caption": "Go To Sample Page'",
        },
        elementAttribute: {
            
        }
    },
    btnGoToOrderHistoryDetails: {
        componentType: 'button',
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Go To Order History Details Page',
            componentVisible: true,
            componentUUID: 'btnGoToOrderHistoryDetails',
            componentName: 'btnGoToOrderHistoryDetails'
        },
        attribute: {
            "caption": "Go To Order History Details Page",
        },
        elementAttribute: {
            
        }
    },
    editFocus: {
        style: {},
        property: {
            componentType: 'edit',
            componentName: 'editFocus',
            componentValue: 'Enter Text Here!',
            componentUUID: 'editFocus',
            componentName: 'editFocus'
        },
        attribute: {
            "caption": "Enter Text Here!",
        },
        elementAttribute: {
            
        }
    },
    btnSetComponentFocus: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Set Component Focus',
            componentVisible: true,
            componentUUID: 'btnSetComponentFocus',
            componentName: 'btnSetComponentFocus'
        },
        attribute: {
            "caption": "Set Component Focus",
        },
        elementAttribute: {
            
        }
    },
    btnBBCodeToCanvas: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'BB Code To Canvas',
            componentVisible: true,
            componentUUID: 'btnBBCodeToCanvas',
            componentName: 'btnBBCodeToCanvas'
        },
        attribute: {
            "caption": "BB Code To Canvas",
        },
        elementAttribute: {
            
        }
    },
    btnBBCodeToCanvasSync: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'BB Code To Canvas Sync',
            componentVisible: true,
            componentUUID: 'btnBBCodeToCanvasSync',
            componentName: 'btnBBCodeToCanvasSync'
        },
        attribute: {
            "caption": "BB Code To Canvas Sync",
        },
        elementAttribute: {
            
        }
    },
}