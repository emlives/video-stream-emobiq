export const StateName = 'pgOrderDetails';
export const State = {
	"pnlModalBodyButtons": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"background": "#ffffff",
			"marginLeft": "5%",
			"width": "90%",
			"textAlign": "center",
			"marginRight": "5%"
		},
		"parent": "pnlModalCell"
	},
	"pnlUpdateCartModalTableOrderDetails": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalTableOrderDetails",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlUpdateCartModalOrderDetails"
	},
	"lblUpdateCartItemCodeValueOD": {
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateCartItemCodeValueOD",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"display": "block",
			"color": "rgb(60, 60, 59)",
			"fontSize": "24px",
			"textAlign": "left"
		},
		"parent": "pnlUpdateCartModalBodyOrderDetailsCode"
	},
	"pnlUpdateCartModalBodyOrderDetailsQty": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyOrderDetailsQty",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "Panel293"
	},
	"pnlMainBodyForm": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"backgroundColor": "rgb(84, 84, 84)",
			"padding": "10px"
		},
		"parent": "pnlMainBody1"
	},
	"lblOrderNo": {
		"property": {
			"componentType": "label",
			"componentName": "lblOrderNo",
			"field": "name",
			"componentVisible": true
		},
		"style": {
			"fontSize": "24px",
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"fontWeight": "normal",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyForm"
	},
	"pnlMainBodyForm3Input77725": {
		"parent": "pnlMainBodyFormOrderDateOrderDetails",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm3Input77725",
			"componentVisible": true
		},
		"style": {
			"display": "none",
			"width": "100%",
			"textAlign": "center"
		}
	},
	"pnlMainBodyDetailsTotalAmt": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyDetailsTotalAmt",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "40px",
			"backgroundColor": "rgb(84, 84, 84)",
			"padding": "5px"
		},
		"parent": "pnlMain"
	},
	"pnlUpdateCartModalBodyLeft": {
		"style": {
			"verticalAlign": "middle",
			"width": "50px",
			"height": "100%",
			"backgroundColor": "rgb(255, 69, 0)",
			"display": "table-cell",
			"textAlign": "center",
			"border": "1px solid #FFF"
		},
		"parent": "pnlUpdateCartModalBodyQtyValueOD",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyLeft",
			"componentVisible": true
		}
	},
	"pnlMainFooter": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooter",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgb(255, 69, 0)",
			"width": "100%",
			"height": "50px"
		},
		"parent": "pnlMain"
	},
	"pnlModal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"zIndex": "3",
			"height": "100%",
			"top": "0px",
			"width": "100%",
			"background": "rgba(0, 0, 0, 0.60)"
		},
		"parent": "pnlHolder"
	},
	"pnlModalBodyButtonNegative": {
		"style": {
			"float": "none",
			"width": "50%",
			"textAlign": "center",
			"padding": "10px 0px",
			"display": "inline-block"
		},
		"parent": "pnlModalBodyButtons",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		}
	},
	"imgOrderDetailsClose": {
		"property": {
			"componentType": "image",
			"componentName": "imgOrderDetailsClose",
			"componentValue": require("../../asset/icon/icon-close-gray.png"),
			"componentVisible": true
		},
		"style": {
			"top": "5px",
			"right": "5px",
			"width": "25px",
			"position": "absolute",
			"height": "25px"
		},
		"parent": "pnlUpdateCartModalBodyOrderDetailsImage"
	},
	"pnlMainBody1": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody1",
			"componentVisible": true
		},
		"style": {
			"height": "calc(100% - 50px - 50px - 40px - 15px)",
			"padding": "5px20px",
			"overflow": "auto",
			"width": "100%"
		},
		"parent": "pnlMain"
	},
	"pnlMainBodyFormLabelCustomerCode": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyFormLabelCustomerCode",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyFormCustomerCodeOrderDetails"
	},
	"txtOrderDateOrderDetails": {
		"property": {
			"componentType": "edit",
			"componentName": "txtOrderDateOrderDetails",
			"disabled": true,
			"type": "date",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"margin": "0px",
			"height": "50px",
			"fontSize": "16px",
			"border": "1px solid #D9D9D9",
			"padding": "0px"
		},
		"parent": "pnlMainBodyForm3Input77725"
	},
	"pnlMainBodyItems1": {
		"style": {
			"width": "100%",
			"backgroundColor": "rgb(135, 135, 135)"
		},
		"parent": "pnlMainBody1",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyItems1",
			"componentVisible": true
		}
	},
	"btnUpdate": {
		"property": {
			"componentType": "fbutton",
			"componentName": "btnUpdate",
			"componentValue": "UPDATE",
			"componentVisible": true
		},
		"style": {
			"height": "40px",
			"fontSize": "20px",
			"backgroundColor": "rgb(255, 69, 0)",
			"margin": "0px",
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontWeight": "bold",
			"padding": "8px"
		},
		"parent": "pnlUpdateCartModalBodyButtonOrderDetails"
	},
	"pnlMainBodyForm3Input": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm3Input",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyFormInvoiceDateOrderDetails"
	},
	"lblItems": {
		"property": {
			"componentType": "label",
			"componentName": "lblItems",
			"componentValue": "DETAILS",
			"componentVisible": true
		},
		"style": {
			"paddingBottom": "5px",
			"width": "calc(100% - 30px)",
			"paddingTop": "5px",
			"color": "rgb(255, 215, 0)",
			"fontSize": "16px",
			"fontWeight": "bold",
			"padding": "10px"
		},
		"parent": "pnlMainBodyItems1"
	},
	"pnlModalCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModalTable"
	},
	"lblModalTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"fontWeight": "bold",
			"width": "100%",
			"color": "rgb(60, 60, 59)"
		},
		"parent": "pnlModalBodyTitle"
	},
	"imgBack": {
		"property": {
			"componentType": "image",
			"componentName": "imgBack",
			"componentValue": require("../../asset/icon/icon-back.png"),
			"componentVisible": true
		},
		"style": {
			"height": "35px",
			"width": "35px"
		},
		"parent": "pnlMainHeaderLeftCell"
	},
	"lblMainTitle": {
		"parent": "pnlMainHeaderMiddleCell",
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle",
			"componentValue": "ORDER DETAILS",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500"
		}
	},
	"pnlMainBodyForm3LabelCustomerNameOrderDetails": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm3LabelCustomerNameOrderDetails",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyFormCustomerNameOrderDetails"
	},
	"pnlMainBodyForm3InputValueOrderDetails": {
		"style": {
			"width": "100%",
			"textAlign": "center",
			"display": "none"
		},
		"parent": "pnlMainBodyFormCustomerNameOrderDetails",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm3InputValueOrderDetails",
			"componentVisible": true
		}
	},
	"pnlUpdateCartModalBodyOrderDetailsCode": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyOrderDetailsCode",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"width": "100%"
		},
		"parent": "Panel964"
	},
	"pnlModalBodyMessage": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"lblOrderDetailsPriceValueOD": {
		"property": {
			"componentType": "label",
			"componentName": "lblOrderDetailsPriceValueOD",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(0, 153, 0)",
			"textAlign": "left",
			"display": "block",
			"fontSize": "20px",
			"fontWeight": "bold"
		},
		"parent": "pnlUpdateCartModalBodyOrderDetailsPrice"
	},
	"pnlMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"zIndex": "0",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlMainHeaderLeft": {
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeft",
			"componentVisible": true
		}
	},
	"pnlTotalAmountTableCell1": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlTotalAmountTableCell1",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"textAlign": "left"
		},
		"parent": "pnlTotalAmountTable"
	},
	"pnlModalBodyButtonPositive": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"width": "50%",
			"padding": "10px 0px",
			"float": "none",
			"textAlign": "center"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlUpdateCartModalBodyOrderDetailsPrice": {
		"style": {
			"width": "100%",
			"marginTop": "20px"
		},
		"parent": "Panel964",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyOrderDetailsPrice",
			"componentVisible": true
		}
	},
	"lblStyle": {
		"property": {
			"componentType": "label",
			"componentName": "lblStyle",
			"componentValue": "N/A",
			"componentVisible": true
		},
		"parent": "pnlCSS"
	},
	"pnlMainHeaderRightTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRight"
	},
	"pnlTotalAmountTableCell2": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlTotalAmountTableCell2",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"textAlign": "right"
		},
		"parent": "pnlTotalAmountTable"
	},
	"pnlModalBodyTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlUpdateCartModalBodyOrderDetailsName": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyOrderDetailsName",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"position": "relative"
		},
		"parent": "Panel964"
	},
	"pnlHolder": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		},
		"style": {
			"overflow": "auto",
			"width": "100%",
			"height": "100%",
			"backgroundColor": "rgb(245, 245, 245)"
		},
		"parent": "pgOrderDetails"
	},
	"pnlMainHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"backgroundColor": "rgb(255, 69, 0)",
			"overflow": "auto",
			"height": "65px",
			"width": "100%",
			"paddingTop": "15px"
		},
		"parent": "pnlMain"
	},
	"pnlMainHeaderRight": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRight",
			"componentVisible": true
		},
		"style": {
			"width": "15%",
			"height": "100%",
			"float": "left"
		},
		"parent": "pnlMainHeader"
	},
	"lblModalPositive": {
		"parent": "pnlModalBodyButtonPositive",
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		}
	},
	"imgMinus": {
		"property": {
			"componentType": "image",
			"componentName": "imgMinus",
			"componentValue": require("../../asset/minus-white.png"),
			"componentVisible": true
		},
		"style": {
			"width": "30px",
			"height": "30px"
		},
		"parent": "pnlUpdateCartModalBodyLeft"
	},
	"txtQuantityOderDetails": {
		"property": {
			"componentType": "edit",
			"componentName": "txtQuantityOderDetails",
			"type": "number",
			"componentValue": "0",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"border": "1px solid #D9D9D9",
			"width": "100%",
			"height": "40px",
			"color": "rgb(33, 33, 33)",
			"fontSize": "24px",
			"fontWeight": "normal"
		},
		"parent": "pnlUpdateCartModalBodyMiddle"
	},
	"pnlMainHeaderMiddle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddle",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "70%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlTotalAmountTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlTotalAmountTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainBodyDetailsTotalAmt"
	},
	"lblModalMessage": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		},
		"style": {
			"fontSize": "16px",
			"width": "100%",
			"color": "rgb(111, 111, 110)"
		},
		"parent": "pnlModalBodyMessage"
	},
	"pnlUpdateCartModalCellOrderDetails": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalCellOrderDetails",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlUpdateCartModalTableOrderDetails"
	},
	"pnlModalBodyLoading": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	},
	"imgModalLoading": {
		"parent": "pnlModalBodyLoading",
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		},
		"style": {
			"height": "10px",
			"width": "50px"
		}
	},
	"pnlUpdateCartModalBodyDiscountOD": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyDiscountOD",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"float": "left",
			"width": "30%"
		},
		"parent": "pnlMainBodyOrderDetailsDiscount"
	},
	"pnlMainHeaderMiddleTable": {
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddle",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTable",
			"componentVisible": true
		}
	},
	"pnlMainBodyFormCustomerNameOrderDetails": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyFormCustomerNameOrderDetails",
			"componentVisible": true
		},
		"style": {
			"display": "none",
			"width": "100%",
			"textAlign": "center",
			"paddingBottom": "10px"
		},
		"parent": "pnlMainBodyForm"
	},
	"pnlMainBodyForm3LabelAddressOrderDetails": {
		"parent": "pnlMainBodyAddressOrderDetails",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm3LabelAddressOrderDetails",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "left"
		}
	},
	"pnlMainBodyFormInvoiceDateOrderDetails": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyFormInvoiceDateOrderDetails",
			"componentVisible": true
		},
		"style": {
			"display": "none",
			"width": "100%",
			"textAlign": "center",
			"paddingBottom": "10px"
		},
		"parent": "pnlMainBodyForm"
	},
	"pnlUpdateCartModalBodyRight": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyRight",
			"componentVisible": true
		},
		"style": {
			"border": "1px solid #FFF",
			"backgroundColor": "rgb(255, 69, 0)",
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "50px",
			"height": "100%",
			"textAlign": "center"
		},
		"parent": "pnlUpdateCartModalBodyQtyValueOD"
	},
	"imgPlus": {
		"style": {
			"width": "30px",
			"height": "30px"
		},
		"parent": "pnlUpdateCartModalBodyRight",
		"property": {
			"componentType": "image",
			"componentName": "imgPlus",
			"componentValue": require("../../asset/plus-white.png"),
			"componentVisible": true
		}
	},
	"pnlUpdateCartModalBodyButtonOrderDetails": {
		"style": {
			"display": "block",
			"width": "100%",
			"height": "calc( 100% - 310px )",
			"textAlign": "center"
		},
		"parent": "pnlUpdateCartModalBodyOrderDetails",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyButtonOrderDetails",
			"componentVisible": true
		}
	},
	"pnlCSS": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCSS",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"display": "none",
			"top": "0px",
			"zIndex": "4",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"lblAddress_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblAddress_data",
			"field": "name",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"fontSize": "16px",
			"textAlign": "center",
			"fontWeight": "normal",
			"color": "rgb(255, 255, 255)"
		},
		"parent": "pnlMainBodyForm"
	},
	"txtAddressOrderDetails": {
		"property": {
			"componentType": "edit",
			"componentName": "txtAddressOrderDetails",
			"disabled": true,
			"type": "text",
			"componentVisible": true
		},
		"style": {
			"height": "50px",
			"width": "100%",
			"fontSize": "16px",
			"border": "1px solid #D9D9D9",
			"padding": "0px",
			"margin": "0px"
		},
		"parent": "pnlMainBodyFormInputValue"
	},
	"pnlMainBodyItems": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyItems",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"paddingTop": "5px",
			"padding": "5px",
			"paddingBottom": "0px",
			"marginBottom": "20px"
		},
		"parent": "pnlMainBody1"
	},
	"pnlUpdateCartModalOrderDetails": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalOrderDetails",
			"componentVisible": false
		},
		"style": {
			"zIndex": "2",
			"width": "100%",
			"top": "0px",
			"background": "rgba(0, 0, 0, 0.60)",
			"position": "fixed",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"lblTotalAmountExclGST": {
		"property": {
			"componentType": "label",
			"componentName": "lblTotalAmountExclGST",
			"componentValue": "Total Amount",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "bolder"
		},
		"parent": "pnlTotalAmountTableCell1"
	},
	"lblBtnUpdate": {
		"property": {
			"componentType": "label",
			"componentName": "lblBtnUpdate",
			"componentValue": "UPDATE",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "bold"
		},
		"parent": "pnlMainFooterTblCell1"
	},
	"pnlUpdateCartModalBodyOrderDetails": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyOrderDetails",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"width": "90%",
			"textAlign": "center",
			"marginLeft": "5%",
			"marginRight": "5%",
			"background": "#ffffff"
		},
		"parent": "pnlUpdateCartModalCellOrderDetails"
	},
	"lblOrderDate_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblOrderDate_data",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"fontSize": "16px",
			"textAlign": "center",
			"color": "rgb(255, 255, 255)",
			"fontWeight": "normal"
		},
		"parent": "pnlMainBodyForm"
	},
	"pnlMainBodyForm3LabelInvoiceDateOrderDetails": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm3LabelInvoiceDateOrderDetails",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyFormInvoiceDateOrderDetails"
	},
	"lblInvoiceDateOrderDetails": {
		"property": {
			"componentType": "label",
			"componentName": "lblInvoiceDateOrderDetails",
			"componentValue": "Invoice Date",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"fontWeight": "normal",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyForm3LabelInvoiceDateOrderDetails"
	},
	"Image553": {
		"parent": "pnlMainBodyItems1",
		"property": {
			"componentType": "image",
			"componentName": "Image553",
			"componentValue": require("../../asset/plus-white.png"),
			"zoom": false,
			"componentVisible": true
		},
		"style": {
			"height": "25px"
		}
	},
	"pnlMainHeaderLeftCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"height": "100%",
			"verticalAlign": "middle",
			"width": "100%",
			"textAlign": "left",
			"paddingLeft": "10px"
		},
		"parent": "pnlMainHeaderLeftTable"
	},
	"lblCustomerCodeName_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblCustomerCodeName_data",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 215, 0)",
			"fontSize": "20px",
			"textAlign": "center",
			"fontWeight": "normal",
			"display": "block"
		},
		"parent": "pnlMainBodyForm"
	},
	"txtCustomerNameOrderDetails": {
		"property": {
			"componentType": "edit",
			"componentName": "txtCustomerNameOrderDetails",
			"type": "text",
			"disabled": true,
			"componentVisible": true
		},
		"style": {
			"height": "50px",
			"fontSize": "16px",
			"border": "1px solid #D9D9D9",
			"width": "100%",
			"padding": "0px",
			"margin": "0px"
		},
		"parent": "pnlMainBodyForm3InputValueOrderDetails"
	},
	"pnlMainBodyForm3LabelOrderDateOrderDetails": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm3LabelOrderDateOrderDetails",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyFormOrderDateOrderDetails"
	},
	"Panel293": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel293",
			"componentVisible": true
		},
		"style": {
			"paddingBottom": "5px",
			"width": "100%",
			"backgroundColor": "rgb(84, 84, 84)",
			"padding": "10px"
		},
		"parent": "pnlUpdateCartModalBodyOrderDetails"
	},
	"pnlUpdateCartModalBodyQtyValueOD": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyQtyValueOD",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"display": "table",
			"height": "40px"
		},
		"parent": "pnlUpdateCartModalBodyOrderDetailsQty"
	},
	"pnlUpdateCartModalBodyMiddle": {
		"parent": "pnlUpdateCartModalBodyQtyValueOD",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyMiddle",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"display": "table-cell",
			"width": "calc(100% - 50px - 50px)",
			"border": "1px solid #FFF"
		}
	},
	"pnlModalTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModal"
	},
	"lblUOMValue": {
		"style": {
			"textAlign": "center",
			"marginTop": "10px",
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"fontWeight": "normal"
		},
		"parent": "pnlUpdateCartModalBodyOrderDetailsQty",
		"property": {
			"componentType": "label",
			"componentName": "lblUOMValue",
			"componentVisible": true
		}
	},
	"lblDiscountIL": {
		"property": {
			"componentType": "label",
			"componentName": "lblDiscountIL",
			"componentValue": "Disc:",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"fontSize": "16px",
			"fontWeight": "bolder",
			"textAlign": "left",
			"color": "rgb(60, 60, 59)"
		},
		"parent": "pnlUpdateCartModalBodyDiscountOD"
	},
	"pgOrderDetails": {
		"property": {
			"componentType": "page",
			"componentName": "pgOrderDetails",
			"title": "OrderDetails",
			"componentVisible": true
		}
	},
	"txtCustomerCodeOrderDetails": {
		"property": {
			"componentType": "edit",
			"componentName": "txtCustomerCodeOrderDetails",
			"type": "text",
			"disabled": true,
			"componentVisible": true
		},
		"style": {
			"height": "50px",
			"fontSize": "16px",
			"width": "100%",
			"border": "1px solid #D9D9D9",
			"padding": "0px",
			"margin": "0px"
		},
		"parent": "pnlMainBodyFormInputValueOrderDetails"
	},
	"pnlMainBodyFormInputValue": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyFormInputValue",
			"componentVisible": true
		},
		"style": {
			"display": "none",
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyAddressOrderDetails"
	},
	"pnlMainFooterTblCell1": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterTblCell1",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"borderRight": "0px solid #FFF",
			"verticalAlign": "middle",
			"height": "100%",
			"textAlign": "center",
			"backgroundColor": "rgba(0, 0, 0, 0)"
		},
		"parent": "pnlMainFooterTbl"
	},
	"lblDataTotalAmountExclGST": {
		"property": {
			"componentType": "label",
			"componentName": "lblDataTotalAmountExclGST",
			"componentValue": "$ 0.00",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bolder",
			"verticalAlign": "middle",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px"
		},
		"parent": "pnlTotalAmountTableCell2"
	},
	"pnlMainFooterTbl": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterTbl",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table"
		},
		"parent": "pnlMainFooter"
	},
	"lblModalNegative": {
		"parent": "pnlModalBodyButtonNegative",
		"property": {
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		}
	},
	"pnlUpdateCartModalBodyOrderDetailsImage": {
		"parent": "pnlUpdateCartModalBodyOrderDetails",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyOrderDetailsImage",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"width": "100%"
		}
	},
	"pnlMainHeaderLeftTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderLeft"
	},
	"pnlMainHeaderMiddleCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddleTable"
	},
	"pnlMainBodyFormInputValueOrderDetails": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyFormInputValueOrderDetails",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"display": "none"
		},
		"parent": "pnlMainBodyFormCustomerCodeOrderDetails"
	},
	"pnlMainBodyFormOrderDateOrderDetails": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyFormOrderDateOrderDetails",
			"componentVisible": true
		},
		"style": {
			"display": "none",
			"width": "100%",
			"textAlign": "center",
			"paddingBottom": "10px"
		},
		"parent": "pnlMainBodyForm"
	},
	"Panel964": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel964",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"padding": "10px"
		},
		"parent": "pnlUpdateCartModalBodyOrderDetails"
	},
	"pnlMainBodyItemsTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyItemsTable",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		},
		"parent": "pnlMainBodyItems"
	},
	"dlItem": {
		"style": {
			"backgroundColor": "rgb(255, 255, 255)"
		},
		"parent": "pnlMainBodyItemsTable",
		"template": {
			"componentDataItemState": {
				"imgItem": {
					"property": {
						"componentType": "image",
						"componentName": "imgItem",
						"componentValue": require("../../asset/images/default.png"),
						"componentVisible": true
					},
					"style": {
						"height": "90px",
						"width": "100px"
					},
					"parent": "pnlProductBodyLeft"
				},
				"lblPrice": {
					"property": {
						"componentType": "label",
						"componentName": "lblPrice",
						"componentValue": "Price x Qty UOM",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"fontSize": "18px",
						"fontWeight": "bold",
						"textAlign": "left",
						"marginTop": "10px",
						"color": "rgb(0, 153, 0)"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"Label401": {
					"property": {
						"componentType": "label",
						"componentName": "Label401",
						"componentValue": "REMOVE",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(255, 255, 255)",
						"fontSize": "16px"
					},
					"parent": "Panel22"
				},
				"pnlProductBodyMiddle1": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle1",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"width": "100%"
					},
					"parent": "pnlProductBodyMiddle"
				},
				"Label235": {
					"style": {
						"fontSize": "16px",
						"color": "rgb(255, 255, 255)"
					},
					"parent": "pnlProductBodyRight1Cell",
					"property": {
						"componentType": "label",
						"componentName": "Label235",
						"componentValue": "EDIT QTY",
						"componentVisible": true
					}
				},
				"Panel22": {
					"parent": "pnlProductBodyRight",
					"property": {
						"componentType": "panel",
						"componentName": "Panel22",
						"componentVisible": true
					},
					"style": {
						"display": "table-cell",
						"verticalAlign": "middle",
						"width": "50%",
						"textAlign": "center",
						"borderLeft": "1px solid #FFF"
					}
				},
				"pnlProductBodyMiddle": {
					"style": {
						"verticalAlign": "top",
						"textAlign": "left",
						"paddingLeft": "10px",
						"display": "table-cell",
						"width": "calc(100% - 100px - 50px)"
					},
					"parent": "pnlProductBodyItems1",
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle",
						"componentVisible": true
					}
				},
				"lblItemName": {
					"property": {
						"componentType": "label",
						"componentName": "lblItemName",
						"componentValue": "ItemName",
						"field": "Description",
						"componentVisible": true
					},
					"style": {
						"fontSize": "18px",
						"fontWeight": "normal",
						"display": "block",
						"color": "rgb(60, 60, 59)",
						"textAlign": "left"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"pnlProductBodyRight": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyRight",
						"componentVisible": true
					},
					"style": {
						"backgroundColor": "rgb(71, 65, 65)",
						"display": "table",
						"width": "100%",
						"height": "30px",
						"borderRadius": "0px 0px 5px 5px"
					},
					"parent": "Panel293xx"
				},
				"Panel293xx": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel293xx",
						"componentVisible": true
					},
					"style": {
						"borderRadius": "5px",
						"marginBottom": "5px",
						"boxShadow": "-1px 1px 1px #000",
						"color": "rgba(0, 0, 0, 0)",
						"border": "1px solid #3C3C3B"
					}
				},
				"pnlProductItem": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductItem",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"padding": "5px",
						"width": "100%"
					},
					"parent": "Panel293xx"
				},
				"pnlProductBodyLeft": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyLeft",
						"componentVisible": true
					},
					"style": {
						"paddingTop": "0px",
						"verticalAlign": "top",
						"width": "100px",
						"textAlign": "center",
						"display": "table-cell"
					},
					"parent": "pnlProductBodyItems1"
				},
				"pnlProductBodyItems1": {
					"parent": "pnlProductItem",
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyItems1",
						"componentVisible": true
					},
					"style": {
						"display": "table",
						"width": "100%",
						"borderRadius": "10px",
						"background": "url() no-repeat fixed"
					}
				},
				"lblItemCode": {
					"property": {
						"componentType": "label",
						"componentName": "lblItemCode",
						"field": "No",
						"componentValue": "ItemCode",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"color": "rgb(60, 60, 59)",
						"fontSize": "24px",
						"fontWeight": "bold"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"pnlProductBodyRight1Cell": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyRight1Cell",
						"componentVisible": true
					},
					"style": {
						"textAlign": "center",
						"borderRight": "1px solid #FFF",
						"height": "100%",
						"verticalAlign": "middle",
						"width": "50%",
						"display": "table-cell"
					},
					"parent": "pnlProductBodyRight"
				}
			}
		},
		"property": {
			"componentType": "datalist",
			"componentName": "dlItem",
			"componentValue": "l_orderlist_details",
			"autoLoad": false,
			"componentData": [],
			"componentVisible": true
		}
	},
	"pnlModalBodyMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		},
		"style": {
			"width": "90%",
			"padding": "10px 0px",
			"marginLeft": "5%",
			"marginRight": "5%",
			"textAlign": "center",
			"background": "#ffffff",
			"borderBottom": "1px solid #D9D9D9"
		},
		"parent": "pnlModalCell"
	},
	"imgOrderDetailsImage": {
		"property": {
			"componentType": "image",
			"componentName": "imgOrderDetailsImage",
			"componentValue": require("../../asset/images/default.png"),
			"componentVisible": true
		},
		"style": {
			"margin": "20px",
			"width": "100px",
			"height": "90px"
		},
		"parent": "pnlUpdateCartModalBodyOrderDetailsImage"
	},
	"pnlMainHeaderRightCell": {
		"parent": "pnlMainHeaderRightTable",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		}
	},
	"pnlMainBodyFormCustomerCodeOrderDetails": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyFormCustomerCodeOrderDetails",
			"componentVisible": true
		},
		"style": {
			"display": "none",
			"width": "100%",
			"textAlign": "center",
			"paddingBottom": "10px"
		},
		"parent": "pnlMainBodyForm"
	},
	"pnlMainBodyAddressOrderDetails": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyAddressOrderDetails",
			"componentVisible": true
		},
		"style": {
			"paddingBottom": "10px",
			"display": "none",
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyForm"
	},
	"txtInvoiceDateOrderDetails": {
		"property": {
			"componentType": "edit",
			"componentName": "txtInvoiceDateOrderDetails",
			"type": "date",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"fontSize": "16px",
			"padding": "0px",
			"border": "1px solid #D9D9D9",
			"backgroundColor": "rgb(255, 255, 255)",
			"margin": "0px",
			"height": "50px",
			"color": "rgb(60, 60, 59)"
		},
		"parent": "pnlMainBodyForm3Input"
	},
	"lblUpdateCartItemNameValueOD": {
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateCartItemNameValueOD",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontSize": "18px",
			"fontWeight": "normal",
			"display": "block",
			"textAlign": "left"
		},
		"parent": "pnlUpdateCartModalBodyOrderDetailsName"
	},
	"pnlMainBodyOrderDetailsDiscount": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyOrderDetailsDiscount",
			"componentVisible": true
		},
		"style": {
			"height": "70px",
			"overflow": "auto",
			"width": "100%",
			"display": "none"
		},
		"parent": "pnlUpdateCartModalBodyOrderDetails"
	}
};
