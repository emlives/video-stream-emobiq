export const StateName = 'pgTransferStockDetails';
export const State = {
	"lblToLocation_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblToLocation_data",
			"componentValue": "Label",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"fontWeight": "600"
		},
		"parent": "pnlBodyContent680452"
	},
	"pnlBtnCalculateYes912": {
		"parent": "pnlMainFooterCalculateBtn455",
		"property": {
			"componentType": "panel",
			"componentName": "pnlBtnCalculateYes912",
			"componentVisible": true
		},
		"style": {
			"borderRight": "1px solid #FFF",
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "50%",
			"textAlign": "center"
		}
	},
	"pnlModalBodyMain": {
		"style": {
			"padding": "10px 0px",
			"marginLeft": "5%",
			"marginRight": "5%",
			"background": "#ffffff",
			"borderBottom": "1px solid #D9D9D9",
			"width": "90%",
			"textAlign": "center"
		},
		"parent": "pnlModalCell",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		}
	},
	"Panel291662": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel291662",
			"componentVisible": true
		},
		"style": {
			"paddingRight": "10px",
			"height": "50px",
			"paddingTop": "5px",
			"paddingBottom": "5px",
			"paddingLeft": "10px"
		},
		"parent": "pnlCalculateTableCellContent638435"
	},
	"pnlBtnCalculateNo27883": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBtnCalculateNo27883",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"borderLeft": "1px solid #FFF",
			"width": "50%",
			"textAlign": "center"
		},
		"parent": "pnlMainFooterCalculateBtn455655"
	},
	"lblCalculateOK302949": {
		"parent": "pnlBtnCalculateYes912158",
		"property": {
			"componentType": "label",
			"componentName": "lblCalculateOK302949",
			"componentValue": "CANCEL",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "bold",
			"backgroundColor": "rgba(255, 255, 255, 0)"
		}
	},
	"pnlModal": {
		"parent": "pnlHolder",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"top": "0px",
			"zIndex": "4",
			"width": "100%",
			"height": "100%",
			"background": "rgba(0, 0, 0, 0.60)"
		}
	},
	"pnlModalTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModal"
	},
	"pnlModalBodyButtonPositive": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"float": "none",
			"width": "50%",
			"display": "inline-block",
			"textAlign": "center",
			"padding": "10px 0px"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlBtnCalculateYes912356": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBtnCalculateYes912356",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"borderRight": "0px solid #FFF",
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "50%"
		},
		"parent": "pnlMainFooterCalculateBtn455666"
	},
	"pnlModalAddQty": {
		"parent": "pnlHolder",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalAddQty",
			"componentVisible": false
		},
		"style": {
			"position": "absolute",
			"zIndex": "2",
			"top": "0px",
			"width": "100%",
			"height": "100%",
			"background": "rgba(0, 0, 0, 0.60)"
		}
	},
	"pnlMainBodyCalculateContent58800": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyCalculateContent58800",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "150px",
			"padding": "10px"
		},
		"parent": "pnlCalculateTableCellContent6386"
	},
	"pnlMainBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		},
		"style": {
			"height": "calc(100% - 70px - 60px - 50px)",
			"overflow": "auto",
			"width": "100%"
		},
		"parent": "pnlMain"
	},
	"pnlBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBody",
			"componentVisible": true
		},
		"style": {
			"height": "100%"
		},
		"parent": "pnlMainBody"
	},
	"dtTransferLines": {
		"parent": "pnlBodyContent",
		"template": {
			"componentDataItemState": {
				"pnlMainDataContent": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlMainDataContent",
						"componentVisible": true
					},
					"style": {
						"textAlign": "left",
						"backgroundColor": "rgba(0, 0, 0, 0)",
						"width": "100%"
					}
				},
				"pnlDataContentTbl": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlDataContentTbl",
						"componentVisible": true
					},
					"style": {
						"borderRadius": "5px",
						"boxShadow": "-1px 1px 1px #545454",
						"background": "url() no-repeat fixed",
						"backgroundColor": "rgb(255, 255, 255)",
						"border": "1px solid #3C3C3B",
						"display": "table",
						"width": "100%"
					},
					"parent": "pnlMainDataContent"
				},
				"lblDataContentNo_data": {
					"style": {
						"display": "block",
						"color": "rgb(0, 0, 0)",
						"fontSize": "20px",
						"fontWeight": "bold"
					},
					"parent": "pnlDataContentNo",
					"property": {
						"componentType": "label",
						"componentName": "lblDataContentNo_data",
						"field": "Item_No",
						"componentVisible": true
					}
				},
				"pnlDataContentToLocation": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlDataContentToLocation",
						"componentVisible": true
					},
					"style": {
						"width": "100%",
						"display": "block"
					},
					"parent": "pnlDataContentTblCell1"
				},
				"lblDataContentToLocation_data": {
					"property": {
						"componentType": "label",
						"componentName": "lblDataContentToLocation_data",
						"field": "Description",
						"componentVisible": true
					},
					"style": {
						"paddingTop": "2px",
						"display": "block",
						"color": "rgb(0, 0, 0)",
						"fontSize": "14px",
						"fontWeight": "normal"
					},
					"parent": "pnlDataContentToLocation"
				},
				"pnlDataContentPostingDate": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlDataContentPostingDate",
						"componentVisible": true
					},
					"style": {
						"width": "100%",
						"display": "block"
					},
					"parent": "pnlDataContentTblCell1"
				},
				"lblDataContentPostingDate_data": {
					"property": {
						"componentType": "label",
						"componentName": "lblDataContentPostingDate_data",
						"componentVisible": true
					},
					"style": {
						"fontSize": "14px",
						"fontWeight": "normal",
						"paddingTop": "2px",
						"display": "block",
						"color": "rgb(0, 0, 0)"
					},
					"parent": "pnlDataContentPostingDate"
				},
				"pnlDataContentStatus": {
					"style": {
						"width": "100%",
						"display": "block"
					},
					"parent": "pnlDataContentTblCell1",
					"property": {
						"componentType": "panel",
						"componentName": "pnlDataContentStatus",
						"componentVisible": true
					}
				},
				"pnlDataContentTblCell1": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlDataContentTblCell1",
						"componentVisible": true
					},
					"style": {
						"display": "table-cell",
						"textAlign": "left",
						"width": "calc(100% - 40px)",
						"padding": "5px"
					},
					"parent": "pnlDataContentTbl"
				},
				"pnlDataContentNo": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlDataContentNo",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"width": "100%"
					},
					"parent": "pnlDataContentTblCell1"
				},
				"pnlDataContentTblCell2": {
					"parent": "pnlDataContentTbl",
					"property": {
						"componentType": "panel",
						"componentName": "pnlDataContentTblCell2",
						"componentVisible": true
					},
					"style": {
						"textAlign": "center",
						"backgroundColor": "rgb(71, 65, 65)",
						"borderRadius": "0px 5px 5px 0px",
						"display": "table-cell",
						"verticalAlign": "middle",
						"width": "40px"
					}
				},
				"Image776": {
					"parent": "pnlDataContentTblCell2",
					"property": {
						"componentType": "image",
						"componentName": "Image776",
						"componentValue": require("../../asset/icon/pencil_white.png"),
						"zoom": false,
						"componentVisible": true
					},
					"style": {
						"width": "20px"
					}
				}
			}
		},
		"property": {
			"componentType": "datalist",
			"componentName": "dtTransferLines",
			"componentValue": "l_TransferLines",
			"autoLoad": false,
			"componentData": [],
			"componentVisible": true
		},
		"style": {
			"overflow": "auto"
		}
	},
	"pnlCalculateTableCell600941": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTableCell600941",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlCalculateTable668645118423"
	},
	"pnlCalculateTableCellContent638": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTableCellContent638",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"background": "#ffffff",
			"position": "relative",
			"width": "90%",
			"borderRadius": "10px",
			"margin": "5%"
		},
		"parent": "pnlCalculateTableCell600"
	},
	"pnlCalculateTableCell600671": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTableCell600671",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"verticalAlign": "middle",
			"display": "table-cell",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlCalculateTable66864539346"
	},
	"pnlHolder": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"backgroundColor": "rgb(245, 245, 245)"
		},
		"parent": "pgTransferStockDetails"
	},
	"pnlMainHeaderRightTblCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTblCell",
			"componentVisible": true
		},
		"style": {
			"textAlign": "right",
			"paddingRight": "10px",
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRightTbl"
	},
	"lblReturn": {
		"property": {
			"componentType": "label",
			"componentName": "lblReturn",
			"componentValue": "ADD",
			"componentVisible": true
		},
		"style": {
			"display": "none",
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"fontWeight": "500"
		},
		"parent": "pnlMainHeaderRightTblCell"
	},
	"pnlBody921975": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBody921975",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"padding": "10px",
			"display": "table",
			"width": "100%"
		},
		"parent": "pnlMainSubHeader"
	},
	"lblModalPositive": {
		"parent": "pnlModalBodyButtonPositive",
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		}
	},
	"lblEdtQtyTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblEdtQtyTitle",
			"componentValue": "Message title",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontSize": "20px",
			"fontWeight": "500",
			"paddingBottom": "0px",
			"display": "block",
			"padding": "10px"
		},
		"parent": "pnlCalculateTableCellContent638"
	},
	"pnlMainBodyCalculateContent58": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyCalculateContent58",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "150px",
			"padding": "10px"
		},
		"parent": "pnlCalculateTableCellContent638"
	},
	"pnlCalculateTableCellContent638435": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTableCellContent638435",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"margin": "5%",
			"width": "90%",
			"height": "90%",
			"background": "#ffffff",
			"borderRadius": "10px"
		},
		"parent": "pnlCalculateTableCell600941"
	},
	"pgTransferStockDetails": {
		"property": {
			"componentType": "page",
			"componentName": "pgTransferStockDetails",
			"title": "Transfer Stock Details",
			"componentVisible": true
		}
	},
	"pnlMainHeaderLeftTbl": {
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderLeft",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTbl",
			"componentVisible": true
		}
	},
	"pnlMainHeaderMiddleTbl": {
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddle",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTbl",
			"componentVisible": true
		}
	},
	"pnlBodyContent680346": {
		"parent": "pnlBody921975",
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyContent680346",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "20%",
			"textAlign": "center"
		}
	},
	"DataList334": {
		"template": {
			"componentDataItemState": {
				"Label117": {
					"parent": "Panel60",
					"property": {
						"componentType": "label",
						"componentName": "Label117",
						"field": "No",
						"componentValue": "Item No",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"color": "rgb(60, 60, 59)",
						"fontSize": "20px",
						"fontWeight": "600"
					}
				},
				"Label742": {
					"property": {
						"componentType": "label",
						"componentName": "Label742",
						"componentValue": "Description",
						"field": "Description",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(60, 60, 59)",
						"fontSize": "18px"
					},
					"parent": "Panel60"
				},
				"Panel435": {
					"parent": "pnlBodyCalculateContent907237",
					"property": {
						"componentType": "panel",
						"componentName": "Panel435",
						"componentVisible": true
					},
					"style": {
						"display": "table-cell",
						"verticalAlign": "middle",
						"width": "80px",
						"textAlign": "center"
					}
				},
				"Image794": {
					"style": {
						"width": "25px",
						"height": "25px"
					},
					"parent": "Panel435",
					"property": {
						"componentType": "image",
						"componentName": "Image794",
						"componentValue": require("../../asset/icon/icon-add-gray.png"),
						"zoom": false,
						"componentVisible": true
					}
				},
				"pnlBodyCalculateContent907237": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlBodyCalculateContent907237",
						"componentVisible": true
					},
					"style": {
						"marginBottom": "5px",
						"display": "table",
						"border": "1px solid #3C3C3B",
						"borderRadius": "5px",
						"paddingBottom": "0px",
						"width": "100%",
						"boxShadow": "-1px 1px 1px #545454",
						"textAlign": "center",
						"verticalAlign": "middle",
						"padding": "5px"
					}
				},
				"Panel816": {
					"style": {
						"display": "table-cell",
						"width": "80px",
						"height": "75px"
					},
					"parent": "pnlBodyCalculateContent907237",
					"property": {
						"componentType": "panel",
						"componentName": "Panel816",
						"componentVisible": true
					}
				},
				"Image180": {
					"style": {
						"width": "80px",
						"height": "75px"
					},
					"parent": "Panel816",
					"property": {
						"componentType": "image",
						"componentName": "Image180",
						"zoom": false,
						"componentValue": require("../../asset/images/default.png"),
						"componentVisible": true
					}
				},
				"Panel60": {
					"style": {
						"paddingLeft": "10px",
						"display": "table-cell",
						"verticalAlign": "top",
						"textAlign": "left"
					},
					"parent": "pnlBodyCalculateContent907237",
					"property": {
						"componentType": "panel",
						"componentName": "Panel60",
						"componentVisible": true
					}
				}
			}
		},
		"property": {
			"componentType": "datalist",
			"componentName": "DataList334",
			"componentValue": "l_Item_List",
			"componentData": [],
			"componentVisible": true
		},
		"style": {
			"minWidth": "40px",
			"minHeight": "40px"
		},
		"parent": "pnlMainBodyCalculateContent58149"
	},
	"lblAddQtyUOM": {
		"property": {
			"componentType": "label",
			"componentName": "lblAddQtyUOM",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"fontSize": "16px",
			"textAlign": "center",
			"paddingTop": "10px",
			"fontWeight": "bold"
		},
		"parent": "pnlBodyCalculateContent907564"
	},
	"pnlMainFooterCalculateBtn455655": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterCalculateBtn455655",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "1%",
			"width": "100%",
			"height": "50px",
			"textAlign": "center",
			"borderRadius": "0px 0px 10px 10px",
			"paddingBottom": "1%",
			"display": "table",
			"backgroundColor": "rgb(255, 69, 0)"
		},
		"parent": "pnlCalculateTableCellContent6386"
	},
	"pnlCSS": {
		"parent": "pnlHolder",
		"property": {
			"componentType": "panel",
			"componentName": "pnlCSS",
			"componentVisible": false
		},
		"style": {
			"top": "0px",
			"zIndex": "5",
			"width": "100%",
			"position": "fixed",
			"display": "none",
			"height": "100%"
		}
	},
	"pnlBodyCalculateContent907": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyCalculateContent907",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyCalculateContent58"
	},
	"lblCalculateOK302": {
		"property": {
			"componentType": "label",
			"componentName": "lblCalculateOK302",
			"componentValue": "CANCEL",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgba(255, 255, 255, 0)",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "bold"
		},
		"parent": "pnlBtnCalculateYes912"
	},
	"lblAddItemQtyTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblAddItemQtyTitle",
			"componentValue": "Message title",
			"componentVisible": true
		},
		"style": {
			"fontSize": "20px",
			"fontWeight": "500",
			"padding": "10px",
			"display": "block",
			"paddingBottom": "0px"
		},
		"parent": "pnlCalculateTableCellContent6386"
	},
	"pnlMainSubHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainSubHeader",
			"componentVisible": true
		},
		"style": {
			"overflow": "auto",
			"width": "100%",
			"backgroundColor": "rgb(71, 65, 65)",
			"height": "60px"
		},
		"parent": "pnlMain"
	},
	"pnlBodyContent680": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyContent680",
			"componentVisible": true
		},
		"style": {
			"width": "40%",
			"textAlign": "center",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlBody921975"
	},
	"pnlModalBodyTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"marginBottom": "10px",
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlModalBodyLoading": {
		"style": {
			"width": "100%"
		},
		"parent": "pnlModalBodyMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		}
	},
	"lblCalculateNo807336": {
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "bold",
			"backgroundColor": "rgba(255, 255, 255, 0)"
		},
		"parent": "pnlBtnCalculateNo27883",
		"property": {
			"componentType": "label",
			"componentName": "lblCalculateNo807336",
			"componentValue": "OK",
			"componentVisible": true
		}
	},
	"lblModalMessage": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(111, 111, 110)",
			"fontSize": "16px"
		},
		"parent": "pnlModalBodyMessage"
	},
	"pnlModalBodyButtons": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"background": "#ffffff",
			"marginLeft": "5%",
			"marginRight": "5%",
			"width": "90%",
			"textAlign": "center"
		},
		"parent": "pnlModalCell"
	},
	"lblEdtQtyTitle537": {
		"property": {
			"componentType": "label",
			"componentName": "lblEdtQtyTitle537",
			"componentValue": "ITEM LIST",
			"componentVisible": true
		},
		"style": {
			"fontSize": "20px",
			"padding": "10px",
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"fontWeight": "600"
		},
		"parent": "Panel291"
	},
	"pnlCalculateTable668645": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTable668645",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModalEditQty"
	},
	"pnlMainHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		},
		"style": {
			"height": "70px",
			"paddingTop": "15px",
			"width": "100%",
			"textAlign": "center",
			"backgroundColor": "rgb(255, 69, 0)"
		},
		"parent": "pnlMain"
	},
	"pnlBodyContent": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyContent",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlBody"
	},
	"lblModalTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "20px",
			"fontWeight": "bold"
		},
		"parent": "pnlModalBodyTitle"
	},
	"pnlModalEditQty": {
		"parent": "pnlHolder",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalEditQty",
			"componentVisible": false
		},
		"style": {
			"top": "0px",
			"width": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"position": "absolute",
			"zIndex": "2",
			"height": "100%"
		}
	},
	"lblModalNegative": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonNegative"
	},
	"pnlBtnCalculateNo27": {
		"parent": "pnlMainFooterCalculateBtn455",
		"property": {
			"componentType": "panel",
			"componentName": "pnlBtnCalculateNo27",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"width": "50%",
			"display": "table-cell",
			"textAlign": "center",
			"borderLeft": "1px solid #FFF"
		}
	},
	"lblCalculateOK302338": {
		"property": {
			"componentType": "label",
			"componentName": "lblCalculateOK302338",
			"componentValue": "CANCEL",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "bold",
			"backgroundColor": "rgba(255, 255, 255, 0)"
		},
		"parent": "pnlBtnCalculateYes912356"
	},
	"edtAddQty": {
		"property": {
			"componentType": "edit",
			"componentName": "edtAddQty",
			"type": "number",
			"componentValue": "100",
			"componentVisible": true
		},
		"style": {
			"fontSize": "30px",
			"textAlign": "center",
			"verticalAlign": "middle",
			"height": "80px",
			"fontWeight": "500",
			"width": "75%"
		},
		"parent": "pnlBodyCalculateContent907564"
	},
	"imgMenu": {
		"property": {
			"componentType": "image",
			"componentName": "imgMenu",
			"componentValue": require("../../asset/icon/icon-back.png"),
			"componentVisible": true
		},
		"style": {
			"width": "35px",
			"height": "35px"
		},
		"parent": "pnlMainHeaderLeftTblCell"
	},
	"pnlMainHeaderMiddle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddle",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"float": "left",
			"width": "70%"
		},
		"parent": "pnlMainHeader"
	},
	"lblMainTitle_data": {
		"parent": "pnlMainHeaderMiddleTblCell",
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle_data",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500",
			"width": "100%"
		}
	},
	"lblFooterBtnRelease": {
		"property": {
			"componentType": "label",
			"componentName": "lblFooterBtnRelease",
			"componentValue": "RELEASE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"fontWeight": "bold"
		},
		"parent": "pnlFooterBtnRelease"
	},
	"pnlCalculateTable66864539346": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTable66864539346",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%",
			"position": "relative"
		},
		"parent": "pnlModalAddQty"
	},
	"Image9985622": {
		"parent": "pnlBodyContent680346",
		"property": {
			"componentType": "image",
			"componentName": "Image9985622",
			"componentValue": require("../../asset/icon/arrow2.png"),
			"zoom": false,
			"componentVisible": true
		},
		"style": {
			"height": "30px"
		}
	},
	"edtQty": {
		"property": {
			"componentType": "edit",
			"componentName": "edtQty",
			"componentValue": "100",
			"type": "number",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"height": "80px",
			"width": "75%",
			"fontSize": "30px",
			"fontWeight": "500",
			"textAlign": "center"
		},
		"parent": "pnlBodyCalculateContent907"
	},
	"lblEditQtyUOM": {
		"property": {
			"componentType": "label",
			"componentName": "lblEditQtyUOM",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "10px",
			"display": "block",
			"fontSize": "16px",
			"textAlign": "center",
			"color": "rgb(60, 60, 59)",
			"fontWeight": "bold"
		},
		"parent": "pnlBodyCalculateContent907"
	},
	"pnlModalAddItem": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalAddItem",
			"componentVisible": false
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"position": "absolute",
			"top": "0px",
			"zIndex": "2"
		},
		"parent": "pnlHolder"
	},
	"pnlBodyContent680452": {
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "40%",
			"textAlign": "center"
		},
		"parent": "pnlBody921975",
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyContent680452",
			"componentVisible": true
		}
	},
	"pnlCalculateTable668645118423": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTable668645118423",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModalAddItem"
	},
	"pnlModalCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModalTable"
	},
	"pnlBodyCalculateContent907564": {
		"parent": "pnlMainBodyCalculateContent58800",
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyCalculateContent907564",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"textAlign": "center",
			"display": "table-cell"
		}
	},
	"pnlMain": {
		"parent": "pnlHolder",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"zIndex": "0"
		}
	},
	"pnlMainHeaderMiddleTblCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTblCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddleTbl"
	},
	"pnlMainHeaderRight": {
		"style": {
			"height": "100%",
			"float": "left",
			"width": "15%"
		},
		"parent": "pnlMainHeader",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRight",
			"componentVisible": true
		}
	},
	"pnlMainFooter": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooter",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "50px",
			"backgroundColor": "rgb(255, 69, 0)"
		},
		"parent": "pnlMain"
	},
	"pnlMainFooterCalculateBtn455666": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterCalculateBtn455666",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgb(255, 69, 0)",
			"height": "50px",
			"textAlign": "center",
			"paddingTop": "1%",
			"width": "100%",
			"display": "table",
			"borderRadius": "0px 0px 10px 10px",
			"paddingBottom": "1%"
		},
		"parent": "pnlCalculateTableCellContent638435"
	},
	"pnlBtnCalculateYes912158": {
		"style": {
			"width": "50%",
			"textAlign": "center",
			"borderRight": "1px solid #FFF",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlMainFooterCalculateBtn455655",
		"property": {
			"componentType": "panel",
			"componentName": "pnlBtnCalculateYes912158",
			"componentVisible": true
		}
	},
	"lblMainTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle",
			"componentValue": "TRANSFER STOCKS",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "500",
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "14px"
		},
		"parent": "pnlMainHeaderMiddleTblCell"
	},
	"pnlMainHeaderRightTbl": {
		"parent": "pnlMainHeaderRight",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTbl",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"display": "table",
			"width": "100%"
		}
	},
	"pnlModalBodyMessage": {
		"parent": "pnlModalBodyMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"padding": "10px",
			"marginBottom": "10px"
		}
	},
	"lblStyle": {
		"property": {
			"componentType": "label",
			"componentName": "lblStyle",
			"componentValue": "N/A",
			"componentVisible": true
		},
		"parent": "pnlCSS"
	},
	"pnlFooterBtnRelease": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlFooterBtnRelease",
			"componentVisible": true
		},
		"style": {
			"borderLeft": "0px solid #FFF",
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "50%",
			"textAlign": "center"
		},
		"parent": "pnlMainFooter"
	},
	"pnlMainFooterCalculateBtn455": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterCalculateBtn455",
			"componentVisible": true
		},
		"style": {
			"paddingBottom": "1%",
			"display": "table",
			"height": "50px",
			"textAlign": "center",
			"paddingTop": "1%",
			"width": "100%",
			"backgroundColor": "rgb(255, 69, 0)",
			"borderRadius": "0px 0px 10px 10px"
		},
		"parent": "pnlCalculateTableCellContent638"
	},
	"pnlCalculateTableCellContent6386": {
		"parent": "pnlCalculateTableCell600671",
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTableCellContent6386",
			"componentVisible": true
		},
		"style": {
			"width": "90%",
			"textAlign": "center",
			"borderRadius": "10px",
			"margin": "5%",
			"position": "relative",
			"background": "#ffffff"
		}
	},
	"imgModalLoading": {
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		},
		"style": {
			"height": "10px",
			"width": "50px"
		},
		"parent": "pnlModalBodyLoading"
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"padding": "10px 0px",
			"display": "inline-block",
			"float": "none",
			"textAlign": "center",
			"width": "50%"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlCalculateTableCell600": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTableCell600",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"position": "relative",
			"display": "table-cell",
			"verticalAlign": "middle",
			"height": "100%"
		},
		"parent": "pnlCalculateTable668645"
	},
	"lblCalculateNo807": {
		"property": {
			"componentType": "label",
			"componentName": "lblCalculateNo807",
			"componentValue": "OK",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "bold",
			"backgroundColor": "rgba(255, 255, 255, 0)"
		},
		"parent": "pnlBtnCalculateNo27"
	},
	"pnlMainHeaderLeft": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeft",
			"componentVisible": true
		},
		"style": {
			"width": "15%",
			"height": "100%",
			"float": "left"
		},
		"parent": "pnlMainHeader"
	},
	"pnlMainHeaderLeftTblCell": {
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"height": "100%",
			"width": "100%",
			"textAlign": "left",
			"paddingLeft": "10px"
		},
		"parent": "pnlMainHeaderLeftTbl",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTblCell",
			"componentVisible": true
		}
	},
	"imgMenu236": {
		"style": {
			"width": "30px",
			"height": "30px"
		},
		"parent": "pnlMainHeaderRightTblCell",
		"property": {
			"componentType": "image",
			"componentName": "imgMenu236",
			"componentValue": require("../../asset/icon/_add.png"),
			"componentVisible": true
		}
	},
	"lblFromLocation_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblFromLocation_data",
			"componentValue": "Label",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"fontWeight": "600"
		},
		"parent": "pnlBodyContent680"
	},
	"Panel291": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel291",
			"componentVisible": true
		},
		"style": {
			"height": "60px",
			"backgroundColor": "rgb(255, 69, 0)"
		},
		"parent": "pnlCalculateTableCellContent638435"
	},
	"edtSearch": {
		"parent": "Panel291662",
		"property": {
			"componentType": "edit",
			"componentName": "edtSearch",
			"type": "search",
			"placeHolder": "Search",
			"backgroundAsset": "icon/_search.png",
			"componentVisible": true
		},
		"style": {
			"backgroundSize": "16px",
			"paddingLeft": "30px",
			"width": "100%",
			"backgroundPosition": "left .5rem center",
			"border": "1px solid #D9D9D9",
			"height": "40px"
		}
	},
	"pnlMainBodyCalculateContent58149": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyCalculateContent58149",
			"componentVisible": true
		},
		"style": {
			"padding": "10px",
			"overflow": "auto",
			"width": "100%",
			"height": "calc(100% - 60px - 50px - 50px + 1px)"
		},
		"parent": "pnlCalculateTableCellContent638435"
	}
};
