export const StateName = 'EditOnChangePage';
export const State = {
	"pnlMainHeaderLeftTable": {
		"parent": "pnlMainHeaderLeft",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTable",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"display": "table",
			"width": "100%"
		}
	},
	"pnlMainHeaderLeftCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderLeftTable"
	},
	"pnlModalOutletBody2": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalOutletBody2",
			"componentVisible": true
		},
		"style": {
			"display": "none",
			"width": "100%",
			"paddingLeft": "10px",
			"paddingRight": "10px"
		},
		"parent": "pnlModalOutletBodyMain"
	},
	"pnlModalBodyButtons": {
		"style": {
			"textAlign": "center",
			"marginLeft": "5%",
			"background": "#ffffff",
			"marginRight": "5%",
			"width": "90%"
		},
		"parent": "pnlModalCell",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"addClass": "hide",
			"componentVisible": true
		}
	},
	"pnlModalOutletBody1": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalOutletBody1",
			"componentVisible": true
		},
		"style": {
			"paddingLeft": "10px",
			"width": "100%",
			"paddingRight": "10px",
			"marginBottom": "10px"
		},
		"parent": "pnlModalOutletBodyMain"
	},
	"pnlModalBodyMessage": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlMainBodySearch": {
		"parent": "pnlMainBody1",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodySearch",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginTop": "44px",
			"padding": "15px"
		}
	},
	"txtShipToCode": {
		"property": {
			"componentType": "edit",
			"componentName": "txtShipToCode",
			"placeHolder": " Ship To Code",
			"type": "search",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(60, 60, 59)",
			"background": "url('data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Capa_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 451 451\" style=\"enable-background:new 0 0 451 451;\" xml:space=\"preserve\" width=\"512px\" height=\"512px\"><g><path d=\"M447.05,428l-109.6-109.6c29.4-33.8,47.2-77.9,47.2-126.1C384.65,86.2,298.35,0,192.35,0C86.25,0,0.05,86.3,0.05,192.3   s86.3,192.3,192.3,192.3c48.2,0,92.3-17.8,126.1-47.2L428.05,447c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4   C452.25,441.8,452.25,433.2,447.05,428z M26.95,192.3c0-91.2,74.2-165.3,165.3-165.3c91.2,0,165.3,74.2,165.3,165.3   s-74.1,165.4-165.3,165.4C101.15,357.7,26.95,283.5,26.95,192.3z\" fill=\"#3c3c3b\"/></g></svg>') no-repeat",
			"backgroundPosition": "right .5rem center",
			"backgroundSize": "16px",
			"border": "1px solid #D9D9D9",
			"margin": "0px",
			"textAlign": "left",
			"padding": "0px",
			"height": "30px",
			"fontSize": "16px",
			"backgroundColor": "rgb(255, 255, 255)"
		},
		"parent": "pnlModalOutletBody1"
	},
	"lblModalTitle": {
		"style": {
			"width": "100%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "18px",
			"fontWeight": "bold"
		},
		"parent": "pnlModalBodyTitle",
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		}
	},
	"lblAll": {
		"property": {
			"componentType": "label",
			"componentName": "lblAll",
			"componentValue": "My",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500"
		},
		"parent": "pnlMainHeaderRightCell"
	},
	"pnlMainBodyDate": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyDate",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"padding": "3px"
		},
		"parent": "Panel28"
	},
	"pnlMainHeaderLeft": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeft",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlModalOutletBodyTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalOutletBodyTitle",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalOutletBodyMain"
	},
	"lblAddress": {
		"style": {
			"fontSize": "18px",
			"textAlign": "left",
			"width": "100%",
			"color": "rgb(111, 111, 110)"
		},
		"parent": "pnlModalOutletBody2",
		"property": {
			"componentType": "label",
			"componentName": "lblAddress",
			"componentValue": "Address",
			"componentVisible": true
		}
	},
	"pnlModalOutletBodyButtonPositive": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalOutletBodyButtonPositive",
			"componentVisible": true
		},
		"style": {
			"float": "none",
			"width": "50%",
			"display": "inline-block",
			"textAlign": "center",
			"padding": "10px 0px"
		},
		"parent": "pnlModalOutletBodyButtons"
	},
	"pnlCSS": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCSS",
			"componentVisible": false
		},
		"style": {
			"display": "none",
			"zIndex": "3",
			"height": "100%",
			"position": "fixed",
			"top": "0px",
			"width": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlHolder": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"backgroundColor": "rgb(245, 245, 245)"
		},
		"parent": "pgCustomerListing"
	},
	"pnlModalOutletTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalOutletTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModalOutlet"
	},
	"pnlModalBodyMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		},
		"style": {
			"marginLeft": "5%",
			"width": "90%",
			"textAlign": "center",
			"background": "#ffffff",
			"borderBottom": "1px solid #D9D9D9",
			"padding": "10px 0px",
			"marginRight": "5%"
		},
		"parent": "pnlModalCell"
	},
	"pnlModalBodyTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"marginBottom": "10px",
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	},
	"lblModalMessage": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(111, 111, 110)",
			"fontSize": "16px"
		},
		"parent": "pnlModalBodyMessage"
	},
	"pnlMainHeaderRightTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRight"
	},
	"lblModalOutletTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalOutletTitle",
			"componentValue": "Select Outlet",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "20px",
			"fontWeight": "bold"
		},
		"parent": "pnlModalOutletBodyTitle"
	},
	"lblModalOutletOkay": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalOutletOkay",
			"componentValue": "OK",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)"
		},
		"parent": "pnlModalOutletBodyButtonPositive"
	},
	"pnlModalBodyButtonPositive": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"float": "none",
			"textAlign": "center",
			"padding": "10px 0px",
			"display": "inline-block",
			"width": "50%"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"float": "none",
			"width": "50%",
			"display": "inline-block",
			"textAlign": "center",
			"padding": "10px 0px"
		},
		"parent": "pnlModalBodyButtons"
	},
	"txtSearch": {
		"style": {
			"fontSize": "16px",
			"backgroundColor": "rgb(255, 255, 255)",
			"backgroundPosition": "left .5rem center",
			"color": "rgb(60, 60, 59)",
			"paddingLeft": "30px",
			"width": "100%",
			"height": "40px",
			"backgroundSize": "16px",
			"display": "inline",
			"textAlign": "left",
			"background": "url('data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Capa_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 451 451\" style=\"enable-background:new 0 0 451 451;\" xml:space=\"preserve\" width=\"512px\" height=\"512px\"><g><path d=\"M447.05,428l-109.6-109.6c29.4-33.8,47.2-77.9,47.2-126.1C384.65,86.2,298.35,0,192.35,0C86.25,0,0.05,86.3,0.05,192.3   s86.3,192.3,192.3,192.3c48.2,0,92.3-17.8,126.1-47.2L428.05,447c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4   C452.25,441.8,452.25,433.2,447.05,428z M26.95,192.3c0-91.2,74.2-165.3,165.3-165.3c91.2,0,165.3,74.2,165.3,165.3   s-74.1,165.4-165.3,165.4C101.15,357.7,26.95,283.5,26.95,192.3z\" fill=\"#3c3c3b\"/></g></svg>') no-repeat",
			"border": "1px solid #D9D9D9"
		},
		"parent": "pnlMainBodySearch",
		"property": {
			"componentType": "edit",
			"componentName": "txtSearch",
			"placeHolder": "Search",
			"type": "search",
			"backgroundAsset": "icon/_search.png",
			"componentVisible": true
		}
	},
	"pnlMainBody2": {
		"parent": "pnlMainBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody2",
			"componentVisible": true
		},
		"style": {
			"overflow": "auto",
			"height": "calc(100% - 95px)"
		}
	},
	"dlCustomer_List": {
		"property": {
			"componentType": "datalist",
			"componentName": "dlCustomer_List",
			"componentValue": "l_customer",
			"autoLoad": false,
			"componentData": [],
			"componentVisible": true
		},
		"parent": "pnlMainBody2",
		"template": {
			"componentDataItemState": {
				"Panel375": {
					"style": {
						"display": "table",
						"boxShadow": "-1px 1px 1px #545454",
						"backgroundColor": "rgb(255, 255, 255)",
						"width": "100%",
						"border": "1px solid #545454",
						"borderRadius": "5px",
						"padding": "10px"
					},
					"parent": "pnlCustomerListing",
					"property": {
						"componentType": "panel",
						"componentName": "Panel375",
						"componentVisible": true
					}
				},
				"pnlProductBodyLeftCustomerDetails": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyLeftCustomerDetails",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"width": "100%"
					},
					"parent": "pnlProductBodyLeftCustomerDetails1"
				},
				"lblCustomerName": {
					"property": {
						"componentType": "label",
						"componentName": "lblCustomerName",
						"field": "Name",
						"componentVisible": true
					},
					"style": {
						"fontSize": "18px",
						"fontWeight": "normal",
						"textAlign": "left",
						"color": "rgb(111, 111, 110)"
					},
					"parent": "pnlProductBodyMainDetails3"
				},
				"Panel683": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel683",
						"componentVisible": true
					},
					"style": {
						"display": "table-cell",
						"verticalAlign": "middle",
						"width": "40px",
						"textAlign": "center"
					},
					"parent": "Panel375"
				},
				"Image718": {
					"style": {
						"width": "30px",
						"height": "30xp"
					},
					"parent": "Panel683",
					"property": {
						"componentType": "image",
						"componentName": "Image718",
						"componentValue": require("../../asset/images/icon-go-gray (1).png"),
						"zoom": false,
						"componentVisible": true
					}
				},
				"pnlCustomerListing": {
					"style": {
						"padding": "5px",
						"paddingBottom": "0px",
						"width": "100%",
						"background": "url() no-repeat fixed"
					},
					"property": {
						"componentType": "panel",
						"componentName": "pnlCustomerListing",
						"componentVisible": true
					}
				},
				"pnlProductBodyLeftCustomerDetails1": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyLeftCustomerDetails1",
						"componentVisible": true
					},
					"style": {
						"display": "table-cell",
						"width": "calc(100%- 40px)"
					},
					"parent": "Panel375"
				},
				"lblCustomerDetails": {
					"property": {
						"componentType": "label",
						"componentName": "lblCustomerDetails",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(60, 60, 59)",
						"fontSize": "18px",
						"fontWeight": "bold"
					},
					"parent": "pnlProductBodyLeftCustomerDetails"
				},
				"pnlProductBodyMainDetails3": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMainDetails3",
						"componentVisible": true
					},
					"style": {
						"width": "100%",
						"display": "block"
					},
					"parent": "pnlProductBodyLeftCustomerDetails1"
				},
				"pnlProductBodyMiddle1": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle1",
						"componentVisible": true
					},
					"style": {
						"display": "none",
						"width": "100%"
					},
					"parent": "pnlProductBodyLeftCustomerDetails1"
				},
				"lblTerm": {
					"property": {
						"componentType": "label",
						"componentName": "lblTerm",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(111, 111, 110)",
						"fontSize": "14px",
						"fontWeight": "normal"
					},
					"parent": "pnlProductBodyMiddle1"
				}
			}
		}
	},
	"etAddress": {
		"property": {
			"componentType": "memo",
			"componentName": "etAddress",
			"rows": "5",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(33, 33, 33)",
			"background": "rgb(236, 236, 236)",
			"fontSize": "18px",
			"border": "none",
			"margin": "0px"
		},
		"parent": "pnlModalOutletBody2"
	},
	"pnlModalOutletBodyButtonNegative": {
		"parent": "pnlModalOutletBodyButtons",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalOutletBodyButtonNegative",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"padding": "10px 0px",
			"float": "none",
			"width": "50%",
			"display": "inline-block"
		}
	},
	"pnlMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"zIndex": "0",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlMainHeaderRightCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightCell",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%",
			"display": "table-cell"
		},
		"parent": "pnlMainHeaderRightTable"
	},
	"pnlMainHeaderMiddleTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddle"
	},
	"pnlMainHeaderMiddleCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddleTable"
	},
	"pnlMainBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		},
		"style": {
			"height": "calc(100% - 50px - 15px)",
			"width": "100%"
		},
		"parent": "pnlMain"
	},
	"pnlModal": {
		"style": {
			"top": "0px",
			"height": "100%",
			"position": "fixed",
			"zIndex": "2",
			"width": "100%",
			"background": "rgba(0, 0, 0, 0.60)"
		},
		"parent": "pnlHolder",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		}
	},
	"pgCustomerListing": {
		"property": {
			"componentType": "page",
			"componentName": "pgCustomerListing",
			"title": "Customer Listing",
			"componentVisible": true
		}
	},
	"pnlMainHeader": {
		"parent": "pnlMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "65px",
			"textAlign": "center",
			"backgroundColor": "rgb(255, 69, 0)",
			"paddingTop": "15px"
		}
	},
	"lblMainTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle",
			"componentValue": "CUSTOMER LIST",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500",
			"width": "100%"
		},
		"parent": "pnlMainHeaderMiddleCell"
	},
	"Panel28": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel28",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"padding": "2px",
			"width": "100%",
			"height": "50px"
		},
		"parent": "pnlMainBody"
	},
	"lblStyle": {
		"parent": "pnlCSS",
		"property": {
			"componentType": "label",
			"componentName": "lblStyle",
			"componentValue": "N/A",
			"componentVisible": true
		}
	},
	"pnlMainHeaderMiddle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddle",
			"componentVisible": true
		},
		"style": {
			"width": "70%",
			"height": "100%",
			"float": "left"
		},
		"parent": "pnlMainHeader"
	},
	"DatePicker": {
		"property": {
			"componentType": "datepicker",
			"componentName": "DatePicker",
			"firstDay": true,
			"componentVisible": true
		},
		"style": {
			"backgroundPosition": "left .5rem center",
			"fontSize": "16px",
			"border": "1px solid #D9D9D9",
			"height": "40px",
			"width": "100%",
			"backgroundSize": "16px",
			"margin": "0px"
		},
		"parent": "pnlMainBodyDate"
	},
	"pnlModalOutletCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalOutletCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModalOutletTable"
	},
	"lblModalPositive": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		},
		"style": {
			"fontSize": "20px",
			"color": "rgb(74, 134, 237)"
		},
		"parent": "pnlModalBodyButtonPositive"
	},
	"lblModalNegative": {
		"parent": "pnlModalBodyButtonNegative",
		"property": {
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		}
	},
	"pnlModalOutletBodyMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalOutletBodyMain",
			"componentVisible": true
		},
		"style": {
			"padding": "10px 0px",
			"background": "#ffffff",
			"borderBottom": "1px solid #D9D9D9",
			"marginRight": "5%",
			"width": "90%",
			"textAlign": "center",
			"marginLeft": "5%"
		},
		"parent": "pnlModalOutletCell"
	},
	"dlShipToCodeList": {
		"template": {
			"componentDataItemState": {
				"pnlShipToCodeListing": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlShipToCodeListing",
						"componentVisible": true
					},
					"style": {
						"position": "relative",
						"width": "100%",
						"background": "url() no-repeat fixed",
						"borderBottom": "1px solid #D9D9D9",
						"padding": "2px",
						"overflow": "auto"
					}
				},
				"pnlShipToCodeBodyMiddle": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlShipToCodeBodyMiddle",
						"componentVisible": true
					},
					"style": {
						"textAlign": "left",
						"float": "left",
						"width": "100%",
						"padding": "2px"
					},
					"parent": "pnlShipToCodeListing"
				},
				"pnlShipToCodeBodyMiddle1": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlShipToCodeBodyMiddle1",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"width": "100%"
					},
					"parent": "pnlShipToCodeBodyMiddle"
				},
				"lblDataAddress": {
					"style": {
						"fontWeight": "normal",
						"color": "rgb(111, 111, 110)",
						"fontSize": "18px"
					},
					"parent": "pnlShipToCodeBodyMiddle1",
					"property": {
						"componentType": "label",
						"componentName": "lblDataAddress",
						"field": "Code",
						"componentVisible": true
					}
				}
			}
		},
		"property": {
			"componentType": "datalist",
			"componentName": "dlShipToCodeList",
			"componentValue": "l_outlet",
			"autoLoad": false,
			"componentData": [],
			"componentVisible": true
		},
		"parent": "pnlMainBodyShipToCode"
	},
	"pnlModalOutletBodyButtons": {
		"parent": "pnlModalOutletCell",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalOutletBodyButtons",
			"componentVisible": true
		},
		"style": {
			"width": "90%",
			"marginLeft": "5%",
			"marginRight": "5%",
			"textAlign": "center",
			"background": "#ffffff"
		}
	},
	"pnlModalTable": {
		"parent": "pnlModal",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		}
	},
	"pnlModalBodyLoading": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlMainHeaderRight": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRight",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlMainBody1": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody1",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"padding": "5px",
			"borderBottom": "1px solid #D9D9D9",
			"paddingTop": "0px",
			"height": "45px",
			"textAlign": "center"
		},
		"parent": "pnlMainBody"
	},
	"imgModalLoading": {
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		},
		"style": {
			"width": "50px",
			"height": "10px"
		},
		"parent": "pnlModalBodyLoading"
	},
	"imgBack": {
		"parent": "pnlMainHeaderLeftCell",
		"property": {
			"componentType": "image",
			"componentName": "imgBack",
			"componentValue": require("../../asset/icon/icon-menu.png"),
			"componentVisible": true
		},
		"style": {
			"height": "35px",
			"width": "35px"
		}
	},
	"pnlModalOutlet": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalOutlet",
			"componentVisible": false
		},
		"style": {
			"top": "0px",
			"zIndex": "1",
			"background": "rgba(0, 0, 0, 0.60)",
			"position": "fixed",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlMainBodyShipToCode": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyShipToCode",
			"componentVisible": true
		},
		"style": {
			"height": "200px",
			"overflow": "auto",
			"width": "100%"
		},
		"parent": "pnlModalOutletBody1"
	},
	"lblModalOutletCancel": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalOutletCancel",
			"componentValue": "CANCEL",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)"
		},
		"parent": "pnlModalOutletBodyButtonNegative"
	},
	"pnlModalCell": {
		"style": {
			"height": "100%",
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%"
		},
		"parent": "pnlModalTable",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		}
	}
};
