export const StateName = 'pgOrderHistory';
export const State = {
	"txtCustomerName": {
		"property": {
			"componentType": "edit",
			"componentName": "txtCustomerName",
			"placeHolder": "Customer Name",
			"type": "text",
			"disabled": true,
			"componentVisible": true
		},
		"style": {
			"padding": "0px",
			"width": "100%",
			"height": "30px",
			"fontSize": "16px",
			"border": "1px solid #D9D9D9",
			"margin": "0px"
		},
		"parent": "pnlMainBodyForm1Input"
	},
	"pnlModalBodyMessage": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlModalFilterTbl": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalFilterTbl",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table"
		},
		"parent": "pnlModalFilter"
	},
	"pnlModalBodyMessage512": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage512",
			"componentVisible": true
		},
		"style": {
			"marginBottom": "10px",
			"width": "100%",
			"textAlign": "left",
			"padding": "10px"
		},
		"parent": "pnlModalFilterTblBody"
	},
	"pnlMainHeaderLeft": {
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeft",
			"componentVisible": true
		}
	},
	"pnlBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBody",
			"componentVisible": true
		},
		"style": {
			"height": "100%"
		},
		"parent": "pnlMainBody"
	},
	"pnlMainBodyForm2Label": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm2Label",
			"componentVisible": true
		},
		"style": {
			"textAlign": "left",
			"width": "100%"
		},
		"parent": "pnlMainBodyForm2"
	},
	"lblCustomerName22": {
		"property": {
			"componentType": "label",
			"componentName": "lblCustomerName22",
			"componentValue": "Sales Employee",
			"componentVisible": true
		},
		"style": {
			"textAlign": "left",
			"fontWeight": "normal",
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px"
		},
		"parent": "pnlMainBodyForm1Label554794"
	},
	"dpDateFrom": {
		"property": {
			"componentType": "edit",
			"componentName": "dpDateFrom",
			"type": "date",
			"componentVisible": true
		},
		"style": {
			"height": "40px",
			"border": "1px solid #000",
			"width": "100%"
		},
		"parent": "Panel617"
	},
	"pnlMainBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		},
		"style": {
			"height": "calc(100% - 100px - 15px)",
			"overflow": "auto",
			"width": "100%"
		},
		"parent": "pnlMain"
	},
	"pnlUpdateCartModalTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalTable",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"display": "table",
			"width": "100%"
		},
		"parent": "pnlUpdateCartModal"
	},
	"pnlModalTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"display": "table",
			"width": "100%"
		},
		"parent": "pnlModal"
	},
	"lblModalMessage": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(111, 111, 110)",
			"fontSize": "14px"
		},
		"parent": "pnlModalBodyMessage"
	},
	"lblDeliveryDateFrom": {
		"property": {
			"componentType": "label",
			"componentName": "lblDeliveryDateFrom",
			"componentValue": "Req. Delivery Date",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "normal",
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyForm2Label"
	},
	"pnlModalCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModalTable"
	},
	"pnlCSS": {
		"parent": "pnlHolder",
		"property": {
			"componentType": "panel",
			"componentName": "pnlCSS",
			"componentVisible": false
		},
		"style": {
			"zIndex": "4",
			"height": "100%",
			"position": "fixed",
			"display": "none",
			"top": "0px",
			"width": "100%"
		}
	},
	"dpDateTo": {
		"property": {
			"componentType": "edit",
			"componentName": "dpDateTo",
			"type": "date",
			"componentVisible": true
		},
		"style": {
			"border": "1px solid #000",
			"width": "100%",
			"height": "40px"
		},
		"parent": "Panel617854"
	},
	"pnlSelectCustomerMainHeaderRight": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSelectCustomerMainHeaderRight",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"display": "none",
			"float": "right",
			"width": "15%"
		},
		"parent": "pnlSelectCustomerMainHeader"
	},
	"pnlModalBodyButtonPositive": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "50%",
			"textAlign": "center",
			"display": "inline-block",
			"float": "none",
			"padding": "10px 0px"
		},
		"parent": "pnlModalBodyButtons"
	},
	"lblModalTitle437": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle437",
			"componentValue": "Filter By",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"width": "100%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "18px"
		},
		"parent": "pnlModalBodyTitle267"
	},
	"pnlModalBodyButtons738": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons738",
			"componentVisible": true
		},
		"style": {
			"width": "90%",
			"marginLeft": "5%",
			"marginRight": "5%",
			"textAlign": "center",
			"background": "#ffffff"
		},
		"parent": "pnlModalFilterTblCell"
	},
	"pnlMainHeaderRightTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRight"
	},
	"lblSelectOutletCode": {
		"parent": "pnlMainBodyForm1Label551333",
		"property": {
			"componentType": "label",
			"componentName": "lblSelectOutletCode",
			"componentValue": "SELECT",
			"componentVisible": true
		},
		"style": {
			"width": "30%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"fontWeight": "bold",
			"textAlign": "right"
		}
	},
	"pnlSelectCustomerMainHeaderRightCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSelectCustomerMainHeaderRightCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlSelectCustomerMainHeaderRightTable"
	},
	"pnlSelectCustomerMainBody1": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSelectCustomerMainBody1",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"position": "relative",
			"height": "50px",
			"textAlign": "center",
			"borderBottom": "1px solid #D9D9D9",
			"padding": "5px 0px"
		},
		"parent": "pnlSelectCustomerModalBody"
	},
	"imgSelectCustomerBack": {
		"style": {
			"width": "25px",
			"height": "25px"
		},
		"parent": "pnlSelectCustomerMainHeaderLeftCell",
		"property": {
			"componentType": "image",
			"componentName": "imgSelectCustomerBack",
			"componentValue": require("../../asset/icon/icon-back.png"),
			"componentVisible": true
		}
	},
	"pnlMainHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "15px",
			"overflow": "auto",
			"height": "65px",
			"backgroundColor": "rgb(255, 69, 0)",
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlMain"
	},
	"pnlMainHeaderLeftCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftCell",
			"componentVisible": true
		},
		"style": {
			"paddingLeft": "10px",
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%",
			"textAlign": "left"
		},
		"parent": "pnlMainHeaderLeftTable"
	},
	"pnlUpdateCartModalBody1": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBody1",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"height": "15px",
			"marginBottom": "10px",
			"width": "100%"
		},
		"parent": "pnlUpdateCartModalBody"
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"float": "none",
			"textAlign": "center",
			"padding": "10px 0px",
			"width": "50%"
		},
		"parent": "pnlModalBodyButtons"
	},
	"lblStyle": {
		"property": {
			"componentType": "label",
			"componentName": "lblStyle",
			"componentValue": "N/A",
			"componentVisible": true
		},
		"parent": "pnlCSS"
	},
	"lblModalPositive564183217": {
		"parent": "pnlModalBodyButtonPositive165757",
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive564183217",
			"componentValue": "CANCEL",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		}
	},
	"pnlHolder": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"backgroundColor": "rgb(245, 245, 245)",
			"overflow": "auto",
			"width": "100%"
		},
		"parent": "pgOrderHistory"
	},
	"cmbRoute": {
		"property": {
			"componentType": "combobox",
			"componentName": "cmbRoute",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "40px",
			"fontSize": "18px",
			"border": "1px solid #000"
		},
		"parent": "Panel819474"
	},
	"pnlMainBodyForm1Label551333": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm1Label551333",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyForm1481"
	},
	"dlPostedSalesInvoiceList": {
		"property": {
			"componentType": "datalist",
			"componentName": "dlPostedSalesInvoiceList",
			"componentValue": "l_Posted_Sales_Invoice_List",
			"autoLoad": false,
			"componentData": [],
			"componentVisible": true
		},
		"style": {
			"paddingTop": "5px"
		},
		"parent": "pnlBody",
		"template": {
			"componentDataItemState": {
				"Panel212641": {
					"parent": "pnlDataTbl",
					"property": {
						"componentType": "panel",
						"componentName": "Panel212641",
						"componentVisible": true
					},
					"style": {
						"display": "table-cell",
						"verticalAlign": "middle",
						"width": "50px",
						"textAlign": "center"
					}
				},
				"Image318": {
					"style": {
						"width": "25px"
					},
					"parent": "Panel212641",
					"property": {
						"componentType": "image",
						"componentName": "Image318",
						"zoom": false,
						"componentValue": require("../../asset/images/icon-go-gray.png"),
						"componentVisible": true
					}
				},
				"pnlMainDataContainer": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlMainDataContainer",
						"componentVisible": true
					},
					"style": {
						"minWidth": "40px",
						"minHeight": "50px",
						"padding": "5px",
						"paddingBottom": "0px"
					}
				},
				"Panel212": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel212",
						"componentVisible": true
					},
					"style": {
						"padding": "10px",
						"display": "table-cell",
						"verticalAlign": "middle",
						"width": "calc(100% - 50px)"
					},
					"parent": "pnlDataTbl"
				},
				"lblAmount": {
					"property": {
						"componentType": "label",
						"componentName": "lblAmount",
						"componentVisible": true
					},
					"style": {
						"marginTop": "10px",
						"display": "block",
						"color": "rgb(0, 153, 0)",
						"fontSize": "20px"
					},
					"parent": "Panel212"
				},
				"lblPostingDate": {
					"property": {
						"componentType": "label",
						"componentName": "lblPostingDate",
						"componentVisible": true
					},
					"style": {
						"fontSize": "16px",
						"display": "block",
						"color": "rgb(60, 60, 59)"
					},
					"parent": "Panel212"
				},
				"pnlDataTbl": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlDataTbl",
						"componentVisible": true
					},
					"style": {
						"backgroundColor": "rgb(255, 255, 255)",
						"border": "1px solid #3C3C3B",
						"display": "table",
						"width": "100%",
						"boxShadow": "-1px 1px 1px #545454",
						"borderRadius": "5px"
					},
					"parent": "pnlMainDataContainer"
				},
				"lblNo": {
					"parent": "Panel212",
					"property": {
						"componentType": "label",
						"componentName": "lblNo",
						"field": "No",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(60, 60, 59)",
						"fontSize": "20px",
						"fontWeight": "bold",
						"display": "block"
					}
				},
				"lblCustomer": {
					"property": {
						"componentType": "label",
						"componentName": "lblCustomer",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"color": "rgb(60, 60, 59)",
						"fontSize": "18px"
					},
					"parent": "Panel212"
				}
			}
		}
	},
	"pnlUpdateCartModalBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBody",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"padding": "10px",
			"marginLeft": "5%",
			"width": "90%",
			"background": "#ffffff",
			"marginRight": "5%"
		},
		"parent": "pnlUpdateCartModalCell"
	},
	"imgUpdateCartClose": {
		"property": {
			"componentType": "image",
			"componentName": "imgUpdateCartClose",
			"componentValue": require("../../asset/icon/icon-close-gray.png"),
			"componentVisible": true
		},
		"style": {
			"width": "45px",
			"height": "45px",
			"right": "0px",
			"position": "absolute"
		},
		"parent": "pnlUpdateCartModalBody1"
	},
	"lblSelectCustomer": {
		"parent": "pnlMainBodyForm1Label",
		"property": {
			"componentType": "label",
			"componentName": "lblSelectCustomer",
			"componentValue": "SELECT",
			"componentVisible": true
		},
		"style": {
			"width": "30%",
			"color": "rgb(60, 60, 59)",
			"fontWeight": "bold",
			"textAlign": "right",
			"fontSize": "16px"
		}
	},
	"Button986": {
		"style": {
			"float": "right",
			"height": "40px",
			"backgroundColor": "rgb(111, 111, 110)",
			"width": "80px",
			"border": "1px solid #000"
		},
		"parent": "Panel819",
		"property": {
			"componentType": "fbutton",
			"componentName": "Button986",
			"componentValue": "Browse",
			"componentVisible": true
		}
	},
	"pnlSelectCustomerMainHeaderMiddleTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSelectCustomerMainHeaderMiddleTable",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table"
		},
		"parent": "pnlSelectCustomerMainHeaderMiddle"
	},
	"pnlMainHeaderLeftTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderLeft"
	},
	"pnlMainBodyForm1Input": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm1Input",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyForm1"
	},
	"cmbSalesperson": {
		"property": {
			"componentType": "combobox",
			"componentName": "cmbSalesperson",
			"componentVisible": true
		},
		"style": {
			"border": "1px solid #D9D9D9",
			"margin": "0px"
		},
		"parent": "pnlMainBodyForm1Input696752"
	},
	"pnlUpdateCartModalBody5": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBody5",
			"componentVisible": true
		},
		"style": {
			"marginTop": "20px",
			"marginBottom": "10px",
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlUpdateCartModalBody"
	},
	"pnlSelectCustomerMainBody2": {
		"style": {
			"overflow": "auto",
			"paddingLeft": "5%",
			"position": "absolute",
			"width": "100%",
			"height": "calc(100% - 100px)",
			"padding": "2%",
			"paddingRight": "5%"
		},
		"parent": "pnlSelectCustomerModalBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlSelectCustomerMainBody2",
			"componentVisible": true
		}
	},
	"pnlMainHeaderMiddleCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddleTable"
	},
	"pnlModalBodyLoading": {
		"parent": "pnlModalBodyMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		}
	},
	"Label377810993": {
		"property": {
			"componentType": "label",
			"componentName": "Label377810993",
			"componentValue": "Label",
			"componentVisible": true
		},
		"style": {
			"display": "none",
			"width": "100%",
			"height": "45px"
		},
		"parent": "Panel819474"
	},
	"pnlUpdateCartModal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModal",
			"componentVisible": false
		},
		"style": {
			"width": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"position": "fixed",
			"top": "0px",
			"zIndex": "1",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"txtOutletCode": {
		"property": {
			"componentType": "edit",
			"componentName": "txtOutletCode",
			"type": "text",
			"placeHolder": "Outlet Code",
			"disabled": true,
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"border": "1px solid #D9D9D9",
			"height": "30px",
			"fontSize": "16px",
			"padding": "0px",
			"margin": "0px"
		},
		"parent": "pnlMainBodyForm1Input180830"
	},
	"txtDateFrom": {
		"style": {
			"display": "inline-block",
			"backgroundColor": "rgba(255, 255, 255, 0.98)",
			"border": "1px solid #D9D9D9",
			"width": "45%",
			"color": "rgb(0, 0, 0)",
			"fontSize": "16px",
			"margin": "0px",
			"height": "30px",
			"padding": "0px"
		},
		"parent": "pnlMainBodyForm2Input",
		"property": {
			"componentType": "edit",
			"componentName": "txtDateFrom",
			"type": "date",
			"disabled": false,
			"componentVisible": true
		}
	},
	"lblDocumentDate": {
		"property": {
			"componentType": "label",
			"componentName": "lblDocumentDate",
			"componentValue": "Document Date",
			"componentVisible": true
		},
		"style": {
			"fontSize": "16px",
			"fontWeight": "normal",
			"textAlign": "left",
			"color": "rgb(60, 60, 59)"
		},
		"parent": "pnlMainBodyForm2Label820995"
	},
	"pnlSelectCustomerModal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSelectCustomerModal",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"zIndex": "2",
			"width": "100%",
			"height": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"top": "0px"
		},
		"parent": "pnlHolder"
	},
	"pnlSelectCustomerModalBody": {
		"style": {
			"width": "90%",
			"height": "90%",
			"marginLeft": "5%",
			"position": "relative",
			"textAlign": "center",
			"background": "#ffffff",
			"marginRight": "5%"
		},
		"parent": "pnlSelectCustomerModalCell",
		"property": {
			"componentType": "panel",
			"componentName": "pnlSelectCustomerModalBody",
			"componentVisible": true
		}
	},
	"pnlMainBodySearch": {
		"parent": "pnlMainSearch",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodySearch",
			"componentVisible": true
		},
		"style": {
			"overflow": "auto",
			"width": "100%",
			"padding": "5px"
		}
	},
	"Label411": {
		"property": {
			"componentType": "label",
			"componentName": "Label411",
			"componentValue": "Date From",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"fontSize": "16px"
		},
		"parent": "Panel617"
	},
	"Label41195247": {
		"property": {
			"componentType": "label",
			"componentName": "Label41195247",
			"componentValue": "Date To",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"fontSize": "16px"
		},
		"parent": "Panel617854"
	},
	"lblModalPositive564": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive564",
			"componentValue": "SAVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonPositive165"
	},
	"pnlMainBodyForm1": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm1",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"marginBottom": "10px",
			"width": "100%"
		},
		"parent": "pnlUpdateCartModalBody2"
	},
	"lblCustomerName": {
		"property": {
			"componentType": "label",
			"componentName": "lblCustomerName",
			"componentValue": "Customer Name",
			"componentVisible": true
		},
		"style": {
			"fontSize": "16px",
			"fontWeight": "normal",
			"width": "70%",
			"color": "rgb(60, 60, 59)",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyForm1Label"
	},
	"pnlSelectCustomerModalCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSelectCustomerModalCell",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"position": "relative",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlSelectCustomerModalTable"
	},
	"txtSelectCustomerSearch": {
		"property": {
			"componentType": "edit",
			"componentName": "txtSelectCustomerSearch",
			"placeHolder": "Search",
			"type": "search",
			"componentVisible": true
		},
		"style": {
			"height": "30px",
			"textAlign": "left",
			"backgroundColor": "rgb(255, 255, 255)",
			"backgroundPosition": "left .5rem center",
			"display": "inline",
			"width": "90%",
			"backgroundSize": "16px",
			"paddingLeft": "30px",
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"background": "url('data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Capa_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 451 451\" style=\"enable-background:new 0 0 451 451;\" xml:space=\"preserve\" width=\"512px\" height=\"512px\"><g><path d=\"M447.05,428l-109.6-109.6c29.4-33.8,47.2-77.9,47.2-126.1C384.65,86.2,298.35,0,192.35,0C86.25,0,0.05,86.3,0.05,192.3   s86.3,192.3,192.3,192.3c48.2,0,92.3-17.8,126.1-47.2L428.05,447c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4   C452.25,441.8,452.25,433.2,447.05,428z M26.95,192.3c0-91.2,74.2-165.3,165.3-165.3c91.2,0,165.3,74.2,165.3,165.3   s-74.1,165.4-165.3,165.4C101.15,357.7,26.95,283.5,26.95,192.3z\" fill=\"#3c3c3b\"/></g></svg>') no-repeat",
			"border": "1px solid #D9D9D9",
			"margin": "0px"
		},
		"parent": "pnlSelectCustomerMainBodySearch"
	},
	"pnlMain": {
		"style": {
			"zIndex": "0",
			"height": "100%",
			"width": "100%"
		},
		"parent": "pnlHolder",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		}
	},
	"pnlMainBodyForm2Label820995": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm2Label820995",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyForm2498"
	},
	"pnlModalFilter": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalFilter",
			"componentVisible": false
		},
		"style": {
			"background": "rgba(0, 0, 0, 0.60)",
			"zIndex": "5",
			"width": "100%",
			"height": "100%",
			"position": "fixed",
			"top": "0px"
		},
		"parent": "pnlHolder"
	},
	"Label377": {
		"parent": "Panel819",
		"property": {
			"componentType": "label",
			"componentName": "Label377",
			"componentValue": "Label",
			"componentVisible": true
		},
		"style": {
			"display": "none",
			"width": "100%",
			"height": "45px"
		}
	},
	"pnlSelectCustomerModalTable": {
		"parent": "pnlSelectCustomerModal",
		"property": {
			"componentType": "panel",
			"componentName": "pnlSelectCustomerModalTable",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"display": "table",
			"width": "100%",
			"height": "100%"
		}
	},
	"pnlMainBodyForm1Input696752": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm1Input696752",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"width": "100%"
		},
		"parent": "pnlMainBodyForm3"
	},
	"pnlModalBodyButtonPositive165757": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive165757",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"display": "inline-block",
			"width": "50%",
			"float": "none",
			"padding": "10px 0px"
		},
		"parent": "pnlModalBodyButtons738"
	},
	"lblModalMessage602144736": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage602144736",
			"componentValue": "Route",
			"componentVisible": true
		},
		"style": {
			"fontSize": "16px",
			"display": "block",
			"color": "rgb(0, 0, 0)"
		},
		"parent": "Panel819474"
	},
	"imgUpdateCartImage": {
		"parent": "pnlUpdateCartModalBody1",
		"property": {
			"componentType": "image",
			"componentName": "imgUpdateCartImage",
			"componentValue": require("../../asset/images/default.png"),
			"componentVisible": true
		},
		"style": {
			"width": "150px",
			"display": "none"
		}
	},
	"txtDocumentDateFrom": {
		"property": {
			"componentType": "edit",
			"componentName": "txtDocumentDateFrom",
			"disabled": false,
			"type": "date",
			"componentVisible": true
		},
		"style": {
			"fontSize": "16px",
			"padding": "0px",
			"display": "inline-block",
			"height": "30px",
			"margin": "0px",
			"color": "rgb(0, 0, 0)",
			"border": "1px solid #D9D9D9",
			"width": "45%",
			"backgroundColor": "rgba(255, 255, 255, 0.98)"
		},
		"parent": "pnlMainBodyForm2Input739198"
	},
	"lblModalPositive": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)"
		},
		"parent": "pnlModalBodyButtonPositive"
	},
	"pnlModalBodyTitle267": {
		"parent": "pnlModalFilterTblBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle267",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		}
	},
	"Panel617854": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel617854",
			"componentVisible": true
		},
		"style": {
			"minWidth": "40px",
			"minHeight": "50px",
			"marginTop": "10px"
		},
		"parent": "pnlModalBodyMessage512"
	},
	"pnlMainBodyForm2Input739198": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm2Input739198",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyForm2498"
	},
	"Panel617": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel617",
			"componentVisible": true
		},
		"style": {
			"minHeight": "50px",
			"minWidth": "40px"
		},
		"parent": "pnlModalBodyMessage512"
	},
	"imgSearchFilters": {
		"property": {
			"componentType": "image",
			"componentName": "imgSearchFilters",
			"componentValue": require("../../asset/Filter-Funnel-White.png"),
			"componentVisible": true
		},
		"style": {
			"width": "35px",
			"height": "35px",
			"padding": "2px"
		},
		"parent": "pnlMainHeaderRightCell"
	},
	"lblModalTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "14px",
			"fontWeight": "bold"
		},
		"parent": "pnlModalBodyTitle"
	},
	"pnlSelectCustomerMainHeaderMiddle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSelectCustomerMainHeaderMiddle",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"float": "left",
			"width": "70%",
			"height": "100%"
		},
		"parent": "pnlSelectCustomerMainHeader"
	},
	"lblAll": {
		"property": {
			"componentType": "label",
			"componentName": "lblAll",
			"componentValue": "All",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "500",
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "16px"
		},
		"parent": "pnlSelectCustomerMainHeaderRightCell"
	},
	"imgBack": {
		"parent": "pnlMainHeaderLeftCell",
		"property": {
			"componentType": "image",
			"componentName": "imgBack",
			"componentValue": require("../../asset/icon/icon-back.png"),
			"componentVisible": true
		},
		"style": {
			"height": "35px",
			"width": "35px"
		}
	},
	"pnlMainBodyForm2498": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm2498",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"marginBottom": "10px"
		},
		"parent": "pnlUpdateCartModalBody2"
	},
	"pnlModal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"height": "100%",
			"position": "fixed",
			"top": "0px",
			"zIndex": "3",
			"width": "100%",
			"background": "rgba(0, 0, 0, 0.60)"
		},
		"parent": "pnlHolder"
	},
	"lblSelectCustomerMainTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblSelectCustomerMainTitle",
			"componentValue": "CUSTOMER LIST",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500"
		},
		"parent": "pnlSelectCustomerMainHeaderMiddleCell"
	},
	"lblMainTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle",
			"componentValue": "INVOICE HISTORY",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500"
		},
		"parent": "pnlMainHeaderMiddleCell"
	},
	"pnlMainSearch": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainSearch",
			"componentVisible": true
		},
		"style": {
			"height": "50px",
			"textAlign": "center",
			"width": "100%",
			"borderBottom": "1px solid #D9D9D9"
		},
		"parent": "pnlMain"
	},
	"pnlModalBodyButtons": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"marginLeft": "5%",
			"textAlign": "center",
			"background": "#ffffff",
			"marginRight": "5%",
			"width": "90%"
		},
		"parent": "pnlModalCell"
	},
	"pnlUpdateCartModalBody2": {
		"style": {
			"marginBottom": "10px",
			"width": "100%",
			"paddingTop": "20px"
		},
		"parent": "pnlUpdateCartModalBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBody2",
			"componentVisible": true
		}
	},
	"pnlMainBodyForm2": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm2",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"marginBottom": "10px"
		},
		"parent": "pnlUpdateCartModalBody2"
	},
	"imgModalLoading": {
		"parent": "pnlModalBodyLoading",
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		},
		"style": {
			"width": "50px",
			"height": "10px"
		}
	},
	"pnlModalFilterTblCell": {
		"parent": "pnlModalFilterTbl",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalFilterTblCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		}
	},
	"Panel819474": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel819474",
			"componentVisible": true
		},
		"parent": "pnlModalBodyMessage512"
	},
	"pnlSelectCustomerMainHeaderLeftTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSelectCustomerMainHeaderLeftTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlSelectCustomerMainHeaderLeft"
	},
	"pnlMainBodyForm1481": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm1481",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"marginBottom": "10px",
			"width": "100%"
		},
		"parent": "pnlUpdateCartModalBody2"
	},
	"pnlMainBodyForm1Label554794": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm1Label554794",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyForm3"
	},
	"lblDocumentDateTo851": {
		"property": {
			"componentType": "label",
			"componentName": "lblDocumentDateTo851",
			"componentValue": "To",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "normal",
			"textAlign": "center",
			"width": "10%",
			"color": "rgb(60, 60, 59)",
			"display": "inline-block",
			"fontSize": "12px"
		},
		"parent": "pnlMainBodyForm2Input739198"
	},
	"txtDocumentDateTo": {
		"property": {
			"componentType": "edit",
			"componentName": "txtDocumentDateTo",
			"disabled": false,
			"type": "date",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgba(255, 255, 255, 0.98)",
			"display": "inline-block",
			"color": "rgb(0, 0, 0)",
			"fontSize": "16px",
			"width": "45%",
			"height": "30px",
			"border": "1px solid #D9D9D9",
			"padding": "0px",
			"margin": "0px"
		},
		"parent": "pnlMainBodyForm2Input739198"
	},
	"pnlSelectCustomerMainHeaderLeftCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSelectCustomerMainHeaderLeftCell",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlSelectCustomerMainHeaderLeftTable"
	},
	"pnlSelectCustomerMainBodySearch": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSelectCustomerMainBodySearch",
			"componentVisible": true
		},
		"style": {
			"padding": "5px 0px",
			"overflow": "auto",
			"width": "100%"
		},
		"parent": "pnlSelectCustomerMainBody1"
	},
	"pnlMainHeaderRight": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRight",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"txtSearch": {
		"parent": "pnlMainBodySearch",
		"property": {
			"componentType": "edit",
			"componentName": "txtSearch",
			"placeHolder": "Search",
			"type": "search",
			"backgroundAsset": "icon/_search.png",
			"componentVisible": true
		},
		"style": {
			"height": "40px",
			"fontSize": "16px",
			"border": "1px solid #D9D9D9",
			"display": "inline",
			"backgroundColor": "rgb(255, 255, 255)",
			"margin": "0px",
			"textAlign": "left",
			"background": "url('data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Capa_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 451 451\" style=\"enable-background:new 0 0 451 451;\" xml:space=\"preserve\" width=\"512px\" height=\"512px\"><g><path d=\"M447.05,428l-109.6-109.6c29.4-33.8,47.2-77.9,47.2-126.1C384.65,86.2,298.35,0,192.35,0C86.25,0,0.05,86.3,0.05,192.3   s86.3,192.3,192.3,192.3c48.2,0,92.3-17.8,126.1-47.2L428.05,447c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4   C452.25,441.8,452.25,433.2,447.05,428z M26.95,192.3c0-91.2,74.2-165.3,165.3-165.3c91.2,0,165.3,74.2,165.3,165.3   s-74.1,165.4-165.3,165.4C101.15,357.7,26.95,283.5,26.95,192.3z\" fill=\"#3c3c3b\"/></g></svg>') no-repeat",
			"backgroundSize": "16px",
			"paddingLeft": "30px",
			"width": "100%",
			"color": "rgb(60, 60, 59)",
			"backgroundPosition": "left .5rem center"
		}
	},
	"pnlModalBodyMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		},
		"style": {
			"width": "90%",
			"textAlign": "center",
			"marginRight": "5%",
			"background": "#ffffff",
			"borderBottom": "1px solid #D9D9D9",
			"padding": "10px 0px",
			"marginLeft": "5%"
		},
		"parent": "pnlModalCell"
	},
	"pnlSelectCustomerMainHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSelectCustomerMainHeader",
			"componentVisible": true
		},
		"style": {
			"height": "50px",
			"textAlign": "center",
			"overflow": "auto",
			"width": "100%",
			"backgroundColor": "rgba(242, 34, 34, 0.95)",
			"position": "relative"
		},
		"parent": "pnlSelectCustomerModalBody"
	},
	"txtDateTo": {
		"property": {
			"componentType": "edit",
			"componentName": "txtDateTo",
			"type": "date",
			"disabled": false,
			"componentVisible": true
		},
		"style": {
			"border": "1px solid #D9D9D9",
			"backgroundColor": "rgba(255, 255, 255, 0.98)",
			"padding": "0px",
			"height": "30px",
			"color": "rgb(0, 0, 0)",
			"fontSize": "16px",
			"width": "45%",
			"margin": "0px",
			"display": "inline-block"
		},
		"parent": "pnlMainBodyForm2Input"
	},
	"cmbCustomer": {
		"parent": "Panel819",
		"property": {
			"componentType": "combobox",
			"componentName": "cmbCustomer",
			"componentVisible": true
		},
		"style": {
			"height": "40px",
			"fontSize": "18px",
			"border": "1px solid #000",
			"width": "calc(100% - 80px)"
		}
	},
	"pnlModalFilterTblBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalFilterTblBody",
			"componentVisible": true
		},
		"style": {
			"padding": "10px 0px",
			"background": "#ffffff",
			"borderBottom": "1px solid #D9D9D9",
			"marginLeft": "5%",
			"marginRight": "5%",
			"width": "90%",
			"textAlign": "center"
		},
		"parent": "pnlModalFilterTblCell"
	},
	"pnlSelectCustomerMainHeaderLeft": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSelectCustomerMainHeaderLeft",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlSelectCustomerMainHeader"
	},
	"pnlMainHeaderMiddle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddle",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "70%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlMainHeaderRightCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightCell",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"paddingRight": "10px",
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"textAlign": "right"
		},
		"parent": "pnlMainHeaderRightTable"
	},
	"pnlUpdateCartModalCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlUpdateCartModalTable"
	},
	"pnlMainBodyForm1Label": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm1Label",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyForm1"
	},
	"pnlMainBodyForm2Input": {
		"parent": "pnlMainBodyForm2",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm2Input",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center"
		}
	},
	"pnlMainBodyForm3": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm3",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"marginBottom": "10px"
		},
		"parent": "pnlUpdateCartModalBody2"
	},
	"pnlSelectCustomerMainHeaderMiddleCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSelectCustomerMainHeaderMiddleCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlSelectCustomerMainHeaderMiddleTable"
	},
	"pgOrderHistory": {
		"property": {
			"componentType": "page",
			"componentName": "pgOrderHistory",
			"title": "OrderHistory",
			"componentVisible": true
		}
	},
	"pnlMainHeaderMiddleTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTable",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table"
		},
		"parent": "pnlMainHeaderMiddle"
	},
	"lblOutletCode": {
		"property": {
			"componentType": "label",
			"componentName": "lblOutletCode",
			"componentValue": "Outlet Code",
			"componentVisible": true
		},
		"style": {
			"textAlign": "left",
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"fontWeight": "normal",
			"width": "70%"
		},
		"parent": "pnlMainBodyForm1Label551333"
	},
	"btnSearch": {
		"property": {
			"componentType": "fbutton",
			"componentName": "btnSearch",
			"componentValue": "Search",
			"componentVisible": true
		},
		"style": {
			"margin": "0px",
			"fontWeight": "bold",
			"backgroundColor": "rgb(242, 114, 34)",
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"padding": "8px",
			"width": "80%"
		},
		"parent": "pnlUpdateCartModalBody5"
	},
	"pnlModalBodyTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"marginBottom": "10px",
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlSelectCustomerMainHeaderRightTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSelectCustomerMainHeaderRightTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlSelectCustomerMainHeaderRight"
	},
	"pnlMainBodyForm1Input180830": {
		"parent": "pnlMainBodyForm1481",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm1Input180830",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center"
		}
	},
	"lblDocumentDateTo": {
		"parent": "pnlMainBodyForm2Input",
		"property": {
			"componentType": "label",
			"componentName": "lblDocumentDateTo",
			"componentValue": "To",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"width": "10%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "12px",
			"fontWeight": "normal",
			"textAlign": "center"
		}
	},
	"lblModalNegative": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)"
		},
		"parent": "pnlModalBodyButtonNegative"
	},
	"lblModalMessage602": {
		"parent": "Panel819",
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage602",
			"componentValue": "Select Customer",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(0, 0, 0)",
			"fontSize": "16px"
		}
	},
	"pnlModalBodyButtonPositive165": {
		"style": {
			"width": "50%",
			"padding": "10px 0px",
			"display": "inline-block",
			"float": "none",
			"textAlign": "center"
		},
		"parent": "pnlModalBodyButtons738",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive165",
			"componentVisible": true
		}
	},
	"Panel819": {
		"parent": "pnlModalBodyMessage512",
		"property": {
			"componentType": "panel",
			"componentName": "Panel819",
			"componentVisible": true
		}
	},
	"dlCustomerList": {
		"property": {
			"componentType": "datalist",
			"componentName": "dlCustomerList",
			"componentValue": "l_customer",
			"autoLoad": false,
			"componentData": [],
			"componentVisible": true
		},
		"parent": "pnlSelectCustomerMainBody2",
		"template": {
			"componentDataItemState": {
				"pnlSelectCustomerRight1Cell": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlSelectCustomerRight1Cell",
						"componentVisible": true
					},
					"style": {
						"display": "table-cell",
						"verticalAlign": "middle",
						"width": "100%",
						"height": "100%"
					},
					"parent": "pnlSelectCustomerRight1Table"
				},
				"pnlSelectCustomerListing": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlSelectCustomerListing",
						"componentVisible": true
					},
					"style": {
						"overflow": "auto",
						"borderBottom": "1px solid #D9D9D9",
						"padding": "5px",
						"position": "relative",
						"width": "100%",
						"background": "url() no-repeat fixed"
					}
				},
				"pnlSelectCustomerBodyLeft": {
					"parent": "pnlSelectCustomerListing",
					"property": {
						"componentType": "panel",
						"componentName": "pnlSelectCustomerBodyLeft",
						"componentVisible": true
					},
					"style": {
						"float": "left",
						"width": "100%",
						"textAlign": "left",
						"padding": "10px"
					}
				},
				"pnlSelectCustomerBodyLeft1": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlSelectCustomerBodyLeft1",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"width": "100%"
					},
					"parent": "pnlSelectCustomerBodyLeft"
				},
				"lblName": {
					"property": {
						"componentType": "label",
						"componentName": "lblName",
						"field": "Name",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"fontWeight": "normal",
						"color": "rgb(111, 111, 110)",
						"fontSize": "18px"
					},
					"parent": "pnlSelectCustomerBodyLeft1"
				},
				"pnlSelectCustomerBodyMiddle1": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlSelectCustomerBodyMiddle1",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"width": "100%"
					},
					"parent": "pnlSelectCustomerBodyMiddle"
				},
				"pnlSelectCustomerBodyRight": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlSelectCustomerBodyRight",
						"componentVisible": true
					},
					"style": {
						"float": "left",
						"display": "none",
						"width": "20px"
					},
					"parent": "pnlSelectCustomerListing"
				},
				"pnlSelectCustomerBodyRight1": {
					"parent": "pnlSelectCustomerBodyRight",
					"property": {
						"componentType": "panel",
						"componentName": "pnlSelectCustomerBodyRight1",
						"componentVisible": true
					},
					"style": {
						"position": "absolute",
						"top": "0",
						"right": "0px",
						"display": "block",
						"zIndex": "1",
						"height": "100%"
					}
				},
				"imgGo": {
					"property": {
						"componentType": "image",
						"componentName": "imgGo",
						"componentValue": require("../../asset/public/images/default.png"),
						"componentVisible": true
					},
					"style": {
						"width": "20px",
						"height": "20px"
					},
					"parent": "pnlSelectCustomerRight1Cell"
				},
				"lblCardCode": {
					"property": {
						"componentType": "label",
						"componentName": "lblCardCode",
						"field": "No",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(60, 60, 59)",
						"fontWeight": "normal",
						"display": "block",
						"fontSize": "18px"
					},
					"parent": "pnlSelectCustomerBodyLeft1"
				},
				"lblPaymTerms": {
					"property": {
						"componentType": "label",
						"componentName": "lblPaymTerms",
						"field": "Payment_Terms_Code",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"color": "rgb(111, 111, 110)",
						"fontSize": "18px",
						"fontWeight": "normal",
						"marginTop": "10px"
					},
					"parent": "pnlSelectCustomerBodyLeft1"
				},
				"pnlSelectCustomerBodyMiddle": {
					"parent": "pnlSelectCustomerListing",
					"property": {
						"componentType": "panel",
						"componentName": "pnlSelectCustomerBodyMiddle",
						"componentVisible": true
					},
					"style": {
						"padding": "10px",
						"float": "none",
						"width": "50%",
						"textAlign": "left"
					}
				},
				"pnlSelectCustomerRight1Table": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlSelectCustomerRight1Table",
						"componentVisible": true
					},
					"style": {
						"display": "table",
						"width": "100%",
						"height": "100%"
					},
					"parent": "pnlSelectCustomerBodyRight1"
				}
			}
		}
	}
};
