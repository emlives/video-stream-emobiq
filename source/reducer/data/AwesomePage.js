export const StateName = "AwesomePage";
export const State = {
	page: {
		style: {
			flex: 1,
			backgroundColor: 'seashell',
			alignItems: 'center',
		},
		property: {
			componentDisabled: true,
		}
	},
	panelB: {
		style: {
			padding: 20,
			backgroundColor: 'springgreen',
			width: '90%',
			marginBottom: 20,
			justifyContent: 'space-evenly',
		},
	},
	panelA: {
		style: {
			backgroundColor: 'yellow',
			width: '90%',
			marginBottom: 20,
			flexDirection: 'row',
			padding: 20,
			alignItems: 'center',
		},
	},
	panelC: {
		style: {
			backgroundColor: 'pink',
			width: '90%',
			height: 150,
			flexDirection: 'column',
			margin: 20,
		},
	},
	tfComments: {
		style: {
			backgroundColor: 'white',
			textAlign: 'center',
			paddingLeft: 20,
			paddingRight: 20,
		},
		property: {
			componentValue: 'Enter your comments here',
			componentVisible: true,
		}
	},
	lblTitle: {
		style: {
			width: '100%',
			color: 'black',
			textAlign: 'center',
			padding: 15,
			fontSize: 19,
			fontWeight: 'bold',
		},
		property: {
			componentValue: 'Title',
			componentVisible: true,
		}
	},
	lblComments: {
		style: {
			color: 'black',
			padding: 5,
			fontWeight: 'bold',
		},
		property: {
			componentValue: 'Comments',
			componentVisible: true,
		}
	},
	lblAppleURL: {
		style: {
			color: 'black',
			padding: 5,
			fontSize: 19,
			fontWeight: 'bold',
		},
		property: {
			componentValue: 'www.apple.com',
			componentVisible: true,
		}
	},
	btnOpenURL: {
		style: {
			marginTop: 50,
			width: '100%',
			color: 'yellow',
			marginBottom: 50,
		},
		property: {
			componentValue: 'Open URL',
			componentVisible: true,
		}
	},
	btnDismissKeyboard: {
		style: {
			marginTop: 50,
			width: '100%',
			color: 'springgreen',
		},
		property: {
			componentValue: 'Hide Keyboard',
			componentVisible: true,
		}
	},
	btnHideOpenURL: {
		style: {
			marginTop: 50,
			width: '100%',
			color: 'springgreen',
		},
		property: {
			componentValue: 'Toogle Button Visibility',
			componentVisible: true,
		}
	},
	lblIsElementShown: {
		style: {
			color: 'black',
			padding: 5,
			marginBottom: 50,
		},
		property: {
			componentValue: 'Open-URL button is visible',
			componentVisible: true,
		}
	},
	btnSetComponentLink: {
		style: {
			marginTop: 50,
			width: '100%',
			color: 'yellow',
			marginBottom: 50,
		},
		property: {
			componentValue: 'Set Component Link',
			componentVisible: true,
		}
	},

};