export const StateName = 'TestPage';
export const State = {
    TestPage: {
        property: {
            componentUUID: "abcdef",
            componentType: "page",
            componentName: "TestPage"
        }
    },
    pnlMain: {
        property: {
            componentUUID: "abcdef123",
            componentType: "panel",
            componentName: "pnlMain"
        }
    },
    lblName: {
        style: {
            color: "red",
            paddingTop: "10px",
            paddingBottom: "10px",
            fontStyle: "italic"
        },
        property: {
            componentUUID: "abcdef678",
            componentType: "label",
            componentValue: "Testing",
            componentName: "lblName"
        }
    },
    lblName2: {
        property: {
            componentUUID: "abcdef6789",
            componentType: "label",
            componentValue: "Testing2",
            componentName: "lblName2"
        }
    },
    memDesc: {
        property: {
            rows: 10,
            cols: 10,
            componentUUID: "abcdef67890",
            componentType: "memo",
            componentValue: "Lorem ipsum dolor sit amit",
            componentName: "memDesc"
        }
    },
    txtEdit: {
        property: {
            componentUUID: "abcdef67891",
            componentType: "edit",
            componentValue: "Lorem ipsum dolor sit amit",
            componentName: "txtEdit"
        }
    },
    imgTest: {
        style: {
            width: "50%"
        },
        property: {
            componentUUID: "abcdef67892",
            componentType: "image",
            componentName: "imgTest"
        }
    },
    btnTest: {
        style: {
            width: "50%"
        },
        property: {
            componentUUID: "abcdef67893",
            componentType: "button",
            componentName: "btnTest",
            componentValue: "Submit"
        }
    },
    cbTest: {
        property: {
            componentUUID: "abcdef67894",
            componentType: "checkbox",
            componentName: "cbTest",
            componentValue: true
        }
    },
    cmbTest: {
        property: {
            componentUUID: "abcdef67895",
            componentType: "combobox",
            componentName: "cmbTest",
            componentOptions: [{"label": "XXRAJ174303023", "value": "B0:91:22:EF:1C:A2"}, 
            {"label": "QLN320A3", "value": "AC:3F:A4:0C:6F:BF"}]
        }
    },
    pnlTest: {
        property: {
            componentUUID: "abcdef67896",
            componentType: "panel",
            componentName: "pnlTest",
        }
    },
    btnPanelTest: {
        property: {
            componentUUID: "abcdef67897",
            componentType: "button",
            componentName: "btnPanelTest",
            componentValue: "Submit in Panel"
        }
    },
    sigTest: {
        property: {
            componentUUID: "abcdef67898",
            componentType: "button",
            componentName: "sigTest",
            componentValue: "Signature"
        }
    },
    btnTestFlatListEndReachedPage :{
        property: {
            componentUUID: "abcdef67899",
            componentType: "button",
            componentName: "btnTestFlatListEndReachedPage",
            componentValue: "Test FlatList End Reach"
        }

    }
};