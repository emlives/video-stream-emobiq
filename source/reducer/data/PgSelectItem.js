export const StateName = 'pgSelectItem';
export const State = {
	"pnlMainHeaderMiddle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddle",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"float": "left",
			"width": "70%"
		},
		"parent": "pnlMainHeader"
	},
	"lblUpdateSalesReturnLinesQuantity": {
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateSalesReturnLinesQuantity",
			"componentValue": "Quantity",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"textAlign": "center",
			"display": "block",
			"fontSize": "16px",
			"fontWeight": "bolder"
		},
		"parent": "pnlUpdateSalesReturnLinesQtyContent3"
	},
	"pnlUpdateSalesReturnLinesQtyCtrlTableCell2": {
		"parent": "pnlUpdateSalesReturnLinesQtyControl",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesQtyCtrlTableCell2",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"width": "calc(100% - 120px)"
		}
	},
	"lblModalMessage": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(131, 136, 146)",
			"fontSize": "16px"
		},
		"parent": "pnlModalBodyMessage"
	},
	"pnlSearch": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSearch",
			"componentVisible": true
		},
		"style": {
			"padding": "5px",
			"overflow": "auto",
			"width": "100%"
		},
		"parent": "pnlMainBodySearch"
	},
	"txtSearch": {
		"property": {
			"componentType": "edit",
			"componentName": "txtSearch",
			"backgroundAsset": "icon/_search.png",
			"type": "search",
			"placeHolder": "Search",
			"componentVisible": true
		},
		"style": {
			"backgroundPosition": "left .5rem center",
			"display": "inline",
			"height": "40px",
			"color": "rgb(60, 60, 59)",
			"textAlign": "left",
			"backgroundSize": "16px",
			"paddingLeft": "30px",
			"margin": "0px",
			"width": "100%",
			"fontSize": "16px",
			"background": "url('data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Capa_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 451 451\" style=\"enable-background:new 0 0 451 451;\" xml:space=\"preserve\" width=\"512px\" height=\"512px\"><g><path d=\"M447.05,428l-109.6-109.6c29.4-33.8,47.2-77.9,47.2-126.1C384.65,86.2,298.35,0,192.35,0C86.25,0,0.05,86.3,0.05,192.3   s86.3,192.3,192.3,192.3c48.2,0,92.3-17.8,126.1-47.2L428.05,447c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4   C452.25,441.8,452.25,433.2,447.05,428z M26.95,192.3c0-91.2,74.2-165.3,165.3-165.3c91.2,0,165.3,74.2,165.3,165.3   s-74.1,165.4-165.3,165.4C101.15,357.7,26.95,283.5,26.95,192.3z\" fill=\"#3c3c3b\"/></g></svg>') no-repeat",
			"border": "1px solid #D9D9D9",
			"backgroundColor": "rgb(255, 255, 255)"
		},
		"parent": "pnlSearch"
	},
	"pnlUpdateSalesReturnLinesQtyControl": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesQtyControl",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"height": "40px",
			"width": "100%"
		},
		"parent": "pnlUpdateSalesReturnLinesQtyContent2"
	},
	"pnlUpdateSalesReturnLinesQtyCtrlTableCell1": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesQtyCtrlTableCell1",
			"componentVisible": true
		},
		"style": {
			"width": "50px",
			"backgroundColor": "rgb(255, 69, 0)",
			"display": "table-cell",
			"height": "40px"
		},
		"parent": "pnlUpdateSalesReturnLinesQtyControl"
	},
	"imgMinus": {
		"property": {
			"componentType": "image",
			"componentName": "imgMinus",
			"componentValue": require("../../asset/minus-white.png"),
			"componentVisible": true
		},
		"style": {
			"margin": "0px",
			"float": "left",
			"width": "50px",
			"height": "40px"
		},
		"parent": "pnlUpdateSalesReturnLinesQtyCtrlTableCell1"
	},
	"pnlUpdateSalesReturnLinesContent3": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesContent3",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"width": "100%"
		},
		"parent": "Panel45926"
	},
	"pnlUpdateSalesReturnLinesBtnAdd": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesBtnAdd",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"textAlign": "center"
		},
		"parent": "pnlUpdateSalesReturnLinesFooterBtn"
	},
	"pnlModalBodyButtonPositive": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBody"
	},
	"btnModalPositive": {
		"style": {
			"width": "80%",
			"fontWeight": "bold",
			"margin": "0px"
		},
		"parent": "pnlModalBodyButtonPositive",
		"property": {
			"componentType": "fbutton",
			"componentName": "btnModalPositive",
			"componentValue": "OK",
			"ColoringStyle": "normal",
			"componentVisible": true
		}
	},
	"pnlMainHeaderMiddleCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddleTable"
	},
	"pnlMainHeaderRightTable": {
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRight",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTable",
			"componentVisible": true
		}
	},
	"pnlMainBodyContent": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyContent",
			"componentVisible": true
		},
		"style": {
			"overflow": "auto",
			"width": "100%",
			"height": "calc(100% - 50px)"
		},
		"parent": "pnlMainBody"
	},
	"pnlModalTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModal"
	},
	"pnlUpdateSalesReturnLinesContent4": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesContent4",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginTop": "20px"
		},
		"parent": "Panel45926"
	},
	"pnlHolder": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		},
		"style": {
			"overflow": "auto",
			"width": "100%",
			"height": "100%",
			"backgroundColor": "rgb(245, 245, 245)"
		},
		"parent": "pgSelectItem"
	},
	"pnlMainHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "65px",
			"textAlign": "center",
			"backgroundColor": "rgb(255, 69, 0)",
			"paddingTop": "15px",
			"overflow": "auto"
		},
		"parent": "pnlMain"
	},
	"pnlMainHeaderLeft": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeft",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"imgBack": {
		"property": {
			"componentType": "image",
			"componentName": "imgBack",
			"componentValue": require("../../asset/icon/icon-back.png"),
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"height": "35px",
			"width": "35px"
		},
		"parent": "pnlMainHeaderLeftCell"
	},
	"dtItem_List": {
		"property": {
			"componentType": "datalist",
			"componentName": "dtItem_List",
			"componentValue": "l_Item_List",
			"autoLoad": false,
			"componentData": [],
			"componentVisible": true
		},
		"parent": "pnlMainBodyContent",
		"template": {
			"componentDataItemState": {
				"pnlProductBody": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBody",
						"componentVisible": true
					},
					"style": {
						"width": "100%",
						"boxShadow": "-1px 1px 1px #545454",
						"border": "1px solid #3C3C3B",
						"display": "table",
						"background": "url() no-repeat fixed",
						"backgroundColor": "rgb(255, 255, 255)",
						"borderRadius": "5px",
						"padding": "5px"
					},
					"parent": "pnlProduct"
				},
				"imgItem": {
					"parent": "pnlProductBodyLeft",
					"property": {
						"componentType": "image",
						"componentName": "imgItem",
						"componentValue": require("../../asset/images/default.png"),
						"componentVisible": true
					},
					"style": {
						"verticalAlign": "top",
						"width": "100px",
						"height": "90px"
					}
				},
				"pnlProductBodyMiddle1": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle1",
						"componentVisible": true
					},
					"style": {
						"width": "100%",
						"padding": "5px",
						"paddingBottom": "0px",
						"display": "block"
					},
					"parent": "pnlProductBodyMiddle"
				},
				"lblItemCode": {
					"property": {
						"componentType": "label",
						"componentName": "lblItemCode",
						"field": "No",
						"componentValue": "ItemCode",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"color": "rgb(60, 60, 59)",
						"fontSize": "20px",
						"fontWeight": "bold"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"lblStockOnHand": {
					"property": {
						"componentType": "label",
						"componentName": "lblStockOnHand",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"color": "rgb(0, 153, 0)",
						"fontSize": "18px",
						"fontWeight": "normal",
						"textAlign": "left"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"lblQtyAdded": {
					"property": {
						"componentType": "label",
						"componentName": "lblQtyAdded",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(111, 111, 110)",
						"display": "block",
						"fontSize": "12px",
						"textAlign": "left",
						"paddingTop": "10px",
						"fontWeight": "normal"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"pnlProductBodyRight": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyRight",
						"componentVisible": true
					},
					"style": {
						"width": "45px",
						"verticalAlign": "middle",
						"textAlign": "center",
						"display": "table-cell"
					},
					"parent": "pnlProductBody"
				},
				"pnlProduct": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProduct",
						"componentVisible": true
					},
					"style": {
						"padding": "5px",
						"paddingBottom": "0px",
						"width": "100%",
						"backgroundColor": "rgba(244, 244, 244, 0)"
					}
				},
				"pnlProductBodyLeft": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyLeft",
						"componentVisible": true
					},
					"style": {
						"textAlign": "center",
						"display": "table-cell",
						"width": "100px",
						"height": "100px",
						"verticalAlign": "top"
					},
					"parent": "pnlProductBody"
				},
				"pnlProductBodyMiddle": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle",
						"componentVisible": true
					},
					"style": {
						"paddingLeft": "10px",
						"display": "table-cell",
						"verticalAlign": "top",
						"width": "calc(100% - 90px - 45px)"
					},
					"parent": "pnlProductBody"
				},
				"lblItemName": {
					"property": {
						"componentType": "label",
						"componentName": "lblItemName",
						"componentValue": "ItemName",
						"componentVisible": true
					},
					"style": {
						"fontSize": "18px",
						"fontWeight": "normal",
						"display": "block",
						"textAlign": "left",
						"paddingBottom": "5px",
						"color": "rgb(111, 111, 110)",
						"paddingTop": "5px"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"imgAddItem": {
					"property": {
						"componentType": "image",
						"componentName": "imgAddItem",
						"componentValue": require("../../asset/icon/icon-add-gray.png"),
						"componentVisible": true
					},
					"style": {
						"width": "25px",
						"height": "25px"
					},
					"parent": "pnlProductBodyRight"
				}
			}
		}
	},
	"Panel45926": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel45926",
			"componentVisible": true
		},
		"style": {
			"minWidth": "40px",
			"minHeight": "50px",
			"padding": "5%",
			"paddingBottom": "1%"
		},
		"parent": "pnlUpdateSalesReturnLinesContent"
	},
	"imgUpdateSalesReturnLinesItemImage": {
		"property": {
			"componentType": "image",
			"componentName": "imgUpdateSalesReturnLinesItemImage",
			"componentValue": require("../../asset/images/default.png"),
			"componentVisible": true
		},
		"style": {
			"height": "100px",
			"width": "100px"
		},
		"parent": "pnlUpdateSalesReturnLinesContent1"
	},
	"pnlUpdateSalesReturnLinesQtyContent1Table": {
		"parent": "pnlUpdateSalesReturnLinesQtyContent1",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesQtyContent1Table",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"textAlign": "left"
		}
	},
	"pnlUpdateSalesReturnLines": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLines",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"width": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"height": "100%",
			"top": "0px",
			"zIndex": "1"
		},
		"parent": "pnlHolder"
	},
	"lblUpdateSalesReturnLinesItemCode_data": {
		"parent": "pnlUpdateSalesReturnLinesContent2",
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateSalesReturnLinesItemCode_data",
			"field": "ItemCode",
			"componentVisible": true
		},
		"style": {
			"paddingBottom": "2px",
			"display": "block",
			"fontSize": "24px",
			"fontWeight": "bold",
			"color": "rgb(60, 60, 59)",
			"textAlign": "left",
			"paddingTop": "2px"
		}
	},
	"pnlModal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"top": "0px",
			"background": "rgba(0, 0, 0, 0.60)",
			"zIndex": "2",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlModalCell": {
		"parent": "pnlModalTable",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		}
	},
	"pnlMainHeaderMiddleTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddle"
	},
	"pnlUpdateSalesReturnLinesContent2": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesContent2",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"width": "100%",
			"paddingTop": "10px"
		},
		"parent": "Panel45926"
	},
	"pnlUpdateSalesReturnLinesQtyCtrlTableCell3": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesQtyCtrlTableCell3",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"backgroundColor": "rgb(255, 69, 0)",
			"width": "50px",
			"height": "40px"
		},
		"parent": "pnlUpdateSalesReturnLinesQtyControl"
	},
	"btnModalNegative": {
		"parent": "pnlModalBodyButtonNegative",
		"property": {
			"componentType": "fbutton",
			"componentName": "btnModalNegative",
			"componentValue": "CANCEL",
			"ColoringStyle": "alert",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"margin": "0px",
			"width": "80%"
		}
	},
	"lblStyle": {
		"parent": "pnlCSS",
		"property": {
			"componentType": "label",
			"componentName": "lblStyle",
			"componentValue": "N/A",
			"componentVisible": true
		}
	},
	"UpdateSalesReturnLinesFooterMainBtn": {
		"property": {
			"componentType": "panel",
			"componentName": "UpdateSalesReturnLinesFooterMainBtn",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"backgroundColor": "rgb(255, 69, 0)",
			"display": "block",
			"height": "40px"
		},
		"parent": "pnlUpdateSalesReturnLinesContent"
	},
	"pnlModalBodyMessage": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"marginBottom": "10px",
			"width": "100%"
		},
		"parent": "pnlModalBody"
	},
	"pgSelectItem": {
		"property": {
			"componentType": "page",
			"componentName": "pgSelectItem",
			"title": "Select Item",
			"componentVisible": true
		}
	},
	"pnlMainBodySearch": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodySearch",
			"componentVisible": true
		},
		"style": {
			"borderBottom": "1px solid #D9D9D9",
			"width": "100%",
			"height": "50px",
			"textAlign": "center"
		},
		"parent": "pnlMainBody"
	},
	"lblUpdateSalesReturnLinesItemName_data": {
		"style": {
			"display": "block",
			"color": "rgb(60, 60, 59)",
			"paddingBottom": "2px",
			"fontSize": "20px",
			"fontWeight": "normal",
			"textAlign": "left",
			"paddingTop": "2px"
		},
		"parent": "pnlUpdateSalesReturnLinesContent3",
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateSalesReturnLinesItemName_data",
			"field": "ItemName",
			"componentVisible": true
		}
	},
	"pnlUpdateSalesReturnLinesQtyContent1TableCell": {
		"style": {
			"display": "table-cell"
		},
		"parent": "pnlUpdateSalesReturnLinesQtyContent1Table",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesQtyContent1TableCell",
			"componentVisible": true
		}
	},
	"imgPlus": {
		"property": {
			"componentType": "image",
			"componentName": "imgPlus",
			"componentValue": require("../../asset/plus-white.png"),
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "50px",
			"height": "40px"
		},
		"parent": "pnlUpdateSalesReturnLinesQtyCtrlTableCell3"
	},
	"pnlUpdateSalesReturnLinesFooterBtn": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesFooterBtn",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgba(0, 0, 0, 0)",
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "UpdateSalesReturnLinesFooterMainBtn"
	},
	"pnlModalBodyTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBody"
	},
	"lblModalTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(33, 33, 33)",
			"fontSize": "18px",
			"fontWeight": "bold"
		},
		"parent": "pnlModalBodyTitle"
	},
	"pnlMainFooter": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooter",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "50px",
			"textAlign": "center",
			"backgroundColor": "rgb(255, 69, 0)"
		},
		"parent": "pnlMain"
	},
	"pnlUpdateSalesReturnLinesTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlUpdateSalesReturnLines"
	},
	"imgUpdateSalesReturnLinesClose": {
		"style": {
			"float": "right",
			"right": "7%",
			"position": "absolute",
			"width": "25px",
			"height": "25px"
		},
		"parent": "pnlUpdateSalesReturnLinesContent",
		"property": {
			"componentType": "image",
			"componentName": "imgUpdateSalesReturnLinesClose",
			"componentValue": require("../../asset/icon/icon-close-gray.png"),
			"componentVisible": true
		}
	},
	"pnlUpdateSalesReturnLinesUOMTableCell": {
		"style": {
			"verticalAlign": "middle",
			"textAlign": "center",
			"display": "table-cell"
		},
		"parent": "pnlUpdateSalesReturnLinesUOMTable",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesUOMTableCell",
			"componentVisible": true
		}
	},
	"pnlUpdateSalesReturnLinesQtyContent2": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesQtyContent2",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"width": "100%"
		},
		"parent": "pnlUpdateSalesReturnLinesQty"
	},
	"pnlModalBodyLoading": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBody"
	},
	"pnlCSS": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCSS",
			"componentVisible": false
		},
		"style": {
			"top": "0px",
			"position": "fixed",
			"display": "none",
			"zIndex": "3",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"lblMainTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle",
			"componentValue": "ITEM LIST",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "500",
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px"
		},
		"parent": "pnlMainHeaderMiddleCell"
	},
	"lblUpdateSalesReturnLinesUnitPrice_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateSalesReturnLinesUnitPrice_data",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"fontWeight": "normal",
			"textAlign": "left",
			"fontSize": "20px",
			"paddingTop": "2px",
			"paddingBottom": "2px",
			"color": "rgb(0, 153, 0)"
		},
		"parent": "pnlUpdateSalesReturnLinesContent4"
	},
	"lblUpdateSalesReturnLinesUOM": {
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateSalesReturnLinesUOM",
			"componentValue": "UOM",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(0, 0, 0)",
			"fontSize": "16px",
			"fontWeight": "bolder"
		},
		"parent": "pnlUpdateSalesReturnLinesUOMTableCell"
	},
	"lblUpdateSalesReturnLinesBtnAdd": {
		"parent": "pnlUpdateSalesReturnLinesBtnAdd",
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateSalesReturnLinesBtnAdd",
			"componentValue": "ADD",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "bold",
			"display": "block",
			"backgroundColor": "rgba(255, 255, 255, 0)"
		}
	},
	"imgModalLoading": {
		"style": {
			"width": "50px",
			"height": "10px"
		},
		"parent": "pnlModalBodyLoading",
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		}
	},
	"pnlMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"zIndex": "0",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"cmbUpdateSalesReturnLinesUOM": {
		"property": {
			"componentType": "combobox",
			"componentName": "cmbUpdateSalesReturnLinesUOM",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"paddingLeft": "5px",
			"margin": "0px",
			"float": "right",
			"width": "calc(100% - 50px)",
			"height": "40px",
			"border": "1px solid #FFFFFF"
		},
		"parent": "pnlUpdateSalesReturnLinesQtyContent1TableCell"
	},
	"pnlUpdateSalesReturnLinesQtyContent3": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesQtyContent3",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"padding": "5px"
		},
		"parent": "pnlUpdateSalesReturnLinesQty"
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"width": "100%"
		},
		"parent": "pnlModalBody"
	},
	"pnlUpdateSalesReturnLinesTableCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesTableCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlUpdateSalesReturnLinesTable"
	},
	"pnlUpdateSalesReturnLinesQty": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesQty",
			"componentVisible": true
		},
		"style": {
			"paddingBottom": "5%",
			"backgroundColor": "rgb(71, 65, 65)",
			"padding": "5%"
		},
		"parent": "pnlUpdateSalesReturnLinesContent"
	},
	"pnlUpdateSalesReturnLinesQtyContent1": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesQtyContent1",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "5px",
			"height": "40px"
		},
		"parent": "pnlUpdateSalesReturnLinesQty"
	},
	"txtQuantity": {
		"style": {
			"textAlign": "center",
			"float": "left",
			"border": "1px solid #D9D9D9",
			"color": "rgb(33, 33, 33)",
			"fontWeight": "normal",
			"width": "100%",
			"height": "40px",
			"margin": "0px",
			"fontSize": "24px"
		},
		"parent": "pnlUpdateSalesReturnLinesQtyCtrlTableCell2",
		"property": {
			"componentType": "edit",
			"componentName": "txtQuantity",
			"type": "number",
			"componentVisible": true
		}
	},
	"pnlModalBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBody",
			"componentVisible": true
		},
		"style": {
			"background": "#ffffff",
			"marginLeft": "5%",
			"marginRight": "5%",
			"width": "90%",
			"textAlign": "center",
			"padding": "10px"
		},
		"parent": "pnlModalCell"
	},
	"pnlMainHeaderRight": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRight",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlMainBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		},
		"style": {
			"height": "calc(100% - 100px - 15px)",
			"width": "100%"
		},
		"parent": "pnlMain"
	},
	"pnlUpdateSalesReturnLinesContent1": {
		"parent": "Panel45926",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesContent1",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"width": "100%",
			"height": "100px"
		}
	},
	"pnlUpdateSalesReturnLinesUOM": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesUOM",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"width": "50px",
			"height": "40px"
		},
		"parent": "pnlUpdateSalesReturnLinesQtyContent1TableCell"
	},
	"pnlUpdateSalesReturnLinesUOMTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesUOMTable",
			"componentVisible": true
		},
		"style": {
			"height": "40px",
			"backgroundColor": "rgb(228, 228, 228)",
			"border": "1px solid #FFFFFF",
			"display": "table",
			"width": "50px"
		},
		"parent": "pnlUpdateSalesReturnLinesUOM"
	},
	"pnlMainHeaderLeftTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderLeft"
	},
	"pnlMainHeaderLeftCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderLeftTable"
	},
	"lblBtnOK": {
		"parent": "pnlFooterBtn",
		"property": {
			"componentType": "label",
			"componentName": "lblBtnOK",
			"componentValue": "OK",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "bold",
			"display": "block"
		}
	},
	"pnlFooterBtn": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlFooterBtn",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"backgroundColor": "rgba(0, 0, 0, 0)"
		},
		"parent": "pnlMainFooter"
	},
	"pnlUpdateSalesReturnLinesContent": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateSalesReturnLinesContent",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"width": "90%",
			"marginLeft": "5%",
			"marginRight": "5%",
			"textAlign": "center",
			"background": "#ffffff",
			"padding": "0%",
			"paddingTop": "2%",
			"paddingBottom": "0%"
		},
		"parent": "pnlUpdateSalesReturnLinesTableCell"
	}
};
