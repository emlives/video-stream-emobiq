export const StateName = 'pgPayment';
export const State = {
	"imgBack555": {
		"style": {
			"width": "35px",
			"height": "35px"
		},
		"parent": "pnlMainHeaderLeftCell192",
		"property": {
			"componentType": "image",
			"componentName": "imgBack555",
			"componentValue": require("../../asset/icon/icon-back.png"),
			"componentVisible": true
		}
	},
	"pnlMainFooterTableCell1": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterTableCell1",
			"componentVisible": true
		},
		"style": {
			"width": "50%",
			"color": "rgba(255, 255, 255, 0)",
			"textAlign": "center",
			"borderRight": "1px solid #FFF",
			"display": "table-cell",
			"backgroundColor": "rgba(0, 0, 0, 0)",
			"verticalAlign": "middle"
		},
		"parent": "pnlMainFooterTable"
	},
	"Label894367": {
		"property": {
			"componentType": "label",
			"componentName": "Label894367",
			"componentValue": "OK",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"backgroundColor": "rgba(255, 255, 255, 0)",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px"
		},
		"parent": "Panel37680"
	},
	"pnlPrinter": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlPrinter",
			"componentVisible": false
		},
		"style": {
			"height": "100%",
			"position": "absolute",
			"top": "0px",
			"width": "100%",
			"zIndex": "2",
			"background": "rgba(0, 0, 0, 0.60)"
		},
		"parent": "pnlHolder"
	},
	"lblSelectPrinter": {
		"property": {
			"componentType": "label",
			"componentName": "lblSelectPrinter",
			"componentValue": "Select Printer",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"fontWeight": "bold",
			"color": "rgb(60, 60, 59)"
		},
		"parent": "pnlMainBodypnlConfirmPaymentMethodDetailsSign105386"
	},
	"lblAmountToPay": {
		"property": {
			"componentType": "label",
			"componentName": "lblAmountToPay",
			"componentValue": "Amount to Pay",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"fontWeight": "bold"
		},
		"parent": "pnlAmountToPay"
	},
	"pnlPaymentMethod": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlPaymentMethod",
			"componentVisible": true
		},
		"style": {
			"marginTop": "20px"
		},
		"parent": "pnlBodyContentBody"
	},
	"pnlModalTable": {
		"parent": "pnlModal",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		}
	},
	"imgUpdateCartClose": {
		"property": {
			"componentType": "image",
			"componentName": "imgUpdateCartClose",
			"componentValue": require("../../asset/icon/icon-close-gray.png"),
			"componentVisible": true
		},
		"style": {
			"float": "right",
			"right": "0px",
			"width": "20px",
			"height": "20px"
		},
		"parent": "pnlpnlConfirmPaymentMethodDetailsImages"
	},
	"pnlSignBoxTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSignBoxTable",
			"componentVisible": true
		},
		"style": {
			"height": "40px",
			"display": "table",
			"width": "100%"
		},
		"parent": "pnlUpdateCartModalBody5"
	},
	"lblOrderDate_data": {
		"parent": "pnlOrderDate",
		"property": {
			"componentType": "label",
			"componentName": "lblOrderDate_data",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"display": "block",
			"color": "rgb(60, 60, 59)"
		}
	},
	"pnlModalBodyLoading": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlCSS": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCSS",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"display": "none",
			"top": "0px",
			"zIndex": "4",
			"height": "100%",
			"width": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlConfirmPaymentdDetails": {
		"style": {
			"width": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"zIndex": "2",
			"height": "100%",
			"position": "absolute",
			"top": "0px"
		},
		"parent": "pnlHolder",
		"property": {
			"componentType": "panel",
			"componentName": "pnlConfirmPaymentdDetails",
			"componentVisible": false
		}
	},
	"pnlSignBoxTableCell1": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSignBoxTableCell1",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"width": "50%",
			"textAlign": "center",
			"borderRight": "1px solid #FFF",
			"display": "table-cell"
		},
		"parent": "pnlSignBoxTable"
	},
	"lblOrderNo_data": {
		"style": {
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"fontSize": "26px",
			"textAlign": "center"
		},
		"parent": "pnlBodyContentHeader",
		"property": {
			"componentType": "label",
			"componentName": "lblOrderNo_data",
			"componentValue": "ORDER",
			"componentVisible": true
		}
	},
	"pnlPaymentAmount": {
		"style": {
			"minWidth": "40px",
			"minHeight": "50px",
			"marginTop": "10px"
		},
		"parent": "pnlBodyContentBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlPaymentAmount",
			"componentVisible": true
		}
	},
	"btnFull": {
		"property": {
			"componentType": "fbutton",
			"componentName": "btnFull",
			"componentValue": "FULL",
			"componentVisible": true
		},
		"style": {
			"border": "1px solid #3C3C3B",
			"margin": "0",
			"backgroundColor": "rgb(255, 69, 0)",
			"width": "60px",
			"height": "40px",
			"color": "rgb(255, 255, 255)"
		},
		"parent": "pnlPaymentAmount"
	},
	"lblModalMessage": {
		"parent": "pnlModalBodyMessage",
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(111, 111, 110)",
			"fontSize": "18px"
		}
	},
	"pnlMainBodySignpnlConfirmPaymentMethodDetailsClear": {
		"parent": "pnlMainBodypnlConfirmPaymentMethodDetailsSign",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodySignpnlConfirmPaymentMethodDetailsClear",
			"componentVisible": true
		},
		"style": {
			"display": "none",
			"position": "relative",
			"width": "100%"
		}
	},
	"Label894367403": {
		"parent": "Panel37680600",
		"property": {
			"componentType": "label",
			"componentName": "Label894367403",
			"componentValue": "OK",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"fontWeight": "bold",
			"backgroundColor": "rgba(255, 255, 255, 0)"
		}
	},
	"pgPayment": {
		"property": {
			"componentType": "page",
			"componentName": "pgPayment",
			"title": "Payment",
			"hide": false,
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgba(0, 0, 0, 0)"
		}
	},
	"pnlMainHeaderMiddleTable764": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTable764",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table"
		},
		"parent": "pnlMainHeaderMiddle71"
	},
	"pnlpnlConfirmPaymentMethodDetailsImages": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlpnlConfirmPaymentMethodDetailsImages",
			"componentVisible": true
		},
		"style": {
			"marginBottom": "5px",
			"width": "100%",
			"height": "35px"
		},
		"parent": "Panel103"
	},
	"pnlConfirmPaymentMethodDetailsBody264652": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlConfirmPaymentMethodDetailsBody264652",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"background": "#ffffff",
			"margin": "5%",
			"width": "90%",
			"textAlign": "center"
		},
		"parent": "pnlConfirmPaymentMethodDetailsCell206351"
	},
	"pnlHolder": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"width": "100%",
			"height": "100%",
			"overflow": "auto",
			"backgroundColor": "rgb(245, 245, 245)"
		},
		"parent": "pgPayment"
	},
	"pnlMainHeaderLeftTable326": {
		"parent": "pnlMainHeaderLeft756",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTable326",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		}
	},
	"pnlMainHeaderLeftCell192": {
		"parent": "pnlMainHeaderLeftTable326",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftCell192",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%",
			"paddingLeft": "10px",
			"display": "table-cell",
			"textAlign": "left"
		}
	},
	"pnlMainHeaderMiddle71": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddle71",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "70%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"lblBtnPayPost": {
		"property": {
			"componentType": "label",
			"componentName": "lblBtnPayPost",
			"componentValue": "PAY & INVOICE",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px"
		},
		"parent": "pnlMainFooterTableCell1"
	},
	"Panel103": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel103",
			"componentVisible": true
		},
		"style": {
			"padding": "10px"
		},
		"parent": "pnlConfirmPaymentMethodDetailsBody"
	},
	"pnlMainBodypnlConfirmPaymentMethodDetailsSign": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodypnlConfirmPaymentMethodDetailsSign",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "calc(100% - 35px - 50px - 20px)"
		},
		"parent": "Panel103"
	},
	"pnlUpdateCartModalBody589675": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBody589675",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgb(255, 69, 0)",
			"textAlign": "center",
			"height": "40px",
			"width": "100%"
		},
		"parent": "pnlConfirmPaymentMethodDetailsBody264652"
	},
	"lblMainTitle453": {
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle453",
			"componentValue": "PAYMENT",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500"
		},
		"parent": "pnlMainHeaderMiddleCell629"
	},
	"pnlModal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"background": "rgba(0, 0, 0, 0.60)",
			"height": "100%",
			"position": "fixed",
			"top": "0px",
			"zIndex": "3",
			"width": "100%"
		},
		"parent": "pnlHolder"
	},
	"lblModalPositive": {
		"parent": "pnlModalBodyButtonPositive",
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		}
	},
	"pnlConfirmPaymentMethodDetailsCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlConfirmPaymentMethodDetailsCell",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"display": "table-cell",
			"height": "100%",
			"verticalAlign": "middle",
			"width": "100%"
		},
		"parent": "pnlConfirmPaymentMethodDetailsTable"
	},
	"lblAlertMsg": {
		"property": {
			"componentType": "label",
			"componentName": "lblAlertMsg",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px"
		},
		"parent": "pnlMainBodypnlConfirmPaymentMethodDetailsSign105"
	},
	"pnlMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"zIndex": "0",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"lblCodeName_data": {
		"parent": "pnlBodyContentHeader",
		"property": {
			"componentType": "label",
			"componentName": "lblCodeName_data",
			"componentValue": "Code - Customer Name",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(255, 215, 0)",
			"fontSize": "16px",
			"fontWeight": "bold",
			"textAlign": "center"
		}
	},
	"pnlOrderDate": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlOrderDate",
			"componentVisible": true
		},
		"style": {
			"minWidth": "40px",
			"minHeight": "50px"
		},
		"parent": "pnlBodyContentBody"
	},
	"pnlInvoiceDate": {
		"parent": "pnlBodyContentBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlInvoiceDate",
			"componentVisible": true
		},
		"style": {
			"marginTop": "10px"
		}
	},
	"pnlMainFooterTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainFooter"
	},
	"pnlMainFooterTableCell2": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterTableCell2",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"display": "table-cell",
			"backgroundColor": "rgba(0, 0, 0, 0)",
			"width": "50%",
			"color": "rgba(255, 255, 255, 0)",
			"textAlign": "center",
			"borderLeft": "1px solid #FFF"
		},
		"parent": "pnlMainFooterTable"
	},
	"pnlModalBodyMessage": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"imgModalLoading": {
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		},
		"style": {
			"height": "10px",
			"width": "50px"
		},
		"parent": "pnlModalBodyLoading"
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"float": "none",
			"display": "inline-block",
			"width": "50%",
			"textAlign": "center",
			"padding": "10px 0px"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlMainHeaderLeft756": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeft756",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlBodyContentBody": {
		"style": {
			"padding": "10px"
		},
		"parent": "pnlMainBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyContentBody",
			"componentVisible": true
		}
	},
	"lblAmountToPay_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblAmountToPay_data",
			"componentValue": "$ 0.00",
			"componentVisible": true
		},
		"style": {
			"fontSize": "24px",
			"fontWeight": "bold",
			"display": "block",
			"color": "rgb(0, 153, 0)"
		},
		"parent": "pnlAmountToPay"
	},
	"cmbPaymentMethod": {
		"property": {
			"componentType": "combobox",
			"componentName": "cmbPaymentMethod",
			"componentVisible": true
		},
		"style": {
			"height": "40px",
			"fontSize": "18px",
			"border": "1px solid #3C3C3B",
			"margin": "0",
			"width": "100%"
		},
		"parent": "pnlPaymentMethod"
	},
	"pnlChequeNumber": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlChequeNumber",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"minWidth": "40px",
			"minHeight": "50px",
			"marginTop": "10px"
		},
		"parent": "pnlBodyContentBody"
	},
	"pnlUpdateCartModalBody5": {
		"parent": "pnlConfirmPaymentMethodDetailsBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBody5",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "40px",
			"textAlign": "center",
			"backgroundColor": "rgb(255, 69, 0)"
		}
	},
	"pnlConfirmPaymentMethodDetailsBody264": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlConfirmPaymentMethodDetailsBody264",
			"componentVisible": true
		},
		"style": {
			"background": "#ffffff",
			"margin": "5%",
			"position": "relative",
			"textAlign": "center",
			"width": "90%"
		},
		"parent": "pnlConfirmPaymentMethodDetailsCell206"
	},
	"pnlMainHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		},
		"style": {
			"overflow": "auto",
			"width": "100%",
			"height": "65px",
			"textAlign": "center",
			"backgroundColor": "rgb(255, 69, 0)",
			"paddingTop": "15px"
		},
		"parent": "pnlMain"
	},
	"imgBack555407": {
		"parent": "pnlMainHeaderRightCell877",
		"property": {
			"componentType": "image",
			"componentName": "imgBack555407",
			"componentValue": require("../../asset/Printer_white.png"),
			"componentVisible": true
		},
		"style": {
			"height": "35px",
			"width": "35px"
		}
	},
	"lblInvoiceDate": {
		"property": {
			"componentType": "label",
			"componentName": "lblInvoiceDate",
			"componentValue": "Invoice Date",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"fontWeight": "bold",
			"display": "block"
		},
		"parent": "pnlInvoiceDate"
	},
	"edtInvoiceDate": {
		"property": {
			"componentType": "edit",
			"componentName": "edtInvoiceDate",
			"type": "date",
			"componentVisible": true
		},
		"style": {
			"height": "40px",
			"fontSize": "18px",
			"border": "1px solid #3C3C3B",
			"width": "100%"
		},
		"parent": "pnlInvoiceDate"
	},
	"lblPaymentMethod": {
		"property": {
			"componentType": "label",
			"componentName": "lblPaymentMethod",
			"componentValue": "Payment Method",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"fontWeight": "bold"
		},
		"parent": "pnlPaymentMethod"
	},
	"lblBtnPost": {
		"property": {
			"componentType": "label",
			"componentName": "lblBtnPost",
			"componentValue": "INVOICE",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "bold"
		},
		"parent": "pnlMainFooterTableCell2"
	},
	"pnlModalBodyMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		},
		"style": {
			"marginLeft": "5%",
			"width": "90%",
			"borderBottom": "1px solid #D9D9D9",
			"padding": "10px 0px",
			"marginRight": "5%",
			"textAlign": "center",
			"background": "#ffffff"
		},
		"parent": "pnlModalCell"
	},
	"pnlModalBodyButtons": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"marginRight": "5%",
			"textAlign": "center",
			"marginLeft": "5%",
			"width": "90%",
			"background": "#ffffff"
		},
		"parent": "pnlModalCell"
	},
	"pnlModalBodyButtonPositive": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"padding": "10px 0px",
			"display": "inline-block",
			"float": "none",
			"width": "50%",
			"textAlign": "center"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlSignBoxTableCell2": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSignBoxTableCell2",
			"componentVisible": true
		},
		"style": {
			"borderLeft": "1px solid #FFF",
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "50%",
			"textAlign": "center"
		},
		"parent": "pnlSignBoxTable"
	},
	"Panel37680": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel37680",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "50%",
			"textAlign": "center"
		},
		"parent": "pnlUpdateCartModalBody5896"
	},
	"lblAddress_data": {
		"parent": "pnlBodyContentHeader",
		"property": {
			"componentType": "label",
			"componentName": "lblAddress_data",
			"componentValue": "Address",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"textAlign": "center"
		}
	},
	"lblOrderDate": {
		"property": {
			"componentType": "label",
			"componentName": "lblOrderDate",
			"componentValue": "Order Date",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"fontWeight": "bold"
		},
		"parent": "pnlOrderDate"
	},
	"pnlMainFooter": {
		"style": {
			"backgroundColor": "rgb(255, 69, 0)",
			"width": "100%",
			"height": "40px"
		},
		"parent": "pnlMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooter",
			"componentVisible": true
		}
	},
	"lblStyle": {
		"property": {
			"componentType": "label",
			"componentName": "lblStyle",
			"componentValue": "N/A",
			"componentVisible": true
		},
		"parent": "pnlCSS"
	},
	"lblSign": {
		"property": {
			"componentType": "label",
			"componentName": "lblSign",
			"componentValue": "Sign In The Box",
			"componentVisible": true
		},
		"style": {
			"marginBottom": "10px",
			"fontWeight": "bolder",
			"textAlign": "inherit",
			"color": "rgb(60, 60, 59)",
			"width": "100%"
		},
		"parent": "pnlMainBodypnlConfirmPaymentMethodDetailsSign"
	},
	"pnlConfirmPaymentMethodDetailsCell206": {
		"style": {
			"position": "relative",
			"width": "100%",
			"height": "100%",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlConfirmPaymentMethodDetailsTable695203",
		"property": {
			"componentType": "panel",
			"componentName": "pnlConfirmPaymentMethodDetailsCell206",
			"componentVisible": true
		}
	},
	"pnlConfirmPaymentMethodDetailsCell206351": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlConfirmPaymentMethodDetailsCell206351",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"position": "relative",
			"verticalAlign": "middle",
			"width": "100%",
			"display": "table-cell"
		},
		"parent": "pnlConfirmPaymentMethodDetailsTable695203598148"
	},
	"pnlMainBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		},
		"style": {
			"zIndex": "1",
			"width": "100%",
			"height": "calc(100% - 50px - 40px - 15px)",
			"position": "relative",
			"overflow": "auto"
		},
		"parent": "pnlMain"
	},
	"pnlBodyContentHeader": {
		"style": {
			"backgroundColor": "rgb(84, 84, 84)",
			"padding": "10px"
		},
		"parent": "pnlMainBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyContentHeader",
			"componentVisible": true
		}
	},
	"lblPaymentAmount": {
		"style": {
			"display": "block",
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"fontWeight": "bold"
		},
		"parent": "pnlPaymentAmount",
		"property": {
			"componentType": "label",
			"componentName": "lblPaymentAmount",
			"componentValue": "Payment Amount",
			"componentVisible": true
		}
	},
	"lblModalTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "18px",
			"fontWeight": "bold"
		},
		"parent": "pnlModalBodyTitle"
	},
	"pnlConfirmPaymentMethodDetailsTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlConfirmPaymentMethodDetailsTable",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"position": "relative",
			"display": "table",
			"width": "100%"
		},
		"parent": "pnlConfirmPaymentdDetails"
	},
	"pnlConfirmPaymentMethodDetailsTable695203598148": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlConfirmPaymentMethodDetailsTable695203598148",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlPrinter"
	},
	"pnlMainHeaderMiddleCell629": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleCell629",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddleTable764"
	},
	"lblClear": {
		"property": {
			"componentType": "label",
			"componentName": "lblClear",
			"componentValue": "Clear",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bolder",
			"textAlign": "inherit",
			"position": "relative",
			"width": "100%",
			"color": "rgb(33, 33, 33)"
		},
		"parent": "pnlMainBodySignpnlConfirmPaymentMethodDetailsClear"
	},
	"lblSubmitBtn": {
		"property": {
			"componentType": "label",
			"componentName": "lblSubmitBtn",
			"componentValue": "SUBMIT",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"fontWeight": "bold"
		},
		"parent": "pnlSignBoxTableCell2"
	},
	"pnlUpdateCartModalBody5896": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBody5896",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgba(242, 34, 34, 0.95)",
			"paddingTop": "1%",
			"display": "table",
			"width": "100%",
			"height": "50px",
			"paddingBottom": "1%",
			"textAlign": "center"
		},
		"parent": "pnlConfirmPaymentMethodDetailsBody264"
	},
	"pnlMainBodypnlConfirmPaymentMethodDetailsSign105386": {
		"style": {
			"verticalAlign": "middle",
			"textAlign": "left",
			"display": "table-cell"
		},
		"parent": "Panel103885987",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodypnlConfirmPaymentMethodDetailsSign105386",
			"componentVisible": true
		}
	},
	"pnlMainHeaderRight233": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRight233",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"height": "100%",
			"textAlign": "right",
			"paddingRight": "10px",
			"float": "left",
			"width": "15%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlMainHeaderRightCell877": {
		"style": {
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%",
			"display": "table-cell"
		},
		"parent": "pnlMainHeaderRightTable2",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightCell877",
			"componentVisible": true
		}
	},
	"lblModalNegative": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonNegative"
	},
	"sgUser": {
		"property": {
			"componentType": "signature",
			"componentName": "sgUser",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"width": "100%",
			"position": "relative",
			"height": "250px",
			"background": "#ffffff",
			"border": "1px solid #3C3C3B"
		},
		"parent": "pnlMainBodypnlConfirmPaymentMethodDetailsSign"
	},
	"pnlMainBodypnlConfirmPaymentMethodDetailsSign105": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodypnlConfirmPaymentMethodDetailsSign105",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"display": "table-cell",
			"textAlign": "center"
		},
		"parent": "Panel103885"
	},
	"cmbPrinter": {
		"style": {
			"width": "100%",
			"border": "1px solid  #3C3C3B"
		},
		"parent": "pnlMainBodypnlConfirmPaymentMethodDetailsSign105386",
		"property": {
			"componentType": "combobox",
			"componentName": "cmbPrinter",
			"componentVisible": true
		}
	},
	"pnlMainHeaderRightTable2": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTable2",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRight233"
	},
	"edtChequeNumber": {
		"property": {
			"componentType": "edit",
			"componentName": "edtChequeNumber",
			"type": "text",
			"componentVisible": true
		},
		"style": {
			"height": "40px",
			"fontSize": "18px",
			"border": "1px solid #3C3C3B",
			"width": "100%"
		},
		"parent": "pnlChequeNumber"
	},
	"pnlModalCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModalTable"
	},
	"pnlConfirmPaymentMethodDetailsBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlConfirmPaymentMethodDetailsBody",
			"componentVisible": true
		},
		"style": {
			"background": "#ffffff",
			"margin": "5%",
			"position": "relative",
			"width": "90%",
			"textAlign": "center"
		},
		"parent": "pnlConfirmPaymentMethodDetailsCell"
	},
	"lblClearBtn": {
		"property": {
			"componentType": "label",
			"componentName": "lblClearBtn",
			"componentValue": "CLEAR",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"fontWeight": "bold",
			"backgroundColor": "rgba(255, 255, 255, 0)"
		},
		"parent": "pnlSignBoxTableCell1"
	},
	"pnlConfirmPaymentMethodDetailsTable695203": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlConfirmPaymentMethodDetailsTable695203",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlAlert"
	},
	"Panel103885": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel103885",
			"componentVisible": true
		},
		"style": {
			"padding": "10px",
			"display": "table",
			"width": "100%",
			"height": "100px"
		},
		"parent": "pnlConfirmPaymentMethodDetailsBody264"
	},
	"Panel103885987": {
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100px",
			"padding": "10px"
		},
		"parent": "pnlConfirmPaymentMethodDetailsBody264652",
		"property": {
			"componentType": "panel",
			"componentName": "Panel103885987",
			"componentVisible": true
		}
	},
	"pnlAmountToPay": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlAmountToPay",
			"componentVisible": true
		},
		"style": {
			"marginTop": "10px"
		},
		"parent": "pnlBodyContentBody"
	},
	"lblChequeNumber": {
		"property": {
			"componentType": "label",
			"componentName": "lblChequeNumber",
			"componentValue": "Cheque Number",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"fontWeight": "bold",
			"display": "block"
		},
		"parent": "pnlChequeNumber"
	},
	"edtPaymentAmount": {
		"property": {
			"componentType": "edit",
			"componentName": "edtPaymentAmount",
			"type": "number",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"fontSize": "18px",
			"border": "1px solid #3C3C3B",
			"width": "calc(100% - 60px)",
			"height": "40px"
		},
		"parent": "pnlPaymentAmount"
	},
	"pnlModalBodyTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlAlert": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlAlert",
			"componentVisible": false
		},
		"style": {
			"zIndex": "2",
			"width": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"top": "0px",
			"position": "absolute",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"Panel154": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel154",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"minWidth": "40px",
			"minHeight": "50px",
			"display": "table"
		},
		"parent": "pnlUpdateCartModalBody589675"
	},
	"Panel37680600": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel37680600",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "50%",
			"textAlign": "center"
		},
		"parent": "Panel154"
	}
};
