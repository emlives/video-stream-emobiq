export const StateName = 'pgSummary';
export const State = {
	"pnlMainHeaderLeft": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeft",
			"componentVisible": true
		},
		"style": {
			"width": "15%",
			"height": "100%",
			"float": "left"
		},
		"parent": "pnlMainHeader"
	},
	"lblTotalAmount": {
		"property": {
			"componentType": "label",
			"componentName": "lblTotalAmount",
			"componentValue": "Total Amount",
			"componentVisible": true
		},
		"style": {
			"width": "120px",
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"fontWeight": "bold",
			"display": "inline-block"
		},
		"parent": "Panel979155"
	},
	"pnlModalBodyMain": {
		"style": {
			"width": "90%",
			"marginRight": "5%",
			"textAlign": "center",
			"background": "#ffffff",
			"borderBottom": "1px solid #D9D9D9",
			"padding": "10px 0px",
			"marginLeft": "5%"
		},
		"parent": "pnlModalCell",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		}
	},
	"pnlMainHeaderRight": {
		"style": {
			"height": "100%",
			"float": "left",
			"width": "15%"
		},
		"parent": "pnlMainHeader",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRight",
			"componentVisible": true
		}
	},
	"lblSubTotal_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblSubTotal_data",
			"componentValue": "$ 0.00",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"fontSize": "18px",
			"textAlign": "right",
			"width": "calc(100% - 100px)",
			"color": "rgb(60, 60, 59)"
		},
		"parent": "Panel979"
	},
	"Panel979": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel979",
			"componentVisible": true
		},
		"parent": "Panel929"
	},
	"Panel979909": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel979909",
			"componentVisible": true
		},
		"style": {
			"borderBottom": "1px solid #c4c4c4"
		},
		"parent": "Panel929"
	},
	"pnlMainFooterTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterTable",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"display": "table",
			"width": "100%"
		},
		"parent": "pnlMainFooter"
	},
	"imgModalLoading": {
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		},
		"style": {
			"width": "50px",
			"height": "10px"
		},
		"parent": "pnlModalBodyLoading"
	},
	"Panel916": {
		"style": {
			"padding": "10px"
		},
		"parent": "pnlMainBody",
		"property": {
			"componentType": "panel",
			"componentName": "Panel916",
			"componentVisible": true
		}
	},
	"Panel929": {
		"style": {
			"borderBottom": "3px solid #3C3C3B",
			"marginBottom": "10px",
			"minWidth": "40px",
			"minHeight": "50px"
		},
		"parent": "Panel916",
		"property": {
			"componentType": "panel",
			"componentName": "Panel929",
			"componentVisible": true
		}
	},
	"pnlModalTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"display": "table",
			"width": "100%"
		},
		"parent": "pnlModal"
	},
	"pnlModalCell": {
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModalTable",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		}
	},
	"pnlMainHeaderRightCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightCell",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%",
			"display": "table-cell"
		},
		"parent": "pnlMainHeaderRightTable"
	},
	"lblOrderDate_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblOrderDate_data",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"fontSize": "14px",
			"textAlign": "center"
		},
		"parent": "pnlBodyContentHeader"
	},
	"lblStyle174": {
		"parent": "pnlCSS",
		"property": {
			"componentType": "label",
			"componentName": "lblStyle174",
			"componentValue": "N/A",
			"componentVisible": true
		}
	},
	"pnlBodyContentHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyContentHeader",
			"componentVisible": true
		},
		"style": {
			"padding": "10px",
			"backgroundColor": "rgb(84, 84, 84)"
		},
		"parent": "pnlMainBody"
	},
	"lblGST_data": {
		"style": {
			"display": "inline-block",
			"width": "calc(100% - 150px)",
			"fontSize": "18px",
			"textAlign": "right",
			"color": "rgb(60, 60, 59)"
		},
		"parent": "Panel979909",
		"property": {
			"componentType": "label",
			"componentName": "lblGST_data",
			"componentValue": "$ 0.00",
			"componentVisible": true
		}
	},
	"lblRemarks_data": {
		"parent": "pnlRemarks",
		"property": {
			"componentType": "label",
			"componentName": "lblRemarks_data",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(60, 60, 59)",
			"fontSize": "18px"
		}
	},
	"Panel99": {
		"style": {
			"width": "100%",
			"backgroundColor": "rgb(135, 135, 135)"
		},
		"parent": "pnlContentMainLines",
		"property": {
			"componentType": "panel",
			"componentName": "Panel99",
			"componentVisible": true
		}
	},
	"pnlCSS": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCSS",
			"componentVisible": false
		},
		"style": {
			"zIndex": "4",
			"width": "100%",
			"position": "fixed",
			"display": "none",
			"top": "0px",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlMainHeaderMiddleCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddleTable"
	},
	"lblSubTotal": {
		"property": {
			"componentType": "label",
			"componentName": "lblSubTotal",
			"componentValue": "Sub Total",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"width": "100px",
			"color": "rgb(60, 60, 59)",
			"fontSize": "14px"
		},
		"parent": "Panel979"
	},
	"Panel979155": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel979155",
			"componentVisible": true
		},
		"style": {
			"paddingBottom": "5px",
			"paddingTop": "5px"
		},
		"parent": "Panel929"
	},
	"pnlModalBodyButtons": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"marginRight": "5%",
			"width": "90%",
			"marginLeft": "5%",
			"textAlign": "center",
			"background": "#ffffff"
		},
		"parent": "pnlModalCell"
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "50%",
			"padding": "10px 0px",
			"display": "inline-block",
			"float": "none",
			"textAlign": "center"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pgSummary": {
		"property": {
			"componentType": "page",
			"componentName": "pgSummary",
			"title": "Summary",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgba(0, 0, 0, 0)"
		}
	},
	"lblMainTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle",
			"componentValue": "SUMMARY",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500"
		},
		"parent": "pnlMainHeaderMiddleCell"
	},
	"pnlMainFooter": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooter",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "40px",
			"backgroundColor": "rgb(255, 69, 0)"
		},
		"parent": "pnlMain"
	},
	"pnlMainHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgb(255, 69, 0)",
			"paddingTop": "15px",
			"width": "100%",
			"height": "65px",
			"overflow": "auto",
			"textAlign": "center"
		},
		"parent": "pnlMain"
	},
	"imgBack": {
		"style": {
			"width": "35px",
			"height": "35px"
		},
		"parent": "pnlMainHeaderLeftCell",
		"property": {
			"componentType": "image",
			"componentName": "imgBack",
			"componentValue": require("../../asset/icon/icon-menu.png"),
			"componentVisible": true
		}
	},
	"pnlModal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"zIndex": "3",
			"width": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"top": "0px",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlModalBodyLoading": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	},
	"lblModalPositive": {
		"parent": "pnlModalBodyButtonPositive",
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		},
		"style": {
			"fontSize": "20px",
			"color": "rgb(74, 134, 237)"
		}
	},
	"lblModalNegative": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonNegative"
	},
	"pnlHolder": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgb(245, 245, 245)",
			"overflow": "auto",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pgSummary"
	},
	"pnlMainFooterTableCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterTableCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"color": "rgba(255, 255, 255, 0)",
			"backgroundColor": "rgba(0, 0, 0, 0)",
			"verticalAlign": "middle",
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlMainFooterTable"
	},
	"lblModalMessage": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		},
		"style": {
			"fontSize": "16px",
			"width": "100%",
			"color": "rgb(111, 111, 110)"
		},
		"parent": "pnlModalBodyMessage"
	},
	"pnlMainHeaderMiddleTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddle"
	},
	"pnlRemarks": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlRemarks",
			"componentVisible": true
		},
		"style": {
			"fontFamily": "18px",
			"marginTop": "5px",
			"marginBottom": "10px"
		},
		"parent": "Panel916"
	},
	"lblAddress_data": {
		"parent": "pnlBodyContentHeader",
		"property": {
			"componentType": "label",
			"componentName": "lblAddress_data",
			"componentValue": "Address",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"fontSize": "16px",
			"textAlign": "center"
		}
	},
	"lblRemarks": {
		"property": {
			"componentType": "label",
			"componentName": "lblRemarks",
			"componentValue": "Remarks",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(60, 60, 59)",
			"fontSize": "14px",
			"fontWeight": "bold"
		},
		"parent": "pnlRemarks"
	},
	"lblItems": {
		"property": {
			"componentType": "label",
			"componentName": "lblItems",
			"componentValue": "ITEMS",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "5px",
			"fontWeight": "bold",
			"padding": "10px",
			"color": "rgb(255, 215, 0)",
			"fontSize": "16px",
			"display": "inline-block",
			"paddingBottom": "5px",
			"width": "calc(100% - 48px)"
		},
		"parent": "Panel99"
	},
	"lblOrderNo_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblOrderNo_data",
			"componentValue": "ORDER",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"fontSize": "26px",
			"textAlign": "center"
		},
		"parent": "pnlBodyContentHeader"
	},
	"lblCodeName_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblCodeName_data",
			"componentValue": "Code - Customer Name",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"color": "rgb(255, 215, 0)",
			"fontWeight": "bold",
			"fontSize": "18px",
			"display": "block"
		},
		"parent": "pnlBodyContentHeader"
	},
	"pnlMainHeaderLeftTable": {
		"parent": "pnlMainHeaderLeft",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		}
	},
	"lblModalTitle": {
		"style": {
			"width": "100%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "18px",
			"fontWeight": "bold"
		},
		"parent": "pnlModalBodyTitle",
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		}
	},
	"lblBtnConfirm": {
		"property": {
			"componentType": "label",
			"componentName": "lblBtnConfirm",
			"componentValue": "CONFIRMED",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "bold"
		},
		"parent": "pnlMainFooterTableCell"
	},
	"pnlMainHeaderLeftCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftCell",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"paddingLeft": "10px",
			"display": "table-cell",
			"verticalAlign": "middle",
			"textAlign": "left"
		},
		"parent": "pnlMainHeaderLeftTable"
	},
	"pnlMainBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		},
		"style": {
			"overflow": "auto",
			"width": "100%",
			"height": "calc(100% - 50px - 40px - 15px)"
		},
		"parent": "pnlMain"
	},
	"pnlModalBodyTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlModalBodyMessage": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"marginBottom": "10px",
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	},
	"lblGST": {
		"parent": "Panel979909",
		"property": {
			"componentType": "label",
			"componentName": "lblGST",
			"componentValue": "GST",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"width": "150px",
			"color": "rgb(60, 60, 59)",
			"fontSize": "14px"
		}
	},
	"pnlContentMainLines": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlContentMainLines",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "0px"
		},
		"parent": "pnlMainBody"
	},
	"pnlMainHeaderRightTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRight"
	},
	"lblTotalAmount_data": {
		"parent": "Panel979155",
		"property": {
			"componentType": "label",
			"componentName": "lblTotalAmount_data",
			"componentValue": "$ 0.00",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"width": "calc(100% - 120px)",
			"fontSize": "24px",
			"color": "rgb(0, 153, 0)",
			"fontWeight": "bold",
			"textAlign": "right"
		}
	},
	"DataList760": {
		"property": {
			"componentType": "datalist",
			"componentName": "DataList760",
			"componentValue": "l_OrderLines",
			"componentData": [],
			"componentVisible": true
		},
		"parent": "pnlContentMainLines",
		"template": {
			"componentDataItemState": {
				"Label379774": {
					"property": {
						"componentType": "label",
						"componentName": "Label379774",
						"componentValue": "Amount",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"color": "rgb(0, 153, 0)",
						"fontSize": "16px",
						"fontWeight": "bold",
						"marginTop": "10px"
					},
					"parent": "Panel397"
				},
				"Panel846": {
					"parent": "Panel132",
					"property": {
						"componentType": "panel",
						"componentName": "Panel846",
						"componentVisible": true
					},
					"style": {
						"border": "1px solid #3C3C3B",
						"borderRadius": "5px",
						"padding": "5px",
						"display": "table",
						"boxShadow": "-1px 1px 1px #545454",
						"backgroundColor": "rgb(255, 255, 255)",
						"width": "100%"
					}
				},
				"Panel397": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel397",
						"componentVisible": true
					},
					"style": {
						"paddingLeft": "5px"
					},
					"parent": "Panel481444"
				},
				"Label379297": {
					"property": {
						"componentType": "label",
						"componentName": "Label379297",
						"componentValue": "Qty",
						"componentVisible": true
					},
					"style": {
						"paddingTop": "2px",
						"paddingBottom": "2px",
						"display": "block",
						"color": "rgb(60, 60, 59)",
						"fontSize": "16px"
					},
					"parent": "Panel397"
				},
				"Panel481444": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel481444",
						"componentVisible": true
					},
					"style": {
						"display": "table-cell",
						"verticalAlign": "top"
					},
					"parent": "Panel846"
				},
				"Label379": {
					"property": {
						"componentType": "label",
						"componentName": "Label379",
						"componentValue": "Code",
						"field": "No",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"fontSize": "20px",
						"color": "rgb(60, 60, 59)",
						"fontWeight": "bold"
					},
					"parent": "Panel397"
				},
				"Label161": {
					"property": {
						"componentType": "label",
						"componentName": "Label161",
						"field": "Description",
						"componentValue": "Name",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"color": "rgb(60, 60, 59)",
						"fontSize": "16px"
					},
					"parent": "Panel397"
				},
				"Panel132": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel132",
						"componentVisible": true
					},
					"style": {
						"padding": "5px",
						"paddingBottom": "0px",
						"minWidth": "40px",
						"minHeight": "50px"
					}
				},
				"Panel481": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel481",
						"addClass": "hide",
						"componentVisible": true
					},
					"style": {
						"display": "none",
						"verticalAlign": "middle",
						"width": "95px",
						"textAlign": "center"
					},
					"parent": "Panel846"
				},
				"Image159": {
					"property": {
						"componentType": "image",
						"componentName": "Image159",
						"zoom": false,
						"componentVisible": true
					},
					"style": {
						"width": "100px",
						"height": "90px"
					},
					"parent": "Panel481"
				}
			}
		}
	},
	"pnlModalBodyButtonPositive": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"padding": "10px 0px",
			"float": "none",
			"width": "50%",
			"textAlign": "center",
			"display": "inline-block"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlMain": {
		"parent": "pnlHolder",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"zIndex": "0",
			"width": "100%",
			"height": "100%"
		}
	},
	"pnlMainHeaderMiddle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddle",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "70%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	}
};
