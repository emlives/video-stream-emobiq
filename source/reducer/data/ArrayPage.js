export const StateName = 'ArrayPage';
export const State = {
    lblResult: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'label',
            componentValue: 'Result',
            componentVisible: true,
        }
    },
    btnSum: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Sum (Array)',
            componentVisible: true,
        }
    },
    btnCount: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Count (Array)',
            componentVisible: true,
        }
    },
    btnAverage: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Average (Array)',
            componentVisible: true,
        }
    },
    btnSort: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Sort (Array) - Console',
            componentVisible: true,
        }
    },
    btnNewArray: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'New Array - Console',
            componentVisible: true,
        }
    },
    btnNewObject: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'New Object - Console',
            componentVisible: true,
        }
    },
    btnPush: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Push - Console',
            componentVisible: true,
        }
    },
    btnPushObject: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Push Object - Console',
            componentVisible: true,
        }
    },
    btnPop: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Pop - Console',
            componentVisible: true,
        }
    },
    btnUnshift: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Unshift - Console',
            componentVisible: true,
        }
    },
    btnShift: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Shift - Console',
            componentVisible: true,
        }
    },
    btnFindArrayIndex: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Find Array Index - Console',
            componentVisible: true,
        }
    }
}