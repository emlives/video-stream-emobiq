export const StateName = 'pgSalesReturnLists';
export const State = {
	"pnlMainHeaderLeftCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftCell",
			"componentVisible": true
		},
		"style": {
			"textAlign": "left",
			"paddingLeft": "10px",
			"width": "100%",
			"height": "100%",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlMainHeaderLeftTable"
	},
	"pnlMainSearch": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainSearch",
			"componentVisible": true
		},
		"style": {
			"height": "50px",
			"textAlign": "center",
			"borderBottom": "1px solid #D9D9D9",
			"width": "100%"
		},
		"parent": "pnlMain"
	},
	"pnlModalBodyMessage": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlMainHeaderLeftTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderLeft"
	},
	"pnlMainHeaderMiddleCell": {
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddleTable",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleCell",
			"componentVisible": true
		}
	},
	"lblMainTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle",
			"componentValue": "SALES RETURNS",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500"
		},
		"parent": "pnlMainHeaderMiddleCell"
	},
	"pnlMainHeaderRightCell": {
		"style": {
			"width": "100%",
			"height": "100%",
			"textAlign": "right",
			"display": "table-cell",
			"paddingRight": "10px",
			"verticalAlign": "middle"
		},
		"parent": "pnlMainHeaderRightTable",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightCell",
			"componentVisible": true
		}
	},
	"txtSearch": {
		"property": {
			"componentType": "edit",
			"componentName": "txtSearch",
			"type": "search",
			"backgroundAsset": "icon/_search.png",
			"placeHolder": "Search",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"textAlign": "left",
			"backgroundSize": "16px",
			"margin": "0px",
			"display": "inline",
			"width": "100%",
			"height": "40px",
			"backgroundColor": "rgb(255, 255, 255)",
			"paddingLeft": "30px",
			"background": "url('data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Capa_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 451 451\" style=\"enable-background:new 0 0 451 451;\" xml:space=\"preserve\" width=\"512px\" height=\"512px\"><g><path d=\"M447.05,428l-109.6-109.6c29.4-33.8,47.2-77.9,47.2-126.1C384.65,86.2,298.35,0,192.35,0C86.25,0,0.05,86.3,0.05,192.3   s86.3,192.3,192.3,192.3c48.2,0,92.3-17.8,126.1-47.2L428.05,447c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4   C452.25,441.8,452.25,433.2,447.05,428z M26.95,192.3c0-91.2,74.2-165.3,165.3-165.3c91.2,0,165.3,74.2,165.3,165.3   s-74.1,165.4-165.3,165.4C101.15,357.7,26.95,283.5,26.95,192.3z\" fill=\"#3c3c3b\"/></g></svg>') no-repeat",
			"backgroundPosition": "left .5rem center",
			"border": "1px solid #D9D9D9"
		},
		"parent": "pnlMainBodySearch"
	},
	"imgModalLoadin": {
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoadin",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		},
		"style": {
			"width": "50px",
			"height": "10px"
		},
		"parent": "pnlModalBodyLoading"
	},
	"pnlMainHeaderRight": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRight",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlMainBodySearch": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodySearch",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "50px",
			"padding": "5px"
		},
		"parent": "pnlMainSearch"
	},
	"lblModalPositive": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonPositive"
	},
	"pnlMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"zIndex": "0"
		},
		"parent": "pnlHolder"
	},
	"lblFilter": {
		"property": {
			"componentType": "label",
			"componentName": "lblFilter",
			"componentValue": "NEW",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "500",
			"display": "none",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px"
		},
		"parent": "pnlMainHeaderRightCell"
	},
	"pnlModalCell": {
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModalTable",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		}
	},
	"pnlModalBodyTitle": {
		"parent": "pnlModalBodyMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		}
	},
	"lblModalTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "20px",
			"fontWeight": "bold"
		},
		"parent": "pnlModalBodyTitle"
	},
	"pgSalesReturnLists": {
		"property": {
			"componentType": "page",
			"componentName": "pgSalesReturnLists",
			"title": "Sales Return Lists",
			"componentVisible": true
		}
	},
	"pnlMainHeaderMiddle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddle",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"float": "left",
			"width": "70%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlModalTable": {
		"parent": "pnlModal",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		}
	},
	"pnlModalBodyLoading": {
		"style": {
			"width": "100%"
		},
		"parent": "pnlModalBodyMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		}
	},
	"pnlCSS": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCSS",
			"componentVisible": false
		},
		"style": {
			"display": "none",
			"zIndex": "5",
			"width": "100%",
			"position": "fixed",
			"top": "0px",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlMainHeaderLeft": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeft",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlBody573": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBody573",
			"componentVisible": true
		},
		"style": {
			"height": "100%"
		},
		"parent": "pnlMainBody106"
	},
	"pnlMainTabPending548": {
		"style": {
			"display": "block",
			"width": "100%"
		},
		"parent": "pnlBody573",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainTabPending548",
			"componentVisible": true
		}
	},
	"pnlModal": {
		"parent": "pnlHolder",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"top": "0px",
			"zIndex": "4",
			"width": "100%",
			"height": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"position": "fixed"
		}
	},
	"lblStyle": {
		"property": {
			"componentType": "label",
			"componentName": "lblStyle",
			"componentValue": "N/A",
			"componentVisible": true
		},
		"parent": "pnlCSS"
	},
	"pnlModalBodyButtons": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"background": "#ffffff",
			"marginLeft": "5%",
			"marginRight": "5%",
			"width": "90%"
		},
		"parent": "pnlModalCell"
	},
	"pnlModalBodyButtonPositive": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"padding": "10px 0px",
			"float": "none",
			"width": "50%",
			"textAlign": "center"
		},
		"parent": "pnlModalBodyButtons"
	},
	"pnlMainHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"backgroundColor": "rgb(255, 69, 0)",
			"paddingTop": "15px",
			"height": "65px"
		},
		"parent": "pnlMain"
	},
	"pnlMainHeaderRightTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRight"
	},
	"imgAddNew": {
		"property": {
			"componentType": "image",
			"componentName": "imgAddNew",
			"componentValue": require("../../asset/icon/_add.png"),
			"componentVisible": true
		},
		"style": {
			"width": "35px",
			"height": "35px",
			"padding": "5px"
		},
		"parent": "pnlMainHeaderRightCell"
	},
	"pnlMainBody106": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody106",
			"componentVisible": true
		},
		"style": {
			"overflow": "auto",
			"width": "100%",
			"height": "calc(100% - 100px - 15px)"
		},
		"parent": "pnlMain"
	},
	"dtSales_Return_Order": {
		"parent": "pnlMainTabPending548",
		"template": {
			"componentDataItemState": {
				"pnlProductBodyMiddle1": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle1",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"width": "100%"
					},
					"parent": "pnlProductBodyMiddle"
				},
				"lblDocNumber_data": {
					"property": {
						"componentType": "label",
						"componentName": "lblDocNumber_data",
						"field": "No",
						"componentVisible": true
					},
					"style": {
						"fontWeight": "bold",
						"color": "rgb(60, 60, 59)",
						"fontSize": "24px",
						"display": "block"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"pnlProductBodyMiddle2": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle2",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"overflow": "auto",
						"width": "100%"
					},
					"parent": "pnlProductBodyMiddle"
				},
				"pnlProductBodyMiddle4141161": {
					"style": {
						"display": "block",
						"width": "100%"
					},
					"parent": "pnlProductBodyMiddle",
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle4141161",
						"componentVisible": true
					}
				},
				"pnlProductBodyMiddle5": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle5",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"width": "100%",
						"marginTop": "10px"
					},
					"parent": "pnlProductBodyMiddle"
				},
				"Image9": {
					"property": {
						"componentType": "image",
						"componentName": "Image9",
						"zoom": false,
						"componentValue": require("../../asset/images/icon-go-gray (1).png"),
						"componentVisible": true
					},
					"style": {
						"width": "30px"
					},
					"parent": "Panel822"
				},
				"pnlProduct": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProduct",
						"componentVisible": true
					},
					"style": {
						"width": "100%",
						"textAlign": "left",
						"paddingBottom": "0px",
						"backgroundColor": "rgba(0, 0, 0, 0)",
						"padding": "5px"
					}
				},
				"pnlProductBody": {
					"style": {
						"width": "100%",
						"background": "url() no-repeat fixed",
						"borderRadius": "5px",
						"display": "table",
						"boxShadow": "-1px 1px 1px #545454",
						"backgroundColor": "rgb(255, 255, 255)",
						"border": "1px solid #000",
						"padding": "10px"
					},
					"parent": "pnlProduct",
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBody",
						"componentVisible": true
					}
				},
				"pnlProductBodyMiddle": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle",
						"componentVisible": true
					},
					"style": {
						"display": "table-cell",
						"width": "calc(100% - 40px)",
						"textAlign": "left"
					},
					"parent": "pnlProductBody"
				},
				"lblCustomer_data": {
					"property": {
						"componentType": "label",
						"componentName": "lblCustomer_data",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"fontWeight": "normal",
						"color": "rgb(60, 60, 59)",
						"fontSize": "18px"
					},
					"parent": "pnlProductBodyMiddle2"
				},
				"lblDocumentDate_data4": {
					"style": {
						"display": "block",
						"color": "rgb(60, 60, 59)",
						"fontSize": "16px",
						"fontWeight": "normal",
						"paddingTop": "2px"
					},
					"parent": "pnlProductBodyMiddle4141161",
					"property": {
						"componentType": "label",
						"componentName": "lblDocumentDate_data4",
						"componentVisible": true
					}
				},
				"lblDocTotal_data": {
					"style": {
						"display": "block",
						"color": "rgb(0, 153, 0)",
						"fontSize": "18px",
						"fontWeight": "bold"
					},
					"parent": "pnlProductBodyMiddle5",
					"property": {
						"componentType": "label",
						"componentName": "lblDocTotal_data",
						"field": "Reason_Code",
						"componentVisible": true
					}
				},
				"Panel822": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel822",
						"componentVisible": true
					},
					"style": {
						"display": "table-cell",
						"verticalAlign": "middle",
						"width": "40px",
						"textAlign": "center"
					},
					"parent": "pnlProductBody"
				}
			}
		},
		"property": {
			"componentType": "datalist",
			"componentName": "dtSales_Return_Order",
			"componentValue": "l_Sales_Return_Order",
			"autoLoad": false,
			"componentData": [],
			"componentVisible": true
		},
		"style": {
			"overflow": "auto"
		}
	},
	"pnlModalBodyMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		},
		"style": {
			"background": "#ffffff",
			"padding": "10px 0px",
			"marginRight": "5%",
			"textAlign": "center",
			"borderBottom": "1px solid #D9D9D9",
			"marginLeft": "5%",
			"width": "90%"
		},
		"parent": "pnlModalCell"
	},
	"lblModalNegative": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonNegative"
	},
	"pnlHolder": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"backgroundColor": "rgb(245, 245, 245)",
			"width": "100%"
		},
		"parent": "pgSalesReturnLists"
	},
	"imgBack": {
		"property": {
			"componentType": "image",
			"componentName": "imgBack",
			"componentValue": require("../../asset/icon/icon-menu.png"),
			"componentVisible": true
		},
		"style": {
			"width": "35px",
			"height": "35px"
		},
		"parent": "pnlMainHeaderLeftCell"
	},
	"pnlMainHeaderMiddleTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddle"
	},
	"lblModalMessage": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(111, 111, 110)",
			"fontSize": "16px"
		},
		"parent": "pnlModalBodyMessage"
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"float": "none",
			"textAlign": "center",
			"padding": "10px 0px",
			"display": "inline-block",
			"width": "50%"
		},
		"parent": "pnlModalBodyButtons"
	}
};
