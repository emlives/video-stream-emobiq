export const StateName = 'DevicePage';
export const State = {
    lblResult: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'label',
            componentValue: 'Result',
            componentVisible: true,
        }
    },
    btnDeviceManufacturer: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Device Manufacturer',
            componentVisible: true,
        }
    },
    btnDeviceSerial: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Device Serial',
            componentVisible: true,
        }
    },
    btnOnBackButtonFirst: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'First Back Button - Alert',
            componentVisible: true,
        }
    },
    btnOnBackButtonSecond: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Second Back Button - Device Manufacturer',
            componentVisible: true,
        }
    }
}