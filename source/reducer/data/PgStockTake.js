export const StateName = 'pgStockTake';
export const State = {
	"lblModalMessage": {
		"parent": "pnlModalBodyMessage",
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(111, 111, 110)",
			"fontSize": "16px",
			"width": "100%"
		}
	},
	"DataList334": {
		"property": {
			"componentType": "datalist",
			"componentName": "DataList334",
			"componentValue": "l_Item_List",
			"componentData": [],
			"componentVisible": true
		},
		"style": {
			"minWidth": "40px",
			"minHeight": "40px"
		},
		"parent": "pnlMainBodyCalculateContent58149",
		"template": {
			"componentDataItemState": {
				"pnlBodyCalculateContent907237": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlBodyCalculateContent907237",
						"componentVisible": true
					},
					"style": {
						"textAlign": "center",
						"padding": "5px",
						"width": "100%",
						"boxShadow": "-1px 1px 1px #545454",
						"border": "1px solid #3C3C3B",
						"borderRadius": "5px",
						"display": "table",
						"verticalAlign": "middle",
						"marginBottom": "5px"
					}
				},
				"Panel816": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel816",
						"componentVisible": true
					},
					"style": {
						"display": "table-cell",
						"width": "80px",
						"height": "75px"
					},
					"parent": "pnlBodyCalculateContent907237"
				},
				"Image180": {
					"property": {
						"componentType": "image",
						"componentName": "Image180",
						"componentValue": require("../../asset/images/default.png"),
						"zoom": false,
						"componentVisible": true
					},
					"style": {
						"width": "80px",
						"height": "75px"
					},
					"parent": "Panel816"
				},
				"Panel60": {
					"style": {
						"padding": "5px",
						"display": "table-cell",
						"verticalAlign": "top",
						"textAlign": "left"
					},
					"parent": "pnlBodyCalculateContent907237",
					"property": {
						"componentType": "panel",
						"componentName": "Panel60",
						"componentVisible": true
					}
				},
				"Label117": {
					"property": {
						"componentType": "label",
						"componentName": "Label117",
						"field": "No",
						"componentValue": "Item No",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"color": "rgb(60, 60, 59)",
						"fontSize": "20px",
						"fontWeight": "600"
					},
					"parent": "Panel60"
				},
				"Label742": {
					"parent": "Panel60",
					"property": {
						"componentType": "label",
						"componentName": "Label742",
						"componentValue": "Description",
						"field": "Description",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(60, 60, 59)",
						"fontSize": "18px"
					}
				},
				"Panel435": {
					"parent": "pnlBodyCalculateContent907237",
					"property": {
						"componentType": "panel",
						"componentName": "Panel435",
						"componentVisible": true
					},
					"style": {
						"textAlign": "center",
						"display": "table-cell",
						"verticalAlign": "middle",
						"width": "40px"
					}
				},
				"Image794": {
					"property": {
						"componentType": "image",
						"componentName": "Image794",
						"componentValue": require("../../asset/icon/icon-add-gray.png"),
						"zoom": false,
						"componentVisible": true
					},
					"style": {
						"width": "25px"
					},
					"parent": "Panel435"
				}
			}
		}
	},
	"pnlMainBodyCalculateContent58436": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyCalculateContent58436",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "150px"
		},
		"parent": "pnlCalculateTableCellContent63886"
	},
	"pnlBtnCalculateYes9129823": {
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "50%",
			"textAlign": "center",
			"borderRight": "1px solid #FFF"
		},
		"parent": "pnlMainFooterCalculateBtn4551783",
		"property": {
			"componentType": "panel",
			"componentName": "pnlBtnCalculateYes9129823",
			"componentVisible": true
		}
	},
	"pnlMainHeaderLeft": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeft",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlFooter899": {
		"style": {
			"textAlign": "center",
			"borderLeft": "0px solid #FFF",
			"verticalAlign": "middle",
			"width": "50%",
			"display": "table-cell"
		},
		"parent": "pnlMainFooter",
		"property": {
			"componentType": "panel",
			"componentName": "pnlFooter899",
			"componentVisible": true
		}
	},
	"pnlCalculateTableCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTableCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"height": "100%",
			"width": "100%",
			"position": "relative",
			"verticalAlign": "middle"
		},
		"parent": "pnlCalculateTable"
	},
	"lblCalculateNo": {
		"property": {
			"componentType": "label",
			"componentName": "lblCalculateNo",
			"componentValue": "NO",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgba(255, 255, 255, 0)",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "bold"
		},
		"parent": "pnlBtnCalculateNo"
	},
	"pnlCalculateTableCell600": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTableCell600",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"verticalAlign": "middle",
			"display": "table-cell",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlCalculateTable668645"
	},
	"pnlModalAddItem": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalAddItem",
			"componentVisible": false
		},
		"style": {
			"background": "rgba(0, 0, 0, 0.60)",
			"position": "absolute",
			"top": "0px",
			"zIndex": "2",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"Panel291": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel291",
			"componentVisible": true
		},
		"style": {
			"height": "50px",
			"backgroundColor": "rgb(255, 69, 0)"
		},
		"parent": "pnlCalculateTableCellContent638435"
	},
	"lblCalculateNo807937": {
		"property": {
			"componentType": "label",
			"componentName": "lblCalculateNo807937",
			"componentValue": "SAVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "16px",
			"backgroundColor": "rgba(255, 255, 255, 0)"
		},
		"parent": "pnlBtnCalculateNo272602"
	},
	"dtSales_Return_Lines": {
		"property": {
			"componentType": "datalist",
			"componentName": "dtSales_Return_Lines",
			"componentValue": "l_Physical_Inventory_Journal",
			"autoLoad": false,
			"componentData": [],
			"componentVisible": true
		},
		"parent": "pnlMainBodyItemsCO2656",
		"template": {
			"componentDataItemState": {
				"Panel17": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel17",
						"componentVisible": true
					},
					"style": {
						"width": "100%",
						"paddingBottom": "0px",
						"display": "table",
						"padding": "5px"
					}
				},
				"pnlProductBodyMiddle1": {
					"parent": "pnlProductBodyMiddle",
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle1",
						"componentVisible": true
					},
					"style": {
						"width": "100%"
					}
				},
				"lblItemCode": {
					"property": {
						"componentType": "label",
						"componentName": "lblItemCode",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(60, 60, 59)",
						"fontWeight": "bold",
						"display": "block",
						"fontSize": "20px"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"lblQuantity": {
					"property": {
						"componentType": "label",
						"componentName": "lblQuantity",
						"componentVisible": true
					},
					"style": {
						"fontSize": "18px",
						"display": "block",
						"marginTop": "10px",
						"textAlign": "left",
						"color": "rgb(0, 153, 0)",
						"fontWeight": "bold",
						"paddingTop": "2px"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"Image776": {
					"property": {
						"componentType": "image",
						"componentName": "Image776",
						"componentValue": require("../../asset/icon/pencil_white.png"),
						"zoom": false,
						"componentVisible": true
					},
					"style": {
						"width": "20px"
					},
					"parent": "Panel489"
				},
				"pnlProduct": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProduct",
						"componentVisible": true
					},
					"style": {
						"position": "relative",
						"width": "100%",
						"border": "1px solid #3C3C3B",
						"display": "table",
						"boxShadow": "-1px 1px 1px #545454",
						"backgroundColor": "rgb(255, 255, 255)",
						"textAlign": "left",
						"borderRadius": "5px"
					},
					"parent": "Panel17"
				},
				"pnlProductBodyMiddle": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle",
						"componentVisible": true
					},
					"style": {
						"padding": "10px",
						"display": "table-cell",
						"verticalAlign": "top",
						"width": "calc(100% - 40px)"
					},
					"parent": "pnlProduct"
				},
				"lblItemName": {
					"style": {
						"fontSize": "20px",
						"display": "none",
						"color": "rgb(111, 111, 110)",
						"fontWeight": "normal",
						"textAlign": "left"
					},
					"parent": "pnlProductBodyMiddle1",
					"property": {
						"componentType": "label",
						"componentName": "lblItemName",
						"field": "Description",
						"componentVisible": true
					}
				},
				"Panel489": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel489",
						"componentVisible": true
					},
					"style": {
						"borderRadius": "0px 5px 5px 0px",
						"display": "table-cell",
						"backgroundColor": "rgb(71, 65, 65)",
						"textAlign": "center",
						"verticalAlign": "middle",
						"width": "40px"
					},
					"parent": "pnlProduct"
				}
			}
		}
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"float": "none",
			"width": "50%",
			"textAlign": "center",
			"padding": "10px 0px"
		},
		"parent": "pnlModalBodyButtons"
	},
	"lblCalculateOK": {
		"property": {
			"componentType": "label",
			"componentName": "lblCalculateOK",
			"componentValue": "YES",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"backgroundColor": "rgba(255, 255, 255, 0)",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px"
		},
		"parent": "pnlBtnCalculateYes"
	},
	"pnlBtnCalculateYes912": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBtnCalculateYes912",
			"componentVisible": true
		},
		"style": {
			"width": "50%",
			"textAlign": "center",
			"borderRight": "1px solid #FFF",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlMainFooterCalculateBtn455"
	},
	"pnlCalculateTableCell600941": {
		"parent": "pnlCalculateTable668645118423",
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTableCell600941",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		}
	},
	"Panel291662": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel291662",
			"componentVisible": true
		},
		"style": {
			"height": "50px",
			"paddingTop": "5px",
			"paddingBottom": "5px",
			"paddingLeft": "10px",
			"paddingRight": "10px"
		},
		"parent": "pnlCalculateTableCellContent638435"
	},
	"pnlModalAddItemQty": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalAddItemQty",
			"componentVisible": false
		},
		"style": {
			"top": "0px",
			"zIndex": "2",
			"width": "100%",
			"height": "100%",
			"position": "absolute",
			"background": "rgba(0, 0, 0, 0.60)"
		},
		"parent": "pnlHolder"
	},
	"pnlCalculateTableCell60070": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTableCell60070",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%",
			"position": "relative",
			"display": "table-cell"
		},
		"parent": "pnlCalculateTable668645362665"
	},
	"pnlMainHeaderLeftTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderLeft"
	},
	"edtAddItemQty": {
		"property": {
			"componentType": "edit",
			"componentName": "edtAddItemQty",
			"componentValue": "100",
			"type": "number",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"verticalAlign": "middle",
			"height": "80px",
			"width": "75%",
			"fontSize": "30px",
			"fontWeight": "500"
		},
		"parent": "pnlBodyCalculateContent907501"
	},
	"pnlModalBodyMain": {
		"parent": "pnlModalCell",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"background": "#ffffff",
			"padding": "10px 0px",
			"marginLeft": "5%",
			"marginRight": "5%",
			"width": "90%",
			"borderBottom": "1px solid #D9D9D9"
		}
	},
	"pnlModalBodyButtons": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"marginRight": "5%",
			"background": "#ffffff",
			"marginLeft": "5%",
			"width": "90%",
			"textAlign": "center"
		},
		"parent": "pnlModalCell"
	},
	"lblCalculateOK302": {
		"property": {
			"componentType": "label",
			"componentName": "lblCalculateOK302",
			"componentValue": "CANCEL",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "16px",
			"backgroundColor": "rgba(255, 255, 255, 0)"
		},
		"parent": "pnlBtnCalculateYes912"
	},
	"lblFilter54": {
		"property": {
			"componentType": "label",
			"componentName": "lblFilter54",
			"componentValue": "ADD",
			"componentVisible": true
		},
		"style": {
			"display": "none",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500"
		},
		"parent": "pnlMainHeaderRightCell"
	},
	"pnlModalBodyTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlModalBodyMessage": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"marginBottom": "10px",
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlModalCalculate": {
		"style": {
			"position": "absolute",
			"top": "0px",
			"zIndex": "2",
			"width": "100%",
			"height": "100%",
			"background": "rgba(0, 0, 0, 0.60)"
		},
		"parent": "pnlHolder",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCalculate",
			"componentVisible": false
		}
	},
	"pnlBtnCalculateYes": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBtnCalculateYes",
			"componentVisible": true
		},
		"style": {
			"width": "50%",
			"textAlign": "center",
			"borderRight": "1px solid #FFF",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlMainFooterCalculateBtn"
	},
	"pnlMainBodyCalculateContent58": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyCalculateContent58",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "150px"
		},
		"parent": "pnlCalculateTableCellContent638"
	},
	"pnlMainHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "65px",
			"backgroundColor": "rgb(255, 69, 0)",
			"paddingTop": "15px",
			"textAlign": "center"
		},
		"parent": "pnlMain"
	},
	"pnlMainHeaderLeftCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftCell",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"paddingLeft": "10px",
			"verticalAlign": "middle",
			"height": "100%",
			"textAlign": "left",
			"display": "table-cell"
		},
		"parent": "pnlMainHeaderLeftTable"
	},
	"pnlMainHeaderMiddle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddle",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "70%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlBodyCalculateContent907": {
		"parent": "pnlMainBodyCalculateContent58",
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyCalculateContent907",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"textAlign": "center"
		}
	},
	"Edit658": {
		"property": {
			"componentType": "edit",
			"componentName": "Edit658",
			"placeHolder": "Search",
			"backgroundAsset": "icon/_search.png",
			"componentVisible": true
		},
		"style": {
			"border": "1px solid #D9D9D9",
			"paddingLeft": "30px",
			"width": "100%",
			"backgroundPosition": "left .5rem center",
			"height": "40px",
			"backgroundSize": "16px"
		},
		"parent": "Panel291662"
	},
	"pnlHolder": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"backgroundColor": "rgb(245, 245, 245)"
		},
		"parent": "pgStockTake"
	},
	"pnlMainBodySearch": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodySearch",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "50px",
			"padding": "5px"
		},
		"parent": "pnlMainSearch"
	},
	"pnlBtnCalculateNo27": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBtnCalculateNo27",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"borderLeft": "1px solid #FFF",
			"display": "table-cell",
			"width": "50%",
			"verticalAlign": "middle"
		},
		"parent": "pnlMainFooterCalculateBtn455"
	},
	"pnlBtnCalculateNo272602": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBtnCalculateNo272602",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"textAlign": "center",
			"borderLeft": "1px solid #FFF",
			"width": "50%"
		},
		"parent": "pnlMainFooterCalculateBtn4551783"
	},
	"imgBack": {
		"property": {
			"componentType": "image",
			"componentName": "imgBack",
			"componentValue": require("../../asset/icon/icon-menu.png"),
			"componentVisible": true
		},
		"style": {
			"width": "35px",
			"height": "35px"
		},
		"parent": "pnlMainHeaderLeftCell"
	},
	"lblModalTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"width": "100%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyTitle"
	},
	"pnlModalBodyButtonPositive": {
		"parent": "pnlModalBodyButtons",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"float": "none",
			"textAlign": "center",
			"padding": "10px 0px",
			"display": "inline-block",
			"width": "50%"
		}
	},
	"lblMsgCalculate": {
		"property": {
			"componentType": "label",
			"componentName": "lblMsgCalculate",
			"componentValue": "Do you want to calculate stocks?",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"display": "block",
			"fontSize": "18px",
			"fontWeight": "bold"
		},
		"parent": "pnlBodyCalculateContent"
	},
	"pnlMainFooterCalculateBtn": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterCalculateBtn",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"display": "table",
			"paddingTop": "1%",
			"paddingBottom": "1%",
			"backgroundColor": "rgba(242, 34, 34, 0.95)",
			"borderRadius": "0px 0px 10px 10px",
			"textAlign": "center",
			"height": "50px"
		},
		"parent": "pnlCalculateTableCellContent"
	},
	"pnlCalculateTableCellContent638": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTableCellContent638",
			"componentVisible": true
		},
		"style": {
			"margin": "5%",
			"background": "#ffffff",
			"borderRadius": "10px",
			"position": "relative",
			"width": "90%",
			"textAlign": "center"
		},
		"parent": "pnlCalculateTableCell600"
	},
	"lblEditQtyUOM": {
		"property": {
			"componentType": "label",
			"componentName": "lblEditQtyUOM",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"paddingTop": "10px",
			"display": "block",
			"fontWeight": "bold",
			"textAlign": "center"
		},
		"parent": "pnlBodyCalculateContent907"
	},
	"pnlMainFooterCalculateBtn455": {
		"style": {
			"width": "100%",
			"height": "40px",
			"backgroundColor": "rgb(255, 69, 0)",
			"borderRadius": "0px 0px 10px 10px",
			"display": "table",
			"textAlign": "center"
		},
		"parent": "pnlCalculateTableCellContent638",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterCalculateBtn455",
			"componentVisible": true
		}
	},
	"pnlMainHeaderMiddleCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleCell",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%",
			"display": "table-cell"
		},
		"parent": "pnlMainHeaderMiddleTable"
	},
	"imgModalLoading": {
		"style": {
			"width": "50px",
			"height": "10px"
		},
		"parent": "pnlModalBodyLoading",
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		}
	},
	"pnlCSS": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCSS",
			"componentVisible": false
		},
		"style": {
			"zIndex": "5",
			"height": "100%",
			"position": "fixed",
			"display": "none",
			"top": "0px",
			"width": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlBodyCalculateContent": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyCalculateContent",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyCalculateContent"
	},
	"pnlModalEditQty": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalEditQty",
			"componentVisible": false
		},
		"style": {
			"position": "absolute",
			"top": "0px",
			"zIndex": "2",
			"background": "rgba(0, 0, 0, 0.60)",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlMainFooterCalculateBtn455666": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterCalculateBtn455666",
			"componentVisible": true
		},
		"style": {
			"borderRadius": "0px 0px 10px 10px",
			"display": "table",
			"width": "100%",
			"textAlign": "center",
			"height": "40px",
			"backgroundColor": "rgb(255, 69, 0)"
		},
		"parent": "pnlCalculateTableCellContent638435"
	},
	"pnlCalculateTableCellContent63886": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTableCellContent63886",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"background": "#ffffff",
			"borderRadius": "10px",
			"position": "relative",
			"width": "90%",
			"margin": "5%"
		},
		"parent": "pnlCalculateTableCell60070"
	},
	"lblAddItemQtyTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblAddItemQtyTitle",
			"componentValue": "Message title",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"fontSize": "20px",
			"fontWeight": "500",
			"padding": "10px",
			"paddingBottom": "0px"
		},
		"parent": "pnlCalculateTableCellContent63886"
	},
	"pnlMainBodyItemsCO2656": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyItemsCO2656",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		},
		"parent": "pnlMainBody"
	},
	"pnlMainHeaderRight": {
		"parent": "pnlMainHeader",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRight",
			"componentVisible": true
		},
		"style": {
			"width": "15%",
			"textAlign": "right",
			"paddingRight": "10px",
			"float": "left",
			"height": "100%"
		}
	},
	"pnlMainHeaderRightCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRightTable"
	},
	"pnlMainSearch": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainSearch",
			"componentVisible": true
		},
		"style": {
			"borderBottom": "1px solid #D9D9D9",
			"width": "100%",
			"height": "50px",
			"textAlign": "center"
		},
		"parent": "pnlMain"
	},
	"pnlModalBodyLoading": {
		"parent": "pnlModalBodyMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		}
	},
	"pnlCalculateTable668645362665": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTable668645362665",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"position": "relative",
			"display": "table"
		},
		"parent": "pnlModalAddItemQty"
	},
	"lblMainTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle",
			"componentValue": "STOCK COUNT",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "500",
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px"
		},
		"parent": "pnlMainHeaderMiddleCell"
	},
	"pnlMainHeaderMiddleTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddle"
	},
	"imgAddNew": {
		"property": {
			"componentType": "image",
			"componentName": "imgAddNew",
			"componentValue": require("../../asset/icon/_add.png"),
			"componentVisible": true
		},
		"style": {
			"width": "35px",
			"height": "35px",
			"padding": "5px"
		},
		"parent": "pnlMainHeaderRightCell"
	},
	"lblModalPositive": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonPositive"
	},
	"pnlBtnCalculateNo": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBtnCalculateNo",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"textAlign": "center",
			"verticalAlign": "middle",
			"width": "50%",
			"borderLeft": "1px solid #FFF"
		},
		"parent": "pnlMainFooterCalculateBtn"
	},
	"edtQty": {
		"property": {
			"componentType": "edit",
			"componentName": "edtQty",
			"type": "number",
			"componentValue": "100",
			"componentVisible": true
		},
		"style": {
			"height": "80px",
			"fontWeight": "500",
			"textAlign": "center",
			"verticalAlign": "middle",
			"width": "75%",
			"fontSize": "30px"
		},
		"parent": "pnlBodyCalculateContent907"
	},
	"pnlMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"zIndex": "0",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"lblStyle": {
		"property": {
			"componentType": "label",
			"componentName": "lblStyle",
			"componentValue": "N/A",
			"componentVisible": true
		},
		"parent": "pnlCSS"
	},
	"pnlCalculateTable": {
		"style": {
			"position": "relative",
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModalCalculate",
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTable",
			"componentVisible": true
		}
	},
	"pnlMainBodyCalculateContent": {
		"style": {
			"display": "table",
			"width": "100%",
			"height": "200px",
			"padding": "10px"
		},
		"parent": "pnlCalculateTableCellContent",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyCalculateContent",
			"componentVisible": true
		}
	},
	"lblCalculateNo807": {
		"property": {
			"componentType": "label",
			"componentName": "lblCalculateNo807",
			"componentValue": "SAVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "16px",
			"backgroundColor": "rgba(255, 255, 255, 0)"
		},
		"parent": "pnlBtnCalculateNo27"
	},
	"txtSearch": {
		"style": {
			"background": "url('data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Capa_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 451 451\" style=\"enable-background:new 0 0 451 451;\" xml:space=\"preserve\" width=\"512px\" height=\"512px\"><g><path d=\"M447.05,428l-109.6-109.6c29.4-33.8,47.2-77.9,47.2-126.1C384.65,86.2,298.35,0,192.35,0C86.25,0,0.05,86.3,0.05,192.3   s86.3,192.3,192.3,192.3c48.2,0,92.3-17.8,126.1-47.2L428.05,447c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4   C452.25,441.8,452.25,433.2,447.05,428z M26.95,192.3c0-91.2,74.2-165.3,165.3-165.3c91.2,0,165.3,74.2,165.3,165.3   s-74.1,165.4-165.3,165.4C101.15,357.7,26.95,283.5,26.95,192.3z\" fill=\"#3c3c3b\"/></g></svg>') no-repeat",
			"margin": "0px",
			"display": "inline",
			"border": "1px solid #D9D9D9",
			"paddingLeft": "30px",
			"fontSize": "16px",
			"textAlign": "left",
			"height": "40px",
			"backgroundColor": "rgb(255, 255, 255)",
			"backgroundPosition": "left .5rem center",
			"backgroundSize": "16px",
			"width": "100%",
			"color": "rgb(60, 60, 59)"
		},
		"parent": "pnlMainBodySearch",
		"property": {
			"componentType": "edit",
			"componentName": "txtSearch",
			"type": "search",
			"backgroundAsset": "icon/_search.png",
			"placeHolder": "Search",
			"componentVisible": true
		}
	},
	"pnlMainFooterCalculateBtn4551783": {
		"style": {
			"backgroundColor": "rgb(255, 69, 0)",
			"display": "table",
			"color": "rgba(0, 0, 0, 0)",
			"paddingTop": "1%",
			"width": "100%",
			"borderRadius": "0px 0px 10px 10px",
			"paddingBottom": "1%",
			"height": "40px",
			"textAlign": "center"
		},
		"parent": "pnlCalculateTableCellContent63886",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterCalculateBtn4551783",
			"componentVisible": true
		}
	},
	"lblCalculateOK302794": {
		"parent": "pnlBtnCalculateYes9129823",
		"property": {
			"componentType": "label",
			"componentName": "lblCalculateOK302794",
			"componentValue": "CANCEL",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "16px",
			"backgroundColor": "rgba(255, 255, 255, 0)"
		}
	},
	"lblEdtQtyTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblEdtQtyTitle",
			"componentValue": "Message title",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "500",
			"padding": "10px",
			"display": "block",
			"color": "rgb(60, 60, 59)",
			"fontSize": "20px",
			"paddingBottom": "0px"
		},
		"parent": "pnlCalculateTableCellContent638"
	},
	"pnlMainFooter": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooter",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "40px",
			"backgroundColor": "rgb(255, 69, 0)"
		},
		"parent": "pnlMain"
	},
	"lblPending35985603856": {
		"property": {
			"componentType": "label",
			"componentName": "lblPending35985603856",
			"componentValue": "RELEASE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px"
		},
		"parent": "pnlFooter899"
	},
	"pnlCalculateTable668645": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTable668645",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"position": "relative",
			"display": "table"
		},
		"parent": "pnlModalEditQty"
	},
	"pnlCalculateTable668645118423": {
		"style": {
			"display": "block",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModalAddItem",
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTable668645118423",
			"componentVisible": true
		}
	},
	"pgStockTake": {
		"property": {
			"componentType": "page",
			"componentName": "pgStockTake",
			"title": "Stock Take",
			"componentVisible": true
		}
	},
	"pnlMainBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		},
		"style": {
			"height": "calc(100% - 100px - 15px - 40px)",
			"overflow": "auto",
			"width": "100%"
		},
		"parent": "pnlMain"
	},
	"pnlModal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"top": "0px",
			"zIndex": "4",
			"width": "100%",
			"height": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"position": "fixed"
		},
		"parent": "pnlHolder"
	},
	"pnlModalTable": {
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table"
		},
		"parent": "pnlModal",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		}
	},
	"lblModalNegative": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonNegative"
	},
	"pnlCalculateTableCellContent": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTableCellContent",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"background": "#ffffff",
			"borderRadius": "10px",
			"position": "relative",
			"width": "90%",
			"margin": "5%"
		},
		"parent": "pnlCalculateTableCell"
	},
	"pnlCalculateTableCellContent638435": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCalculateTableCellContent638435",
			"componentVisible": true
		},
		"style": {
			"height": "90%",
			"borderRadius": "10px",
			"margin": "5%",
			"width": "90%",
			"textAlign": "center",
			"background": "#ffffff"
		},
		"parent": "pnlCalculateTableCell600941"
	},
	"pnlMainBodyCalculateContent58149": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyCalculateContent58149",
			"componentVisible": true
		},
		"style": {
			"padding": "10px",
			"overflow": "auto",
			"width": "100%",
			"height": "calc(100% - 50px - 50px - 40px + 1px)"
		},
		"parent": "pnlCalculateTableCellContent638435"
	},
	"pnlMainHeaderRightTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRight"
	},
	"lblAddItemQtyTitleUOM": {
		"property": {
			"componentType": "label",
			"componentName": "lblAddItemQtyTitleUOM",
			"componentValue": "Quantity",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"display": "block",
			"backgroundColor": "rgba(255, 255, 255, 0)",
			"padding": "10px"
		},
		"parent": "pnlBodyCalculateContent907501"
	},
	"lblEdtQtyTitle537": {
		"property": {
			"componentType": "label",
			"componentName": "lblEdtQtyTitle537",
			"componentValue": "ITEM LIST",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "600",
			"padding": "10px",
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px"
		},
		"parent": "Panel291"
	},
	"pnlBtnCalculateYes912356": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBtnCalculateYes912356",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"width": "50%",
			"textAlign": "center",
			"display": "table-cell"
		},
		"parent": "pnlMainFooterCalculateBtn455666"
	},
	"lblCalculateOK302338": {
		"property": {
			"componentType": "label",
			"componentName": "lblCalculateOK302338",
			"componentValue": "CANCEL",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"backgroundColor": "rgba(255, 255, 255, 0)"
		},
		"parent": "pnlBtnCalculateYes912356"
	},
	"pnlBodyCalculateContent907501": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyCalculateContent907501",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyCalculateContent58436"
	},
	"pnlModalCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlModalTable"
	}
};
