export const StateName = 'pgCheckout';
export const State = {
	"pnlMainBodyItemsCO1": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyItemsCO1",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"backgroundColor": "rgb(135, 135, 135)",
			"marginBottom": "5px"
		},
		"parent": "pnlMainBody"
	},
	"lblBtnOrder": {
		"property": {
			"componentType": "label",
			"componentName": "lblBtnOrder",
			"componentValue": "SUBMIT ORDER",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px"
		},
		"parent": "pnlMainFooterTblCell1"
	},
	"imgUpdateCartClose": {
		"property": {
			"componentType": "image",
			"componentName": "imgUpdateCartClose",
			"componentValue": require("../../asset/icon/icon-close-gray.png"),
			"componentVisible": true
		},
		"style": {
			"right": "7%",
			"height": "25px",
			"position": "absolute",
			"float": "right",
			"zIndex": "2",
			"width": "25px"
		},
		"parent": "Panel513"
	},
	"pnlUpdateCartModalBodyButtonCheckout": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyButtonCheckout",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"backgroundColor": "rgb(255, 69, 0)",
			"display": "block",
			"width": "100%",
			"height": "40px"
		},
		"parent": "pnlUpdateCartModalBodyCheckout"
	},
	"pgCheckout": {
		"style": {
			"backgroundColor": "rgba(0, 0, 0, 0)"
		},
		"property": {
			"componentType": "page",
			"componentName": "pgCheckout",
			"title": "Checkout",
			"componentVisible": true
		}
	},
	"pnlMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"zIndex": "0",
			"width": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlMainHeaderRightCell": {
		"parent": "pnlMainHeaderRightTable",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightCell",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table-cell",
			"verticalAlign": "middle"
		}
	},
	"pnlMainBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "calc(100% - 50px - 40px - 35px - 15px)",
			"overflow": "auto"
		},
		"parent": "pnlMain"
	},
	"pnlUpdateCartModalTableCheckout": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalTableCheckout",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlUpdateCartModalCheckout"
	},
	"pnlUpdateCartModalBodyMiddle": {
		"parent": "pnlUpdateCartModalBodyQtyValueCO",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyMiddle",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"width": "calc(100% - 120px)",
			"height": "40px"
		}
	},
	"pnlMainHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgb(255, 69, 0)",
			"width": "100%",
			"height": "65px",
			"textAlign": "center",
			"paddingTop": "15px"
		},
		"parent": "pnlMain"
	},
	"pnlMainHeaderMiddleCell31": {
		"parent": "pnlMainHeaderMiddleTable161860",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleCell31",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		}
	},
	"pnlMainBodyDetails1": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyDetails1",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainBodyDetails"
	},
	"lblModalTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"width": "100%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "18px"
		},
		"parent": "pnlModalBodyTitle"
	},
	"lblAddress": {
		"style": {
			"float": "initial",
			"textAlign": "center",
			"color": "rgb(255, 255, 255)",
			"fontWeight": "normal",
			"display": "block",
			"fontSize": "16px"
		},
		"parent": "pnlMainBodyForm3LabelCustomerCode",
		"property": {
			"componentType": "label",
			"componentName": "lblAddress",
			"field": "name",
			"componentVisible": true
		}
	},
	"lblUpdateCartItemNameValueCO": {
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateCartItemNameValueCO",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"fontWeight": "normal",
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"textAlign": "left"
		},
		"parent": "pnlUpdateCartModalBodyCheckoutName"
	},
	"imgUpdateCartImage": {
		"parent": "pnlUpdateCartModalBodyCheckoutImage",
		"property": {
			"componentType": "image",
			"componentName": "imgUpdateCartImage",
			"componentValue": require("../../asset/images/default.png"),
			"componentVisible": true
		},
		"style": {
			"height": "100px",
			"width": "100px"
		}
	},
	"pnlUpdateCartModalBodyCheckoutName": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyCheckoutName",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"position": "relative"
		},
		"parent": "Panel514"
	},
	"pnlMainHeaderMiddleTable161860": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTable161860",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddle458"
	},
	"pnlMainBodyForm": {
		"parent": "pnlMainBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"backgroundColor": "rgb(84, 84, 84)"
		}
	},
	"pnlMainBodyItemsCO2": {
		"parent": "pnlMainBodyItems",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyItemsCO2",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"padding": "5px",
			"paddingTop": "0px",
			"paddingBottom": "0px"
		}
	},
	"lblTotalAmountExclGST": {
		"property": {
			"componentType": "label",
			"componentName": "lblTotalAmountExclGST",
			"componentValue": "Total Amount",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "600"
		},
		"parent": "Panel495"
	},
	"pnlUpdateCartModalBodyCheckoutCode": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyCheckoutCode",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"width": "100%",
			"paddingTop": "20px"
		},
		"parent": "Panel514"
	},
	"pnlMainHeaderLeft": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeft",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"float": "left",
			"width": "15%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlMainBodyForm3LabelCustomerCode": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm3LabelCustomerCode",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyFormCustomerCode"
	},
	"lblTotalAmount_data": {
		"property": {
			"componentType": "label",
			"componentName": "lblTotalAmount_data",
			"componentValue": "$ 0.00",
			"componentVisible": true
		},
		"style": {
			"fontSize": "20px",
			"fontWeight": "600",
			"color": "rgb(255, 255, 255)"
		},
		"parent": "Panel495500"
	},
	"pnlMainFooterTblCell1": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterTblCell1",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgba(0, 0, 0, 0)",
			"height": "100%",
			"verticalAlign": "middle",
			"textAlign": "center",
			"display": "table-cell"
		},
		"parent": "pnlMainFooterTbl"
	},
	"pnlModalTable": {
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModal",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		}
	},
	"pnlModalCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlModalTable"
	},
	"pnlUpdateCartModalCheckout": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalCheckout",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"zIndex": "2",
			"height": "100%",
			"top": "0px",
			"width": "100%",
			"background": "rgba(0, 0, 0, 0.60)"
		},
		"parent": "pnlHolder"
	},
	"lblUpdateCartQuantityIL": {
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateCartQuantityIL",
			"componentValue": "Quantity",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"fontSize": "16px",
			"fontWeight": "bolder",
			"color": "rgb(255, 255, 255)",
			"display": "none"
		},
		"parent": "pnlUpdateCartModalBodyQtyCO"
	},
	"btnUpdate": {
		"property": {
			"componentType": "fbutton",
			"componentName": "btnUpdate",
			"componentValue": "UPDATE",
			"componentVisible": true
		},
		"style": {
			"padding": "8px",
			"margin": "0px",
			"display": "none",
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"height": "40px",
			"fontWeight": "bold",
			"backgroundColor": "rgba(242, 34, 34, 0.95)"
		},
		"parent": "pnlUpdateCartModalBodyButtonCheckout"
	},
	"pnlMainHeaderMiddle458": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddle458",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "70%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"lblOrderDate": {
		"parent": "pnlMainBodyForm3LabelCustomerCode",
		"property": {
			"componentType": "label",
			"componentName": "lblOrderDate",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"textAlign": "center",
			"fontWeight": "normal",
			"fontSize": "16px"
		}
	},
	"Panel495": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel495",
			"componentVisible": true
		},
		"style": {
			"paddingLeft": "10px",
			"display": "table-cell",
			"verticalAlign": "middle",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyDetails1"
	},
	"pnlModalBodyMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		},
		"style": {
			"borderBottom": "1px solid #D9D9D9",
			"padding": "10px 0px",
			"marginLeft": "5%",
			"marginRight": "5%",
			"width": "90%",
			"textAlign": "center",
			"background": "#ffffff"
		},
		"parent": "pnlModalCell"
	},
	"imgPlus": {
		"style": {
			"width": "50px",
			"height": "40px",
			"float": "left"
		},
		"parent": "pnlUpdateCartModalBodyRight",
		"property": {
			"componentType": "image",
			"componentName": "imgPlus",
			"componentValue": require("../../asset/plus-white.png"),
			"componentVisible": true
		}
	},
	"pnlUpdateCartModalBodyQtyCO": {
		"parent": "Panel414",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyQtyCO",
			"componentVisible": true
		},
		"style": {
			"textAlign": "left"
		}
	},
	"dlltem": {
		"property": {
			"componentType": "datalist",
			"componentName": "dlltem",
			"componentValue": "l_cart",
			"autoLoad": false,
			"componentData": [],
			"componentVisible": true
		},
		"parent": "pnlMainBodyItemsCO2",
		"template": {
			"componentDataItemState": {
				"pnlProductBodyMiddle": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle",
						"componentVisible": true
					},
					"style": {
						"display": "table-cell",
						"paddingLeft": "10px",
						"verticalAlign": "top",
						"width": "calc(100% - 110px - 50px)"
					},
					"parent": "pnlProduct"
				},
				"lblItemCode": {
					"property": {
						"componentType": "label",
						"componentName": "lblItemCode",
						"field": "ItemCode",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(60, 60, 59)",
						"fontSize": "24px",
						"fontWeight": "bold"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"pnlLineFooter": {
					"style": {
						"backgroundColor": "rgb(71, 65, 65)",
						"display": "table",
						"height": "25px",
						"borderRadius": "0px 0px 5px 5px",
						"width": "100%"
					},
					"parent": "Panel764",
					"property": {
						"componentType": "panel",
						"componentName": "pnlLineFooter",
						"componentVisible": true
					}
				},
				"pnlLineFooterEditQty": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlLineFooterEditQty",
						"componentVisible": true
					},
					"style": {
						"verticalAlign": "middle",
						"width": "50%",
						"display": "table-cell",
						"height": "100%",
						"textAlign": "center",
						"borderRight": "1px solid #FFF"
					},
					"parent": "pnlLineFooter"
				},
				"Label561": {
					"property": {
						"componentType": "label",
						"componentName": "Label561",
						"componentValue": "REMOVE",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(255, 255, 255)",
						"fontSize": "16px"
					},
					"parent": "pnlLineFooterRemove"
				},
				"pnlProductBodyMiddle1": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle1",
						"componentVisible": true
					},
					"style": {
						"width": "100%"
					},
					"parent": "pnlProductBodyMiddle"
				},
				"Panel764": {
					"style": {
						"boxShadow": "-1px 1px 1px #545454",
						"borderRadius": "5px",
						"marginBottom": "5px",
						"minWidth": "40px",
						"minHeight": "50px",
						"border": "1px solid #3C3C3B"
					},
					"property": {
						"componentType": "panel",
						"componentName": "Panel764",
						"componentVisible": true
					}
				},
				"pnlProduct": {
					"parent": "Panel764",
					"property": {
						"componentType": "panel",
						"componentName": "pnlProduct",
						"componentVisible": true
					},
					"style": {
						"backgroundColor": "rgb(255, 255, 255)",
						"width": "100%",
						"textAlign": "left",
						"borderRadius": "5px 5px 0px 0px",
						"padding": "5px",
						"position": "relative",
						"display": "table"
					}
				},
				"lblPrice": {
					"property": {
						"componentType": "label",
						"componentName": "lblPrice",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"paddingTop": "2px",
						"color": "rgb(0, 153, 0)",
						"textAlign": "left",
						"marginTop": "10px",
						"fontSize": "18px",
						"fontWeight": "bold"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"pnlProductBodyLeft": {
					"parent": "pnlProduct",
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyLeft",
						"componentVisible": true
					},
					"style": {
						"textAlign": "center",
						"display": "table-cell",
						"verticalAlign": "top",
						"width": "110px"
					}
				},
				"imgItem": {
					"property": {
						"componentType": "image",
						"componentName": "imgItem",
						"componentValue": require("../../asset/images/default.png"),
						"componentVisible": true
					},
					"style": {
						"height": "90px",
						"width": "100px"
					},
					"parent": "pnlProductBodyLeft"
				},
				"lblItemName": {
					"style": {
						"fontWeight": "normal",
						"display": "block",
						"color": "rgb(60, 60, 59)",
						"fontSize": "20px",
						"textAlign": "left"
					},
					"parent": "pnlProductBodyMiddle1",
					"property": {
						"componentType": "label",
						"componentName": "lblItemName",
						"field": "ItemName",
						"componentVisible": true
					}
				},
				"Label191": {
					"property": {
						"componentType": "label",
						"componentName": "Label191",
						"componentValue": "EDIT QTY",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(255, 255, 255)",
						"fontSize": "16px"
					},
					"parent": "pnlLineFooterEditQty"
				},
				"pnlLineFooterRemove": {
					"style": {
						"width": "50%",
						"textAlign": "center",
						"borderLeft": "1px solid #FFF",
						"display": "table-cell",
						"verticalAlign": "middle"
					},
					"parent": "pnlLineFooter",
					"property": {
						"componentType": "panel",
						"componentName": "pnlLineFooterRemove",
						"componentVisible": true
					}
				}
			}
		}
	},
	"pnlModalBodyLoading": {
		"parent": "pnlModalBodyMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		}
	},
	"lblStyle": {
		"property": {
			"componentType": "label",
			"componentName": "lblStyle",
			"componentValue": "N/A",
			"componentVisible": true
		},
		"parent": "pnlCSS"
	},
	"pnlUpdateCartModalBodyCheckoutImage": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyCheckoutImage",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"position": "relative",
			"display": "block",
			"height": "100px"
		},
		"parent": "Panel514"
	},
	"pnlMainHeaderLeftCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"width": "100%",
			"height": "100%",
			"verticalAlign": "middle",
			"textAlign": "left",
			"paddingLeft": "10px"
		},
		"parent": "pnlMainHeaderLeftTable"
	},
	"Panel513": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel513",
			"componentVisible": true
		},
		"style": {
			"minHeight": "50px",
			"paddingTop": "2%",
			"minWidth": "40px"
		},
		"parent": "pnlUpdateCartModalBodyCheckout"
	},
	"Panel514": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel514",
			"componentVisible": true
		},
		"style": {
			"minHeight": "50px",
			"padding": "3%",
			"zIndex": "1",
			"minWidth": "40px"
		},
		"parent": "Panel513"
	},
	"lblUpdateCartItemCodeValueCO": {
		"style": {
			"display": "block",
			"color": "rgb(60, 60, 59)",
			"textAlign": "left",
			"fontSize": "24px",
			"fontWeight": "normal"
		},
		"parent": "pnlUpdateCartModalBodyCheckoutCode",
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateCartItemCodeValueCO",
			"componentVisible": true
		}
	},
	"pnlModalBodyTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlCSS": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCSS",
			"componentVisible": false
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"position": "fixed",
			"display": "none",
			"top": "0px",
			"zIndex": "4"
		},
		"parent": "pnlHolder"
	},
	"pnlUpdateCartModalCellCheckout": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalCellCheckout",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlUpdateCartModalTableCheckout"
	},
	"pnlUpdateCartModalBodyCheckoutQty": {
		"parent": "Panel414",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyCheckoutQty",
			"componentVisible": true
		},
		"style": {
			"marginBottom": "10px",
			"width": "100%"
		}
	},
	"imgModalLoading": {
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		},
		"style": {
			"width": "50px",
			"height": "10px"
		},
		"parent": "pnlModalBodyLoading"
	},
	"lblModalPositive": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonPositive"
	},
	"pnlUpdateCartModalBodyLeft": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyLeft",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"backgroundColor": "rgb(255, 69, 0)",
			"verticalAlign": "middle",
			"height": "40px",
			"display": "table-cell",
			"width": "50px"
		},
		"parent": "pnlUpdateCartModalBodyQtyValueCO"
	},
	"pnlUpdateCartModalCheckoutFooterNavTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalCheckoutFooterNavTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlUpdateCartModalBodyButtonCheckout"
	},
	"pnlHolder": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%"
		},
		"parent": "pgCheckout"
	},
	"pnlMainHeaderLeftTable": {
		"parent": "pnlMainHeaderLeft",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		}
	},
	"lblItems": {
		"property": {
			"componentType": "label",
			"componentName": "lblItems",
			"componentValue": "DETAILS",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"paddingTop": "5px",
			"display": "inline-block",
			"fontSize": "16px",
			"paddingBottom": "5px",
			"width": "calc(100% - 35px)",
			"padding": "10px",
			"color": "rgb(255, 215, 0)"
		},
		"parent": "pnlMainBodyItemsCO1"
	},
	"pnlMainBodyDetails": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyDetails",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "35px",
			"backgroundColor": "rgb(84, 84, 84)"
		},
		"parent": "pnlMain"
	},
	"txtQuantity": {
		"property": {
			"componentType": "edit",
			"componentName": "txtQuantity",
			"componentValue": "0",
			"type": "number",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "100%",
			"fontSize": "24px",
			"fontWeight": "normal",
			"height": "40px",
			"color": "rgb(33, 33, 33)",
			"border": "1px solid #FFF",
			"textAlign": "center"
		},
		"parent": "pnlUpdateCartModalBodyMiddle"
	},
	"pnlUpdateCartModalBodyRight": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyRight",
			"componentVisible": true
		},
		"style": {
			"height": "40px",
			"backgroundColor": "rgb(255, 69, 0)",
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "50px",
			"textAlign": "center"
		},
		"parent": "pnlUpdateCartModalBodyQtyValueCO"
	},
	"pnlMainHeaderRightTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRight"
	},
	"pnlMainBodyItems": {
		"parent": "pnlMainBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyItems",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		}
	},
	"pnlModalBodyButtons": {
		"style": {
			"width": "90%",
			"background": "#ffffff",
			"textAlign": "center",
			"marginLeft": "5%",
			"marginRight": "5%"
		},
		"parent": "pnlModalCell",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"addClass": "hide",
			"componentVisible": true
		}
	},
	"lblUpdateCartPriceValueCO": {
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateCartPriceValueCO",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(0, 153, 0)",
			"textAlign": "left",
			"fontSize": "20px",
			"fontWeight": "normal"
		},
		"parent": "pnlUpdateCartModalBodyCheckoutPrice"
	},
	"Image553": {
		"style": {
			"width": "25px"
		},
		"parent": "pnlMainBodyItemsCO1",
		"property": {
			"componentType": "image",
			"componentName": "Image553",
			"componentValue": require("../../asset/plus-white.png"),
			"zoom": false,
			"componentVisible": true
		}
	},
	"pnlMainFooter": {
		"style": {
			"width": "100%",
			"height": "40px",
			"backgroundColor": "rgb(255, 69, 0)"
		},
		"parent": "pnlMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooter",
			"componentVisible": true
		}
	},
	"pnlMainFooterTbl": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterTbl",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainFooter"
	},
	"pnlModal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"width": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"top": "0px",
			"zIndex": "3",
			"position": "fixed",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"lblMainTitle667": {
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle667",
			"componentValue": "CHECKOUT",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500"
		},
		"parent": "pnlMainHeaderMiddleCell31"
	},
	"pnlMainHeaderRight": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRight",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlMainBodyFormCustomerCode": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyFormCustomerCode",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"padding": "10px"
		},
		"parent": "pnlMainBodyForm"
	},
	"lblCustomerCodeName": {
		"property": {
			"componentType": "label",
			"componentName": "lblCustomerCodeName",
			"field": "code",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(255, 215, 0)",
			"fontWeight": "bold",
			"textAlign": "center",
			"fontSize": "18px"
		},
		"parent": "pnlMainBodyForm3LabelCustomerCode"
	},
	"lblUOMValue": {
		"parent": "pnlUpdateCartModalBodyQtyCO",
		"property": {
			"componentType": "label",
			"componentName": "lblUOMValue",
			"componentVisible": true
		},
		"style": {
			"fontSize": "16px",
			"textAlign": "center",
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"fontWeight": "bold"
		}
	},
	"pnlModalBodyMessage": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"pnlModalBodyButtonPositive": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"float": "none",
			"width": "50%",
			"textAlign": "center",
			"padding": "10px 0px"
		},
		"parent": "pnlModalBodyButtons"
	},
	"lblModalNegative": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonNegative"
	},
	"Panel414": {
		"style": {
			"backgroundColor": "rgb(71, 65, 65)",
			"padding": "5%",
			"paddingBottom": "20px"
		},
		"parent": "Panel505",
		"property": {
			"componentType": "panel",
			"componentName": "Panel414",
			"componentVisible": true
		}
	},
	"pnlFooterNavUpdateBtn": {
		"property": {
			"componentType": "label",
			"componentName": "pnlFooterNavUpdateBtn",
			"componentValue": "UPDATE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px"
		},
		"parent": "pnlUpdateCartModalCheckoutFooterNavTableCell"
	},
	"pnlUpdateCartModalBodyCheckoutPrice": {
		"parent": "Panel514",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyCheckoutPrice",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginTop": "20px"
		}
	},
	"Panel505": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel505",
			"componentVisible": true
		},
		"style": {
			"minWidth": "40px",
			"minHeight": "50px"
		},
		"parent": "Panel513"
	},
	"imgMinus": {
		"property": {
			"componentType": "image",
			"componentName": "imgMinus",
			"componentValue": require("../../asset/minus-white.png"),
			"componentVisible": true
		},
		"style": {
			"width": "50px",
			"height": "40px",
			"float": "left"
		},
		"parent": "pnlUpdateCartModalBodyLeft"
	},
	"pnlUpdateCartModalCheckoutFooterNavTableCell": {
		"parent": "pnlUpdateCartModalCheckoutFooterNavTable",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalCheckoutFooterNavTableCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"textAlign": "center"
		}
	},
	"pnlUpdateCartModalBodyCheckout": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyCheckout",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"background": "#ffffff",
			"marginRight": "5%",
			"width": "90%",
			"textAlign": "center",
			"marginLeft": "5%"
		},
		"parent": "pnlUpdateCartModalCellCheckout"
	},
	"pnlUpdateCartModalBodyQtyValueCO": {
		"style": {
			"display": "table",
			"width": "100%",
			"height": "40px"
		},
		"parent": "pnlUpdateCartModalBodyCheckoutQty",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyQtyValueCO",
			"componentVisible": true
		}
	},
	"imgBack": {
		"property": {
			"componentType": "image",
			"componentName": "imgBack",
			"componentValue": require("../../asset/icon/icon-back.png"),
			"componentVisible": true
		},
		"style": {
			"width": "35px",
			"height": "35px"
		},
		"parent": "pnlMainHeaderLeftCell"
	},
	"Panel495500": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel495500",
			"componentVisible": true
		},
		"style": {
			"textAlign": "right",
			"paddingRight": "10px",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlMainBodyDetails1"
	},
	"lblModalMessage": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(111, 111, 110)",
			"fontSize": "18px"
		},
		"parent": "pnlModalBodyMessage"
	},
	"pnlModalBodyButtonNegative": {
		"style": {
			"display": "inline-block",
			"float": "none",
			"textAlign": "center",
			"padding": "10px 0px",
			"width": "50%"
		},
		"parent": "pnlModalBodyButtons",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		}
	}
};
