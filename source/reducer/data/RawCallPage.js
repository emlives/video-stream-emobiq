export const StateName = 'RawCallPage';
export const State = {
    page: {
		style: {
			flex: 1,
            position: 'relative',
            backgroundColor: 'white'
		},
		property: {
			componentDisabled: false,
		}
	},
    buttonTestRawCall: {
        style:{
            backgroundColor: 'black',
            borderRadius: 12,
            margin: '10px 20px',
            marginTop: '20px'
        },
        property: {
            componentType: 'button',
            componentValue: 'RawCall { local = false }'
        }
    },
    buttonTestLocalRawCallGet: {
        style:{
            backgroundColor: 'black',
            borderRadius: 12,
            margin: '10px 20px',
            marginTop: '20px'
        },
        property: {
            componentType: 'button',
            componentValue: 'RawCall { local = true }'
        }
    },
    "Image716977": {
		"property": {
			"componentType": "image",
			"componentName": "Image716977",
			"componentValue": require("../../asset/images/logo-emobiq-power.png"),
			"componentVisible": true
		},
		"style": {
			"width": "50%"
		},
		"parent": "Panel205931"
	},
    label276: {
        style: {
            width:'117.00px',
            fontSize: 16,
            fontWeight: 'bold',
            position: 'absolute',
            overflowWrap: 'break-word',
            top: '11.98px',
            left: '13.95px',
            height: '25.00px',
            // lineHeight: 1.8
        },
        property: {
            componentValue: 'White Tiger Roller Coaster',
            componentVisible: true,
        },
        componentType: 'label',
    },
    label146: {
        style: {
            top: '87.95px',
            width: '130.00px',
            color: 'rgb(252, 249, 249)',
            backgroundSize: 'cover',
            padding: '10px',
            overflowWrap: 'break-word',
            left: '14.97px',
            backgroundImage: {uri:'https://reactjs.org/logo-og.png'},
            // backgroundPosition: 'top',
            position: 'absolute',
            height: '148.00px',
            borderBottomRightRadius: 40,
            overflow: 'hidden',
            borderStyle: 'dashed',
            borderWidth: 4,
            borderColor: 'orange'
        },
        property: {
            componentValue: 'White Tiger Roller Coaster',
            componentVisible: true,
        },
        componentType: 'label',
    },
    label141: {
        style: {
            top: '2.62%',
            left: '53.00%',
            height: '161.00px',
            padding: '20px',
            width: '123.00px',
            overflowWrap: 'break-word',
            textAlign: 'center',
            position: 'absolute',
            justifyContent: 'center',
            alignItems: 'center',
            // backgroundImage: 'linear-gradient(to right, red, yellow)',
            color: 'white',
            backgroundImage: {uri:'https://reactjs.org/logo-og.png'},
            backgroundRepeat: 'hidden',
            // backgroundSize: '50% 50%', 
            borderStyle: 'dotted', // won't work for one-sided border
            borderRadius: 1, // need to include this for one-sided border to work
            borderWidth: 4,
            borderColor: 'orange',
            borderImageWidth : 4,
        },
        property: {
            componentValue: 'Alice in Borderland',
            componentVisible: true,
        },
        componentType: 'label',
    },
    label343: {
        style: {
            overflowWrap: 'break-word',
            justifyContent: 'center',
            textAlign: 'center',
            top: '255.98px',
            alignItems: 'center',
            height: '88.00px',
            left: '16.97px',
            width: '129.00px',
            position: 'absolute',
            backgroundImage: 'linear-gradient(to left, tomato, yellow, hotpink, springgreen)',
            color: 'red'
        },
        property: {
            componentValue: 'Netflix',
            componentVisible: true,
        },
        componentType: 'label',
    },
    Panel583: {
        style: {
            position: 'absolute',
            top: '78.73%',
            left: '5.70%',
            width: '268.00px',
            height: '100.00px',
            backgroundColor: 'rgb(241, 251, 190)',
            borderStyle: 'solid',
            borderWidth: '4px',
            borderColor: 'hotpink',
            flexDirection: "row",
            backgroundImage: {uri:'https://reactjs.org/logo-og.png'},
        }
    },
    Label870: {
        style: {
            position: "relative",
            top: 0,
            left: 0,
            color: 'springgreen'
            // float: "right",
            // color: "blue",
        },
        property: {
            componentValue: 'Label1',
            componentName: 'Label870',
        },
        componentType: 'label',
    },

    Image387: {
        style: {
            position: "absolute",
            top: "16.05%",
            left: "3.18%",
            width: 63.00,
            height: 71.00,
        },
        property: {
            //componentValue: require('../../framework/core/asset/default.png'),
            componentValue: {uri: 'https://i.imgur.com/P2A4PB6.jpg'},
            componentVisible: true,
        },
        componentType: 'image',
    },
    
    Image159: {
        style: {
            position: 'absolute',
            flex: 1,
            top: '41.88%',
            left: '52.85%',
            width: '100px',
            height: '100px',
            backgroundColor: 'rgb(225, 116, 116)',
            backgroundSize: '50% 50%',
            padding: '10px',
            borderStyle: 'dotted',
            borderWidth: '2px',
            borderColor: 'springgreen',
        },
        property: {
            //componentValue: require('../../framework/core/asset/default.png'),
            componentValue: {uri:'https://reactjs.org/logo-og.png'},
            componentVisible: true,
        },
        componentType: 'image',
    },


    // testing
    labelSimple: {
        style: {
            position:'absolute',
            flex: 1,
	        // padding:'10px',
	        backgroundColor:'white',
            // backgroundImage: 'linear-gradient(to right, red, orange)',
            // backgroundImage: 'linear-gradient(to left, tomato, yellow, hotpink, springgreen)',
            // backgroundImage: {uri:'https://reactjs.org/logo-og.png'},
	        /*overflow-wrap:break-word;*/
            // backgroundSize: '70px 100%',
            // backgroundRepeat: 'repeat',
            // backgroundSize: '50% 100%',
            // backgroundSize: 'cover',
	        top:'5%',
	        left:'5%',
	        width:'40%',
	        // height:'30%',
            padding: 0,
            fontSize: 16,
            fontWeight: 'bold',
            color: 'black',
            lineHeight: 20,
        },
        property: {
            componentValue: 'White Tiger Roller Coaster',
            componentVisible: true,
        },
        componentType: 'label',
    },
    labelCover: {
        style: {
            position:'absolute',
	        // padding:'10px',
	        backgroundColor:'white',
            // backgroundImage: 'linear-gradient(to right, red, orange)',
            backgroundImage: 'linear-gradient(to left, tomato, yellow, hotpink, springgreen)',
            backgroundImage: {uri:'https://reactjs.org/logo-og.png'},
	        /*overflow-wrap:break-word;*/
            // backgroundSize: '70px 100%',
            // backgroundRepeat: 'repeat',
            // backgroundSize: '50% 100%',
            // backgroundSize: 'cover',
	        top:'12%',
	        left:'5%',
	        width:'40%',
	        height:'29%',
            padding: 10,
            color: 'white',
            lineHeight: 20,
        },
        property: {
            componentValue: 'White Tiger Roller Coaster',
            componentVisible: true,
        },
        componentType: 'label',
    },
    labelRepeat: {
        style: {
            position:'absolute',
	        // padding:'10px',
	        backgroundColor:'white',
            // backgroundImage: 'linear-gradient(to right, red, orange)',
            backgroundImage: 'linear-gradient(to left, tomato, yellow, hotpink, springgreen)',
            backgroundImage: {uri:'https://reactjs.org/logo-og.png'},
	        /*overflow-wrap:break-word;*/
            // backgroundSize: '60px 100%',
            backgroundRepeat: 'repeat',
            backgroundSize: '50% 50%',
            // backgroundSize: 'cover',
	        top:'5%',
	        left:'50%',
	        width:'40%',
	        height:'180.00px',
            padding: 20,
            color: 'white',
            lineHeight: 20,
            textAlign: 'center',
        },
        property: {
            componentValue: 'White Tiger Roller Coaster',
            componentVisible: true,
        },
        componentType: 'label',
    },
    labelStretch: {
        style: {
            position:'absolute',
	        // padding:'10px',
	        backgroundColor:'white',
            // backgroundImage: 'linear-gradient(to right, red, orange)',
            // backgroundImage: 'linear-gradient(to left, tomato, yellow, hotpink, springgreen)',
            backgroundImage: {uri:'https://reactjs.org/logo-og.png'},
	        /*overflow-wrap:break-word;*/
            // backgroundSize: '50% 100%',
            // backgroundRepeat: 'repeat',
            // backgroundSize: '50% 50%',
            // backgroundSize: 'cover',
	        top:'5%',
	        left:'50%',
	        width:'40%',
	        height:'180.00px',
            padding: 20,
            color: 'white',
            lineHeight: 20,
            textAlign: 'center',
        },
        property: {
            componentValue: 'White Tiger Roller Coaster',
            componentVisible: true,
        },
        componentType: 'label',
    },
    labelGradientToLeft: {
        style: {
            position:'absolute',
            overflow: 'hidden',
	        backgroundColor:'white',
            // backgroundImage: 'linear-gradient(to right, red, orange)',
            backgroundImage: 'linear-gradient(to left, tomato, yellow, hotpink, springgreen)',
            // backgroundImage: {uri:'https://reactjs.org/logo-og.png'},
	        /*overflow-wrap:break-word;*/
            // backgroundSize: '60px 100%',
            // backgroundRepeat: 'repeat',
            // backgroundSize: '50% 50%',
            // backgroundSize: 'cover',
	        top:'45%',
	        left:'5%',
	        width:'40%',
	        height:'100.00px',
            fontSize: 24,
            padding: 10,
            color: 'orangered',
            borderBottomRightRadius: 40,
            lineHeight: 20,
            textAlign: 'center',
            justifyContent: 'center', //Centered vertically
            alignItems: 'center', // Centered horizontally
        },
        property: {
            componentValue: 'Netflix',
            componentVisible: true,
        },
        componentType: 'label',
    },
    labelGradientToBottom: {
        style: {
            overflow: 'hidden',
            position:'absolute',
	        backgroundColor:'white',
            // backgroundImage: 'linear-gradient(to right, red, orange)',
            backgroundImage: 'linear-gradient(tomato, yellow, hotpink, springgreen)',
            // backgroundImage: {uri:'https://reactjs.org/logo-og.png'},
	        /*overflow-wrap:break-word;*/
            backgroundSize: '50% 50%',
            // backgroundRepeat: 'repeat',
            // backgroundSize: '50% 50%',
            // backgroundSize: 'cover',
	        top:'30%',
	        left:'50%',
	        width:'160px',
	        height:'160px',
            borderRadius: 80,
            borderWidth: 4,
            borderColor: 'hotpink',
            borderStyle: 'dashed',
            // padding: 10,
            color: 'orangered',
            lineHeight: 20,
            textAlign: 'center',
            justifyContent: 'center',
            alignItems: 'center',
            margin: 10,
        },
        property: {
            componentValue: 'Netflix',
            componentVisible: true,
        },
        componentType: 'label',
    },
    //Panel
   
   
    Label871: {
        style: {
            position: "relative",
            top: 0,
            left: 0,
        },
        property: {
            componentValue: 'Label',
        },
        componentType: 'label',
    },
    Label872: {
        style: {
        position: "relative",
            top: 0,
            left: 0,
        },
        property: {
            componentValue: 'Label',
        },
        componentType: 'label',
    },
}
