export const StateName = 'pgSalesReturnDetailsUpdate';
export const State = {
	"pnlMainHeaderRight": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRight",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlModal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"top": "0px",
			"height": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"zIndex": "3",
			"width": "100%"
		},
		"parent": "pnlHolder"
	},
	"pgSalesReturnDetailsUpdate": {
		"property": {
			"componentType": "page",
			"componentName": "pgSalesReturnDetailsUpdate",
			"title": "Sales Return Details Update",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgba(0, 0, 0, 0)"
		}
	},
	"pnlMainHeaderLeft": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeft",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"lblAddress": {
		"property": {
			"componentType": "label",
			"componentName": "lblAddress",
			"field": "name",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"fontWeight": "normal",
			"color": "rgb(255, 255, 255)",
			"fontSize": "16px",
			"float": "initial",
			"textAlign": "center"
		},
		"parent": "pnlBodyContentHeader"
	},
	"Label9": {
		"property": {
			"componentType": "label",
			"componentName": "Label9",
			"componentValue": "OK",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"fontWeight": "500"
		},
		"parent": "Panel960"
	},
	"pnlMainBodyDetails1872": {
		"style": {
			"borderTop": "0px solid #FFF",
			"paddingTop": "5px",
			"width": "100%",
			"overflow": "auto"
		},
		"parent": "pnlMainBodyDetails",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyDetails1872",
			"componentVisible": true
		}
	},
	"pnlUpdateQtyContentInputQtyTableCell3": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentInputQtyTableCell3",
			"componentVisible": true
		},
		"style": {
			"width": "50px",
			"height": "40px",
			"display": "table-cell"
		},
		"parent": "pnlUpdateQtyContentInputQtyTable"
	},
	"dtSales_Return_Lines": {
		"property": {
			"componentType": "datalist",
			"componentName": "dtSales_Return_Lines",
			"componentValue": "l_ Sales_Return_Lines",
			"autoLoad": false,
			"componentData": [],
			"componentVisible": true
		},
		"parent": "pnlMainBodyItemsCO2656",
		"template": {
			"componentDataItemState": {
				"Panel17": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel17",
						"componentVisible": true
					},
					"style": {
						"border": "2px solid #545454",
						"width": "100%",
						"boxShadow": "-3px 3px 3px #545454",
						"borderRadius": "10px",
						"marginBottom": "5px",
						"display": "table"
					}
				},
				"pnlProduct": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProduct",
						"componentVisible": true
					},
					"style": {
						"position": "relative",
						"padding": "10px",
						"width": "100%",
						"textAlign": "left",
						"paddingTop": "5px",
						"display": "table",
						"backgroundColor": "rgb(255, 255, 255)",
						"paddingBottom": "10px",
						"borderRadius": "10px 10px 0px 0px"
					},
					"parent": "Panel17"
				},
				"imgItem": {
					"style": {
						"width": "100px",
						"height": "90px"
					},
					"parent": "pnlProductBodyLeft",
					"property": {
						"componentType": "image",
						"componentName": "imgItem",
						"componentValue": require("../../asset/images/default.png"),
						"componentVisible": true
					}
				},
				"lblItemName": {
					"property": {
						"componentType": "label",
						"componentName": "lblItemName",
						"field": "Description",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"color": "rgb(111, 111, 110)",
						"textAlign": "left",
						"fontSize": "20px",
						"fontWeight": "normal"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"lblReasonCode_data": {
					"property": {
						"componentType": "label",
						"componentName": "lblReasonCode_data",
						"field": "Return_Reason_Code",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(153, 0, 0)",
						"fontSize": "16px"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"Label335": {
					"property": {
						"componentType": "label",
						"componentName": "Label335",
						"componentValue": "EDIT QTY",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(255, 255, 255)",
						"fontSize": "18px",
						"fontWeight": "500"
					},
					"parent": "Panel489"
				},
				"Panel489850": {
					"style": {
						"verticalAlign": "middle",
						"width": "33%",
						"textAlign": "center",
						"display": "table-cell"
					},
					"parent": "Panel233",
					"property": {
						"componentType": "panel",
						"componentName": "Panel489850",
						"componentVisible": true
					}
				},
				"pnlProductBodyLeft": {
					"style": {
						"textAlign": "center",
						"width": "110px",
						"display": "table-cell",
						"verticalAlign": "top"
					},
					"parent": "pnlProduct",
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyLeft",
						"componentVisible": true
					}
				},
				"pnlProductBodyMiddle1": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle1",
						"componentVisible": true
					},
					"style": {
						"width": "100%"
					},
					"parent": "pnlProductBodyMiddle"
				},
				"lblItemCode": {
					"property": {
						"componentType": "label",
						"componentName": "lblItemCode",
						"field": "No",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(60, 60, 59)",
						"fontSize": "24px",
						"fontWeight": "bold"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"Label335785391": {
					"property": {
						"componentType": "label",
						"componentName": "Label335785391",
						"componentValue": "REMOVE",
						"componentVisible": true
					},
					"style": {
						"color": "rgb(255, 255, 255)",
						"fontSize": "18px",
						"fontWeight": "500"
					},
					"parent": "Panel489850"
				},
				"pnlProductBodyMiddle": {
					"style": {
						"display": "table-cell",
						"verticalAlign": "top",
						"width": "calc(100% - 110px)"
					},
					"parent": "pnlProduct",
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle",
						"componentVisible": true
					}
				},
				"lblDiscCO": {
					"property": {
						"componentType": "label",
						"componentName": "lblDiscCO",
						"componentValue": "Disc",
						"field": "disc",
						"componentVisible": true
					},
					"style": {
						"display": "none",
						"color": "rgb(60, 60, 59)",
						"fontSize": "16px",
						"fontWeight": "normal"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"Panel489": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel489",
						"componentVisible": true
					},
					"style": {
						"width": "33%",
						"textAlign": "center",
						"display": "table-cell",
						"verticalAlign": "middle"
					},
					"parent": "Panel233"
				},
				"Panel489504": {
					"property": {
						"componentType": "panel",
						"componentName": "Panel489504",
						"componentVisible": true
					},
					"style": {
						"verticalAlign": "middle",
						"width": "33%",
						"borderLeft": "2px solid #FFF",
						"display": "table-cell",
						"textAlign": "center",
						"borderRight": "2px solid #FFF"
					},
					"parent": "Panel233"
				},
				"Label335234361": {
					"property": {
						"componentType": "label",
						"componentName": "Label335234361",
						"componentValue": "REASON",
						"componentVisible": true
					},
					"style": {
						"fontSize": "18px",
						"fontWeight": "500",
						"color": "rgb(255, 255, 255)"
					},
					"parent": "Panel489504"
				},
				"lblPrice": {
					"style": {
						"textAlign": "left",
						"display": "block",
						"fontSize": "18px",
						"marginTop": "10px",
						"color": "rgb(0, 153, 0)",
						"fontWeight": "bold",
						"paddingTop": "2px"
					},
					"parent": "pnlProductBodyMiddle1",
					"property": {
						"componentType": "label",
						"componentName": "lblPrice",
						"componentVisible": true
					}
				},
				"Panel233": {
					"parent": "Panel17",
					"property": {
						"componentType": "panel",
						"componentName": "Panel233",
						"componentVisible": true
					},
					"style": {
						"width": "100%",
						"height": "40px",
						"backgroundColor": "rgb(71, 65, 65)",
						"borderRadius": "0px 0px 10px 10px",
						"display": "table"
					}
				}
			}
		}
	},
	"Panel960": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel960",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "50%",
			"textAlign": "center",
			"borderRight": "1px solid #FFF"
		},
		"parent": "pnlUpdateQtyFooterBtn704"
	},
	"pnlMainBodyForm2LabelRemarks767": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm2LabelRemarks767",
			"componentVisible": true
		},
		"style": {
			"textAlign": "left",
			"width": "100%"
		},
		"parent": "pnlMainBodyHeaderRemarks"
	},
	"pnlUpdateQtyContentMainInput": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentMainInput",
			"componentVisible": true
		},
		"style": {
			"minWidth": "40px",
			"minHeight": "50px"
		},
		"parent": "Panel513"
	},
	"lblMainTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle",
			"componentValue": "SALES RETURN DETAILS",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500"
		},
		"parent": "pnlMainHeaderMiddleCell"
	},
	"pnlMainBody": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		},
		"style": {
			"overflow": "auto",
			"width": "100%",
			"height": "calc(100% - 50px - 50px - 0px - 15px)"
		},
		"parent": "pnlMain"
	},
	"pnlModalCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlModalTable"
	},
	"pnlUpdateQtyContentInput": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentInput",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgb(71, 65, 65)",
			"padding": "5%",
			"paddingBottom": "20px"
		},
		"parent": "pnlUpdateQtyContentMainInput"
	},
	"Panel513530": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel513530",
			"componentVisible": true
		},
		"style": {
			"minWidth": "40px",
			"minHeight": "50px"
		},
		"parent": "pnlUpdateQtyContent277"
	},
	"lblTotalAmountExclGST237": {
		"property": {
			"componentType": "label",
			"componentName": "lblTotalAmountExclGST237",
			"componentValue": "Total Excl. GST",
			"componentVisible": true
		},
		"style": {
			"fontSize": "20px",
			"fontWeight": "bolder",
			"float": "left",
			"color": "rgb(255, 255, 255)"
		},
		"parent": "pnlMainBodyDetails1872"
	},
	"pnlMainFooterTbl361": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterTbl361",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainFooter"
	},
	"lblModalNegative": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalNegative",
			"componentValue": "NEGATIVE",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(74, 134, 237)",
			"fontSize": "20px"
		},
		"parent": "pnlModalBodyButtonNegative"
	},
	"pnlUpdateQtyContentInputQtyTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentInputQtyTable",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "40px",
			"display": "table"
		},
		"parent": "pnlUpdateQtyContentInputQty"
	},
	"imgMinus": {
		"property": {
			"componentType": "image",
			"componentName": "imgMinus",
			"componentValue": require("../../asset/minus-white.png"),
			"componentVisible": true
		},
		"style": {
			"width": "50px",
			"height": "40px",
			"border": "1px solid #FFF",
			"float": "left"
		},
		"parent": "pnlUpdateQtyContentInputQtyTableCell1"
	},
	"pnlMainHeaderMiddleTable": {
		"parent": "pnlMainHeaderMiddle",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		}
	},
	"pnlMainFooter": {
		"parent": "pnlMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooter",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "50px"
		}
	},
	"pnlCSS": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCSS",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"top": "0px",
			"zIndex": "4",
			"display": "none",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlSelectReasonCode": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlSelectReasonCode",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"zIndex": "2",
			"width": "100%",
			"height": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"top": "0px"
		},
		"parent": "pnlHolder"
	},
	"pnlMainHeaderLeftCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftCell",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlMainHeaderLeftTable"
	},
	"lblCustomerCodeName": {
		"property": {
			"componentType": "label",
			"componentName": "lblCustomerCodeName",
			"field": "code",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(255, 215, 0)",
			"fontSize": "18px",
			"fontWeight": "bold",
			"textAlign": "center"
		},
		"parent": "pnlBodyContentHeader"
	},
	"Panel401": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel401",
			"componentVisible": true
		},
		"style": {
			"padding": "10px"
		},
		"parent": "Panel513530"
	},
	"pnlUpdateQtyContentInputQtyTableCell1": {
		"parent": "pnlUpdateQtyContentInputQtyTable",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentInputQtyTableCell1",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"width": "50px",
			"height": "40px"
		}
	},
	"pnlBodyMainContent": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyMainContent",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"padding": "10px"
		},
		"parent": "pnlMainBodyHeader"
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "50%",
			"textAlign": "center",
			"display": "inline-block",
			"padding": "10px 0px",
			"float": "none"
		},
		"parent": "pnlModalBodyButtons"
	},
	"lblUpdateQtyItemNo": {
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateQtyItemNo",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(60, 60, 59)",
			"textAlign": "left",
			"display": "block",
			"fontSize": "24px",
			"fontWeight": "normal"
		},
		"parent": "pnlUpdateQtyItemNo"
	},
	"pnlUpdateQtyItemImage874": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyItemImage874",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"height": "50px",
			"display": "block",
			"width": "100%"
		},
		"parent": "pnlUpdateQtyContentHeader387"
	},
	"imgUpdateQtyClose": {
		"style": {
			"right": "7%",
			"width": "25px",
			"position": "absolute",
			"zIndex": "2",
			"height": "25px",
			"float": "right"
		},
		"parent": "Panel513",
		"property": {
			"componentType": "image",
			"componentName": "imgUpdateQtyClose",
			"componentValue": require("../../asset/icon/icon-close-gray.png"),
			"componentVisible": true
		}
	},
	"imgPlus": {
		"property": {
			"componentType": "image",
			"componentName": "imgPlus",
			"componentValue": require("../../asset/plus-white.png"),
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "50px",
			"height": "40px",
			"border": "1px solid #FFF"
		},
		"parent": "pnlUpdateQtyContentInputQtyTableCell3"
	},
	"Label285": {
		"property": {
			"componentType": "label",
			"componentName": "Label285",
			"componentValue": "Select Return Reason",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"fontWeight": "500"
		},
		"parent": "Panel401"
	},
	"pnlUpdateQtyContentHeader387": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentHeader387",
			"componentVisible": true
		},
		"style": {
			"zIndex": "1",
			"minWidth": "40px",
			"minHeight": "50px",
			"padding": "3%"
		},
		"parent": "Panel513530"
	},
	"pnlUpdateQtyFooterBtn704": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyFooterBtn704",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "40px",
			"display": "table",
			"textAlign": "center",
			"backgroundColor": "rgba(242, 34, 34, 0.95)",
			"borderRadius": "0px 0px 10px 10px"
		},
		"parent": "pnlUpdateQtyContent277"
	},
	"pnlMainBodyHeaderRemarks": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyHeaderRemarks",
			"componentVisible": true
		},
		"style": {
			"padding": "10px",
			"paddingBottom": "5px",
			"display": "none",
			"textAlign": "center",
			"paddingTop": "0px",
			"width": "100%"
		},
		"parent": "pnlMainBody"
	},
	"pnlMainBodyForm2InputValue32": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyForm2InputValue32",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyHeaderRemarks"
	},
	"pnlModalBodyMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMain",
			"componentVisible": true
		},
		"style": {
			"width": "90%",
			"textAlign": "center",
			"borderBottom": "1px solid #D9D9D9",
			"background": "#ffffff",
			"padding": "10px 0px",
			"marginLeft": "5%",
			"marginRight": "5%"
		},
		"parent": "pnlModalCell"
	},
	"lblModalPositive": {
		"parent": "pnlModalBodyButtonPositive",
		"property": {
			"componentType": "label",
			"componentName": "lblModalPositive",
			"componentValue": "POSITIVE",
			"componentVisible": true
		},
		"style": {
			"fontSize": "20px",
			"color": "rgb(74, 134, 237)"
		}
	},
	"pnlUpdateQtyTable": {
		"parent": "pnlUpdateQty",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		}
	},
	"lblUpdateQtyContentInputUOM": {
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateQtyContentInputUOM",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"fontWeight": "bold",
			"fontSize": "16px",
			"textAlign": "center"
		},
		"parent": "pnlUpdateQtyContentInputUOM"
	},
	"pnlUpdateQtyFooterBtn": {
		"style": {
			"textAlign": "center",
			"display": "block",
			"width": "100%",
			"height": "40px"
		},
		"parent": "pnlUpdateQtyContent",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyFooterBtn",
			"componentVisible": true
		}
	},
	"pnlUpdateQtyTableCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyTableCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlUpdateQtyTable"
	},
	"pnlMainHeaderRightCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRightTable"
	},
	"pnlBodyContentHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlBodyContentHeader",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlBodyMainContent"
	},
	"pnlMainBodyButtonsINV65": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyButtonsINV65",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"padding": "5px",
			"overflow": "auto",
			"width": "50%"
		},
		"parent": "pnlMainBodyButtons510"
	},
	"pnlModalBodyButtons": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtons",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "90%",
			"textAlign": "center",
			"background": "#ffffff",
			"marginLeft": "5%",
			"marginRight": "5%"
		},
		"parent": "pnlModalCell"
	},
	"pnlModalBodyMessage": {
		"parent": "pnlModalBodyMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"padding": "10px",
			"marginBottom": "10px"
		}
	},
	"pnlMainHeaderLeftTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTable",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table"
		},
		"parent": "pnlMainHeaderLeft"
	},
	"pnlUpdateQtyContent277": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContent277",
			"componentVisible": true
		},
		"style": {
			"marginRight": "5%",
			"width": "90%",
			"marginLeft": "5%",
			"background": "#ffffff",
			"borderRadius": "10px",
			"display": "block",
			"textAlign": "center"
		},
		"parent": "pnlUpdateQtyTableCell515"
	},
	"pnlMainBodyButtonsDrafft886": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyButtonsDrafft886",
			"componentVisible": true
		},
		"style": {
			"display": "inline-block",
			"overflow": "auto",
			"width": "50%",
			"padding": "5px"
		},
		"parent": "pnlMainBodyButtons510"
	},
	"pnlMainBodyDetails": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyDetails",
			"componentVisible": true
		},
		"style": {
			"padding": "10px",
			"width": "100%",
			"backgroundColor": "rgb(84, 84, 84)",
			"display": "none",
			"height": "60px"
		},
		"parent": "pnlMain"
	},
	"pnlUpdateQtyItemImage": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyItemImage",
			"componentVisible": true
		},
		"style": {
			"height": "100px",
			"position": "relative",
			"display": "block",
			"width": "100%"
		},
		"parent": "pnlUpdateQtyContentHeader"
	},
	"Label750": {
		"property": {
			"componentType": "label",
			"componentName": "Label750",
			"componentValue": "CANCEL",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"fontWeight": "500",
			"color": "rgb(255, 255, 255)"
		},
		"parent": "Panel561"
	},
	"txtRemarks709": {
		"property": {
			"componentType": "edit",
			"componentName": "txtRemarks709",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "40px",
			"border": "1px solid #000"
		},
		"parent": "pnlMainBodyForm2InputValue32"
	},
	"lblModalMessage": {
		"parent": "pnlModalBodyMessage",
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"width": "100%",
			"color": "rgb(111, 111, 110)"
		}
	},
	"pnlUpdateQtyContentInputQty": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentInputQty",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlUpdateQtyContentInput"
	},
	"pnlMainHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"backgroundColor": "rgba(242, 34, 34, 0.95)",
			"height": "65px",
			"paddingTop": "15"
		},
		"parent": "pnlMain"
	},
	"lblStyle744": {
		"property": {
			"componentType": "label",
			"componentName": "lblStyle744",
			"componentValue": "N/A",
			"componentVisible": true
		},
		"parent": "pnlCSS"
	},
	"btnUpdateQtyUpdate": {
		"style": {
			"backgroundColor": "rgba(242, 34, 34, 0.95)",
			"margin": "0px",
			"height": "40px",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"padding": "8px",
			"width": "100%",
			"fontWeight": "bold"
		},
		"parent": "pnlUpdateQtyFooterBtn",
		"property": {
			"componentType": "fbutton",
			"componentName": "btnUpdateQtyUpdate",
			"componentValue": "UPDATE",
			"componentVisible": true
		}
	},
	"Panel561": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel561",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"display": "table-cell",
			"width": "50%",
			"borderLeft": "1px solid #FFF",
			"verticalAlign": "middle"
		},
		"parent": "pnlUpdateQtyFooterBtn704"
	},
	"btnSAVE691": {
		"style": {
			"verticalAlign": "middle",
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"height": "50px",
			"fontWeight": "bold",
			"textAlign": "center",
			"backgroundColor": "rgba(242, 34, 34, 0.95)"
		},
		"parent": "pnlMainBodyButtonsDrafft886",
		"property": {
			"componentType": "fbutton",
			"componentName": "btnSAVE691",
			"componentValue": "ORDER",
			"componentVisible": true
		}
	},
	"lblDataTotalAmountExclGST363": {
		"property": {
			"componentType": "label",
			"componentName": "lblDataTotalAmountExclGST363",
			"componentValue": "$ 0.00",
			"componentVisible": true
		},
		"style": {
			"float": "right",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "bolder"
		},
		"parent": "pnlMainBodyDetails1872"
	},
	"pnlModalBodyTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBodyMain"
	},
	"txtQuantity": {
		"property": {
			"componentType": "edit",
			"componentName": "txtQuantity",
			"type": "number",
			"componentValue": "0",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(33, 33, 33)",
			"float": "left",
			"fontSize": "24px",
			"fontWeight": "normal",
			"height": "40px",
			"textAlign": "center",
			"border": "1px solid #FFF"
		},
		"parent": "pnlUpdateQtyContentInputQtyTableCell2"
	},
	"pnlHolder": {
		"style": {
			"width": "100%",
			"height": "100%"
		},
		"parent": "pgSalesReturnDetailsUpdate",
		"property": {
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		}
	},
	"pnlMainBodyHeaderCaption": {
		"style": {
			"marginBottom": "5px",
			"width": "100%",
			"backgroundColor": "rgb(135, 135, 135)"
		},
		"parent": "pnlMainBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyHeaderCaption",
			"componentVisible": true
		}
	},
	"pnlUpdateQtyTableCell515": {
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlUpdateQtyTable992753",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyTableCell515",
			"componentVisible": true
		}
	},
	"lblRemarksChar395": {
		"property": {
			"componentType": "label",
			"componentName": "lblRemarksChar395",
			"componentValue": "(Characters left: 200)",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(111, 111, 110)",
			"fontWeight": "normal",
			"display": "none",
			"fontSize": "16px",
			"textAlign": "left",
			"marginLeft": "5px"
		},
		"parent": "pnlMainBodyForm2LabelRemarks767"
	},
	"btnOrder743881": {
		"property": {
			"componentType": "fbutton",
			"componentName": "btnOrder743881",
			"componentValue": "ORDER<br>INVOICE",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"height": "50px",
			"fontWeight": "bold",
			"textAlign": "center",
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"backgroundColor": "rgba(242, 34, 34, 0.95)"
		},
		"parent": "pnlMainBodyButtonsINV65"
	},
	"lblBtnOrder72": {
		"property": {
			"componentType": "label",
			"componentName": "lblBtnOrder72",
			"componentValue": "SUBMIT",
			"componentVisible": true
		},
		"style": {
			"fontSize": "20px",
			"fontWeight": "bold",
			"display": "block",
			"color": "rgb(255, 255, 255)"
		},
		"parent": "pnlMainFooterTblCell1865"
	},
	"lblOrderDate": {
		"property": {
			"componentType": "label",
			"componentName": "lblOrderDate",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"color": "rgb(255, 255, 255)",
			"fontSize": "16px",
			"textAlign": "center",
			"fontWeight": "normal"
		},
		"parent": "pnlBodyContentHeader"
	},
	"pnlMainFooterTblCell1865": {
		"parent": "pnlMainFooterTbl361",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainFooterTblCell1865",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"textAlign": "center",
			"backgroundColor": "rgba(242, 34, 34, 0.95)",
			"display": "table-cell",
			"height": "100%"
		}
	},
	"pnlUpdateQtyContentInputUOM": {
		"style": {
			"textAlign": "left"
		},
		"parent": "pnlUpdateQtyContentInput",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentInputUOM",
			"componentVisible": true
		}
	},
	"pnlMainHeaderMiddle": {
		"style": {
			"height": "100%",
			"float": "left",
			"width": "70%"
		},
		"parent": "pnlMainHeader",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddle",
			"componentVisible": true
		}
	},
	"pnlMainBodyItemsCO2656": {
		"parent": "pnlMainBodyItems84",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyItemsCO2656",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		}
	},
	"imgUpdateQtyItemImage": {
		"property": {
			"componentType": "image",
			"componentName": "imgUpdateQtyItemImage",
			"componentValue": require("../../asset/images/default.png"),
			"componentVisible": true
		},
		"style": {
			"height": "100px",
			"width": "100px"
		},
		"parent": "pnlUpdateQtyItemImage"
	},
	"pnlUpdateQtyItemUnitPrice": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyItemUnitPrice",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginTop": "20px"
		},
		"parent": "pnlUpdateQtyContentHeader"
	},
	"lblItems509": {
		"property": {
			"componentType": "label",
			"componentName": "lblItems509",
			"componentValue": "ITEMS",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bold",
			"paddingTop": "5px",
			"display": "inline-block",
			"width": "calc(100% - 48px)",
			"fontSize": "18px",
			"color": "rgb(255, 215, 0)",
			"paddingBottom": "5px",
			"padding": "10px"
		},
		"parent": "pnlMainBodyHeaderCaption"
	},
	"pnlMainBodyItems84": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyItems84",
			"addClass": "pnlMainBodyHeader",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		},
		"parent": "pnlMainBody"
	},
	"pnlMainBodyButtons510": {
		"parent": "pnlMainBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyButtons510",
			"componentVisible": true
		},
		"style": {
			"display": "none",
			"width": "100%"
		}
	},
	"lblModalTitle": {
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"color": "rgb(60, 60, 59)",
			"fontSize": "18px",
			"fontWeight": "bold"
		},
		"parent": "pnlModalBodyTitle"
	},
	"pnlUpdateQtyContent": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContent",
			"componentVisible": true
		},
		"style": {
			"marginLeft": "5%",
			"display": "block",
			"width": "90%",
			"textAlign": "center",
			"background": "#ffffff",
			"marginRight": "5%"
		},
		"parent": "pnlUpdateQtyTableCell"
	},
	"pnlUpdateQtyContentInputQtyTableCell2": {
		"style": {
			"display": "table-cell",
			"width": "calc(100% - 120px)",
			"height": "40px"
		},
		"parent": "pnlUpdateQtyContentInputQtyTable",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentInputQtyTableCell2",
			"componentVisible": true
		}
	},
	"pnlUpdateQtyTable992753": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyTable992753",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlSelectReasonCode"
	},
	"imgModalLoading": {
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		},
		"style": {
			"height": "10px",
			"width": "50px"
		},
		"parent": "pnlModalBodyLoading"
	},
	"pnlUpdateQtyItemNo": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyItemNo",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"position": "relative"
		},
		"parent": "pnlUpdateQtyContentHeader"
	},
	"pnlUpdateQtyItemName": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyItemName",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"width": "100%"
		},
		"parent": "pnlUpdateQtyContentHeader"
	},
	"lblRemarks706": {
		"style": {
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"fontWeight": "normal",
			"textAlign": "left"
		},
		"parent": "pnlMainBodyForm2LabelRemarks767",
		"property": {
			"componentType": "label",
			"componentName": "lblRemarks706",
			"componentValue": "Remarks",
			"componentVisible": true
		}
	},
	"Image553564": {
		"property": {
			"componentType": "image",
			"componentName": "Image553564",
			"componentValue": require("../../asset/plus-white.png"),
			"zoom": false,
			"componentVisible": true
		},
		"style": {
			"width": "40px"
		},
		"parent": "pnlMainBodyHeaderCaption"
	},
	"pnlModalBodyButtonPositive": {
		"parent": "pnlModalBodyButtons",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"padding": "10px 0px",
			"display": "inline-block",
			"float": "none",
			"width": "50%",
			"textAlign": "center"
		}
	},
	"pnlMain": {
		"parent": "pnlHolder",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"zIndex": "0",
			"width": "100%",
			"height": "100%"
		}
	},
	"pnlMainBodyHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyHeader",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgb(84, 84, 84)",
			"width": "100%"
		},
		"parent": "pnlMainBody"
	},
	"pnlUpdateQty": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQty",
			"componentVisible": false
		},
		"style": {
			"position": "fixed",
			"zIndex": "2",
			"height": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"top": "0px",
			"width": "100%"
		},
		"parent": "pnlHolder"
	},
	"lblUpdateQtyItemName": {
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateQtyItemName",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"fontSize": "16px",
			"fontWeight": "normal",
			"color": "rgb(60, 60, 59)",
			"textAlign": "left"
		},
		"parent": "pnlUpdateQtyItemName"
	},
	"lblUpdateQtyItemUnitPrice": {
		"style": {
			"color": "rgb(0, 153, 0)",
			"fontWeight": "normal",
			"textAlign": "left",
			"display": "block",
			"fontSize": "20px"
		},
		"parent": "pnlUpdateQtyItemUnitPrice",
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateQtyItemUnitPrice",
			"componentVisible": true
		}
	},
	"cmbReasonCode": {
		"property": {
			"componentType": "combobox",
			"componentName": "cmbReasonCode",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px"
		},
		"parent": "pnlUpdateQtyItemImage874"
	},
	"imgBack": {
		"property": {
			"componentType": "image",
			"componentName": "imgBack",
			"componentValue": require("../../asset/icon/icon-back.png"),
			"componentVisible": true
		},
		"style": {
			"width": "35px",
			"height": "35px"
		},
		"parent": "pnlMainHeaderLeftCell"
	},
	"pnlMainHeaderMiddleCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleCell",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddleTable"
	},
	"pnlMainHeaderRightTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRight"
	},
	"Panel513": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel513",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "2%",
			"minWidth": "40px",
			"minHeight": "50px"
		},
		"parent": "pnlUpdateQtyContent"
	},
	"pnlUpdateQtyContentHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateQtyContentHeader",
			"componentVisible": true
		},
		"style": {
			"padding": "3%",
			"zIndex": "1",
			"minWidth": "40px",
			"minHeight": "50px"
		},
		"parent": "Panel513"
	},
	"pnlModalTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table"
		},
		"parent": "pnlModal"
	},
	"pnlModalBodyLoading": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%"
		},
		"parent": "pnlModalBodyMain"
	}
};
