export const StateName = 'MainMenuPage';
export const State = {
    page:{
        style:{
            flex: 1,
            position: 'relative',
            backgroundColor:'rgb(245, 245, 245)',
        }
    },
    pnlHolder: {
        style: {
            width:'100%',
            height:'100%',
            overflow: 'auto',
        },
        componentType: 'panel',
    },
    pnlMain: {
        style: {
            width:'100%',
            height:'100%',
            zIndex: 0
        },
        componentType: 'panel',
    },
    pnlMainHeader: {
        style:{
            width:'100%',
            height:'65px',
            //flexDirection: 'row', 
            textAlign: 'center',
            backgroundColor: 'rgb(255, 69, 0)',
            paddingTop: '15px',
        },
        componentType: 'panel',
    },
    pnlMainHeaderLeft: {
        style:{
            float:'left',
            width: '100px',
            height: '100%',
            // backgroundColor:'pink'
        },
        componentType: 'panel',
        parent: 'pnlMainHeader'
    },
    pnlMainHeaderLeftTable :{
        style:{
            height: '100%',
            display: 'table',
            width: '100%',
        },
        componentType: 'panel',
    },
    pnlMainHeaderLeftCell:{
        style:{
            display: 'table-cell',
            verticalAlign: 'middle',
            width: '100%',
            height: '100%',
            backgroundColor: 'green'
        },
        componentType: 'panel',
        parent: 'pnlMainHeaderLeftTable'
    },
    imgBack:{
        style:{
            width:'35px',
            height:'35px',
        },
        property: {
            componentValue: {uri: 'https://i.imgur.com/P2A4PB6.jpg'},
        },
        componentType: 'image',
    },
    pnlMainHeaderMiddle:{
        style:{
            float:'left',
            width:'120px',// width:'calc(100% - 100px - 100px)',
            height:'100%',
            backgroundColor: 'white',
        },
        componentType: 'panel',
        parent: 'pnlMainHeader'
    },
    pnlMainHeaderMiddleTable:{
        style:{
            display:'table',
            width:'100%',
            height:'100%',
        },
        componentType: 'panel',
    },
    pnlMainHeaderMiddleCell:{
        style:{
            height:'100%',
            display: 'table-cell',
            verticalAlign: 'middle',
            width: '100%',
        },
        componentType: 'panel',
        parent: 'pnlMainHeaderMiddleTable'
    },
    lblTitle:{
        style:{
            width:'100%',
            color:'black',
            fontSize:'18px',
            fontWeight:'500'
        },
        property: {
			componentValue: 'lblTitle',
		},
        componentType: 'label',
    },
    lblLoginAs:{
        style:{
            width: '100%',
            color:'black',
            fontSize:'12px',
        },
        property: {
			componentValue: 'lblLoginAs',
		},
        componentType: 'label',
    },
    pnlMainHeaderRight:{
        style:{
            height:'100%',
            float: 'left',
            width: '100px',
            // backgroundColor:'green'

        },
        componentType: 'panel',
        parent: 'pnlMainHeader'
    },
    pnlMainHeaderRightTable:{
        style:{
            display:'table',
            width:'100%',
            height:'100%',
        },
        componentType: 'panel',
    },
    pnlMainHeaderRightCell:{
        style:{
            display: 'table-cell',
            verticalAlign: 'middle',
            width: '100%',
            height: '100%',
            textAlign: 'right',
            paddingRight: '10px',
        },
        componentType: 'panel',
        parent: 'pnlMainHeaderRightTable'
    },
    lblAll:{
        style:{
            fontSize: '20px',
            display: 'none',
            fontWeight:'500',
            width:'100%',
            color:'black'
        },
        property: {
			componentValue: 'LOGOUT',
		},
        componentType: 'label',
    },
    Image85:{
        style:{
            width:'35px',
            height:'35px',
            minHeight:'40p'
        },
        property: {
            componentValue: {uri: 'https://i.imgur.com/P2A4PB6.jpg'},
        },
        componentType: 'image',
    },
    pnlMainBodySales:{
        style:{
            verticalAlign: 'middle',
            width: '100%',
            textAlign: 'center',// not centering contents in child components (e.g Customer, Item Catalog)
            overflow: 'auto',
            // height: 400,// height: 'calc(100% - 65px)'
            backgroundColor: 'yellow',
        },
    },
    cmbRoute: {
        style: {
            position: 'relative',
            display: 'none', // component still takes up space
            width: '90%',
            fontSize:'16px',
            marginLeft: '20px',
            height: '40px',
            border:'1px solid #D9D9D9'
        },
        property: {
            componentValue: '',
            componentName:"cmbRoute",
        },
        componentType: 'comboBox',
        options: [
            // {
            //     field:"Java",
            //     value:"java"
            // },
            // {
            //     field:"Golang",
            //     value:"go"
            // },
        ]
    },
    lblMainCustName: {
        style:{
            color:'rgb(255, 255, 255)',
            display: 'block',
            fontSize: '18px',
            textAlign: 'center',
        },
        property: {
			componentValue: 'lblMainCustName',
		},
        componentType: 'label',
    },
    lblMainCustAddress:{
        style:{
            paddingBottom:'5px',
            fontSize:'14px',
            textAlign:'center',
            display: 'block',
            color:'rgb(255, 255, 255)',
        },
        property: {
			componentValue: 'lblMainCustAddress',
		},
        componentType: 'label',
    },
    pnlMainBodyTitleSalesOrder:{
        style:{
            display:'none',
            overflow:'auto',
            background: '#CCCCCC',
        },
        componentType: 'panel',
    },
    lblPONumber383:{
        style:{
            fontSize:'16px',
            textAlign:'center',
            width:'20%',
            color:'white',
            fontWeight:'normal',
            display:'none',
        },
        property: {
			componentValue: 'lblPONumber383',
		},
        componentType: 'label',
    },
    Label559:{
        style:{
            width:'100%',
            fontSize:'larger',
            fontWeight: 'bold',
            backgroundColor: 'rgba(185, 185, 185, 0.97)',
        },
        property: {
			componentValue: 'Label559',
		},
        componentType: 'label',
    },
    pnlMainBodyCombo:{
        style:{
            width:'100%',
            backgroundColor:'rgb(71, 65, 65)',
        },
        componentType: 'panel',
    },
    lblMainCustCode: {
        style:{
            fontWeight:'bold',
            textAlign:'center',
            paddingTop:'5px',
            color:'white',
            display: 'block',
            fontSize: '20px',
        },
        property: {
			componentValue: 'lblMainCustCode',
		},
        componentType: 'label',
    },
    pnlMainBody1: {
        style:{
            overflow:'auto',
            borderBottom:'0px solid #878786',
            padding: '5px 5px'
        },
        property: {
            componentName: 'pnlMainBody1'
        },
        componentType: 'panel',
    },
    pnlMainBody1Left: {
        style:{
            float:'left',
            width:'50%',
            padding:'5px',
            // alignItems: 'center',
            backgroundColor: 'pink',
        },
        property: {
            componentName: "pnlMainBody1Left"  
        },
        componentType: 'panel',
        parent: 'pnlMainBody1'
    },
    pnlMainBody1Middle: {
        style:{
            float:'left',
            width:'50%',
            padding:'5px',
        },
        componentType: 'panel',
        parent: 'pnlMainBody1'
    },
    lblHistory: {
        style: {
            color:'rgb(60, 60, 59)',
            fontSize:'18px',
            width:'100%',
        },
        property: {
			componentValue: 'Customer',
		},
        componentType: 'label',
    },
    imgHistory: {
        style:{
            marginBottom:'10px',
            width:'70px',
        },
        property: {
            componentValue: {uri: 'https://i.imgur.com/P2A4PB6.jpg'},
            componentName: "imgHistory",
        },
        componentType: 'image',
    },
    imgHistory65501: {
        style:{
            width:'70px',
            marginBottom: '10px',
        },
        property: {
            componentValue: {uri: 'https://i.imgur.com/P2A4PB6.jpg'},
            componentName: "imgHistory65501",
        },
        componentType: 'image',
    },
    lblSettings: {
        style:{
            width:'100%',
            color:'rgb(60, 60, 59)',
            fontSize:'18px'
        },
        property: {
            componentValue: 'Item Catalog',
        },
        componentType: 'label',
    },
    pnlMainBody2: {
        style:{
            overflow: 'auto',
            borderBottom:'0px solid #878786',
            padding: '5px 5px',
            backgroundColor: 'orange'
        },
        componentType: 'panel',
    },
    
    
    //modal
    pnlModal: {
        style:{
            width:'100%',
            height:'100%',
            background:'rgba(0, 0, 0, 0.60)',
            position:'fixed', // fixed casues error
            top:'0px',
            zIndex:1
        },
        componentType: 'panel'
    },
    pnlModalTable: {
        style:{
            display:'table',
            width:'100%',
            height:'100%',
            backgroundColor: 'yellow'
        },
        componentType: 'panel'
    },
    pnlModalCell: {
        style:{
            display:'table-cell', // this is what centers the current modal child components
            verticalAlign: 'middle', // doesn work, justifyContent: 'center', also doesn't work
            width:'100%',
            height:'100%',
        },
        componentType: 'panel',
        parent: 'pnlModalTable'
    },
    pnlModalBodyMain: {
        style:{
            marginRight:'5%',
            textAlign: 'center',
            borderBottom: '1px solid #D9D9D9',
            padding: '10px 0px',
            marginLeft: '5%',
            width: '90%',
            // background: '#ffffff',
            backgroundColor: 'white',
        },
        componentType: 'panel'
    },
    pnlModalBodyTitle: {
        style:{
            width:'100%',
            marginBottom:'10px',
            backgroundColor: 'pink',
        },
        componentType: 'panel'
    },
    pnlModalBodyMessage: {
        style:{
            marginBottom: '10px',
            width: '100%',
            backgroundColor: 'springgreen',
        },
        componentType: 'panel'
    },
    pnlModalBodyLoading: {
        style:{ 
            width:'100%'
        },
        componentType: 'panel'
    },
    lblModalTitle: {
        style:{
            color:'rgb(60, 60, 59)',
            fontSize:'18px',
            fontWeight:'bold',
            width: '100%',
        },
        property: {
            componentValue: 'Loading',
        },
        componentType: 'label'
    },
    lblModalMessage: {
        style:{
            color:'rgb(111, 111, 110)',
            fontSize: '16px',
            width:'100%',
        },
        property: {
            componentValue: 'Loading ...',
        },
        componentType: 'label'
    },
    imgModalLoading: {
        style:{
            width:'50px',
            height:'10px'
        },
        property: {
            componentValue: {uri: 'https://i.imgur.com/P2A4PB6.jpg'},
        },
        componentType: 'image'
    },
    pnlModalBodyButtons: {
        style: {
            // flexDirection:'row', // added
            textAlign:'center',
            width:'90%',
            marginLeft:'5%',
            marginRight:'5%',
            // background is shorthand to combine many background tags into one line. background: #ffffff url("img_tree.png") no-repeat right top;
            // background:'#ffffff' 
            backgroundColor: 'papayawhip'
        },
        componentType: 'panel',
        property: {
            componentName: "pnlModalBodyButtons"
        }
    },
    pnlModalBodyButtonPositive: {
        style:{
            float:'none',
            display: 'inline-block', // not working in RN, so do flexDirection:'row' in parent instead
            width: '50%',
            textAlign: 'center',
            padding: '10px 0px'
        },
        componentType: 'panel',
        parent: 'pnlModalBodyButtons'

    },
    pnlModalBodyButtonNegative: {
        style:{
            float:'none',
            display:'inline-block', // not working in RN, so do flexDirection:'row' in parent instead
            width:'50%',
            textAlign:'center',
            padding: '10px 0px',
        },
        componentType: 'panel',
        parent: 'pnlModalBodyButtons'

    },
    lblModalPositive: {
        style:{
            color:'rgb(74, 134, 237)',
            fontSize: '20px',
        }, 
        property: {
            componentValue: 'POSITIVE',
        },
        componentType: 'label'
    },
    lblModalNegative: {
        style:{
            color:'rgb(74, 134, 237)',
            fontSize: '20px'
        },
        property: {
            componentValue: 'NEGATIVE',
        },
        componentType: 'label'
    }
}
