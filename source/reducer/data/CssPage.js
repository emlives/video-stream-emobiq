export const StateName = 'CssPage';
export const State = {
    page: {
        style:{
            color:"black"  
        }
    },
    Label579: {
        style: {
            left: "39.85%",
            width: 100.00,
            height: 30.00,
            position: "absolute",
            top: "3.28%",
            //padding: "10px 10px 10px 10px",
            //margin: "10px 10px 10px 10px",
        },
        property: {
            componentType: 'label',
            componentName: 'Label579',
            componentUUID: '1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed',
            componentValue: 'Caption'
        },
        attribute: {
            addClass: "addedClass",
            caption: "Caption",
            field: "field",
            hide: false,
            idField: "idField",
            idPrefix: "idPrefix"
        },
        elementAttribute: {
            
        }
    },
    Button682: {
        style: {
            left: 0,
            width: "50%",
            position: "relative",
            top: "0",
            padding: "10px 10px 10px 10px",
            margin: "10px 10px 10px 10px",
        },
        property: {
            componentValue: 'SetComboOptions',
            componentVisible: true,
            componentType: 'button',
            componentUUID: 'Button682',
            componentName: 'Button682'
        },
        attribute: {
            "caption": "SetComboOptions",
            "position": "absolute",
            "top": "17%",
            "left": "0",
            "width": "50%",
            "fontSize": "medium",
            "fontStyle": "italic",
            "fontVariant": "small-caps",
        },
        elementAttribute: {
            
        }
    },
    Button854: {
        style: {
            top: "30%",
            left: "52%",
            width: 135.20,
            height: 38.52,
            position: "relative",
        },
        property: {
            componentValue: 'Second Button',
            componentType: 'button',
            componentUUID: 'Button854',
            componentName: 'Button854'
        },
        attribute: {
            "caption": "Second Button",
            "position": "absolute",
            "overflowWrap": "break-word",
            "top": "30%",
            "left": "52%",
            "width": "135.20px",
            "height": "38.52px",
        },
        elementAttribute: {
            
        }
    },
    Edit219: {
        style: {
            top: "30%",
            left: "5%",
            position: "absolute",
            width: 134.00,
            height: 27.00,
            // margin: "10px",
            // padding: "10px"
        },
        property: {
            componentValue: '',
            componentName: 'Edit219',
            componentType: 'edit',
            componentUUID: 'Edit219'
        },
    },
    ComboBox520: {
        style: {
            height: 39.00,
            position: "absolute",
            top: "17%",
            left: "55%",
            width: "45%",
            padding: "10px",
            margin: "10px",
        },
        property: {
            componentValue: '',
            componentName:"ComboBox520",
            componentUUID:"ComboBox520",
            componentOptions: [{"label": "XXRAJ174303023", "value": "B0:91:22:EF:1C:A2"}, 
            {"label": "QLN320A3", "value": "AC:3F:A4:0C:6F:BF"}],
            componentType: 'comboBox',
        },
    },
    DataList467: {
        property: {
            autoLoad:false,
            componentValue:"l_data_list",
            componentName:"DataList467",
            componentUUID:"DataList467",
            componentData: [
                {
                    Label439:{
                        style: {
                        },
                        property: {
                            dataListField:'value',
                            componentType: 'label',
                            componentName: 'Label439',
                            componentUUID: '2',
                            componentValue: 'Hello'
                        },
                        attribute: {
                            caption: "Hello",
                            field: "value",
                            left: "",
                            position: "",
                            top: "",
                        },
                        elementAttribute: {
                            
                        }
                    },
                },
                {
                    Label439:{
                        style: {
                        },
                        property: {
                            dataListField:'value',
                            componentType: 'label',
                            componentName: 'Label439',
                            componentUUID: '3',
                            componentValue: "World"
                        },
                        attribute: {
                            caption: "World",
                            field: "value",
                            left: "",
                            position: "",
                            top: "",
                        },
                        elementAttribute: {
                            
                        }
                    },
                }
            ],
            componentType: 'dataList',
        },
        style: {
            position: "absolute",
            top: "45%",
            left: "45%",
            width: 158.00,
            height: 100.00,  
            fontSize: "medium",
            fontStyle: "italic",
            fontVariant: "small-caps",
            fontWeight: "bold",
            textDecorationLine: "underline",
            textDecorationStyle: "solid",
            textDecorationColor: "red",
            textShadow: "2px 2px 3px red",
        },
        template: {
            componentDataItemState: {
                Label439:{
                    style: {
                        width: '100%',
                        color: 'rgb(232, 38, 38)',
                        textAlign: 'center',
                    },
                    property: {
                        componentValue: '',
                        field: 'value',
                        componentType: 'label',
                    },
                },
            }  
        },
    },
    Panel518: {
        property: {
            componentUUID: "Panel518",
            componentType: "panel"
        },
        style: {
            position: "absolute",
            top: "71.71%",
            left: "5.70%",
            width: 277.00,
            height: 100.00,
            borderStyle: "solid",
            borderWidth: 1,
            borderColor: "#AAAAAA",
            margin: "10px",
            padding: "10px"
        }
    },
    Panel583: {
        property: {
            componentUUID: "Panel583",
            componentType: "panel"
        },
        style: {
            //position: "absolute",
            //top: "120%",
            left: 10,
            width: 292.00,
            height: 200.00,
            borderStyle: "solid",
            borderWidth: 1,
            borderColor: "#AAAAAA",
            flexDirection: "row",
            color: "blue",
            boxShadow: "3px 3px 3px 6px black",
            filter: "blur(10px)",
        }
    },
    Label870: {
        style: {
            position: "relative",
            top: 0,
            left: 0,
            float: "right"
        },
        property: {
            componentName: 'Label870',
            componentType: 'label',
            componentUUID: '4',
            componentValue: "Label"
        },
        attribute: {
            "caption": "Label",
            "position": "relative",

        },
        elementAttribute: {
            
        }
    },
    Label871: {
        style: {
            position: "relative",
        },
        property: {
            componentType: 'label',
            componentUUID: '5',
            componentValue: "Label",
            componentName: "Label871"
        },
        attribute: {
            "caption": "Label",
            "position": "relative",
            "top": "0",
            "left": "0",
        },
        elementAttribute: {
            
        }
    },
    Label872: {
        style: {
        position: "relative",
        },
        property: {
            componentType: 'label',
            componentUUID: 'Label872',
            componentValue: "Label",
            componentName: "Label872"
        },
        attribute: {
            "caption": "Label",
            "position": "relative",
            "top": "0",
            "left": "0",
            "flexGrow": "initial",
        },
        elementAttribute: {
            
        }
    },
    Image387: {
        style: {
            position: "absolute",
            top: "16.05%",
            left: "3.18%",
            width: 63.00,
            height: 71.00,
        },
        property: {
            //componentValue: require('../../framework/core/asset/default.png'),
            componentValue: "icon/_add.png",
            componentVisible: true,
            componentType: 'image',
            componentUUID: 'Image387'
        },
    },
    Signature551: {
        property: {
            componentName:"Signature551",
            componentUUID:"Signature551",
            componentType: 'signature',
        },
        style: {
            position: "absolute",
            top: "0.38%",
            left: "30.01%",
            width: 111.00,
            height: 99.00,
        },
    },
    Memo768: {
        style: {
            position: "absolute",
            top: 20.05,
            left: 203.05,
            width: 60.00,
            height: 64.00,
        },
        property: {
            componentValue: 'Drink Water',
            componentType: 'memo',
            componentUUID: 'Memo768',
        },
    },
    Checkbox505: {
        style: {
            position: "absolute",
            top: "5.70%",
            left: "13.61%",
        },
        property: {
            componentValue: true,
            componentName: 'Checkbox505',
            componentType: 'checkBox',
            componentUUID: 'Checkbox505'
        },
    },
    Label403: {
        style: {
            left: "43.03%",
            position: "absolute",
            top: "110%"
        },
        property: {
            componentVisible: true,
            componentType: 'label',
            componentUUID: 'Label403',
            componentValue: "Label",
            componentName: "Label403"
        },
        attribute: {
            "caption": "IN FRONT",
            "position": "absolute",
            "top": "110%",
            "left": "43.04%",
            "zIndex": "initial",
        },
        elementAttribute: {
            
        }
    },
    Button774: {
        style: {
            top: "120%",
            left: "43.03%",
            position: "relative",
            opacity: 1
        },
        property: {
            componentValue: 'IN FRONT',
            componentType: 'button',
            componentUUID: 'Button774',
            componentName: 'Button774'
        },
        attribute: {
            "caption": "IN FRONT",
            "position": "absolute",
            "top": "110%",
            "left": "43.04%",
            "opacity": "1",
        },
        elementAttribute: {
            
        }
    },
    snippetA: {
        style: {
            width: '100%',
            top: "120%",
            position: "absolute"
        },
        property: {
            componentType: 'snippet',
            componentValue: 'sampleSnippet',
            componentUUID: 'snippetA',
            componentData: {
                btnSnippet1: {
                    style: {
                        width: '100%',
                        textAlign: 'center',
                    },
                    property: {
                        componentType: 'button',
                        componentValue: 'Sample Button Snippet 1',
                        componentUUID: 'btnSnippet1',
                        componentName: 'btnSnippet1'
                    }
                },
                btnSnippet2: {
                    style: {
                        width: '100%',
                        textAlign: 'center',
                    },
                    property: {
                        componentType: 'button',
                        componentValue: 'Sample Button Snippet 2',
                        componentUUID: 'btnSnippet2',
                        componentName: 'btnSnippet2'
                    }
                },
                snippetAChild1: {
                    style: {
                        width: '100%',
                        backgroundColor: 'yellow',
                    },
                    property: {
                        componentType: 'snippet',
                        componentValue: 'secondSnippet',
                        componentUUID: 'snippetAChild1',
                        componentData: {
                            btnSecond1: {
                                style: {
                                    width: '100%',
                                    textAlign: 'center',
                                },
                                property: {
                                    componentType: 'button',
                                    componentValue: 'Child Button 1',
                                    componentUUID: 'btnSecond1',
                                    componentName: 'btnSecond1'
                                },

                            },
                            btnSecond2: {
                                style: {
                                    width: '100%',
                                    textAlign: 'center',
                                },
                                property: {
                                    componentType: 'button',
                                    componentValue: 'Child Button 2',
                                    componentUUID: 'btnSecond2',
                                    componentName: 'btnSecond2'
                                }
                            }
                        }
                    }
                }
            }
        },
    },
    _hiddenComponentKey:[
    ],
}

// Notes
// POSITION + DISPLAY
// - Position only supports absolute and relative
// - there is no float property
// - react only supports display flex and none, ref: https://stackoverflow.com/questions/34624100/simulate-display-inline-in-react-native#:~:text=React%20Native%20doesn't%20support,m%20flustered%20with%20inline%20text.
//   by default all elements use the behavior of display: flex
// - Top, Right, Bottom, Left doesn't support "initial"
// - zIndex, alignItems don't support auto, initial, and inherit
// - couldn't find anything about vertical-align, currently assume it doesn't work on React Native
// - flexDirection, flexWrap, justifyContent, alignContent, alignItems, flexGrow, flexShrink don't support initial and inherit
// - boxShadow doesn't work in android
// - fontStyle doesn't support oblique
// - fontWeight doesn't support bolder and lighter
// - textDecorationLine doesn't support overline
// - doesn't support writing
