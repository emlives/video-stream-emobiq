export const StateName = 'SamplePage';
export const State = {
    image1: {
        style: {
            paddingTop: '50%',
            height: '30%'
        },
        property: {
            componentValue: { uri: '../../framework/core/asset/default.png' },
            componentVisible: true,
            componentType: 'image',
        },
    },
    lblFirst: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentUUID: '1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed',
            componentName: 'lblFirst',
            componentValue: 'Sample Page',
            componentVisible: true,
            componentType: 'label',
        },
    },
    lblSecond: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentUUID: '1b9d6bcd-bbfd-4b2d-12ac-ab8dfbbd4bed',
            componentName: 'lblSecond',
            componentID: 'label_2',
            componentType: 'label',
            componentValue: 'NAME: ORANGEKLOUD',
            componentVisible: true,
        },
    },
    txtMemo: {
        property: {
            componentUUID: '1b9d6bcd-bbfd-4b2d-1b3d-ab8dfbbd4bed',
            componentType: 'memo',
            componentName: 'txtMemo',
            componentValue: 'test'
        }
    },
    lblThird: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'label',
            componentValue: 'User Defined',
            componentVisible: true,
        },
    },
    btnCaption: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Caption',
            componentVisible: true,
        },
    },
    btnConsole: {
        style: {
            position: "absolute",
            top: "17.10%",
            left: "0.00%",
        },
        property: {
            componentType: 'button',
            componentValue: 'Console',
        },
    },
    btnSelectBy: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Select By',
        }
    },
    btnUpdateBy: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Update By',
        }
    },
    btnCapture: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Capture',
        },
    },
    btnChangeSnippet: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Change Used Snippet',
        },
    },
    btnClearData: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Clear Data',
        },
    },
    btnDataFromString: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentName: 'btnDataFromString',
            componentType: 'button',
            componentValue: 'Data From String',
        },
    },
    btnSelectAll: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Select All',
        },
    },
    btnClearData: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Clear Data',
        },
    },
    btnLoadData: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Load Data',
        },
    },
    btnNextPage: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Next page',
        },
    },
    btnDeleteBy: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Delete By',
        },
    },
    btnInsert: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Insert',
        },
    },
    btnSaveSign: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {

            componentValue: 'Save Signature',
        },
    },
    btnComboOptions: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Set Combo Options',
        },
    },
    btnGotoPage: {
        property: {
            componentType: 'button',
            componentValue: 'Goto App Page',
        }
    },
    btnSetComponent: {
        property: {
            componentType: 'button',
            componentValue: 'Set Component Value',
        }
    },
    input1: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentUUID: '2b5d3bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed',
            componentType: 'edit',
            componentName: 'input1',
            componentValue: '',
        },
    },
    memo1: {
        style: {
            width: 218,
            height: 143,
        },
        property: {
            componentType: 'memo',
            componentName: 'memo1',
            componentValue: '',
        },
    },
    checkbox1: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'checkbox1',
            componentName: 'checkbox1',
            componentValue: true,
        },
    },
    comboBox1: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'comboBox',
            componentName: 'comboBox1',
            componentValue: '',
            componentOptions: [
                {
                    label: 'Java',
                    value: 'java'
                },
                {
                    label: 'Golang',
                    value: 'go'
                },
            ]
        },
    },
    snippetA: {
        style: {
            width: '100%'
        },
        property: {
            componentType: 'snippet',
            componentValue: 'sampleSnippet',
            componentUUID: 'snippetA',
            componentData: {
                btnSnippet1: {
                    style: {
                        width: '100%',
                        textAlign: 'center',
                    },
                    property: {
                        componentType: 'button',
                        componentValue: 'Sample Button Snippet 1',
                        componentUUID: 'btnSnippet1',
                    }
                },
                btnSnippet2: {
                    style: {
                        width: '100%',
                        textAlign: 'center',
                    },
                    property: {
                        componentType: 'button',
                        componentValue: 'Sample Button Snippet 2',
                        componentUUID: 'btnSnippet2',
                    }
                },
                snippetAChild1: {
                    style: {
                        width: '100%',
                        backgroundColor: 'yellow',
                    },
                    property: {
                        componentType: 'snippet',
                        componentValue: 'secondSnippet',
                        componentUUID: 'snippetAChild1',
                        componentData: {
                            btnSecond1: {
                                style: {
                                    width: '100%',
                                    textAlign: 'center',
                                },
                                property: {
                                    componentType: 'button',
                                    componentValue: 'Child Button 1',
                                    componentUUID: 'btnSecond1',
                                },

                            },
                            btnSecond2: {
                                style: {
                                    width: '100%',
                                    textAlign: 'center',
                                },
                                property: {
                                    componentType: 'button',
                                    componentValue: 'Child Button 2',
                                    componentUUID: 'btnSecond2',
                                }
                            }
                        }
                    }
                }
            }
        },
    },
    dataList1: {
        property: {
            componentUUID: '2b5d3bcd-bbfd-4b2d-9b5d-ac1dfbbd6bed',
            autoLoad: false,
            componentType: 'dataList',
            componentName: 'dataList1',
            componentValue: 'l_data_list',
            componentData: []
        },
        template: {
            componentDataItemState: {
                labelKey: {
                    style: {
                        width: '100%',
                        color: 'rgb(111, 38, 38)',
                        textAlign: 'center',
                    },
                    property: {
                        componentType: 'label',
                        componentName: 'labelKey',
                        componentValue: '',
                        // field: 'key',
                        idPrefix: 'id_',
                        idField: 'value'
                    },
                },
                labelValue: {
                    style: {
                        width: '100%',
                        color: 'rgb(232, 38, 38)',
                        textAlign: 'center',
                    },
                    property: {
                        componentType: 'label',
                        componentName: 'labelValue',
                        componentValue: '',
                        field: 'value',
                    }
                },
                buttonValue: {
                    style: {
                        width: '100%',
                        color: 'rgb(232, 38, 38)',
                        textAlign: 'center',
                    },
                    property: {
                        componentType: 'button',
                        componentName: 'buttonValue',
                        componentValue: 'Datalist - Click me!',
                    }
                }
            },
        }
    },
    signature1: {
        property: {
            componentType: 'signature',
            componentName: "signature1",
        },
        style: {
            width: '100%',
            height: 100,
            backgroundColor: 'white'
        },
    },
    lblSnippetInformation: {
        style: {
            width: '100%',
            color: 'rgb(232, 38, 38)',
            textAlign: 'center',
        },
        property: {
            componentValue: 'Snippet Below:',
            componentVisible: true,
            componentType: 'label',
        },
    },
    btnLoadNext: {
        style: {
            width: '100%',
            textAlign: 'center',
        },
        property: {
            componentType: 'button',
            componentValue: 'Load Next',
        },
    },
    _snippetStack: {},
}
