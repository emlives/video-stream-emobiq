export const StateName = 'pgOrderItem';
export const State = {
	"pnlUpdateCartModalBodyButtonItemListing": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyButtonItemListing",
			"componentVisible": true
		},
		"style": {
			"height": "calc( 100% - 310px )",
			"textAlign": "center",
			"backgroundColor": "rgb(255, 69, 0)",
			"display": "block",
			"width": "100%"
		},
		"parent": "pnlUpdateCartModalBodyItemListing"
	},
	"pnlModalBodyTitle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyTitle",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBody"
	},
	"imgModalLoading": {
		"style": {
			"width": "50px",
			"height": "10px"
		},
		"parent": "pnlModalBodyLoading",
		"property": {
			"componentType": "image",
			"componentName": "imgModalLoading",
			"componentValue": require("../../asset/images/loading.gif"),
			"componentVisible": true
		}
	},
	"pnlMainBodySearch290": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodySearch290",
			"componentVisible": true
		},
		"style": {
			"overflow": "auto",
			"width": "100%",
			"padding": "5px"
		},
		"parent": "pnlMainBody165"
	},
	"lblUpdateCartItemNameValueIL": {
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateCartItemNameValueIL",
			"field": "ItemName",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"textAlign": "left",
			"color": "rgb(60, 60, 59)",
			"fontSize": "18px",
			"fontWeight": "normal"
		},
		"parent": "pnlUpdateCartModalBodyItemListingCode"
	},
	"imgUpdateCartClose133": {
		"property": {
			"componentType": "image",
			"componentName": "imgUpdateCartClose133",
			"componentValue": require("../../asset/icon/icon-close-gray.png"),
			"componentVisible": true
		},
		"style": {
			"top": "5px",
			"right": "5px",
			"width": "25px",
			"height": "25px",
			"position": "absolute"
		},
		"parent": "pnlUpdateCartModalBodyItemListingImage"
	},
	"btnModalNegative": {
		"property": {
			"componentType": "fbutton",
			"componentName": "btnModalNegative",
			"componentValue": "CANCEL",
			"ColoringStyle": "alert",
			"componentVisible": true
		},
		"style": {
			"width": "80%",
			"fontWeight": "bold",
			"margin": "0px"
		},
		"parent": "pnlModalBodyButtonNegative"
	},
	"pnlMainHeaderMiddleTable107": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleTable107",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddle21"
	},
	"Panel186": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel186",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"textAlign": "center"
		},
		"parent": "pnlMainBodyButtonsCheckout"
	},
	"pnlModal": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModal",
			"componentVisible": false
		},
		"style": {
			"width": "100%",
			"background": "rgba(0, 0, 0, 0.60)",
			"top": "0px",
			"zIndex": "2",
			"position": "fixed",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"lblModalTitle": {
		"style": {
			"width": "100%",
			"color": "rgb(33, 33, 33)",
			"fontSize": "18px",
			"fontWeight": "bold"
		},
		"parent": "pnlModalBodyTitle",
		"property": {
			"componentType": "label",
			"componentName": "lblModalTitle",
			"componentValue": "Loading",
			"componentVisible": true
		}
	},
	"pnlMainHeaderMiddleCell893": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddleCell893",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderMiddleTable107"
	},
	"pnlUpdateCartModalTableItemListing": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalTableItemListing",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlUpdateCart"
	},
	"btnUpdate": {
		"property": {
			"componentType": "fbutton",
			"componentName": "btnUpdate",
			"componentValue": "ADD",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "40px",
			"fontWeight": "bold",
			"padding": "8px",
			"display": "none",
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"backgroundColor": "rgb(255, 69, 0)",
			"margin": "0px"
		},
		"parent": "pnlUpdateCartModalBodyButtonItemListing"
	},
	"lblStyle": {
		"property": {
			"componentType": "label",
			"componentName": "lblStyle",
			"componentValue": "N/A",
			"componentVisible": true
		},
		"parent": "pnlCSS"
	},
	"lblUpdateCartQuantityIL386S260": {
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateCartQuantityIL386S260",
			"componentValue": "UOM",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "16px",
			"fontWeight": "bolder",
			"textAlign": "left"
		},
		"parent": "pnlUpdateCartModalBodyUOMIL707"
	},
	"lblUpdateCartQuantityIL552": {
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateCartQuantityIL552",
			"componentValue": "Quantity",
			"componentVisible": true
		},
		"style": {
			"fontWeight": "bolder",
			"fontSize": "18px",
			"textAlign": "center",
			"display": "block",
			"color": "rgb(255, 255, 255)"
		},
		"parent": "pnlUpdateCartModalBodyQtyIL"
	},
	"pnlMainBodyButtonsCheckout": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBodyButtonsCheckout",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"backgroundColor": "rgb(255, 69, 0)",
			"display": "table",
			"width": "100%",
			"height": "40px"
		},
		"parent": "pnlMain"
	},
	"pnlUpdateCartModalBodyItemListingCode": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyItemListingCode",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"width": "100%",
			"padding": "10px"
		},
		"parent": "pnlUpdateCartModalBodyItemListing"
	},
	"txtQuantity": {
		"property": {
			"componentType": "edit",
			"componentName": "txtQuantity",
			"componentValue": "0",
			"type": "number",
			"componentVisible": true
		},
		"style": {
			"margin": "0px",
			"color": "rgb(33, 33, 33)",
			"width": "100%",
			"fontSize": "24px",
			"float": "left",
			"textAlign": "center",
			"height": "40px",
			"fontWeight": "normal",
			"border": "1px solid #FFF"
		},
		"parent": "pnlUpdateCartModalBodyMiddle"
	},
	"pnlMain": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMain",
			"componentVisible": true
		},
		"style": {
			"zIndex": "0",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"imgBack65": {
		"property": {
			"componentType": "image",
			"componentName": "imgBack65",
			"componentValue": require("../../asset/icon/icon-back.png"),
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "35px",
			"height": "35px"
		},
		"parent": "pnlMainHeaderLeftCell594"
	},
	"pnlUpdateCartModalBodyItemListingImage": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyItemListingImage",
			"componentVisible": true
		},
		"style": {
			"position": "relative",
			"width": "100%"
		},
		"parent": "pnlUpdateCartModalBodyItemListing"
	},
	"pnlCSS": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlCSS",
			"componentVisible": false
		},
		"style": {
			"top": "0px",
			"width": "100%",
			"position": "fixed",
			"display": "none",
			"zIndex": "5",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"pnlUpdateCartModalBodyLeft": {
		"style": {
			"textAlign": "center",
			"border": "1px solid #FFF",
			"verticalAlign": "middle",
			"width": "50px",
			"backgroundColor": "rgb(255, 69, 0)",
			"display": "table-cell"
		},
		"parent": "pnlUpdateCartModalBodyQtyValueIL",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyLeft",
			"componentVisible": true
		}
	},
	"pnlMainHeaderRight518": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRight518",
			"componentVisible": true
		},
		"style": {
			"width": "15%",
			"height": "100%",
			"float": "left"
		},
		"parent": "pnlMainHeader"
	},
	"pnlUpdateCartModa": {
		"style": {
			"display": "table",
			"width": "100%",
			"backgroundColor": "rgba(255, 255, 255, 0)"
		},
		"parent": "Panel524",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModa",
			"componentVisible": true
		}
	},
	"pnlUpdateCartModalBodyUOMValueIL403": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyUOMValueIL403",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"border": "1px solid #FFF",
			"width": "calc(100% - 50px)"
		},
		"parent": "pnlUpdateCartModa"
	},
	"pnlFooterNavBtnUpdateTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlFooterNavBtnUpdateTable",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "40px"
		},
		"parent": "pnlUpdateCartModalBodyButtonItemListing"
	},
	"pnlModalCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalCell",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "100%"
		},
		"parent": "pnlModalTable"
	},
	"btnModalPositive": {
		"property": {
			"componentType": "fbutton",
			"componentName": "btnModalPositive",
			"componentValue": "OK",
			"ColoringStyle": "normal",
			"componentVisible": true
		},
		"style": {
			"margin": "0px",
			"width": "80%",
			"fontWeight": "bold"
		},
		"parent": "pnlModalBodyButtonPositive"
	},
	"txtSearch": {
		"property": {
			"componentType": "edit",
			"componentName": "txtSearch",
			"placeHolder": "Search",
			"backgroundAsset": "icon/_search.png",
			"type": "search",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"border": "1px solid #D9D9D9",
			"backgroundSize": "16px",
			"textAlign": "left",
			"background": "url('data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Capa_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 451 451\" style=\"enable-background:new 0 0 451 451;\" xml:space=\"preserve\" width=\"512px\" height=\"512px\"><g><path d=\"M447.05,428l-109.6-109.6c29.4-33.8,47.2-77.9,47.2-126.1C384.65,86.2,298.35,0,192.35,0C86.25,0,0.05,86.3,0.05,192.3   s86.3,192.3,192.3,192.3c48.2,0,92.3-17.8,126.1-47.2L428.05,447c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4   C452.25,441.8,452.25,433.2,447.05,428z M26.95,192.3c0-91.2,74.2-165.3,165.3-165.3c91.2,0,165.3,74.2,165.3,165.3   s-74.1,165.4-165.3,165.4C101.15,357.7,26.95,283.5,26.95,192.3z\" fill=\"#3c3c3b\"/></g></svg>') no-repeat",
			"backgroundPosition": "left .5rem center",
			"color": "rgb(60, 60, 59)",
			"fontSize": "16px",
			"backgroundColor": "rgb(255, 255, 255)",
			"paddingLeft": "30px",
			"margin": "0px",
			"display": "inline",
			"height": "40px"
		},
		"parent": "pnlMainBodySearch290"
	},
	"Panel524": {
		"property": {
			"componentType": "panel",
			"componentName": "Panel524",
			"componentVisible": true
		},
		"style": {
			"backgroundColor": "rgb(84, 84, 84)",
			"padding": "10px"
		},
		"parent": "pnlUpdateCartModalBodyItemListing"
	},
	"pnlUpdateCart": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCart",
			"componentVisible": false
		},
		"style": {
			"top": "0px",
			"zIndex": "1",
			"width": "100%",
			"position": "fixed",
			"background": "rgba(0, 0, 0, 0.60)",
			"height": "100%"
		},
		"parent": "pnlHolder"
	},
	"lblUpdateCartItemCodeValueIL": {
		"style": {
			"fontSize": "24px",
			"display": "block",
			"color": "rgb(60, 60, 59)",
			"fontWeight": "bold",
			"textAlign": "left"
		},
		"parent": "pnlUpdateCartModalBodyItemListingCode",
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateCartItemCodeValueIL",
			"field": "ItemCode",
			"componentVisible": true
		}
	},
	"pnlModalBodyButtonPositive": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonPositive",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBody"
	},
	"pnlModalBodyButtonNegative": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyButtonNegative",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"textAlign": "center"
		},
		"parent": "pnlModalBody"
	},
	"pnlMainHeaderLeftCell594": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftCell594",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"height": "100%",
			"paddingLeft": "10px",
			"width": "100%",
			"textAlign": "left"
		},
		"parent": "pnlMainHeaderLeftTable917"
	},
	"pnlMainBody165": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody165",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"borderBottom": "1px solid #D9D9D9",
			"width": "100%",
			"height": "50px"
		},
		"parent": "pnlMainBody"
	},
	"pnlModalTable": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalTable",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table"
		},
		"parent": "pnlModal"
	},
	"lblModalMessage": {
		"style": {
			"width": "100%",
			"color": "rgb(131, 136, 146)",
			"fontSize": "16px"
		},
		"parent": "pnlModalBodyMessage",
		"property": {
			"componentType": "label",
			"componentName": "lblModalMessage",
			"componentValue": "Loading ...",
			"componentVisible": true
		}
	},
	"dlProducts541": {
		"property": {
			"componentType": "datalist",
			"componentName": "dlProducts541",
			"componentValue": "l_Item_List",
			"autoLoad": false,
			"componentData": [],
			"componentVisible": true
		},
		"parent": "pnlMainBody2358",
		"template": {
			"componentDataItemState": {
				"pnlProductBodyLeft": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyLeft",
						"componentVisible": true
					},
					"style": {
						"verticalAlign": "top",
						"display": "table-cell",
						"width": "100px",
						"textAlign": "center"
					},
					"parent": "pnlProductBody"
				},
				"imgItem": {
					"property": {
						"componentType": "image",
						"componentName": "imgItem",
						"componentValue": require("../../asset/images/default.png"),
						"componentVisible": true
					},
					"style": {
						"width": "100px",
						"height": "90px"
					},
					"parent": "pnlProductBodyLeft"
				},
				"pnlProductBodyMiddle1": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle1",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"width": "100%"
					},
					"parent": "pnlProductBodyMiddle"
				},
				"lblItemCode": {
					"style": {
						"textAlign": "left",
						"color": "rgb(60, 60, 59)",
						"fontSize": "24px",
						"fontWeight": "bold",
						"display": "block"
					},
					"parent": "pnlProductBodyMiddle1",
					"property": {
						"componentType": "label",
						"componentName": "lblItemCode",
						"componentValue": "ItemCode",
						"field": "No",
						"componentVisible": true
					}
				},
				"lblItemName": {
					"property": {
						"componentType": "label",
						"componentName": "lblItemName",
						"componentValue": "ItemName",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"fontWeight": "normal",
						"color": "rgb(60, 60, 59)",
						"fontSize": "18px",
						"textAlign": "left"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"lblStockOnHand": {
					"property": {
						"componentType": "label",
						"componentName": "lblStockOnHand",
						"componentValue": "Qty         UOM",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"fontWeight": "normal",
						"textAlign": "left",
						"fontSize": "16px",
						"color": "rgb(0, 153, 0)",
						"marginTop": "10px"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"pnlProduct": {
					"style": {
						"width": "100%",
						"padding": "5px",
						"paddingBottom": "0px",
						"position": "relative",
						"textAlign": "left"
					},
					"property": {
						"componentType": "panel",
						"componentName": "pnlProduct",
						"componentVisible": true
					}
				},
				"pnlProductBody": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBody",
						"componentVisible": true
					},
					"style": {
						"display": "table",
						"boxShadow": "-1px 1px 1px #545454",
						"background": "url() no-repeat fixed",
						"borderRadius": "5px",
						"width": "100%",
						"backgroundColor": "rgb(255, 255, 255)",
						"border": "1px solid #3C3C3B",
						"padding": "10px"
					},
					"parent": "pnlProduct"
				},
				"imgAddItem": {
					"style": {
						"width": "30px",
						"height": "30px"
					},
					"parent": "pnlProductBodyRight1Cell",
					"property": {
						"componentType": "image",
						"componentName": "imgAddItem",
						"componentValue": require("../../asset/icon/icon-add-gray.png"),
						"componentVisible": true
					}
				},
				"lblCurrentQtyOrder": {
					"property": {
						"componentType": "label",
						"componentName": "lblCurrentQtyOrder",
						"componentVisible": true
					},
					"style": {
						"display": "block",
						"color": "rgb(60, 60, 59)",
						"textAlign": "left"
					},
					"parent": "pnlProductBodyMiddle1"
				},
				"pnlProductBodyRight": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyRight",
						"componentVisible": true
					},
					"style": {
						"verticalAlign": "middle",
						"width": "50px",
						"textAlign": "center",
						"display": "table-cell",
						"height": "100%"
					},
					"parent": "pnlProductBody"
				},
				"pnlProductBodyMiddle": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyMiddle",
						"componentVisible": true
					},
					"style": {
						"display": "table-cell",
						"verticalAlign": "top",
						"width": "calc(100% - 100px - 50px)",
						"textAlign": "center",
						"paddingLeft": "10px"
					},
					"parent": "pnlProductBody"
				},
				"pnlProductBodyRight1Cell": {
					"property": {
						"componentType": "panel",
						"componentName": "pnlProductBodyRight1Cell",
						"componentVisible": true
					},
					"style": {
						"width": "100%",
						"height": "100%"
					},
					"parent": "pnlProductBodyRight"
				}
			}
		}
	},
	"pnlFooterNavBtnUpdateTableCell": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlFooterNavBtnUpdateTableCell",
			"componentVisible": true
		},
		"style": {
			"textAlign": "center",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlFooterNavBtnUpdateTable"
	},
	"pnlUpdateCartModalBodyQtyValueIL": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyQtyValueIL",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "40px"
		},
		"parent": "pnlUpdateCartModalBodyItemListingQty"
	},
	"pnlUpdateCartModalBodyQtyIL": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyQtyIL",
			"componentVisible": true
		},
		"style": {
			"marginTop": "10px",
			"display": "block",
			"textAlign": "left"
		},
		"parent": "pnlUpdateCartModalBodyItemListingQty"
	},
	"lblBtnUpdate": {
		"property": {
			"componentType": "label",
			"componentName": "lblBtnUpdate",
			"componentValue": "ADD",
			"componentVisible": true
		},
		"style": {
			"fontSize": "18px",
			"color": "rgb(255, 255, 255)"
		},
		"parent": "pnlFooterNavBtnUpdateTableCell"
	},
	"pnlMainBody2358": {
		"parent": "pnlMainBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody2358",
			"componentVisible": true
		},
		"style": {
			"height": "calc(100% - 50px)",
			"overflow": "auto",
			"width": "100%"
		}
	},
	"pnlUpdateCartModalBodyItemListingQty": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyItemListingQty",
			"componentVisible": true
		},
		"style": {
			"marginTop": "10px",
			"width": "100%"
		},
		"parent": "Panel524"
	},
	"pnlUpdateCartModalBodyUOMIL707": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyUOMIL707",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"verticalAlign": "middle",
			"width": "50px",
			"textAlign": "center",
			"border": "1px solid #FFF"
		},
		"parent": "pnlUpdateCartModa"
	},
	"pnlUpdateCartModalBodyMiddle": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyMiddle",
			"componentVisible": true
		},
		"style": {
			"display": "table-cell",
			"width": "calc(100% - 50px - 50px)",
			"height": "100%",
			"border": "1px solid #FFF"
		},
		"parent": "pnlUpdateCartModalBodyQtyValueIL"
	},
	"pnlUpdateCartModalBodyRight": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyRight",
			"componentVisible": true
		},
		"style": {
			"verticalAlign": "middle",
			"height": "100%",
			"textAlign": "center",
			"backgroundColor": "rgb(255, 69, 0)",
			"border": "1px solid #FFF",
			"display": "table-cell",
			"width": "50px"
		},
		"parent": "pnlUpdateCartModalBodyQtyValueIL"
	},
	"pnlModalBodyLoading": {
		"parent": "pnlModalBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyLoading",
			"addClass": "hide",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		}
	},
	"pnlHolder": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlHolder",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"backgroundColor": "rgb(245, 245, 245)"
		},
		"parent": "pgOrderItem"
	},
	"lblMainTitle309": {
		"style": {
			"width": "100%",
			"color": "rgb(255, 255, 255)",
			"fontSize": "20px",
			"fontWeight": "500"
		},
		"parent": "pnlMainHeaderMiddleCell893",
		"property": {
			"componentType": "label",
			"componentName": "lblMainTitle309",
			"componentValue": "ITEM LIST",
			"componentVisible": true
		}
	},
	"Label752": {
		"style": {
			"color": "rgb(255, 255, 255)",
			"fontSize": "18px",
			"fontWeight": "bold"
		},
		"parent": "Panel186",
		"property": {
			"componentType": "label",
			"componentName": "Label752",
			"componentValue": "OK",
			"componentVisible": true
		}
	},
	"pnlUpdateCartModalCellItemListing": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalCellItemListing",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table-cell",
			"verticalAlign": "middle"
		},
		"parent": "pnlUpdateCartModalTableItemListing"
	},
	"pnlMainHeaderRightTable18": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderRightTable18",
			"componentVisible": true
		},
		"style": {
			"display": "table",
			"width": "100%",
			"height": "100%"
		},
		"parent": "pnlMainHeaderRight518"
	},
	"pnlMainBody": {
		"style": {
			"height": "calc(100% - 50px - 15px - 40px)",
			"width": "100%"
		},
		"parent": "pnlMain",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainBody",
			"componentVisible": true
		}
	},
	"ComboUOMItemList": {
		"property": {
			"componentType": "combobox",
			"componentName": "ComboUOMItemList",
			"componentVisible": true
		},
		"style": {
			"width": "100%",
			"height": "40px",
			"margin": "0px"
		},
		"parent": "pnlUpdateCartModalBodyUOMValueIL403"
	},
	"imgPlus266": {
		"property": {
			"componentType": "image",
			"componentName": "imgPlus266",
			"componentValue": require("../../asset/plus-white.png"),
			"componentVisible": true
		},
		"style": {
			"width": "30px",
			"height": "30px"
		},
		"parent": "pnlUpdateCartModalBodyRight"
	},
	"pnlMainHeaderLeft495": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeft495",
			"componentVisible": true
		},
		"style": {
			"float": "left",
			"width": "15%",
			"height": "100%"
		},
		"parent": "pnlMainHeader"
	},
	"pnlUpdateCartModalBodyItemListing": {
		"parent": "pnlUpdateCartModalCellItemListing",
		"property": {
			"componentType": "panel",
			"componentName": "pnlUpdateCartModalBodyItemListing",
			"componentVisible": true
		},
		"style": {
			"display": "block",
			"background": "#ffffff",
			"marginRight": "5%",
			"width": "90%",
			"textAlign": "center",
			"marginLeft": "5%"
		}
	},
	"pnlMainHeaderMiddle21": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderMiddle21",
			"componentVisible": true
		},
		"style": {
			"height": "100%",
			"float": "left",
			"width": "70%"
		},
		"parent": "pnlMainHeader"
	},
	"imgUpdateCartImage": {
		"property": {
			"componentType": "image",
			"componentName": "imgUpdateCartImage",
			"componentValue": require("../../asset/images/default.png"),
			"componentVisible": true
		},
		"style": {
			"height": "90px",
			"margin": "20px",
			"width": "100px"
		},
		"parent": "pnlUpdateCartModalBodyItemListingImage"
	},
	"lblUpdateCartPriceValueIL": {
		"property": {
			"componentType": "label",
			"componentName": "lblUpdateCartPriceValueIL",
			"componentVisible": true
		},
		"style": {
			"color": "rgb(0, 153, 0)",
			"textAlign": "left",
			"marginTop": "20px",
			"display": "block",
			"fontSize": "20px",
			"fontWeight": "bold"
		},
		"parent": "pnlUpdateCartModalBodyItemListingCode"
	},
	"imgMinus345": {
		"property": {
			"componentType": "image",
			"componentName": "imgMinus345",
			"componentValue": require("../../asset/minus-white.png"),
			"componentVisible": true
		},
		"style": {
			"width": "30px",
			"height": "30px"
		},
		"parent": "pnlUpdateCartModalBodyLeft"
	},
	"pgOrderItem": {
		"property": {
			"componentType": "page",
			"componentName": "pgOrderItem",
			"title": "OrderItem",
			"componentVisible": true
		}
	},
	"pnlMainHeaderLeftTable917": {
		"style": {
			"width": "100%",
			"height": "100%",
			"display": "table"
		},
		"parent": "pnlMainHeaderLeft495",
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeaderLeftTable917",
			"componentVisible": true
		}
	},
	"pnlModalBodyMessage": {
		"style": {
			"width": "100%",
			"marginBottom": "10px"
		},
		"parent": "pnlModalBody",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBodyMessage",
			"addClass": "hide",
			"componentVisible": true
		}
	},
	"pnlMainHeader": {
		"property": {
			"componentType": "panel",
			"componentName": "pnlMainHeader",
			"componentVisible": true
		},
		"style": {
			"paddingTop": "15px",
			"width": "100%",
			"height": "65px",
			"backgroundColor": "rgb(255, 69, 0)",
			"textAlign": "center"
		},
		"parent": "pnlMain"
	},
	"pnlModalBody": {
		"style": {
			"background": "#ffffff",
			"marginLeft": "5%",
			"marginRight": "5%",
			"width": "90%",
			"textAlign": "center",
			"padding": "10px"
		},
		"parent": "pnlModalCell",
		"property": {
			"componentType": "panel",
			"componentName": "pnlModalBody",
			"componentVisible": true
		}
	}
};
