import { combineReducers } from 'redux';

import reducerWrapper from '../framework/core/reducer/Wrapper'

import { StateName as StateNameAppPage, State as StateAppPage } from './data/AppPage';
import { StateName as StateNameSamplePage, State as StateSamplePage } from './data/SamplePage';
import { StateName as StateNameLanguagePage, State as StateLanguagePage } from './data/LanguagePage';
import { StateName as StateNameArrayPage, State as StateArrayPage } from './data/ArrayPage';
import { StateName as StateNameConversionPage, State as StateConversionPage } from './data/ConversionPage';
import { StateName as StateNameNavisionPage, State as StateNavisionPage } from './data/NavisionPage';
import { StateName as StateNameDevicePage, State as StateDevicePage } from './data/DevicePage';
import { StateName as StateNameCssPage, State as StateCssPage } from './data/CssPage';
import { StateName as StateNameStylePage, State as StateStylePage } from './data/StylePage';
import { StateName as StateNameAwesomePage, State as StateAwesomePage } from './data/AwesomePage';
import { StateName as StateNamePrinterPage, State as StatePrinterPage } from './data/PrinterPage';
import { StateName as StateNameNextPage, State as StateNextPage } from './data/NextPage';
import { StateName as StateNameOrderHistoryDetails, State as StateOrderHistoryDetails } from './data/OrderHistoryDetails';
import { StateName as StateNameMainMenuPage, State as StateMainMenuPage } from './data/MainMenuPage';
import { StateName as StateNameLocalAuthPage, State as StateLocalAuthPage } from './data/LocalAuthPage';
import { StateName as StateNameQueryLocalTablePage, State as StateQueryLocalTablePage } from './data/QueryLocalTablePage';
import { StateName as StateNameRenderPage, State as StateRenderPage } from './data/RenderPage';
import { StateName as StateNameShiftingPage, State as StateShiftingPage } from './data/ShiftingPage';
import { StateName as StateNameRawCallPage, State as StateRawCallPage } from './data/RawCallPage';
import { StateName as StateNameTestPage, State as StateTestPage } from './data/TestPage';
import { StateName as StateNameScaleImagePage, State as StateScaleImagePage } from './data/ScaleImagePage';
import { StateName as StateNameEditOnChangePage, State as StateEditOnChangePage } from './data/EditOnChangePage';
import { StateName as StateNameFlatListEndReachedPage, State as StateFlatListEndReachedPage } from './data/FlatListEndReachedPage';

//VANSALES
import { StateName as StateNamePgSplash, State as StatePgSplash } from './data/PgSplash'
import { StateName as StateNamePgMainMenu, State as StatePgMainMenu } from './data/PgMainMenu'
import { StateName as StateNamePgCustomerListing, State as StatePgCustomerListing } from './data/PgCustomerListing'
import { StateName as StateNamePgItemListing, State as StatePgItemListing } from './data/PgItemListing'
import { StateName as StateNamePgCheckout, State as StatePgCheckout } from './data/PgCheckout'
import { StateName as StateNamePgOrderList, State as StatePgOrderList } from './data/PgOrderList'
import { StateName as StateNamePgOrderDetails, State as StatePgOrderDetails } from './data/PgOrderDetails'
import { StateName as StateNamePgOrderHistory, State as StatePgOrderHistory } from './data/PgOrderHistory'
import { StateName as StateNamePgOrderHistoryDetails, State as StatePgOrderHistoryDetails } from './data/PgOrderHistoryDetails'
import { StateName as StateNamePgOrderItem, State as StatePgOrderItem } from './data/PgOrderItem'
import { StateName as StateNamePgSummary, State as StatePgSummary } from './data/PgSummary'
import { StateName as StateNamePgPayment, State as StatePgPayment } from './data/PgPayment'
import { StateName as StateNamePgSettings, State as StatePgSettings } from './data/PgSettings'
import { StateName as StateNamePgCashCollection, State as StatePgCashCollection } from './data/PgCashCollection'
import { StateName as StateNamePgSalesReturnLists, State as StatePgSalesReturnLists } from './data/PgSalesReturnLists'
import { StateName as StateNamePgSalesReturnDetailsNew, State as StatePgSalesReturnDetailsNew } from './data/PgSalesReturnDetailsNew'
import { StateName as StateNamePgSelectItem, State as StatePgSelectItem } from './data/PgSelectItem'
import { StateName as StateNamePgSalesReturnDetailsUpdate, State as StatePgSalesReturnDetailsUpdate } from './data/PgSalesReturnDetailsUpdate'
import { StateName as StateNamePgStockTake, State as StatePgStockTake } from './data/PgStockTake'
import { StateName as StateNamePgTransferStocks, State as StatePgTransferStocks } from './data/PgTransferStocks'
import { StateName as StateNamePgTransferStockDetails, State as StatePgTransferStockDetails } from './data/PgTransferStockDetails'
import { StateName as StateNamePgDailySalesReport, State as StatePgDailySalesReport } from './data/PgDailySalesReport'
import { StateName as StateNamePgLogin, State as StatePgLogin } from './data/PgLogin'
import { StateName as StateNamePgRegisterNAVConnectionInfo, State as StatePgRegisterNAVConnectionInfo } from './data/PgRegisterNAVConnectionInfo'

export default combineReducers({
	[StateNameAppPage]: reducerWrapper(StateNameAppPage, StateAppPage),
	[StateNameSamplePage]: reducerWrapper(StateNameSamplePage, StateSamplePage),
	[StateNameLanguagePage]: reducerWrapper(StateNameLanguagePage, StateLanguagePage),
	[StateNameArrayPage]: reducerWrapper(StateNameArrayPage, StateArrayPage),
	[StateNameConversionPage]: reducerWrapper(StateNameConversionPage, StateConversionPage),
	[StateNameNavisionPage]: reducerWrapper(StateNameNavisionPage, StateNavisionPage),
	[StateNameDevicePage]: reducerWrapper(StateNameDevicePage, StateDevicePage),
	[StateNameCssPage]: reducerWrapper(StateNameCssPage, StateCssPage),
	[StateNameAwesomePage]: reducerWrapper(StateNameAwesomePage, StateAwesomePage),
	[StateNamePrinterPage]: reducerWrapper(StateNamePrinterPage, StatePrinterPage),
	[StateNameNextPage]: reducerWrapper(StateNameNextPage, StateNextPage),
	[StateNameStylePage]: reducerWrapper(StateNameStylePage, StateStylePage),
	[StateNameOrderHistoryDetails]: reducerWrapper(StateNameOrderHistoryDetails, StateOrderHistoryDetails),
	[StateNameMainMenuPage]: reducerWrapper(StateNameMainMenuPage, StateMainMenuPage),
	[StateNameLocalAuthPage]: reducerWrapper(StateNameLocalAuthPage, StateLocalAuthPage),
	[StateNameQueryLocalTablePage]: reducerWrapper(StateNameQueryLocalTablePage, StateQueryLocalTablePage),
	[StateNameRenderPage]: reducerWrapper(StateNameRenderPage, StateRenderPage),
	[StateNameShiftingPage]: reducerWrapper(StateNameShiftingPage, StateShiftingPage),
	[StateNameRawCallPage]: reducerWrapper(StateNameRawCallPage, StateRawCallPage),
	[StateNameTestPage]: reducerWrapper(StateNameTestPage, StateTestPage),
	[StateNameScaleImagePage]: reducerWrapper(StateNameScaleImagePage, StateScaleImagePage),
	[StateNameEditOnChangePage]: reducerWrapper(StateNameEditOnChangePage, StateEditOnChangePage),
	[StateNameFlatListEndReachedPage]: reducerWrapper(StateNameFlatListEndReachedPage, StateFlatListEndReachedPage),

	// VANSALES
	[StateNamePgSplash]: reducerWrapper(StateNamePgSplash, StatePgSplash),
	[StateNamePgMainMenu]: reducerWrapper(StateNamePgMainMenu, StatePgMainMenu),
	[StateNamePgCustomerListing]: reducerWrapper(StateNamePgCustomerListing, StatePgCustomerListing),
	[StateNamePgItemListing]: reducerWrapper(StateNamePgItemListing, StatePgItemListing),
	[StateNamePgCheckout]: reducerWrapper(StateNamePgCheckout, StatePgCheckout),
	[StateNamePgOrderList]: reducerWrapper(StateNamePgOrderList, StatePgOrderList),
	[StateNamePgOrderDetails]: reducerWrapper(StateNamePgOrderDetails, StatePgOrderDetails),
	[StateNamePgOrderHistory]: reducerWrapper(StateNamePgOrderHistory, StatePgOrderHistory),
	[StateNamePgOrderHistoryDetails]: reducerWrapper(StateNamePgOrderHistoryDetails, StatePgOrderHistoryDetails),
	[StateNamePgOrderItem]: reducerWrapper(StateNamePgOrderItem, StatePgOrderItem),
	[StateNamePgSummary]: reducerWrapper(StateNamePgSummary, StatePgSummary),
	[StateNamePgPayment]: reducerWrapper(StateNamePgPayment, StatePgPayment),
	[StateNamePgSettings]: reducerWrapper(StateNamePgSettings, StatePgSettings),
	[StateNamePgCashCollection]: reducerWrapper(StateNamePgCashCollection, StatePgCashCollection),
	[StateNamePgSalesReturnLists]: reducerWrapper(StateNamePgSalesReturnLists, StatePgSalesReturnLists),
	[StateNamePgSalesReturnDetailsNew]: reducerWrapper(StateNamePgSalesReturnDetailsNew, StatePgSalesReturnDetailsNew),
	[StateNamePgSelectItem]: reducerWrapper(StateNamePgSelectItem, StatePgSelectItem),
	[StateNamePgSalesReturnDetailsUpdate]: reducerWrapper(StateNamePgSalesReturnDetailsUpdate, StatePgSalesReturnDetailsUpdate),
	[StateNamePgStockTake]: reducerWrapper(StateNamePgStockTake, StatePgStockTake),
	[StateNamePgTransferStocks]: reducerWrapper(StateNamePgTransferStocks, StatePgTransferStocks),
	[StateNamePgTransferStockDetails]: reducerWrapper(StateNamePgTransferStockDetails, StatePgTransferStockDetails),
	[StateNamePgDailySalesReport]: reducerWrapper(StateNamePgDailySalesReport, StatePgDailySalesReport),
	[StateNamePgLogin]: reducerWrapper(StateNamePgLogin, StatePgLogin),
	[StateNamePgRegisterNAVConnectionInfo]: reducerWrapper(StateNamePgRegisterNAVConnectionInfo, StatePgRegisterNAVConnectionInfo)
});