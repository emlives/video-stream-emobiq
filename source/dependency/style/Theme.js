/**
 * Contains all the css that was generated from eMOBIQ
 * to be used by the framework default styles.
 * 
 * Note: Don't delete this file, you can edit it but make
 * sure you know what you are doing as it might break your app.
 */

import { StyleSheet } from 'react-native';

export default Theme = StyleSheet.create({
    page: {
        flexGrow: 1,
    },
    button: {
        color: '#ffffff',
        backgroundColor: '#2199e8',
        borderStyle: 'solid',
        borderColor: '#2199e8',
        borderWidth: 1,
        borderRadius: 2,
        padding: 10,
    },
    edit: {
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: '#aaaaaa'
    },
    memo: {
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: '#aaaaaa'
    },
    comboBox: {
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: "black",
        display: "flex",
        justifyContent: "center"
    },
    /**
      * Later this one needs to be inserted within the style generation
      * this should be the default behavior as it is how it runs in
      * browser.
      */
    image: {
        resizeMode: 'contain'
    }
});