/**
 * Contains all the services that was obtained from eMOBIQ.
 * 
 * To be used by the framework.
 * 
 * Note: Don't delete this file, you can edit it but make
 * sure you know what you are doing as it might break your app.
 */

import LocalTable from '../../framework/service/dataset/LocalTable';
import NavisionConnector from '../../framework/service/connector/NavisionConnector';
import RawConnector from '../../framework/service/connector/RawConnector';

import NavisionDataset from '../../framework/service/dataset/NavisionDataset';

/**
 * Variable that would contain all the services to be used
 * by the framework.
 */
const Services = {};


/*
 * /// VANSALES ///
 */
const A_setting = new LocalTable(
	'A_setting', 
	{
		"fields": {
			"id": {
				"tp": "text"
			},
			"language": {
				"tp": "text"
			},
			"notification": {
				"tp": "text"
			},
			"touch_sensor": {
				"tp": "text"
			}
		}
	}
);

Services.A_setting = A_setting;

const A_user = new LocalTable(
	'A_user', 
	{
		"fields": {
			"address": {
				"tp": "text"
			},
			"blocked": {
				"tp": "text"
			},
			"company": {
				"tp": "text"
			},
			"customer_name": {
				"tp": "text"
			},
			"customer_no": {
				"tp": "text"
			},
			"discount_percentage": {
				"tp": "text"
			},
			"id": {
				"tp": "text"
			},
			"price_group": {
				"tp": "text"
			},
			"vat_percentage": {
				"tp": "text"
			}
		}
	}
);

Services.A_user = A_user;

const A_languages = new LocalTable(
	'A_languages', 
	{
		"fields": {
			"name": {
				"tp": "text"
			},
			"value": {
				"tp": "text"
			}
		}
	}
);

Services.A_languages = A_languages;

const l_item = new LocalTable(
	'l_item', 
	{
		"fields": {
			"code": {
				"tp": "text"
			},
			"name": {
				"tp": "text"
			},
			"price": {
				"tp": "text"
			},
			"quantity": {
				"tp": "text"
			},
			"type": {
				"tp": "text"
			}
		}
	}
);

Services.l_item = l_item;

const l_salesDocument = new LocalTable(
	'l_salesDocument', 
	{
		"fields": []
	}
);

Services.l_salesDocument = l_salesDocument;

const l_exchange = new LocalTable(
	'l_exchange', 
	{
		"fields": {
			"UOM": {
				"tp": "text"
			},
			"code": {
				"tp": "text"
			},
			"quantity": {
				"tp": "text"
			}
		}
	}
);

Services.l_exchange = l_exchange;

const nav = new NavisionConnector(
	'nav', 
	{
		"company": "Orangekloud",
		"local": false,
		"password": "",
		"timeOut": "600000",
		"type": "soap",
		"url": "https://api.businesscentral.dynamics.com/v1.0/425096bb-f087-4c7d-ac68-7ac4ca13116d/Sandbox",
		"user": ""
	}, 
	{
		"AccountantPortalActivityCues": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"AccountantPortalActivityCues": {
					"AverageCollectionDaysAmount": "string",
					"ContactNameAmount": "string",
					"IncDocAwaitingVerifAmount": "string",
					"Key": "string",
					"MyIncomingDocumentsAmount": "string",
					"NonAppliedPaymentsAmount": "string",
					"OngoingPurchaseInvoicesAmount": "string",
					"OngoingSalesInvoicesAmount": "string",
					"OngoingSalesOrdersAmount": "string",
					"OngoingSalesQuotesAmount": "string",
					"OverduePurchInvoiceAmount": "string",
					"OverdueSalesInvoiceAmount": "string",
					"PurchInvoicesDueNextWeekAmount": "string",
					"PurchaseOrdersAmount": "string",
					"RequeststoApproveAmount": "string",
					"SalesCrMPendDocExchangeAmount": "string",
					"SalesInvPendDocExchangeAmount": "string",
					"SalesInvoicesDueNextWeekAmount": "string",
					"SalesThisMonthAmount": "string",
					"Top10CustomerSalesYTDAmount": "string"
				},
				"AccountantPortalActivityCues_Fields": "string",
				"AccountantPortalActivityCues_Filter": {
					"Criteria": "string",
					"Field": "AccountantPortalActivityCues_Fields"
				},
				"AccountantPortalActivityCues_List": {
					"AccountantPortalActivityCues": "AccountantPortalActivityCues"
				},
				"Create": {
					"AccountantPortalActivityCues": "AccountantPortalActivityCues"
				},
				"CreateMultiple": {
					"AccountantPortalActivityCues_List": "AccountantPortalActivityCues_List"
				},
				"CreateMultiple_Result": {
					"AccountantPortalActivityCues_List": "AccountantPortalActivityCues_List"
				},
				"Create_Result": {
					"AccountantPortalActivityCues": "AccountantPortalActivityCues"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"AccountantPortalActivityCues": "AccountantPortalActivityCues"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "AccountantPortalActivityCues_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "AccountantPortalActivityCues_List"
				},
				"Update": {
					"AccountantPortalActivityCues": "AccountantPortalActivityCues"
				},
				"UpdateMultiple": {
					"AccountantPortalActivityCues_List": "AccountantPortalActivityCues_List"
				},
				"UpdateMultiple_Result": {
					"AccountantPortalActivityCues_List": "AccountantPortalActivityCues_List"
				},
				"Update_Result": {
					"AccountantPortalActivityCues": "AccountantPortalActivityCues"
				}
			}
		},
		"AccountantPortalFinanceCues": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"AccountantPortalFinanceCues": {
					"ApprovedIncomingDocumentsAmount": "string",
					"ApprovedPurchaseOrdersAmount": "string",
					"ApprovedSalesOrdersAmount": "string",
					"CashAccountsBalanceAmount": "string",
					"CustomersBlockedAmount": "string",
					"Key": "string",
					"LastDepreciatedPostedDateAmount": "string",
					"LastLoginDateAmount": "string",
					"NewIncomingDocumentsAmount": "string",
					"OCRCompletedAmount": "string",
					"OCRPendingAmount": "string",
					"OverduePurchaseDocumentsAmount": "string",
					"OverdueSalesDocumentsAmount": "string",
					"POsPendingApprovalAmount": "string",
					"PurchaseDiscountsNextWeekAmount": "string",
					"PurchaseDocumentsDueTodayAmount": "string",
					"PurchaseReturnOrdersAmount": "string",
					"RequestsSentForApprovalAmount": "string",
					"RequestsToApproveAmount": "string",
					"SOsPendingApprovalAmount": "string",
					"SalesReturnOrdersAllAmount": "string",
					"VendorsPaymentsOnHoldAmount": "string"
				},
				"AccountantPortalFinanceCues_Fields": "string",
				"AccountantPortalFinanceCues_Filter": {
					"Criteria": "string",
					"Field": "AccountantPortalFinanceCues_Fields"
				},
				"AccountantPortalFinanceCues_List": {
					"AccountantPortalFinanceCues": "AccountantPortalFinanceCues"
				},
				"Create": {
					"AccountantPortalFinanceCues": "AccountantPortalFinanceCues"
				},
				"CreateMultiple": {
					"AccountantPortalFinanceCues_List": "AccountantPortalFinanceCues_List"
				},
				"CreateMultiple_Result": {
					"AccountantPortalFinanceCues_List": "AccountantPortalFinanceCues_List"
				},
				"Create_Result": {
					"AccountantPortalFinanceCues": "AccountantPortalFinanceCues"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"AccountantPortalFinanceCues": "AccountantPortalFinanceCues"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "AccountantPortalFinanceCues_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "AccountantPortalFinanceCues_List"
				},
				"Update": {
					"AccountantPortalFinanceCues": "AccountantPortalFinanceCues"
				},
				"UpdateMultiple": {
					"AccountantPortalFinanceCues_List": "AccountantPortalFinanceCues_List"
				},
				"UpdateMultiple_Result": {
					"AccountantPortalFinanceCues_List": "AccountantPortalFinanceCues_List"
				},
				"Update_Result": {
					"AccountantPortalFinanceCues": "AccountantPortalFinanceCues"
				}
			}
		},
		"AccountantPortalUserTasks": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"AccountantPortalUserTasks": {
					"Assigned_To": "string",
					"Created_By_Name": "string",
					"Created_DateTime": "dateTime",
					"Due_DateTime": "dateTime",
					"ID": "int",
					"Key": "string",
					"Link": "string",
					"Percent_Complete": "int",
					"Priority": "Priority",
					"Start_DateTime": "dateTime",
					"Title": "string",
					"User_Task_Group_Assigned_To": "string"
				},
				"AccountantPortalUserTasks_Fields": "string",
				"AccountantPortalUserTasks_Filter": {
					"Criteria": "string",
					"Field": "AccountantPortalUserTasks_Fields"
				},
				"AccountantPortalUserTasks_List": {
					"AccountantPortalUserTasks": "AccountantPortalUserTasks"
				},
				"Create": {
					"AccountantPortalUserTasks": "AccountantPortalUserTasks"
				},
				"CreateMultiple": {
					"AccountantPortalUserTasks_List": "AccountantPortalUserTasks_List"
				},
				"CreateMultiple_Result": {
					"AccountantPortalUserTasks_List": "AccountantPortalUserTasks_List"
				},
				"Create_Result": {
					"AccountantPortalUserTasks": "AccountantPortalUserTasks"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Priority": "string",
				"Read": {
					"ID": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"AccountantPortalUserTasks": "AccountantPortalUserTasks"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "AccountantPortalUserTasks_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "AccountantPortalUserTasks_List"
				},
				"Read_Result": {
					"AccountantPortalUserTasks": "AccountantPortalUserTasks"
				},
				"Update": {
					"AccountantPortalUserTasks": "AccountantPortalUserTasks"
				},
				"UpdateMultiple": {
					"AccountantPortalUserTasks_List": "AccountantPortalUserTasks_List"
				},
				"UpdateMultiple_Result": {
					"AccountantPortalUserTasks_List": "AccountantPortalUserTasks_List"
				},
				"Update_Result": {
					"AccountantPortalUserTasks": "AccountantPortalUserTasks"
				}
			}
		},
		"C2Graph": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"C2Graph": {
					"Component": "string",
					"Key": "string",
					"Schema": "string",
					"Type": "string"
				},
				"C2Graph_Fields": "string",
				"C2Graph_Filter": {
					"Criteria": "string",
					"Field": "C2Graph_Fields"
				},
				"C2Graph_List": {
					"C2Graph": "C2Graph"
				},
				"Create": {
					"C2Graph": "C2Graph"
				},
				"CreateMultiple": {
					"C2Graph_List": "C2Graph_List"
				},
				"CreateMultiple_Result": {
					"C2Graph_List": "C2Graph_List"
				},
				"Create_Result": {
					"C2Graph": "C2Graph"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"Component": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"C2Graph": "C2Graph"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "C2Graph_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "C2Graph_List"
				},
				"Read_Result": {
					"C2Graph": "C2Graph"
				},
				"Update": {
					"C2Graph": "C2Graph"
				},
				"UpdateMultiple": {
					"C2Graph_List": "C2Graph_List"
				},
				"UpdateMultiple_Result": {
					"C2Graph_List": "C2Graph_List"
				},
				"Update_Result": {
					"C2Graph": "C2Graph"
				}
			}
		},
		"Chart_of_Accounts": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Account_Category": "string",
				"Account_Type": "string",
				"Chart_of_Accounts": {
					"Account_Category": "Account_Category",
					"Account_Subcategory_Descript": "string",
					"Account_Type": "Account_Type",
					"Add_Currency_Balance_at_Date": "decimal",
					"Additional_Currency_Balance": "decimal",
					"Additional_Currency_Net_Change": "decimal",
					"Balance": "decimal",
					"Balance_at_Date": "decimal",
					"Consol_Credit_Acc": "string",
					"Consol_Debit_Acc": "string",
					"Consol_Translation_Method": "Consol_Translation_Method",
					"Cost_Type_No": "string",
					"Credit_Amount": "decimal",
					"Debit_Amount": "decimal",
					"Default_Deferral_Template_Code": "string",
					"Default_IC_Partner_G_L_Acc_No": "string",
					"Direct_Posting": "boolean",
					"Gen_Bus_Posting_Group": "string",
					"Gen_Posting_Type": "Gen_Posting_Type",
					"Gen_Prod_Posting_Group": "string",
					"Income_Balance": "Income_Balance",
					"Key": "string",
					"Name": "string",
					"Net_Change": "decimal",
					"No": "string",
					"Totaling": "string",
					"VAT_Bus_Posting_Group": "string",
					"VAT_Prod_Posting_Group": "string"
				},
				"Chart_of_Accounts_Fields": "string",
				"Chart_of_Accounts_Filter": {
					"Criteria": "string",
					"Field": "Chart_of_Accounts_Fields"
				},
				"Chart_of_Accounts_List": {
					"Chart_of_Accounts": "Chart_of_Accounts"
				},
				"Consol_Translation_Method": "string",
				"Create": {
					"Chart_of_Accounts": "Chart_of_Accounts"
				},
				"CreateMultiple": {
					"Chart_of_Accounts_List": "Chart_of_Accounts_List"
				},
				"CreateMultiple_Result": {
					"Chart_of_Accounts_List": "Chart_of_Accounts_List"
				},
				"Create_Result": {
					"Chart_of_Accounts": "Chart_of_Accounts"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Gen_Posting_Type": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"Income_Balance": "string",
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"Chart_of_Accounts": "Chart_of_Accounts"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "Chart_of_Accounts_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "Chart_of_Accounts_List"
				},
				"Read_Result": {
					"Chart_of_Accounts": "Chart_of_Accounts"
				},
				"Update": {
					"Chart_of_Accounts": "Chart_of_Accounts"
				},
				"UpdateMultiple": {
					"Chart_of_Accounts_List": "Chart_of_Accounts_List"
				},
				"UpdateMultiple_Result": {
					"Chart_of_Accounts_List": "Chart_of_Accounts_List"
				},
				"Update_Result": {
					"Chart_of_Accounts": "Chart_of_Accounts"
				}
			}
		},
		"CompanySetupService": {
			"f": {
				"ConfigureCompany": {
					"i": "ConfigureCompany",
					"o": "ConfigureCompany_Result"
				}
			},
			"sp": "Codeunit",
			"t": {
				"ConfigureCompany": {
					"address": "string",
					"address2": "string",
					"city": "string",
					"countryCode": "string",
					"county": "string",
					"name": "string",
					"phoneNo": "string",
					"postCode": "string"
				},
				"ConfigureCompany_Result": {
					"return_value": "boolean"
				}
			}
		},
		"EncryptedKeyValue": {
			"f": {
				"Cleanup": {
					"i": "Cleanup",
					"o": "Cleanup_Result"
				},
				"Insert": {
					"i": "Insert",
					"o": "Insert_Result"
				},
				"Remove": {
					"i": "Remove",
					"o": "Remove_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				}
			},
			"sp": "Codeunit",
			"t": {
				"Cleanup": {
					"": ""
				},
				"Cleanup_Result": {
					"return_value": "boolean"
				},
				"Insert": {
					"key": "string",
					"namespace": "string",
					"value": "string"
				},
				"Insert_Result": {
					"return_value": "boolean"
				},
				"Remove": {
					"key": "string",
					"namespace": "string"
				},
				"Remove_Result": {
					"return_value": "boolean"
				},
				"Update": {
					"key": "string",
					"namespace": "string",
					"value": "string"
				},
				"Update_Result": {
					"return_value": "boolean"
				}
			}
		},
		"ExcelTemplateAgedAccountsPayable": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"ExcelTemplateAgedAccountsPayable": {
					"Key": "string",
					"agedAsOfDate": "date",
					"balanceDue": "decimal",
					"currencyCode": "string",
					"currentAmount": "decimal",
					"name": "string",
					"period1Amount": "decimal",
					"period2Amount": "decimal",
					"period3Amount": "decimal",
					"periodLengthFilter": "string",
					"vendorId": "string",
					"vendorNumber": "string"
				},
				"ExcelTemplateAgedAccountsPayable_Fields": "string",
				"ExcelTemplateAgedAccountsPayable_Filter": {
					"Criteria": "string",
					"Field": "ExcelTemplateAgedAccountsPayable_Fields"
				},
				"ExcelTemplateAgedAccountsPayable_List": {
					"ExcelTemplateAgedAccountsPayable": "ExcelTemplateAgedAccountsPayable"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"vendorId": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"ExcelTemplateAgedAccountsPayable": "ExcelTemplateAgedAccountsPayable"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "ExcelTemplateAgedAccountsPayable_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "ExcelTemplateAgedAccountsPayable_List"
				},
				"Read_Result": {
					"ExcelTemplateAgedAccountsPayable": "ExcelTemplateAgedAccountsPayable"
				}
			}
		},
		"ExcelTemplateAgedAccountsReceivable": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"ExcelTemplateAgedAccountsReceivable": {
					"Key": "string",
					"agedAsOfDate": "date",
					"balanceDue": "decimal",
					"currencyCode": "string",
					"currentAmount": "decimal",
					"customerId": "string",
					"customerNumber": "string",
					"name": "string",
					"period1Amount": "decimal",
					"period2Amount": "decimal",
					"period3Amount": "decimal",
					"periodLengthFilter": "string"
				},
				"ExcelTemplateAgedAccountsReceivable_Fields": "string",
				"ExcelTemplateAgedAccountsReceivable_Filter": {
					"Criteria": "string",
					"Field": "ExcelTemplateAgedAccountsReceivable_Fields"
				},
				"ExcelTemplateAgedAccountsReceivable_List": {
					"ExcelTemplateAgedAccountsReceivable": "ExcelTemplateAgedAccountsReceivable"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"customerId": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"ExcelTemplateAgedAccountsReceivable": "ExcelTemplateAgedAccountsReceivable"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "ExcelTemplateAgedAccountsReceivable_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "ExcelTemplateAgedAccountsReceivable_List"
				},
				"Read_Result": {
					"ExcelTemplateAgedAccountsReceivable": "ExcelTemplateAgedAccountsReceivable"
				}
			}
		},
		"ExcelTemplateBalanceSheet": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"ExcelTemplateBalanceSheet": {
					"Key": "string",
					"balance": "decimal",
					"dateFilter": "date",
					"display": "string",
					"indentation": "int",
					"lineNumber": "int",
					"lineType": "string"
				},
				"ExcelTemplateBalanceSheet_Fields": "string",
				"ExcelTemplateBalanceSheet_Filter": {
					"Criteria": "string",
					"Field": "ExcelTemplateBalanceSheet_Fields"
				},
				"ExcelTemplateBalanceSheet_List": {
					"ExcelTemplateBalanceSheet": "ExcelTemplateBalanceSheet"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"lineNumber": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"ExcelTemplateBalanceSheet": "ExcelTemplateBalanceSheet"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "ExcelTemplateBalanceSheet_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "ExcelTemplateBalanceSheet_List"
				},
				"Read_Result": {
					"ExcelTemplateBalanceSheet": "ExcelTemplateBalanceSheet"
				}
			}
		},
		"ExcelTemplateCashFlowStatement": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"ExcelTemplateCashFlowStatement": {
					"Key": "string",
					"dateFilter": "date",
					"display": "string",
					"indentation": "int",
					"lineNumber": "int",
					"lineType": "string",
					"netChange": "decimal"
				},
				"ExcelTemplateCashFlowStatement_Fields": "string",
				"ExcelTemplateCashFlowStatement_Filter": {
					"Criteria": "string",
					"Field": "ExcelTemplateCashFlowStatement_Fields"
				},
				"ExcelTemplateCashFlowStatement_List": {
					"ExcelTemplateCashFlowStatement": "ExcelTemplateCashFlowStatement"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"lineNumber": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"ExcelTemplateCashFlowStatement": "ExcelTemplateCashFlowStatement"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "ExcelTemplateCashFlowStatement_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "ExcelTemplateCashFlowStatement_List"
				},
				"Read_Result": {
					"ExcelTemplateCashFlowStatement": "ExcelTemplateCashFlowStatement"
				}
			}
		},
		"ExcelTemplateIncomeStatement": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"ExcelTemplateIncomeStatement": {
					"Key": "string",
					"dateFilter": "date",
					"display": "string",
					"indentation": "int",
					"lineNumber": "int",
					"lineType": "string",
					"netChange": "decimal"
				},
				"ExcelTemplateIncomeStatement_Fields": "string",
				"ExcelTemplateIncomeStatement_Filter": {
					"Criteria": "string",
					"Field": "ExcelTemplateIncomeStatement_Fields"
				},
				"ExcelTemplateIncomeStatement_List": {
					"ExcelTemplateIncomeStatement": "ExcelTemplateIncomeStatement"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"lineNumber": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"ExcelTemplateIncomeStatement": "ExcelTemplateIncomeStatement"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "ExcelTemplateIncomeStatement_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "ExcelTemplateIncomeStatement_List"
				},
				"Read_Result": {
					"ExcelTemplateIncomeStatement": "ExcelTemplateIncomeStatement"
				}
			}
		},
		"ExcelTemplateRetainedEarnings": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"ExcelTemplateRetainedEarnings": {
					"Key": "string",
					"dateFilter": "date",
					"display": "string",
					"indentation": "int",
					"lineNumber": "int",
					"lineType": "string",
					"netChange": "decimal"
				},
				"ExcelTemplateRetainedEarnings_Fields": "string",
				"ExcelTemplateRetainedEarnings_Filter": {
					"Criteria": "string",
					"Field": "ExcelTemplateRetainedEarnings_Fields"
				},
				"ExcelTemplateRetainedEarnings_List": {
					"ExcelTemplateRetainedEarnings": "ExcelTemplateRetainedEarnings"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"lineNumber": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"ExcelTemplateRetainedEarnings": "ExcelTemplateRetainedEarnings"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "ExcelTemplateRetainedEarnings_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "ExcelTemplateRetainedEarnings_List"
				},
				"Read_Result": {
					"ExcelTemplateRetainedEarnings": "ExcelTemplateRetainedEarnings"
				}
			}
		},
		"ExcelTemplateTrialBalance": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"ExcelTemplateTrialBalance": {
					"Key": "string",
					"accountId": "string",
					"accountType": "accountType",
					"balanceAtDateCredit": "string",
					"balanceAtDateDebit": "string",
					"dateFilter": "date",
					"display": "string",
					"number": "string",
					"totalCredit": "string",
					"totalDebit": "string"
				},
				"ExcelTemplateTrialBalance_Fields": "string",
				"ExcelTemplateTrialBalance_Filter": {
					"Criteria": "string",
					"Field": "ExcelTemplateTrialBalance_Fields"
				},
				"ExcelTemplateTrialBalance_List": {
					"ExcelTemplateTrialBalance": "ExcelTemplateTrialBalance"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"ExcelTemplateTrialBalance": "ExcelTemplateTrialBalance"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "ExcelTemplateTrialBalance_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "ExcelTemplateTrialBalance_List"
				},
				"Read_Result": {
					"ExcelTemplateTrialBalance": "ExcelTemplateTrialBalance"
				},
				"accountType": "string"
			}
		},
		"ExcelTemplateViewCompanyInformation": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"ExcelTemplateViewCompanyInformation": {
					"Currency": "string",
					"DisplayName": "string",
					"Key": "string"
				},
				"ExcelTemplateViewCompanyInformation_Fields": "string",
				"ExcelTemplateViewCompanyInformation_Filter": {
					"Criteria": "string",
					"Field": "ExcelTemplateViewCompanyInformation_Fields"
				},
				"ExcelTemplateViewCompanyInformation_List": {
					"ExcelTemplateViewCompanyInformation": "ExcelTemplateViewCompanyInformation"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"ExcelTemplateViewCompanyInformation": "ExcelTemplateViewCompanyInformation"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "ExcelTemplateViewCompanyInformation_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "ExcelTemplateViewCompanyInformation_List"
				}
			}
		},
		"ExchangeServiceSetup": {
			"f": {
				"Store": {
					"i": "Store",
					"o": "Store_Result"
				}
			},
			"sp": "Codeunit",
			"t": {
				"Store": {
					"authenticationEndpoint": "string",
					"certificateThumbprint": "string",
					"clientID": "string",
					"exchangeEndpoint": "string",
					"exchangeResourceUri": "string"
				},
				"Store_Result": {
					"": ""
				}
			}
		},
		"InvoiceDocument": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"InvoiceDocument": {
					"InvoiceId": "string",
					"Key": "string"
				},
				"InvoiceDocument_Fields": "string",
				"InvoiceDocument_Filter": {
					"Criteria": "string",
					"Field": "InvoiceDocument_Fields"
				},
				"InvoiceDocument_List": {
					"InvoiceDocument": "InvoiceDocument"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"InvoiceId": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"InvoiceDocument": "InvoiceDocument"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "InvoiceDocument_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "InvoiceDocument_List"
				},
				"Read_Result": {
					"InvoiceDocument": "InvoiceDocument"
				}
			}
		},
		"InvoiceReminder": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"InvoiceReminder": "InvoiceReminder"
				},
				"CreateMultiple": {
					"InvoiceReminder_List": "InvoiceReminder_List"
				},
				"CreateMultiple_Result": {
					"InvoiceReminder_List": "InvoiceReminder_List"
				},
				"Create_Result": {
					"InvoiceReminder": "InvoiceReminder"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"InvoiceReminder": {
					"InvoiceId": "string",
					"Key": "string",
					"Message": "string"
				},
				"InvoiceReminder_Fields": "string",
				"InvoiceReminder_Filter": {
					"Criteria": "string",
					"Field": "InvoiceReminder_Fields"
				},
				"InvoiceReminder_List": {
					"InvoiceReminder": "InvoiceReminder"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"InvoiceId": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"InvoiceReminder": "InvoiceReminder"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "InvoiceReminder_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "InvoiceReminder_List"
				},
				"Read_Result": {
					"InvoiceReminder": "InvoiceReminder"
				}
			}
		},
		"Job_List": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Job_List": {
					"Bill_to_Customer_No": "string",
					"Description": "string",
					"Job_Posting_Group": "string",
					"Key": "string",
					"Next_Invoice_Date": "date",
					"No": "string",
					"Percent_Completed": "decimal",
					"Percent_Invoiced": "decimal",
					"Percent_of_Overdue_Planning_Lines": "decimal",
					"Person_Responsible": "string",
					"Project_Manager": "string",
					"Search_Description": "string",
					"Status": "Status"
				},
				"Job_List_Fields": "string",
				"Job_List_Filter": {
					"Criteria": "string",
					"Field": "Job_List_Fields"
				},
				"Job_List_List": {
					"Job_List": "Job_List"
				},
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"Job_List": "Job_List"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "Job_List_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "Job_List_List"
				},
				"Read_Result": {
					"Job_List": "Job_List"
				},
				"Status": "string"
			}
		},
		"Job_Planning_Lines": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"Job_Planning_Lines": "Job_Planning_Lines"
				},
				"CreateMultiple": {
					"Job_Planning_Lines_List": "Job_Planning_Lines_List"
				},
				"CreateMultiple_Result": {
					"Job_Planning_Lines_List": "Job_Planning_Lines_List"
				},
				"Create_Result": {
					"Job_Planning_Lines": "Job_Planning_Lines"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Job_Planning_Lines": {
					"Currency_Date": "date",
					"Description": "string",
					"Direct_Unit_Cost_LCY": "decimal",
					"Document_No": "string",
					"Gen_Bus_Posting_Group": "string",
					"Gen_Prod_Posting_Group": "string",
					"Invoiced_Amount_LCY": "decimal",
					"Invoiced_Cost_Amount_LCY": "decimal",
					"Job_Contract_Entry_No": "int",
					"Job_No": "string",
					"Job_Task_No": "string",
					"Key": "string",
					"Ledger_Entry_No": "int",
					"Ledger_Entry_Type": "Ledger_Entry_Type",
					"Line_Amount": "decimal",
					"Line_Amount_LCY": "decimal",
					"Line_Discount_Amount": "decimal",
					"Line_Discount_Percent": "decimal",
					"Line_No": "int",
					"Line_Type": "Line_Type",
					"Location_Code": "string",
					"Lot_No": "string",
					"No": "string",
					"Overdue": "boolean",
					"Planned_Delivery_Date": "date",
					"Planning_Date": "date",
					"Posted_Line_Amount": "decimal",
					"Posted_Line_Amount_LCY": "decimal",
					"Posted_Total_Cost": "decimal",
					"Posted_Total_Cost_LCY": "decimal",
					"Qty_Invoiced": "decimal",
					"Qty_Posted": "decimal",
					"Qty_Transferred_to_Invoice": "decimal",
					"Qty_to_Invoice": "decimal",
					"Qty_to_Transfer_to_Invoice": "decimal",
					"Qty_to_Transfer_to_Journal": "decimal",
					"Quantity": "decimal",
					"Quantity_Base": "decimal",
					"Remaining_Line_Amount": "decimal",
					"Remaining_Line_Amount_LCY": "decimal",
					"Remaining_Qty": "decimal",
					"Remaining_Total_Cost": "decimal",
					"Remaining_Total_Cost_LCY": "decimal",
					"Reserve": "Reserve",
					"Reserved_Quantity": "decimal",
					"Serial_No": "string",
					"System_Created_Entry": "boolean",
					"Total_Cost": "decimal",
					"Total_Cost_LCY": "decimal",
					"Total_Price": "decimal",
					"Total_Price_LCY": "decimal",
					"Type": "Type",
					"Unit_Cost": "decimal",
					"Unit_Cost_LCY": "decimal",
					"Unit_Price": "decimal",
					"Unit_Price_LCY": "decimal",
					"Unit_of_Measure_Code": "string",
					"Usage_Link": "boolean",
					"User_ID": "string",
					"Variant_Code": "string",
					"Work_Type_Code": "string"
				},
				"Job_Planning_Lines_Fields": "string",
				"Job_Planning_Lines_Filter": {
					"Criteria": "string",
					"Field": "Job_Planning_Lines_Fields"
				},
				"Job_Planning_Lines_List": {
					"Job_Planning_Lines": "Job_Planning_Lines"
				},
				"Ledger_Entry_Type": "string",
				"Line_Type": "string",
				"Read": {
					"Job_No": "string",
					"Job_Task_No": "string",
					"Line_No": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"Job_Planning_Lines": "Job_Planning_Lines"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "Job_Planning_Lines_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "Job_Planning_Lines_List"
				},
				"Read_Result": {
					"Job_Planning_Lines": "Job_Planning_Lines"
				},
				"Reserve": "string",
				"Type": "string",
				"Update": {
					"Job_Planning_Lines": "Job_Planning_Lines"
				},
				"UpdateMultiple": {
					"Job_Planning_Lines_List": "Job_Planning_Lines_List"
				},
				"UpdateMultiple_Result": {
					"Job_Planning_Lines_List": "Job_Planning_Lines_List"
				},
				"Update_Result": {
					"Job_Planning_Lines": "Job_Planning_Lines"
				}
			}
		},
		"Job_Task_Lines": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"Job_Task_Lines": "Job_Task_Lines"
				},
				"CreateMultiple": {
					"Job_Task_Lines_List": "Job_Task_Lines_List"
				},
				"CreateMultiple_Result": {
					"Job_Task_Lines_List": "Job_Task_Lines_List"
				},
				"Create_Result": {
					"Job_Task_Lines": "Job_Task_Lines"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Job_Task_Lines": {
					"Amt_Rcd_Not_Invoiced": "decimal",
					"Contract_Invoiced_Cost": "decimal",
					"Contract_Invoiced_Price": "decimal",
					"Contract_Total_Cost": "decimal",
					"Contract_Total_Price": "decimal",
					"Description": "string",
					"EAC_Total_Cost": "decimal",
					"EAC_Total_Price": "decimal",
					"End_Date": "date",
					"Global_Dimension_1_Code": "string",
					"Global_Dimension_2_Code": "string",
					"Job_No": "string",
					"Job_Posting_Group": "string",
					"Job_Task_No": "string",
					"Job_Task_Type": "Job_Task_Type",
					"Key": "string",
					"Outstanding_Orders": "decimal",
					"Remaining_Total_Cost": "decimal",
					"Remaining_Total_Price": "decimal",
					"Schedule_Total_Cost": "decimal",
					"Schedule_Total_Price": "decimal",
					"Start_Date": "date",
					"Totaling": "string",
					"Usage_Total_Cost": "decimal",
					"Usage_Total_Price": "decimal",
					"WIP_Method": "string",
					"WIP_Total": "WIP_Total"
				},
				"Job_Task_Lines_Fields": "string",
				"Job_Task_Lines_Filter": {
					"Criteria": "string",
					"Field": "Job_Task_Lines_Fields"
				},
				"Job_Task_Lines_List": {
					"Job_Task_Lines": "Job_Task_Lines"
				},
				"Job_Task_Type": "string",
				"Read": {
					"Job_No": "string",
					"Job_Task_No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"Job_Task_Lines": "Job_Task_Lines"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "Job_Task_Lines_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "Job_Task_Lines_List"
				},
				"Read_Result": {
					"Job_Task_Lines": "Job_Task_Lines"
				},
				"Update": {
					"Job_Task_Lines": "Job_Task_Lines"
				},
				"UpdateMultiple": {
					"Job_Task_Lines_List": "Job_Task_Lines_List"
				},
				"UpdateMultiple_Result": {
					"Job_Task_Lines_List": "Job_Task_Lines_List"
				},
				"Update_Result": {
					"Job_Task_Lines": "Job_Task_Lines"
				},
				"WIP_Total": "string"
			}
		},
		"OK365_Apply_Customer": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Document_Type": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Apply_Customer": {
					"Amount": "decimal",
					"AmountToApply": "decimal",
					"Amount_to_Apply": "decimal",
					"AppliedAmount": "decimal",
					"AppliesToID": "string",
					"ApplnAmountToApply": "decimal",
					"ApplnCurrencyCode": "string",
					"ApplnRounding": "decimal",
					"ApplyingAmount": "decimal",
					"ApplyingCustLedgEntry_Amount": "decimal",
					"ApplyingCustLedgEntry_Currency_Code": "string",
					"ApplyingCustLedgEntry_Document_No": "string",
					"ApplyingCustLedgEntry_Posting_Date": "date",
					"ApplyingCustLedgEntry_Remaining_Amount": "decimal",
					"ApplyingCustomerName": "string",
					"ApplyingCustomerNo": "string",
					"ApplyingDescription": "string",
					"CalcApplnRemainingAmount_Remaining_Amount": "decimal",
					"CalcApplnRemainingAmount_Remaining_Pmt_Disc_Possible": "decimal",
					"ControlBalance": "decimal",
					"Credit_Amount": "decimal",
					"Currency_Code": "string",
					"Customer_Name": "string",
					"Customer_No": "string",
					"Debit_Amount": "decimal",
					"Description": "string",
					"Document_No": "string",
					"Document_Type": "Document_Type",
					"Due_Date": "date",
					"Global_Dimension_1_Code": "string",
					"Global_Dimension_2_Code": "string",
					"Key": "string",
					"Max_Payment_Tolerance": "decimal",
					"Open": "boolean",
					"Original_Amount": "decimal",
					"Original_Pmt_Disc_Possible": "decimal",
					"PmtDiscountAmount": "decimal",
					"Pmt_Disc_Tolerance_Date": "date",
					"Pmt_Discount_Date": "date",
					"Positive": "boolean",
					"Posting_Date": "date",
					"Remaining_Amount": "decimal",
					"Remaining_Pmt_Disc_Possible": "decimal",
					"Select_for_Payment_OkFldSls": "boolean"
				},
				"OK365_Apply_Customer_Fields": "string",
				"OK365_Apply_Customer_Filter": {
					"Criteria": "string",
					"Field": "OK365_Apply_Customer_Fields"
				},
				"OK365_Apply_Customer_List": {
					"OK365_Apply_Customer": "OK365_Apply_Customer"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Apply_Customer": "OK365_Apply_Customer"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Apply_Customer_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Apply_Customer_List"
				},
				"Update": {
					"OK365_Apply_Customer": "OK365_Apply_Customer"
				},
				"UpdateMultiple": {
					"OK365_Apply_Customer_List": "OK365_Apply_Customer_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Apply_Customer_List": "OK365_Apply_Customer_List"
				},
				"Update_Result": {
					"OK365_Apply_Customer": "OK365_Apply_Customer"
				}
			}
		},
		"OK365_CashReceipt": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Account_Type": "string",
				"Applies_to_Doc_Type": "string",
				"Create": {
					"OK365_CashReceipt": "OK365_CashReceipt"
				},
				"CreateMultiple": {
					"OK365_CashReceipt_List": "OK365_CashReceipt_List"
				},
				"CreateMultiple_Result": {
					"OK365_CashReceipt_List": "OK365_CashReceipt_List"
				},
				"Create_Result": {
					"OK365_CashReceipt": "OK365_CashReceipt"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Document_Type": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_CashReceipt": {
					"Account_No": "string",
					"Account_Type": "Account_Type",
					"Amount": "decimal",
					"Applies_to_Doc_No": "string",
					"Applies_to_Doc_Type": "Applies_to_Doc_Type",
					"Document_No": "string",
					"Document_Type": "Document_Type",
					"External_Document_No": "string",
					"GeneralBatchName": "string",
					"GeneralJournalTemplate": "string",
					"Key": "string",
					"Line_No": "int",
					"Mobile_User_ID": "string",
					"Payment_Method_Code": "string",
					"Posting_Date": "date",
					"Route_Code": "string"
				},
				"OK365_CashReceipt_Fields": "string",
				"OK365_CashReceipt_Filter": {
					"Criteria": "string",
					"Field": "OK365_CashReceipt_Fields"
				},
				"OK365_CashReceipt_List": {
					"OK365_CashReceipt": "OK365_CashReceipt"
				},
				"Read": {
					"Line_No": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_CashReceipt": "OK365_CashReceipt"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_CashReceipt_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_CashReceipt_List"
				},
				"Read_Result": {
					"OK365_CashReceipt": "OK365_CashReceipt"
				},
				"Update": {
					"OK365_CashReceipt": "OK365_CashReceipt"
				},
				"UpdateMultiple": {
					"OK365_CashReceipt_List": "OK365_CashReceipt_List"
				},
				"UpdateMultiple_Result": {
					"OK365_CashReceipt_List": "OK365_CashReceipt_List"
				},
				"Update_Result": {
					"OK365_CashReceipt": "OK365_CashReceipt"
				}
			}
		},
		"OK365_Cash_Receipt_1": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Account_Type": "string",
				"Applies_to_Doc_Type": "string",
				"Bal_Account_Type": "string",
				"Bal_Gen_Posting_Type": "string",
				"Create": {
					"CurrentJnlBatchName": "string",
					"OK365_Cash_Receipt_1": "OK365_Cash_Receipt_1"
				},
				"CreateMultiple": {
					"CurrentJnlBatchName": "string",
					"OK365_Cash_Receipt_1_List": "OK365_Cash_Receipt_1_List"
				},
				"CreateMultiple_Result": {
					"OK365_Cash_Receipt_1_List": "OK365_Cash_Receipt_1_List"
				},
				"Create_Result": {
					"OK365_Cash_Receipt_1": "OK365_Cash_Receipt_1"
				},
				"Delete": {
					"CurrentJnlBatchName": "string",
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Document_Type": "string",
				"Gen_Posting_Type": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Cash_Receipt_1": {
					"AccName": "string",
					"Account_No": "string",
					"Account_Type": "Account_Type",
					"Amount": "decimal",
					"Amount_LCY": "decimal",
					"Applied_Yes_No": "boolean",
					"Applies_to_Doc_No": "string",
					"Applies_to_Doc_Type": "Applies_to_Doc_Type",
					"Applies_to_ID": "string",
					"BalAccName": "string",
					"Bal_Account_No": "string",
					"Bal_Account_Type": "Bal_Account_Type",
					"Bal_Gen_Bus_Posting_Group": "string",
					"Bal_Gen_Posting_Type": "Bal_Gen_Posting_Type",
					"Bal_Gen_Prod_Posting_Group": "string",
					"Bal_VAT_Amount": "decimal",
					"Bal_VAT_Bus_Posting_Group": "string",
					"Bal_VAT_Difference": "decimal",
					"Bal_VAT_Prod_Posting_Group": "string",
					"Balance": "decimal",
					"Campaign_No": "string",
					"Comment": "string",
					"Correction": "boolean",
					"Credit_Amount": "decimal",
					"Currency_Code": "string",
					"Debit_Amount": "decimal",
					"Description": "string",
					"Direct_Debit_Mandate_ID": "string",
					"Document_Date": "date",
					"Document_No": "string",
					"Document_Type": "Document_Type",
					"External_Document_No": "string",
					"Gen_Bus_Posting_Group": "string",
					"Gen_Posting_Type": "Gen_Posting_Type",
					"Gen_Prod_Posting_Group": "string",
					"Incoming_Document_Entry_No": "int",
					"Key": "string",
					"Mobile_User_ID_OkFldSls": "string",
					"Payment_Method_Code_OkFldSls": "string",
					"Posting_Date": "date",
					"Reason_Code": "string",
					"Route_Code_OkFldSls": "string",
					"Salespers_Purch_Code": "string",
					"ShortcutDimCode3": "string",
					"ShortcutDimCode4": "string",
					"ShortcutDimCode5": "string",
					"ShortcutDimCode6": "string",
					"ShortcutDimCode7": "string",
					"ShortcutDimCode8": "string",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"TotalBalance": "decimal",
					"VAT_Amount": "decimal",
					"VAT_Bus_Posting_Group": "string",
					"VAT_Difference": "decimal",
					"VAT_Prod_Posting_Group": "string"
				},
				"OK365_Cash_Receipt_1_Fields": "string",
				"OK365_Cash_Receipt_1_Filter": {
					"Criteria": "string",
					"Field": "OK365_Cash_Receipt_1_Fields"
				},
				"OK365_Cash_Receipt_1_List": {
					"OK365_Cash_Receipt_1": "OK365_Cash_Receipt_1"
				},
				"ReadByRecId": {
					"CurrentJnlBatchName": "string",
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Cash_Receipt_1": "OK365_Cash_Receipt_1"
				},
				"ReadMultiple": {
					"CurrentJnlBatchName": "string",
					"bookmarkKey": "string",
					"filter": "OK365_Cash_Receipt_1_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Cash_Receipt_1_List"
				},
				"Update": {
					"CurrentJnlBatchName": "string",
					"OK365_Cash_Receipt_1": "OK365_Cash_Receipt_1"
				},
				"UpdateMultiple": {
					"CurrentJnlBatchName": "string",
					"OK365_Cash_Receipt_1_List": "OK365_Cash_Receipt_1_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Cash_Receipt_1_List": "OK365_Cash_Receipt_1_List"
				},
				"Update_Result": {
					"OK365_Cash_Receipt_1": "OK365_Cash_Receipt_1"
				}
			}
		},
		"OK365_Company_Information": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Check_Avail_Time_Bucket": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IC_Inbox_Type": "string",
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Company_Information": {
					"Address": "string",
					"Address_2": "string",
					"Allow_Blank_Payment_Info": "boolean",
					"Auto_Send_Transactions": "boolean",
					"BankAccountPostingGroup": "string",
					"Bank_Account_No": "string",
					"Bank_Branch_No": "string",
					"Bank_Name": "string",
					"Base_Calendar_Code": "string",
					"Cal_Convergence_Time_Frame": "string",
					"Check_Avail_Period_Calc": "string",
					"Check_Avail_Time_Bucket": "Check_Avail_Time_Bucket",
					"City": "string",
					"Contact_Person": "string",
					"Country_Region_Code": "string",
					"County": "string",
					"Customized_Calendar": "string",
					"E_Mail": "string",
					"Experience": "string",
					"Fax_No": "string",
					"GLN": "string",
					"Giro_No": "string",
					"Home_Page": "string",
					"IBAN": "string",
					"IC_Inbox_Details": "string",
					"IC_Inbox_Type": "IC_Inbox_Type",
					"IC_Partner_Code": "string",
					"Industrial_Classification": "string",
					"Key": "string",
					"Location_Code": "string",
					"Name": "string",
					"Payment_Routing_No": "string",
					"Phone_No": "string",
					"Post_Code": "string",
					"Responsibility_Center": "string",
					"SWIFT_Code": "string",
					"Ship_to_Address": "string",
					"Ship_to_Address_2": "string",
					"Ship_to_City": "string",
					"Ship_to_Contact": "string",
					"Ship_to_Country_Region_Code": "string",
					"Ship_to_County": "string",
					"Ship_to_Name": "string",
					"Ship_to_Post_Code": "string",
					"System_Indicator": "System_Indicator",
					"System_Indicator_Style": "System_Indicator_Style",
					"System_Indicator_Text": "string",
					"VAT_Registration_No": "string"
				},
				"OK365_Company_Information_Fields": "string",
				"OK365_Company_Information_Filter": {
					"Criteria": "string",
					"Field": "OK365_Company_Information_Fields"
				},
				"OK365_Company_Information_List": {
					"OK365_Company_Information": "OK365_Company_Information"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Company_Information": "OK365_Company_Information"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Company_Information_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Company_Information_List"
				},
				"System_Indicator": "string",
				"System_Indicator_Style": "string",
				"Update": {
					"OK365_Company_Information": "OK365_Company_Information"
				},
				"UpdateMultiple": {
					"OK365_Company_Information_List": "OK365_Company_Information_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Company_Information_List": "OK365_Company_Information_List"
				},
				"Update_Result": {
					"OK365_Company_Information": "OK365_Company_Information"
				}
			}
		},
		"OK365_Contact_Card": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"Delete_Contact_Card_Line": {
					"i": "Delete_Contact_Card_Line",
					"o": "Delete_Contact_Card_Line_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Answer_Priority": "string",
				"Contact_Card_Line": {
					"Answer": "string",
					"Answer_Priority": "Answer_Priority",
					"Key": "string",
					"Last_Date_Updated": "date",
					"Profile_Questionnaire_Priority": "Profile_Questionnaire_Priority",
					"Question": "string",
					"Questions_Answered_Percent": "decimal"
				},
				"Contact_Card_Line_List": {
					"Contact_Card_Line": "Contact_Card_Line"
				},
				"Correspondence_Type": "string",
				"Create": {
					"OK365_Contact_Card": "OK365_Contact_Card"
				},
				"CreateMultiple": {
					"OK365_Contact_Card_List": "OK365_Contact_Card_List"
				},
				"CreateMultiple_Result": {
					"OK365_Contact_Card_List": "OK365_Contact_Card_List"
				},
				"Create_Result": {
					"OK365_Contact_Card": "OK365_Contact_Card"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Contact_Card_Line": {
					"Contact_Card_Line_Key": "string"
				},
				"Delete_Contact_Card_Line_Result": {
					"Delete_Contact_Card_Line_Result": "boolean"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Contact_Card": {
					"Address": "string",
					"Address_2": "string",
					"City": "string",
					"Company_Name": "string",
					"Company_No": "string",
					"Contact_Card_Line": "Contact_Card_Line_List",
					"Correspondence_Type": "Correspondence_Type",
					"Country_Region_Code": "string",
					"Currency_Code": "string",
					"Date_of_Last_Interaction": "date",
					"E_Mail": "string",
					"Exclude_from_Segment": "boolean",
					"Fax_No": "string",
					"Home_Page": "string",
					"IntegrationCustomerNo": "string",
					"Key": "string",
					"Language_Code": "string",
					"Last_Date_Attempted": "date",
					"Last_Date_Modified": "date",
					"Minor": "boolean",
					"Mobile_Phone_No": "string",
					"Name": "string",
					"Next_Task_Date": "date",
					"No": "string",
					"Organizational_Level_Code": "string",
					"Parental_Consent_Received": "boolean",
					"Phone_No": "string",
					"Post_Code": "string",
					"Privacy_Blocked": "boolean",
					"Salesperson_Code": "string",
					"Salutation_Code": "string",
					"Search_Name": "string",
					"ShowMap": "string",
					"Territory_Code": "string",
					"Type": "Type",
					"VAT_Registration_No": "string"
				},
				"OK365_Contact_Card_Fields": "string",
				"OK365_Contact_Card_Filter": {
					"Criteria": "string",
					"Field": "OK365_Contact_Card_Fields"
				},
				"OK365_Contact_Card_List": {
					"OK365_Contact_Card": "OK365_Contact_Card"
				},
				"Profile_Questionnaire_Priority": "string",
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Contact_Card": "OK365_Contact_Card"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Contact_Card_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Contact_Card_List"
				},
				"Read_Result": {
					"OK365_Contact_Card": "OK365_Contact_Card"
				},
				"Type": "string",
				"Update": {
					"OK365_Contact_Card": "OK365_Contact_Card"
				},
				"UpdateMultiple": {
					"OK365_Contact_Card_List": "OK365_Contact_Card_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Contact_Card_List": "OK365_Contact_Card_List"
				},
				"Update_Result": {
					"OK365_Contact_Card": "OK365_Contact_Card"
				}
			}
		},
		"OK365_Contacts": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Contacts": {
					"Company_Name": "string",
					"Country_Region_Code": "string",
					"Currency_Code": "string",
					"E_Mail": "string",
					"Fax_No": "string",
					"Key": "string",
					"Language_Code": "string",
					"Minor": "boolean",
					"Mobile_Phone_No": "string",
					"Name": "string",
					"No": "string",
					"Parental_Consent_Received": "boolean",
					"Phone_No": "string",
					"Post_Code": "string",
					"Privacy_Blocked": "boolean",
					"Salesperson_Code": "string",
					"Search_Name": "string",
					"Territory_Code": "string"
				},
				"OK365_Contacts_Fields": "string",
				"OK365_Contacts_Filter": {
					"Criteria": "string",
					"Field": "OK365_Contacts_Fields"
				},
				"OK365_Contacts_List": {
					"OK365_Contacts": "OK365_Contacts"
				},
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Contacts": "OK365_Contacts"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Contacts_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Contacts_List"
				},
				"Read_Result": {
					"OK365_Contacts": "OK365_Contacts"
				}
			}
		},
		"OK365_Currencies": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"OK365_Currencies": "OK365_Currencies"
				},
				"CreateMultiple": {
					"OK365_Currencies_List": "OK365_Currencies_List"
				},
				"CreateMultiple_Result": {
					"OK365_Currencies_List": "OK365_Currencies_List"
				},
				"Create_Result": {
					"OK365_Currencies": "OK365_Currencies"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"Invoice_Rounding_Type": "string",
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Currencies": {
					"Amount_Decimal_Places": "string",
					"Amount_Rounding_Precision": "decimal",
					"Appln_Rounding_Precision": "decimal",
					"Code": "string",
					"Conv_LCY_Rndg_Credit_Acc": "string",
					"Conv_LCY_Rndg_Debit_Acc": "string",
					"CurrencyFactor": "decimal",
					"Description": "string",
					"EMU_Currency": "boolean",
					"ExchangeRateAmt": "decimal",
					"ExchangeRateDate": "date",
					"ISO_Code": "string",
					"ISO_Numeric_Code": "string",
					"Invoice_Rounding_Precision": "decimal",
					"Invoice_Rounding_Type": "Invoice_Rounding_Type",
					"Key": "string",
					"Last_Date_Adjusted": "date",
					"Last_Date_Modified": "date",
					"Max_Payment_Tolerance_Amount": "decimal",
					"Max_VAT_Difference_Allowed": "decimal",
					"Payment_Tolerance_Percent": "decimal",
					"Realized_G_L_Gains_Account": "string",
					"Realized_G_L_Losses_Account": "string",
					"Realized_Gains_Acc": "string",
					"Realized_Losses_Acc": "string",
					"Residual_Gains_Account": "string",
					"Residual_Losses_Account": "string",
					"Unit_Amount_Decimal_Places": "string",
					"Unit_Amount_Rounding_Precision": "decimal",
					"Unrealized_Gains_Acc": "string",
					"Unrealized_Losses_Acc": "string",
					"VAT_Rounding_Type": "VAT_Rounding_Type"
				},
				"OK365_Currencies_Fields": "string",
				"OK365_Currencies_Filter": {
					"Criteria": "string",
					"Field": "OK365_Currencies_Fields"
				},
				"OK365_Currencies_List": {
					"OK365_Currencies": "OK365_Currencies"
				},
				"Read": {
					"Code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Currencies": "OK365_Currencies"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Currencies_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Currencies_List"
				},
				"Read_Result": {
					"OK365_Currencies": "OK365_Currencies"
				},
				"Update": {
					"OK365_Currencies": "OK365_Currencies"
				},
				"UpdateMultiple": {
					"OK365_Currencies_List": "OK365_Currencies_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Currencies_List": "OK365_Currencies_List"
				},
				"Update_Result": {
					"OK365_Currencies": "OK365_Currencies"
				},
				"VAT_Rounding_Type": "string"
			}
		},
		"OK365_Customer_Card": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"Delete_AgedAccReceivableChart": {
					"i": "Delete_AgedAccReceivableChart",
					"o": "Delete_AgedAccReceivableChart_Result"
				},
				"Delete_PriceAndLineDisc": {
					"i": "Delete_PriceAndLineDisc",
					"o": "Delete_PriceAndLineDisc_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Aged_Acc_Receivable_Chart": {
					"Key": "string"
				},
				"Aged_Acc_Receivable_Chart_List": {
					"Aged_Acc_Receivable_Chart": "Aged_Acc_Receivable_Chart"
				},
				"Application_Method": "string",
				"Blocked": "string",
				"Copy_Sell_to_Addr_to_Qte_From": "string",
				"Create": {
					"OK365_Customer_Card": "OK365_Customer_Card"
				},
				"CreateMultiple": {
					"OK365_Customer_Card_List": "OK365_Customer_Card_List"
				},
				"CreateMultiple_Result": {
					"OK365_Customer_Card_List": "OK365_Customer_Card_List"
				},
				"Create_Result": {
					"OK365_Customer_Card": "OK365_Customer_Card"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_AgedAccReceivableChart": {
					"AgedAccReceivableChart_Key": "string"
				},
				"Delete_AgedAccReceivableChart_Result": {
					"Delete_AgedAccReceivableChart_Result": "boolean"
				},
				"Delete_PriceAndLineDisc": {
					"PriceAndLineDisc_Key": "string"
				},
				"Delete_PriceAndLineDisc_Result": {
					"Delete_PriceAndLineDisc_Result": "boolean"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Line_Type": "string",
				"OK365_Customer_Card": {
					"Address": "string",
					"Address_2": "string",
					"AdjCustProfit": "decimal",
					"AdjProfitPct": "decimal",
					"AgedAccReceivableChart": "Aged_Acc_Receivable_Chart_List",
					"Allow_Line_Disc": "boolean",
					"Application_Method": "Application_Method",
					"Balance_Due": "decimal",
					"Balance_Due_LCY": "decimal",
					"Balance_LCY": "decimal",
					"Base_Calendar_Code": "string",
					"Bill_to_Customer_No": "string",
					"Block_Payment_Tolerance": "boolean",
					"Blocked": "Blocked",
					"CalcCreditLimitLCYExpendedPct": "decimal",
					"Cash_Flow_Payment_Terms_Code": "string",
					"City": "string",
					"Combine_Shipments": "boolean",
					"ContactName": "string",
					"Copy_Sell_to_Addr_to_Qte_From": "Copy_Sell_to_Addr_to_Qte_From",
					"Country_Region_Code": "string",
					"County": "string",
					"Credit_Limit_LCY": "decimal",
					"Currency_Code": "string",
					"CustInvDiscAmountLCY": "decimal",
					"CustSalesLCY_CustProfit_AdjmtCostLCY": "decimal",
					"CustomerMgt_AvgDaysToPay_No": "decimal",
					"Customer_Disc_Group": "string",
					"Customer_Posting_Group": "string",
					"Customer_Price_Group": "string",
					"Customized_Calendar": "string",
					"DaysPaidPastDueDate": "decimal",
					"Disable_Search_by_Name": "boolean",
					"Document_Sending_Profile": "string",
					"E_Mail": "string",
					"ExpectedCustMoneyOwed": "decimal",
					"Fax_No": "string",
					"Fin_Charge_Terms_Code": "string",
					"GLN": "string",
					"Gen_Bus_Posting_Group": "string",
					"GetAmountOnCrMemo": "decimal",
					"GetAmountOnOutstandingCrMemos": "decimal",
					"GetAmountOnOutstandingInvoices": "decimal",
					"GetAmountOnPostedInvoices": "decimal",
					"Home_Page": "string",
					"IC_Partner_Code": "string",
					"Invoice_Copies": "int",
					"Invoice_Disc_Code": "string",
					"Key": "string",
					"Language_Code": "string",
					"Last_Date_Modified": "date",
					"Last_Statement_No": "int",
					"Location_Code": "string",
					"Name": "string",
					"No": "string",
					"Partner_Type": "Partner_Type",
					"Payment_Method_Code": "string",
					"Payment_Terms_Code": "string",
					"Payments_LCY": "decimal",
					"Phone_No": "string",
					"Post_Code": "string",
					"Preferred_Bank_Account_Code": "string",
					"Prepayment_Percent": "decimal",
					"PriceAndLineDisc": "Sales_Pr__x0026__Line_Disc_Part_List",
					"Prices_Including_VAT": "boolean",
					"Primary_Contact_No": "string",
					"Print_Statements": "boolean",
					"Privacy_Blocked": "boolean",
					"Reminder_Terms_Code": "string",
					"Reserve": "Reserve",
					"Responsibility_Center": "string",
					"Salesperson_Code": "string",
					"Search_Name": "string",
					"Service_Zone_Code": "string",
					"Ship_to_Code": "string",
					"Shipment_Method_Code": "string",
					"Shipping_Advice": "Shipping_Advice",
					"Shipping_Agent_Code": "string",
					"Shipping_Agent_Service_Code": "string",
					"Shipping_Time": "string",
					"ShowMap": "string",
					"TotalMoneyOwed": "decimal",
					"TotalSales2": "decimal",
					"Totals": "decimal",
					"VAT_Bus_Posting_Group": "string",
					"VAT_Registration_No": "string"
				},
				"OK365_Customer_Card_Fields": "string",
				"OK365_Customer_Card_Filter": {
					"Criteria": "string",
					"Field": "OK365_Customer_Card_Fields"
				},
				"OK365_Customer_Card_List": {
					"OK365_Customer_Card": "OK365_Customer_Card"
				},
				"Partner_Type": "string",
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Customer_Card": "OK365_Customer_Card"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Customer_Card_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Customer_Card_List"
				},
				"Read_Result": {
					"OK365_Customer_Card": "OK365_Customer_Card"
				},
				"Reserve": "string",
				"Sales_Pr__x0026__Line_Disc_Part": {
					"Allow_Invoice_Disc": "boolean",
					"Allow_Line_Disc": "boolean",
					"Code": "string",
					"Currency_Code": "string",
					"Ending_Date": "date",
					"Key": "string",
					"Line_Discount_Percent": "decimal",
					"Line_Type": "Line_Type",
					"Minimum_Quantity": "decimal",
					"Price_Includes_VAT": "boolean",
					"Sales_Code": "string",
					"Sales_Type": "Sales_Type",
					"Starting_Date": "date",
					"Type": "Type",
					"Unit_Price": "decimal",
					"Unit_of_Measure_Code": "string",
					"VAT_Bus_Posting_Gr_Price": "string",
					"Variant_Code": "string"
				},
				"Sales_Pr__x0026__Line_Disc_Part_List": {
					"Sales_Pr__x0026__Line_Disc_Part": "Sales_Pr__x0026__Line_Disc_Part"
				},
				"Sales_Type": "string",
				"Shipping_Advice": "string",
				"Type": "string",
				"Update": {
					"OK365_Customer_Card": "OK365_Customer_Card"
				},
				"UpdateMultiple": {
					"OK365_Customer_Card_List": "OK365_Customer_Card_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Customer_Card_List": "OK365_Customer_Card_List"
				},
				"Update_Result": {
					"OK365_Customer_Card": "OK365_Customer_Card"
				}
			}
		},
		"OK365_Customer_Ledger_Entries": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Customer_List": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Application_Method": "string",
				"Blocked": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Customer_List": {
					"Application_Method": "Application_Method",
					"Balance_Due_LCY": "decimal",
					"Balance_LCY": "decimal",
					"Base_Calendar_Code": "string",
					"Bill_to_Customer_No_OkFldSls": "string",
					"Blocked": "Blocked",
					"Combine_Shipments": "boolean",
					"Contact": "string",
					"Country_Region_Code": "string",
					"Credit_Limit_LCY": "decimal",
					"Currency_Code": "string",
					"Customer_Disc_Group": "string",
					"Customer_Posting_Group": "string",
					"Customer_Price_Group": "string",
					"Fin_Charge_Terms_Code": "string",
					"Gen_Bus_Posting_Group": "string",
					"IC_Partner_Code": "string",
					"Key": "string",
					"Language_Code": "string",
					"Last_Date_Modified": "date",
					"Location_Code": "string",
					"Name": "string",
					"No": "string",
					"Payment_Terms_Code": "string",
					"Payments_LCY": "decimal",
					"Phone_No": "string",
					"Post_Code": "string",
					"Privacy_Blocked": "boolean",
					"Reminder_Terms_Code": "string",
					"Reserve": "Reserve",
					"Responsibility_Center": "string",
					"Sales_LCY": "decimal",
					"Salesperson_Code": "string",
					"Search_Name": "string",
					"Ship_to_Code": "string",
					"Shipping_Advice": "Shipping_Advice",
					"Shipping_Agent_Code": "string",
					"VAT_Bus_Posting_Group": "string"
				},
				"OK365_Customer_List_Fields": "string",
				"OK365_Customer_List_Filter": {
					"Criteria": "string",
					"Field": "OK365_Customer_List_Fields"
				},
				"OK365_Customer_List_List": {
					"OK365_Customer_List": "OK365_Customer_List"
				},
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Customer_List": "OK365_Customer_List"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Customer_List_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Customer_List_List"
				},
				"Read_Result": {
					"OK365_Customer_List": "OK365_Customer_List"
				},
				"Reserve": "string",
				"Shipping_Advice": "string"
			}
		},
		"OK365_Daily_Sales_Report": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Daily_Sales_Report": {
					"Cash_Collection_Amount": "decimal",
					"Cash_Sales_Amount": "decimal",
					"Cheque_Collection_Amount": "decimal",
					"Credit_Sales_Amount": "decimal",
					"Date_Filter": "date",
					"Key": "string",
					"Return_Sales_Amount": "decimal",
					"Route_Code": "string"
				},
				"OK365_Daily_Sales_Report_Fields": "string",
				"OK365_Daily_Sales_Report_Filter": {
					"Criteria": "string",
					"Field": "OK365_Daily_Sales_Report_Fields"
				},
				"OK365_Daily_Sales_Report_List": {
					"OK365_Daily_Sales_Report": "OK365_Daily_Sales_Report"
				},
				"Read": {
					"Route_Code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Daily_Sales_Report": "OK365_Daily_Sales_Report"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Daily_Sales_Report_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Daily_Sales_Report_List"
				},
				"Read_Result": {
					"OK365_Daily_Sales_Report": "OK365_Daily_Sales_Report"
				},
				"Update": {
					"OK365_Daily_Sales_Report": "OK365_Daily_Sales_Report"
				},
				"UpdateMultiple": {
					"OK365_Daily_Sales_Report_List": "OK365_Daily_Sales_Report_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Daily_Sales_Report_List": "OK365_Daily_Sales_Report_List"
				},
				"Update_Result": {
					"OK365_Daily_Sales_Report": "OK365_Daily_Sales_Report"
				}
			}
		},
		"OK365_GST_Setup": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"OK365_GST_Setup": "OK365_GST_Setup"
				},
				"CreateMultiple": {
					"OK365_GST_Setup_List": "OK365_GST_Setup_List"
				},
				"CreateMultiple_Result": {
					"OK365_GST_Setup_List": "OK365_GST_Setup_List"
				},
				"Create_Result": {
					"OK365_GST_Setup": "OK365_GST_Setup"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_GST_Setup": {
					"Adjust_for_Payment_Discount": "boolean",
					"Certificate_of_Supply_Required": "boolean",
					"Description": "string",
					"EU_Service": "boolean",
					"Key": "string",
					"Purch_VAT_Unreal_Account": "string",
					"Purchase_VAT_Account": "string",
					"Reverse_Chrg_VAT_Acc": "string",
					"Reverse_Chrg_VAT_Unreal_Acc": "string",
					"Sales_VAT_Account": "string",
					"Sales_VAT_Unreal_Account": "string",
					"Tax_Category": "string",
					"Unrealized_VAT_Type": "Unrealized_VAT_Type",
					"VAT_Bus_Posting_Group": "string",
					"VAT_Calculation_Type": "VAT_Calculation_Type",
					"VAT_Clause_Code": "string",
					"VAT_Identifier": "string",
					"VAT_Percent": "decimal",
					"VAT_Prod_Posting_Group": "string"
				},
				"OK365_GST_Setup_Fields": "string",
				"OK365_GST_Setup_Filter": {
					"Criteria": "string",
					"Field": "OK365_GST_Setup_Fields"
				},
				"OK365_GST_Setup_List": {
					"OK365_GST_Setup": "OK365_GST_Setup"
				},
				"Read": {
					"VAT_Bus_Posting_Group": "string",
					"VAT_Prod_Posting_Group": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_GST_Setup": "OK365_GST_Setup"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_GST_Setup_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_GST_Setup_List"
				},
				"Read_Result": {
					"OK365_GST_Setup": "OK365_GST_Setup"
				},
				"Unrealized_VAT_Type": "string",
				"Update": {
					"OK365_GST_Setup": "OK365_GST_Setup"
				},
				"UpdateMultiple": {
					"OK365_GST_Setup_List": "OK365_GST_Setup_List"
				},
				"UpdateMultiple_Result": {
					"OK365_GST_Setup_List": "OK365_GST_Setup_List"
				},
				"Update_Result": {
					"OK365_GST_Setup": "OK365_GST_Setup"
				},
				"VAT_Calculation_Type": "string"
			}
		},
		"OK365_G_L_Account_List": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Account_Category": "string",
				"Account_Type": "string",
				"Consol_Translation_Method": "string",
				"Create": {
					"OK365_G_L_Account_List": "OK365_G_L_Account_List"
				},
				"CreateMultiple": {
					"OK365_G_L_Account_List_List": "OK365_G_L_Account_List_List"
				},
				"CreateMultiple_Result": {
					"OK365_G_L_Account_List_List": "OK365_G_L_Account_List_List"
				},
				"Create_Result": {
					"OK365_G_L_Account_List": "OK365_G_L_Account_List"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Gen_Posting_Type": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"Income_Balance": "string",
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_G_L_Account_List": {
					"Account_Category": "Account_Category",
					"Account_Subcategory_Descript": "string",
					"Account_Type": "Account_Type",
					"Add_Currency_Balance_at_Date": "decimal",
					"Additional_Currency_Balance": "decimal",
					"Additional_Currency_Net_Change": "decimal",
					"Balance": "decimal",
					"Balance_at_Date": "decimal",
					"Consol_Credit_Acc": "string",
					"Consol_Debit_Acc": "string",
					"Consol_Translation_Method": "Consol_Translation_Method",
					"Cost_Type_No": "string",
					"Credit_Amount": "decimal",
					"Debit_Amount": "decimal",
					"Default_Deferral_Template_Code": "string",
					"Default_IC_Partner_G_L_Acc_No": "string",
					"Direct_Posting": "boolean",
					"Gen_Bus_Posting_Group": "string",
					"Gen_Posting_Type": "Gen_Posting_Type",
					"Gen_Prod_Posting_Group": "string",
					"Income_Balance": "Income_Balance",
					"Key": "string",
					"Name": "string",
					"Net_Change": "decimal",
					"No": "string",
					"Totaling": "string",
					"VAT_Bus_Posting_Group": "string",
					"VAT_Prod_Posting_Group": "string"
				},
				"OK365_G_L_Account_List_Fields": "string",
				"OK365_G_L_Account_List_Filter": {
					"Criteria": "string",
					"Field": "OK365_G_L_Account_List_Fields"
				},
				"OK365_G_L_Account_List_List": {
					"OK365_G_L_Account_List": "OK365_G_L_Account_List"
				},
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_G_L_Account_List": "OK365_G_L_Account_List"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_G_L_Account_List_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_G_L_Account_List_List"
				},
				"Read_Result": {
					"OK365_G_L_Account_List": "OK365_G_L_Account_List"
				},
				"Update": {
					"OK365_G_L_Account_List": "OK365_G_L_Account_List"
				},
				"UpdateMultiple": {
					"OK365_G_L_Account_List_List": "OK365_G_L_Account_List_List"
				},
				"UpdateMultiple_Result": {
					"OK365_G_L_Account_List_List": "OK365_G_L_Account_List_List"
				},
				"Update_Result": {
					"OK365_G_L_Account_List": "OK365_G_L_Account_List"
				}
			}
		},
		"OK365_GetItemSalesPrice": {
			"f": {
				"CalculateInventory": {
					"i": "CalculateInventory",
					"o": "CalculateInventory_Result"
				},
				"DeleteSalesReturnLines": {
					"i": "DeleteSalesReturnLines",
					"o": "DeleteSalesReturnLines_Result"
				},
				"GetHandQuantity": {
					"i": "GetHandQuantity",
					"o": "GetHandQuantity_Result"
				},
				"GetItemPicture": {
					"i": "GetItemPicture",
					"o": "GetItemPicture_Result"
				},
				"GetPostedSalesInvoiceSignature": {
					"i": "GetPostedSalesInvoiceSignature",
					"o": "GetPostedSalesInvoiceSignature_Result"
				},
				"SetItemPicture": {
					"i": "SetItemPicture",
					"o": "SetItemPicture_Result"
				},
				"SetPostedSalesInvoiceSignature": {
					"i": "SetPostedSalesInvoiceSignature",
					"o": "SetPostedSalesInvoiceSignature_Result"
				},
				"UpdateDailySalesReportDateFilter": {
					"i": "UpdateDailySalesReportDateFilter",
					"o": "UpdateDailySalesReportDateFilter_Result"
				},
				"UpdatePhysicalJournalStatus": {
					"i": "UpdatePhysicalJournalStatus",
					"o": "UpdatePhysicalJournalStatus_Result"
				},
				"a61a61a61LineDiscounta61a61a61": {
					"i": "a61a61a61LineDiscounta61a61a61",
					"o": "a61a61a61LineDiscounta61a61a61_Result"
				},
				"fnGetItemSalesPrice": {
					"i": "fnGetItemSalesPrice",
					"o": "fnGetItemSalesPrice_Result"
				},
				"fnReleaseTransferOrder": {
					"i": "fnReleaseTransferOrder",
					"o": "fnReleaseTransferOrder_Result"
				},
				"g_fnDeleteSalesOrdLine": {
					"i": "g_fnDeleteSalesOrdLine",
					"o": "g_fnDeleteSalesOrdLine_Result"
				},
				"g_fnSalesPostOrder": {
					"i": "g_fnSalesPostOrder",
					"o": "g_fnSalesPostOrder_Result"
				}
			},
			"sp": "Codeunit",
			"t": {
				"CalculateInventory": {
					"documentNo": "string",
					"locationCode": "string",
					"postingDate": "date"
				},
				"CalculateInventory_Result": {
					"": ""
				},
				"DeleteSalesReturnLines": {
					"documentNo": "string"
				},
				"DeleteSalesReturnLines_Result": {
					"": ""
				},
				"GetHandQuantity": {
					"cdItemCode": "string",
					"cdLoc": "string",
					"cdVarCode": "string"
				},
				"GetHandQuantity_Result": {
					"return_value": "decimal"
				},
				"GetItemPicture": {
					"itemNo": "string"
				},
				"GetItemPicture_Result": {
					"return_value": "string"
				},
				"GetPostedSalesInvoiceSignature": {
					"postedSalesInvoiceNo": "string"
				},
				"GetPostedSalesInvoiceSignature_Result": {
					"return_value": "string"
				},
				"SetItemPicture": {
					"itemNo": "string",
					"picture": "string"
				},
				"SetItemPicture_Result": {
					"": ""
				},
				"SetPostedSalesInvoiceSignature": {
					"postedSalesInvoiceNo": "string",
					"signaturePicture": "string"
				},
				"SetPostedSalesInvoiceSignature_Result": {
					"": ""
				},
				"UpdateDailySalesReportDateFilter": {
					"dateFilter": "date"
				},
				"UpdateDailySalesReportDateFilter_Result": {
					"": ""
				},
				"UpdatePhysicalJournalStatus": {
					"documentNo": "string"
				},
				"UpdatePhysicalJournalStatus_Result": {
					"": ""
				},
				"a61a61a61LineDiscounta61a61a61": {
					"": ""
				},
				"a61a61a61LineDiscounta61a61a61_Result": {
					"": ""
				},
				"fnGetItemSalesPrice": {
					"currencyCode": "string",
					"customerNo": "string",
					"itemNo": "string",
					"itemVariantCode": "string",
					"orderDate": "date",
					"uOM": "string"
				},
				"fnGetItemSalesPrice_Result": {
					"return_value": "decimal"
				},
				"fnReleaseTransferOrder": {
					"cdeTransferOrdNo": "string"
				},
				"fnReleaseTransferOrder_Result": {
					"": ""
				},
				"g_fnDeleteSalesOrdLine": {
					"cdeSalesOrdNo": "string"
				},
				"g_fnDeleteSalesOrdLine_Result": {
					"": ""
				},
				"g_fnSalesPostOrder": {
					"blnInvoice": "boolean",
					"blnShip": "boolean",
					"cdeSalesOrdNo": "string",
					"optDocType": "int"
				},
				"g_fnSalesPostOrder_Result": {
					"": ""
				}
			}
		},
		"OK365_Item_Card": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Assembly_Policy": "string",
				"Costing_Method": "string",
				"Create": {
					"OK365_Item_Card": "OK365_Item_Card"
				},
				"CreateMultiple": {
					"OK365_Item_Card_List": "OK365_Item_Card_List"
				},
				"CreateMultiple_Result": {
					"OK365_Item_Card_List": "OK365_Item_Card_List"
				},
				"Create_Result": {
					"OK365_Item_Card": "OK365_Item_Card"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Flushing_Method": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Manufacturing_Policy": "string",
				"OK365_Item_Card": {
					"Allow_Invoice_Disc": "boolean",
					"Application_Wksh_User_ID": "string",
					"Assembly_BOM": "boolean",
					"Assembly_Policy": "Assembly_Policy",
					"Automatic_Ext_Texts": "boolean",
					"Base_Unit_of_Measure": "string",
					"Blocked": "boolean",
					"CalcUnitPriceExclVAT": "decimal",
					"Common_Item_No": "string",
					"Cost_is_Adjusted": "boolean",
					"Cost_is_Posted_to_G_L": "boolean",
					"Costing_Method": "Costing_Method",
					"Country_Region_of_Origin_Code": "string",
					"Created_From_Nonstock_Item": "boolean",
					"Critical": "boolean",
					"Dampener_Period": "string",
					"Dampener_Quantity": "decimal",
					"Default_Deferral_Template_Code": "string",
					"Description": "string",
					"Expiration_Calculation": "string",
					"Flushing_Method": "Flushing_Method",
					"GTIN": "string",
					"Gen_Prod_Posting_Group": "string",
					"Gross_Weight": "decimal",
					"Identifier_Code": "string",
					"Include_Inventory": "boolean",
					"Indirect_Cost_Percent": "decimal",
					"Inventory": "decimal",
					"Inventory_Posting_Group": "string",
					"Item_Category_Code": "string",
					"Item_Disc_Group": "string",
					"Item_Tracking_Code": "string",
					"Key": "string",
					"Last_Counting_Period_Update": "date",
					"Last_Date_Modified": "date",
					"Last_Direct_Cost": "decimal",
					"Last_Phys_Invt_Date": "date",
					"Lead_Time_Calculation": "string",
					"Lot_Accumulation_Period": "string",
					"Lot_Nos": "string",
					"Lot_Size": "decimal",
					"Manufacturing_Policy": "Manufacturing_Policy",
					"Maximum_Inventory": "decimal",
					"Maximum_Order_Quantity": "decimal",
					"Minimum_Order_Quantity": "decimal",
					"Net_Invoiced_Qty": "decimal",
					"Net_Weight": "decimal",
					"Next_Counting_End_Date": "date",
					"Next_Counting_Start_Date": "date",
					"No": "string",
					"Order_Multiple": "decimal",
					"Order_Tracking_Policy": "Order_Tracking_Policy",
					"Overflow_Level": "decimal",
					"Overhead_Rate": "decimal",
					"Phys_Invt_Counting_Period_Code": "string",
					"PreventNegInventoryDefaultYes": "PreventNegInventoryDefaultYes",
					"Price_Includes_VAT": "boolean",
					"Price_Profit_Calculation": "Price_Profit_Calculation",
					"Production_BOM_No": "string",
					"Profit_Percent": "decimal",
					"Purch_Unit_of_Measure": "string",
					"Purchasing_Blocked": "boolean",
					"Put_away_Template_Code": "string",
					"Put_away_Unit_of_Measure_Code": "string",
					"Qty_on_Asm_Component": "decimal",
					"Qty_on_Assembly_Order": "decimal",
					"Qty_on_Component_Lines": "decimal",
					"Qty_on_Job_Order": "decimal",
					"Qty_on_Prod_Order": "decimal",
					"Qty_on_Purch_Order": "decimal",
					"Qty_on_Sales_Order": "decimal",
					"Qty_on_Service_Order": "decimal",
					"Reorder_Point": "decimal",
					"Reorder_Quantity": "decimal",
					"Reordering_Policy": "Reordering_Policy",
					"Replenishment_System": "Replenishment_System",
					"Rescheduling_Period": "string",
					"Reserve": "Reserve",
					"Rounding_Precision": "decimal",
					"Routing_No": "string",
					"Safety_Lead_Time": "string",
					"Safety_Stock_Quantity": "decimal",
					"Sales_Blocked": "boolean",
					"Sales_Unit_of_Measure": "string",
					"Scrap_Percent": "decimal",
					"Search_Description": "string",
					"Serial_Nos": "string",
					"Service_Item_Group": "string",
					"Shelf_No": "string",
					"SpecialPricesAndDiscountsTxt": "string",
					"SpecialPurchPricesAndDiscountsTxt": "string",
					"Special_Equipment_Code": "string",
					"Standard_Cost": "decimal",
					"Stockkeeping_Unit_Exists": "boolean",
					"StockoutWarningDefaultYes": "StockoutWarningDefaultYes",
					"Tariff_No": "string",
					"Time_Bucket": "string",
					"Type": "Type",
					"Unit_Cost": "decimal",
					"Unit_Price": "decimal",
					"Unit_Volume": "decimal",
					"Use_Cross_Docking": "boolean",
					"VAT_Bus_Posting_Gr_Price": "string",
					"VAT_Prod_Posting_Group": "string",
					"Vendor_Item_No": "string",
					"Vendor_No": "string",
					"Warehouse_Class_Code": "string"
				},
				"OK365_Item_Card_Fields": "string",
				"OK365_Item_Card_Filter": {
					"Criteria": "string",
					"Field": "OK365_Item_Card_Fields"
				},
				"OK365_Item_Card_List": {
					"OK365_Item_Card": "OK365_Item_Card"
				},
				"Order_Tracking_Policy": "string",
				"PreventNegInventoryDefaultYes": "string",
				"Price_Profit_Calculation": "string",
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Item_Card": "OK365_Item_Card"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Item_Card_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Item_Card_List"
				},
				"Read_Result": {
					"OK365_Item_Card": "OK365_Item_Card"
				},
				"Reordering_Policy": "string",
				"Replenishment_System": "string",
				"Reserve": "string",
				"StockoutWarningDefaultYes": "string",
				"Type": "string",
				"Update": {
					"OK365_Item_Card": "OK365_Item_Card"
				},
				"UpdateMultiple": {
					"OK365_Item_Card_List": "OK365_Item_Card_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Item_Card_List": "OK365_Item_Card_List"
				},
				"Update_Result": {
					"OK365_Item_Card": "OK365_Item_Card"
				}
			}
		},
		"OK365_Item_Categories": {
			"f": {
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Item_Categories": {
					"Code": "string",
					"Description": "string",
					"Key": "string"
				},
				"OK365_Item_Categories_Fields": "string",
				"OK365_Item_Categories_Filter": {
					"Criteria": "string",
					"Field": "OK365_Item_Categories_Fields"
				},
				"OK365_Item_Categories_List": {
					"OK365_Item_Categories": "OK365_Item_Categories"
				},
				"Read": {
					"Code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Item_Categories": "OK365_Item_Categories"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Item_Categories_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Item_Categories_List"
				},
				"Read_Result": {
					"OK365_Item_Categories": "OK365_Item_Categories"
				},
				"Update": {
					"OK365_Item_Categories": "OK365_Item_Categories"
				},
				"UpdateMultiple": {
					"OK365_Item_Categories_List": "OK365_Item_Categories_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Item_Categories_List": "OK365_Item_Categories_List"
				},
				"Update_Result": {
					"OK365_Item_Categories": "OK365_Item_Categories"
				}
			}
		},
		"OK365_Item_Journal": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"CurrentJnlBatchName": "string",
					"OK365_Item_Journal": "OK365_Item_Journal"
				},
				"CreateMultiple": {
					"CurrentJnlBatchName": "string",
					"OK365_Item_Journal_List": "OK365_Item_Journal_List"
				},
				"CreateMultiple_Result": {
					"OK365_Item_Journal_List": "OK365_Item_Journal_List"
				},
				"Create_Result": {
					"OK365_Item_Journal": "OK365_Item_Journal"
				},
				"Delete": {
					"CurrentJnlBatchName": "string",
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Entry_Type": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Item_Journal": {
					"Amount": "decimal",
					"Applies_from_Entry": "int",
					"Applies_to_Entry": "int",
					"Bin_Code": "string",
					"Country_Region_Code": "string",
					"Description": "string",
					"Discount_Amount": "decimal",
					"Document_Date": "date",
					"Document_No": "string",
					"Entry_Type": "Entry_Type",
					"External_Document_No": "string",
					"Gen_Bus_Posting_Group": "string",
					"Gen_Prod_Posting_Group": "string",
					"Indirect_Cost_Percent": "decimal",
					"ItemDescription": "string",
					"Item_No": "string",
					"Key": "string",
					"Location_Code": "string",
					"Posting_Date": "date",
					"Quantity": "decimal",
					"Reason_Code": "string",
					"Salespers_Purch_Code": "string",
					"ShortcutDimCode3": "string",
					"ShortcutDimCode4": "string",
					"ShortcutDimCode5": "string",
					"ShortcutDimCode6": "string",
					"ShortcutDimCode7": "string",
					"ShortcutDimCode8": "string",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"Transaction_Type": "string",
					"Transport_Method": "string",
					"Unit_Amount": "decimal",
					"Unit_Cost": "decimal",
					"Unit_of_Measure_Code": "string",
					"Variant_Code": "string"
				},
				"OK365_Item_Journal_Fields": "string",
				"OK365_Item_Journal_Filter": {
					"Criteria": "string",
					"Field": "OK365_Item_Journal_Fields"
				},
				"OK365_Item_Journal_List": {
					"OK365_Item_Journal": "OK365_Item_Journal"
				},
				"ReadByRecId": {
					"CurrentJnlBatchName": "string",
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Item_Journal": "OK365_Item_Journal"
				},
				"ReadMultiple": {
					"CurrentJnlBatchName": "string",
					"bookmarkKey": "string",
					"filter": "OK365_Item_Journal_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Item_Journal_List"
				},
				"Update": {
					"CurrentJnlBatchName": "string",
					"OK365_Item_Journal": "OK365_Item_Journal"
				},
				"UpdateMultiple": {
					"CurrentJnlBatchName": "string",
					"OK365_Item_Journal_List": "OK365_Item_Journal_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Item_Journal_List": "OK365_Item_Journal_List"
				},
				"Update_Result": {
					"OK365_Item_Journal": "OK365_Item_Journal"
				}
			}
		},
		"OK365_Item_List": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Assembly_Policy": "string",
				"Costing_Method": "string",
				"Flushing_Method": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Manufacturing_Policy": "string",
				"OK365_Item_List": {
					"Assembly_BOM": "boolean",
					"Assembly_Policy": "Assembly_Policy",
					"Base_Unit_of_Measure": "string",
					"Blocked": "boolean",
					"Cost_is_Adjusted": "boolean",
					"Costing_Method": "Costing_Method",
					"Created_From_Nonstock_Item": "boolean",
					"Default_Deferral_Template_Code": "string",
					"Description": "string",
					"Flushing_Method": "Flushing_Method",
					"Gen_Prod_Posting_Group": "string",
					"GetPicture": "string",
					"Indirect_Cost_Percent": "decimal",
					"Inventory": "decimal",
					"Inventory_Posting_Group": "string",
					"Item_Category_Code": "string",
					"Item_Disc_Group": "string",
					"Item_Tracking_Code": "string",
					"Key": "string",
					"Last_Date_Modified": "date",
					"Last_Direct_Cost": "decimal",
					"Lead_Time_Calculation": "string",
					"Location_Filter": "string",
					"Manufacturing_Policy": "Manufacturing_Policy",
					"No": "string",
					"Overhead_Rate": "decimal",
					"Price_Profit_Calculation": "Price_Profit_Calculation",
					"Production_BOM_No": "string",
					"Profit_Percent": "decimal",
					"Purch_Unit_of_Measure": "string",
					"Replenishment_System": "Replenishment_System",
					"Routing_No": "string",
					"Sales_Unit_of_Measure": "string",
					"Search_Description": "string",
					"Shelf_No": "string",
					"Standard_Cost": "decimal",
					"Stockkeeping_Unit_Exists": "boolean",
					"Substitutes_Exist": "boolean",
					"Tariff_No": "string",
					"Type": "Type",
					"Unit_Cost": "decimal",
					"Unit_Price": "decimal",
					"VAT_Prod_Posting_Group": "string",
					"Vendor_Item_No": "string",
					"Vendor_No": "string"
				},
				"OK365_Item_List_Fields": "string",
				"OK365_Item_List_Filter": {
					"Criteria": "string",
					"Field": "OK365_Item_List_Fields"
				},
				"OK365_Item_List_List": {
					"OK365_Item_List": "OK365_Item_List"
				},
				"Price_Profit_Calculation": "string",
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Item_List": "OK365_Item_List"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Item_List_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Item_List_List"
				},
				"Read_Result": {
					"OK365_Item_List": "OK365_Item_List"
				},
				"Replenishment_System": "string",
				"Type": "string"
			}
		},
		"OK365_Item_Unit_of_Measure": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"OK365_Item_Unit_of_Measure": "OK365_Item_Unit_of_Measure"
				},
				"CreateMultiple": {
					"OK365_Item_Unit_of_Measure_List": "OK365_Item_Unit_of_Measure_List"
				},
				"CreateMultiple_Result": {
					"OK365_Item_Unit_of_Measure_List": "OK365_Item_Unit_of_Measure_List"
				},
				"Create_Result": {
					"OK365_Item_Unit_of_Measure": "OK365_Item_Unit_of_Measure"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Item_Unit_of_Measure": {
					"Code": "string",
					"Cubage": "decimal",
					"Height": "decimal",
					"ItemUnitOfMeasure": "string",
					"Item_No": "string",
					"Key": "string",
					"Length": "decimal",
					"Qty_per_Unit_of_Measure": "decimal",
					"Weight": "decimal",
					"Width": "decimal"
				},
				"OK365_Item_Unit_of_Measure_Fields": "string",
				"OK365_Item_Unit_of_Measure_Filter": {
					"Criteria": "string",
					"Field": "OK365_Item_Unit_of_Measure_Fields"
				},
				"OK365_Item_Unit_of_Measure_List": {
					"OK365_Item_Unit_of_Measure": "OK365_Item_Unit_of_Measure"
				},
				"Read": {
					"Code": "string",
					"Item_No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Item_Unit_of_Measure": "OK365_Item_Unit_of_Measure"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Item_Unit_of_Measure_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Item_Unit_of_Measure_List"
				},
				"Read_Result": {
					"OK365_Item_Unit_of_Measure": "OK365_Item_Unit_of_Measure"
				},
				"Update": {
					"OK365_Item_Unit_of_Measure": "OK365_Item_Unit_of_Measure"
				},
				"UpdateMultiple": {
					"OK365_Item_Unit_of_Measure_List": "OK365_Item_Unit_of_Measure_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Item_Unit_of_Measure_List": "OK365_Item_Unit_of_Measure_List"
				},
				"Update_Result": {
					"OK365_Item_Unit_of_Measure": "OK365_Item_Unit_of_Measure"
				}
			}
		},
		"OK365_Location_List": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Location_List": {
					"Code": "string",
					"Key": "string",
					"Name": "string"
				},
				"OK365_Location_List_Fields": "string",
				"OK365_Location_List_Filter": {
					"Criteria": "string",
					"Field": "OK365_Location_List_Fields"
				},
				"OK365_Location_List_List": {
					"OK365_Location_List": "OK365_Location_List"
				},
				"Read": {
					"Code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Location_List": "OK365_Location_List"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Location_List_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Location_List_List"
				},
				"Read_Result": {
					"OK365_Location_List": "OK365_Location_List"
				}
			}
		},
		"OK365_Location_card": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Bin_Capacity_Policy": "string",
				"Create": {
					"OK365_Location_card": "OK365_Location_card"
				},
				"CreateMultiple": {
					"OK365_Location_card_List": "OK365_Location_card_List"
				},
				"CreateMultiple_Result": {
					"OK365_Location_card_List": "OK365_Location_card_List"
				},
				"Create_Result": {
					"OK365_Location_card": "OK365_Location_card"
				},
				"Default_Bin_Selection": "string",
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Location_card": {
					"Address": "string",
					"Address_2": "string",
					"Adjustment_Bin_Code": "string",
					"Allow_Breakbulk": "boolean",
					"Always_Create_Pick_Line": "boolean",
					"Always_Create_Put_away_Line": "boolean",
					"Asm_to_Order_Shpt_Bin_Code": "string",
					"Base_Calendar_Code": "string",
					"Bin_Capacity_Policy": "Bin_Capacity_Policy",
					"Bin_Mandatory": "boolean",
					"City": "string",
					"Code": "string",
					"Contact": "string",
					"Country_Region_Code": "string",
					"Cross_Dock_Bin_Code": "string",
					"Cross_Dock_Due_Date_Calc": "string",
					"Customized_Calendar": "string",
					"Default_Bin_Selection": "Default_Bin_Selection",
					"Directed_Put_away_and_Pick": "boolean",
					"E_Mail": "string",
					"Fax_No": "string",
					"From_Assembly_Bin_Code": "string",
					"From_Production_Bin_Code": "string",
					"Home_Page": "string",
					"Inbound_Whse_Handling_Time": "string",
					"Key": "string",
					"Name": "string",
					"Open_Shop_Floor_Bin_Code": "string",
					"Outbound_Whse_Handling_Time": "string",
					"Phone_No": "string",
					"Pick_According_to_FEFO": "boolean",
					"Post_Code": "string",
					"Put_away_Template_Code": "string",
					"Receipt_Bin_Code": "string",
					"Require_Pick": "boolean",
					"Require_Put_away": "boolean",
					"Require_Receive": "boolean",
					"Require_Shipment": "boolean",
					"Shipment_Bin_Code": "string",
					"ShowMap": "string",
					"Special_Equipment": "Special_Equipment",
					"To_Assembly_Bin_Code": "string",
					"To_Production_Bin_Code": "string",
					"Use_ADCS": "boolean",
					"Use_As_In_Transit": "boolean",
					"Use_Cross_Docking": "boolean",
					"Use_Put_away_Worksheet": "boolean"
				},
				"OK365_Location_card_Fields": "string",
				"OK365_Location_card_Filter": {
					"Criteria": "string",
					"Field": "OK365_Location_card_Fields"
				},
				"OK365_Location_card_List": {
					"OK365_Location_card": "OK365_Location_card"
				},
				"Read": {
					"Code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Location_card": "OK365_Location_card"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Location_card_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Location_card_List"
				},
				"Read_Result": {
					"OK365_Location_card": "OK365_Location_card"
				},
				"Special_Equipment": "string",
				"Update": {
					"OK365_Location_card": "OK365_Location_card"
				},
				"UpdateMultiple": {
					"OK365_Location_card_List": "OK365_Location_card_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Location_card_List": "OK365_Location_card_List"
				},
				"Update_Result": {
					"OK365_Location_card": "OK365_Location_card"
				}
			}
		},
		"OK365_Mobile_User_Master": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Order_Entry": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Order_Entry_Line": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Order_List": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Payment_Terms": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Physical_Inventory_Journal": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Post_Codes": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Posted_Sales_Invoice_Header": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Posted_Sales_Invoice_Line": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Posted_Sales_Invoice_List": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Reason_code": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Return_reason_code": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Route_And_Customer_Priority": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Route_Master": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Sales_Invoice": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Sales_Price": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Sales_Return_Order": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Sales__x0026__Receivable_Setup": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Salespeople_Purchasers": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Shipment_Method": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Shipping_Address_Card": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Shipping_Address_List": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Transfer_Order_List": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Transfer_Orders": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"OK365_Units_of_Measure": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"Power_BI_Aged_Acc_Payable": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"Power_BI_Aged_Acc_Receivable": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"Power_BI_Aged_Inventory_Chart": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"Power_BI_Job_Act_v_Budg_Cost": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"Power_BI_Job_Act_v_Budg_Price": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"Power_BI_Job_Profitability": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"Power_BI_Report_Labels": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"Power_BI_Sales_Pipeline": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"Power_BI_Top_5_Opportunities": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"Power_BI_WorkDate_Calc": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"UserTaskSetComplete": {
			"f": {
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Priority": "string",
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"UserTaskSetComplete": "UserTaskSetComplete"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "UserTaskSetComplete_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "UserTaskSetComplete_List"
				},
				"UserTaskSetComplete": {
					"Assigned_To_User_Name": "string",
					"Completed_By_User_Name": "string",
					"Completed_DateTime": "dateTime",
					"Created_By_User_Name": "string",
					"Created_DateTime": "dateTime",
					"Due_DateTime": "dateTime",
					"Key": "string",
					"Percent_Complete": "int",
					"Priority": "Priority",
					"Start_DateTime": "dateTime",
					"Title": "string",
					"User_Task_Group_Assigned_To": "string"
				},
				"UserTaskSetComplete_Fields": "string",
				"UserTaskSetComplete_Filter": {
					"Criteria": "string",
					"Field": "UserTaskSetComplete_Fields"
				},
				"UserTaskSetComplete_List": {
					"UserTaskSetComplete": "UserTaskSetComplete"
				}
			}
		},
		"WorkflowActionResponse": {
			"f": {
				"Approve": {
					"i": "Approve",
					"o": "Approve_Result"
				},
				"Reject": {
					"i": "Reject",
					"o": "Reject_Result"
				}
			},
			"sp": "Codeunit",
			"t": {
				"Approve": {
					"workflowStepInstanceId": "string"
				},
				"Approve_Result": {
					"": ""
				},
				"Reject": {
					"workflowStepInstanceId": "string"
				},
				"Reject_Result": {
					"": ""
				}
			}
		},
		"nativeInvoicingAttachments": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingAttachments": "nativeInvoicingAttachments"
				},
				"CreateMultiple": {
					"nativeInvoicingAttachments_List": "nativeInvoicingAttachments_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingAttachments_List": "nativeInvoicingAttachments_List"
				},
				"Create_Result": {
					"nativeInvoicingAttachments": "nativeInvoicingAttachments"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"id": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingAttachments": "nativeInvoicingAttachments"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingAttachments_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingAttachments_List"
				},
				"Read_Result": {
					"nativeInvoicingAttachments": "nativeInvoicingAttachments"
				},
				"Update": {
					"nativeInvoicingAttachments": "nativeInvoicingAttachments"
				},
				"UpdateMultiple": {
					"nativeInvoicingAttachments_List": "nativeInvoicingAttachments_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingAttachments_List": "nativeInvoicingAttachments_List"
				},
				"Update_Result": {
					"nativeInvoicingAttachments": "nativeInvoicingAttachments"
				},
				"nativeInvoicingAttachments": {
					"Key": "string",
					"base64Content": "string",
					"byteSize": "int",
					"documentId": "string",
					"fileName": "string",
					"id": "string",
					"lastModifiedDateTime": "dateTime"
				},
				"nativeInvoicingAttachments_Fields": "string",
				"nativeInvoicingAttachments_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingAttachments_Fields"
				},
				"nativeInvoicingAttachments_List": {
					"nativeInvoicingAttachments": "nativeInvoicingAttachments"
				}
			}
		},
		"nativeInvoicingContacts": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingContacts": "nativeInvoicingContacts"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingContacts_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingContacts_List"
				},
				"Read_Result": {
					"nativeInvoicingContacts": "nativeInvoicingContacts"
				},
				"nativeInvoicingContacts": {
					"Key": "string",
					"customerId": "string",
					"displayName": "string",
					"email": "string",
					"number": "string",
					"phoneNumber": "string",
					"xrmId": "string"
				},
				"nativeInvoicingContacts_Fields": "string",
				"nativeInvoicingContacts_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingContacts_Fields"
				},
				"nativeInvoicingContacts_List": {
					"nativeInvoicingContacts": "nativeInvoicingContacts"
				}
			}
		},
		"nativeInvoicingCountryRegion": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingCountryRegion": "nativeInvoicingCountryRegion"
				},
				"CreateMultiple": {
					"nativeInvoicingCountryRegion_List": "nativeInvoicingCountryRegion_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingCountryRegion_List": "nativeInvoicingCountryRegion_List"
				},
				"Create_Result": {
					"nativeInvoicingCountryRegion": "nativeInvoicingCountryRegion"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingCountryRegion": "nativeInvoicingCountryRegion"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingCountryRegion_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingCountryRegion_List"
				},
				"Read_Result": {
					"nativeInvoicingCountryRegion": "nativeInvoicingCountryRegion"
				},
				"Update": {
					"nativeInvoicingCountryRegion": "nativeInvoicingCountryRegion"
				},
				"UpdateMultiple": {
					"nativeInvoicingCountryRegion_List": "nativeInvoicingCountryRegion_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingCountryRegion_List": "nativeInvoicingCountryRegion_List"
				},
				"Update_Result": {
					"nativeInvoicingCountryRegion": "nativeInvoicingCountryRegion"
				},
				"nativeInvoicingCountryRegion": {
					"Key": "string",
					"code": "string",
					"displayName": "string",
					"id": "string",
					"lastModifiedDateTime": "dateTime"
				},
				"nativeInvoicingCountryRegion_Fields": "string",
				"nativeInvoicingCountryRegion_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingCountryRegion_Fields"
				},
				"nativeInvoicingCountryRegion_List": {
					"nativeInvoicingCountryRegion": "nativeInvoicingCountryRegion"
				}
			}
		},
		"nativeInvoicingCustomers": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"Delete_Coupons": {
					"i": "Delete_Coupons",
					"o": "Delete_Coupons_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingCustomers": "nativeInvoicingCustomers"
				},
				"CreateMultiple": {
					"nativeInvoicingCustomers_List": "nativeInvoicingCustomers_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingCustomers_List": "nativeInvoicingCustomers_List"
				},
				"Create_Result": {
					"nativeInvoicingCustomers": "nativeInvoicingCustomers"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Coupons": {
					"Coupons_Key": "string"
				},
				"Delete_Coupons_Result": {
					"Delete_Coupons_Result": "boolean"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Native_Coupons": {
					"Key": "string",
					"amount": "string",
					"claimId": "string",
					"code": "string",
					"createdDateTime": "dateTime",
					"customerId": "string",
					"discountType": "discountType",
					"discountValue": "decimal",
					"expiration": "date",
					"graphContactId": "string",
					"isApplied": "boolean",
					"isValid": "boolean",
					"offer": "string",
					"status": "string",
					"terms": "string",
					"usage": "usage"
				},
				"Native_Coupons_List": {
					"Native_Coupons": "Native_Coupons"
				},
				"Read": {
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingCustomers": "nativeInvoicingCustomers"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingCustomers_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingCustomers_List"
				},
				"Read_Result": {
					"nativeInvoicingCustomers": "nativeInvoicingCustomers"
				},
				"Update": {
					"nativeInvoicingCustomers": "nativeInvoicingCustomers"
				},
				"UpdateMultiple": {
					"nativeInvoicingCustomers_List": "nativeInvoicingCustomers_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingCustomers_List": "nativeInvoicingCustomers_List"
				},
				"Update_Result": {
					"nativeInvoicingCustomers": "nativeInvoicingCustomers"
				},
				"discountType": "string",
				"nativeInvoicingCustomers": {
					"Coupons": "Native_Coupons_List",
					"Key": "string",
					"address": "string",
					"balance": "decimal",
					"contactId": "string",
					"displayName": "string",
					"email": "string",
					"graphContactId": "string",
					"id": "string",
					"isBlocked": "boolean",
					"lastModifiedDateTime": "dateTime",
					"number": "string",
					"overdueAmount": "decimal",
					"paymentMethodId": "string",
					"paymentTermsId": "string",
					"phoneNumber": "string",
					"pricesIncludeTax": "boolean",
					"shipmentMethodId": "string",
					"taxAreaDisplayName": "string",
					"taxAreaId": "string",
					"taxLiable": "boolean",
					"taxRegistrationNumber": "string",
					"totalSalesExcludingTax": "decimal",
					"type": "type",
					"website": "string"
				},
				"nativeInvoicingCustomers_Fields": "string",
				"nativeInvoicingCustomers_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingCustomers_Fields"
				},
				"nativeInvoicingCustomers_List": {
					"nativeInvoicingCustomers": "nativeInvoicingCustomers"
				},
				"type": "string",
				"usage": "string"
			}
		},
		"nativeInvoicingEmailPreview": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingEmailPreview": "nativeInvoicingEmailPreview"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingEmailPreview_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingEmailPreview_List"
				},
				"Update": {
					"nativeInvoicingEmailPreview": "nativeInvoicingEmailPreview"
				},
				"UpdateMultiple": {
					"nativeInvoicingEmailPreview_List": "nativeInvoicingEmailPreview_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingEmailPreview_List": "nativeInvoicingEmailPreview_List"
				},
				"Update_Result": {
					"nativeInvoicingEmailPreview": "nativeInvoicingEmailPreview"
				},
				"nativeInvoicingEmailPreview": {
					"Key": "string",
					"bodyText": "string",
					"documentId": "string",
					"email": "string",
					"subject": "string"
				},
				"nativeInvoicingEmailPreview_Fields": "string",
				"nativeInvoicingEmailPreview_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingEmailPreview_Fields"
				},
				"nativeInvoicingEmailPreview_List": {
					"nativeInvoicingEmailPreview": "nativeInvoicingEmailPreview"
				}
			}
		},
		"nativeInvoicingEmailSetting": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingEmailSetting": "nativeInvoicingEmailSetting"
				},
				"CreateMultiple": {
					"nativeInvoicingEmailSetting_List": "nativeInvoicingEmailSetting_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingEmailSetting_List": "nativeInvoicingEmailSetting_List"
				},
				"Create_Result": {
					"nativeInvoicingEmailSetting": "nativeInvoicingEmailSetting"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"code": "string",
					"recipientType": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingEmailSetting": "nativeInvoicingEmailSetting"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingEmailSetting_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingEmailSetting_List"
				},
				"Read_Result": {
					"nativeInvoicingEmailSetting": "nativeInvoicingEmailSetting"
				},
				"Update": {
					"nativeInvoicingEmailSetting": "nativeInvoicingEmailSetting"
				},
				"UpdateMultiple": {
					"nativeInvoicingEmailSetting_List": "nativeInvoicingEmailSetting_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingEmailSetting_List": "nativeInvoicingEmailSetting_List"
				},
				"Update_Result": {
					"nativeInvoicingEmailSetting": "nativeInvoicingEmailSetting"
				},
				"nativeInvoicingEmailSetting": {
					"Key": "string",
					"code": "string",
					"eMail": "string",
					"recipientType": "recipientType"
				},
				"nativeInvoicingEmailSetting_Fields": "string",
				"nativeInvoicingEmailSetting_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingEmailSetting_Fields"
				},
				"nativeInvoicingEmailSetting_List": {
					"nativeInvoicingEmailSetting": "nativeInvoicingEmailSetting"
				},
				"recipientType": "string"
			}
		},
		"nativeInvoicingExportInvoices": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingExportInvoices": "nativeInvoicingExportInvoices"
				},
				"CreateMultiple": {
					"nativeInvoicingExportInvoices_List": "nativeInvoicingExportInvoices_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingExportInvoices_List": "nativeInvoicingExportInvoices_List"
				},
				"Create_Result": {
					"nativeInvoicingExportInvoices": "nativeInvoicingExportInvoices"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingExportInvoices": "nativeInvoicingExportInvoices"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingExportInvoices_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingExportInvoices_List"
				},
				"nativeInvoicingExportInvoices": {
					"Key": "string",
					"email": "string",
					"endDate": "date",
					"startDate": "date"
				},
				"nativeInvoicingExportInvoices_Fields": "string",
				"nativeInvoicingExportInvoices_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingExportInvoices_Fields"
				},
				"nativeInvoicingExportInvoices_List": {
					"nativeInvoicingExportInvoices": "nativeInvoicingExportInvoices"
				}
			}
		},
		"nativeInvoicingGeneralSettings": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"primaryKey": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingGeneralSettings": "nativeInvoicingGeneralSettings"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingGeneralSettings_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingGeneralSettings_List"
				},
				"Read_Result": {
					"nativeInvoicingGeneralSettings": "nativeInvoicingGeneralSettings"
				},
				"Update": {
					"nativeInvoicingGeneralSettings": "nativeInvoicingGeneralSettings"
				},
				"UpdateMultiple": {
					"nativeInvoicingGeneralSettings_List": "nativeInvoicingGeneralSettings_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingGeneralSettings_List": "nativeInvoicingGeneralSettings_List"
				},
				"Update_Result": {
					"nativeInvoicingGeneralSettings": "nativeInvoicingGeneralSettings"
				},
				"nativeInvoicingGeneralSettings": {
					"Key": "string",
					"amountRoundingPrecision": "decimal",
					"countryRegionCode": "string",
					"currencySymbol": "string",
					"defaultPaymentMethodDisplayName": "string",
					"defaultPaymentMethodId": "string",
					"defaultPaymentTermsDisplayName": "string",
					"defaultPaymentTermsId": "string",
					"defaultTaxDisplayName": "string",
					"defaultTaxId": "string",
					"draftInvoiceFileName": "string",
					"enableSyncCoupons": "boolean",
					"enableSynchronization": "boolean",
					"languageCode": "string",
					"languageDisplayName": "string",
					"languageId": "int",
					"nonTaxableGroupId": "string",
					"paypalEmailAddress": "string",
					"postedInvoiceFileName": "string",
					"primaryKey": "string",
					"quantityRoundingPrecision": "decimal",
					"quoteFileName": "string",
					"taxRoundingPrecision": "decimal",
					"taxableGroupId": "string",
					"unitAmountRoundingPrecision": "decimal",
					"updateVersion": "string"
				},
				"nativeInvoicingGeneralSettings_Fields": "string",
				"nativeInvoicingGeneralSettings_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingGeneralSettings_Fields"
				},
				"nativeInvoicingGeneralSettings_List": {
					"nativeInvoicingGeneralSettings": "nativeInvoicingGeneralSettings"
				}
			}
		},
		"nativeInvoicingItems": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingItems": "nativeInvoicingItems"
				},
				"CreateMultiple": {
					"nativeInvoicingItems_List": "nativeInvoicingItems_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingItems_List": "nativeInvoicingItems_List"
				},
				"Create_Result": {
					"nativeInvoicingItems": "nativeInvoicingItems"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingItems": "nativeInvoicingItems"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingItems_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingItems_List"
				},
				"Read_Result": {
					"nativeInvoicingItems": "nativeInvoicingItems"
				},
				"Update": {
					"nativeInvoicingItems": "nativeInvoicingItems"
				},
				"UpdateMultiple": {
					"nativeInvoicingItems_List": "nativeInvoicingItems_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingItems_List": "nativeInvoicingItems_List"
				},
				"Update_Result": {
					"nativeInvoicingItems": "nativeInvoicingItems"
				},
				"nativeInvoicingItems": {
					"Key": "string",
					"baseUnitOfMeasureDescription": "string",
					"baseUnitOfMeasureId": "string",
					"baseUnitOfMeasureIntStdCode": "string",
					"blocked": "boolean",
					"displayName": "string",
					"gtin": "string",
					"id": "string",
					"inventory": "decimal",
					"lastModifiedDateTime": "dateTime",
					"number": "string",
					"priceIncludesTax": "boolean",
					"taxGroupCode": "string",
					"taxGroupId": "string",
					"taxable": "boolean",
					"type": "type",
					"unitCost": "decimal",
					"unitPrice": "decimal"
				},
				"nativeInvoicingItems_Fields": "string",
				"nativeInvoicingItems_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingItems_Fields"
				},
				"nativeInvoicingItems_List": {
					"nativeInvoicingItems": "nativeInvoicingItems"
				},
				"type": "string"
			}
		},
		"nativeInvoicingKPIs": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"primaryKey": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingKPIs": "nativeInvoicingKPIs"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingKPIs_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingKPIs_List"
				},
				"Read_Result": {
					"nativeInvoicingKPIs": "nativeInvoicingKPIs"
				},
				"nativeInvoicingKPIs": {
					"Key": "string",
					"invoicedCurrentMonth": "decimal",
					"invoicedYearToDate": "decimal",
					"numberOfDraftInvoices": "int",
					"numberOfInvoicesYearToDate": "int",
					"numberOfQuotes": "int",
					"primaryKey": "string",
					"requestedDateTime": "dateTime",
					"salesInvoicesOutsdanding": "decimal",
					"salesInvoicesOverdue": "decimal"
				},
				"nativeInvoicingKPIs_Fields": "string",
				"nativeInvoicingKPIs_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingKPIs_Fields"
				},
				"nativeInvoicingKPIs_List": {
					"nativeInvoicingKPIs": "nativeInvoicingKPIs"
				}
			}
		},
		"nativeInvoicingLanguages": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"languageId": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingLanguages": "nativeInvoicingLanguages"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingLanguages_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingLanguages_List"
				},
				"Read_Result": {
					"nativeInvoicingLanguages": "nativeInvoicingLanguages"
				},
				"nativeInvoicingLanguages": {
					"Key": "string",
					"default": "boolean",
					"displayName": "string",
					"languageCode": "string",
					"languageId": "int"
				},
				"nativeInvoicingLanguages_Fields": "string",
				"nativeInvoicingLanguages_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingLanguages_Fields"
				},
				"nativeInvoicingLanguages_List": {
					"nativeInvoicingLanguages": "nativeInvoicingLanguages"
				}
			}
		},
		"nativeInvoicingPDFs": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingPDFs": "nativeInvoicingPDFs"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingPDFs_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingPDFs_List"
				},
				"nativeInvoicingPDFs": {
					"Key": "string",
					"documentId": "string",
					"fileName": "string"
				},
				"nativeInvoicingPDFs_Fields": "string",
				"nativeInvoicingPDFs_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingPDFs_Fields"
				},
				"nativeInvoicingPDFs_List": {
					"nativeInvoicingPDFs": "nativeInvoicingPDFs"
				}
			}
		},
		"nativeInvoicingPaymentMethods": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingPaymentMethods": "nativeInvoicingPaymentMethods"
				},
				"CreateMultiple": {
					"nativeInvoicingPaymentMethods_List": "nativeInvoicingPaymentMethods_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingPaymentMethods_List": "nativeInvoicingPaymentMethods_List"
				},
				"Create_Result": {
					"nativeInvoicingPaymentMethods": "nativeInvoicingPaymentMethods"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingPaymentMethods": "nativeInvoicingPaymentMethods"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingPaymentMethods_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingPaymentMethods_List"
				},
				"Read_Result": {
					"nativeInvoicingPaymentMethods": "nativeInvoicingPaymentMethods"
				},
				"Update": {
					"nativeInvoicingPaymentMethods": "nativeInvoicingPaymentMethods"
				},
				"UpdateMultiple": {
					"nativeInvoicingPaymentMethods_List": "nativeInvoicingPaymentMethods_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingPaymentMethods_List": "nativeInvoicingPaymentMethods_List"
				},
				"Update_Result": {
					"nativeInvoicingPaymentMethods": "nativeInvoicingPaymentMethods"
				},
				"nativeInvoicingPaymentMethods": {
					"Key": "string",
					"code": "string",
					"default": "boolean",
					"displayName": "string",
					"id": "string",
					"lastModifiedDateTime": "dateTime"
				},
				"nativeInvoicingPaymentMethods_Fields": "string",
				"nativeInvoicingPaymentMethods_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingPaymentMethods_Fields"
				},
				"nativeInvoicingPaymentMethods_List": {
					"nativeInvoicingPaymentMethods": "nativeInvoicingPaymentMethods"
				}
			}
		},
		"nativeInvoicingPaymentTerms": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingPaymentTerms": "nativeInvoicingPaymentTerms"
				},
				"CreateMultiple": {
					"nativeInvoicingPaymentTerms_List": "nativeInvoicingPaymentTerms_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingPaymentTerms_List": "nativeInvoicingPaymentTerms_List"
				},
				"Create_Result": {
					"nativeInvoicingPaymentTerms": "nativeInvoicingPaymentTerms"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingPaymentTerms": "nativeInvoicingPaymentTerms"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingPaymentTerms_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingPaymentTerms_List"
				},
				"Read_Result": {
					"nativeInvoicingPaymentTerms": "nativeInvoicingPaymentTerms"
				},
				"Update": {
					"nativeInvoicingPaymentTerms": "nativeInvoicingPaymentTerms"
				},
				"UpdateMultiple": {
					"nativeInvoicingPaymentTerms_List": "nativeInvoicingPaymentTerms_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingPaymentTerms_List": "nativeInvoicingPaymentTerms_List"
				},
				"Update_Result": {
					"nativeInvoicingPaymentTerms": "nativeInvoicingPaymentTerms"
				},
				"nativeInvoicingPaymentTerms": {
					"Key": "string",
					"calculateDiscountOnCreditMemos": "boolean",
					"code": "string",
					"default": "boolean",
					"discountDateCalculation": "string",
					"discountPercent": "decimal",
					"displayName": "string",
					"dueDateCalculation": "string",
					"id": "string",
					"lastModifiedDateTime": "dateTime"
				},
				"nativeInvoicingPaymentTerms_Fields": "string",
				"nativeInvoicingPaymentTerms_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingPaymentTerms_Fields"
				},
				"nativeInvoicingPaymentTerms_List": {
					"nativeInvoicingPaymentTerms": "nativeInvoicingPaymentTerms"
				}
			}
		},
		"nativeInvoicingQBOSyncAuth": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingQBOSyncAuth": "nativeInvoicingQBOSyncAuth"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingQBOSyncAuth_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingQBOSyncAuth_List"
				},
				"nativeInvoicingQBOSyncAuth": {
					"Key": "string",
					"authorizationUrl": "string"
				},
				"nativeInvoicingQBOSyncAuth_Fields": "string",
				"nativeInvoicingQBOSyncAuth_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingQBOSyncAuth_Fields"
				},
				"nativeInvoicingQBOSyncAuth_List": {
					"nativeInvoicingQBOSyncAuth": "nativeInvoicingQBOSyncAuth"
				}
			}
		},
		"nativeInvoicingSMTPMailSetup": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingSMTPMailSetup": "nativeInvoicingSMTPMailSetup"
				},
				"CreateMultiple": {
					"nativeInvoicingSMTPMailSetup_List": "nativeInvoicingSMTPMailSetup_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingSMTPMailSetup_List": "nativeInvoicingSMTPMailSetup_List"
				},
				"Create_Result": {
					"nativeInvoicingSMTPMailSetup": "nativeInvoicingSMTPMailSetup"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"primaryKey": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingSMTPMailSetup": "nativeInvoicingSMTPMailSetup"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingSMTPMailSetup_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingSMTPMailSetup_List"
				},
				"Read_Result": {
					"nativeInvoicingSMTPMailSetup": "nativeInvoicingSMTPMailSetup"
				},
				"Update": {
					"nativeInvoicingSMTPMailSetup": "nativeInvoicingSMTPMailSetup"
				},
				"UpdateMultiple": {
					"nativeInvoicingSMTPMailSetup_List": "nativeInvoicingSMTPMailSetup_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingSMTPMailSetup_List": "nativeInvoicingSMTPMailSetup_List"
				},
				"Update_Result": {
					"nativeInvoicingSMTPMailSetup": "nativeInvoicingSMTPMailSetup"
				},
				"authentication": "string",
				"nativeInvoicingSMTPMailSetup": {
					"Key": "string",
					"SMTPServer": "string",
					"SMTPServerPort": "int",
					"authentication": "authentication",
					"passWord": "string",
					"primaryKey": "string",
					"secureConnection": "boolean",
					"userName": "string"
				},
				"nativeInvoicingSMTPMailSetup_Fields": "string",
				"nativeInvoicingSMTPMailSetup_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingSMTPMailSetup_Fields"
				},
				"nativeInvoicingSMTPMailSetup_List": {
					"nativeInvoicingSMTPMailSetup": "nativeInvoicingSMTPMailSetup"
				}
			}
		},
		"nativeInvoicingSalesInvoiceOverview": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingSalesInvoiceOverview": "nativeInvoicingSalesInvoiceOverview"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingSalesInvoiceOverview_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingSalesInvoiceOverview_List"
				},
				"Read_Result": {
					"nativeInvoicingSalesInvoiceOverview": "nativeInvoicingSalesInvoiceOverview"
				},
				"nativeInvoicingSalesInvoiceOverview": {
					"Key": "string",
					"currencyCode": "string",
					"customerName": "string",
					"customerNumber": "string",
					"dueDate": "date",
					"id": "string",
					"invoiceDate": "date",
					"isTest": "boolean",
					"lastModifiedDateTime": "dateTime",
					"number": "string",
					"status": "status",
					"subtotalAmount": "decimal",
					"totalAmountExcludingTax": "decimal",
					"totalAmountIncludingTax": "decimal",
					"totalTaxAmount": "decimal"
				},
				"nativeInvoicingSalesInvoiceOverview_Fields": "string",
				"nativeInvoicingSalesInvoiceOverview_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingSalesInvoiceOverview_Fields"
				},
				"nativeInvoicingSalesInvoiceOverview_List": {
					"nativeInvoicingSalesInvoiceOverview": "nativeInvoicingSalesInvoiceOverview"
				},
				"status": "string"
			}
		},
		"nativeInvoicingSalesInvoices": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"Delete_Payments": {
					"i": "Delete_Payments",
					"o": "Delete_Payments_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingSalesInvoices": "nativeInvoicingSalesInvoices"
				},
				"CreateMultiple": {
					"nativeInvoicingSalesInvoices_List": "nativeInvoicingSalesInvoices_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingSalesInvoices_List": "nativeInvoicingSalesInvoices_List"
				},
				"Create_Result": {
					"nativeInvoicingSalesInvoices": "nativeInvoicingSalesInvoices"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Payments": {
					"Payments_Key": "string"
				},
				"Delete_Payments_Result": {
					"Delete_Payments_Result": "boolean"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Native_Payments": {
					"Key": "string",
					"amount": "decimal",
					"appliesToInvoiceId": "string",
					"customerId": "string",
					"paymentDate": "date",
					"paymentMethodId": "string",
					"paymentNo": "int"
				},
				"Native_Payments_List": {
					"Native_Payments": "Native_Payments"
				},
				"Read": {
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingSalesInvoices": "nativeInvoicingSalesInvoices"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingSalesInvoices_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingSalesInvoices_List"
				},
				"Read_Result": {
					"nativeInvoicingSalesInvoices": "nativeInvoicingSalesInvoices"
				},
				"Update": {
					"nativeInvoicingSalesInvoices": "nativeInvoicingSalesInvoices"
				},
				"UpdateMultiple": {
					"nativeInvoicingSalesInvoices_List": "nativeInvoicingSalesInvoices_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingSalesInvoices_List": "nativeInvoicingSalesInvoices_List"
				},
				"Update_Result": {
					"nativeInvoicingSalesInvoices": "nativeInvoicingSalesInvoices"
				},
				"invoiceDiscountCalculation": "string",
				"nativeInvoicingSalesInvoices": {
					"Key": "string",
					"Payments": "Native_Payments_List",
					"attachments": "string",
					"billingPostalAddress": "string",
					"coupons": "string",
					"currencyCode": "string",
					"customerEmail": "string",
					"customerId": "string",
					"customerName": "string",
					"customerNumber": "string",
					"discountAmount": "decimal",
					"discountAppliedBeforeTax": "boolean",
					"dueDate": "date",
					"graphContactId": "string",
					"id": "string",
					"invoiceDate": "date",
					"invoiceDiscountCalculation": "invoiceDiscountCalculation",
					"invoiceDiscountValue": "decimal",
					"isCustomerBlocked": "boolean",
					"isTest": "boolean",
					"lastEmailSentTime": "dateTime",
					"lastModifiedDateTime": "dateTime",
					"lines": "string",
					"noteForCustomer": "string",
					"number": "string",
					"pricesIncludeTax": "boolean",
					"remainingAmount": "decimal",
					"status": "status",
					"subtotalAmount": "decimal",
					"taxAreaDisplayName": "string",
					"taxAreaId": "string",
					"taxLiable": "boolean",
					"taxRegistrationNumber": "string",
					"totalAmountExcludingTax": "decimal",
					"totalAmountIncludingTax": "decimal",
					"totalTaxAmount": "decimal"
				},
				"nativeInvoicingSalesInvoices_Fields": "string",
				"nativeInvoicingSalesInvoices_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingSalesInvoices_Fields"
				},
				"nativeInvoicingSalesInvoices_List": {
					"nativeInvoicingSalesInvoices": "nativeInvoicingSalesInvoices"
				},
				"status": "string"
			}
		},
		"nativeInvoicingSalesQuotes": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingSalesQuotes": "nativeInvoicingSalesQuotes"
				},
				"CreateMultiple": {
					"nativeInvoicingSalesQuotes_List": "nativeInvoicingSalesQuotes_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingSalesQuotes_List": "nativeInvoicingSalesQuotes_List"
				},
				"Create_Result": {
					"nativeInvoicingSalesQuotes": "nativeInvoicingSalesQuotes"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingSalesQuotes": "nativeInvoicingSalesQuotes"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingSalesQuotes_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingSalesQuotes_List"
				},
				"Read_Result": {
					"nativeInvoicingSalesQuotes": "nativeInvoicingSalesQuotes"
				},
				"Update": {
					"nativeInvoicingSalesQuotes": "nativeInvoicingSalesQuotes"
				},
				"UpdateMultiple": {
					"nativeInvoicingSalesQuotes_List": "nativeInvoicingSalesQuotes_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingSalesQuotes_List": "nativeInvoicingSalesQuotes_List"
				},
				"Update_Result": {
					"nativeInvoicingSalesQuotes": "nativeInvoicingSalesQuotes"
				},
				"invoiceDiscountCalculation": "string",
				"nativeInvoicingSalesQuotes": {
					"Key": "string",
					"accepted": "boolean",
					"acceptedDate": "date",
					"attachments": "string",
					"billingPostalAddress": "string",
					"coupons": "string",
					"currencyCode": "string",
					"customerEmail": "string",
					"customerId": "string",
					"customerName": "string",
					"customerNumber": "string",
					"discountAmount": "decimal",
					"discountAppliedBeforeTax": "boolean",
					"dueDate": "date",
					"graphContactId": "string",
					"id": "string",
					"invoiceDiscountCalculation": "invoiceDiscountCalculation",
					"invoiceDiscountValue": "decimal",
					"isCustomerBlocked": "boolean",
					"lastEmailSentTime": "dateTime",
					"lastModifiedDateTime": "dateTime",
					"lines": "string",
					"noteForCustomer": "string",
					"number": "string",
					"pricesIncludeTax": "boolean",
					"quoteDate": "date",
					"salesperson": "string",
					"shipmentMethod": "string",
					"status": "status",
					"subtotalAmount": "decimal",
					"taxAreaDisplayName": "string",
					"taxAreaId": "string",
					"taxLiable": "boolean",
					"taxRegistrationNumber": "string",
					"totalAmountExcludingTax": "decimal",
					"totalAmountIncludingTax": "decimal",
					"totalTaxAmount": "decimal",
					"validUntilDate": "date"
				},
				"nativeInvoicingSalesQuotes_Fields": "string",
				"nativeInvoicingSalesQuotes_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingSalesQuotes_Fields"
				},
				"nativeInvoicingSalesQuotes_List": {
					"nativeInvoicingSalesQuotes": "nativeInvoicingSalesQuotes"
				},
				"status": "string"
			}
		},
		"nativeInvoicingSalesTaxSetup": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingSalesTaxSetup": "nativeInvoicingSalesTaxSetup"
				},
				"CreateMultiple": {
					"nativeInvoicingSalesTaxSetup_List": "nativeInvoicingSalesTaxSetup_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingSalesTaxSetup_List": "nativeInvoicingSalesTaxSetup_List"
				},
				"Create_Result": {
					"nativeInvoicingSalesTaxSetup": "nativeInvoicingSalesTaxSetup"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"id": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingSalesTaxSetup": "nativeInvoicingSalesTaxSetup"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingSalesTaxSetup_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingSalesTaxSetup_List"
				},
				"Read_Result": {
					"nativeInvoicingSalesTaxSetup": "nativeInvoicingSalesTaxSetup"
				},
				"Update": {
					"nativeInvoicingSalesTaxSetup": "nativeInvoicingSalesTaxSetup"
				},
				"UpdateMultiple": {
					"nativeInvoicingSalesTaxSetup_List": "nativeInvoicingSalesTaxSetup_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingSalesTaxSetup_List": "nativeInvoicingSalesTaxSetup_List"
				},
				"Update_Result": {
					"nativeInvoicingSalesTaxSetup": "nativeInvoicingSalesTaxSetup"
				},
				"nativeInvoicingSalesTaxSetup": {
					"Key": "string",
					"canadaGstHstDescription": "string",
					"canadaGstHstRate": "decimal",
					"canadaPstDescription": "string",
					"canadaPstRate": "decimal",
					"city": "string",
					"cityRate": "decimal",
					"default": "boolean",
					"displayName": "string",
					"id": "string",
					"lastModifiedDateTime": "dateTime",
					"state": "string",
					"stateRate": "decimal"
				},
				"nativeInvoicingSalesTaxSetup_Fields": "string",
				"nativeInvoicingSalesTaxSetup_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingSalesTaxSetup_Fields"
				},
				"nativeInvoicingSalesTaxSetup_List": {
					"nativeInvoicingSalesTaxSetup": "nativeInvoicingSalesTaxSetup"
				}
			}
		},
		"nativeInvoicingSyncServicesSetting": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingSyncServicesSetting": "nativeInvoicingSyncServicesSetting"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingSyncServicesSetting_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingSyncServicesSetting_List"
				},
				"Update": {
					"nativeInvoicingSyncServicesSetting": "nativeInvoicingSyncServicesSetting"
				},
				"UpdateMultiple": {
					"nativeInvoicingSyncServicesSetting_List": "nativeInvoicingSyncServicesSetting_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingSyncServicesSetting_List": "nativeInvoicingSyncServicesSetting_List"
				},
				"Update_Result": {
					"nativeInvoicingSyncServicesSetting": "nativeInvoicingSyncServicesSetting"
				},
				"nativeInvoicingSyncServicesSetting": {
					"Key": "string",
					"qbdSyncDescription": "string",
					"qbdSyncEnabled": "boolean",
					"qbdSyncSendToEmail": "string",
					"qbdSyncTitle": "string",
					"qboSyncDescription": "string",
					"qboSyncEnabled": "boolean",
					"qboSyncTitle": "string"
				},
				"nativeInvoicingSyncServicesSetting_Fields": "string",
				"nativeInvoicingSyncServicesSetting_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingSyncServicesSetting_Fields"
				},
				"nativeInvoicingSyncServicesSetting_List": {
					"nativeInvoicingSyncServicesSetting": "nativeInvoicingSyncServicesSetting"
				}
			}
		},
		"nativeInvoicingTaxAreas": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingTaxAreas": "nativeInvoicingTaxAreas"
				},
				"CreateMultiple": {
					"nativeInvoicingTaxAreas_List": "nativeInvoicingTaxAreas_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingTaxAreas_List": "nativeInvoicingTaxAreas_List"
				},
				"Create_Result": {
					"nativeInvoicingTaxAreas": "nativeInvoicingTaxAreas"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"id": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingTaxAreas": "nativeInvoicingTaxAreas"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingTaxAreas_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingTaxAreas_List"
				},
				"Read_Result": {
					"nativeInvoicingTaxAreas": "nativeInvoicingTaxAreas"
				},
				"Update": {
					"nativeInvoicingTaxAreas": "nativeInvoicingTaxAreas"
				},
				"UpdateMultiple": {
					"nativeInvoicingTaxAreas_List": "nativeInvoicingTaxAreas_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingTaxAreas_List": "nativeInvoicingTaxAreas_List"
				},
				"Update_Result": {
					"nativeInvoicingTaxAreas": "nativeInvoicingTaxAreas"
				},
				"nativeInvoicingTaxAreas": {
					"Key": "string",
					"code": "string",
					"displayName": "string",
					"id": "string",
					"lastModifiedDateTime": "dateTime",
					"taxType": "taxType"
				},
				"nativeInvoicingTaxAreas_Fields": "string",
				"nativeInvoicingTaxAreas_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingTaxAreas_Fields"
				},
				"nativeInvoicingTaxAreas_List": {
					"nativeInvoicingTaxAreas": "nativeInvoicingTaxAreas"
				},
				"taxType": "string"
			}
		},
		"nativeInvoicingTaxGroups": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"id": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingTaxGroups": "nativeInvoicingTaxGroups"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingTaxGroups_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingTaxGroups_List"
				},
				"Read_Result": {
					"nativeInvoicingTaxGroups": "nativeInvoicingTaxGroups"
				},
				"Update": {
					"nativeInvoicingTaxGroups": "nativeInvoicingTaxGroups"
				},
				"UpdateMultiple": {
					"nativeInvoicingTaxGroups_List": "nativeInvoicingTaxGroups_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingTaxGroups_List": "nativeInvoicingTaxGroups_List"
				},
				"Update_Result": {
					"nativeInvoicingTaxGroups": "nativeInvoicingTaxGroups"
				},
				"nativeInvoicingTaxGroups": {
					"Key": "string",
					"code": "string",
					"displayName": "string",
					"id": "string",
					"lastModifiedDateTime": "dateTime",
					"taxType": "taxType"
				},
				"nativeInvoicingTaxGroups_Fields": "string",
				"nativeInvoicingTaxGroups_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingTaxGroups_Fields"
				},
				"nativeInvoicingTaxGroups_List": {
					"nativeInvoicingTaxGroups": "nativeInvoicingTaxGroups"
				},
				"taxType": "string"
			}
		},
		"nativeInvoicingTaxRates": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"taxAreaId": "string",
					"taxGroupId": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingTaxRates": "nativeInvoicingTaxRates"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingTaxRates_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingTaxRates_List"
				},
				"Read_Result": {
					"nativeInvoicingTaxRates": "nativeInvoicingTaxRates"
				},
				"nativeInvoicingTaxRates": {
					"Key": "string",
					"taxAreaId": "string",
					"taxGroupId": "string",
					"taxRate": "decimal"
				},
				"nativeInvoicingTaxRates_Fields": "string",
				"nativeInvoicingTaxRates_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingTaxRates_Fields"
				},
				"nativeInvoicingTaxRates_List": {
					"nativeInvoicingTaxRates": "nativeInvoicingTaxRates"
				}
			}
		},
		"nativeInvoicingTestMail": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingTestMail": "nativeInvoicingTestMail"
				},
				"CreateMultiple": {
					"nativeInvoicingTestMail_List": "nativeInvoicingTestMail_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingTestMail_List": "nativeInvoicingTestMail_List"
				},
				"Create_Result": {
					"nativeInvoicingTestMail": "nativeInvoicingTestMail"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingTestMail": "nativeInvoicingTestMail"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingTestMail_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingTestMail_List"
				},
				"nativeInvoicingTestMail": {
					"Key": "string",
					"email": "string"
				},
				"nativeInvoicingTestMail_Fields": "string",
				"nativeInvoicingTestMail_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingTestMail_Fields"
				},
				"nativeInvoicingTestMail_List": {
					"nativeInvoicingTestMail": "nativeInvoicingTestMail"
				}
			}
		},
		"nativeInvoicingUnitsOfMeasure": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingUnitsOfMeasure": "nativeInvoicingUnitsOfMeasure"
				},
				"CreateMultiple": {
					"nativeInvoicingUnitsOfMeasure_List": "nativeInvoicingUnitsOfMeasure_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingUnitsOfMeasure_List": "nativeInvoicingUnitsOfMeasure_List"
				},
				"Create_Result": {
					"nativeInvoicingUnitsOfMeasure": "nativeInvoicingUnitsOfMeasure"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingUnitsOfMeasure": "nativeInvoicingUnitsOfMeasure"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingUnitsOfMeasure_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingUnitsOfMeasure_List"
				},
				"Read_Result": {
					"nativeInvoicingUnitsOfMeasure": "nativeInvoicingUnitsOfMeasure"
				},
				"Update": {
					"nativeInvoicingUnitsOfMeasure": "nativeInvoicingUnitsOfMeasure"
				},
				"UpdateMultiple": {
					"nativeInvoicingUnitsOfMeasure_List": "nativeInvoicingUnitsOfMeasure_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingUnitsOfMeasure_List": "nativeInvoicingUnitsOfMeasure_List"
				},
				"Update_Result": {
					"nativeInvoicingUnitsOfMeasure": "nativeInvoicingUnitsOfMeasure"
				},
				"nativeInvoicingUnitsOfMeasure": {
					"Key": "string",
					"code": "string",
					"displayName": "string",
					"id": "string",
					"internationalStandardCode": "string",
					"lastModifiedDateTime": "dateTime"
				},
				"nativeInvoicingUnitsOfMeasure_Fields": "string",
				"nativeInvoicingUnitsOfMeasure_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingUnitsOfMeasure_Fields"
				},
				"nativeInvoicingUnitsOfMeasure_List": {
					"nativeInvoicingUnitsOfMeasure": "nativeInvoicingUnitsOfMeasure"
				}
			}
		},
		"nativeInvoicingVATSetup": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"id": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingVATSetup": "nativeInvoicingVATSetup"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingVATSetup_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingVATSetup_List"
				},
				"Read_Result": {
					"nativeInvoicingVATSetup": "nativeInvoicingVATSetup"
				},
				"Update": {
					"nativeInvoicingVATSetup": "nativeInvoicingVATSetup"
				},
				"UpdateMultiple": {
					"nativeInvoicingVATSetup_List": "nativeInvoicingVATSetup_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingVATSetup_List": "nativeInvoicingVATSetup_List"
				},
				"Update_Result": {
					"nativeInvoicingVATSetup": "nativeInvoicingVATSetup"
				},
				"nativeInvoicingVATSetup": {
					"Key": "string",
					"default": "boolean",
					"displayName": "string",
					"id": "string",
					"lastModifiedDateTime": "dateTime",
					"vatPercentage": "decimal",
					"vatRegulationDescription": "string"
				},
				"nativeInvoicingVATSetup_Fields": "string",
				"nativeInvoicingVATSetup_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingVATSetup_Fields"
				},
				"nativeInvoicingVATSetup_List": {
					"nativeInvoicingVATSetup": "nativeInvoicingVATSetup"
				}
			}
		},
		"powerbifinance": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"No": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"powerbifinance": "powerbifinance"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "powerbifinance_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "powerbifinance_List"
				},
				"Read_Result": {
					"powerbifinance": "powerbifinance"
				},
				"powerbifinance": {
					"Account_Schedule_Name": "string",
					"Balance_at_Date_Actual": "decimal",
					"Balance_at_Date_Actual_Last_Year": "decimal",
					"Balance_at_Date_Budget": "decimal",
					"Balance_at_Date_Budget_Last_Year": "decimal",
					"Balance_at_Date_Forecast": "decimal",
					"Closed_Period": "boolean",
					"Date": "date",
					"KPI_Code": "string",
					"KPI_Name": "string",
					"Key": "string",
					"Net_Change_Actual": "decimal",
					"Net_Change_Actual_Last_Year": "decimal",
					"Net_Change_Budget": "decimal",
					"Net_Change_Budget_Last_Year": "decimal",
					"Net_Change_Forecast": "decimal",
					"No": "int"
				},
				"powerbifinance_Fields": "string",
				"powerbifinance_Filter": {
					"Criteria": "string",
					"Field": "powerbifinance_Fields"
				},
				"powerbifinance_List": {
					"powerbifinance": "powerbifinance"
				}
			}
		},
		"purchaseDocumentLines": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"purchaseDocuments": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"salesDocumentLines": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"salesDocuments": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"workflowCustomers": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"workflowGenJournalBatches": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"workflowGenJournalLines": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"workflowItems": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"workflowPurchaseDocumentLines": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"workflowPurchaseDocuments": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"workflowSalesDocumentLines": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"workflowSalesDocuments": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"workflowVendors": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		},
		"workflowWebhookSubscriptions": {
			"f": {
				"err": "",
				"s": false
			},
			"sp": "Page",
			"t": {
				"err": "",
				"s": false
			}
		}
	}
);

Services.nav = nav;

const nav_odata = new NavisionConnector(
	'nav_odata', 
	{
		"company": "Orangekloud",
		"local": false,
		"navconnector": "",
		"password": "",
		"timeOut": "600000",
		"type": "odatav4",
		"url": "https://api.businesscentral.dynamics.com/v1.0/425096bb-f087-4c7d-ac68-7ac4ca13116d/Sandbox",
		"user": ""
	}, 
	{
		"AccountantPortalActivityCues": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"AccountantPortalActivityCues": {
					"AverageCollectionDaysAmount": "string",
					"ContactNameAmount": "string",
					"IncDocAwaitingVerifAmount": "string",
					"Key": "string",
					"MyIncomingDocumentsAmount": "string",
					"NonAppliedPaymentsAmount": "string",
					"OngoingPurchaseInvoicesAmount": "string",
					"OngoingSalesInvoicesAmount": "string",
					"OngoingSalesOrdersAmount": "string",
					"OngoingSalesQuotesAmount": "string",
					"OverduePurchInvoiceAmount": "string",
					"OverdueSalesInvoiceAmount": "string",
					"PurchInvoicesDueNextWeekAmount": "string",
					"PurchaseOrdersAmount": "string",
					"RequeststoApproveAmount": "string",
					"SalesCrMPendDocExchangeAmount": "string",
					"SalesInvPendDocExchangeAmount": "string",
					"SalesInvoicesDueNextWeekAmount": "string",
					"SalesThisMonthAmount": "string",
					"Top10CustomerSalesYTDAmount": "string"
				},
				"AccountantPortalActivityCues_Fields": "string",
				"AccountantPortalActivityCues_Filter": {
					"Criteria": "string",
					"Field": "AccountantPortalActivityCues_Fields"
				},
				"AccountantPortalActivityCues_List": {
					"AccountantPortalActivityCues": "AccountantPortalActivityCues"
				},
				"Create": {
					"AccountantPortalActivityCues": "AccountantPortalActivityCues"
				},
				"CreateMultiple": {
					"AccountantPortalActivityCues_List": "AccountantPortalActivityCues_List"
				},
				"CreateMultiple_Result": {
					"AccountantPortalActivityCues_List": "AccountantPortalActivityCues_List"
				},
				"Create_Result": {
					"AccountantPortalActivityCues": "AccountantPortalActivityCues"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"AccountantPortalActivityCues": "AccountantPortalActivityCues"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "AccountantPortalActivityCues_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "AccountantPortalActivityCues_List"
				},
				"Update": {
					"AccountantPortalActivityCues": "AccountantPortalActivityCues"
				},
				"UpdateMultiple": {
					"AccountantPortalActivityCues_List": "AccountantPortalActivityCues_List"
				},
				"UpdateMultiple_Result": {
					"AccountantPortalActivityCues_List": "AccountantPortalActivityCues_List"
				},
				"Update_Result": {
					"AccountantPortalActivityCues": "AccountantPortalActivityCues"
				}
			}
		},
		"AccountantPortalFinanceCues": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"AccountantPortalFinanceCues": {
					"ApprovedIncomingDocumentsAmount": "string",
					"ApprovedPurchaseOrdersAmount": "string",
					"ApprovedSalesOrdersAmount": "string",
					"CashAccountsBalanceAmount": "string",
					"CustomersBlockedAmount": "string",
					"Key": "string",
					"LastDepreciatedPostedDateAmount": "string",
					"LastLoginDateAmount": "string",
					"NewIncomingDocumentsAmount": "string",
					"OCRCompletedAmount": "string",
					"OCRPendingAmount": "string",
					"OverduePurchaseDocumentsAmount": "string",
					"OverdueSalesDocumentsAmount": "string",
					"POsPendingApprovalAmount": "string",
					"PurchaseDiscountsNextWeekAmount": "string",
					"PurchaseDocumentsDueTodayAmount": "string",
					"PurchaseReturnOrdersAmount": "string",
					"RequestsSentForApprovalAmount": "string",
					"RequestsToApproveAmount": "string",
					"SOsPendingApprovalAmount": "string",
					"SalesReturnOrdersAllAmount": "string",
					"VendorsPaymentsOnHoldAmount": "string"
				},
				"AccountantPortalFinanceCues_Fields": "string",
				"AccountantPortalFinanceCues_Filter": {
					"Criteria": "string",
					"Field": "AccountantPortalFinanceCues_Fields"
				},
				"AccountantPortalFinanceCues_List": {
					"AccountantPortalFinanceCues": "AccountantPortalFinanceCues"
				},
				"Create": {
					"AccountantPortalFinanceCues": "AccountantPortalFinanceCues"
				},
				"CreateMultiple": {
					"AccountantPortalFinanceCues_List": "AccountantPortalFinanceCues_List"
				},
				"CreateMultiple_Result": {
					"AccountantPortalFinanceCues_List": "AccountantPortalFinanceCues_List"
				},
				"Create_Result": {
					"AccountantPortalFinanceCues": "AccountantPortalFinanceCues"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"AccountantPortalFinanceCues": "AccountantPortalFinanceCues"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "AccountantPortalFinanceCues_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "AccountantPortalFinanceCues_List"
				},
				"Update": {
					"AccountantPortalFinanceCues": "AccountantPortalFinanceCues"
				},
				"UpdateMultiple": {
					"AccountantPortalFinanceCues_List": "AccountantPortalFinanceCues_List"
				},
				"UpdateMultiple_Result": {
					"AccountantPortalFinanceCues_List": "AccountantPortalFinanceCues_List"
				},
				"Update_Result": {
					"AccountantPortalFinanceCues": "AccountantPortalFinanceCues"
				}
			}
		},
		"AccountantPortalUserTasks": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"AccountantPortalUserTasks": {
					"Assigned_To": "string",
					"Created_By_Name": "string",
					"Created_DateTime": "dateTime",
					"Due_DateTime": "dateTime",
					"ID": "int",
					"Key": "string",
					"Link": "string",
					"Percent_Complete": "int",
					"Priority": "Priority",
					"Start_DateTime": "dateTime",
					"Title": "string",
					"User_Task_Group_Assigned_To": "string"
				},
				"AccountantPortalUserTasks_Fields": "string",
				"AccountantPortalUserTasks_Filter": {
					"Criteria": "string",
					"Field": "AccountantPortalUserTasks_Fields"
				},
				"AccountantPortalUserTasks_List": {
					"AccountantPortalUserTasks": "AccountantPortalUserTasks"
				},
				"Create": {
					"AccountantPortalUserTasks": "AccountantPortalUserTasks"
				},
				"CreateMultiple": {
					"AccountantPortalUserTasks_List": "AccountantPortalUserTasks_List"
				},
				"CreateMultiple_Result": {
					"AccountantPortalUserTasks_List": "AccountantPortalUserTasks_List"
				},
				"Create_Result": {
					"AccountantPortalUserTasks": "AccountantPortalUserTasks"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Priority": "string",
				"Read": {
					"ID": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"AccountantPortalUserTasks": "AccountantPortalUserTasks"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "AccountantPortalUserTasks_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "AccountantPortalUserTasks_List"
				},
				"Read_Result": {
					"AccountantPortalUserTasks": "AccountantPortalUserTasks"
				},
				"Update": {
					"AccountantPortalUserTasks": "AccountantPortalUserTasks"
				},
				"UpdateMultiple": {
					"AccountantPortalUserTasks_List": "AccountantPortalUserTasks_List"
				},
				"UpdateMultiple_Result": {
					"AccountantPortalUserTasks_List": "AccountantPortalUserTasks_List"
				},
				"Update_Result": {
					"AccountantPortalUserTasks": "AccountantPortalUserTasks"
				}
			}
		},
		"C2Graph": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"C2Graph": {
					"Component": "string",
					"Key": "string",
					"Schema": "string",
					"Type": "string"
				},
				"C2Graph_Fields": "string",
				"C2Graph_Filter": {
					"Criteria": "string",
					"Field": "C2Graph_Fields"
				},
				"C2Graph_List": {
					"C2Graph": "C2Graph"
				},
				"Create": {
					"C2Graph": "C2Graph"
				},
				"CreateMultiple": {
					"C2Graph_List": "C2Graph_List"
				},
				"CreateMultiple_Result": {
					"C2Graph_List": "C2Graph_List"
				},
				"Create_Result": {
					"C2Graph": "C2Graph"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"Component": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"C2Graph": "C2Graph"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "C2Graph_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "C2Graph_List"
				},
				"Read_Result": {
					"C2Graph": "C2Graph"
				},
				"Update": {
					"C2Graph": "C2Graph"
				},
				"UpdateMultiple": {
					"C2Graph_List": "C2Graph_List"
				},
				"UpdateMultiple_Result": {
					"C2Graph_List": "C2Graph_List"
				},
				"Update_Result": {
					"C2Graph": "C2Graph"
				}
			}
		},
		"Chart_of_Accounts": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Account_Category": "string",
				"Account_Type": "string",
				"Chart_of_Accounts": {
					"Account_Category": "Account_Category",
					"Account_Subcategory_Descript": "string",
					"Account_Type": "Account_Type",
					"Add_Currency_Balance_at_Date": "decimal",
					"Additional_Currency_Balance": "decimal",
					"Additional_Currency_Net_Change": "decimal",
					"Balance": "decimal",
					"Balance_at_Date": "decimal",
					"Consol_Credit_Acc": "string",
					"Consol_Debit_Acc": "string",
					"Consol_Translation_Method": "Consol_Translation_Method",
					"Cost_Type_No": "string",
					"Credit_Amount": "decimal",
					"Debit_Amount": "decimal",
					"Default_Deferral_Template_Code": "string",
					"Default_IC_Partner_G_L_Acc_No": "string",
					"Direct_Posting": "boolean",
					"Gen_Bus_Posting_Group": "string",
					"Gen_Posting_Type": "Gen_Posting_Type",
					"Gen_Prod_Posting_Group": "string",
					"Income_Balance": "Income_Balance",
					"Key": "string",
					"Name": "string",
					"Net_Change": "decimal",
					"No": "string",
					"Totaling": "string",
					"VAT_Bus_Posting_Group": "string",
					"VAT_Prod_Posting_Group": "string"
				},
				"Chart_of_Accounts_Fields": "string",
				"Chart_of_Accounts_Filter": {
					"Criteria": "string",
					"Field": "Chart_of_Accounts_Fields"
				},
				"Chart_of_Accounts_List": {
					"Chart_of_Accounts": "Chart_of_Accounts"
				},
				"Consol_Translation_Method": "string",
				"Create": {
					"Chart_of_Accounts": "Chart_of_Accounts"
				},
				"CreateMultiple": {
					"Chart_of_Accounts_List": "Chart_of_Accounts_List"
				},
				"CreateMultiple_Result": {
					"Chart_of_Accounts_List": "Chart_of_Accounts_List"
				},
				"Create_Result": {
					"Chart_of_Accounts": "Chart_of_Accounts"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Gen_Posting_Type": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"Income_Balance": "string",
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"Chart_of_Accounts": "Chart_of_Accounts"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "Chart_of_Accounts_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "Chart_of_Accounts_List"
				},
				"Read_Result": {
					"Chart_of_Accounts": "Chart_of_Accounts"
				},
				"Update": {
					"Chart_of_Accounts": "Chart_of_Accounts"
				},
				"UpdateMultiple": {
					"Chart_of_Accounts_List": "Chart_of_Accounts_List"
				},
				"UpdateMultiple_Result": {
					"Chart_of_Accounts_List": "Chart_of_Accounts_List"
				},
				"Update_Result": {
					"Chart_of_Accounts": "Chart_of_Accounts"
				}
			}
		},
		"CompanySetupService": {
			"f": {
				"ConfigureCompany": {
					"i": "ConfigureCompany",
					"o": "ConfigureCompany_Result"
				}
			},
			"sp": "Codeunit",
			"t": {
				"ConfigureCompany": {
					"address": "string",
					"address2": "string",
					"city": "string",
					"countryCode": "string",
					"county": "string",
					"name": "string",
					"phoneNo": "string",
					"postCode": "string"
				},
				"ConfigureCompany_Result": {
					"return_value": "boolean"
				}
			}
		},
		"ExcelTemplateAgedAccountsPayable": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"ExcelTemplateAgedAccountsPayable": {
					"Key": "string",
					"agedAsOfDate": "date",
					"balanceDue": "decimal",
					"currencyCode": "string",
					"currentAmount": "decimal",
					"name": "string",
					"period1Amount": "decimal",
					"period2Amount": "decimal",
					"period3Amount": "decimal",
					"periodLengthFilter": "string",
					"vendorId": "string",
					"vendorNumber": "string"
				},
				"ExcelTemplateAgedAccountsPayable_Fields": "string",
				"ExcelTemplateAgedAccountsPayable_Filter": {
					"Criteria": "string",
					"Field": "ExcelTemplateAgedAccountsPayable_Fields"
				},
				"ExcelTemplateAgedAccountsPayable_List": {
					"ExcelTemplateAgedAccountsPayable": "ExcelTemplateAgedAccountsPayable"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"vendorId": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"ExcelTemplateAgedAccountsPayable": "ExcelTemplateAgedAccountsPayable"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "ExcelTemplateAgedAccountsPayable_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "ExcelTemplateAgedAccountsPayable_List"
				},
				"Read_Result": {
					"ExcelTemplateAgedAccountsPayable": "ExcelTemplateAgedAccountsPayable"
				}
			}
		},
		"ExcelTemplateAgedAccountsReceivable": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"ExcelTemplateAgedAccountsReceivable": {
					"Key": "string",
					"agedAsOfDate": "date",
					"balanceDue": "decimal",
					"currencyCode": "string",
					"currentAmount": "decimal",
					"customerId": "string",
					"customerNumber": "string",
					"name": "string",
					"period1Amount": "decimal",
					"period2Amount": "decimal",
					"period3Amount": "decimal",
					"periodLengthFilter": "string"
				},
				"ExcelTemplateAgedAccountsReceivable_Fields": "string",
				"ExcelTemplateAgedAccountsReceivable_Filter": {
					"Criteria": "string",
					"Field": "ExcelTemplateAgedAccountsReceivable_Fields"
				},
				"ExcelTemplateAgedAccountsReceivable_List": {
					"ExcelTemplateAgedAccountsReceivable": "ExcelTemplateAgedAccountsReceivable"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"customerId": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"ExcelTemplateAgedAccountsReceivable": "ExcelTemplateAgedAccountsReceivable"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "ExcelTemplateAgedAccountsReceivable_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "ExcelTemplateAgedAccountsReceivable_List"
				},
				"Read_Result": {
					"ExcelTemplateAgedAccountsReceivable": "ExcelTemplateAgedAccountsReceivable"
				}
			}
		},
		"ExcelTemplateBalanceSheet": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"ExcelTemplateBalanceSheet": {
					"Key": "string",
					"balance": "decimal",
					"dateFilter": "date",
					"display": "string",
					"indentation": "int",
					"lineNumber": "int",
					"lineType": "string"
				},
				"ExcelTemplateBalanceSheet_Fields": "string",
				"ExcelTemplateBalanceSheet_Filter": {
					"Criteria": "string",
					"Field": "ExcelTemplateBalanceSheet_Fields"
				},
				"ExcelTemplateBalanceSheet_List": {
					"ExcelTemplateBalanceSheet": "ExcelTemplateBalanceSheet"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"lineNumber": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"ExcelTemplateBalanceSheet": "ExcelTemplateBalanceSheet"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "ExcelTemplateBalanceSheet_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "ExcelTemplateBalanceSheet_List"
				},
				"Read_Result": {
					"ExcelTemplateBalanceSheet": "ExcelTemplateBalanceSheet"
				}
			}
		},
		"ExcelTemplateCashFlowStatement": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"ExcelTemplateCashFlowStatement": {
					"Key": "string",
					"dateFilter": "date",
					"display": "string",
					"indentation": "int",
					"lineNumber": "int",
					"lineType": "string",
					"netChange": "decimal"
				},
				"ExcelTemplateCashFlowStatement_Fields": "string",
				"ExcelTemplateCashFlowStatement_Filter": {
					"Criteria": "string",
					"Field": "ExcelTemplateCashFlowStatement_Fields"
				},
				"ExcelTemplateCashFlowStatement_List": {
					"ExcelTemplateCashFlowStatement": "ExcelTemplateCashFlowStatement"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"lineNumber": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"ExcelTemplateCashFlowStatement": "ExcelTemplateCashFlowStatement"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "ExcelTemplateCashFlowStatement_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "ExcelTemplateCashFlowStatement_List"
				},
				"Read_Result": {
					"ExcelTemplateCashFlowStatement": "ExcelTemplateCashFlowStatement"
				}
			}
		},
		"ExcelTemplateIncomeStatement": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"ExcelTemplateIncomeStatement": {
					"Key": "string",
					"dateFilter": "date",
					"display": "string",
					"indentation": "int",
					"lineNumber": "int",
					"lineType": "string",
					"netChange": "decimal"
				},
				"ExcelTemplateIncomeStatement_Fields": "string",
				"ExcelTemplateIncomeStatement_Filter": {
					"Criteria": "string",
					"Field": "ExcelTemplateIncomeStatement_Fields"
				},
				"ExcelTemplateIncomeStatement_List": {
					"ExcelTemplateIncomeStatement": "ExcelTemplateIncomeStatement"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"lineNumber": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"ExcelTemplateIncomeStatement": "ExcelTemplateIncomeStatement"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "ExcelTemplateIncomeStatement_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "ExcelTemplateIncomeStatement_List"
				},
				"Read_Result": {
					"ExcelTemplateIncomeStatement": "ExcelTemplateIncomeStatement"
				}
			}
		},
		"ExcelTemplateRetainedEarnings": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"ExcelTemplateRetainedEarnings": {
					"Key": "string",
					"dateFilter": "date",
					"display": "string",
					"indentation": "int",
					"lineNumber": "int",
					"lineType": "string",
					"netChange": "decimal"
				},
				"ExcelTemplateRetainedEarnings_Fields": "string",
				"ExcelTemplateRetainedEarnings_Filter": {
					"Criteria": "string",
					"Field": "ExcelTemplateRetainedEarnings_Fields"
				},
				"ExcelTemplateRetainedEarnings_List": {
					"ExcelTemplateRetainedEarnings": "ExcelTemplateRetainedEarnings"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"lineNumber": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"ExcelTemplateRetainedEarnings": "ExcelTemplateRetainedEarnings"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "ExcelTemplateRetainedEarnings_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "ExcelTemplateRetainedEarnings_List"
				},
				"Read_Result": {
					"ExcelTemplateRetainedEarnings": "ExcelTemplateRetainedEarnings"
				}
			}
		},
		"ExcelTemplateTrialBalance": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"ExcelTemplateTrialBalance": {
					"Key": "string",
					"accountId": "string",
					"accountType": "accountType",
					"balanceAtDateCredit": "string",
					"balanceAtDateDebit": "string",
					"dateFilter": "date",
					"display": "string",
					"number": "string",
					"totalCredit": "string",
					"totalDebit": "string"
				},
				"ExcelTemplateTrialBalance_Fields": "string",
				"ExcelTemplateTrialBalance_Filter": {
					"Criteria": "string",
					"Field": "ExcelTemplateTrialBalance_Fields"
				},
				"ExcelTemplateTrialBalance_List": {
					"ExcelTemplateTrialBalance": "ExcelTemplateTrialBalance"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"ExcelTemplateTrialBalance": "ExcelTemplateTrialBalance"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "ExcelTemplateTrialBalance_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "ExcelTemplateTrialBalance_List"
				},
				"Read_Result": {
					"ExcelTemplateTrialBalance": "ExcelTemplateTrialBalance"
				},
				"accountType": "string"
			}
		},
		"ExcelTemplateViewCompanyInformation": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"ExcelTemplateViewCompanyInformation": {
					"Currency": "string",
					"DisplayName": "string",
					"Key": "string"
				},
				"ExcelTemplateViewCompanyInformation_Fields": "string",
				"ExcelTemplateViewCompanyInformation_Filter": {
					"Criteria": "string",
					"Field": "ExcelTemplateViewCompanyInformation_Fields"
				},
				"ExcelTemplateViewCompanyInformation_List": {
					"ExcelTemplateViewCompanyInformation": "ExcelTemplateViewCompanyInformation"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"ExcelTemplateViewCompanyInformation": "ExcelTemplateViewCompanyInformation"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "ExcelTemplateViewCompanyInformation_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "ExcelTemplateViewCompanyInformation_List"
				}
			}
		},
		"ExchangeServiceSetup": {
			"f": {
				"Store": {
					"i": "Store",
					"o": "Store_Result"
				}
			},
			"sp": "Codeunit",
			"t": {
				"Store": {
					"authenticationEndpoint": "string",
					"certificateThumbprint": "string",
					"clientID": "string",
					"exchangeEndpoint": "string",
					"exchangeResourceUri": "string"
				},
				"Store_Result": {
					"": ""
				}
			}
		},
		"InvoiceDocument": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"InvoiceDocument": {
					"InvoiceId": "string",
					"Key": "string"
				},
				"InvoiceDocument_Fields": "string",
				"InvoiceDocument_Filter": {
					"Criteria": "string",
					"Field": "InvoiceDocument_Fields"
				},
				"InvoiceDocument_List": {
					"InvoiceDocument": "InvoiceDocument"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"InvoiceId": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"InvoiceDocument": "InvoiceDocument"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "InvoiceDocument_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "InvoiceDocument_List"
				},
				"Read_Result": {
					"InvoiceDocument": "InvoiceDocument"
				}
			}
		},
		"InvoiceReminder": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"InvoiceReminder": "InvoiceReminder"
				},
				"CreateMultiple": {
					"InvoiceReminder_List": "InvoiceReminder_List"
				},
				"CreateMultiple_Result": {
					"InvoiceReminder_List": "InvoiceReminder_List"
				},
				"Create_Result": {
					"InvoiceReminder": "InvoiceReminder"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"InvoiceReminder": {
					"InvoiceId": "string",
					"Key": "string",
					"Message": "string"
				},
				"InvoiceReminder_Fields": "string",
				"InvoiceReminder_Filter": {
					"Criteria": "string",
					"Field": "InvoiceReminder_Fields"
				},
				"InvoiceReminder_List": {
					"InvoiceReminder": "InvoiceReminder"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"InvoiceId": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"InvoiceReminder": "InvoiceReminder"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "InvoiceReminder_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "InvoiceReminder_List"
				},
				"Read_Result": {
					"InvoiceReminder": "InvoiceReminder"
				}
			}
		},
		"Job_List": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Job_List": {
					"Bill_to_Customer_No": "string",
					"Description": "string",
					"Job_Posting_Group": "string",
					"Key": "string",
					"Next_Invoice_Date": "date",
					"No": "string",
					"Percent_Completed": "decimal",
					"Percent_Invoiced": "decimal",
					"Percent_of_Overdue_Planning_Lines": "decimal",
					"Person_Responsible": "string",
					"Project_Manager": "string",
					"Search_Description": "string",
					"Status": "Status"
				},
				"Job_List_Fields": "string",
				"Job_List_Filter": {
					"Criteria": "string",
					"Field": "Job_List_Fields"
				},
				"Job_List_List": {
					"Job_List": "Job_List"
				},
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"Job_List": "Job_List"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "Job_List_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "Job_List_List"
				},
				"Read_Result": {
					"Job_List": "Job_List"
				},
				"Status": "string"
			}
		},
		"Job_Planning_Lines": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Cost_Calculation_Method": "string",
				"Create": {
					"Job_Planning_Lines": "Job_Planning_Lines"
				},
				"CreateMultiple": {
					"Job_Planning_Lines_List": "Job_Planning_Lines_List"
				},
				"CreateMultiple_Result": {
					"Job_Planning_Lines_List": "Job_Planning_Lines_List"
				},
				"Create_Result": {
					"Job_Planning_Lines": "Job_Planning_Lines"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Job_Planning_Lines": {
					"Cost_Calculation_Method": "Cost_Calculation_Method",
					"Currency_Date": "date",
					"Description": "string",
					"Direct_Unit_Cost_LCY": "decimal",
					"Document_No": "string",
					"Gen_Bus_Posting_Group": "string",
					"Gen_Prod_Posting_Group": "string",
					"Invoiced_Amount_LCY": "decimal",
					"Invoiced_Cost_Amount_LCY": "decimal",
					"Job_Contract_Entry_No": "int",
					"Job_No": "string",
					"Job_Task_No": "string",
					"Key": "string",
					"Ledger_Entry_No": "int",
					"Ledger_Entry_Type": "Ledger_Entry_Type",
					"Line_Amount": "decimal",
					"Line_Amount_LCY": "decimal",
					"Line_Discount_Amount": "decimal",
					"Line_Discount_Percent": "decimal",
					"Line_No": "int",
					"Line_Type": "Line_Type",
					"Location_Code": "string",
					"Lot_No": "string",
					"No": "string",
					"Overdue": "boolean",
					"Planned_Delivery_Date": "date",
					"Planning_Date": "date",
					"Posted_Line_Amount": "decimal",
					"Posted_Line_Amount_LCY": "decimal",
					"Posted_Total_Cost": "decimal",
					"Posted_Total_Cost_LCY": "decimal",
					"Price_Calculation_Method": "Price_Calculation_Method",
					"Qty_Invoiced": "decimal",
					"Qty_Posted": "decimal",
					"Qty_Transferred_to_Invoice": "decimal",
					"Qty_to_Invoice": "decimal",
					"Qty_to_Transfer_to_Invoice": "decimal",
					"Qty_to_Transfer_to_Journal": "decimal",
					"Quantity": "decimal",
					"Quantity_Base": "decimal",
					"Remaining_Line_Amount": "decimal",
					"Remaining_Line_Amount_LCY": "decimal",
					"Remaining_Qty": "decimal",
					"Remaining_Total_Cost": "decimal",
					"Remaining_Total_Cost_LCY": "decimal",
					"ReserveName": "ReserveName",
					"Reserved_Quantity": "decimal",
					"Serial_No": "string",
					"System_Created_Entry": "boolean",
					"Total_Cost": "decimal",
					"Total_Cost_LCY": "decimal",
					"Total_Price": "decimal",
					"Total_Price_LCY": "decimal",
					"Type": "Type",
					"Unit_Cost": "decimal",
					"Unit_Cost_LCY": "decimal",
					"Unit_Price": "decimal",
					"Unit_Price_LCY": "decimal",
					"Unit_of_Measure_Code": "string",
					"Usage_Link": "boolean",
					"User_ID": "string",
					"Variant_Code": "string",
					"Work_Type_Code": "string"
				},
				"Job_Planning_Lines_Fields": "string",
				"Job_Planning_Lines_Filter": {
					"Criteria": "string",
					"Field": "Job_Planning_Lines_Fields"
				},
				"Job_Planning_Lines_List": {
					"Job_Planning_Lines": "Job_Planning_Lines"
				},
				"Ledger_Entry_Type": "string",
				"Line_Type": "string",
				"Price_Calculation_Method": "string",
				"Read": {
					"Job_No": "string",
					"Job_Task_No": "string",
					"Line_No": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"Job_Planning_Lines": "Job_Planning_Lines"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "Job_Planning_Lines_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "Job_Planning_Lines_List"
				},
				"Read_Result": {
					"Job_Planning_Lines": "Job_Planning_Lines"
				},
				"ReserveName": "string",
				"Type": "string",
				"Update": {
					"Job_Planning_Lines": "Job_Planning_Lines"
				},
				"UpdateMultiple": {
					"Job_Planning_Lines_List": "Job_Planning_Lines_List"
				},
				"UpdateMultiple_Result": {
					"Job_Planning_Lines_List": "Job_Planning_Lines_List"
				},
				"Update_Result": {
					"Job_Planning_Lines": "Job_Planning_Lines"
				}
			}
		},
		"Job_Task_Lines": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"Job_Task_Lines": "Job_Task_Lines"
				},
				"CreateMultiple": {
					"Job_Task_Lines_List": "Job_Task_Lines_List"
				},
				"CreateMultiple_Result": {
					"Job_Task_Lines_List": "Job_Task_Lines_List"
				},
				"Create_Result": {
					"Job_Task_Lines": "Job_Task_Lines"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Job_Task_Lines": {
					"Amt_Rcd_Not_Invoiced": "decimal",
					"Contract_Invoiced_Cost": "decimal",
					"Contract_Invoiced_Price": "decimal",
					"Contract_Total_Cost": "decimal",
					"Contract_Total_Price": "decimal",
					"Description": "string",
					"EAC_Total_Cost": "decimal",
					"EAC_Total_Price": "decimal",
					"End_Date": "date",
					"Global_Dimension_1_Code": "string",
					"Global_Dimension_2_Code": "string",
					"Job_No": "string",
					"Job_Posting_Group": "string",
					"Job_Task_No": "string",
					"Job_Task_Type": "Job_Task_Type",
					"Key": "string",
					"Outstanding_Orders": "decimal",
					"Remaining_Total_Cost": "decimal",
					"Remaining_Total_Price": "decimal",
					"Schedule_Total_Cost": "decimal",
					"Schedule_Total_Price": "decimal",
					"Start_Date": "date",
					"Totaling": "string",
					"Usage_Total_Cost": "decimal",
					"Usage_Total_Price": "decimal",
					"WIP_Method": "string",
					"WIP_Total": "WIP_Total"
				},
				"Job_Task_Lines_Fields": "string",
				"Job_Task_Lines_Filter": {
					"Criteria": "string",
					"Field": "Job_Task_Lines_Fields"
				},
				"Job_Task_Lines_List": {
					"Job_Task_Lines": "Job_Task_Lines"
				},
				"Job_Task_Type": "string",
				"Read": {
					"Job_No": "string",
					"Job_Task_No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"Job_Task_Lines": "Job_Task_Lines"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "Job_Task_Lines_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "Job_Task_Lines_List"
				},
				"Read_Result": {
					"Job_Task_Lines": "Job_Task_Lines"
				},
				"Update": {
					"Job_Task_Lines": "Job_Task_Lines"
				},
				"UpdateMultiple": {
					"Job_Task_Lines_List": "Job_Task_Lines_List"
				},
				"UpdateMultiple_Result": {
					"Job_Task_Lines_List": "Job_Task_Lines_List"
				},
				"Update_Result": {
					"Job_Task_Lines": "Job_Task_Lines"
				},
				"WIP_Total": "string"
			}
		},
		"OK365_Apply_Customer": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Document_Type": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Apply_Customer": {
					"Amount": "decimal",
					"AmountToApply": "decimal",
					"Amount_to_Apply": "decimal",
					"AppliedAmount": "decimal",
					"AppliesToID": "string",
					"ApplnAmountToApply": "decimal",
					"ApplnCurrencyCode": "string",
					"ApplnRounding": "decimal",
					"ApplyingAmount": "decimal",
					"ApplyingCustLedgEntry_Amount": "decimal",
					"ApplyingCustLedgEntry_Currency_Code": "string",
					"ApplyingCustLedgEntry_Document_No": "string",
					"ApplyingCustLedgEntry_Posting_Date": "date",
					"ApplyingCustLedgEntry_Remaining_Amount": "decimal",
					"ApplyingCustomerName": "string",
					"ApplyingCustomerNo": "string",
					"ApplyingDescription": "string",
					"CalcApplnRemainingAmount_Remaining_Amount": "decimal",
					"CalcApplnRemainingAmount_Remaining_Pmt_Disc_Possible": "decimal",
					"ControlBalance": "decimal",
					"Credit_Amount": "decimal",
					"Currency_Code": "string",
					"Customer_Name": "string",
					"Customer_No": "string",
					"Debit_Amount": "decimal",
					"Description": "string",
					"Document_No": "string",
					"Document_Type": "Document_Type",
					"Due_Date": "date",
					"Global_Dimension_1_Code": "string",
					"Global_Dimension_2_Code": "string",
					"Key": "string",
					"Max_Payment_Tolerance": "decimal",
					"Open": "boolean",
					"Original_Amount": "decimal",
					"Original_Pmt_Disc_Possible": "decimal",
					"PmtDiscountAmount": "decimal",
					"Pmt_Disc_Tolerance_Date": "date",
					"Pmt_Discount_Date": "date",
					"Positive": "boolean",
					"Posting_Date": "date",
					"Remaining_Amount": "decimal",
					"Remaining_Pmt_Disc_Possible": "decimal",
					"Select_for_Payment_OkFldSls": "boolean"
				},
				"OK365_Apply_Customer_Fields": "string",
				"OK365_Apply_Customer_Filter": {
					"Criteria": "string",
					"Field": "OK365_Apply_Customer_Fields"
				},
				"OK365_Apply_Customer_List": {
					"OK365_Apply_Customer": "OK365_Apply_Customer"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Apply_Customer": "OK365_Apply_Customer"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Apply_Customer_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Apply_Customer_List"
				},
				"Update": {
					"OK365_Apply_Customer": "OK365_Apply_Customer"
				},
				"UpdateMultiple": {
					"OK365_Apply_Customer_List": "OK365_Apply_Customer_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Apply_Customer_List": "OK365_Apply_Customer_List"
				},
				"Update_Result": {
					"OK365_Apply_Customer": "OK365_Apply_Customer"
				}
			}
		},
		"OK365_CashReceipt": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Account_Type": "string",
				"Applies_to_Doc_Type": "string",
				"Create": {
					"OK365_CashReceipt": "OK365_CashReceipt"
				},
				"CreateMultiple": {
					"OK365_CashReceipt_List": "OK365_CashReceipt_List"
				},
				"CreateMultiple_Result": {
					"OK365_CashReceipt_List": "OK365_CashReceipt_List"
				},
				"Create_Result": {
					"OK365_CashReceipt": "OK365_CashReceipt"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Document_Type": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_CashReceipt": {
					"Account_No": "string",
					"Account_Type": "Account_Type",
					"Amount": "decimal",
					"Applies_to_Doc_No": "string",
					"Applies_to_Doc_Type": "Applies_to_Doc_Type",
					"Date_Received_OkFldSls": "date",
					"Description": "string",
					"Document_No": "string",
					"Document_Type": "Document_Type",
					"External_Document_No": "string",
					"GeneralBatchName": "string",
					"GeneralJournalTemplate": "string",
					"Key": "string",
					"Line_No": "int",
					"Mobile_User_ID": "string",
					"Payment_Method_Code": "string",
					"Posting_Date": "date",
					"Route_Code": "string",
					"Salespers_Purch_Code": "string"
				},
				"OK365_CashReceipt_Fields": "string",
				"OK365_CashReceipt_Filter": {
					"Criteria": "string",
					"Field": "OK365_CashReceipt_Fields"
				},
				"OK365_CashReceipt_List": {
					"OK365_CashReceipt": "OK365_CashReceipt"
				},
				"Read": {
					"Line_No": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_CashReceipt": "OK365_CashReceipt"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_CashReceipt_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_CashReceipt_List"
				},
				"Read_Result": {
					"OK365_CashReceipt": "OK365_CashReceipt"
				},
				"Update": {
					"OK365_CashReceipt": "OK365_CashReceipt"
				},
				"UpdateMultiple": {
					"OK365_CashReceipt_List": "OK365_CashReceipt_List"
				},
				"UpdateMultiple_Result": {
					"OK365_CashReceipt_List": "OK365_CashReceipt_List"
				},
				"Update_Result": {
					"OK365_CashReceipt": "OK365_CashReceipt"
				}
			}
		},
		"OK365_Cash_Receipt_1": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Account_Type": "string",
				"Applies_to_Doc_Type": "string",
				"Bal_Account_Type": "string",
				"Bal_Gen_Posting_Type": "string",
				"Create": {
					"CurrentJnlBatchName": "string",
					"OK365_Cash_Receipt_1": "OK365_Cash_Receipt_1"
				},
				"CreateMultiple": {
					"CurrentJnlBatchName": "string",
					"OK365_Cash_Receipt_1_List": "OK365_Cash_Receipt_1_List"
				},
				"CreateMultiple_Result": {
					"OK365_Cash_Receipt_1_List": "OK365_Cash_Receipt_1_List"
				},
				"Create_Result": {
					"OK365_Cash_Receipt_1": "OK365_Cash_Receipt_1"
				},
				"Delete": {
					"CurrentJnlBatchName": "string",
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Document_Type": "string",
				"Gen_Posting_Type": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Job_Queue_Status": "string",
				"OK365_Cash_Receipt_1": {
					"AccName": "string",
					"Account_No": "string",
					"Account_Type": "Account_Type",
					"Amount": "decimal",
					"Amount_LCY": "decimal",
					"Applied_Yes_No": "boolean",
					"Applies_to_Doc_No": "string",
					"Applies_to_Doc_Type": "Applies_to_Doc_Type",
					"Applies_to_ID": "string",
					"BalAccName": "string",
					"Bal_Account_No": "string",
					"Bal_Account_Type": "Bal_Account_Type",
					"Bal_Gen_Bus_Posting_Group": "string",
					"Bal_Gen_Posting_Type": "Bal_Gen_Posting_Type",
					"Bal_Gen_Prod_Posting_Group": "string",
					"Bal_VAT_Amount": "decimal",
					"Bal_VAT_Bus_Posting_Group": "string",
					"Bal_VAT_Difference": "decimal",
					"Bal_VAT_Prod_Posting_Group": "string",
					"Balance": "decimal",
					"Campaign_No": "string",
					"Comments": "string",
					"Correction": "boolean",
					"Credit_Amount": "decimal",
					"Currency_Code": "string",
					"Date_Received_OkFldSls": "date",
					"Debit_Amount": "decimal",
					"Description": "string",
					"Direct_Debit_Mandate_ID": "string",
					"Document_Date": "date",
					"Document_No": "string",
					"Document_Type": "Document_Type",
					"External_Document_No": "string",
					"Gen_Bus_Posting_Group": "string",
					"Gen_Posting_Type": "Gen_Posting_Type",
					"Gen_Prod_Posting_Group": "string",
					"Incoming_Document_Entry_No": "int",
					"Job_Queue_Status": "Job_Queue_Status",
					"Key": "string",
					"Mobile_User_ID_OkFldSls": "string",
					"NumberOfJournalRecords": "int",
					"Payment_Method_Code_OkFldSls": "string",
					"Posting_Date": "date",
					"Reason_Code": "string",
					"Route_Code_OkFldSls": "string",
					"Salespers_Purch_Code": "string",
					"ShortcutDimCode3": "string",
					"ShortcutDimCode4": "string",
					"ShortcutDimCode5": "string",
					"ShortcutDimCode6": "string",
					"ShortcutDimCode7": "string",
					"ShortcutDimCode8": "string",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"TotalBalance": "decimal",
					"VAT_Amount": "decimal",
					"VAT_Bus_Posting_Group": "string",
					"VAT_Difference": "decimal",
					"VAT_Prod_Posting_Group": "string"
				},
				"OK365_Cash_Receipt_1_Fields": "string",
				"OK365_Cash_Receipt_1_Filter": {
					"Criteria": "string",
					"Field": "OK365_Cash_Receipt_1_Fields"
				},
				"OK365_Cash_Receipt_1_List": {
					"OK365_Cash_Receipt_1": "OK365_Cash_Receipt_1"
				},
				"ReadByRecId": {
					"CurrentJnlBatchName": "string",
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Cash_Receipt_1": "OK365_Cash_Receipt_1"
				},
				"ReadMultiple": {
					"CurrentJnlBatchName": "string",
					"bookmarkKey": "string",
					"filter": "OK365_Cash_Receipt_1_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Cash_Receipt_1_List"
				},
				"Update": {
					"CurrentJnlBatchName": "string",
					"OK365_Cash_Receipt_1": "OK365_Cash_Receipt_1"
				},
				"UpdateMultiple": {
					"CurrentJnlBatchName": "string",
					"OK365_Cash_Receipt_1_List": "OK365_Cash_Receipt_1_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Cash_Receipt_1_List": "OK365_Cash_Receipt_1_List"
				},
				"Update_Result": {
					"OK365_Cash_Receipt_1": "OK365_Cash_Receipt_1"
				}
			}
		},
		"OK365_Company_Information": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Check_Avail_Time_Bucket": "string",
				"Company_Badge": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IC_Inbox_Type": "string",
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Company_Information": {
					"Address": "string",
					"Address_2": "string",
					"Allow_Blank_Payment_Info": "boolean",
					"Auto_Send_Transactions": "boolean",
					"BankAccountPostingGroup": "string",
					"Bank_Account_No": "string",
					"Bank_Branch_No": "string",
					"Bank_Name": "string",
					"Base_Calendar_Code": "string",
					"Cal_Convergence_Time_Frame": "string",
					"Check_Avail_Period_Calc": "string",
					"Check_Avail_Time_Bucket": "Check_Avail_Time_Bucket",
					"City": "string",
					"Company_Badge": "Company_Badge",
					"Contact_Person": "string",
					"Country_Region_Code": "string",
					"County": "string",
					"Customized_Calendar": "string",
					"E_Mail": "string",
					"Experience": "string",
					"Fax_No": "string",
					"GLN": "string",
					"Giro_No": "string",
					"Home_Page": "string",
					"IBAN": "string",
					"IC_Inbox_Details": "string",
					"IC_Inbox_Type": "IC_Inbox_Type",
					"IC_Partner_Code": "string",
					"Industrial_Classification": "string",
					"Key": "string",
					"Location_Code": "string",
					"Name": "string",
					"Payment_Routing_No": "string",
					"Phone_No": "string",
					"Post_Code": "string",
					"Responsibility_Center": "string",
					"SWIFT_Code": "string",
					"Ship_to_Address": "string",
					"Ship_to_Address_2": "string",
					"Ship_to_City": "string",
					"Ship_to_Contact": "string",
					"Ship_to_Country_Region_Code": "string",
					"Ship_to_County": "string",
					"Ship_to_Name": "string",
					"Ship_to_Post_Code": "string",
					"System_Indicator_Style": "System_Indicator_Style",
					"System_Indicator_Text": "string",
					"Use_GLN_in_Electronic_Document": "boolean",
					"VAT_Registration_No": "string"
				},
				"OK365_Company_Information_Fields": "string",
				"OK365_Company_Information_Filter": {
					"Criteria": "string",
					"Field": "OK365_Company_Information_Fields"
				},
				"OK365_Company_Information_List": {
					"OK365_Company_Information": "OK365_Company_Information"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Company_Information": "OK365_Company_Information"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Company_Information_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Company_Information_List"
				},
				"System_Indicator_Style": "string",
				"Update": {
					"OK365_Company_Information": "OK365_Company_Information"
				},
				"UpdateMultiple": {
					"OK365_Company_Information_List": "OK365_Company_Information_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Company_Information_List": "OK365_Company_Information_List"
				},
				"Update_Result": {
					"OK365_Company_Information": "OK365_Company_Information"
				}
			}
		},
		"OK365_Contact_Card": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Answer_Priority": "string",
				"Contact_Card_Line": {
					"Answer": "string",
					"Answer_Priority": "Answer_Priority",
					"Key": "string",
					"Last_Date_Updated": "date",
					"Profile_Questionnaire_Priority": "Profile_Questionnaire_Priority",
					"Question": "string",
					"Questions_Answered_Percent": "decimal"
				},
				"Contact_Card_Line_List": {
					"Contact_Card_Line": "Contact_Card_Line"
				},
				"Correspondence_Type": "string",
				"Create": {
					"OK365_Contact_Card": "OK365_Contact_Card"
				},
				"CreateMultiple": {
					"OK365_Contact_Card_List": "OK365_Contact_Card_List"
				},
				"CreateMultiple_Result": {
					"OK365_Contact_Card_List": "OK365_Contact_Card_List"
				},
				"Create_Result": {
					"OK365_Contact_Card": "OK365_Contact_Card"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Contact_Card": {
					"Address": "string",
					"Address_2": "string",
					"City": "string",
					"Company_Name": "string",
					"Company_No": "string",
					"Correspondence_Type": "Correspondence_Type",
					"Country_Region_Code": "string",
					"Currency_Code": "string",
					"Date_of_Last_Interaction": "date",
					"E_Mail": "string",
					"Exclude_from_Segment": "boolean",
					"Fax_No": "string",
					"Home_Page": "string",
					"IntegrationCustomerNo": "string",
					"Key": "string",
					"Language_Code": "string",
					"LastDateTimeModified": "dateTime",
					"Last_Date_Attempted": "date",
					"Minor": "boolean",
					"Mobile_Phone_No": "string",
					"Name": "string",
					"Next_Task_Date": "date",
					"No": "string",
					"Organizational_Level_Code": "string",
					"Parental_Consent_Received": "boolean",
					"Phone_No": "string",
					"Post_Code": "string",
					"Privacy_Blocked": "boolean",
					"Profile_Questionnaire": "Contact_Card_Line_List",
					"Salesperson_Code": "string",
					"Salutation_Code": "string",
					"Search_Name": "string",
					"ShowMap": "string",
					"Territory_Code": "string",
					"Type": "Type",
					"VAT_Registration_No": "string"
				},
				"OK365_Contact_Card_Fields": "string",
				"OK365_Contact_Card_Filter": {
					"Criteria": "string",
					"Field": "OK365_Contact_Card_Fields"
				},
				"OK365_Contact_Card_List": {
					"OK365_Contact_Card": "OK365_Contact_Card"
				},
				"Profile_Questionnaire_Priority": "string",
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Contact_Card": "OK365_Contact_Card"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Contact_Card_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Contact_Card_List"
				},
				"Read_Result": {
					"OK365_Contact_Card": "OK365_Contact_Card"
				},
				"Type": "string",
				"Update": {
					"OK365_Contact_Card": "OK365_Contact_Card"
				},
				"UpdateMultiple": {
					"OK365_Contact_Card_List": "OK365_Contact_Card_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Contact_Card_List": "OK365_Contact_Card_List"
				},
				"Update_Result": {
					"OK365_Contact_Card": "OK365_Contact_Card"
				}
			}
		},
		"OK365_Contacts": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Contacts": {
					"Company_Name": "string",
					"Country_Region_Code": "string",
					"Currency_Code": "string",
					"E_Mail": "string",
					"Fax_No": "string",
					"Key": "string",
					"Language_Code": "string",
					"Minor": "boolean",
					"Mobile_Phone_No": "string",
					"Name": "string",
					"No": "string",
					"Parental_Consent_Received": "boolean",
					"Phone_No": "string",
					"Post_Code": "string",
					"Privacy_Blocked": "boolean",
					"Salesperson_Code": "string",
					"Search_Name": "string",
					"Territory_Code": "string"
				},
				"OK365_Contacts_Fields": "string",
				"OK365_Contacts_Filter": {
					"Criteria": "string",
					"Field": "OK365_Contacts_Fields"
				},
				"OK365_Contacts_List": {
					"OK365_Contacts": "OK365_Contacts"
				},
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Contacts": "OK365_Contacts"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Contacts_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Contacts_List"
				},
				"Read_Result": {
					"OK365_Contacts": "OK365_Contacts"
				}
			}
		},
		"OK365_Currencies": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"OK365_Currencies": "OK365_Currencies"
				},
				"CreateMultiple": {
					"OK365_Currencies_List": "OK365_Currencies_List"
				},
				"CreateMultiple_Result": {
					"OK365_Currencies_List": "OK365_Currencies_List"
				},
				"Create_Result": {
					"OK365_Currencies": "OK365_Currencies"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"Invoice_Rounding_Type": "string",
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Currencies": {
					"Amount_Decimal_Places": "string",
					"Amount_Rounding_Precision": "decimal",
					"Appln_Rounding_Precision": "decimal",
					"Code": "string",
					"Conv_LCY_Rndg_Credit_Acc": "string",
					"Conv_LCY_Rndg_Debit_Acc": "string",
					"CurrencyFactor": "decimal",
					"Description": "string",
					"EMU_Currency": "boolean",
					"ExchangeRateAmt": "decimal",
					"ExchangeRateDate": "date",
					"ISO_Code": "string",
					"ISO_Numeric_Code": "string",
					"Invoice_Rounding_Precision": "decimal",
					"Invoice_Rounding_Type": "Invoice_Rounding_Type",
					"Key": "string",
					"Last_Date_Adjusted": "date",
					"Last_Date_Modified": "date",
					"Max_Payment_Tolerance_Amount": "decimal",
					"Max_VAT_Difference_Allowed": "decimal",
					"Payment_Tolerance_Percent": "decimal",
					"Realized_G_L_Gains_Account": "string",
					"Realized_G_L_Losses_Account": "string",
					"Realized_Gains_Acc": "string",
					"Realized_Losses_Acc": "string",
					"Residual_Gains_Account": "string",
					"Residual_Losses_Account": "string",
					"Unit_Amount_Decimal_Places": "string",
					"Unit_Amount_Rounding_Precision": "decimal",
					"Unrealized_Gains_Acc": "string",
					"Unrealized_Losses_Acc": "string",
					"VAT_Rounding_Type": "VAT_Rounding_Type"
				},
				"OK365_Currencies_Fields": "string",
				"OK365_Currencies_Filter": {
					"Criteria": "string",
					"Field": "OK365_Currencies_Fields"
				},
				"OK365_Currencies_List": {
					"OK365_Currencies": "OK365_Currencies"
				},
				"Read": {
					"Code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Currencies": "OK365_Currencies"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Currencies_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Currencies_List"
				},
				"Read_Result": {
					"OK365_Currencies": "OK365_Currencies"
				},
				"Update": {
					"OK365_Currencies": "OK365_Currencies"
				},
				"UpdateMultiple": {
					"OK365_Currencies_List": "OK365_Currencies_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Currencies_List": "OK365_Currencies_List"
				},
				"Update_Result": {
					"OK365_Currencies": "OK365_Currencies"
				},
				"VAT_Rounding_Type": "string"
			}
		},
		"OK365_Customer_Card": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Aged_Acc_Receivable_Chart": {
					"Key": "string"
				},
				"Aged_Acc_Receivable_Chart_List": {
					"Aged_Acc_Receivable_Chart": "Aged_Acc_Receivable_Chart"
				},
				"Application_Method": "string",
				"Blocked": "string",
				"Copy_Sell_to_Addr_to_Qte_From": "string",
				"Create": {
					"OK365_Customer_Card": "OK365_Customer_Card"
				},
				"CreateMultiple": {
					"OK365_Customer_Card_List": "OK365_Customer_Card_List"
				},
				"CreateMultiple_Result": {
					"OK365_Customer_Card_List": "OK365_Customer_Card_List"
				},
				"Create_Result": {
					"OK365_Customer_Card": "OK365_Customer_Card"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Line_Type": "string",
				"OK365_Customer_Card": {
					"Address": "string",
					"Address_2": "string",
					"AdjCustProfit": "decimal",
					"AdjProfitPct": "decimal",
					"AgedAccReceivableChart": "Aged_Acc_Receivable_Chart_List",
					"Allow_Line_Disc": "boolean",
					"AmountOnCrMemo": "decimal",
					"AmountOnOutstandingCrMemos": "decimal",
					"AmountOnOutstandingInvoices": "decimal",
					"AmountOnPostedInvoices": "decimal",
					"Application_Method": "Application_Method",
					"Balance_Due": "decimal",
					"Balance_Due_LCY": "decimal",
					"Balance_LCY": "decimal",
					"Base_Calendar_Code": "string",
					"Bill_to_Customer_No": "string",
					"Block_Payment_Tolerance": "boolean",
					"Blocked": "Blocked",
					"CalcCreditLimitLCYExpendedPct": "decimal",
					"Cash_Flow_Payment_Terms_Code": "string",
					"City": "string",
					"Combine_Shipments": "boolean",
					"ContactName": "string",
					"Copy_Sell_to_Addr_to_Qte_From": "Copy_Sell_to_Addr_to_Qte_From",
					"Country_Region_Code": "string",
					"County": "string",
					"Credit_Limit_LCY": "decimal",
					"Currency_Code": "string",
					"CustInvDiscAmountLCY": "decimal",
					"CustSalesLCY_CustProfit_AdjmtCostLCY": "decimal",
					"CustomerMgt_AvgDaysToPay_No": "decimal",
					"Customer_Disc_Group": "string",
					"Customer_Posting_Group": "string",
					"Customer_Price_Group": "string",
					"Customized_Calendar": "string",
					"DaysPaidPastDueDate": "decimal",
					"Disable_Search_by_Name": "boolean",
					"Document_Sending_Profile": "string",
					"E_Mail": "string",
					"ExpectedCustMoneyOwed": "decimal",
					"Fax_No": "string",
					"Fin_Charge_Terms_Code": "string",
					"GLN": "string",
					"Gen_Bus_Posting_Group": "string",
					"Home_Page": "string",
					"IC_Partner_Code": "string",
					"Invoice_Copies": "int",
					"Invoice_Disc_Code": "string",
					"Key": "string",
					"Language_Code": "string",
					"Last_Date_Modified": "date",
					"Last_Statement_No": "int",
					"Location_Code": "string",
					"Name": "string",
					"Name_2": "string",
					"No": "string",
					"Partner_Type": "Partner_Type",
					"Payment_Method_Code": "string",
					"Payment_Terms_Code": "string",
					"Payments_LCY": "decimal",
					"Phone_No": "string",
					"Post_Code": "string",
					"Preferred_Bank_Account_Code": "string",
					"Prepayment_Percent": "decimal",
					"PriceAndLineDisc": "Sales_Pr__x0026__Line_Disc_Part_List",
					"Price_Calculation_Method": "Price_Calculation_Method",
					"Prices_Including_VAT": "boolean",
					"Primary_Contact_No": "string",
					"Print_Statements": "boolean",
					"Privacy_Blocked": "boolean",
					"Reminder_Terms_Code": "string",
					"Reserve": "Reserve",
					"Responsibility_Center": "string",
					"Salesperson_Code": "string",
					"Search_Name": "string",
					"Service_Zone_Code": "string",
					"Ship_to_Code": "string",
					"Shipment_Method_Code": "string",
					"Shipping_Advice": "Shipping_Advice",
					"Shipping_Agent_Code": "string",
					"Shipping_Agent_Service_Code": "string",
					"Shipping_Time": "string",
					"ShowMap": "string",
					"Tax_Area_Code": "string",
					"Tax_Liable": "boolean",
					"TotalMoneyOwed": "decimal",
					"TotalSales2": "decimal",
					"Totals": "decimal",
					"Use_GLN_in_Electronic_Document": "boolean",
					"VAT_Bus_Posting_Group": "string",
					"VAT_Registration_No": "string"
				},
				"OK365_Customer_Card_Fields": "string",
				"OK365_Customer_Card_Filter": {
					"Criteria": "string",
					"Field": "OK365_Customer_Card_Fields"
				},
				"OK365_Customer_Card_List": {
					"OK365_Customer_Card": "OK365_Customer_Card"
				},
				"Partner_Type": "string",
				"Price_Calculation_Method": "string",
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Customer_Card": "OK365_Customer_Card"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Customer_Card_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Customer_Card_List"
				},
				"Read_Result": {
					"OK365_Customer_Card": "OK365_Customer_Card"
				},
				"Reserve": "string",
				"Sales_Pr__x0026__Line_Disc_Part": {
					"Allow_Invoice_Disc": "boolean",
					"Allow_Line_Disc": "boolean",
					"Code": "string",
					"Currency_Code": "string",
					"Ending_Date": "date",
					"Key": "string",
					"Line_Discount_Percent": "decimal",
					"Line_Type": "Line_Type",
					"Minimum_Quantity": "decimal",
					"Price_Includes_VAT": "boolean",
					"Sales_Code": "string",
					"Sales_Type": "Sales_Type",
					"Starting_Date": "date",
					"Type": "Type",
					"Unit_Price": "decimal",
					"Unit_of_Measure_Code": "string",
					"VAT_Bus_Posting_Gr_Price": "string",
					"Variant_Code": "string"
				},
				"Sales_Pr__x0026__Line_Disc_Part_List": {
					"Sales_Pr__x0026__Line_Disc_Part": "Sales_Pr__x0026__Line_Disc_Part"
				},
				"Sales_Type": "string",
				"Shipping_Advice": "string",
				"Type": "string",
				"Update": {
					"OK365_Customer_Card": "OK365_Customer_Card"
				},
				"UpdateMultiple": {
					"OK365_Customer_Card_List": "OK365_Customer_Card_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Customer_Card_List": "OK365_Customer_Card_List"
				},
				"Update_Result": {
					"OK365_Customer_Card": "OK365_Customer_Card"
				}
			}
		},
		"OK365_Customer_Ledger_Entries": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Bal_Account_Type": "string",
				"Create": {
					"OK365_Customer_Ledger_Entries": "OK365_Customer_Ledger_Entries"
				},
				"CreateMultiple": {
					"OK365_Customer_Ledger_Entries_List": "OK365_Customer_Ledger_Entries_List"
				},
				"CreateMultiple_Result": {
					"OK365_Customer_Ledger_Entries_List": "OK365_Customer_Ledger_Entries_List"
				},
				"Create_Result": {
					"OK365_Customer_Ledger_Entries": "OK365_Customer_Ledger_Entries"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Document_Type": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Customer_Ledger_Entries": {
					"Amount": "decimal",
					"Amount_LCY": "decimal",
					"Amount_to_Apply": "decimal",
					"Applies_to_ID": "string",
					"Bal_Account_No": "string",
					"Bal_Account_Type": "Bal_Account_Type",
					"Credit_Amount": "decimal",
					"Credit_Amount_LCY": "decimal",
					"Currency_Code": "string",
					"Customer_No": "string",
					"Date_Received_OkFldSls": "date",
					"Debit_Amount": "decimal",
					"Debit_Amount_LCY": "decimal",
					"Description": "string",
					"Dimension_Set_ID": "int",
					"Direct_Debit_Mandate_ID": "string",
					"Document_No": "string",
					"Document_Type": "Document_Type",
					"Due_Date": "date",
					"Entry_No": "int",
					"Exported_to_Payment_File": "boolean",
					"External_Document_No": "string",
					"Global_Dimension_1_Code": "string",
					"Global_Dimension_2_Code": "string",
					"IC_Partner_Code": "string",
					"Key": "string",
					"Max_Payment_Tolerance": "decimal",
					"Message_to_Recipient": "string",
					"On_Hold": "string",
					"Open": "boolean",
					"Original_Amount": "decimal",
					"Original_Amt_LCY": "decimal",
					"Original_Pmt_Disc_Possible": "decimal",
					"Payment_Method_Code": "string",
					"Pmt_Disc_Tolerance_Date": "date",
					"Pmt_Discount_Date": "date",
					"Posting_Date": "date",
					"Reason_Code": "string",
					"Remaining_Amount": "decimal",
					"Remaining_Amt_LCY": "decimal",
					"Remaining_Pmt_Disc_Possible": "decimal",
					"Reversed": "boolean",
					"Reversed_Entry_No": "int",
					"Reversed_by_Entry_No": "int",
					"Sales_LCY": "decimal",
					"Salesperson_Code": "string",
					"Select_for_Payment": "boolean",
					"Source_Code": "string",
					"User_ID": "string"
				},
				"OK365_Customer_Ledger_Entries_Fields": "string",
				"OK365_Customer_Ledger_Entries_Filter": {
					"Criteria": "string",
					"Field": "OK365_Customer_Ledger_Entries_Fields"
				},
				"OK365_Customer_Ledger_Entries_List": {
					"OK365_Customer_Ledger_Entries": "OK365_Customer_Ledger_Entries"
				},
				"Read": {
					"Entry_No": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Customer_Ledger_Entries": "OK365_Customer_Ledger_Entries"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Customer_Ledger_Entries_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Customer_Ledger_Entries_List"
				},
				"Read_Result": {
					"OK365_Customer_Ledger_Entries": "OK365_Customer_Ledger_Entries"
				},
				"Update": {
					"OK365_Customer_Ledger_Entries": "OK365_Customer_Ledger_Entries"
				},
				"UpdateMultiple": {
					"OK365_Customer_Ledger_Entries_List": "OK365_Customer_Ledger_Entries_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Customer_Ledger_Entries_List": "OK365_Customer_Ledger_Entries_List"
				},
				"Update_Result": {
					"OK365_Customer_Ledger_Entries": "OK365_Customer_Ledger_Entries"
				}
			}
		},
		"OK365_Customer_List": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Application_Method": "string",
				"Blocked": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Customer_List": {
					"Application_Method": "Application_Method",
					"Available_Credit_Limit_OKFldSls": "decimal",
					"Balance_Due_LCY": "decimal",
					"Balance_LCY": "decimal",
					"Base_Calendar_Code": "string",
					"Bill_to_Customer_No_OkFldSls": "string",
					"Blocked": "Blocked",
					"Combine_Shipments": "boolean",
					"Contact": "string",
					"Country_Region_Code": "string",
					"Credit_Limit_LCY": "decimal",
					"Currency_Code": "string",
					"Customer_Disc_Group": "string",
					"Customer_Posting_Group": "string",
					"Customer_Price_Group": "string",
					"Fin_Charge_Terms_Code": "string",
					"Gen_Bus_Posting_Group": "string",
					"IC_Partner_Code": "string",
					"Key": "string",
					"Language_Code": "string",
					"Last_Date_Modified": "date",
					"Location_Code": "string",
					"Name": "string",
					"Name_2": "string",
					"No": "string",
					"Payment_Terms_Code": "string",
					"Payments_LCY": "decimal",
					"Phone_No": "string",
					"Post_Code": "string",
					"Privacy_Blocked": "boolean",
					"Reminder_Terms_Code": "string",
					"Reserve": "Reserve",
					"Responsibility_Center": "string",
					"Sales_LCY": "decimal",
					"Salesperson_Code": "string",
					"Search_Name": "string",
					"Ship_to_Code": "string",
					"Shipping_Advice": "Shipping_Advice",
					"Shipping_Agent_Code": "string",
					"VAT_Bus_Posting_Group": "string"
				},
				"OK365_Customer_List_Fields": "string",
				"OK365_Customer_List_Filter": {
					"Criteria": "string",
					"Field": "OK365_Customer_List_Fields"
				},
				"OK365_Customer_List_List": {
					"OK365_Customer_List": "OK365_Customer_List"
				},
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Customer_List": "OK365_Customer_List"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Customer_List_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Customer_List_List"
				},
				"Read_Result": {
					"OK365_Customer_List": "OK365_Customer_List"
				},
				"Reserve": "string",
				"Shipping_Advice": "string"
			}
		},
		"OK365_Daily_Sales_Report": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Daily_Sales_Report": {
					"Cash_Collection_Amount": "decimal",
					"Cash_Sales_Amount": "decimal",
					"Cheque_Collection_Amount": "decimal",
					"Credit_Sales_Amount": "decimal",
					"Date_Filter": "date",
					"Key": "string",
					"Return_Sales_Amount": "decimal",
					"Route_Code": "string"
				},
				"OK365_Daily_Sales_Report_Fields": "string",
				"OK365_Daily_Sales_Report_Filter": {
					"Criteria": "string",
					"Field": "OK365_Daily_Sales_Report_Fields"
				},
				"OK365_Daily_Sales_Report_List": {
					"OK365_Daily_Sales_Report": "OK365_Daily_Sales_Report"
				},
				"Read": {
					"Route_Code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Daily_Sales_Report": "OK365_Daily_Sales_Report"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Daily_Sales_Report_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Daily_Sales_Report_List"
				},
				"Read_Result": {
					"OK365_Daily_Sales_Report": "OK365_Daily_Sales_Report"
				},
				"Update": {
					"OK365_Daily_Sales_Report": "OK365_Daily_Sales_Report"
				},
				"UpdateMultiple": {
					"OK365_Daily_Sales_Report_List": "OK365_Daily_Sales_Report_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Daily_Sales_Report_List": "OK365_Daily_Sales_Report_List"
				},
				"Update_Result": {
					"OK365_Daily_Sales_Report": "OK365_Daily_Sales_Report"
				}
			}
		},
		"OK365_GST_Setup": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"OK365_GST_Setup": "OK365_GST_Setup"
				},
				"CreateMultiple": {
					"OK365_GST_Setup_List": "OK365_GST_Setup_List"
				},
				"CreateMultiple_Result": {
					"OK365_GST_Setup_List": "OK365_GST_Setup_List"
				},
				"Create_Result": {
					"OK365_GST_Setup": "OK365_GST_Setup"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_GST_Setup": {
					"Adjust_for_Payment_Discount": "boolean",
					"Certificate_of_Supply_Required": "boolean",
					"Description": "string",
					"EU_Service": "boolean",
					"Key": "string",
					"Purch_VAT_Unreal_Account": "string",
					"Purchase_VAT_Account": "string",
					"Reverse_Chrg_VAT_Acc": "string",
					"Reverse_Chrg_VAT_Unreal_Acc": "string",
					"Sales_VAT_Account": "string",
					"Sales_VAT_Unreal_Account": "string",
					"Tax_Category": "string",
					"Unrealized_VAT_Type": "Unrealized_VAT_Type",
					"VAT_Bus_Posting_Group": "string",
					"VAT_Calculation_Type": "VAT_Calculation_Type",
					"VAT_Clause_Code": "string",
					"VAT_Identifier": "string",
					"VAT_Percent": "decimal",
					"VAT_Prod_Posting_Group": "string"
				},
				"OK365_GST_Setup_Fields": "string",
				"OK365_GST_Setup_Filter": {
					"Criteria": "string",
					"Field": "OK365_GST_Setup_Fields"
				},
				"OK365_GST_Setup_List": {
					"OK365_GST_Setup": "OK365_GST_Setup"
				},
				"Read": {
					"VAT_Bus_Posting_Group": "string",
					"VAT_Prod_Posting_Group": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_GST_Setup": "OK365_GST_Setup"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_GST_Setup_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_GST_Setup_List"
				},
				"Read_Result": {
					"OK365_GST_Setup": "OK365_GST_Setup"
				},
				"Unrealized_VAT_Type": "string",
				"Update": {
					"OK365_GST_Setup": "OK365_GST_Setup"
				},
				"UpdateMultiple": {
					"OK365_GST_Setup_List": "OK365_GST_Setup_List"
				},
				"UpdateMultiple_Result": {
					"OK365_GST_Setup_List": "OK365_GST_Setup_List"
				},
				"Update_Result": {
					"OK365_GST_Setup": "OK365_GST_Setup"
				},
				"VAT_Calculation_Type": "string"
			}
		},
		"OK365_G_L_Account_List": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Account_Category": "string",
				"Account_Type": "string",
				"Consol_Translation_Method": "string",
				"Create": {
					"OK365_G_L_Account_List": "OK365_G_L_Account_List"
				},
				"CreateMultiple": {
					"OK365_G_L_Account_List_List": "OK365_G_L_Account_List_List"
				},
				"CreateMultiple_Result": {
					"OK365_G_L_Account_List_List": "OK365_G_L_Account_List_List"
				},
				"Create_Result": {
					"OK365_G_L_Account_List": "OK365_G_L_Account_List"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Gen_Posting_Type": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"Income_Balance": "string",
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_G_L_Account_List": {
					"Account_Category": "Account_Category",
					"Account_Subcategory_Descript": "string",
					"Account_Type": "Account_Type",
					"Add_Currency_Balance_at_Date": "decimal",
					"Additional_Currency_Balance": "decimal",
					"Additional_Currency_Net_Change": "decimal",
					"Balance": "decimal",
					"Balance_at_Date": "decimal",
					"Consol_Credit_Acc": "string",
					"Consol_Debit_Acc": "string",
					"Consol_Translation_Method": "Consol_Translation_Method",
					"Cost_Type_No": "string",
					"Credit_Amount": "decimal",
					"Debit_Amount": "decimal",
					"Default_Deferral_Template_Code": "string",
					"Default_IC_Partner_G_L_Acc_No": "string",
					"Direct_Posting": "boolean",
					"Gen_Bus_Posting_Group": "string",
					"Gen_Posting_Type": "Gen_Posting_Type",
					"Gen_Prod_Posting_Group": "string",
					"Income_Balance": "Income_Balance",
					"Key": "string",
					"Name": "string",
					"Net_Change": "decimal",
					"No": "string",
					"Totaling": "string",
					"VAT_Bus_Posting_Group": "string",
					"VAT_Prod_Posting_Group": "string"
				},
				"OK365_G_L_Account_List_Fields": "string",
				"OK365_G_L_Account_List_Filter": {
					"Criteria": "string",
					"Field": "OK365_G_L_Account_List_Fields"
				},
				"OK365_G_L_Account_List_List": {
					"OK365_G_L_Account_List": "OK365_G_L_Account_List"
				},
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_G_L_Account_List": "OK365_G_L_Account_List"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_G_L_Account_List_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_G_L_Account_List_List"
				},
				"Read_Result": {
					"OK365_G_L_Account_List": "OK365_G_L_Account_List"
				},
				"Update": {
					"OK365_G_L_Account_List": "OK365_G_L_Account_List"
				},
				"UpdateMultiple": {
					"OK365_G_L_Account_List_List": "OK365_G_L_Account_List_List"
				},
				"UpdateMultiple_Result": {
					"OK365_G_L_Account_List_List": "OK365_G_L_Account_List_List"
				},
				"Update_Result": {
					"OK365_G_L_Account_List": "OK365_G_L_Account_List"
				}
			}
		},
		"OK365_GetItemSalesPrice": {
			"f": {
				"CalculateInventory": {
					"i": "CalculateInventory",
					"o": "CalculateInventory_Result"
				},
				"DeleteSalesReturnLines": {
					"i": "DeleteSalesReturnLines",
					"o": "DeleteSalesReturnLines_Result"
				},
				"GetCompanyLogo": {
					"i": "GetCompanyLogo",
					"o": "GetCompanyLogo_Result"
				},
				"GetHandQuantity": {
					"i": "GetHandQuantity",
					"o": "GetHandQuantity_Result"
				},
				"GetItemPicture": {
					"i": "GetItemPicture",
					"o": "GetItemPicture_Result"
				},
				"GetPostedSalesInvoiceSignature": {
					"i": "GetPostedSalesInvoiceSignature",
					"o": "GetPostedSalesInvoiceSignature_Result"
				},
				"SetItemPicture": {
					"i": "SetItemPicture",
					"o": "SetItemPicture_Result"
				},
				"SetPostedSalesInvoiceSignature": {
					"i": "SetPostedSalesInvoiceSignature",
					"o": "SetPostedSalesInvoiceSignature_Result"
				},
				"StandardSalesDiscount": {
					"i": "StandardSalesDiscount",
					"o": "StandardSalesDiscount_Result"
				},
				"StandardSalesPrice": {
					"i": "StandardSalesPrice",
					"o": "StandardSalesPrice_Result"
				},
				"UpdateDailySalesReportDateFilter": {
					"i": "UpdateDailySalesReportDateFilter",
					"o": "UpdateDailySalesReportDateFilter_Result"
				},
				"UpdatePhysicalJournalStatus": {
					"i": "UpdatePhysicalJournalStatus",
					"o": "UpdatePhysicalJournalStatus_Result"
				},
				"a61a61a61LineDiscounta61a61a61": {
					"i": "a61a61a61LineDiscounta61a61a61",
					"o": "a61a61a61LineDiscounta61a61a61_Result"
				},
				"fnGetItemSalesPrice": {
					"i": "fnGetItemSalesPrice",
					"o": "fnGetItemSalesPrice_Result"
				},
				"fnReleaseTransferOrder": {
					"i": "fnReleaseTransferOrder",
					"o": "fnReleaseTransferOrder_Result"
				},
				"g_fnDeleteSalesOrdLine": {
					"i": "g_fnDeleteSalesOrdLine",
					"o": "g_fnDeleteSalesOrdLine_Result"
				},
				"g_fnSalesPostOrder": {
					"i": "g_fnSalesPostOrder",
					"o": "g_fnSalesPostOrder_Result"
				}
			},
			"sp": "Codeunit",
			"t": {
				"CalculateInventory": {
					"documentNo": "string",
					"locationCode": "string",
					"postingDate": "date"
				},
				"CalculateInventory_Result": {
					"": ""
				},
				"DeleteSalesReturnLines": {
					"documentNo": "string"
				},
				"DeleteSalesReturnLines_Result": {
					"": ""
				},
				"GetCompanyLogo": {
					"": ""
				},
				"GetCompanyLogo_Result": {
					"return_value": "string"
				},
				"GetHandQuantity": {
					"cdItemCode": "string",
					"cdLoc": "string",
					"cdVarCode": "string"
				},
				"GetHandQuantity_Result": {
					"return_value": "decimal"
				},
				"GetItemPicture": {
					"itemNo": "string"
				},
				"GetItemPicture_Result": {
					"return_value": "string"
				},
				"GetPostedSalesInvoiceSignature": {
					"postedSalesInvoiceNo": "string"
				},
				"GetPostedSalesInvoiceSignature_Result": {
					"return_value": "string"
				},
				"SetItemPicture": {
					"itemNo": "string",
					"picture": "string"
				},
				"SetItemPicture_Result": {
					"": ""
				},
				"SetPostedSalesInvoiceSignature": {
					"postedSalesInvoiceNo": "string",
					"signaturePicture": "string"
				},
				"SetPostedSalesInvoiceSignature_Result": {
					"": ""
				},
				"StandardSalesDiscount": {
					"currencyCode": "string",
					"customerNo": "string",
					"itemDiscGrCode": "string",
					"itemNo": "string",
					"itemVariantCode": "string",
					"orderDate": "date",
					"uOM": "string"
				},
				"StandardSalesDiscount_Result": {
					"return_value": "decimal"
				},
				"StandardSalesPrice": {
					"currencyCode": "string",
					"customerNo": "string",
					"itemNo": "string",
					"itemVariantCode": "string",
					"orderDate": "date",
					"uOM": "string"
				},
				"StandardSalesPrice_Result": {
					"return_value": "decimal"
				},
				"UpdateDailySalesReportDateFilter": {
					"dateFilter": "date"
				},
				"UpdateDailySalesReportDateFilter_Result": {
					"": ""
				},
				"UpdatePhysicalJournalStatus": {
					"documentNo": "string"
				},
				"UpdatePhysicalJournalStatus_Result": {
					"": ""
				},
				"a61a61a61LineDiscounta61a61a61": {
					"": ""
				},
				"a61a61a61LineDiscounta61a61a61_Result": {
					"": ""
				},
				"fnGetItemSalesPrice": {
					"currencyCode": "string",
					"customerNo": "string",
					"itemNo": "string",
					"itemVariantCode": "string",
					"orderDate": "date",
					"uOM": "string"
				},
				"fnGetItemSalesPrice_Result": {
					"return_value": "decimal"
				},
				"fnReleaseTransferOrder": {
					"cdeTransferOrdNo": "string"
				},
				"fnReleaseTransferOrder_Result": {
					"": ""
				},
				"g_fnDeleteSalesOrdLine": {
					"cdeSalesOrdNo": "string"
				},
				"g_fnDeleteSalesOrdLine_Result": {
					"": ""
				},
				"g_fnSalesPostOrder": {
					"blnInvoice": "boolean",
					"blnShip": "boolean",
					"cdeSalesOrdNo": "string",
					"optDocType": "int"
				},
				"g_fnSalesPostOrder_Result": {
					"": ""
				}
			}
		},
		"OK365_Item_Card": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Assembly_Policy": "string",
				"Costing_Method": "string",
				"Create": {
					"OK365_Item_Card": "OK365_Item_Card"
				},
				"CreateMultiple": {
					"OK365_Item_Card_List": "OK365_Item_Card_List"
				},
				"CreateMultiple_Result": {
					"OK365_Item_Card_List": "OK365_Item_Card_List"
				},
				"Create_Result": {
					"OK365_Item_Card": "OK365_Item_Card"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Flushing_Method": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Manufacturing_Policy": "string",
				"OK365_Item_Card": {
					"Allow_Invoice_Disc": "boolean",
					"Application_Wksh_User_ID": "string",
					"AssemblyBOM": "boolean",
					"Assembly_Policy": "Assembly_Policy",
					"Automatic_Ext_Texts": "boolean",
					"Base_Unit_of_Measure": "string",
					"Blocked": "boolean",
					"CalcUnitPriceExclVAT": "decimal",
					"Common_Item_No": "string",
					"Cost_is_Adjusted": "boolean",
					"Cost_is_Posted_to_G_L": "boolean",
					"Costing_Method": "Costing_Method",
					"Country_Region_of_Origin_Code": "string",
					"Created_From_Nonstock_Item": "boolean",
					"Critical": "boolean",
					"Dampener_Period": "string",
					"Dampener_Quantity": "decimal",
					"Default_Deferral_Template_Code": "string",
					"Description": "string",
					"Expiration_Calculation": "string",
					"Flushing_Method": "Flushing_Method",
					"GTIN": "string",
					"Gen_Prod_Posting_Group": "string",
					"Gross_Weight": "decimal",
					"Identifier_Code": "string",
					"Include_Inventory": "boolean",
					"Indirect_Cost_Percent": "decimal",
					"Inventory": "decimal",
					"Inventory_Posting_Group": "string",
					"Item_Category_Code": "string",
					"Item_Disc_Group": "string",
					"Item_Tracking_Code": "string",
					"Key": "string",
					"Last_Counting_Period_Update": "date",
					"Last_Date_Modified": "date",
					"Last_Direct_Cost": "decimal",
					"Last_Phys_Invt_Date": "date",
					"Lead_Time_Calculation": "string",
					"Lot_Accumulation_Period": "string",
					"Lot_Nos": "string",
					"Lot_Size": "decimal",
					"Manufacturing_Policy": "Manufacturing_Policy",
					"Maximum_Inventory": "decimal",
					"Maximum_Order_Quantity": "decimal",
					"Minimum_Order_Quantity": "decimal",
					"Net_Invoiced_Qty": "decimal",
					"Net_Weight": "decimal",
					"Next_Counting_End_Date": "date",
					"Next_Counting_Start_Date": "date",
					"No": "string",
					"OK365_Item_OkFldSls": "boolean",
					"Order_Multiple": "decimal",
					"Order_Tracking_Policy": "Order_Tracking_Policy",
					"Over_Receipt_Code": "string",
					"Overflow_Level": "decimal",
					"Overhead_Rate": "decimal",
					"Phys_Invt_Counting_Period_Code": "string",
					"PreventNegInventoryDefaultYes": "PreventNegInventoryDefaultYes",
					"Price_Includes_VAT": "boolean",
					"Price_Profit_Calculation": "Price_Profit_Calculation",
					"Production_BOM_No": "string",
					"Profit_Percent": "decimal",
					"Purch_Unit_of_Measure": "string",
					"Purchasing_Blocked": "boolean",
					"Purchasing_Code": "string",
					"Put_away_Template_Code": "string",
					"Put_away_Unit_of_Measure_Code": "string",
					"Qty_on_Asm_Component": "decimal",
					"Qty_on_Assembly_Order": "decimal",
					"Qty_on_Component_Lines": "decimal",
					"Qty_on_Job_Order": "decimal",
					"Qty_on_Prod_Order": "decimal",
					"Qty_on_Purch_Order": "decimal",
					"Qty_on_Sales_Order": "decimal",
					"Qty_on_Service_Order": "decimal",
					"Reorder_Point": "decimal",
					"Reorder_Quantity": "decimal",
					"Reordering_Policy": "Reordering_Policy",
					"Replenishment_System": "Replenishment_System",
					"Rescheduling_Period": "string",
					"Reserve": "Reserve",
					"Rounding_Precision": "decimal",
					"Routing_No": "string",
					"Safety_Lead_Time": "string",
					"Safety_Stock_Quantity": "decimal",
					"Sales_Blocked": "boolean",
					"Sales_Unit_of_Measure": "string",
					"Scrap_Percent": "decimal",
					"Search_Description": "string",
					"Serial_Nos": "string",
					"Service_Item_Group": "string",
					"Shelf_No": "string",
					"SpecialPricesAndDiscountsTxt": "string",
					"SpecialPurchPricesAndDiscountsTxt": "string",
					"Special_Equipment_Code": "string",
					"Standard_Cost": "decimal",
					"Stockkeeping_Unit_Exists": "boolean",
					"StockoutWarningDefaultYes": "StockoutWarningDefaultYes",
					"Tariff_No": "string",
					"Tax_Group_Code": "string",
					"Time_Bucket": "string",
					"Type": "Type",
					"Unit_Cost": "decimal",
					"Unit_Price": "decimal",
					"Unit_Volume": "decimal",
					"Use_Cross_Docking": "boolean",
					"VAT_Bus_Posting_Gr_Price": "string",
					"VAT_Prod_Posting_Group": "string",
					"Vendor_Item_No": "string",
					"Vendor_No": "string",
					"Warehouse_Class_Code": "string"
				},
				"OK365_Item_Card_Fields": "string",
				"OK365_Item_Card_Filter": {
					"Criteria": "string",
					"Field": "OK365_Item_Card_Fields"
				},
				"OK365_Item_Card_List": {
					"OK365_Item_Card": "OK365_Item_Card"
				},
				"Order_Tracking_Policy": "string",
				"PreventNegInventoryDefaultYes": "string",
				"Price_Profit_Calculation": "string",
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Item_Card": "OK365_Item_Card"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Item_Card_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Item_Card_List"
				},
				"Read_Result": {
					"OK365_Item_Card": "OK365_Item_Card"
				},
				"Reordering_Policy": "string",
				"Replenishment_System": "string",
				"Reserve": "string",
				"StockoutWarningDefaultYes": "string",
				"Type": "string",
				"Update": {
					"OK365_Item_Card": "OK365_Item_Card"
				},
				"UpdateMultiple": {
					"OK365_Item_Card_List": "OK365_Item_Card_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Item_Card_List": "OK365_Item_Card_List"
				},
				"Update_Result": {
					"OK365_Item_Card": "OK365_Item_Card"
				}
			}
		},
		"OK365_Item_Categories": {
			"f": {
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Item_Categories": {
					"Code": "string",
					"Description": "string",
					"Key": "string"
				},
				"OK365_Item_Categories_Fields": "string",
				"OK365_Item_Categories_Filter": {
					"Criteria": "string",
					"Field": "OK365_Item_Categories_Fields"
				},
				"OK365_Item_Categories_List": {
					"OK365_Item_Categories": "OK365_Item_Categories"
				},
				"Read": {
					"Code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Item_Categories": "OK365_Item_Categories"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Item_Categories_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Item_Categories_List"
				},
				"Read_Result": {
					"OK365_Item_Categories": "OK365_Item_Categories"
				},
				"Update": {
					"OK365_Item_Categories": "OK365_Item_Categories"
				},
				"UpdateMultiple": {
					"OK365_Item_Categories_List": "OK365_Item_Categories_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Item_Categories_List": "OK365_Item_Categories_List"
				},
				"Update_Result": {
					"OK365_Item_Categories": "OK365_Item_Categories"
				}
			}
		},
		"OK365_Item_Journal": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"CurrentJnlBatchName": "string",
					"OK365_Item_Journal": "OK365_Item_Journal"
				},
				"CreateMultiple": {
					"CurrentJnlBatchName": "string",
					"OK365_Item_Journal_List": "OK365_Item_Journal_List"
				},
				"CreateMultiple_Result": {
					"OK365_Item_Journal_List": "OK365_Item_Journal_List"
				},
				"Create_Result": {
					"OK365_Item_Journal": "OK365_Item_Journal"
				},
				"Delete": {
					"CurrentJnlBatchName": "string",
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Entry_Type": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Item_Journal": {
					"Amount": "decimal",
					"Applies_from_Entry": "int",
					"Applies_to_Entry": "int",
					"Bin_Code": "string",
					"Country_Region_Code": "string",
					"Description": "string",
					"Discount_Amount": "decimal",
					"Document_Date": "date",
					"Document_No": "string",
					"Entry_Type": "Entry_Type",
					"External_Document_No": "string",
					"Gen_Bus_Posting_Group": "string",
					"Gen_Prod_Posting_Group": "string",
					"Indirect_Cost_Percent": "decimal",
					"ItemDescription": "string",
					"Item_No": "string",
					"Key": "string",
					"Location_Code": "string",
					"Posting_Date": "date",
					"Price_Calculation_Method": "Price_Calculation_Method",
					"Quantity": "decimal",
					"Reason_Code": "string",
					"Salespers_Purch_Code": "string",
					"ShortcutDimCode3": "string",
					"ShortcutDimCode4": "string",
					"ShortcutDimCode5": "string",
					"ShortcutDimCode6": "string",
					"ShortcutDimCode7": "string",
					"ShortcutDimCode8": "string",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"Transaction_Type": "string",
					"Transport_Method": "string",
					"Unit_Amount": "decimal",
					"Unit_Cost": "decimal",
					"Unit_of_Measure_Code": "string",
					"Variant_Code": "string"
				},
				"OK365_Item_Journal_Fields": "string",
				"OK365_Item_Journal_Filter": {
					"Criteria": "string",
					"Field": "OK365_Item_Journal_Fields"
				},
				"OK365_Item_Journal_List": {
					"OK365_Item_Journal": "OK365_Item_Journal"
				},
				"Price_Calculation_Method": "string",
				"ReadByRecId": {
					"CurrentJnlBatchName": "string",
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Item_Journal": "OK365_Item_Journal"
				},
				"ReadMultiple": {
					"CurrentJnlBatchName": "string",
					"bookmarkKey": "string",
					"filter": "OK365_Item_Journal_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Item_Journal_List"
				},
				"Update": {
					"CurrentJnlBatchName": "string",
					"OK365_Item_Journal": "OK365_Item_Journal"
				},
				"UpdateMultiple": {
					"CurrentJnlBatchName": "string",
					"OK365_Item_Journal_List": "OK365_Item_Journal_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Item_Journal_List": "OK365_Item_Journal_List"
				},
				"Update_Result": {
					"OK365_Item_Journal": "OK365_Item_Journal"
				}
			}
		},
		"OK365_Item_List": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Assembly_Policy": "string",
				"Costing_Method": "string",
				"Flushing_Method": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Manufacturing_Policy": "string",
				"OK365_Item_List": {
					"Assembly_BOM": "boolean",
					"Assembly_Policy": "Assembly_Policy",
					"Base_Unit_of_Measure": "string",
					"Blocked": "boolean",
					"Cost_is_Adjusted": "boolean",
					"Costing_Method": "Costing_Method",
					"Created_From_Nonstock_Item": "boolean",
					"Default_Deferral_Template_Code": "string",
					"Description": "string",
					"Flushing_Method": "Flushing_Method",
					"Gen_Prod_Posting_Group": "string",
					"GetPicture": "string",
					"Indirect_Cost_Percent": "decimal",
					"Inventory": "decimal",
					"Inventory_Posting_Group": "string",
					"Item_Category_Code": "string",
					"Item_Disc_Group": "string",
					"Item_Tracking_Code": "string",
					"Key": "string",
					"Last_Date_Modified": "date",
					"Last_Direct_Cost": "decimal",
					"Lead_Time_Calculation": "string",
					"Location_Filter": "string",
					"Manufacturing_Policy": "Manufacturing_Policy",
					"No": "string",
					"OK365_Item_OkFldSls": "boolean",
					"Overhead_Rate": "decimal",
					"Price_Profit_Calculation": "Price_Profit_Calculation",
					"Production_BOM_No": "string",
					"Profit_Percent": "decimal",
					"Purch_Unit_of_Measure": "string",
					"Replenishment_System": "Replenishment_System",
					"Routing_No": "string",
					"Sales_Unit_of_Measure": "string",
					"Search_Description": "string",
					"Shelf_No": "string",
					"Standard_Cost": "decimal",
					"Stockkeeping_Unit_Exists": "boolean",
					"Substitutes_Exist": "boolean",
					"Tariff_No": "string",
					"Type": "Type",
					"Unit_Cost": "decimal",
					"Unit_Price": "decimal",
					"VAT_Prod_Posting_Group": "string",
					"Vendor_Item_No": "string",
					"Vendor_No": "string"
				},
				"OK365_Item_List_Fields": "string",
				"OK365_Item_List_Filter": {
					"Criteria": "string",
					"Field": "OK365_Item_List_Fields"
				},
				"OK365_Item_List_List": {
					"OK365_Item_List": "OK365_Item_List"
				},
				"Price_Profit_Calculation": "string",
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Item_List": "OK365_Item_List"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Item_List_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Item_List_List"
				},
				"Read_Result": {
					"OK365_Item_List": "OK365_Item_List"
				},
				"Replenishment_System": "string",
				"Type": "string"
			}
		},
		"OK365_Item_List_STD": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Assembly_Policy": "string",
				"Costing_Method": "string",
				"Flushing_Method": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Manufacturing_Policy": "string",
				"OK365_Item_List_STD": {
					"Assembly_BOM": "boolean",
					"Assembly_Policy": "Assembly_Policy",
					"Base_Unit_of_Measure": "string",
					"Blocked": "boolean",
					"Cost_is_Adjusted": "boolean",
					"Costing_Method": "Costing_Method",
					"Created_From_Nonstock_Item": "boolean",
					"Default_Deferral_Template_Code": "string",
					"Description": "string",
					"Flushing_Method": "Flushing_Method",
					"Gen_Prod_Posting_Group": "string",
					"Indirect_Cost_Percent": "decimal",
					"InventoryField": "decimal",
					"Inventory_Posting_Group": "string",
					"Item_Category_Code": "string",
					"Item_Disc_Group": "string",
					"Item_Tracking_Code": "string",
					"Key": "string",
					"Last_Date_Modified": "date",
					"Last_Direct_Cost": "decimal",
					"Lead_Time_Calculation": "string",
					"Manufacturing_Policy": "Manufacturing_Policy",
					"No": "string",
					"OK365_Item_OkFldSls": "boolean",
					"Overhead_Rate": "decimal",
					"Price_Profit_Calculation": "Price_Profit_Calculation",
					"Production_BOM_No": "string",
					"Profit_Percent": "decimal",
					"Purch_Unit_of_Measure": "string",
					"Replenishment_System": "Replenishment_System",
					"Routing_No": "string",
					"Sales_Unit_of_Measure": "string",
					"Search_Description": "string",
					"Shelf_No": "string",
					"Standard_Cost": "decimal",
					"Stockkeeping_Unit_Exists": "boolean",
					"Substitutes_Exist": "boolean",
					"Tariff_No": "string",
					"Type": "Type",
					"Unit_Cost": "decimal",
					"Unit_Price": "decimal",
					"VAT_Prod_Posting_Group": "string",
					"Vendor_Item_No": "string",
					"Vendor_No": "string"
				},
				"OK365_Item_List_STD_Fields": "string",
				"OK365_Item_List_STD_Filter": {
					"Criteria": "string",
					"Field": "OK365_Item_List_STD_Fields"
				},
				"OK365_Item_List_STD_List": {
					"OK365_Item_List_STD": "OK365_Item_List_STD"
				},
				"Price_Profit_Calculation": "string",
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Item_List_STD": "OK365_Item_List_STD"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Item_List_STD_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Item_List_STD_List"
				},
				"Read_Result": {
					"OK365_Item_List_STD": "OK365_Item_List_STD"
				},
				"Replenishment_System": "string",
				"Type": "string"
			}
		},
		"OK365_Item_Unit_of_Measure": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"OK365_Item_Unit_of_Measure": "OK365_Item_Unit_of_Measure"
				},
				"CreateMultiple": {
					"OK365_Item_Unit_of_Measure_List": "OK365_Item_Unit_of_Measure_List"
				},
				"CreateMultiple_Result": {
					"OK365_Item_Unit_of_Measure_List": "OK365_Item_Unit_of_Measure_List"
				},
				"Create_Result": {
					"OK365_Item_Unit_of_Measure": "OK365_Item_Unit_of_Measure"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Item_Unit_of_Measure": {
					"Code": "string",
					"Cubage": "decimal",
					"Height": "decimal",
					"ItemUnitOfMeasure": "string",
					"Item_No": "string",
					"Key": "string",
					"Length": "decimal",
					"Qty_per_Unit_of_Measure": "decimal",
					"Weight": "decimal",
					"Width": "decimal"
				},
				"OK365_Item_Unit_of_Measure_Fields": "string",
				"OK365_Item_Unit_of_Measure_Filter": {
					"Criteria": "string",
					"Field": "OK365_Item_Unit_of_Measure_Fields"
				},
				"OK365_Item_Unit_of_Measure_List": {
					"OK365_Item_Unit_of_Measure": "OK365_Item_Unit_of_Measure"
				},
				"Read": {
					"Code": "string",
					"Item_No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Item_Unit_of_Measure": "OK365_Item_Unit_of_Measure"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Item_Unit_of_Measure_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Item_Unit_of_Measure_List"
				},
				"Read_Result": {
					"OK365_Item_Unit_of_Measure": "OK365_Item_Unit_of_Measure"
				},
				"Update": {
					"OK365_Item_Unit_of_Measure": "OK365_Item_Unit_of_Measure"
				},
				"UpdateMultiple": {
					"OK365_Item_Unit_of_Measure_List": "OK365_Item_Unit_of_Measure_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Item_Unit_of_Measure_List": "OK365_Item_Unit_of_Measure_List"
				},
				"Update_Result": {
					"OK365_Item_Unit_of_Measure": "OK365_Item_Unit_of_Measure"
				}
			}
		},
		"OK365_Location_List": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Location_List": {
					"Code": "string",
					"Key": "string",
					"Name": "string"
				},
				"OK365_Location_List_Fields": "string",
				"OK365_Location_List_Filter": {
					"Criteria": "string",
					"Field": "OK365_Location_List_Fields"
				},
				"OK365_Location_List_List": {
					"OK365_Location_List": "OK365_Location_List"
				},
				"Read": {
					"Code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Location_List": "OK365_Location_List"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Location_List_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Location_List_List"
				},
				"Read_Result": {
					"OK365_Location_List": "OK365_Location_List"
				}
			}
		},
		"OK365_Location_card": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Bin_Capacity_Policy": "string",
				"Create": {
					"OK365_Location_card": "OK365_Location_card"
				},
				"CreateMultiple": {
					"OK365_Location_card_List": "OK365_Location_card_List"
				},
				"CreateMultiple_Result": {
					"OK365_Location_card_List": "OK365_Location_card_List"
				},
				"Create_Result": {
					"OK365_Location_card": "OK365_Location_card"
				},
				"Default_Bin_Selection": "string",
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Location_card": {
					"Address": "string",
					"Address_2": "string",
					"Adjustment_Bin_Code": "string",
					"Allow_Breakbulk": "boolean",
					"Always_Create_Pick_Line": "boolean",
					"Always_Create_Put_away_Line": "boolean",
					"Asm_to_Order_Shpt_Bin_Code": "string",
					"Base_Calendar_Code": "string",
					"Bin_Capacity_Policy": "Bin_Capacity_Policy",
					"Bin_Mandatory": "boolean",
					"City": "string",
					"Code": "string",
					"Contact": "string",
					"Country_Region_Code": "string",
					"Cross_Dock_Bin_Code": "string",
					"Cross_Dock_Due_Date_Calc": "string",
					"Customized_Calendar": "string",
					"Default_Bin_Selection": "Default_Bin_Selection",
					"Directed_Put_away_and_Pick": "boolean",
					"E_Mail": "string",
					"Fax_No": "string",
					"From_Assembly_Bin_Code": "string",
					"From_Production_Bin_Code": "string",
					"Home_Page": "string",
					"Inbound_Whse_Handling_Time": "string",
					"Key": "string",
					"Name": "string",
					"Open_Shop_Floor_Bin_Code": "string",
					"Outbound_Whse_Handling_Time": "string",
					"Phone_No": "string",
					"Pick_According_to_FEFO": "boolean",
					"Post_Code": "string",
					"Put_away_Template_Code": "string",
					"Receipt_Bin_Code": "string",
					"Require_Pick": "boolean",
					"Require_Put_away": "boolean",
					"Require_Receive": "boolean",
					"Require_Shipment": "boolean",
					"Shipment_Bin_Code": "string",
					"ShowMap": "string",
					"Special_Equipment": "Special_Equipment",
					"To_Assembly_Bin_Code": "string",
					"To_Production_Bin_Code": "string",
					"Use_ADCS": "boolean",
					"Use_As_In_Transit": "boolean",
					"Use_Cross_Docking": "boolean",
					"Use_Put_away_Worksheet": "boolean"
				},
				"OK365_Location_card_Fields": "string",
				"OK365_Location_card_Filter": {
					"Criteria": "string",
					"Field": "OK365_Location_card_Fields"
				},
				"OK365_Location_card_List": {
					"OK365_Location_card": "OK365_Location_card"
				},
				"Read": {
					"Code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Location_card": "OK365_Location_card"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Location_card_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Location_card_List"
				},
				"Read_Result": {
					"OK365_Location_card": "OK365_Location_card"
				},
				"Special_Equipment": "string",
				"Update": {
					"OK365_Location_card": "OK365_Location_card"
				},
				"UpdateMultiple": {
					"OK365_Location_card_List": "OK365_Location_card_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Location_card_List": "OK365_Location_card_List"
				},
				"Update_Result": {
					"OK365_Location_card": "OK365_Location_card"
				}
			}
		},
		"OK365_Mobile_User_Master": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"OK365_Mobile_User_Master": "OK365_Mobile_User_Master"
				},
				"CreateMultiple": {
					"OK365_Mobile_User_Master_List": "OK365_Mobile_User_Master_List"
				},
				"CreateMultiple_Result": {
					"OK365_Mobile_User_Master_List": "OK365_Mobile_User_Master_List"
				},
				"Create_Result": {
					"OK365_Mobile_User_Master": "OK365_Mobile_User_Master"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Mobile_User_Master": {
					"Key": "string",
					"Location_Code": "string",
					"Mobile_Login_User_ID": "string",
					"Password": "string",
					"Route_Card_no": "string",
					"Salesperson_Code_OkFldSls": "string",
					"User_Type": "User_Type"
				},
				"OK365_Mobile_User_Master_Fields": "string",
				"OK365_Mobile_User_Master_Filter": {
					"Criteria": "string",
					"Field": "OK365_Mobile_User_Master_Fields"
				},
				"OK365_Mobile_User_Master_List": {
					"OK365_Mobile_User_Master": "OK365_Mobile_User_Master"
				},
				"Read": {
					"Mobile_Login_User_ID": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Mobile_User_Master": "OK365_Mobile_User_Master"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Mobile_User_Master_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Mobile_User_Master_List"
				},
				"Read_Result": {
					"OK365_Mobile_User_Master": "OK365_Mobile_User_Master"
				},
				"Update": {
					"OK365_Mobile_User_Master": "OK365_Mobile_User_Master"
				},
				"UpdateMultiple": {
					"OK365_Mobile_User_Master_List": "OK365_Mobile_User_Master_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Mobile_User_Master_List": "OK365_Mobile_User_Master_List"
				},
				"Update_Result": {
					"OK365_Mobile_User_Master": "OK365_Mobile_User_Master"
				},
				"User_Type": "string"
			}
		},
		"OK365_Order_Entry": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"Delete_SalesLines": {
					"i": "Delete_SalesLines",
					"o": "Delete_SalesLines_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Control50": "string",
				"Create": {
					"OK365_Order_Entry": "OK365_Order_Entry"
				},
				"CreateMultiple": {
					"OK365_Order_Entry_List": "OK365_Order_Entry_List"
				},
				"CreateMultiple_Result": {
					"OK365_Order_Entry_List": "OK365_Order_Entry_List"
				},
				"Create_Result": {
					"OK365_Order_Entry": "OK365_Order_Entry"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Delete_SalesLines": {
					"SalesLines_Key": "string"
				},
				"Delete_SalesLines_Result": {
					"Delete_SalesLines_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IC_Partner_Ref_Type": "string",
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Job_Queue_Status": "string",
				"OK365_Order_Entry": {
					"Amount_Including_VAT_OkFldSls": "decimal",
					"Amount_OkFldSls": "decimal",
					"Area": "string",
					"Assigned_User_ID": "string",
					"Bill_to_Address": "string",
					"Bill_to_Address_2": "string",
					"Bill_to_City": "string",
					"Bill_to_Contact": "string",
					"Bill_to_Contact_No": "string",
					"Bill_to_Country_Region_Code": "string",
					"Bill_to_County": "string",
					"Bill_to_Customer_No_OkFldSls": "string",
					"Bill_to_Name": "string",
					"Bill_to_Post_Code": "string",
					"Campaign_No": "string",
					"Combine_Shipments": "boolean",
					"Compress_Prepayment": "boolean",
					"Currency_Code": "string",
					"Direct_Debit_Mandate_ID": "string",
					"Document_Date": "date",
					"Due_Date": "date",
					"EU_3_Party_Trade": "boolean",
					"Exit_Point": "string",
					"External_Document_No": "string",
					"Job_Queue_Status": "Job_Queue_Status",
					"Key": "string",
					"Late_Order_Shipping": "boolean",
					"Location_Code": "string",
					"Manual_Amount_OkFldSls": "decimal",
					"Mobile_User_ID_OkFldSls": "string",
					"No": "string",
					"No_of_Archived_Versions": "int",
					"Opportunity_No": "string",
					"Order_Date": "date",
					"Outbound_Whse_Handling_Time": "string",
					"Package_Tracking_No": "string",
					"Payment_Discount_Percent": "decimal",
					"Payment_Method_Code": "string",
					"Payment_Terms_Code": "string",
					"Pmt_Discount_Date": "date",
					"Posting_Date": "date",
					"Posting_Description": "string",
					"Prepayment_Due_Date": "date",
					"Prepayment_Percent": "decimal",
					"Prepmt_Payment_Discount_Percent": "decimal",
					"Prepmt_Payment_Terms_Code": "string",
					"Prepmt_Pmt_Discount_Date": "date",
					"Prices_Including_VAT": "boolean",
					"Promised_Delivery_Date": "date",
					"Quote_No": "string",
					"Remarks_OkFldSls": "string",
					"Requested_Delivery_Date": "date",
					"Responsibility_Center": "string",
					"Route_Code_OkFldSls": "string",
					"SalesLines": "Sales_Order_Line_List",
					"Salesperson_Code": "string",
					"SelectedPayments": "string",
					"Sell_to_Address": "string",
					"Sell_to_Address_2": "string",
					"Sell_to_City": "string",
					"Sell_to_Contact": "string",
					"Sell_to_Contact_No": "string",
					"Sell_to_Country_Region_Code": "string",
					"Sell_to_County": "string",
					"Sell_to_Customer_Name": "string",
					"Sell_to_Customer_No": "string",
					"Sell_to_E_Mail": "string",
					"Sell_to_Phone_No": "string",
					"Sell_to_Post_Code": "string",
					"Ship_to_Address": "string",
					"Ship_to_Address_2": "string",
					"Ship_to_City": "string",
					"Ship_to_Code": "string",
					"Ship_to_Contact": "string",
					"Ship_to_Country_Region_Code": "string",
					"Ship_to_County": "string",
					"Ship_to_Name": "string",
					"Ship_to_Post_Code": "string",
					"Shipment_Date": "date",
					"Shipment_Method_Code": "string",
					"Shipping_Advice": "Shipping_Advice",
					"Shipping_Agent_Code": "string",
					"Shipping_Agent_Service_Code": "string",
					"Shipping_Time": "string",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"Status": "Status",
					"Transaction_Specification": "string",
					"Transaction_Type": "string",
					"Transport_Method": "string",
					"User_Type_OkFldSls": "User_Type_OkFldSls",
					"VAT_Bus_Posting_Group": "string",
					"WorkDescription": "string",
					"Your_Reference": "string"
				},
				"OK365_Order_Entry_Fields": "string",
				"OK365_Order_Entry_Filter": {
					"Criteria": "string",
					"Field": "OK365_Order_Entry_Fields"
				},
				"OK365_Order_Entry_List": {
					"OK365_Order_Entry": "OK365_Order_Entry"
				},
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Order_Entry": "OK365_Order_Entry"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Order_Entry_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Order_Entry_List"
				},
				"Read_Result": {
					"OK365_Order_Entry": "OK365_Order_Entry"
				},
				"Sales_Order_Line": {
					"ATO_Whse_Outstanding_Qty": "decimal",
					"ATO_Whse_Outstd_Qty_Base": "decimal",
					"Allow_Invoice_Disc": "boolean",
					"Allow_Item_Charge_Assignment": "boolean",
					"Appl_from_Item_Entry": "int",
					"Appl_to_Item_Entry": "int",
					"Bin_Code": "string",
					"Blanket_Order_Line_No": "int",
					"Blanket_Order_No": "string",
					"Control50": "Control50",
					"Cross_Reference_No": "string",
					"Deferral_Code": "string",
					"Depr_until_FA_Posting_Date": "boolean",
					"Depreciation_Book_Code": "string",
					"Description": "string",
					"Document_No": "string",
					"Drop_Shipment": "boolean",
					"Duplicate_in_Depreciation_Book": "string",
					"FA_Posting_Date": "date",
					"FOC_Reason_Code_OkFldSls": "string",
					"FilteredTypeField": "string",
					"IC_Partner_Code": "string",
					"IC_Partner_Ref_Type": "IC_Partner_Ref_Type",
					"IC_Partner_Reference": "string",
					"Inv_Disc_Amount_to_Invoice": "decimal",
					"Inv_Discount_Amount": "decimal",
					"Invoice_Disc_Pct": "decimal",
					"Invoice_Discount_Amount": "decimal",
					"Key": "string",
					"Line_Amount": "decimal",
					"Line_Discount_Amount": "decimal",
					"Line_Discount_Percent": "decimal",
					"Line_No": "int",
					"Location_Code": "string",
					"No": "string",
					"Nonstock": "boolean",
					"Outbound_Whse_Handling_Time": "string",
					"Planned_Delivery_Date": "date",
					"Planned_Shipment_Date": "date",
					"Prepayment_Percent": "decimal",
					"Prepmt_Amt_Deducted": "decimal",
					"Prepmt_Amt_Inv": "decimal",
					"Prepmt_Amt_to_Deduct": "decimal",
					"Prepmt_Line_Amount": "decimal",
					"Promised_Delivery_Date": "date",
					"Purchasing_Code": "string",
					"Qty_Assigned": "decimal",
					"Qty_to_Assemble_to_Order": "decimal",
					"Qty_to_Assign": "decimal",
					"Qty_to_Invoice": "decimal",
					"Qty_to_Ship": "decimal",
					"Quantity": "decimal",
					"Quantity_Invoiced": "decimal",
					"Quantity_Shipped": "decimal",
					"Requested_Delivery_Date": "date",
					"Reserved_Quantity": "decimal",
					"Return_Reason_Code": "string",
					"SalesLineDiscExists": "boolean",
					"SalesPriceExist": "boolean",
					"Shipment_Date": "date",
					"Shipping_Agent_Code": "string",
					"Shipping_Agent_Service_Code": "string",
					"Shipping_Time": "string",
					"ShortcutDimCode3": "string",
					"ShortcutDimCode4": "string",
					"ShortcutDimCode5": "string",
					"ShortcutDimCode6": "string",
					"ShortcutDimCode7": "string",
					"ShortcutDimCode8": "string",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"Special_Order": "boolean",
					"Substitution_Available": "boolean",
					"Tax_Area_Code": "string",
					"Tax_Group_Code": "string",
					"Tax_Liable": "boolean",
					"TotalSalesLine_Line_Amount": "decimal",
					"Total_Amount_Excl_VAT": "decimal",
					"Total_Amount_Incl_VAT": "decimal",
					"Total_VAT_Amount": "decimal",
					"Type": "Type",
					"Unit_Cost_LCY": "decimal",
					"Unit_Price": "decimal",
					"Unit_of_Measure": "string",
					"Unit_of_Measure_Code": "string",
					"Use_Duplication_List": "boolean",
					"VAT_Prod_Posting_Group": "string",
					"Variant_Code": "string",
					"Whse_Outstanding_Qty": "decimal",
					"Whse_Outstanding_Qty_Base": "decimal",
					"Work_Type_Code": "string"
				},
				"Sales_Order_Line_List": {
					"Sales_Order_Line": "Sales_Order_Line"
				},
				"Shipping_Advice": "string",
				"Status": "string",
				"Type": "string",
				"Update": {
					"OK365_Order_Entry": "OK365_Order_Entry"
				},
				"UpdateMultiple": {
					"OK365_Order_Entry_List": "OK365_Order_Entry_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Order_Entry_List": "OK365_Order_Entry_List"
				},
				"Update_Result": {
					"OK365_Order_Entry": "OK365_Order_Entry"
				},
				"User_Type_OkFldSls": "string"
			}
		},
		"OK365_Order_Entry_Line": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Control50": "string",
				"Create": {
					"OK365_Order_Entry_Line": "OK365_Order_Entry_Line"
				},
				"CreateMultiple": {
					"OK365_Order_Entry_Line_List": "OK365_Order_Entry_Line_List"
				},
				"CreateMultiple_Result": {
					"OK365_Order_Entry_Line_List": "OK365_Order_Entry_Line_List"
				},
				"Create_Result": {
					"OK365_Order_Entry_Line": "OK365_Order_Entry_Line"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IC_Partner_Ref_Type": "string",
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Order_Entry_Line": {
					"ATO_Whse_Outstanding_Qty": "decimal",
					"ATO_Whse_Outstd_Qty_Base": "decimal",
					"Allow_Invoice_Disc": "boolean",
					"Allow_Item_Charge_Assignment": "boolean",
					"Appl_from_Item_Entry": "int",
					"Appl_to_Item_Entry": "int",
					"Bin_Code": "string",
					"Blanket_Order_Line_No": "int",
					"Blanket_Order_No": "string",
					"Control50": "Control50",
					"Cross_Reference_No": "string",
					"Deferral_Code": "string",
					"Depr_until_FA_Posting_Date": "boolean",
					"Depreciation_Book_Code": "string",
					"Description": "string",
					"Document_No": "string",
					"Drop_Shipment": "boolean",
					"Duplicate_in_Depreciation_Book": "string",
					"FA_Posting_Date": "date",
					"FOC_Reason_Code_OkFldSls": "string",
					"FilteredTypeField": "string",
					"IC_Partner_Code": "string",
					"IC_Partner_Ref_Type": "IC_Partner_Ref_Type",
					"IC_Partner_Reference": "string",
					"Inv_Disc_Amount_to_Invoice": "decimal",
					"Inv_Discount_Amount": "decimal",
					"Invoice_Disc_Pct": "decimal",
					"Invoice_Discount_Amount": "decimal",
					"Key": "string",
					"Line_Amount": "decimal",
					"Line_Discount_Amount": "decimal",
					"Line_Discount_Percent": "decimal",
					"Line_No": "int",
					"Location_Code": "string",
					"No": "string",
					"Nonstock": "boolean",
					"Outbound_Whse_Handling_Time": "string",
					"Planned_Delivery_Date": "date",
					"Planned_Shipment_Date": "date",
					"Prepayment_Percent": "decimal",
					"Prepmt_Amt_Deducted": "decimal",
					"Prepmt_Amt_Inv": "decimal",
					"Prepmt_Amt_to_Deduct": "decimal",
					"Prepmt_Line_Amount": "decimal",
					"Promised_Delivery_Date": "date",
					"Purchasing_Code": "string",
					"Qty_Assigned": "decimal",
					"Qty_to_Assemble_to_Order": "decimal",
					"Qty_to_Assign": "decimal",
					"Qty_to_Invoice": "decimal",
					"Qty_to_Ship": "decimal",
					"Quantity": "decimal",
					"Quantity_Invoiced": "decimal",
					"Quantity_Shipped": "decimal",
					"Requested_Delivery_Date": "date",
					"Reserved_Quantity": "decimal",
					"Return_Reason_Code": "string",
					"SalesLineDiscExists": "boolean",
					"SalesPriceExist": "boolean",
					"Shipment_Date": "date",
					"Shipping_Agent_Code": "string",
					"Shipping_Agent_Service_Code": "string",
					"Shipping_Time": "string",
					"ShortcutDimCode3": "string",
					"ShortcutDimCode4": "string",
					"ShortcutDimCode5": "string",
					"ShortcutDimCode6": "string",
					"ShortcutDimCode7": "string",
					"ShortcutDimCode8": "string",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"Special_Order": "boolean",
					"Substitution_Available": "boolean",
					"Tax_Area_Code": "string",
					"Tax_Group_Code": "string",
					"Tax_Liable": "boolean",
					"TotalSalesLine_Line_Amount": "decimal",
					"Total_Amount_Excl_VAT": "decimal",
					"Total_Amount_Incl_VAT": "decimal",
					"Total_VAT_Amount": "decimal",
					"Type": "Type",
					"Unit_Cost_LCY": "decimal",
					"Unit_Price": "decimal",
					"Unit_of_Measure": "string",
					"Unit_of_Measure_Code": "string",
					"Use_Duplication_List": "boolean",
					"VAT_Prod_Posting_Group": "string",
					"Variant_Code": "string",
					"Whse_Outstanding_Qty": "decimal",
					"Whse_Outstanding_Qty_Base": "decimal",
					"Work_Type_Code": "string"
				},
				"OK365_Order_Entry_Line_Fields": "string",
				"OK365_Order_Entry_Line_Filter": {
					"Criteria": "string",
					"Field": "OK365_Order_Entry_Line_Fields"
				},
				"OK365_Order_Entry_Line_List": {
					"OK365_Order_Entry_Line": "OK365_Order_Entry_Line"
				},
				"Read": {
					"Document_No": "string",
					"Line_No": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Order_Entry_Line": "OK365_Order_Entry_Line"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Order_Entry_Line_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Order_Entry_Line_List"
				},
				"Read_Result": {
					"OK365_Order_Entry_Line": "OK365_Order_Entry_Line"
				},
				"Type": "string",
				"Update": {
					"OK365_Order_Entry_Line": "OK365_Order_Entry_Line"
				},
				"UpdateMultiple": {
					"OK365_Order_Entry_Line_List": "OK365_Order_Entry_Line_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Order_Entry_Line_List": "OK365_Order_Entry_Line_List"
				},
				"Update_Result": {
					"OK365_Order_Entry_Line": "OK365_Order_Entry_Line"
				}
			}
		},
		"OK365_Order_List": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Job_Queue_Status": "string",
				"OK365_Order_List": {
					"Amount": "decimal",
					"Amount_Including_VAT": "decimal",
					"Amt_Ship_Not_Inv_LCY": "decimal",
					"Amt_Ship_Not_Inv_LCY_Base": "decimal",
					"Assigned_User_ID": "string",
					"Bill_to_Contact": "string",
					"Bill_to_Country_Region_Code": "string",
					"Bill_to_Customer_No": "string",
					"Bill_to_Name": "string",
					"Bill_to_Post_Code": "string",
					"Campaign_No": "string",
					"Completely_Shipped": "boolean",
					"Currency_Code": "string",
					"Document_Date": "date",
					"Due_Date": "date",
					"External_Document_No": "string",
					"Job_Queue_Status": "Job_Queue_Status",
					"Key": "string",
					"Location_Code": "string",
					"No": "string",
					"Package_Tracking_No": "string",
					"Payment_Discount_Percent": "decimal",
					"Payment_Terms_Code": "string",
					"Posting_Date": "date",
					"Posting_Description": "string",
					"Quote_No": "string",
					"Requested_Delivery_Date": "date",
					"Salesperson_Code": "string",
					"Sell_to_Contact": "string",
					"Sell_to_Country_Region_Code": "string",
					"Sell_to_Customer_Name": "string",
					"Sell_to_Customer_No": "string",
					"Sell_to_Post_Code": "string",
					"Ship_to_Code": "string",
					"Ship_to_Contact": "string",
					"Ship_to_Country_Region_Code": "string",
					"Ship_to_Name": "string",
					"Ship_to_Post_Code": "string",
					"Shipment_Date": "date",
					"Shipment_Method_Code": "string",
					"Shipping_Advice": "Shipping_Advice",
					"Shipping_Agent_Code": "string",
					"Shipping_Agent_Service_Code": "string",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"Status": "Status"
				},
				"OK365_Order_List_Fields": "string",
				"OK365_Order_List_Filter": {
					"Criteria": "string",
					"Field": "OK365_Order_List_Fields"
				},
				"OK365_Order_List_List": {
					"OK365_Order_List": "OK365_Order_List"
				},
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Order_List": "OK365_Order_List"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Order_List_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Order_List_List"
				},
				"Read_Result": {
					"OK365_Order_List": "OK365_Order_List"
				},
				"Shipping_Advice": "string",
				"Status": "string"
			}
		},
		"OK365_Payment_Terms": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"OK365_Payment_Terms": "OK365_Payment_Terms"
				},
				"CreateMultiple": {
					"OK365_Payment_Terms_List": "OK365_Payment_Terms_List"
				},
				"CreateMultiple_Result": {
					"OK365_Payment_Terms_List": "OK365_Payment_Terms_List"
				},
				"Create_Result": {
					"OK365_Payment_Terms": "OK365_Payment_Terms"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Payment_Terms": {
					"Calc_Pmt_Disc_on_Cr_Memos": "boolean",
					"Code": "string",
					"Description": "string",
					"Discount_Date_Calculation": "string",
					"Discount_Percent": "decimal",
					"Due_Date_Calculation": "string",
					"Key": "string"
				},
				"OK365_Payment_Terms_Fields": "string",
				"OK365_Payment_Terms_Filter": {
					"Criteria": "string",
					"Field": "OK365_Payment_Terms_Fields"
				},
				"OK365_Payment_Terms_List": {
					"OK365_Payment_Terms": "OK365_Payment_Terms"
				},
				"Read": {
					"Code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Payment_Terms": "OK365_Payment_Terms"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Payment_Terms_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Payment_Terms_List"
				},
				"Read_Result": {
					"OK365_Payment_Terms": "OK365_Payment_Terms"
				},
				"Update": {
					"OK365_Payment_Terms": "OK365_Payment_Terms"
				},
				"UpdateMultiple": {
					"OK365_Payment_Terms_List": "OK365_Payment_Terms_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Payment_Terms_List": "OK365_Payment_Terms_List"
				},
				"Update_Result": {
					"OK365_Payment_Terms": "OK365_Payment_Terms"
				}
			}
		},
		"OK365_Physical_Inventory_Journal": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"OK365_Physical_Inventory_Journal": "OK365_Physical_Inventory_Journal"
				},
				"CreateMultiple": {
					"OK365_Physical_Inventory_Journal_List": "OK365_Physical_Inventory_Journal_List"
				},
				"CreateMultiple_Result": {
					"OK365_Physical_Inventory_Journal_List": "OK365_Physical_Inventory_Journal_List"
				},
				"Create_Result": {
					"OK365_Physical_Inventory_Journal": "OK365_Physical_Inventory_Journal"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Entry_Type": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Physical_Inventory_Journal": {
					"Description": "string",
					"Document_No": "string",
					"Entry_Type": "Entry_Type",
					"Item_No": "string",
					"Journal_Batch_Name": "string",
					"Journal_Template_Name": "string",
					"Key": "string",
					"Line_No": "int",
					"Location_Code": "string",
					"Posting_Date": "date",
					"Qty_Calculated": "decimal",
					"Qty_Phys_Inventory": "decimal",
					"Quantity": "decimal",
					"Status": "Status",
					"Unit_of_Measure_Code": "string"
				},
				"OK365_Physical_Inventory_Journal_Fields": "string",
				"OK365_Physical_Inventory_Journal_Filter": {
					"Criteria": "string",
					"Field": "OK365_Physical_Inventory_Journal_Fields"
				},
				"OK365_Physical_Inventory_Journal_List": {
					"OK365_Physical_Inventory_Journal": "OK365_Physical_Inventory_Journal"
				},
				"Read": {
					"Journal_Batch_Name": "string",
					"Journal_Template_Name": "string",
					"Line_No": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Physical_Inventory_Journal": "OK365_Physical_Inventory_Journal"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Physical_Inventory_Journal_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Physical_Inventory_Journal_List"
				},
				"Read_Result": {
					"OK365_Physical_Inventory_Journal": "OK365_Physical_Inventory_Journal"
				},
				"Status": "string",
				"Update": {
					"OK365_Physical_Inventory_Journal": "OK365_Physical_Inventory_Journal"
				},
				"UpdateMultiple": {
					"OK365_Physical_Inventory_Journal_List": "OK365_Physical_Inventory_Journal_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Physical_Inventory_Journal_List": "OK365_Physical_Inventory_Journal_List"
				},
				"Update_Result": {
					"OK365_Physical_Inventory_Journal": "OK365_Physical_Inventory_Journal"
				}
			}
		},
		"OK365_Post_Codes": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"OK365_Post_Codes": "OK365_Post_Codes"
				},
				"CreateMultiple": {
					"OK365_Post_Codes_List": "OK365_Post_Codes_List"
				},
				"CreateMultiple_Result": {
					"OK365_Post_Codes_List": "OK365_Post_Codes_List"
				},
				"Create_Result": {
					"OK365_Post_Codes": "OK365_Post_Codes"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Post_Codes": {
					"City": "string",
					"Code": "string",
					"Country_Region_Code": "string",
					"County": "string",
					"Key": "string",
					"TimeZone": "string"
				},
				"OK365_Post_Codes_Fields": "string",
				"OK365_Post_Codes_Filter": {
					"Criteria": "string",
					"Field": "OK365_Post_Codes_Fields"
				},
				"OK365_Post_Codes_List": {
					"OK365_Post_Codes": "OK365_Post_Codes"
				},
				"Read": {
					"City": "string",
					"Code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Post_Codes": "OK365_Post_Codes"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Post_Codes_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Post_Codes_List"
				},
				"Read_Result": {
					"OK365_Post_Codes": "OK365_Post_Codes"
				},
				"Update": {
					"OK365_Post_Codes": "OK365_Post_Codes"
				},
				"UpdateMultiple": {
					"OK365_Post_Codes_List": "OK365_Post_Codes_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Post_Codes_List": "OK365_Post_Codes_List"
				},
				"Update_Result": {
					"OK365_Post_Codes": "OK365_Post_Codes"
				}
			}
		},
		"OK365_Posted_Sales_Invoice_Header": {
			"f": {
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Document_Exchange_Status": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Posted_Sales_Invoice_Header": {
					"Amount_Including_VAT_OkFldSls": "decimal",
					"Amount_OkFldSls": "decimal",
					"Area": "string",
					"Bill_to_Address": "string",
					"Bill_to_Address_2": "string",
					"Bill_to_City": "string",
					"Bill_to_Contact": "string",
					"Bill_to_Contact_No": "string",
					"Bill_to_Country_Region_Code": "string",
					"Bill_to_County": "string",
					"Bill_to_Customer_No_OkFldSls": "string",
					"Bill_to_Name": "string",
					"Bill_to_Post_Code": "string",
					"Cancelled": "boolean",
					"Closed": "boolean",
					"Corrective": "boolean",
					"Currency_Code": "string",
					"Direct_Debit_Mandate_ID": "string",
					"Document_Date": "date",
					"Document_Exchange_Status": "Document_Exchange_Status",
					"Due_Date": "date",
					"EU_3_Party_Trade": "boolean",
					"Exit_Point": "string",
					"External_Document_No": "string",
					"GetWorkDescription": "string",
					"Key": "string",
					"Location_Code": "string",
					"Manual_Amount_OkFldSls": "decimal",
					"Mobile_User_ID_OkFldSls": "string",
					"No": "string",
					"No_Printed": "int",
					"Order_No": "string",
					"Package_Tracking_No": "string",
					"Payment_Discount_Percent": "decimal",
					"Payment_Method_Code": "string",
					"Payment_Terms_Code": "string",
					"Pmt_Discount_Date": "date",
					"Posting_Date": "date",
					"Pre_Assigned_No": "string",
					"Quote_No": "string",
					"Remarks_OkFldSls": "string",
					"Responsibility_Center": "string",
					"Route_Code_OkFldSls": "string",
					"SalesInvLines": "Posted_Sales_Invoice_Line_List",
					"Salesperson_Code": "string",
					"SelectedPayments": "string",
					"Sell_to_Address": "string",
					"Sell_to_Address_2": "string",
					"Sell_to_City": "string",
					"Sell_to_Contact": "string",
					"Sell_to_Contact_No": "string",
					"Sell_to_Country_Region_Code": "string",
					"Sell_to_County": "string",
					"Sell_to_Customer_Name": "string",
					"Sell_to_Customer_No_OkFldSls": "string",
					"Sell_to_Post_Code": "string",
					"Ship_to_Address": "string",
					"Ship_to_Address_2": "string",
					"Ship_to_City": "string",
					"Ship_to_Code": "string",
					"Ship_to_Contact": "string",
					"Ship_to_Country_Region_Code": "string",
					"Ship_to_County": "string",
					"Ship_to_Name": "string",
					"Ship_to_Post_Code": "string",
					"Shipment_Date": "date",
					"Shipment_Method_Code": "string",
					"Shipping_Agent_Code": "string",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"Tax_Area_Code": "string",
					"Tax_Liable": "boolean",
					"Transaction_Specification": "string",
					"Transport_Method": "string",
					"User_Type_OkFldSls": "User_Type_OkFldSls",
					"Your_Reference": "string"
				},
				"OK365_Posted_Sales_Invoice_Header_Fields": "string",
				"OK365_Posted_Sales_Invoice_Header_Filter": {
					"Criteria": "string",
					"Field": "OK365_Posted_Sales_Invoice_Header_Fields"
				},
				"OK365_Posted_Sales_Invoice_Header_List": {
					"OK365_Posted_Sales_Invoice_Header": "OK365_Posted_Sales_Invoice_Header"
				},
				"Posted_Sales_Invoice_Line": {
					"Allow_Invoice_Disc": "boolean",
					"Appl_to_Item_Entry": "int",
					"Cross_Reference_No": "string",
					"Deferral_Code": "string",
					"Description": "string",
					"FOC_Reason_Code_OkFldSls": "string",
					"FilteredTypeField": "string",
					"IC_Partner_Code": "string",
					"Invoice_Discount_Amount": "decimal",
					"Job_Contract_Entry_No": "int",
					"Job_No": "string",
					"Job_Task_No": "string",
					"Key": "string",
					"Line_Amount": "decimal",
					"Line_Discount_Amount": "decimal",
					"Line_Discount_Percent": "decimal",
					"No": "string",
					"Quantity": "decimal",
					"Return_Reason_Code": "string",
					"ShortcutDimCode_x005B_3_x005D_": "string",
					"ShortcutDimCode_x005B_4_x005D_": "string",
					"ShortcutDimCode_x005B_5_x005D_": "string",
					"ShortcutDimCode_x005B_6_x005D_": "string",
					"ShortcutDimCode_x005B_7_x005D_": "string",
					"ShortcutDimCode_x005B_8_x005D_": "string",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"Tax_Area_Code": "string",
					"Tax_Group_Code": "string",
					"Tax_Liable": "boolean",
					"Total_Amount_Excl_VAT": "decimal",
					"Total_Amount_Incl_VAT": "decimal",
					"Total_VAT_Amount": "decimal",
					"Type": "Type",
					"Unit_Cost_LCY": "decimal",
					"Unit_Price": "decimal",
					"Unit_of_Measure": "string",
					"Unit_of_Measure_Code": "string",
					"Variant_Code": "string"
				},
				"Posted_Sales_Invoice_Line_List": {
					"Posted_Sales_Invoice_Line": "Posted_Sales_Invoice_Line"
				},
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Posted_Sales_Invoice_Header": "OK365_Posted_Sales_Invoice_Header"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Posted_Sales_Invoice_Header_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Posted_Sales_Invoice_Header_List"
				},
				"Read_Result": {
					"OK365_Posted_Sales_Invoice_Header": "OK365_Posted_Sales_Invoice_Header"
				},
				"Type": "string",
				"Update": {
					"OK365_Posted_Sales_Invoice_Header": "OK365_Posted_Sales_Invoice_Header"
				},
				"UpdateMultiple": {
					"OK365_Posted_Sales_Invoice_Header_List": "OK365_Posted_Sales_Invoice_Header_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Posted_Sales_Invoice_Header_List": "OK365_Posted_Sales_Invoice_Header_List"
				},
				"Update_Result": {
					"OK365_Posted_Sales_Invoice_Header": "OK365_Posted_Sales_Invoice_Header"
				},
				"User_Type_OkFldSls": "string"
			}
		},
		"OK365_Posted_Sales_Invoice_Line": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Posted_Sales_Invoice_Line": {
					"Allow_Invoice_Disc": "boolean",
					"Appl_to_Item_Entry": "int",
					"Cross_Reference_No": "string",
					"Deferral_Code": "string",
					"Description": "string",
					"FOC_Reason_Code_OkFldSls": "string",
					"FilteredTypeField": "string",
					"IC_Partner_Code": "string",
					"Invoice_Discount_Amount": "decimal",
					"Job_Contract_Entry_No": "int",
					"Job_No": "string",
					"Job_Task_No": "string",
					"Key": "string",
					"Line_Amount": "decimal",
					"Line_Discount_Amount": "decimal",
					"Line_Discount_Percent": "decimal",
					"No": "string",
					"Quantity": "decimal",
					"Return_Reason_Code": "string",
					"ShortcutDimCode_x005B_3_x005D_": "string",
					"ShortcutDimCode_x005B_4_x005D_": "string",
					"ShortcutDimCode_x005B_5_x005D_": "string",
					"ShortcutDimCode_x005B_6_x005D_": "string",
					"ShortcutDimCode_x005B_7_x005D_": "string",
					"ShortcutDimCode_x005B_8_x005D_": "string",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"Tax_Area_Code": "string",
					"Tax_Group_Code": "string",
					"Tax_Liable": "boolean",
					"Total_Amount_Excl_VAT": "decimal",
					"Total_Amount_Incl_VAT": "decimal",
					"Total_VAT_Amount": "decimal",
					"Type": "Type",
					"Unit_Cost_LCY": "decimal",
					"Unit_Price": "decimal",
					"Unit_of_Measure": "string",
					"Unit_of_Measure_Code": "string",
					"Variant_Code": "string"
				},
				"OK365_Posted_Sales_Invoice_Line_Fields": "string",
				"OK365_Posted_Sales_Invoice_Line_Filter": {
					"Criteria": "string",
					"Field": "OK365_Posted_Sales_Invoice_Line_Fields"
				},
				"OK365_Posted_Sales_Invoice_Line_List": {
					"OK365_Posted_Sales_Invoice_Line": "OK365_Posted_Sales_Invoice_Line"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Posted_Sales_Invoice_Line": "OK365_Posted_Sales_Invoice_Line"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Posted_Sales_Invoice_Line_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Posted_Sales_Invoice_Line_List"
				},
				"Type": "string"
			}
		},
		"OK365_Posted_Sales_Invoice_List": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Document_Exchange_Status": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Posted_Sales_Invoice_List": {
					"Amount": "decimal",
					"Amount_Including_VAT": "decimal",
					"Bill_to_Contact": "string",
					"Bill_to_Country_Region_Code": "string",
					"Bill_to_Customer_No": "string",
					"Bill_to_Name": "string",
					"Bill_to_Post_Code": "string",
					"Cancelled": "boolean",
					"Closed": "boolean",
					"Corrective": "boolean",
					"Currency_Code": "string",
					"Document_Date": "date",
					"Document_Exchange_Status": "Document_Exchange_Status",
					"Due_Date": "date",
					"External_Document_No": "string",
					"Key": "string",
					"Location_Code": "string",
					"No": "string",
					"No_Printed": "int",
					"Order_No": "string",
					"Payment_Discount_Percent": "decimal",
					"Payment_Terms_Code": "string",
					"Posting_Date": "date",
					"Remaining_Amount": "decimal",
					"Salesperson_Code": "string",
					"Sell_to_Contact": "string",
					"Sell_to_Country_Region_Code": "string",
					"Sell_to_Customer_Name": "string",
					"Sell_to_Customer_No": "string",
					"Sell_to_Post_Code": "string",
					"Ship_to_Code": "string",
					"Ship_to_Contact": "string",
					"Ship_to_Country_Region_Code": "string",
					"Ship_to_Name": "string",
					"Ship_to_Post_Code": "string",
					"Shipment_Date": "date",
					"Shipment_Method_Code": "string",
					"Shipping_Agent_Code": "string",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"_x003C_Document_Exchange_Status_x003E_": "boolean"
				},
				"OK365_Posted_Sales_Invoice_List_Fields": "string",
				"OK365_Posted_Sales_Invoice_List_Filter": {
					"Criteria": "string",
					"Field": "OK365_Posted_Sales_Invoice_List_Fields"
				},
				"OK365_Posted_Sales_Invoice_List_List": {
					"OK365_Posted_Sales_Invoice_List": "OK365_Posted_Sales_Invoice_List"
				},
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Posted_Sales_Invoice_List": "OK365_Posted_Sales_Invoice_List"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Posted_Sales_Invoice_List_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Posted_Sales_Invoice_List_List"
				},
				"Read_Result": {
					"OK365_Posted_Sales_Invoice_List": "OK365_Posted_Sales_Invoice_List"
				}
			}
		},
		"OK365_Reason_code": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"OK365_Reason_code": "OK365_Reason_code"
				},
				"CreateMultiple": {
					"OK365_Reason_code_List": "OK365_Reason_code_List"
				},
				"CreateMultiple_Result": {
					"OK365_Reason_code_List": "OK365_Reason_code_List"
				},
				"Create_Result": {
					"OK365_Reason_code": "OK365_Reason_code"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Reason_code": {
					"Code": "string",
					"Description": "string",
					"Key": "string"
				},
				"OK365_Reason_code_Fields": "string",
				"OK365_Reason_code_Filter": {
					"Criteria": "string",
					"Field": "OK365_Reason_code_Fields"
				},
				"OK365_Reason_code_List": {
					"OK365_Reason_code": "OK365_Reason_code"
				},
				"Read": {
					"Code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Reason_code": "OK365_Reason_code"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Reason_code_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Reason_code_List"
				},
				"Read_Result": {
					"OK365_Reason_code": "OK365_Reason_code"
				},
				"Update": {
					"OK365_Reason_code": "OK365_Reason_code"
				},
				"UpdateMultiple": {
					"OK365_Reason_code_List": "OK365_Reason_code_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Reason_code_List": "OK365_Reason_code_List"
				},
				"Update_Result": {
					"OK365_Reason_code": "OK365_Reason_code"
				}
			}
		},
		"OK365_Return_reason_code": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"OK365_Return_reason_code": "OK365_Return_reason_code"
				},
				"CreateMultiple": {
					"OK365_Return_reason_code_List": "OK365_Return_reason_code_List"
				},
				"CreateMultiple_Result": {
					"OK365_Return_reason_code_List": "OK365_Return_reason_code_List"
				},
				"Create_Result": {
					"OK365_Return_reason_code": "OK365_Return_reason_code"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Return_reason_code": {
					"Code": "string",
					"Default_Location_Code": "string",
					"Description": "string",
					"Inventory_Value_Zero": "boolean",
					"Key": "string"
				},
				"OK365_Return_reason_code_Fields": "string",
				"OK365_Return_reason_code_Filter": {
					"Criteria": "string",
					"Field": "OK365_Return_reason_code_Fields"
				},
				"OK365_Return_reason_code_List": {
					"OK365_Return_reason_code": "OK365_Return_reason_code"
				},
				"Read": {
					"Code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Return_reason_code": "OK365_Return_reason_code"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Return_reason_code_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Return_reason_code_List"
				},
				"Read_Result": {
					"OK365_Return_reason_code": "OK365_Return_reason_code"
				},
				"Update": {
					"OK365_Return_reason_code": "OK365_Return_reason_code"
				},
				"UpdateMultiple": {
					"OK365_Return_reason_code_List": "OK365_Return_reason_code_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Return_reason_code_List": "OK365_Return_reason_code_List"
				},
				"Update_Result": {
					"OK365_Return_reason_code": "OK365_Return_reason_code"
				}
			}
		},
		"OK365_Route_And_Customer_Priority": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"OK365_Route_And_Customer_Priority": "OK365_Route_And_Customer_Priority"
				},
				"CreateMultiple": {
					"OK365_Route_And_Customer_Priority_List": "OK365_Route_And_Customer_Priority_List"
				},
				"CreateMultiple_Result": {
					"OK365_Route_And_Customer_Priority_List": "OK365_Route_And_Customer_Priority_List"
				},
				"Create_Result": {
					"OK365_Route_And_Customer_Priority": "OK365_Route_And_Customer_Priority"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Route_And_Customer_Priority": {
					"Address": "string",
					"Address_2_OkFldSls": "string",
					"Available_Credit_Limit_OKFldSls": "decimal",
					"Balance_LCY_OkFldSls": "decimal",
					"City_OkFldSls": "string",
					"Country_Region_Code_OkFldSls": "string",
					"Credit_Limit_LCY_OkFldSls": "decimal",
					"Currency_code_OkFldSls": "string",
					"Cust_Posting_Group_OkFldSls": "string",
					"Custgroup_Dimension_OkFldSls": "string",
					"Customer_Code": "string",
					"Customer_Price_Group_OkFldSls": "string",
					"Friday": "boolean",
					"Key": "string",
					"Line_No": "int",
					"Location_code_OkFldSls": "string",
					"Monday": "boolean",
					"Name": "string",
					"Payment_Term_Code_OkFldSls": "string",
					"Phone_No_OkFldSls": "string",
					"Post_Code_OkFldSls": "string",
					"Route_No": "string",
					"Salesperson_Code_OkFldSls": "string",
					"Saturday": "boolean",
					"Search_Name_OkFldSls": "string",
					"Sequence": "decimal",
					"Sunday": "boolean",
					"Thursday": "boolean",
					"Tuesday": "boolean",
					"VAT_Buss_Posting_Grp_OkFldSls": "string",
					"Wednesday": "boolean"
				},
				"OK365_Route_And_Customer_Priority_Fields": "string",
				"OK365_Route_And_Customer_Priority_Filter": {
					"Criteria": "string",
					"Field": "OK365_Route_And_Customer_Priority_Fields"
				},
				"OK365_Route_And_Customer_Priority_List": {
					"OK365_Route_And_Customer_Priority": "OK365_Route_And_Customer_Priority"
				},
				"Read": {
					"Customer_Code": "string",
					"Line_No": "int",
					"Route_No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Route_And_Customer_Priority": "OK365_Route_And_Customer_Priority"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Route_And_Customer_Priority_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Route_And_Customer_Priority_List"
				},
				"Read_Result": {
					"OK365_Route_And_Customer_Priority": "OK365_Route_And_Customer_Priority"
				},
				"Update": {
					"OK365_Route_And_Customer_Priority": "OK365_Route_And_Customer_Priority"
				},
				"UpdateMultiple": {
					"OK365_Route_And_Customer_Priority_List": "OK365_Route_And_Customer_Priority_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Route_And_Customer_Priority_List": "OK365_Route_And_Customer_Priority_List"
				},
				"Update_Result": {
					"OK365_Route_And_Customer_Priority": "OK365_Route_And_Customer_Priority"
				}
			}
		},
		"OK365_Route_Master": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"OK365_Route_Master": "OK365_Route_Master"
				},
				"CreateMultiple": {
					"OK365_Route_Master_List": "OK365_Route_Master_List"
				},
				"CreateMultiple_Result": {
					"OK365_Route_Master_List": "OK365_Route_Master_List"
				},
				"Create_Result": {
					"OK365_Route_Master": "OK365_Route_Master"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Route_Master": {
					"Cash_Rcpt_Jnl_Batch_Name": "string",
					"Cash_Rcpt_Jnl_Template": "string",
					"Chq_Rcpt_Bat_Name_OkFldSls": "string",
					"Chq_Rcpt_Jnl_Tmplte_OkFldSls": "string",
					"Description": "string",
					"Key": "string",
					"Location_Code": "string",
					"No_Series_Pay": "string",
					"No_Series_Term": "string",
					"Route_Code": "string"
				},
				"OK365_Route_Master_Fields": "string",
				"OK365_Route_Master_Filter": {
					"Criteria": "string",
					"Field": "OK365_Route_Master_Fields"
				},
				"OK365_Route_Master_List": {
					"OK365_Route_Master": "OK365_Route_Master"
				},
				"Read": {
					"Route_Code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Route_Master": "OK365_Route_Master"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Route_Master_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Route_Master_List"
				},
				"Read_Result": {
					"OK365_Route_Master": "OK365_Route_Master"
				},
				"Update": {
					"OK365_Route_Master": "OK365_Route_Master"
				},
				"UpdateMultiple": {
					"OK365_Route_Master_List": "OK365_Route_Master_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Route_Master_List": "OK365_Route_Master_List"
				},
				"Update_Result": {
					"OK365_Route_Master": "OK365_Route_Master"
				}
			}
		},
		"OK365_Sales_Invoice": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"Delete_SalesLines": {
					"i": "Delete_SalesLines",
					"o": "Delete_SalesLines_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"OK365_Sales_Invoice": "OK365_Sales_Invoice"
				},
				"CreateMultiple": {
					"OK365_Sales_Invoice_List": "OK365_Sales_Invoice_List"
				},
				"CreateMultiple_Result": {
					"OK365_Sales_Invoice_List": "OK365_Sales_Invoice_List"
				},
				"Create_Result": {
					"OK365_Sales_Invoice": "OK365_Sales_Invoice"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Delete_SalesLines": {
					"SalesLines_Key": "string"
				},
				"Delete_SalesLines_Result": {
					"Delete_SalesLines_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IC_Partner_Ref_Type": "string",
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Job_Queue_Status": "string",
				"OK365_Sales_Invoice": {
					"Area": "string",
					"Assigned_User_ID": "string",
					"Bill_to_Address": "string",
					"Bill_to_Address_2": "string",
					"Bill_to_City": "string",
					"Bill_to_Contact": "string",
					"Bill_to_Contact_No": "string",
					"Bill_to_Country_Region_Code": "string",
					"Bill_to_County": "string",
					"Bill_to_Name": "string",
					"Bill_to_Post_Code": "string",
					"Campaign_No": "string",
					"Currency_Code": "string",
					"Direct_Debit_Mandate_ID": "string",
					"Document_Date": "date",
					"Due_Date": "date",
					"EU_3_Party_Trade": "boolean",
					"Exit_Point": "string",
					"External_Document_No": "string",
					"Incoming_Document_Entry_No": "int",
					"Job_Queue_Status": "Job_Queue_Status",
					"Key": "string",
					"Location_Code": "string",
					"No": "string",
					"Package_Tracking_No": "string",
					"Payment_Discount_Percent": "decimal",
					"Payment_Method_Code": "string",
					"Payment_Terms_Code": "string",
					"Pmt_Discount_Date": "date",
					"Posting_Date": "date",
					"Posting_Description": "string",
					"Prices_Including_VAT": "boolean",
					"Quote_No": "string",
					"Responsibility_Center": "string",
					"SalesLines": "Sales_Invoice_Line_List",
					"Salesperson_Code": "string",
					"SelectedPayments": "string",
					"Sell_to_Address": "string",
					"Sell_to_Address_2": "string",
					"Sell_to_City": "string",
					"Sell_to_Contact": "string",
					"Sell_to_Contact_No": "string",
					"Sell_to_Country_Region_Code": "string",
					"Sell_to_County": "string",
					"Sell_to_Customer_Name": "string",
					"Sell_to_Customer_No": "string",
					"Sell_to_Post_Code": "string",
					"Ship_to_Address": "string",
					"Ship_to_Address_2": "string",
					"Ship_to_City": "string",
					"Ship_to_Code": "string",
					"Ship_to_Contact": "string",
					"Ship_to_Country_Region_Code": "string",
					"Ship_to_County": "string",
					"Ship_to_Name": "string",
					"Ship_to_Post_Code": "string",
					"Shipment_Date": "date",
					"Shipment_Method_Code": "string",
					"Shipping_Agent_Code": "string",
					"Shipping_Agent_Service_Code": "string",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"Status": "Status",
					"Transaction_Specification": "string",
					"Transaction_Type": "string",
					"Transport_Method": "string",
					"VAT_Bus_Posting_Group": "string",
					"WorkDescription": "string",
					"Your_Reference": "string"
				},
				"OK365_Sales_Invoice_Fields": "string",
				"OK365_Sales_Invoice_Filter": {
					"Criteria": "string",
					"Field": "OK365_Sales_Invoice_Fields"
				},
				"OK365_Sales_Invoice_List": {
					"OK365_Sales_Invoice": "OK365_Sales_Invoice"
				},
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Sales_Invoice": "OK365_Sales_Invoice"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Sales_Invoice_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Sales_Invoice_List"
				},
				"Read_Result": {
					"OK365_Sales_Invoice": "OK365_Sales_Invoice"
				},
				"Sales_Invoice_Line": {
					"Allow_Invoice_Disc": "boolean",
					"Allow_Item_Charge_Assignment": "boolean",
					"Appl_from_Item_Entry": "int",
					"Appl_to_Item_Entry": "int",
					"Bin_Code": "string",
					"Blanket_Order_Line_No": "int",
					"Blanket_Order_No": "string",
					"Cross_Reference_No": "string",
					"Deferral_Code": "string",
					"Depr_until_FA_Posting_Date": "boolean",
					"Depreciation_Book_Code": "string",
					"Description": "string",
					"Document_No": "string",
					"Duplicate_in_Depreciation_Book": "string",
					"FA_Posting_Date": "date",
					"FilteredTypeField": "string",
					"IC_Partner_Code": "string",
					"IC_Partner_Ref_Type": "IC_Partner_Ref_Type",
					"IC_Partner_Reference": "string",
					"Inv_Discount_Amount": "decimal",
					"Invoice_Disc_Pct": "decimal",
					"Invoice_Discount_Amount": "decimal",
					"Job_Contract_Entry_No": "int",
					"Job_No": "string",
					"Job_Task_No": "string",
					"Key": "string",
					"LineDiscExists": "boolean",
					"Line_Amount": "decimal",
					"Line_Discount_Amount": "decimal",
					"Line_Discount_Percent": "decimal",
					"Line_No": "int",
					"Location_Code": "string",
					"No": "string",
					"Nonstock": "boolean",
					"PriceExists": "boolean",
					"Qty_Assigned": "decimal",
					"Qty_to_Assign": "decimal",
					"Quantity": "decimal",
					"Return_Reason_Code": "string",
					"Shipping_Agent_Code": "string",
					"Shipping_Agent_Service_Code": "string",
					"ShortcutDimCode3": "string",
					"ShortcutDimCode4": "string",
					"ShortcutDimCode5": "string",
					"ShortcutDimCode6": "string",
					"ShortcutDimCode7": "string",
					"ShortcutDimCode8": "string",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"Tax_Area_Code": "string",
					"Tax_Category": "string",
					"Tax_Group_Code": "string",
					"Tax_Liable": "boolean",
					"TotalSalesLine_Line_Amount": "decimal",
					"Total_Amount_Excl_VAT": "decimal",
					"Total_Amount_Incl_VAT": "decimal",
					"Total_VAT_Amount": "decimal",
					"Type": "Type",
					"Unit_Cost_LCY": "decimal",
					"Unit_Price": "decimal",
					"Unit_of_Measure": "string",
					"Unit_of_Measure_Code": "string",
					"Use_Duplication_List": "boolean",
					"VAT_Prod_Posting_Group": "string",
					"Variant_Code": "string",
					"Work_Type_Code": "string"
				},
				"Sales_Invoice_Line_List": {
					"Sales_Invoice_Line": "Sales_Invoice_Line"
				},
				"Status": "string",
				"Type": "string",
				"Update": {
					"OK365_Sales_Invoice": "OK365_Sales_Invoice"
				},
				"UpdateMultiple": {
					"OK365_Sales_Invoice_List": "OK365_Sales_Invoice_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Sales_Invoice_List": "OK365_Sales_Invoice_List"
				},
				"Update_Result": {
					"OK365_Sales_Invoice": "OK365_Sales_Invoice"
				}
			}
		},
		"OK365_Sales_Price": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"OK365_Sales_Price": "OK365_Sales_Price"
				},
				"CreateMultiple": {
					"OK365_Sales_Price_List": "OK365_Sales_Price_List"
				},
				"CreateMultiple_Result": {
					"OK365_Sales_Price_List": "OK365_Sales_Price_List"
				},
				"Create_Result": {
					"OK365_Sales_Price": "OK365_Sales_Price"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Sales_Price": {
					"Allow_Invoice_Disc": "boolean",
					"Allow_Line_Disc": "boolean",
					"CurrencyCodeFilterCtrl": "string",
					"Currency_Code": "string",
					"Ending_Date": "date",
					"FilterDescription": "string",
					"ItemNoFilterCtrl": "string",
					"Item_No": "string",
					"Key": "string",
					"Minimum_Quantity": "decimal",
					"Price_Includes_VAT": "boolean",
					"SalesCodeFilterCtrl": "string",
					"Sales_Code": "string",
					"Sales_Type": "Sales_Type",
					"StartingDateFilter": "string",
					"Starting_Date": "date",
					"Unit_Price": "decimal",
					"Unit_of_Measure_Code": "string",
					"VAT_Bus_Posting_Gr_Price": "string",
					"Variant_Code": "string"
				},
				"OK365_Sales_Price_Fields": "string",
				"OK365_Sales_Price_Filter": {
					"Criteria": "string",
					"Field": "OK365_Sales_Price_Fields"
				},
				"OK365_Sales_Price_List": {
					"OK365_Sales_Price": "OK365_Sales_Price"
				},
				"Read": {
					"Currency_Code": "string",
					"Item_No": "string",
					"Minimum_Quantity": "decimal",
					"Sales_Code": "string",
					"Sales_Type": "string",
					"Starting_Date": "date",
					"Unit_of_Measure_Code": "string",
					"Variant_Code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Sales_Price": "OK365_Sales_Price"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Sales_Price_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Sales_Price_List"
				},
				"Read_Result": {
					"OK365_Sales_Price": "OK365_Sales_Price"
				},
				"Sales_Type": "string",
				"Update": {
					"OK365_Sales_Price": "OK365_Sales_Price"
				},
				"UpdateMultiple": {
					"OK365_Sales_Price_List": "OK365_Sales_Price_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Sales_Price_List": "OK365_Sales_Price_List"
				},
				"Update_Result": {
					"OK365_Sales_Price": "OK365_Sales_Price"
				}
			}
		},
		"OK365_Sales_Return_Order": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"Delete_SalesLines": {
					"i": "Delete_SalesLines",
					"o": "Delete_SalesLines_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Applies_to_Doc_Type": "string",
				"Control28": "string",
				"Create": {
					"OK365_Sales_Return_Order": "OK365_Sales_Return_Order"
				},
				"CreateMultiple": {
					"OK365_Sales_Return_Order_List": "OK365_Sales_Return_Order_List"
				},
				"CreateMultiple_Result": {
					"OK365_Sales_Return_Order_List": "OK365_Sales_Return_Order_List"
				},
				"Create_Result": {
					"OK365_Sales_Return_Order": "OK365_Sales_Return_Order"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Delete_SalesLines": {
					"SalesLines_Key": "string"
				},
				"Delete_SalesLines_Result": {
					"Delete_SalesLines_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IC_Partner_Ref_Type": "string",
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Job_Queue_Status": "string",
				"OK365_Sales_Return_Order": {
					"Amount_Including_VAT_OkFldSls": "decimal",
					"Amount_OkFldSls": "decimal",
					"Applies_to_Doc_No": "string",
					"Applies_to_Doc_Type": "Applies_to_Doc_Type",
					"Applies_to_ID": "string",
					"Area": "string",
					"Assigned_User_ID": "string",
					"Bill_to_Address": "string",
					"Bill_to_Address_2": "string",
					"Bill_to_City": "string",
					"Bill_to_Contact": "string",
					"Bill_to_Contact_No": "string",
					"Bill_to_Country_Region_Code": "string",
					"Bill_to_County": "string",
					"Bill_to_Customer_No_OkFldSls": "string",
					"Bill_to_Name": "string",
					"Bill_to_Post_Code": "string",
					"Campaign_No": "string",
					"Currency_Code": "string",
					"Document_Date": "date",
					"EU_3_Party_Trade": "boolean",
					"Exit_Point": "string",
					"External_Document_No": "string",
					"Job_Queue_Status": "Job_Queue_Status",
					"Key": "string",
					"Location_Code": "string",
					"Mobile_User_ID_OkFldSls": "string",
					"No": "string",
					"No_of_Archived_Versions": "int",
					"Order_Date": "date",
					"Package_Tracking_No": "string",
					"Payment_Method_Code": "string",
					"Payment_Terms_Code": "string",
					"Posting_Date": "date",
					"Prices_Including_VAT": "boolean",
					"Remarks_OkFldSls": "string",
					"Responsibility_Center": "string",
					"Route_Code_OkFldSls": "string",
					"SalesLines": "Sales_Return_Order_Line_List",
					"Salesperson_Code": "string",
					"Sell_to_Address": "string",
					"Sell_to_Address_2": "string",
					"Sell_to_City": "string",
					"Sell_to_Contact": "string",
					"Sell_to_Contact_No": "string",
					"Sell_to_Country_Region_Code": "string",
					"Sell_to_County": "string",
					"Sell_to_Customer_Name": "string",
					"Sell_to_Customer_No": "string",
					"Sell_to_Post_Code": "string",
					"Ship_to_Address": "string",
					"Ship_to_Address_2": "string",
					"Ship_to_City": "string",
					"Ship_to_Contact": "string",
					"Ship_to_Country_Region_Code": "string",
					"Ship_to_County": "string",
					"Ship_to_Name": "string",
					"Ship_to_Post_Code": "string",
					"Shipment_Date": "date",
					"Shipping_Agent_Code": "string",
					"Shipping_Agent_Service_Code": "string",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"Status": "Status",
					"Tax_Area_Code": "string",
					"Tax_Liable": "boolean",
					"Transaction_Specification": "string",
					"Transaction_Type": "string",
					"Transport_Method": "string",
					"User_Type_OkFldSls": "User_Type_OkFldSls",
					"VAT_Bus_Posting_Group": "string",
					"Your_Reference": "string"
				},
				"OK365_Sales_Return_Order_Fields": "string",
				"OK365_Sales_Return_Order_Filter": {
					"Criteria": "string",
					"Field": "OK365_Sales_Return_Order_Fields"
				},
				"OK365_Sales_Return_Order_List": {
					"OK365_Sales_Return_Order": "OK365_Sales_Return_Order"
				},
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Sales_Return_Order": "OK365_Sales_Return_Order"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Sales_Return_Order_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Sales_Return_Order_List"
				},
				"Read_Result": {
					"OK365_Sales_Return_Order": "OK365_Sales_Return_Order"
				},
				"Sales_Return_Order_Line": {
					"Allow_Invoice_Disc": "boolean",
					"Allow_Item_Charge_Assignment": "boolean",
					"Appl_from_Item_Entry": "int",
					"Appl_to_Item_Entry": "int",
					"Bin_Code": "string",
					"Blanket_Order_Line_No": "int",
					"Blanket_Order_No": "string",
					"Control28": "Control28",
					"Cross_Reference_No": "string",
					"Deferral_Code": "string",
					"Description": "string",
					"FilteredTypeField": "string",
					"IC_Partner_Ref_Type": "IC_Partner_Ref_Type",
					"IC_Partner_Reference": "string",
					"Inv_Discount_Amount": "decimal",
					"Invoice_Disc_Pct": "decimal",
					"Invoice_Discount_Amount": "decimal",
					"Key": "string",
					"Line_Amount": "decimal",
					"Line_Discount_Amount": "decimal",
					"Line_Discount_Percent": "decimal",
					"Location_Code": "string",
					"No": "string",
					"Nonstock": "boolean",
					"Planned_Delivery_Date": "date",
					"Planned_Shipment_Date": "date",
					"Promised_Delivery_Date": "date",
					"Qty_Assigned": "decimal",
					"Qty_to_Assign": "decimal",
					"Qty_to_Invoice": "decimal",
					"Quantity": "decimal",
					"Quantity_Invoiced": "decimal",
					"Requested_Delivery_Date": "date",
					"Reserved_Quantity": "decimal",
					"Return_Qty_Received": "decimal",
					"Return_Qty_to_Receive": "decimal",
					"Return_Reason_Code": "string",
					"Returns_Deferral_Start_Date": "date",
					"Shipment_Date": "date",
					"Shipping_Agent_Code": "string",
					"Shipping_Agent_Service_Code": "string",
					"Shipping_Time": "string",
					"ShortcutDimCode3": "string",
					"ShortcutDimCode4": "string",
					"ShortcutDimCode5": "string",
					"ShortcutDimCode6": "string",
					"ShortcutDimCode7": "string",
					"ShortcutDimCode8": "string",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"SubtotalExclVAT": "decimal",
					"Tax_Area_Code": "string",
					"Tax_Group_Code": "string",
					"Tax_Liable": "boolean",
					"Total_Amount_Excl_VAT": "decimal",
					"Total_Amount_Incl_VAT": "decimal",
					"Total_VAT_Amount": "decimal",
					"Type": "Type",
					"Unit_Cost_LCY": "decimal",
					"Unit_Price": "decimal",
					"Unit_of_Measure": "string",
					"Unit_of_Measure_Code": "string",
					"VAT_Prod_Posting_Group": "string",
					"Variant_Code": "string"
				},
				"Sales_Return_Order_Line_List": {
					"Sales_Return_Order_Line": "Sales_Return_Order_Line"
				},
				"Status": "string",
				"Type": "string",
				"Update": {
					"OK365_Sales_Return_Order": "OK365_Sales_Return_Order"
				},
				"UpdateMultiple": {
					"OK365_Sales_Return_Order_List": "OK365_Sales_Return_Order_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Sales_Return_Order_List": "OK365_Sales_Return_Order_List"
				},
				"Update_Result": {
					"OK365_Sales_Return_Order": "OK365_Sales_Return_Order"
				},
				"User_Type_OkFldSls": "string"
			}
		},
		"OK365_Sales__x0026__Receivable_Setup": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Appln_between_Currencies": "string",
				"Archive_Quotes": "string",
				"Credit_Warnings": "string",
				"Default_Posting_Date": "string",
				"Default_Quantity_to_Ship": "string",
				"Discount_Posting": "string",
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Logo_Position_on_Documents": "string",
				"OK365_Sales__x0026__Receivable_Setup": {
					"Allow_Document_Deletion_Before": "date",
					"Allow_VAT_Difference": "boolean",
					"Appln_between_Currencies": "Appln_between_Currencies",
					"Archive_Blanket_Orders": "boolean",
					"Archive_Orders": "boolean",
					"Archive_Quotes": "Archive_Quotes",
					"Archive_Return_Orders": "boolean",
					"Batch_Archiving_Quotes": "boolean",
					"Blanket_Order_Nos": "string",
					"Calc_Inv_Disc_per_VAT_ID": "boolean",
					"Calc_Inv_Discount": "boolean",
					"Canc_Iss_Fin_Ch_Mem_Nos": "string",
					"Canceled_Issued_Reminder_Nos": "string",
					"Check_Prepmt_when_Posting": "boolean",
					"Copy_Cmts_Ret_Ord_to_Cr_Memo": "boolean",
					"Copy_Cmts_Ret_Ord_to_Ret_Rcpt": "boolean",
					"Copy_Comments_Blanket_to_Order": "boolean",
					"Copy_Comments_Order_to_Invoice": "boolean",
					"Copy_Comments_Order_to_Shpt": "boolean",
					"Copy_Customer_Name_to_Entries": "boolean",
					"Copy_Line_Descr_to_G_L_Entry": "boolean",
					"Create_Item_from_Description": "boolean",
					"Create_Item_from_Item_No": "boolean",
					"Credit_Memo_Nos": "string",
					"Credit_Warnings": "Credit_Warnings",
					"Customer_Group_Dimension_Code": "string",
					"Customer_Nos": "string",
					"DefaultItemQuantity": "boolean",
					"Default_Posting_Date": "Default_Posting_Date",
					"Default_Quantity_to_Ship": "Default_Quantity_to_Ship",
					"Direct_Debit_Mandate_Nos": "string",
					"Discount_Posting": "Discount_Posting",
					"Empty_bottle_cost_per_Piece_OkFldSls": "decimal",
					"Exact_Cost_Reversing_Mandatory": "boolean",
					"Ext_Doc_No_Mandatory": "boolean",
					"Fin_Chrg_Memo_Nos": "string",
					"Freight_G_L_Acc_No": "string",
					"GL_Account_for_Empty_Returns_OkFldSls": "string",
					"Ignore_Updated_Addresses": "boolean",
					"Invoice_Nos": "string",
					"Invoice_Rounding": "boolean",
					"Issued_Fin_Chrg_M_Nos": "string",
					"Issued_Reminder_Nos": "string",
					"Item_Rcls_Batch_OkFldSls": "string",
					"Item_Rcls_Jnl_Temp_OkFldSls": "string",
					"Job_Queue_Category_Code": "string",
					"Key": "string",
					"Logo_Position_on_Documents": "Logo_Position_on_Documents",
					"Main_Location_OkFldSls": "string",
					"Notify_On_Success": "boolean",
					"Order_Nos": "string",
					"Post__x0026__Print_with_Job_Queue": "boolean",
					"Post_with_Job_Queue": "boolean",
					"Posted_Credit_Memo_Nos": "string",
					"Posted_Invoice_Nos": "string",
					"Posted_Prepmt_Cr_Memo_Nos": "string",
					"Posted_Prepmt_Inv_Nos": "string",
					"Posted_Return_Receipt_Nos": "string",
					"Posted_Shipment_Nos": "string",
					"Prepmt_Auto_Update_Frequency": "Prepmt_Auto_Update_Frequency",
					"Price_Calculation_Method": "Price_Calculation_Method",
					"Quote_Nos": "string",
					"Quote_Validity_Calculation": "string",
					"Reminder_Nos": "string",
					"Report_Output_Type": "Report_Output_Type",
					"Return_Order_Nos": "string",
					"Return_Receipt_on_Credit_Memo": "boolean",
					"Salesperson_Dimension_Code": "string",
					"Shipment_on_Invoice": "boolean",
					"Skip_Manual_Reservation": "boolean",
					"Stockout_Warning": "boolean",
					"VAT_Bus_Posting_Gr_Price": "string",
					"Write_in_Product_No": "string",
					"Write_in_Product_Type": "Write_in_Product_Type"
				},
				"OK365_Sales__x0026__Receivable_Setup_Fields": "string",
				"OK365_Sales__x0026__Receivable_Setup_Filter": {
					"Criteria": "string",
					"Field": "OK365_Sales__x0026__Receivable_Setup_Fields"
				},
				"OK365_Sales__x0026__Receivable_Setup_List": {
					"OK365_Sales__x0026__Receivable_Setup": "OK365_Sales__x0026__Receivable_Setup"
				},
				"Prepmt_Auto_Update_Frequency": "string",
				"Price_Calculation_Method": "string",
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Sales__x0026__Receivable_Setup": "OK365_Sales__x0026__Receivable_Setup"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Sales__x0026__Receivable_Setup_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Sales__x0026__Receivable_Setup_List"
				},
				"Report_Output_Type": "string",
				"Update": {
					"OK365_Sales__x0026__Receivable_Setup": "OK365_Sales__x0026__Receivable_Setup"
				},
				"UpdateMultiple": {
					"OK365_Sales__x0026__Receivable_Setup_List": "OK365_Sales__x0026__Receivable_Setup_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Sales__x0026__Receivable_Setup_List": "OK365_Sales__x0026__Receivable_Setup_List"
				},
				"Update_Result": {
					"OK365_Sales__x0026__Receivable_Setup": "OK365_Sales__x0026__Receivable_Setup"
				},
				"Write_in_Product_Type": "string"
			}
		},
		"OK365_Salespeople_Purchasers": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Salespeople_Purchasers": {
					"Code": "string",
					"Commission_Percent": "decimal",
					"Key": "string",
					"Name": "string",
					"Phone_No": "string",
					"Privacy_Blocked": "boolean"
				},
				"OK365_Salespeople_Purchasers_Fields": "string",
				"OK365_Salespeople_Purchasers_Filter": {
					"Criteria": "string",
					"Field": "OK365_Salespeople_Purchasers_Fields"
				},
				"OK365_Salespeople_Purchasers_List": {
					"OK365_Salespeople_Purchasers": "OK365_Salespeople_Purchasers"
				},
				"Read": {
					"Code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Salespeople_Purchasers": "OK365_Salespeople_Purchasers"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Salespeople_Purchasers_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Salespeople_Purchasers_List"
				},
				"Read_Result": {
					"OK365_Salespeople_Purchasers": "OK365_Salespeople_Purchasers"
				}
			}
		},
		"OK365_Shipment_Method": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Address_Format": "string",
				"Contact_Address_Format": "string",
				"Create": {
					"OK365_Shipment_Method": "OK365_Shipment_Method"
				},
				"CreateMultiple": {
					"OK365_Shipment_Method_List": "OK365_Shipment_Method_List"
				},
				"CreateMultiple_Result": {
					"OK365_Shipment_Method_List": "OK365_Shipment_Method_List"
				},
				"Create_Result": {
					"OK365_Shipment_Method": "OK365_Shipment_Method"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Shipment_Method": {
					"Address_Format": "Address_Format",
					"Code": "string",
					"Contact_Address_Format": "Contact_Address_Format",
					"County_Name": "string",
					"EU_Country_Region_Code": "string",
					"ISO_Code": "string",
					"ISO_Numeric_Code": "string",
					"Intrastat_Code": "string",
					"Key": "string",
					"Name": "string",
					"VAT_Scheme": "string"
				},
				"OK365_Shipment_Method_Fields": "string",
				"OK365_Shipment_Method_Filter": {
					"Criteria": "string",
					"Field": "OK365_Shipment_Method_Fields"
				},
				"OK365_Shipment_Method_List": {
					"OK365_Shipment_Method": "OK365_Shipment_Method"
				},
				"Read": {
					"Code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Shipment_Method": "OK365_Shipment_Method"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Shipment_Method_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Shipment_Method_List"
				},
				"Read_Result": {
					"OK365_Shipment_Method": "OK365_Shipment_Method"
				},
				"Update": {
					"OK365_Shipment_Method": "OK365_Shipment_Method"
				},
				"UpdateMultiple": {
					"OK365_Shipment_Method_List": "OK365_Shipment_Method_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Shipment_Method_List": "OK365_Shipment_Method_List"
				},
				"Update_Result": {
					"OK365_Shipment_Method": "OK365_Shipment_Method"
				}
			}
		},
		"OK365_Shipping_Address_Card": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"OK365_Shipping_Address_Card": "OK365_Shipping_Address_Card"
				},
				"CreateMultiple": {
					"OK365_Shipping_Address_Card_List": "OK365_Shipping_Address_Card_List"
				},
				"CreateMultiple_Result": {
					"OK365_Shipping_Address_Card_List": "OK365_Shipping_Address_Card_List"
				},
				"Create_Result": {
					"OK365_Shipping_Address_Card": "OK365_Shipping_Address_Card"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Shipping_Address_Card": {
					"Address": "string",
					"Address_2": "string",
					"City": "string",
					"Code": "string",
					"Contact": "string",
					"Country_Region_Code": "string",
					"County": "string",
					"Customer_No": "string",
					"E_Mail": "string",
					"Fax_No": "string",
					"GLN": "string",
					"Home_Page": "string",
					"Key": "string",
					"Last_Date_Modified": "date",
					"Location_Code": "string",
					"Name": "string",
					"Phone_No": "string",
					"Post_Code": "string",
					"Service_Zone_Code": "string",
					"Shipment_Method_Code": "string",
					"Shipping_Agent_Code": "string",
					"Shipping_Agent_Service_Code": "string",
					"ShowMap": "string"
				},
				"OK365_Shipping_Address_Card_Fields": "string",
				"OK365_Shipping_Address_Card_Filter": {
					"Criteria": "string",
					"Field": "OK365_Shipping_Address_Card_Fields"
				},
				"OK365_Shipping_Address_Card_List": {
					"OK365_Shipping_Address_Card": "OK365_Shipping_Address_Card"
				},
				"Read": {
					"Code": "string",
					"Customer_No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Shipping_Address_Card": "OK365_Shipping_Address_Card"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Shipping_Address_Card_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Shipping_Address_Card_List"
				},
				"Read_Result": {
					"OK365_Shipping_Address_Card": "OK365_Shipping_Address_Card"
				},
				"Update": {
					"OK365_Shipping_Address_Card": "OK365_Shipping_Address_Card"
				},
				"UpdateMultiple": {
					"OK365_Shipping_Address_Card_List": "OK365_Shipping_Address_Card_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Shipping_Address_Card_List": "OK365_Shipping_Address_Card_List"
				},
				"Update_Result": {
					"OK365_Shipping_Address_Card": "OK365_Shipping_Address_Card"
				}
			}
		},
		"OK365_Shipping_Address_List": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Shipping_Address_List": {
					"Address": "string",
					"Address_2": "string",
					"City": "string",
					"Code": "string",
					"Contact": "string",
					"Country_Region_Code": "string",
					"Customer_No_OkFldSls": "string",
					"Fax_No": "string",
					"GLN": "string",
					"Key": "string",
					"Location_Code": "string",
					"Name": "string",
					"Phone_No": "string",
					"Post_Code": "string"
				},
				"OK365_Shipping_Address_List_Fields": "string",
				"OK365_Shipping_Address_List_Filter": {
					"Criteria": "string",
					"Field": "OK365_Shipping_Address_List_Fields"
				},
				"OK365_Shipping_Address_List_List": {
					"OK365_Shipping_Address_List": "OK365_Shipping_Address_List"
				},
				"Read": {
					"Code": "string",
					"Customer_No_OkFldSls": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Shipping_Address_List": "OK365_Shipping_Address_List"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Shipping_Address_List_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Shipping_Address_List_List"
				},
				"Read_Result": {
					"OK365_Shipping_Address_List": "OK365_Shipping_Address_List"
				}
			}
		},
		"OK365_Transfer_Order_List": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Transfer_Order_List": {
					"Assigned_User_ID": "string",
					"Direct_Transfer": "boolean",
					"In_Transit_Code": "string",
					"Key": "string",
					"No": "string",
					"Posting_Date_OkFldSls": "date",
					"Receipt_Date": "date",
					"Shipment_Date": "date",
					"Shipment_Method_Code": "string",
					"Shipping_Advice": "Shipping_Advice",
					"Shipping_Agent_Code": "string",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"Status": "Status",
					"Transfer_from_Code": "string",
					"Transfer_to_Code": "string"
				},
				"OK365_Transfer_Order_List_Fields": "string",
				"OK365_Transfer_Order_List_Filter": {
					"Criteria": "string",
					"Field": "OK365_Transfer_Order_List_Fields"
				},
				"OK365_Transfer_Order_List_List": {
					"OK365_Transfer_Order_List": "OK365_Transfer_Order_List"
				},
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Transfer_Order_List": "OK365_Transfer_Order_List"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Transfer_Order_List_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Transfer_Order_List_List"
				},
				"Read_Result": {
					"OK365_Transfer_Order_List": "OK365_Transfer_Order_List"
				},
				"Shipping_Advice": "string",
				"Status": "string"
			}
		},
		"OK365_Transfer_Orders": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"Delete_TransferLines": {
					"i": "Delete_TransferLines",
					"o": "Delete_TransferLines_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"OK365_Transfer_Orders": "OK365_Transfer_Orders"
				},
				"CreateMultiple": {
					"OK365_Transfer_Orders_List": "OK365_Transfer_Orders_List"
				},
				"CreateMultiple_Result": {
					"OK365_Transfer_Orders_List": "OK365_Transfer_Orders_List"
				},
				"Create_Result": {
					"OK365_Transfer_Orders": "OK365_Transfer_Orders"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Delete_TransferLines": {
					"TransferLines_Key": "string"
				},
				"Delete_TransferLines_Result": {
					"Delete_TransferLines_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Transfer_Line_OkFldSls": {
					"Demand": "decimal",
					"Description": "string",
					"Document_No": "string",
					"Item_No": "string",
					"Key": "string",
					"Line_No": "int",
					"Qty_to_Receive": "decimal",
					"Qty_to_Ship": "decimal",
					"Quantity": "decimal",
					"Quantity_Received": "decimal",
					"Quantity_Shipped": "decimal",
					"Receipt_Date": "date",
					"Reserved_Quantity_Inbnd": "decimal",
					"Reserved_Quantity_Outbnd": "decimal",
					"Reserved_Quantity_Shipped": "decimal",
					"Shipment_Date": "date",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"Stock_Balance": "decimal",
					"Unit_of_Measure": "string",
					"Unit_of_Measure_Code": "string"
				},
				"OK365_Transfer_Line_OkFldSls_List": {
					"OK365_Transfer_Line_OkFldSls": "OK365_Transfer_Line_OkFldSls"
				},
				"OK365_Transfer_Orders": {
					"Area": "string",
					"Assigned_User_ID": "string",
					"Entry_Exit_Point": "string",
					"In_Transit_Code": "string",
					"Inbound_Whse_Handling_Time": "string",
					"Key": "string",
					"No": "string",
					"Outbound_Whse_Handling_Time": "string",
					"Posting_Date": "date",
					"Receipt_Date": "date",
					"Shipment_Date": "date",
					"Shipment_Method_Code": "string",
					"Shipping_Advice": "Shipping_Advice",
					"Shipping_Agent_Code": "string",
					"Shipping_Agent_Service_Code": "string",
					"Shipping_Time": "string",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"Status": "Status",
					"Transaction_Specification": "string",
					"Transaction_Type": "string",
					"TransferLines": "OK365_Transfer_Line_OkFldSls_List",
					"Transfer_from_Address": "string",
					"Transfer_from_Address_2": "string",
					"Transfer_from_City": "string",
					"Transfer_from_Code": "string",
					"Transfer_from_Contact": "string",
					"Transfer_from_County": "string",
					"Transfer_from_Name": "string",
					"Transfer_from_Name_2": "string",
					"Transfer_from_Post_Code": "string",
					"Transfer_to_Address": "string",
					"Transfer_to_Address_2": "string",
					"Transfer_to_City": "string",
					"Transfer_to_Code": "string",
					"Transfer_to_Contact": "string",
					"Transfer_to_County": "string",
					"Transfer_to_Name": "string",
					"Transfer_to_Name_2": "string",
					"Transfer_to_Post_Code": "string",
					"Transport_Method": "string",
					"Trsf_from_Country_Region_Code": "string",
					"Trsf_to_Country_Region_Code": "string"
				},
				"OK365_Transfer_Orders_Fields": "string",
				"OK365_Transfer_Orders_Filter": {
					"Criteria": "string",
					"Field": "OK365_Transfer_Orders_Fields"
				},
				"OK365_Transfer_Orders_List": {
					"OK365_Transfer_Orders": "OK365_Transfer_Orders"
				},
				"Read": {
					"No": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Transfer_Orders": "OK365_Transfer_Orders"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Transfer_Orders_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Transfer_Orders_List"
				},
				"Read_Result": {
					"OK365_Transfer_Orders": "OK365_Transfer_Orders"
				},
				"Shipping_Advice": "string",
				"Status": "string",
				"Update": {
					"OK365_Transfer_Orders": "OK365_Transfer_Orders"
				},
				"UpdateMultiple": {
					"OK365_Transfer_Orders_List": "OK365_Transfer_Orders_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Transfer_Orders_List": "OK365_Transfer_Orders_List"
				},
				"Update_Result": {
					"OK365_Transfer_Orders": "OK365_Transfer_Orders"
				}
			}
		},
		"OK365_Units_of_Measure": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"OK365_Units_of_Measure": "OK365_Units_of_Measure"
				},
				"CreateMultiple": {
					"OK365_Units_of_Measure_List": "OK365_Units_of_Measure_List"
				},
				"CreateMultiple_Result": {
					"OK365_Units_of_Measure_List": "OK365_Units_of_Measure_List"
				},
				"Create_Result": {
					"OK365_Units_of_Measure": "OK365_Units_of_Measure"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"OK365_Units_of_Measure": {
					"Code": "string",
					"Description": "string",
					"International_Standard_Code": "string",
					"Key": "string"
				},
				"OK365_Units_of_Measure_Fields": "string",
				"OK365_Units_of_Measure_Filter": {
					"Criteria": "string",
					"Field": "OK365_Units_of_Measure_Fields"
				},
				"OK365_Units_of_Measure_List": {
					"OK365_Units_of_Measure": "OK365_Units_of_Measure"
				},
				"Read": {
					"Code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"OK365_Units_of_Measure": "OK365_Units_of_Measure"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "OK365_Units_of_Measure_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "OK365_Units_of_Measure_List"
				},
				"Read_Result": {
					"OK365_Units_of_Measure": "OK365_Units_of_Measure"
				},
				"Update": {
					"OK365_Units_of_Measure": "OK365_Units_of_Measure"
				},
				"UpdateMultiple": {
					"OK365_Units_of_Measure_List": "OK365_Units_of_Measure_List"
				},
				"UpdateMultiple_Result": {
					"OK365_Units_of_Measure_List": "OK365_Units_of_Measure_List"
				},
				"Update_Result": {
					"OK365_Units_of_Measure": "OK365_Units_of_Measure"
				}
			}
		},
		"Power_BI_Aged_Acc_Payable": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Period_Type": "string",
				"Power_BI_Aged_Acc_Payable": {
					"Date": "string",
					"Date_Sorting": "int",
					"ID": "int",
					"Key": "string",
					"Period_Type": "Period_Type",
					"Period_Type_Sorting": "int",
					"Value": "decimal"
				},
				"Power_BI_Aged_Acc_Payable_Fields": "string",
				"Power_BI_Aged_Acc_Payable_Filter": {
					"Criteria": "string",
					"Field": "Power_BI_Aged_Acc_Payable_Fields"
				},
				"Power_BI_Aged_Acc_Payable_List": {
					"Power_BI_Aged_Acc_Payable": "Power_BI_Aged_Acc_Payable"
				},
				"Read": {
					"ID": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"Power_BI_Aged_Acc_Payable": "Power_BI_Aged_Acc_Payable"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "Power_BI_Aged_Acc_Payable_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "Power_BI_Aged_Acc_Payable_List"
				},
				"Read_Result": {
					"Power_BI_Aged_Acc_Payable": "Power_BI_Aged_Acc_Payable"
				}
			}
		},
		"Power_BI_Aged_Acc_Receivable": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Period_Type": "string",
				"Power_BI_Aged_Acc_Receivable": {
					"Date": "string",
					"Date_Sorting": "int",
					"ID": "int",
					"Key": "string",
					"Measure_Name": "string",
					"Period_Type": "Period_Type",
					"Period_Type_Sorting": "int",
					"Value": "decimal"
				},
				"Power_BI_Aged_Acc_Receivable_Fields": "string",
				"Power_BI_Aged_Acc_Receivable_Filter": {
					"Criteria": "string",
					"Field": "Power_BI_Aged_Acc_Receivable_Fields"
				},
				"Power_BI_Aged_Acc_Receivable_List": {
					"Power_BI_Aged_Acc_Receivable": "Power_BI_Aged_Acc_Receivable"
				},
				"Read": {
					"ID": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"Power_BI_Aged_Acc_Receivable": "Power_BI_Aged_Acc_Receivable"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "Power_BI_Aged_Acc_Receivable_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "Power_BI_Aged_Acc_Receivable_List"
				},
				"Read_Result": {
					"Power_BI_Aged_Acc_Receivable": "Power_BI_Aged_Acc_Receivable"
				}
			}
		},
		"Power_BI_Aged_Inventory_Chart": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Period_Type": "string",
				"Power_BI_Aged_Inventory_Chart": {
					"Date": "string",
					"ID": "int",
					"Key": "string",
					"Period_Type": "Period_Type",
					"Period_Type_Sorting": "int",
					"Value": "decimal"
				},
				"Power_BI_Aged_Inventory_Chart_Fields": "string",
				"Power_BI_Aged_Inventory_Chart_Filter": {
					"Criteria": "string",
					"Field": "Power_BI_Aged_Inventory_Chart_Fields"
				},
				"Power_BI_Aged_Inventory_Chart_List": {
					"Power_BI_Aged_Inventory_Chart": "Power_BI_Aged_Inventory_Chart"
				},
				"Read": {
					"ID": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"Power_BI_Aged_Inventory_Chart": "Power_BI_Aged_Inventory_Chart"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "Power_BI_Aged_Inventory_Chart_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "Power_BI_Aged_Inventory_Chart_List"
				},
				"Read_Result": {
					"Power_BI_Aged_Inventory_Chart": "Power_BI_Aged_Inventory_Chart"
				}
			}
		},
		"Power_BI_Job_Act_v_Budg_Cost": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Power_BI_Job_Act_v_Budg_Cost": {
					"Key": "string",
					"Measure_Name": "string",
					"Measure_No": "string",
					"Value": "decimal"
				},
				"Power_BI_Job_Act_v_Budg_Cost_Fields": "string",
				"Power_BI_Job_Act_v_Budg_Cost_Filter": {
					"Criteria": "string",
					"Field": "Power_BI_Job_Act_v_Budg_Cost_Fields"
				},
				"Power_BI_Job_Act_v_Budg_Cost_List": {
					"Power_BI_Job_Act_v_Budg_Cost": "Power_BI_Job_Act_v_Budg_Cost"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"Power_BI_Job_Act_v_Budg_Cost": "Power_BI_Job_Act_v_Budg_Cost"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "Power_BI_Job_Act_v_Budg_Cost_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "Power_BI_Job_Act_v_Budg_Cost_List"
				}
			}
		},
		"Power_BI_Job_Act_v_Budg_Price": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Power_BI_Job_Act_v_Budg_Price": {
					"Key": "string",
					"Measure_Name": "string",
					"Measure_No": "string",
					"Value": "decimal"
				},
				"Power_BI_Job_Act_v_Budg_Price_Fields": "string",
				"Power_BI_Job_Act_v_Budg_Price_Filter": {
					"Criteria": "string",
					"Field": "Power_BI_Job_Act_v_Budg_Price_Fields"
				},
				"Power_BI_Job_Act_v_Budg_Price_List": {
					"Power_BI_Job_Act_v_Budg_Price": "Power_BI_Job_Act_v_Budg_Price"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"Power_BI_Job_Act_v_Budg_Price": "Power_BI_Job_Act_v_Budg_Price"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "Power_BI_Job_Act_v_Budg_Price_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "Power_BI_Job_Act_v_Budg_Price_List"
				}
			}
		},
		"Power_BI_Job_Profitability": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Power_BI_Job_Profitability": {
					"Key": "string",
					"Measure_Name": "string",
					"Measure_No": "string",
					"Value": "decimal"
				},
				"Power_BI_Job_Profitability_Fields": "string",
				"Power_BI_Job_Profitability_Filter": {
					"Criteria": "string",
					"Field": "Power_BI_Job_Profitability_Fields"
				},
				"Power_BI_Job_Profitability_List": {
					"Power_BI_Job_Profitability": "Power_BI_Job_Profitability"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"Power_BI_Job_Profitability": "Power_BI_Job_Profitability"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "Power_BI_Job_Profitability_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "Power_BI_Job_Profitability_List"
				}
			}
		},
		"Power_BI_Report_Labels": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Power_BI_Report_Labels": {
					"Key": "string",
					"Label_ID": "string",
					"Text_Value": "string"
				},
				"Power_BI_Report_Labels_Fields": "string",
				"Power_BI_Report_Labels_Filter": {
					"Criteria": "string",
					"Field": "Power_BI_Report_Labels_Fields"
				},
				"Power_BI_Report_Labels_List": {
					"Power_BI_Report_Labels": "Power_BI_Report_Labels"
				},
				"Read": {
					"Label_ID": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"Power_BI_Report_Labels": "Power_BI_Report_Labels"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "Power_BI_Report_Labels_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "Power_BI_Report_Labels_List"
				},
				"Read_Result": {
					"Power_BI_Report_Labels": "Power_BI_Report_Labels"
				}
			}
		},
		"Power_BI_Sales_Pipeline": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Power_BI_Sales_Pipeline": {
					"ID": "int",
					"Key": "string",
					"Measure_Name": "string",
					"Measure_No": "string",
					"Row_No": "string",
					"Value": "decimal"
				},
				"Power_BI_Sales_Pipeline_Fields": "string",
				"Power_BI_Sales_Pipeline_Filter": {
					"Criteria": "string",
					"Field": "Power_BI_Sales_Pipeline_Fields"
				},
				"Power_BI_Sales_Pipeline_List": {
					"Power_BI_Sales_Pipeline": "Power_BI_Sales_Pipeline"
				},
				"Read": {
					"ID": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"Power_BI_Sales_Pipeline": "Power_BI_Sales_Pipeline"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "Power_BI_Sales_Pipeline_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "Power_BI_Sales_Pipeline_List"
				},
				"Read_Result": {
					"Power_BI_Sales_Pipeline": "Power_BI_Sales_Pipeline"
				}
			}
		},
		"Power_BI_Top_5_Opportunities": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Power_BI_Top_5_Opportunities": {
					"ID": "int",
					"Key": "string",
					"Measure_Name": "string",
					"Measure_No": "string",
					"Value": "decimal"
				},
				"Power_BI_Top_5_Opportunities_Fields": "string",
				"Power_BI_Top_5_Opportunities_Filter": {
					"Criteria": "string",
					"Field": "Power_BI_Top_5_Opportunities_Fields"
				},
				"Power_BI_Top_5_Opportunities_List": {
					"Power_BI_Top_5_Opportunities": "Power_BI_Top_5_Opportunities"
				},
				"Read": {
					"ID": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"Power_BI_Top_5_Opportunities": "Power_BI_Top_5_Opportunities"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "Power_BI_Top_5_Opportunities_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "Power_BI_Top_5_Opportunities_List"
				},
				"Read_Result": {
					"Power_BI_Top_5_Opportunities": "Power_BI_Top_5_Opportunities"
				}
			}
		},
		"Power_BI_WorkDate_Calc": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"Power_BI_WorkDate_Calc": "Power_BI_WorkDate_Calc"
				},
				"CreateMultiple": {
					"Power_BI_WorkDate_Calc_List": "Power_BI_WorkDate_Calc_List"
				},
				"CreateMultiple_Result": {
					"Power_BI_WorkDate_Calc_List": "Power_BI_WorkDate_Calc_List"
				},
				"Create_Result": {
					"Power_BI_WorkDate_Calc": "Power_BI_WorkDate_Calc"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Power_BI_WorkDate_Calc": {
					"Key": "string",
					"WorkDateNAV": "date"
				},
				"Power_BI_WorkDate_Calc_Fields": "string",
				"Power_BI_WorkDate_Calc_Filter": {
					"Criteria": "string",
					"Field": "Power_BI_WorkDate_Calc_Fields"
				},
				"Power_BI_WorkDate_Calc_List": {
					"Power_BI_WorkDate_Calc": "Power_BI_WorkDate_Calc"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"Power_BI_WorkDate_Calc": "Power_BI_WorkDate_Calc"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "Power_BI_WorkDate_Calc_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "Power_BI_WorkDate_Calc_List"
				},
				"Update": {
					"Power_BI_WorkDate_Calc": "Power_BI_WorkDate_Calc"
				},
				"UpdateMultiple": {
					"Power_BI_WorkDate_Calc_List": "Power_BI_WorkDate_Calc_List"
				},
				"UpdateMultiple_Result": {
					"Power_BI_WorkDate_Calc_List": "Power_BI_WorkDate_Calc_List"
				},
				"Update_Result": {
					"Power_BI_WorkDate_Calc": "Power_BI_WorkDate_Calc"
				}
			}
		},
		"Purchase_Order_Line": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"Purchase_Order_Line": "Purchase_Order_Line"
				},
				"CreateMultiple": {
					"Purchase_Order_Line_List": "Purchase_Order_Line_List"
				},
				"CreateMultiple_Result": {
					"Purchase_Order_Line_List": "Purchase_Order_Line_List"
				},
				"Create_Result": {
					"Purchase_Order_Line": "Purchase_Order_Line"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IC_Partner_Ref_Type": "string",
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Job_Line_Type": "string",
				"Planning_Flexibility": "string",
				"Purchase_Order_Line": {
					"Allow_Invoice_Disc": "boolean",
					"Allow_Item_Charge_Assignment": "boolean",
					"AmountBeforeDiscount": "decimal",
					"Appl_to_Item_Entry": "int",
					"Bin_Code": "string",
					"Blanket_Order_Line_No": "int",
					"Blanket_Order_No": "string",
					"Cross_Reference_No": "string",
					"Deferral_Code": "string",
					"Description": "string",
					"Direct_Unit_Cost": "decimal",
					"Document_No": "string",
					"Drop_Shipment": "boolean",
					"Expected_Receipt_Date": "date",
					"FilteredTypeField": "string",
					"Finished": "boolean",
					"IC_Partner_Code": "string",
					"IC_Partner_Ref_Type": "IC_Partner_Ref_Type",
					"IC_Partner_Reference": "string",
					"Inbound_Whse_Handling_Time": "string",
					"Indirect_Cost_Percent": "decimal",
					"Inv_Disc_Amount_to_Invoice": "decimal",
					"Inv_Discount_Amount": "decimal",
					"Invoice_Disc_Pct": "decimal",
					"Invoice_Discount_Amount": "decimal",
					"Job_Line_Amount": "decimal",
					"Job_Line_Amount_LCY": "decimal",
					"Job_Line_Disc_Amount_LCY": "decimal",
					"Job_Line_Discount_Amount": "decimal",
					"Job_Line_Discount_Percent": "decimal",
					"Job_Line_Type": "Job_Line_Type",
					"Job_No": "string",
					"Job_Planning_Line_No": "int",
					"Job_Remaining_Qty": "decimal",
					"Job_Task_No": "string",
					"Job_Total_Price": "decimal",
					"Job_Total_Price_LCY": "decimal",
					"Job_Unit_Price": "decimal",
					"Job_Unit_Price_LCY": "decimal",
					"Key": "string",
					"Lead_Time_Calculation": "string",
					"Line_Amount": "decimal",
					"Line_Discount_Amount": "decimal",
					"Line_Discount_Percent": "decimal",
					"Line_No": "int",
					"Location_Code": "string",
					"No": "string",
					"Nonstock": "boolean",
					"Operation_No": "string",
					"Order_Date": "date",
					"Over_Receipt_Code": "string",
					"Over_Receipt_Quantity": "decimal",
					"Planned_Receipt_Date": "date",
					"Planning_Flexibility": "Planning_Flexibility",
					"Prepayment_Percent": "decimal",
					"Prepmt_Amt_Deducted": "decimal",
					"Prepmt_Amt_Inv": "decimal",
					"Prepmt_Amt_to_Deduct": "decimal",
					"Prepmt_Line_Amount": "decimal",
					"Prod_Order_Line_No": "int",
					"Prod_Order_No": "string",
					"Promised_Receipt_Date": "date",
					"Qty_Assigned": "decimal",
					"Qty_to_Assign": "decimal",
					"Qty_to_Invoice": "decimal",
					"Qty_to_Receive": "decimal",
					"Quantity": "decimal",
					"Quantity_Invoiced": "decimal",
					"Quantity_Received": "decimal",
					"Requested_Receipt_Date": "date",
					"Reserved_Quantity": "decimal",
					"Return_Reason_Code": "string",
					"ShortcutDimCode3": "string",
					"ShortcutDimCode4": "string",
					"ShortcutDimCode5": "string",
					"ShortcutDimCode6": "string",
					"ShortcutDimCode7": "string",
					"ShortcutDimCode8": "string",
					"Shortcut_Dimension_1_Code": "string",
					"Shortcut_Dimension_2_Code": "string",
					"Tax_Area_Code": "string",
					"Tax_Group_Code": "string",
					"Tax_Liable": "boolean",
					"Total_Amount_Excl_VAT": "decimal",
					"Total_Amount_Incl_VAT": "decimal",
					"Total_VAT_Amount": "decimal",
					"Type": "Type",
					"Unit_Cost_LCY": "decimal",
					"Unit_Price_LCY": "decimal",
					"Unit_of_Measure": "string",
					"Unit_of_Measure_Code": "string",
					"Use_Tax": "boolean",
					"VAT_Prod_Posting_Group": "string",
					"Variant_Code": "string",
					"Whse_Outstanding_Qty_Base": "decimal",
					"Work_Center_No": "string"
				},
				"Purchase_Order_Line_Fields": "string",
				"Purchase_Order_Line_Filter": {
					"Criteria": "string",
					"Field": "Purchase_Order_Line_Fields"
				},
				"Purchase_Order_Line_List": {
					"Purchase_Order_Line": "Purchase_Order_Line"
				},
				"Read": {
					"Document_No": "string",
					"Line_No": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"Purchase_Order_Line": "Purchase_Order_Line"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "Purchase_Order_Line_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "Purchase_Order_Line_List"
				},
				"Read_Result": {
					"Purchase_Order_Line": "Purchase_Order_Line"
				},
				"Type": "string",
				"Update": {
					"Purchase_Order_Line": "Purchase_Order_Line"
				},
				"UpdateMultiple": {
					"Purchase_Order_Line_List": "Purchase_Order_Line_List"
				},
				"UpdateMultiple_Result": {
					"Purchase_Order_Line_List": "Purchase_Order_Line_List"
				},
				"Update_Result": {
					"Purchase_Order_Line": "Purchase_Order_Line"
				}
			}
		},
		"UserTaskSetComplete": {
			"f": {
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Priority": "string",
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"UserTaskSetComplete": "UserTaskSetComplete"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "UserTaskSetComplete_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "UserTaskSetComplete_List"
				},
				"UserTaskSetComplete": {
					"Assigned_To_User_Name": "string",
					"Completed_By_User_Name": "string",
					"Completed_DateTime": "dateTime",
					"Created_By_User_Name": "string",
					"Created_DateTime": "dateTime",
					"Due_DateTime": "dateTime",
					"Key": "string",
					"Percent_Complete": "int",
					"Priority": "Priority",
					"Start_DateTime": "dateTime",
					"Title": "string",
					"User_Task_Group_Assigned_To": "string"
				},
				"UserTaskSetComplete_Fields": "string",
				"UserTaskSetComplete_Filter": {
					"Criteria": "string",
					"Field": "UserTaskSetComplete_Fields"
				},
				"UserTaskSetComplete_List": {
					"UserTaskSetComplete": "UserTaskSetComplete"
				}
			}
		},
		"WorkflowActionResponse": {
			"f": {
				"Approve": {
					"i": "Approve",
					"o": "Approve_Result"
				},
				"Reject": {
					"i": "Reject",
					"o": "Reject_Result"
				}
			},
			"sp": "Codeunit",
			"t": {
				"Approve": {
					"workflowStepInstanceId": "string"
				},
				"Approve_Result": {
					"": ""
				},
				"Reject": {
					"workflowStepInstanceId": "string"
				},
				"Reject_Result": {
					"": ""
				}
			}
		},
		"nativeInvoicingAttachments": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingAttachments": "nativeInvoicingAttachments"
				},
				"CreateMultiple": {
					"nativeInvoicingAttachments_List": "nativeInvoicingAttachments_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingAttachments_List": "nativeInvoicingAttachments_List"
				},
				"Create_Result": {
					"nativeInvoicingAttachments": "nativeInvoicingAttachments"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"id": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingAttachments": "nativeInvoicingAttachments"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingAttachments_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingAttachments_List"
				},
				"Read_Result": {
					"nativeInvoicingAttachments": "nativeInvoicingAttachments"
				},
				"Update": {
					"nativeInvoicingAttachments": "nativeInvoicingAttachments"
				},
				"UpdateMultiple": {
					"nativeInvoicingAttachments_List": "nativeInvoicingAttachments_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingAttachments_List": "nativeInvoicingAttachments_List"
				},
				"Update_Result": {
					"nativeInvoicingAttachments": "nativeInvoicingAttachments"
				},
				"nativeInvoicingAttachments": {
					"Key": "string",
					"base64Content": "string",
					"byteSize": "int",
					"documentId": "string",
					"fileName": "string",
					"id": "string",
					"lastModifiedDateTime": "dateTime"
				},
				"nativeInvoicingAttachments_Fields": "string",
				"nativeInvoicingAttachments_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingAttachments_Fields"
				},
				"nativeInvoicingAttachments_List": {
					"nativeInvoicingAttachments": "nativeInvoicingAttachments"
				}
			}
		},
		"nativeInvoicingContacts": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingContacts": "nativeInvoicingContacts"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingContacts_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingContacts_List"
				},
				"Read_Result": {
					"nativeInvoicingContacts": "nativeInvoicingContacts"
				},
				"nativeInvoicingContacts": {
					"Key": "string",
					"customerId": "string",
					"displayName": "string",
					"email": "string",
					"number": "string",
					"phoneNumber": "string",
					"xrmId": "string"
				},
				"nativeInvoicingContacts_Fields": "string",
				"nativeInvoicingContacts_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingContacts_Fields"
				},
				"nativeInvoicingContacts_List": {
					"nativeInvoicingContacts": "nativeInvoicingContacts"
				}
			}
		},
		"nativeInvoicingCountryRegion": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingCountryRegion": "nativeInvoicingCountryRegion"
				},
				"CreateMultiple": {
					"nativeInvoicingCountryRegion_List": "nativeInvoicingCountryRegion_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingCountryRegion_List": "nativeInvoicingCountryRegion_List"
				},
				"Create_Result": {
					"nativeInvoicingCountryRegion": "nativeInvoicingCountryRegion"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingCountryRegion": "nativeInvoicingCountryRegion"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingCountryRegion_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingCountryRegion_List"
				},
				"Read_Result": {
					"nativeInvoicingCountryRegion": "nativeInvoicingCountryRegion"
				},
				"Update": {
					"nativeInvoicingCountryRegion": "nativeInvoicingCountryRegion"
				},
				"UpdateMultiple": {
					"nativeInvoicingCountryRegion_List": "nativeInvoicingCountryRegion_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingCountryRegion_List": "nativeInvoicingCountryRegion_List"
				},
				"Update_Result": {
					"nativeInvoicingCountryRegion": "nativeInvoicingCountryRegion"
				},
				"nativeInvoicingCountryRegion": {
					"Key": "string",
					"code": "string",
					"displayName": "string",
					"id": "string",
					"lastModifiedDateTime": "dateTime"
				},
				"nativeInvoicingCountryRegion_Fields": "string",
				"nativeInvoicingCountryRegion_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingCountryRegion_Fields"
				},
				"nativeInvoicingCountryRegion_List": {
					"nativeInvoicingCountryRegion": "nativeInvoicingCountryRegion"
				}
			}
		},
		"nativeInvoicingCustomers": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingCustomers": "nativeInvoicingCustomers"
				},
				"CreateMultiple": {
					"nativeInvoicingCustomers_List": "nativeInvoicingCustomers_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingCustomers_List": "nativeInvoicingCustomers_List"
				},
				"Create_Result": {
					"nativeInvoicingCustomers": "nativeInvoicingCustomers"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Native_Coupons": {
					"Key": "string",
					"amount": "string",
					"claimId": "string",
					"code": "string",
					"createdDateTime": "dateTime",
					"customerId": "string",
					"discountType": "discountType",
					"discountValue": "decimal",
					"expiration": "date",
					"graphContactId": "string",
					"isApplied": "boolean",
					"isValid": "boolean",
					"offer": "string",
					"status": "string",
					"terms": "string",
					"usage": "usage"
				},
				"Native_Coupons_List": {
					"Native_Coupons": "Native_Coupons"
				},
				"Read": {
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingCustomers": "nativeInvoicingCustomers"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingCustomers_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingCustomers_List"
				},
				"Read_Result": {
					"nativeInvoicingCustomers": "nativeInvoicingCustomers"
				},
				"Update": {
					"nativeInvoicingCustomers": "nativeInvoicingCustomers"
				},
				"UpdateMultiple": {
					"nativeInvoicingCustomers_List": "nativeInvoicingCustomers_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingCustomers_List": "nativeInvoicingCustomers_List"
				},
				"Update_Result": {
					"nativeInvoicingCustomers": "nativeInvoicingCustomers"
				},
				"discountType": "string",
				"nativeInvoicingCustomers": {
					"Coupons": "Native_Coupons_List",
					"Key": "string",
					"address": "string",
					"balance": "decimal",
					"contactId": "string",
					"displayName": "string",
					"email": "string",
					"graphContactId": "string",
					"id": "string",
					"isBlocked": "boolean",
					"lastModifiedDateTime": "dateTime",
					"number": "string",
					"overdueAmount": "decimal",
					"paymentMethodId": "string",
					"paymentTermsId": "string",
					"phoneNumber": "string",
					"pricesIncludeTax": "boolean",
					"shipmentMethodId": "string",
					"taxAreaDisplayName": "string",
					"taxAreaId": "string",
					"taxLiable": "boolean",
					"taxRegistrationNumber": "string",
					"totalSalesExcludingTax": "decimal",
					"type": "type",
					"website": "string"
				},
				"nativeInvoicingCustomers_Fields": "string",
				"nativeInvoicingCustomers_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingCustomers_Fields"
				},
				"nativeInvoicingCustomers_List": {
					"nativeInvoicingCustomers": "nativeInvoicingCustomers"
				},
				"type": "string",
				"usage": "string"
			}
		},
		"nativeInvoicingEmailPreview": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingEmailPreview": "nativeInvoicingEmailPreview"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingEmailPreview_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingEmailPreview_List"
				},
				"Update": {
					"nativeInvoicingEmailPreview": "nativeInvoicingEmailPreview"
				},
				"UpdateMultiple": {
					"nativeInvoicingEmailPreview_List": "nativeInvoicingEmailPreview_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingEmailPreview_List": "nativeInvoicingEmailPreview_List"
				},
				"Update_Result": {
					"nativeInvoicingEmailPreview": "nativeInvoicingEmailPreview"
				},
				"nativeInvoicingEmailPreview": {
					"Key": "string",
					"bodyText": "string",
					"documentId": "string",
					"email": "string",
					"subject": "string"
				},
				"nativeInvoicingEmailPreview_Fields": "string",
				"nativeInvoicingEmailPreview_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingEmailPreview_Fields"
				},
				"nativeInvoicingEmailPreview_List": {
					"nativeInvoicingEmailPreview": "nativeInvoicingEmailPreview"
				}
			}
		},
		"nativeInvoicingEmailSetting": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingEmailSetting": "nativeInvoicingEmailSetting"
				},
				"CreateMultiple": {
					"nativeInvoicingEmailSetting_List": "nativeInvoicingEmailSetting_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingEmailSetting_List": "nativeInvoicingEmailSetting_List"
				},
				"Create_Result": {
					"nativeInvoicingEmailSetting": "nativeInvoicingEmailSetting"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"code": "string",
					"recipientType": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingEmailSetting": "nativeInvoicingEmailSetting"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingEmailSetting_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingEmailSetting_List"
				},
				"Read_Result": {
					"nativeInvoicingEmailSetting": "nativeInvoicingEmailSetting"
				},
				"Update": {
					"nativeInvoicingEmailSetting": "nativeInvoicingEmailSetting"
				},
				"UpdateMultiple": {
					"nativeInvoicingEmailSetting_List": "nativeInvoicingEmailSetting_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingEmailSetting_List": "nativeInvoicingEmailSetting_List"
				},
				"Update_Result": {
					"nativeInvoicingEmailSetting": "nativeInvoicingEmailSetting"
				},
				"nativeInvoicingEmailSetting": {
					"Key": "string",
					"code": "string",
					"eMail": "string",
					"recipientType": "recipientType"
				},
				"nativeInvoicingEmailSetting_Fields": "string",
				"nativeInvoicingEmailSetting_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingEmailSetting_Fields"
				},
				"nativeInvoicingEmailSetting_List": {
					"nativeInvoicingEmailSetting": "nativeInvoicingEmailSetting"
				},
				"recipientType": "string"
			}
		},
		"nativeInvoicingExportInvoices": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingExportInvoices": "nativeInvoicingExportInvoices"
				},
				"CreateMultiple": {
					"nativeInvoicingExportInvoices_List": "nativeInvoicingExportInvoices_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingExportInvoices_List": "nativeInvoicingExportInvoices_List"
				},
				"Create_Result": {
					"nativeInvoicingExportInvoices": "nativeInvoicingExportInvoices"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingExportInvoices": "nativeInvoicingExportInvoices"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingExportInvoices_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingExportInvoices_List"
				},
				"nativeInvoicingExportInvoices": {
					"Key": "string",
					"email": "string",
					"endDate": "date",
					"startDate": "date"
				},
				"nativeInvoicingExportInvoices_Fields": "string",
				"nativeInvoicingExportInvoices_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingExportInvoices_Fields"
				},
				"nativeInvoicingExportInvoices_List": {
					"nativeInvoicingExportInvoices": "nativeInvoicingExportInvoices"
				}
			}
		},
		"nativeInvoicingGeneralSettings": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"primaryKey": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingGeneralSettings": "nativeInvoicingGeneralSettings"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingGeneralSettings_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingGeneralSettings_List"
				},
				"Read_Result": {
					"nativeInvoicingGeneralSettings": "nativeInvoicingGeneralSettings"
				},
				"Update": {
					"nativeInvoicingGeneralSettings": "nativeInvoicingGeneralSettings"
				},
				"UpdateMultiple": {
					"nativeInvoicingGeneralSettings_List": "nativeInvoicingGeneralSettings_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingGeneralSettings_List": "nativeInvoicingGeneralSettings_List"
				},
				"Update_Result": {
					"nativeInvoicingGeneralSettings": "nativeInvoicingGeneralSettings"
				},
				"nativeInvoicingGeneralSettings": {
					"Key": "string",
					"amountRoundingPrecision": "decimal",
					"countryRegionCode": "string",
					"currencySymbol": "string",
					"defaultPaymentMethodDisplayName": "string",
					"defaultPaymentMethodId": "string",
					"defaultPaymentTermsDisplayName": "string",
					"defaultPaymentTermsId": "string",
					"defaultTaxDisplayName": "string",
					"defaultTaxId": "string",
					"draftInvoiceFileName": "string",
					"enableSyncCoupons": "boolean",
					"enableSynchronization": "boolean",
					"languageCode": "string",
					"languageDisplayName": "string",
					"languageId": "int",
					"nonTaxableGroupId": "string",
					"paypalEmailAddress": "string",
					"postedInvoiceFileName": "string",
					"primaryKey": "string",
					"quantityRoundingPrecision": "decimal",
					"quoteFileName": "string",
					"taxRoundingPrecision": "decimal",
					"taxableGroupId": "string",
					"unitAmountRoundingPrecision": "decimal",
					"updateVersion": "string"
				},
				"nativeInvoicingGeneralSettings_Fields": "string",
				"nativeInvoicingGeneralSettings_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingGeneralSettings_Fields"
				},
				"nativeInvoicingGeneralSettings_List": {
					"nativeInvoicingGeneralSettings": "nativeInvoicingGeneralSettings"
				}
			}
		},
		"nativeInvoicingItems": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingItems": "nativeInvoicingItems"
				},
				"CreateMultiple": {
					"nativeInvoicingItems_List": "nativeInvoicingItems_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingItems_List": "nativeInvoicingItems_List"
				},
				"Create_Result": {
					"nativeInvoicingItems": "nativeInvoicingItems"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingItems": "nativeInvoicingItems"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingItems_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingItems_List"
				},
				"Read_Result": {
					"nativeInvoicingItems": "nativeInvoicingItems"
				},
				"Update": {
					"nativeInvoicingItems": "nativeInvoicingItems"
				},
				"UpdateMultiple": {
					"nativeInvoicingItems_List": "nativeInvoicingItems_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingItems_List": "nativeInvoicingItems_List"
				},
				"Update_Result": {
					"nativeInvoicingItems": "nativeInvoicingItems"
				},
				"nativeInvoicingItems": {
					"Key": "string",
					"baseUnitOfMeasureDescription": "string",
					"baseUnitOfMeasureId": "string",
					"baseUnitOfMeasureIntStdCode": "string",
					"blocked": "boolean",
					"displayName": "string",
					"gtin": "string",
					"id": "string",
					"inventory": "decimal",
					"lastModifiedDateTime": "dateTime",
					"number": "string",
					"priceIncludesTax": "boolean",
					"taxGroupCode": "string",
					"taxGroupId": "string",
					"taxable": "boolean",
					"type": "type",
					"unitCost": "decimal",
					"unitPrice": "decimal"
				},
				"nativeInvoicingItems_Fields": "string",
				"nativeInvoicingItems_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingItems_Fields"
				},
				"nativeInvoicingItems_List": {
					"nativeInvoicingItems": "nativeInvoicingItems"
				},
				"type": "string"
			}
		},
		"nativeInvoicingKPIs": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"primaryKey": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingKPIs": "nativeInvoicingKPIs"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingKPIs_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingKPIs_List"
				},
				"Read_Result": {
					"nativeInvoicingKPIs": "nativeInvoicingKPIs"
				},
				"nativeInvoicingKPIs": {
					"Key": "string",
					"invoicedCurrentMonth": "decimal",
					"invoicedYearToDate": "decimal",
					"numberOfDraftInvoices": "int",
					"numberOfInvoicesYearToDate": "int",
					"numberOfQuotes": "int",
					"primaryKey": "string",
					"requestedDateTime": "dateTime",
					"salesInvoicesOutsdanding": "decimal",
					"salesInvoicesOverdue": "decimal"
				},
				"nativeInvoicingKPIs_Fields": "string",
				"nativeInvoicingKPIs_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingKPIs_Fields"
				},
				"nativeInvoicingKPIs_List": {
					"nativeInvoicingKPIs": "nativeInvoicingKPIs"
				}
			}
		},
		"nativeInvoicingLanguages": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"languageId": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingLanguages": "nativeInvoicingLanguages"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingLanguages_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingLanguages_List"
				},
				"Read_Result": {
					"nativeInvoicingLanguages": "nativeInvoicingLanguages"
				},
				"nativeInvoicingLanguages": {
					"Key": "string",
					"default": "boolean",
					"displayName": "string",
					"languageCode": "string",
					"languageId": "int"
				},
				"nativeInvoicingLanguages_Fields": "string",
				"nativeInvoicingLanguages_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingLanguages_Fields"
				},
				"nativeInvoicingLanguages_List": {
					"nativeInvoicingLanguages": "nativeInvoicingLanguages"
				}
			}
		},
		"nativeInvoicingPDFs": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingPDFs": "nativeInvoicingPDFs"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingPDFs_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingPDFs_List"
				},
				"nativeInvoicingPDFs": {
					"Key": "string",
					"documentId": "string",
					"fileName": "string"
				},
				"nativeInvoicingPDFs_Fields": "string",
				"nativeInvoicingPDFs_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingPDFs_Fields"
				},
				"nativeInvoicingPDFs_List": {
					"nativeInvoicingPDFs": "nativeInvoicingPDFs"
				}
			}
		},
		"nativeInvoicingPaymentMethods": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingPaymentMethods": "nativeInvoicingPaymentMethods"
				},
				"CreateMultiple": {
					"nativeInvoicingPaymentMethods_List": "nativeInvoicingPaymentMethods_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingPaymentMethods_List": "nativeInvoicingPaymentMethods_List"
				},
				"Create_Result": {
					"nativeInvoicingPaymentMethods": "nativeInvoicingPaymentMethods"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingPaymentMethods": "nativeInvoicingPaymentMethods"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingPaymentMethods_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingPaymentMethods_List"
				},
				"Read_Result": {
					"nativeInvoicingPaymentMethods": "nativeInvoicingPaymentMethods"
				},
				"Update": {
					"nativeInvoicingPaymentMethods": "nativeInvoicingPaymentMethods"
				},
				"UpdateMultiple": {
					"nativeInvoicingPaymentMethods_List": "nativeInvoicingPaymentMethods_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingPaymentMethods_List": "nativeInvoicingPaymentMethods_List"
				},
				"Update_Result": {
					"nativeInvoicingPaymentMethods": "nativeInvoicingPaymentMethods"
				},
				"nativeInvoicingPaymentMethods": {
					"Key": "string",
					"code": "string",
					"default": "boolean",
					"displayName": "string",
					"id": "string",
					"lastModifiedDateTime": "dateTime"
				},
				"nativeInvoicingPaymentMethods_Fields": "string",
				"nativeInvoicingPaymentMethods_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingPaymentMethods_Fields"
				},
				"nativeInvoicingPaymentMethods_List": {
					"nativeInvoicingPaymentMethods": "nativeInvoicingPaymentMethods"
				}
			}
		},
		"nativeInvoicingPaymentTerms": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingPaymentTerms": "nativeInvoicingPaymentTerms"
				},
				"CreateMultiple": {
					"nativeInvoicingPaymentTerms_List": "nativeInvoicingPaymentTerms_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingPaymentTerms_List": "nativeInvoicingPaymentTerms_List"
				},
				"Create_Result": {
					"nativeInvoicingPaymentTerms": "nativeInvoicingPaymentTerms"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingPaymentTerms": "nativeInvoicingPaymentTerms"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingPaymentTerms_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingPaymentTerms_List"
				},
				"Read_Result": {
					"nativeInvoicingPaymentTerms": "nativeInvoicingPaymentTerms"
				},
				"Update": {
					"nativeInvoicingPaymentTerms": "nativeInvoicingPaymentTerms"
				},
				"UpdateMultiple": {
					"nativeInvoicingPaymentTerms_List": "nativeInvoicingPaymentTerms_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingPaymentTerms_List": "nativeInvoicingPaymentTerms_List"
				},
				"Update_Result": {
					"nativeInvoicingPaymentTerms": "nativeInvoicingPaymentTerms"
				},
				"nativeInvoicingPaymentTerms": {
					"Key": "string",
					"calculateDiscountOnCreditMemos": "boolean",
					"code": "string",
					"default": "boolean",
					"discountDateCalculation": "string",
					"discountPercent": "decimal",
					"displayName": "string",
					"dueDateCalculation": "string",
					"id": "string",
					"lastModifiedDateTime": "dateTime"
				},
				"nativeInvoicingPaymentTerms_Fields": "string",
				"nativeInvoicingPaymentTerms_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingPaymentTerms_Fields"
				},
				"nativeInvoicingPaymentTerms_List": {
					"nativeInvoicingPaymentTerms": "nativeInvoicingPaymentTerms"
				}
			}
		},
		"nativeInvoicingQBOSyncAuth": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingQBOSyncAuth": "nativeInvoicingQBOSyncAuth"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingQBOSyncAuth_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingQBOSyncAuth_List"
				},
				"nativeInvoicingQBOSyncAuth": {
					"Key": "string",
					"authorizationUrl": "string"
				},
				"nativeInvoicingQBOSyncAuth_Fields": "string",
				"nativeInvoicingQBOSyncAuth_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingQBOSyncAuth_Fields"
				},
				"nativeInvoicingQBOSyncAuth_List": {
					"nativeInvoicingQBOSyncAuth": "nativeInvoicingQBOSyncAuth"
				}
			}
		},
		"nativeInvoicingSMTPMailSetup": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingSMTPMailSetup": "nativeInvoicingSMTPMailSetup"
				},
				"CreateMultiple": {
					"nativeInvoicingSMTPMailSetup_List": "nativeInvoicingSMTPMailSetup_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingSMTPMailSetup_List": "nativeInvoicingSMTPMailSetup_List"
				},
				"Create_Result": {
					"nativeInvoicingSMTPMailSetup": "nativeInvoicingSMTPMailSetup"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"primaryKey": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingSMTPMailSetup": "nativeInvoicingSMTPMailSetup"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingSMTPMailSetup_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingSMTPMailSetup_List"
				},
				"Read_Result": {
					"nativeInvoicingSMTPMailSetup": "nativeInvoicingSMTPMailSetup"
				},
				"Update": {
					"nativeInvoicingSMTPMailSetup": "nativeInvoicingSMTPMailSetup"
				},
				"UpdateMultiple": {
					"nativeInvoicingSMTPMailSetup_List": "nativeInvoicingSMTPMailSetup_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingSMTPMailSetup_List": "nativeInvoicingSMTPMailSetup_List"
				},
				"Update_Result": {
					"nativeInvoicingSMTPMailSetup": "nativeInvoicingSMTPMailSetup"
				},
				"authentication": "string",
				"nativeInvoicingSMTPMailSetup": {
					"Key": "string",
					"SMTPServer": "string",
					"SMTPServerPort": "int",
					"authentication": "authentication",
					"passWord": "string",
					"primaryKey": "string",
					"secureConnection": "boolean",
					"userName": "string"
				},
				"nativeInvoicingSMTPMailSetup_Fields": "string",
				"nativeInvoicingSMTPMailSetup_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingSMTPMailSetup_Fields"
				},
				"nativeInvoicingSMTPMailSetup_List": {
					"nativeInvoicingSMTPMailSetup": "nativeInvoicingSMTPMailSetup"
				}
			}
		},
		"nativeInvoicingSalesInvoiceOverview": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingSalesInvoiceOverview": "nativeInvoicingSalesInvoiceOverview"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingSalesInvoiceOverview_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingSalesInvoiceOverview_List"
				},
				"Read_Result": {
					"nativeInvoicingSalesInvoiceOverview": "nativeInvoicingSalesInvoiceOverview"
				},
				"nativeInvoicingSalesInvoiceOverview": {
					"Key": "string",
					"currencyCode": "string",
					"customerName": "string",
					"customerNumber": "string",
					"dueDate": "date",
					"id": "string",
					"invoiceDate": "date",
					"isTest": "boolean",
					"lastModifiedDateTime": "dateTime",
					"number": "string",
					"status": "status",
					"subtotalAmount": "decimal",
					"totalAmountExcludingTax": "decimal",
					"totalAmountIncludingTax": "decimal",
					"totalTaxAmount": "decimal"
				},
				"nativeInvoicingSalesInvoiceOverview_Fields": "string",
				"nativeInvoicingSalesInvoiceOverview_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingSalesInvoiceOverview_Fields"
				},
				"nativeInvoicingSalesInvoiceOverview_List": {
					"nativeInvoicingSalesInvoiceOverview": "nativeInvoicingSalesInvoiceOverview"
				},
				"status": "string"
			}
		},
		"nativeInvoicingSalesInvoices": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"Delete_Payments": {
					"i": "Delete_Payments",
					"o": "Delete_Payments_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingSalesInvoices": "nativeInvoicingSalesInvoices"
				},
				"CreateMultiple": {
					"nativeInvoicingSalesInvoices_List": "nativeInvoicingSalesInvoices_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingSalesInvoices_List": "nativeInvoicingSalesInvoices_List"
				},
				"Create_Result": {
					"nativeInvoicingSalesInvoices": "nativeInvoicingSalesInvoices"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Payments": {
					"Payments_Key": "string"
				},
				"Delete_Payments_Result": {
					"Delete_Payments_Result": "boolean"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Native_Payments": {
					"Key": "string",
					"amount": "decimal",
					"appliesToInvoiceId": "string",
					"customerId": "string",
					"paymentDate": "date",
					"paymentMethodId": "string",
					"paymentNo": "int"
				},
				"Native_Payments_List": {
					"Native_Payments": "Native_Payments"
				},
				"Read": {
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingSalesInvoices": "nativeInvoicingSalesInvoices"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingSalesInvoices_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingSalesInvoices_List"
				},
				"Read_Result": {
					"nativeInvoicingSalesInvoices": "nativeInvoicingSalesInvoices"
				},
				"Update": {
					"nativeInvoicingSalesInvoices": "nativeInvoicingSalesInvoices"
				},
				"UpdateMultiple": {
					"nativeInvoicingSalesInvoices_List": "nativeInvoicingSalesInvoices_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingSalesInvoices_List": "nativeInvoicingSalesInvoices_List"
				},
				"Update_Result": {
					"nativeInvoicingSalesInvoices": "nativeInvoicingSalesInvoices"
				},
				"invoiceDiscountCalculation": "string",
				"nativeInvoicingSalesInvoices": {
					"Key": "string",
					"Payments": "Native_Payments_List",
					"attachments": "string",
					"billingPostalAddress": "string",
					"coupons": "string",
					"currencyCode": "string",
					"customerEmail": "string",
					"customerId": "string",
					"customerName": "string",
					"customerNumber": "string",
					"discountAmount": "decimal",
					"discountAppliedBeforeTax": "boolean",
					"dueDate": "date",
					"graphContactId": "string",
					"id": "string",
					"invoiceDate": "date",
					"invoiceDiscountCalculation": "invoiceDiscountCalculation",
					"invoiceDiscountValue": "decimal",
					"isCustomerBlocked": "boolean",
					"isTest": "boolean",
					"lastEmailSentTime": "dateTime",
					"lastModifiedDateTime": "dateTime",
					"lines": "string",
					"noteForCustomer": "string",
					"number": "string",
					"pricesIncludeTax": "boolean",
					"remainingAmount": "decimal",
					"status": "status",
					"subtotalAmount": "decimal",
					"taxAreaDisplayName": "string",
					"taxAreaId": "string",
					"taxLiable": "boolean",
					"taxRegistrationNumber": "string",
					"totalAmountExcludingTax": "decimal",
					"totalAmountIncludingTax": "decimal",
					"totalTaxAmount": "decimal"
				},
				"nativeInvoicingSalesInvoices_Fields": "string",
				"nativeInvoicingSalesInvoices_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingSalesInvoices_Fields"
				},
				"nativeInvoicingSalesInvoices_List": {
					"nativeInvoicingSalesInvoices": "nativeInvoicingSalesInvoices"
				},
				"status": "string"
			}
		},
		"nativeInvoicingSalesQuotes": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingSalesQuotes": "nativeInvoicingSalesQuotes"
				},
				"CreateMultiple": {
					"nativeInvoicingSalesQuotes_List": "nativeInvoicingSalesQuotes_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingSalesQuotes_List": "nativeInvoicingSalesQuotes_List"
				},
				"Create_Result": {
					"nativeInvoicingSalesQuotes": "nativeInvoicingSalesQuotes"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingSalesQuotes": "nativeInvoicingSalesQuotes"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingSalesQuotes_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingSalesQuotes_List"
				},
				"Read_Result": {
					"nativeInvoicingSalesQuotes": "nativeInvoicingSalesQuotes"
				},
				"Update": {
					"nativeInvoicingSalesQuotes": "nativeInvoicingSalesQuotes"
				},
				"UpdateMultiple": {
					"nativeInvoicingSalesQuotes_List": "nativeInvoicingSalesQuotes_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingSalesQuotes_List": "nativeInvoicingSalesQuotes_List"
				},
				"Update_Result": {
					"nativeInvoicingSalesQuotes": "nativeInvoicingSalesQuotes"
				},
				"invoiceDiscountCalculation": "string",
				"nativeInvoicingSalesQuotes": {
					"Key": "string",
					"accepted": "boolean",
					"acceptedDate": "date",
					"attachments": "string",
					"billingPostalAddress": "string",
					"coupons": "string",
					"currencyCode": "string",
					"customerEmail": "string",
					"customerId": "string",
					"customerName": "string",
					"customerNumber": "string",
					"discountAmount": "decimal",
					"discountAppliedBeforeTax": "boolean",
					"dueDate": "date",
					"graphContactId": "string",
					"id": "string",
					"invoiceDiscountCalculation": "invoiceDiscountCalculation",
					"invoiceDiscountValue": "decimal",
					"isCustomerBlocked": "boolean",
					"lastEmailSentTime": "dateTime",
					"lastModifiedDateTime": "dateTime",
					"lines": "string",
					"noteForCustomer": "string",
					"number": "string",
					"pricesIncludeTax": "boolean",
					"quoteDate": "date",
					"salesperson": "string",
					"shipmentMethod": "string",
					"status": "status",
					"subtotalAmount": "decimal",
					"taxAreaDisplayName": "string",
					"taxAreaId": "string",
					"taxLiable": "boolean",
					"taxRegistrationNumber": "string",
					"totalAmountExcludingTax": "decimal",
					"totalAmountIncludingTax": "decimal",
					"totalTaxAmount": "decimal",
					"validUntilDate": "date"
				},
				"nativeInvoicingSalesQuotes_Fields": "string",
				"nativeInvoicingSalesQuotes_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingSalesQuotes_Fields"
				},
				"nativeInvoicingSalesQuotes_List": {
					"nativeInvoicingSalesQuotes": "nativeInvoicingSalesQuotes"
				},
				"status": "string"
			}
		},
		"nativeInvoicingSalesTaxSetup": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingSalesTaxSetup": "nativeInvoicingSalesTaxSetup"
				},
				"CreateMultiple": {
					"nativeInvoicingSalesTaxSetup_List": "nativeInvoicingSalesTaxSetup_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingSalesTaxSetup_List": "nativeInvoicingSalesTaxSetup_List"
				},
				"Create_Result": {
					"nativeInvoicingSalesTaxSetup": "nativeInvoicingSalesTaxSetup"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"id": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingSalesTaxSetup": "nativeInvoicingSalesTaxSetup"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingSalesTaxSetup_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingSalesTaxSetup_List"
				},
				"Read_Result": {
					"nativeInvoicingSalesTaxSetup": "nativeInvoicingSalesTaxSetup"
				},
				"Update": {
					"nativeInvoicingSalesTaxSetup": "nativeInvoicingSalesTaxSetup"
				},
				"UpdateMultiple": {
					"nativeInvoicingSalesTaxSetup_List": "nativeInvoicingSalesTaxSetup_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingSalesTaxSetup_List": "nativeInvoicingSalesTaxSetup_List"
				},
				"Update_Result": {
					"nativeInvoicingSalesTaxSetup": "nativeInvoicingSalesTaxSetup"
				},
				"nativeInvoicingSalesTaxSetup": {
					"Key": "string",
					"canadaGstHstDescription": "string",
					"canadaGstHstRate": "decimal",
					"canadaPstDescription": "string",
					"canadaPstRate": "decimal",
					"city": "string",
					"cityRate": "decimal",
					"default": "boolean",
					"displayName": "string",
					"id": "string",
					"lastModifiedDateTime": "dateTime",
					"state": "string",
					"stateRate": "decimal"
				},
				"nativeInvoicingSalesTaxSetup_Fields": "string",
				"nativeInvoicingSalesTaxSetup_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingSalesTaxSetup_Fields"
				},
				"nativeInvoicingSalesTaxSetup_List": {
					"nativeInvoicingSalesTaxSetup": "nativeInvoicingSalesTaxSetup"
				}
			}
		},
		"nativeInvoicingSyncServicesSetting": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingSyncServicesSetting": "nativeInvoicingSyncServicesSetting"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingSyncServicesSetting_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingSyncServicesSetting_List"
				},
				"Update": {
					"nativeInvoicingSyncServicesSetting": "nativeInvoicingSyncServicesSetting"
				},
				"UpdateMultiple": {
					"nativeInvoicingSyncServicesSetting_List": "nativeInvoicingSyncServicesSetting_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingSyncServicesSetting_List": "nativeInvoicingSyncServicesSetting_List"
				},
				"Update_Result": {
					"nativeInvoicingSyncServicesSetting": "nativeInvoicingSyncServicesSetting"
				},
				"nativeInvoicingSyncServicesSetting": {
					"Key": "string",
					"qbdSyncDescription": "string",
					"qbdSyncEnabled": "boolean",
					"qbdSyncSendToEmail": "string",
					"qbdSyncTitle": "string",
					"qboSyncDescription": "string",
					"qboSyncEnabled": "boolean",
					"qboSyncTitle": "string"
				},
				"nativeInvoicingSyncServicesSetting_Fields": "string",
				"nativeInvoicingSyncServicesSetting_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingSyncServicesSetting_Fields"
				},
				"nativeInvoicingSyncServicesSetting_List": {
					"nativeInvoicingSyncServicesSetting": "nativeInvoicingSyncServicesSetting"
				}
			}
		},
		"nativeInvoicingTaxAreas": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingTaxAreas": "nativeInvoicingTaxAreas"
				},
				"CreateMultiple": {
					"nativeInvoicingTaxAreas_List": "nativeInvoicingTaxAreas_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingTaxAreas_List": "nativeInvoicingTaxAreas_List"
				},
				"Create_Result": {
					"nativeInvoicingTaxAreas": "nativeInvoicingTaxAreas"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"id": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingTaxAreas": "nativeInvoicingTaxAreas"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingTaxAreas_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingTaxAreas_List"
				},
				"Read_Result": {
					"nativeInvoicingTaxAreas": "nativeInvoicingTaxAreas"
				},
				"Update": {
					"nativeInvoicingTaxAreas": "nativeInvoicingTaxAreas"
				},
				"UpdateMultiple": {
					"nativeInvoicingTaxAreas_List": "nativeInvoicingTaxAreas_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingTaxAreas_List": "nativeInvoicingTaxAreas_List"
				},
				"Update_Result": {
					"nativeInvoicingTaxAreas": "nativeInvoicingTaxAreas"
				},
				"nativeInvoicingTaxAreas": {
					"Key": "string",
					"code": "string",
					"displayName": "string",
					"id": "string",
					"lastModifiedDateTime": "dateTime",
					"taxType": "taxType"
				},
				"nativeInvoicingTaxAreas_Fields": "string",
				"nativeInvoicingTaxAreas_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingTaxAreas_Fields"
				},
				"nativeInvoicingTaxAreas_List": {
					"nativeInvoicingTaxAreas": "nativeInvoicingTaxAreas"
				},
				"taxType": "string"
			}
		},
		"nativeInvoicingTaxGroups": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"id": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingTaxGroups": "nativeInvoicingTaxGroups"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingTaxGroups_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingTaxGroups_List"
				},
				"Read_Result": {
					"nativeInvoicingTaxGroups": "nativeInvoicingTaxGroups"
				},
				"Update": {
					"nativeInvoicingTaxGroups": "nativeInvoicingTaxGroups"
				},
				"UpdateMultiple": {
					"nativeInvoicingTaxGroups_List": "nativeInvoicingTaxGroups_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingTaxGroups_List": "nativeInvoicingTaxGroups_List"
				},
				"Update_Result": {
					"nativeInvoicingTaxGroups": "nativeInvoicingTaxGroups"
				},
				"nativeInvoicingTaxGroups": {
					"Key": "string",
					"code": "string",
					"displayName": "string",
					"id": "string",
					"lastModifiedDateTime": "dateTime",
					"taxType": "taxType"
				},
				"nativeInvoicingTaxGroups_Fields": "string",
				"nativeInvoicingTaxGroups_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingTaxGroups_Fields"
				},
				"nativeInvoicingTaxGroups_List": {
					"nativeInvoicingTaxGroups": "nativeInvoicingTaxGroups"
				},
				"taxType": "string"
			}
		},
		"nativeInvoicingTaxRates": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"taxAreaId": "string",
					"taxGroupId": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingTaxRates": "nativeInvoicingTaxRates"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingTaxRates_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingTaxRates_List"
				},
				"Read_Result": {
					"nativeInvoicingTaxRates": "nativeInvoicingTaxRates"
				},
				"nativeInvoicingTaxRates": {
					"Key": "string",
					"taxAreaId": "string",
					"taxGroupId": "string",
					"taxRate": "decimal"
				},
				"nativeInvoicingTaxRates_Fields": "string",
				"nativeInvoicingTaxRates_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingTaxRates_Fields"
				},
				"nativeInvoicingTaxRates_List": {
					"nativeInvoicingTaxRates": "nativeInvoicingTaxRates"
				}
			}
		},
		"nativeInvoicingTestMail": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingTestMail": "nativeInvoicingTestMail"
				},
				"CreateMultiple": {
					"nativeInvoicingTestMail_List": "nativeInvoicingTestMail_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingTestMail_List": "nativeInvoicingTestMail_List"
				},
				"Create_Result": {
					"nativeInvoicingTestMail": "nativeInvoicingTestMail"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingTestMail": "nativeInvoicingTestMail"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingTestMail_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingTestMail_List"
				},
				"nativeInvoicingTestMail": {
					"Key": "string",
					"email": "string"
				},
				"nativeInvoicingTestMail_Fields": "string",
				"nativeInvoicingTestMail_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingTestMail_Fields"
				},
				"nativeInvoicingTestMail_List": {
					"nativeInvoicingTestMail": "nativeInvoicingTestMail"
				}
			}
		},
		"nativeInvoicingUnitsOfMeasure": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"nativeInvoicingUnitsOfMeasure": "nativeInvoicingUnitsOfMeasure"
				},
				"CreateMultiple": {
					"nativeInvoicingUnitsOfMeasure_List": "nativeInvoicingUnitsOfMeasure_List"
				},
				"CreateMultiple_Result": {
					"nativeInvoicingUnitsOfMeasure_List": "nativeInvoicingUnitsOfMeasure_List"
				},
				"Create_Result": {
					"nativeInvoicingUnitsOfMeasure": "nativeInvoicingUnitsOfMeasure"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"code": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingUnitsOfMeasure": "nativeInvoicingUnitsOfMeasure"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingUnitsOfMeasure_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingUnitsOfMeasure_List"
				},
				"Read_Result": {
					"nativeInvoicingUnitsOfMeasure": "nativeInvoicingUnitsOfMeasure"
				},
				"Update": {
					"nativeInvoicingUnitsOfMeasure": "nativeInvoicingUnitsOfMeasure"
				},
				"UpdateMultiple": {
					"nativeInvoicingUnitsOfMeasure_List": "nativeInvoicingUnitsOfMeasure_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingUnitsOfMeasure_List": "nativeInvoicingUnitsOfMeasure_List"
				},
				"Update_Result": {
					"nativeInvoicingUnitsOfMeasure": "nativeInvoicingUnitsOfMeasure"
				},
				"nativeInvoicingUnitsOfMeasure": {
					"Key": "string",
					"code": "string",
					"displayName": "string",
					"id": "string",
					"internationalStandardCode": "string",
					"lastModifiedDateTime": "dateTime"
				},
				"nativeInvoicingUnitsOfMeasure_Fields": "string",
				"nativeInvoicingUnitsOfMeasure_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingUnitsOfMeasure_Fields"
				},
				"nativeInvoicingUnitsOfMeasure_List": {
					"nativeInvoicingUnitsOfMeasure": "nativeInvoicingUnitsOfMeasure"
				}
			}
		},
		"nativeInvoicingVATSetup": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"id": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"nativeInvoicingVATSetup": "nativeInvoicingVATSetup"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "nativeInvoicingVATSetup_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "nativeInvoicingVATSetup_List"
				},
				"Read_Result": {
					"nativeInvoicingVATSetup": "nativeInvoicingVATSetup"
				},
				"Update": {
					"nativeInvoicingVATSetup": "nativeInvoicingVATSetup"
				},
				"UpdateMultiple": {
					"nativeInvoicingVATSetup_List": "nativeInvoicingVATSetup_List"
				},
				"UpdateMultiple_Result": {
					"nativeInvoicingVATSetup_List": "nativeInvoicingVATSetup_List"
				},
				"Update_Result": {
					"nativeInvoicingVATSetup": "nativeInvoicingVATSetup"
				},
				"nativeInvoicingVATSetup": {
					"Key": "string",
					"default": "boolean",
					"displayName": "string",
					"id": "string",
					"lastModifiedDateTime": "dateTime",
					"vatPercentage": "decimal",
					"vatRegulationDescription": "string"
				},
				"nativeInvoicingVATSetup_Fields": "string",
				"nativeInvoicingVATSetup_Filter": {
					"Criteria": "string",
					"Field": "nativeInvoicingVATSetup_Fields"
				},
				"nativeInvoicingVATSetup_List": {
					"nativeInvoicingVATSetup": "nativeInvoicingVATSetup"
				}
			}
		},
		"powerbifinance": {
			"f": {
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"No": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"powerbifinance": "powerbifinance"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "powerbifinance_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "powerbifinance_List"
				},
				"Read_Result": {
					"powerbifinance": "powerbifinance"
				},
				"powerbifinance": {
					"Account_Schedule_Name": "string",
					"Balance_at_Date_Actual": "decimal",
					"Balance_at_Date_Actual_Last_Year": "decimal",
					"Balance_at_Date_Budget": "decimal",
					"Balance_at_Date_Budget_Last_Year": "decimal",
					"Balance_at_Date_Forecast": "decimal",
					"Closed_Period": "boolean",
					"Date": "date",
					"KPI_Code": "string",
					"KPI_Name": "string",
					"Key": "string",
					"Net_Change_Actual": "decimal",
					"Net_Change_Actual_Last_Year": "decimal",
					"Net_Change_Budget": "decimal",
					"Net_Change_Budget_Last_Year": "decimal",
					"Net_Change_Forecast": "decimal",
					"No": "int"
				},
				"powerbifinance_Fields": "string",
				"powerbifinance_Filter": {
					"Criteria": "string",
					"Field": "powerbifinance_Fields"
				},
				"powerbifinance_List": {
					"powerbifinance": "powerbifinance"
				}
			}
		},
		"purchaseDocumentLines": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"purchaseDocumentLines": "purchaseDocumentLines"
				},
				"CreateMultiple": {
					"purchaseDocumentLines_List": "purchaseDocumentLines_List"
				},
				"CreateMultiple_Result": {
					"purchaseDocumentLines_List": "purchaseDocumentLines_List"
				},
				"Create_Result": {
					"purchaseDocumentLines": "purchaseDocumentLines"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"documentNumber": "string",
					"documentType": "string",
					"lineNumber": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"purchaseDocumentLines": "purchaseDocumentLines"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "purchaseDocumentLines_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "purchaseDocumentLines_List"
				},
				"Read_Result": {
					"purchaseDocumentLines": "purchaseDocumentLines"
				},
				"Update": {
					"purchaseDocumentLines": "purchaseDocumentLines"
				},
				"UpdateMultiple": {
					"purchaseDocumentLines_List": "purchaseDocumentLines_List"
				},
				"UpdateMultiple_Result": {
					"purchaseDocumentLines_List": "purchaseDocumentLines_List"
				},
				"Update_Result": {
					"purchaseDocumentLines": "purchaseDocumentLines"
				},
				"crossReferenceType": "string",
				"documentType": "string",
				"faPostingType": "string",
				"icPartnerRefType": "string",
				"jobLineType": "string",
				"planningFlexibility": "string",
				"prepmtVatCalcType": "string",
				"purchaseDocumentLines": {
					"Key": "string",
					"aRcdNotInvExVatLcy": "decimal",
					"allowInvoiceDisc": "boolean",
					"allowItemChargeAssignment": "boolean",
					"amount": "decimal",
					"amountIncludingVat": "decimal",
					"amtRcdNotInvoiced": "decimal",
					"amtRcdNotInvoicedLcy": "decimal",
					"applToItemEntry": "int",
					"area": "string",
					"attachedToLineNumber": "int",
					"binCode": "string",
					"blanketOrderLineNumber": "int",
					"blanketOrderNumber": "string",
					"budgetedFaNumber": "string",
					"buyFromVendorNumber": "string",
					"completelyReceived": "boolean",
					"crossReferenceNumber": "string",
					"crossReferenceType": "crossReferenceType",
					"crossReferenceTypeNumber": "string",
					"currencyCode": "string",
					"deferralCode": "string",
					"deprAcquisitionCost": "boolean",
					"deprUntilFaPostingDate": "boolean",
					"depreciationBookCode": "string",
					"description": "string",
					"description2": "string",
					"dimensionSetId": "int",
					"directUnitCost": "decimal",
					"documentNumber": "string",
					"documentType": "documentType",
					"dropShipment": "boolean",
					"duplicateInDepreciationBook": "string",
					"entryPoint": "string",
					"expectedReceiptDate": "date",
					"faPostingDate": "date",
					"faPostingType": "faPostingType",
					"finished": "boolean",
					"genBusPostingGroup": "string",
					"genProdPostingGroup": "string",
					"grossWeight": "decimal",
					"icPartnerCode": "string",
					"icPartnerRefType": "icPartnerRefType",
					"icPartnerReference": "string",
					"inboundWhseHandlingTime": "string",
					"indirectCostPercent": "decimal",
					"insuranceNumber": "string",
					"invDiscAmountToInvoice": "decimal",
					"invDiscountAmount": "decimal",
					"itemCategoryCode": "string",
					"jobCurrencyCode": "string",
					"jobCurrencyFactor": "decimal",
					"jobLineAmount": "decimal",
					"jobLineAmountLcy": "decimal",
					"jobLineDiscAmountLcy": "decimal",
					"jobLineDiscountAmount": "decimal",
					"jobLineDiscountPercent": "decimal",
					"jobLineType": "jobLineType",
					"jobNumber": "string",
					"jobPlanningLineNumber": "int",
					"jobRemainingQty": "decimal",
					"jobRemainingQtyBase": "decimal",
					"jobTaskNumber": "string",
					"jobTotalPrice": "decimal",
					"jobTotalPriceLcy": "decimal",
					"jobUnitPrice": "decimal",
					"jobUnitPriceLcy": "decimal",
					"leadTimeCalculation": "string",
					"lineAmount": "decimal",
					"lineDiscountAmount": "decimal",
					"lineDiscountPercent": "decimal",
					"lineNumber": "int",
					"locationCode": "string",
					"maintenanceCode": "string",
					"mpsOrder": "boolean",
					"netWeight": "decimal",
					"nonstock": "boolean",
					"number": "string",
					"operationNumber": "string",
					"orderDate": "date",
					"outstandingAmount": "decimal",
					"outstandingAmountLcy": "decimal",
					"outstandingAmtExVatLcy": "decimal",
					"outstandingQtyBase": "decimal",
					"outstandingQuantity": "decimal",
					"overheadRate": "decimal",
					"payToVendorNumber": "string",
					"plannedReceiptDate": "date",
					"planningFlexibility": "planningFlexibility",
					"postingGroup": "string",
					"prepaymentAmount": "decimal",
					"prepaymentLine": "boolean",
					"prepaymentPercent": "decimal",
					"prepaymentTaxAreaCode": "string",
					"prepaymentTaxGroupCode": "string",
					"prepaymentTaxLiable": "boolean",
					"prepaymentVatDifference": "decimal",
					"prepaymentVatIdentifier": "string",
					"prepaymentVatPercent": "decimal",
					"prepmtAmountInvInclVat": "decimal",
					"prepmtAmountInvLcy": "decimal",
					"prepmtAmtDeducted": "decimal",
					"prepmtAmtInclVat": "decimal",
					"prepmtAmtInv": "decimal",
					"prepmtAmtToDeduct": "decimal",
					"prepmtLineAmount": "decimal",
					"prepmtVatAmountInvLcy": "decimal",
					"prepmtVatBaseAmt": "decimal",
					"prepmtVatCalcType": "prepmtVatCalcType",
					"prepmtVatDiffDeducted": "decimal",
					"prepmtVatDiffToDeduct": "decimal",
					"prodOrderLineNumber": "int",
					"prodOrderNumber": "string",
					"profitPercent": "decimal",
					"promisedReceiptDate": "date",
					"purchasingCode": "string",
					"qtyAssigned": "decimal",
					"qtyInvoicedBase": "decimal",
					"qtyPerUnitOfMeasure": "decimal",
					"qtyRcdNotInvoiced": "decimal",
					"qtyRcdNotInvoicedBase": "decimal",
					"qtyReceivedBase": "decimal",
					"qtyToAssign": "decimal",
					"qtyToInvoice": "decimal",
					"qtyToInvoiceBase": "decimal",
					"qtyToReceive": "decimal",
					"qtyToReceiveBase": "decimal",
					"quantity": "decimal",
					"quantityBase": "decimal",
					"quantityInvoiced": "decimal",
					"quantityReceived": "decimal",
					"recalculateInvoiceDisc": "boolean",
					"receiptLineNumber": "int",
					"receiptNumber": "string",
					"requestedReceiptDate": "date",
					"reservedQtyBase": "decimal",
					"reservedQuantity": "decimal",
					"responsibilityCenter": "string",
					"retQtyShpdNotInvdBase": "decimal",
					"returnQtyShipped": "decimal",
					"returnQtyShippedBase": "decimal",
					"returnQtyShippedNotInvd": "decimal",
					"returnQtyToShip": "decimal",
					"returnQtyToShipBase": "decimal",
					"returnReasonCode": "string",
					"returnShipmentLineNumber": "int",
					"returnShipmentNumber": "string",
					"returnShpdNotInvd": "decimal",
					"returnShpdNotInvdLcy": "decimal",
					"returnsDeferralStartDate": "date",
					"routingNumber": "string",
					"routingReferenceNumber": "int",
					"safetyLeadTime": "string",
					"salesOrderLineNumber": "int",
					"salesOrderNumber": "string",
					"salvageValue": "decimal",
					"shortcutDimension1Code": "string",
					"shortcutDimension2Code": "string",
					"specialOrder": "boolean",
					"specialOrderSalesLineNumber": "int",
					"specialOrderSalesNumber": "string",
					"subtype": "subtype",
					"systemCreatedEntry": "boolean",
					"taxAreaCode": "string",
					"taxGroupCode": "string",
					"taxLiable": "boolean",
					"transactionSpecification": "string",
					"transactionType": "string",
					"transportMethod": "string",
					"type": "type",
					"unitCost": "decimal",
					"unitCostLcy": "decimal",
					"unitOfMeasure": "string",
					"unitOfMeasureCode": "string",
					"unitOfMeasureCrossRef": "string",
					"unitPriceLcy": "decimal",
					"unitVolume": "decimal",
					"unitsPerParcel": "decimal",
					"useDuplicationList": "boolean",
					"useTax": "boolean",
					"variantCode": "string",
					"vatBaseAmount": "decimal",
					"vatBusPostingGroup": "string",
					"vatCalculationType": "vatCalculationType",
					"vatDifference": "decimal",
					"vatIdentifier": "string",
					"vatPercent": "decimal",
					"vatProdPostingGroup": "string",
					"vendorItemNumber": "string",
					"whseOutstandingQtyBase": "decimal",
					"workCenterNumber": "string"
				},
				"purchaseDocumentLines_Fields": "string",
				"purchaseDocumentLines_Filter": {
					"Criteria": "string",
					"Field": "purchaseDocumentLines_Fields"
				},
				"purchaseDocumentLines_List": {
					"purchaseDocumentLines": "purchaseDocumentLines"
				},
				"subtype": "string",
				"type": "string",
				"vatCalculationType": "string"
			}
		},
		"purchaseDocuments": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"Delete_workflowPurchaseDocumentLines": {
					"i": "Delete_workflowPurchaseDocumentLines",
					"o": "Delete_workflowPurchaseDocumentLines_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"purchaseDocuments": "purchaseDocuments"
				},
				"CreateMultiple": {
					"purchaseDocuments_List": "purchaseDocuments_List"
				},
				"CreateMultiple_Result": {
					"purchaseDocuments_List": "purchaseDocuments_List"
				},
				"Create_Result": {
					"purchaseDocuments": "purchaseDocuments"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Delete_workflowPurchaseDocumentLines": {
					"workflowPurchaseDocumentLines_Key": "string"
				},
				"Delete_workflowPurchaseDocumentLines_Result": {
					"Delete_workflowPurchaseDocumentLines_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Purchase_Document_Line_Entity": {
					"Key": "string",
					"aRcdNotInvExVatLcy": "decimal",
					"allowInvoiceDisc": "boolean",
					"allowItemChargeAssignment": "boolean",
					"amount": "decimal",
					"amountIncludingVat": "decimal",
					"amtRcdNotInvoiced": "decimal",
					"amtRcdNotInvoicedLcy": "decimal",
					"applToItemEntry": "int",
					"area": "string",
					"attachedToLineNumber": "int",
					"binCode": "string",
					"blanketOrderLineNumber": "int",
					"blanketOrderNumber": "string",
					"budgetedFaNumber": "string",
					"buyFromVendorNumber": "string",
					"completelyReceived": "boolean",
					"crossReferenceNumber": "string",
					"crossReferenceType": "crossReferenceType",
					"crossReferenceTypeNumber": "string",
					"currencyCode": "string",
					"deferralCode": "string",
					"deprAcquisitionCost": "boolean",
					"deprUntilFaPostingDate": "boolean",
					"depreciationBookCode": "string",
					"description": "string",
					"description2": "string",
					"dimensionSetId": "int",
					"directUnitCost": "decimal",
					"documentNumber": "string",
					"documentType": "documentType_1",
					"dropShipment": "boolean",
					"duplicateInDepreciationBook": "string",
					"entryPoint": "string",
					"expectedReceiptDate": "date",
					"faPostingDate": "date",
					"faPostingType": "faPostingType",
					"finished": "boolean",
					"genBusPostingGroup": "string",
					"genProdPostingGroup": "string",
					"grossWeight": "decimal",
					"icPartnerCode": "string",
					"icPartnerRefType": "icPartnerRefType",
					"icPartnerReference": "string",
					"inboundWhseHandlingTime": "string",
					"indirectCostPercent": "decimal",
					"insuranceNumber": "string",
					"invDiscAmountToInvoice": "decimal",
					"invDiscountAmount": "decimal",
					"itemCategoryCode": "string",
					"jobCurrencyCode": "string",
					"jobCurrencyFactor": "decimal",
					"jobLineAmount": "decimal",
					"jobLineAmountLcy": "decimal",
					"jobLineDiscAmountLcy": "decimal",
					"jobLineDiscountAmount": "decimal",
					"jobLineDiscountPercent": "decimal",
					"jobLineType": "jobLineType",
					"jobNumber": "string",
					"jobPlanningLineNumber": "int",
					"jobRemainingQty": "decimal",
					"jobRemainingQtyBase": "decimal",
					"jobTaskNumber": "string",
					"jobTotalPrice": "decimal",
					"jobTotalPriceLcy": "decimal",
					"jobUnitPrice": "decimal",
					"jobUnitPriceLcy": "decimal",
					"leadTimeCalculation": "string",
					"lineAmount": "decimal",
					"lineDiscountAmount": "decimal",
					"lineDiscountPercent": "decimal",
					"lineNumber": "int",
					"locationCode": "string",
					"maintenanceCode": "string",
					"mpsOrder": "boolean",
					"netWeight": "decimal",
					"nonstock": "boolean",
					"number": "string",
					"operationNumber": "string",
					"orderDate": "date",
					"outstandingAmount": "decimal",
					"outstandingAmountLcy": "decimal",
					"outstandingAmtExVatLcy": "decimal",
					"outstandingQtyBase": "decimal",
					"outstandingQuantity": "decimal",
					"overheadRate": "decimal",
					"payToVendorNumber": "string",
					"plannedReceiptDate": "date",
					"planningFlexibility": "planningFlexibility",
					"postingGroup": "string",
					"prepaymentAmount": "decimal",
					"prepaymentLine": "boolean",
					"prepaymentPercent": "decimal",
					"prepaymentTaxAreaCode": "string",
					"prepaymentTaxGroupCode": "string",
					"prepaymentTaxLiable": "boolean",
					"prepaymentVatDifference": "decimal",
					"prepaymentVatIdentifier": "string",
					"prepaymentVatPercent": "decimal",
					"prepmtAmountInvInclVat": "decimal",
					"prepmtAmountInvLcy": "decimal",
					"prepmtAmtDeducted": "decimal",
					"prepmtAmtInclVat": "decimal",
					"prepmtAmtInv": "decimal",
					"prepmtAmtToDeduct": "decimal",
					"prepmtLineAmount": "decimal",
					"prepmtVatAmountInvLcy": "decimal",
					"prepmtVatBaseAmt": "decimal",
					"prepmtVatCalcType": "prepmtVatCalcType",
					"prepmtVatDiffDeducted": "decimal",
					"prepmtVatDiffToDeduct": "decimal",
					"prodOrderLineNumber": "int",
					"prodOrderNumber": "string",
					"profitPercent": "decimal",
					"promisedReceiptDate": "date",
					"purchasingCode": "string",
					"qtyAssigned": "decimal",
					"qtyInvoicedBase": "decimal",
					"qtyPerUnitOfMeasure": "decimal",
					"qtyRcdNotInvoiced": "decimal",
					"qtyRcdNotInvoicedBase": "decimal",
					"qtyReceivedBase": "decimal",
					"qtyToAssign": "decimal",
					"qtyToInvoice": "decimal",
					"qtyToInvoiceBase": "decimal",
					"qtyToReceive": "decimal",
					"qtyToReceiveBase": "decimal",
					"quantity": "decimal",
					"quantityBase": "decimal",
					"quantityInvoiced": "decimal",
					"quantityReceived": "decimal",
					"recalculateInvoiceDisc": "boolean",
					"receiptLineNumber": "int",
					"receiptNumber": "string",
					"requestedReceiptDate": "date",
					"reservedQtyBase": "decimal",
					"reservedQuantity": "decimal",
					"responsibilityCenter": "string",
					"retQtyShpdNotInvdBase": "decimal",
					"returnQtyShipped": "decimal",
					"returnQtyShippedBase": "decimal",
					"returnQtyShippedNotInvd": "decimal",
					"returnQtyToShip": "decimal",
					"returnQtyToShipBase": "decimal",
					"returnReasonCode": "string",
					"returnShipmentLineNumber": "int",
					"returnShipmentNumber": "string",
					"returnShpdNotInvd": "decimal",
					"returnShpdNotInvdLcy": "decimal",
					"returnsDeferralStartDate": "date",
					"routingNumber": "string",
					"routingReferenceNumber": "int",
					"safetyLeadTime": "string",
					"salesOrderLineNumber": "int",
					"salesOrderNumber": "string",
					"salvageValue": "decimal",
					"shortcutDimension1Code": "string",
					"shortcutDimension2Code": "string",
					"specialOrder": "boolean",
					"specialOrderSalesLineNumber": "int",
					"specialOrderSalesNumber": "string",
					"subtype": "subtype",
					"systemCreatedEntry": "boolean",
					"taxAreaCode": "string",
					"taxGroupCode": "string",
					"taxLiable": "boolean",
					"transactionSpecification": "string",
					"transactionType": "string",
					"transportMethod": "string",
					"type": "type",
					"unitCost": "decimal",
					"unitCostLcy": "decimal",
					"unitOfMeasure": "string",
					"unitOfMeasureCode": "string",
					"unitOfMeasureCrossRef": "string",
					"unitPriceLcy": "decimal",
					"unitVolume": "decimal",
					"unitsPerParcel": "decimal",
					"useDuplicationList": "boolean",
					"useTax": "boolean",
					"variantCode": "string",
					"vatBaseAmount": "decimal",
					"vatBusPostingGroup": "string",
					"vatCalculationType": "vatCalculationType",
					"vatDifference": "decimal",
					"vatIdentifier": "string",
					"vatPercent": "decimal",
					"vatProdPostingGroup": "string",
					"vendorItemNumber": "string",
					"whseOutstandingQtyBase": "decimal",
					"workCenterNumber": "string"
				},
				"Purchase_Document_Line_Entity_List": {
					"Purchase_Document_Line_Entity": "Purchase_Document_Line_Entity"
				},
				"Read": {
					"documentType": "string",
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"purchaseDocuments": "purchaseDocuments"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "purchaseDocuments_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "purchaseDocuments_List"
				},
				"Read_Result": {
					"purchaseDocuments": "purchaseDocuments"
				},
				"Update": {
					"purchaseDocuments": "purchaseDocuments"
				},
				"UpdateMultiple": {
					"purchaseDocuments_List": "purchaseDocuments_List"
				},
				"UpdateMultiple_Result": {
					"purchaseDocuments_List": "purchaseDocuments_List"
				},
				"Update_Result": {
					"purchaseDocuments": "purchaseDocuments"
				},
				"appliesToDocType": "string",
				"balAccountType": "string",
				"crossReferenceType": "string",
				"documentType": "string",
				"documentType_1": "string",
				"faPostingType": "string",
				"icDirection": "string",
				"icPartnerRefType": "string",
				"icStatus": "string",
				"invoiceDiscountCalculation": "string",
				"jobLineType": "string",
				"jobQueueStatus": "string",
				"planningFlexibility": "string",
				"prepmtVatCalcType": "string",
				"purchaseDocuments": {
					"Key": "string",
					"aRcdNotInvExVatLcy": "decimal",
					"amount": "decimal",
					"amountIncludingVat": "decimal",
					"amtRcdNotInvoicedLcy": "decimal",
					"appliesToDocNumber": "string",
					"appliesToDocType": "appliesToDocType",
					"appliesToId": "string",
					"area": "string",
					"assignedUserId": "string",
					"balAccountNumber": "string",
					"balAccountType": "balAccountType",
					"buyFromAddress": "string",
					"buyFromAddress2": "string",
					"buyFromCity": "string",
					"buyFromContact": "string",
					"buyFromContactNumber": "string",
					"buyFromCountryRegionCode": "string",
					"buyFromCounty": "string",
					"buyFromIcPartnerCode": "string",
					"buyFromPostCode": "string",
					"buyFromVendorName": "string",
					"buyFromVendorName2": "string",
					"buyFromVendorNumber": "string",
					"campaignNumber": "string",
					"comment": "boolean",
					"completelyReceived": "boolean",
					"compressPrepayment": "boolean",
					"correction": "boolean",
					"creditorNumber": "string",
					"currencyCode": "string",
					"currencyFactor": "decimal",
					"dimensionSetId": "int",
					"docNumberOccurrence": "int",
					"documentDate": "date",
					"documentType": "documentType",
					"dueDate": "date",
					"entryPoint": "string",
					"expectedReceiptDate": "date",
					"genBusPostingGroup": "string",
					"icDirection": "icDirection",
					"icStatus": "icStatus",
					"id": "string",
					"inboundWhseHandlingTime": "string",
					"incomingDocumentEntryNumber": "int",
					"invoice": "boolean",
					"invoiceDiscCode": "string",
					"invoiceDiscountAmount": "decimal",
					"invoiceDiscountCalculation": "invoiceDiscountCalculation",
					"invoiceDiscountValue": "decimal",
					"jobQueueEntryId": "string",
					"jobQueueStatus": "jobQueueStatus",
					"languageCode": "string",
					"lastPostingNumber": "string",
					"lastPrepaymentNumber": "string",
					"lastPrepmtCrMemoNumber": "string",
					"lastReceivingNumber": "string",
					"lastReturnShipmentNumber": "string",
					"leadTimeCalculation": "string",
					"locationCode": "string",
					"locationFilter": "string",
					"number": "string",
					"numberOfArchivedVersions": "int",
					"numberPrinted": "int",
					"numberSeries": "string",
					"onHold": "string",
					"orderAddressCode": "string",
					"orderClass": "string",
					"orderDate": "date",
					"payToAddress": "string",
					"payToAddress2": "string",
					"payToCity": "string",
					"payToContact": "string",
					"payToContactNumber": "string",
					"payToCountryRegionCode": "string",
					"payToCounty": "string",
					"payToIcPartnerCode": "string",
					"payToName": "string",
					"payToName2": "string",
					"payToPostCode": "string",
					"payToVendorNumber": "string",
					"paymentDiscountPercent": "decimal",
					"paymentMethodCode": "string",
					"paymentReference": "string",
					"paymentTermsCode": "string",
					"pendingApprovals": "int",
					"pmtDiscountDate": "date",
					"postingDate": "date",
					"postingDescription": "string",
					"postingFromWhseRef": "int",
					"postingNumber": "string",
					"postingNumberSeries": "string",
					"prepaymentDueDate": "date",
					"prepaymentNumber": "string",
					"prepaymentNumberSeries": "string",
					"prepaymentPercent": "decimal",
					"prepmtCrMemoNumber": "string",
					"prepmtCrMemoNumberSeries": "string",
					"prepmtPaymentDiscountPercent": "decimal",
					"prepmtPaymentTermsCode": "string",
					"prepmtPmtDiscountDate": "date",
					"prepmtPostingDescription": "string",
					"pricesIncludingVat": "boolean",
					"printPostedDocuments": "boolean",
					"promisedReceiptDate": "date",
					"purchaserCode": "string",
					"quoteNumber": "string",
					"reasonCode": "string",
					"recalculateInvoiceDisc": "boolean",
					"receive": "boolean",
					"receivingNumber": "string",
					"receivingNumberSeries": "string",
					"requestedReceiptDate": "date",
					"responsibilityCenter": "string",
					"returnShipmentNumber": "string",
					"returnShipmentNumberSeries": "string",
					"sellToCustomerNumber": "string",
					"sendIcDocument": "boolean",
					"ship": "boolean",
					"shipToAddress": "string",
					"shipToAddress2": "string",
					"shipToCity": "string",
					"shipToCode": "string",
					"shipToContact": "string",
					"shipToCountryRegionCode": "string",
					"shipToCounty": "string",
					"shipToName": "string",
					"shipToName2": "string",
					"shipToPostCode": "string",
					"shipmentMethodCode": "string",
					"shortcutDimension1Code": "string",
					"shortcutDimension2Code": "string",
					"status": "status",
					"taxAreaCode": "string",
					"taxLiable": "boolean",
					"transactionSpecification": "string",
					"transactionType": "string",
					"transportMethod": "string",
					"vatBaseDiscountPercent": "decimal",
					"vatBusPostingGroup": "string",
					"vatCountryRegionCode": "string",
					"vatRegistrationNumber": "string",
					"vendorAuthorizationNumber": "string",
					"vendorCrMemoNumber": "string",
					"vendorInvoiceNumber": "string",
					"vendorOrderNumber": "string",
					"vendorPostingGroup": "string",
					"vendorShipmentNumber": "string",
					"workflowPurchaseDocumentLines": "Purchase_Document_Line_Entity_List",
					"yourReference": "string"
				},
				"purchaseDocuments_Fields": "string",
				"purchaseDocuments_Filter": {
					"Criteria": "string",
					"Field": "purchaseDocuments_Fields"
				},
				"purchaseDocuments_List": {
					"purchaseDocuments": "purchaseDocuments"
				},
				"status": "string",
				"subtype": "string",
				"type": "string",
				"vatCalculationType": "string"
			}
		},
		"salesDocumentLines": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"salesDocumentLines": "salesDocumentLines"
				},
				"CreateMultiple": {
					"salesDocumentLines_List": "salesDocumentLines_List"
				},
				"CreateMultiple_Result": {
					"salesDocumentLines_List": "salesDocumentLines_List"
				},
				"Create_Result": {
					"salesDocumentLines": "salesDocumentLines"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"documentNumber": "string",
					"documentType": "string",
					"lineNumber": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"salesDocumentLines": "salesDocumentLines"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "salesDocumentLines_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "salesDocumentLines_List"
				},
				"Read_Result": {
					"salesDocumentLines": "salesDocumentLines"
				},
				"Update": {
					"salesDocumentLines": "salesDocumentLines"
				},
				"UpdateMultiple": {
					"salesDocumentLines_List": "salesDocumentLines_List"
				},
				"UpdateMultiple_Result": {
					"salesDocumentLines_List": "salesDocumentLines_List"
				},
				"Update_Result": {
					"salesDocumentLines": "salesDocumentLines"
				},
				"crossReferenceType": "string",
				"documentType": "string",
				"icPartnerRefType": "string",
				"prepmtVatCalcType": "string",
				"reserve": "string",
				"salesDocumentLines": {
					"Key": "string",
					"allowInvoiceDisc": "boolean",
					"allowItemChargeAssignment": "boolean",
					"allowLineDisc": "boolean",
					"amount": "decimal",
					"amountIncludingVat": "decimal",
					"applFromItemEntry": "int",
					"applToItemEntry": "int",
					"area": "string",
					"atoWhseOutstandingQty": "decimal",
					"atoWhseOutstdQtyBase": "decimal",
					"attachedToLineNumber": "int",
					"billToCustomerNumber": "string",
					"binCode": "string",
					"blanketOrderLineNumber": "int",
					"blanketOrderNumber": "string",
					"bomItemNumber": "string",
					"completelyShipped": "boolean",
					"crossReferenceNumber": "string",
					"crossReferenceType": "crossReferenceType",
					"crossReferenceTypeNumber": "string",
					"currencyCode": "string",
					"customerDiscGroup": "string",
					"customerPriceGroup": "string",
					"deferralCode": "string",
					"deprUntilFaPostingDate": "boolean",
					"depreciationBookCode": "string",
					"description": "string",
					"description2": "string",
					"dimensionSetId": "int",
					"documentNumber": "string",
					"documentType": "documentType",
					"dropShipment": "boolean",
					"duplicateInDepreciationBook": "string",
					"exitPoint": "string",
					"faPostingDate": "date",
					"genBusPostingGroup": "string",
					"genProdPostingGroup": "string",
					"grossWeight": "decimal",
					"icPartnerCode": "string",
					"icPartnerRefType": "icPartnerRefType",
					"icPartnerReference": "string",
					"invDiscAmountToInvoice": "decimal",
					"invDiscountAmount": "decimal",
					"itemCategoryCode": "string",
					"jobContractEntryNumber": "int",
					"jobNumber": "string",
					"jobTaskNumber": "string",
					"lineAmount": "decimal",
					"lineDiscountAmount": "decimal",
					"lineDiscountPercent": "decimal",
					"lineNumber": "int",
					"locationCode": "string",
					"netWeight": "decimal",
					"nonstock": "boolean",
					"number": "string",
					"originallyOrderedNumber": "string",
					"originallyOrderedVarCode": "string",
					"outOfStockSubstitution": "boolean",
					"outboundWhseHandlingTime": "string",
					"outstandingAmount": "decimal",
					"outstandingAmountLcy": "decimal",
					"outstandingQtyBase": "decimal",
					"outstandingQuantity": "decimal",
					"planned": "boolean",
					"plannedDeliveryDate": "date",
					"plannedShipmentDate": "date",
					"postingDate": "date",
					"postingGroup": "string",
					"prepaymentAmount": "decimal",
					"prepaymentLine": "boolean",
					"prepaymentPercent": "decimal",
					"prepaymentTaxAreaCode": "string",
					"prepaymentTaxGroupCode": "string",
					"prepaymentTaxLiable": "boolean",
					"prepaymentVatDifference": "decimal",
					"prepaymentVatIdentifier": "string",
					"prepaymentVatPercent": "decimal",
					"prepmtAmountInvInclVat": "decimal",
					"prepmtAmountInvLcy": "decimal",
					"prepmtAmtDeducted": "decimal",
					"prepmtAmtInclVat": "decimal",
					"prepmtAmtInv": "decimal",
					"prepmtAmtToDeduct": "decimal",
					"prepmtLineAmount": "decimal",
					"prepmtVatAmountInvLcy": "decimal",
					"prepmtVatBaseAmt": "decimal",
					"prepmtVatCalcType": "prepmtVatCalcType",
					"prepmtVatDiffDeducted": "decimal",
					"prepmtVatDiffToDeduct": "decimal",
					"priceDescription": "string",
					"profitPercent": "decimal",
					"promisedDeliveryDate": "date",
					"purchOrderLineNumber": "int",
					"purchaseOrderNumber": "string",
					"purchasingCode": "string",
					"qtyAssigned": "decimal",
					"qtyInvoicedBase": "decimal",
					"qtyPerUnitOfMeasure": "decimal",
					"qtyShippedBase": "decimal",
					"qtyShippedNotInvdBase": "decimal",
					"qtyShippedNotInvoiced": "decimal",
					"qtyToAsmToOrderBase": "decimal",
					"qtyToAssembleToOrder": "decimal",
					"qtyToAssign": "decimal",
					"qtyToInvoice": "decimal",
					"qtyToInvoiceBase": "decimal",
					"qtyToShip": "decimal",
					"qtyToShipBase": "decimal",
					"quantity": "decimal",
					"quantityBase": "decimal",
					"quantityInvoiced": "decimal",
					"quantityShipped": "decimal",
					"recalculateInvoiceDisc": "boolean",
					"requestedDeliveryDate": "date",
					"reserve": "reserve",
					"reservedQtyBase": "decimal",
					"reservedQuantity": "decimal",
					"responsibilityCenter": "string",
					"retQtyRcdNotInvdBase": "decimal",
					"returnQtyRcdNotInvd": "decimal",
					"returnQtyReceived": "decimal",
					"returnQtyReceivedBase": "decimal",
					"returnQtyToReceive": "decimal",
					"returnQtyToReceiveBase": "decimal",
					"returnRcdNotInvd": "decimal",
					"returnRcdNotInvdLcy": "decimal",
					"returnReasonCode": "string",
					"returnReceiptLineNumber": "int",
					"returnReceiptNumber": "string",
					"returnsDeferralStartDate": "date",
					"sellToCustomerNumber": "string",
					"shipmentDate": "date",
					"shipmentLineNumber": "int",
					"shipmentNumber": "string",
					"shippedNotInvLcyNoVat": "decimal",
					"shippedNotInvoiced": "decimal",
					"shippedNotInvoicedLcy": "decimal",
					"shippingAgentCode": "string",
					"shippingAgentServiceCode": "string",
					"shippingTime": "string",
					"shortcutDimension1Code": "string",
					"shortcutDimension2Code": "string",
					"specialOrder": "boolean",
					"specialOrderPurchLineNumber": "int",
					"specialOrderPurchaseNumber": "string",
					"substitutionAvailable": "boolean",
					"subtype": "subtype",
					"systemCreatedEntry": "boolean",
					"taxAreaCode": "string",
					"taxCategory": "string",
					"taxGroupCode": "string",
					"taxLiable": "boolean",
					"transactionSpecification": "string",
					"transactionType": "string",
					"transportMethod": "string",
					"type": "type",
					"unitCost": "decimal",
					"unitCostLcy": "decimal",
					"unitOfMeasure": "string",
					"unitOfMeasureCode": "string",
					"unitOfMeasureCrossRef": "string",
					"unitPrice": "decimal",
					"unitVolume": "decimal",
					"unitsPerParcel": "decimal",
					"useDuplicationList": "boolean",
					"variantCode": "string",
					"vatBaseAmount": "decimal",
					"vatBusPostingGroup": "string",
					"vatCalculationType": "vatCalculationType",
					"vatClauseCode": "string",
					"vatDifference": "decimal",
					"vatIdentifier": "string",
					"vatPercent": "decimal",
					"vatProdPostingGroup": "string",
					"whseOutstandingQty": "decimal",
					"whseOutstandingQtyBase": "decimal",
					"workTypeCode": "string"
				},
				"salesDocumentLines_Fields": "string",
				"salesDocumentLines_Filter": {
					"Criteria": "string",
					"Field": "salesDocumentLines_Fields"
				},
				"salesDocumentLines_List": {
					"salesDocumentLines": "salesDocumentLines"
				},
				"subtype": "string",
				"type": "string",
				"vatCalculationType": "string"
			}
		},
		"salesDocuments": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"Delete_workflowSalesDocumentLines": {
					"i": "Delete_workflowSalesDocumentLines",
					"o": "Delete_workflowSalesDocumentLines_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"salesDocuments": "salesDocuments"
				},
				"CreateMultiple": {
					"salesDocuments_List": "salesDocuments_List"
				},
				"CreateMultiple_Result": {
					"salesDocuments_List": "salesDocuments_List"
				},
				"Create_Result": {
					"salesDocuments": "salesDocuments"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Delete_workflowSalesDocumentLines": {
					"workflowSalesDocumentLines_Key": "string"
				},
				"Delete_workflowSalesDocumentLines_Result": {
					"Delete_workflowSalesDocumentLines_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"documentType": "string",
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"salesDocuments": "salesDocuments"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "salesDocuments_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "salesDocuments_List"
				},
				"Read_Result": {
					"salesDocuments": "salesDocuments"
				},
				"Sales_Document_Line_Entity": {
					"Key": "string",
					"allowInvoiceDisc": "boolean",
					"allowItemChargeAssignment": "boolean",
					"allowLineDisc": "boolean",
					"amount": "decimal",
					"amountIncludingVat": "decimal",
					"applFromItemEntry": "int",
					"applToItemEntry": "int",
					"area": "string",
					"atoWhseOutstandingQty": "decimal",
					"atoWhseOutstdQtyBase": "decimal",
					"attachedToLineNumber": "int",
					"billToCustomerNumber": "string",
					"binCode": "string",
					"blanketOrderLineNumber": "int",
					"blanketOrderNumber": "string",
					"bomItemNumber": "string",
					"completelyShipped": "boolean",
					"crossReferenceNumber": "string",
					"crossReferenceType": "crossReferenceType",
					"crossReferenceTypeNumber": "string",
					"currencyCode": "string",
					"customerDiscGroup": "string",
					"customerPriceGroup": "string",
					"deferralCode": "string",
					"deprUntilFaPostingDate": "boolean",
					"depreciationBookCode": "string",
					"description": "string",
					"description2": "string",
					"dimensionSetId": "int",
					"documentNumber": "string",
					"documentType": "documentType_1",
					"dropShipment": "boolean",
					"duplicateInDepreciationBook": "string",
					"exitPoint": "string",
					"faPostingDate": "date",
					"genBusPostingGroup": "string",
					"genProdPostingGroup": "string",
					"grossWeight": "decimal",
					"icPartnerCode": "string",
					"icPartnerRefType": "icPartnerRefType",
					"icPartnerReference": "string",
					"invDiscAmountToInvoice": "decimal",
					"invDiscountAmount": "decimal",
					"itemCategoryCode": "string",
					"jobContractEntryNumber": "int",
					"jobNumber": "string",
					"jobTaskNumber": "string",
					"lineAmount": "decimal",
					"lineDiscountAmount": "decimal",
					"lineDiscountPercent": "decimal",
					"lineNumber": "int",
					"locationCode": "string",
					"netWeight": "decimal",
					"nonstock": "boolean",
					"number": "string",
					"originallyOrderedNumber": "string",
					"originallyOrderedVarCode": "string",
					"outOfStockSubstitution": "boolean",
					"outboundWhseHandlingTime": "string",
					"outstandingAmount": "decimal",
					"outstandingAmountLcy": "decimal",
					"outstandingQtyBase": "decimal",
					"outstandingQuantity": "decimal",
					"planned": "boolean",
					"plannedDeliveryDate": "date",
					"plannedShipmentDate": "date",
					"postingDate": "date",
					"postingGroup": "string",
					"prepaymentAmount": "decimal",
					"prepaymentLine": "boolean",
					"prepaymentPercent": "decimal",
					"prepaymentTaxAreaCode": "string",
					"prepaymentTaxGroupCode": "string",
					"prepaymentTaxLiable": "boolean",
					"prepaymentVatDifference": "decimal",
					"prepaymentVatIdentifier": "string",
					"prepaymentVatPercent": "decimal",
					"prepmtAmountInvInclVat": "decimal",
					"prepmtAmountInvLcy": "decimal",
					"prepmtAmtDeducted": "decimal",
					"prepmtAmtInclVat": "decimal",
					"prepmtAmtInv": "decimal",
					"prepmtAmtToDeduct": "decimal",
					"prepmtLineAmount": "decimal",
					"prepmtVatAmountInvLcy": "decimal",
					"prepmtVatBaseAmt": "decimal",
					"prepmtVatCalcType": "prepmtVatCalcType",
					"prepmtVatDiffDeducted": "decimal",
					"prepmtVatDiffToDeduct": "decimal",
					"priceDescription": "string",
					"profitPercent": "decimal",
					"promisedDeliveryDate": "date",
					"purchOrderLineNumber": "int",
					"purchaseOrderNumber": "string",
					"purchasingCode": "string",
					"qtyAssigned": "decimal",
					"qtyInvoicedBase": "decimal",
					"qtyPerUnitOfMeasure": "decimal",
					"qtyShippedBase": "decimal",
					"qtyShippedNotInvdBase": "decimal",
					"qtyShippedNotInvoiced": "decimal",
					"qtyToAsmToOrderBase": "decimal",
					"qtyToAssembleToOrder": "decimal",
					"qtyToAssign": "decimal",
					"qtyToInvoice": "decimal",
					"qtyToInvoiceBase": "decimal",
					"qtyToShip": "decimal",
					"qtyToShipBase": "decimal",
					"quantity": "decimal",
					"quantityBase": "decimal",
					"quantityInvoiced": "decimal",
					"quantityShipped": "decimal",
					"recalculateInvoiceDisc": "boolean",
					"requestedDeliveryDate": "date",
					"reserve": "reserve_96",
					"reservedQtyBase": "decimal",
					"reservedQuantity": "decimal",
					"responsibilityCenter": "string",
					"retQtyRcdNotInvdBase": "decimal",
					"returnQtyRcdNotInvd": "decimal",
					"returnQtyReceived": "decimal",
					"returnQtyReceivedBase": "decimal",
					"returnQtyToReceive": "decimal",
					"returnQtyToReceiveBase": "decimal",
					"returnRcdNotInvd": "decimal",
					"returnRcdNotInvdLcy": "decimal",
					"returnReasonCode": "string",
					"returnReceiptLineNumber": "int",
					"returnReceiptNumber": "string",
					"returnsDeferralStartDate": "date",
					"sellToCustomerNumber": "string",
					"shipmentDate": "date",
					"shipmentLineNumber": "int",
					"shipmentNumber": "string",
					"shippedNotInvLcyNoVat": "decimal",
					"shippedNotInvoiced": "decimal",
					"shippedNotInvoicedLcy": "decimal",
					"shippingAgentCode": "string",
					"shippingAgentServiceCode": "string",
					"shippingTime": "string",
					"shortcutDimension1Code": "string",
					"shortcutDimension2Code": "string",
					"specialOrder": "boolean",
					"specialOrderPurchLineNumber": "int",
					"specialOrderPurchaseNumber": "string",
					"substitutionAvailable": "boolean",
					"subtype": "subtype",
					"systemCreatedEntry": "boolean",
					"taxAreaCode": "string",
					"taxCategory": "string",
					"taxGroupCode": "string",
					"taxLiable": "boolean",
					"transactionSpecification": "string",
					"transactionType": "string",
					"transportMethod": "string",
					"type": "type",
					"unitCost": "decimal",
					"unitCostLcy": "decimal",
					"unitOfMeasure": "string",
					"unitOfMeasureCode": "string",
					"unitOfMeasureCrossRef": "string",
					"unitPrice": "decimal",
					"unitVolume": "decimal",
					"unitsPerParcel": "decimal",
					"useDuplicationList": "boolean",
					"variantCode": "string",
					"vatBaseAmount": "decimal",
					"vatBusPostingGroup": "string",
					"vatCalculationType": "vatCalculationType",
					"vatClauseCode": "string",
					"vatDifference": "decimal",
					"vatIdentifier": "string",
					"vatPercent": "decimal",
					"vatProdPostingGroup": "string",
					"whseOutstandingQty": "decimal",
					"whseOutstandingQtyBase": "decimal",
					"workTypeCode": "string"
				},
				"Sales_Document_Line_Entity_List": {
					"Sales_Document_Line_Entity": "Sales_Document_Line_Entity"
				},
				"Update": {
					"salesDocuments": "salesDocuments"
				},
				"UpdateMultiple": {
					"salesDocuments_List": "salesDocuments_List"
				},
				"UpdateMultiple_Result": {
					"salesDocuments_List": "salesDocuments_List"
				},
				"Update_Result": {
					"salesDocuments": "salesDocuments"
				},
				"appliesToDocType": "string",
				"balAccountType": "string",
				"crossReferenceType": "string",
				"documentType": "string",
				"documentType_1": "string",
				"icDirection": "string",
				"icPartnerRefType": "string",
				"icStatus": "string",
				"invoiceDiscountCalculation": "string",
				"jobQueueStatus": "string",
				"prepmtVatCalcType": "string",
				"reserve": "string",
				"reserve_96": "string",
				"salesDocuments": {
					"Key": "string",
					"allowLineDisc": "boolean",
					"amount": "decimal",
					"amountIncludingVAT": "decimal",
					"amountShippedNotInvoiced": "decimal",
					"amountShippedNotInvoicedInclVat": "decimal",
					"appliesToDocNumber": "string",
					"appliesToDocType": "appliesToDocType",
					"appliesToId": "string",
					"area": "string",
					"assignedUserId": "string",
					"balAccountNumber": "string",
					"balAccountType": "balAccountType",
					"billToAddress": "string",
					"billToAddress2": "string",
					"billToCity": "string",
					"billToContact": "string",
					"billToContactNumber": "string",
					"billToCountryRegionCode": "string",
					"billToCounty": "string",
					"billToCustomerNumber": "string",
					"billToCustomerTemplateCode": "string",
					"billToIcPartnerCode": "string",
					"billToName": "string",
					"billToName2": "string",
					"billToPostCode": "string",
					"campaignNumber": "string",
					"combineShipments": "boolean",
					"comment": "boolean",
					"completelyShipped": "boolean",
					"compressPrepayment": "boolean",
					"correction": "boolean",
					"currencyCode": "string",
					"currencyFactor": "decimal",
					"customerDiscGroup": "string",
					"customerPostingGroup": "string",
					"customerPriceGroup": "string",
					"dimensionSetId": "int",
					"directDebitMandateId": "string",
					"docNumberOccurrence": "int",
					"documentDate": "date",
					"documentType": "documentType",
					"dueDate": "date",
					"eu3PartyTrade": "boolean",
					"exitPoint": "string",
					"externalDocumentNumber": "string",
					"genBusPostingGroup": "string",
					"getShipmentUsed": "boolean",
					"icDirection": "icDirection",
					"icStatus": "icStatus",
					"id": "string",
					"incomingDocumentEntryNumber": "int",
					"invoice": "boolean",
					"invoiceDiscCode": "string",
					"invoiceDiscountAmount": "decimal",
					"invoiceDiscountCalculation": "invoiceDiscountCalculation",
					"invoiceDiscountValue": "decimal",
					"jobQueueEntryId": "string",
					"jobQueueStatus": "jobQueueStatus",
					"languageCode": "string",
					"lastPostingNumber": "string",
					"lastPremtCrMemoNumber": "string",
					"lastPrepaymentNumber": "string",
					"lastReturnReceiptNumber": "string",
					"lastShippingNumber": "string",
					"lateOrderShipping": "boolean",
					"locationCode": "string",
					"locationFilter": "string",
					"number": "string",
					"numberOfArchivedVersions": "int",
					"numberPrinted": "int",
					"numberSeries": "string",
					"onHold": "string",
					"opportunityNumber": "string",
					"orderClass": "string",
					"orderDate": "date",
					"outboundWhseHandlingTime": "string",
					"packageTrackingNumber": "string",
					"paymentDiscountPercent": "decimal",
					"paymentMethodCode": "string",
					"paymentServiceSetId": "int",
					"paymentTermsCode": "string",
					"pmtDiscountDate": "date",
					"postingDate": "date",
					"postingDescription": "string",
					"postingFromWhseRef": "int",
					"postingNumber": "string",
					"postingNumberSeries": "string",
					"premptCrMemoNumber": "string",
					"prepaymentDueDate": "date",
					"prepaymentNumber": "string",
					"prepaymentNumberSeries": "string",
					"prepaymentPercent": "decimal",
					"prepmtCrMemoNumberSeries": "string",
					"prepmtPaymentDiscountPercent": "decimal",
					"prepmtPaymentTermsCode": "string",
					"prepmtPmtDiscountDate": "date",
					"prepmtPostingDescription": "string",
					"pricesIncludingVat": "boolean",
					"printPostedDocuments": "boolean",
					"promisedDeliveryDate": "date",
					"quoteAccepted": "boolean",
					"quoteAcceptedDate": "date",
					"quoteNumber": "string",
					"quoteSentToCustomer": "dateTime",
					"quoteValidUntilDate": "date",
					"reasonCode": "string",
					"recalculateInvoiceDisc": "boolean",
					"receive": "boolean",
					"requestedDeliveryDate": "date",
					"reserve": "reserve",
					"responsibilityCenter": "string",
					"returnReceiptNumber": "string",
					"returnReceiptNumberSeries": "string",
					"salespersonCode": "string",
					"sellToAddress": "string",
					"sellToAddress2": "string",
					"sellToCity": "string",
					"sellToContact": "string",
					"sellToContactNumber": "string",
					"sellToCountryRegionCode": "string",
					"sellToCounty": "string",
					"sellToCustomerName": "string",
					"sellToCustomerName2": "string",
					"sellToCustomerNumber": "string",
					"sellToCustomerTemplateCode": "string",
					"sellToIcPartnerCode": "string",
					"sellToPostCode": "string",
					"sendIcDocument": "boolean",
					"ship": "boolean",
					"shipToAddress": "string",
					"shipToAddress2": "string",
					"shipToCity": "string",
					"shipToCode": "string",
					"shipToContact": "string",
					"shipToCountryRegionCode": "string",
					"shipToCounty": "string",
					"shipToName": "string",
					"shipToName2": "string",
					"shipToPostCode": "string",
					"shipmentDate": "date",
					"shipmentMethodCode": "string",
					"shipped": "boolean",
					"shippedNotInvoiced": "boolean",
					"shippingAdvice": "shippingAdvice",
					"shippingAgentCode": "string",
					"shippingAgentServiceCode": "string",
					"shippingNumber": "string",
					"shippingNumberSeries": "string",
					"shippingTime": "string",
					"shortcutDimension1Code": "string",
					"shortcutDimension2Code": "string",
					"status": "status",
					"taxAreaCode": "string",
					"taxLiable": "boolean",
					"transactionSpecification": "string",
					"transactionType": "string",
					"transportMethod": "string",
					"vatBaseDiscountPercent": "decimal",
					"vatBusPostingGroup": "string",
					"vatCountryRegionCode": "string",
					"vatRegistrationNumber": "string",
					"workflowSalesDocumentLines": "Sales_Document_Line_Entity_List",
					"yourReference": "string"
				},
				"salesDocuments_Fields": "string",
				"salesDocuments_Filter": {
					"Criteria": "string",
					"Field": "salesDocuments_Fields"
				},
				"salesDocuments_List": {
					"salesDocuments": "salesDocuments"
				},
				"shippingAdvice": "string",
				"status": "string",
				"subtype": "string",
				"type": "string",
				"vatCalculationType": "string"
			}
		},
		"workflowCustomers": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"workflowCustomers": "workflowCustomers"
				},
				"CreateMultiple": {
					"workflowCustomers_List": "workflowCustomers_List"
				},
				"CreateMultiple_Result": {
					"workflowCustomers_List": "workflowCustomers_List"
				},
				"Create_Result": {
					"workflowCustomers": "workflowCustomers"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"workflowCustomers": "workflowCustomers"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "workflowCustomers_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "workflowCustomers_List"
				},
				"Read_Result": {
					"workflowCustomers": "workflowCustomers"
				},
				"Update": {
					"workflowCustomers": "workflowCustomers"
				},
				"UpdateMultiple": {
					"workflowCustomers_List": "workflowCustomers_List"
				},
				"UpdateMultiple_Result": {
					"workflowCustomers_List": "workflowCustomers_List"
				},
				"Update_Result": {
					"workflowCustomers": "workflowCustomers"
				},
				"applicationMethod": "string",
				"blocked": "string",
				"contactType": "string",
				"copySellToAddrToQteFrom": "string",
				"partnerType": "string",
				"reserve": "string",
				"shippingAdvice": "string",
				"workflowCustomers": {
					"Key": "string",
					"address": "string",
					"address2": "string",
					"allowLineDisc": "boolean",
					"amount": "decimal",
					"applicationMethod": "applicationMethod",
					"balance": "decimal",
					"balanceDue": "decimal",
					"balanceDueLcy": "decimal",
					"balanceLcy": "decimal",
					"baseCalendarCode": "string",
					"billToCustomerNumber": "string",
					"billToNumberOfArchivedDoc": "int",
					"billToNumberOfBlanketOrders": "int",
					"billToNumberOfCreditMemos": "int",
					"billToNumberOfInvoices": "int",
					"billToNumberOfOrders": "int",
					"billToNumberOfPstdCrMemos": "int",
					"billToNumberOfPstdInvoices": "int",
					"billToNumberOfPstdReturnR": "int",
					"billToNumberOfPstdShipments": "int",
					"billToNumberOfQuotes": "int",
					"billToNumberOfReturnOrders": "int",
					"blockPaymentTolerance": "boolean",
					"blocked": "blocked",
					"budgetedAmount": "decimal",
					"cashFlowPaymentTermsCode": "string",
					"chainName": "string",
					"city": "string",
					"collectionMethod": "string",
					"combineShipments": "boolean",
					"comment": "boolean",
					"contact": "string",
					"contactId": "string",
					"contactType": "contactType",
					"contractGainLossAmount": "decimal",
					"copySellToAddrToQteFrom": "copySellToAddrToQteFrom",
					"countryRegionCode": "string",
					"county": "string",
					"crMemoAmounts": "decimal",
					"crMemoAmountsLcy": "decimal",
					"creditAmount": "decimal",
					"creditAmountLcy": "decimal",
					"creditLimitLcy": "decimal",
					"currencyCode": "string",
					"currencyFilter": "string",
					"currencyId": "string",
					"customerDiscGroup": "string",
					"customerPostingGroup": "string",
					"customerPriceGroup": "string",
					"dateFilter": "date",
					"debitAmount": "decimal",
					"debitAmountLcy": "decimal",
					"documentSendingProfile": "string",
					"eMail": "string",
					"faxNumber": "string",
					"finChargeMemoAmountsLcy": "decimal",
					"finChargeTermsCode": "string",
					"financeChargeMemoAmounts": "decimal",
					"genBusPostingGroup": "string",
					"gln": "string",
					"globalDimension1Code": "string",
					"globalDimension1Filter": "string",
					"globalDimension2Code": "string",
					"globalDimension2Filter": "string",
					"homePage": "string",
					"icPartnerCode": "string",
					"id": "string",
					"invAmountsLcy": "decimal",
					"invDiscountsLcy": "decimal",
					"invoiceAmounts": "decimal",
					"invoiceCopies": "int",
					"invoiceDiscCode": "string",
					"languageCode": "string",
					"lastDateModified": "date",
					"lastModifiedDateTime": "dateTime",
					"lastStatementNumber": "int",
					"locationCode": "string",
					"name": "string",
					"name2": "string",
					"netChange": "decimal",
					"netChangeLcy": "decimal",
					"number": "string",
					"numberOfBlanketOrders": "int",
					"numberOfCreditMemos": "int",
					"numberOfInvoices": "int",
					"numberOfOrders": "int",
					"numberOfPstdCreditMemos": "int",
					"numberOfPstdInvoices": "int",
					"numberOfPstdReturnReceipts": "int",
					"numberOfPstdShipments": "int",
					"numberOfQuotes": "int",
					"numberOfReturnOrders": "int",
					"numberOfShipToAddresses": "int",
					"numberSeries": "string",
					"otherAmounts": "decimal",
					"otherAmountsLcy": "decimal",
					"ourAccountNumber": "string",
					"outstandingInvoices": "decimal",
					"outstandingInvoicesLcy": "decimal",
					"outstandingOrders": "decimal",
					"outstandingOrdersLcy": "decimal",
					"outstandingServInvoicesLcy": "decimal",
					"outstandingServOrdersLcy": "decimal",
					"partnerType": "partnerType",
					"paymentMethodCode": "string",
					"paymentMethodId": "string",
					"paymentTermsCode": "string",
					"paymentTermsId": "string",
					"payments": "decimal",
					"paymentsLcy": "decimal",
					"phoneNumber": "string",
					"placeOfExport": "string",
					"pmtDiscToleranceLcy": "decimal",
					"pmtDiscountsLcy": "decimal",
					"pmtToleranceLcy": "decimal",
					"postCode": "string",
					"preferredBankAccountCode": "string",
					"prepaymentPercent": "decimal",
					"pricesIncludingVat": "boolean",
					"primaryContactNumber": "string",
					"printStatements": "boolean",
					"priority": "int",
					"profitLcy": "decimal",
					"refunds": "decimal",
					"refundsLcy": "decimal",
					"reminderAmounts": "decimal",
					"reminderAmountsLcy": "decimal",
					"reminderTermsCode": "string",
					"reserve": "reserve",
					"responsibilityCenter": "string",
					"salesLcy": "decimal",
					"salespersonCode": "string",
					"searchName": "string",
					"sellToNumberOfArchivedDoc": "int",
					"servShippedNotInvoicedLcy": "decimal",
					"serviceZoneCode": "string",
					"shipToFilter": "string",
					"shipmentMethodCode": "string",
					"shipmentMethodId": "string",
					"shippedNotInvoiced": "decimal",
					"shippedNotInvoicedLcy": "decimal",
					"shippingAdvice": "shippingAdvice",
					"shippingAgentCode": "string",
					"shippingAgentServiceCode": "string",
					"shippingTime": "string",
					"statisticsGroup": "int",
					"taxAreaCode": "string",
					"taxAreaId": "string",
					"taxLiable": "boolean",
					"telexAnswerBack": "string",
					"telexNumber": "string",
					"territoryCode": "string",
					"validateEuVatRegNumber": "boolean",
					"vatBusPostingGroup": "string",
					"vatRegistrationNumber": "string"
				},
				"workflowCustomers_Fields": "string",
				"workflowCustomers_Filter": {
					"Criteria": "string",
					"Field": "workflowCustomers_Fields"
				},
				"workflowCustomers_List": {
					"workflowCustomers": "workflowCustomers"
				}
			}
		},
		"workflowGenJournalBatches": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"workflowGenJournalBatches": "workflowGenJournalBatches"
				},
				"CreateMultiple": {
					"workflowGenJournalBatches_List": "workflowGenJournalBatches_List"
				},
				"CreateMultiple_Result": {
					"workflowGenJournalBatches_List": "workflowGenJournalBatches_List"
				},
				"Create_Result": {
					"workflowGenJournalBatches": "workflowGenJournalBatches"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"journalTemplateName": "string",
					"name": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"workflowGenJournalBatches": "workflowGenJournalBatches"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "workflowGenJournalBatches_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "workflowGenJournalBatches_List"
				},
				"Read_Result": {
					"workflowGenJournalBatches": "workflowGenJournalBatches"
				},
				"Update": {
					"workflowGenJournalBatches": "workflowGenJournalBatches"
				},
				"UpdateMultiple": {
					"workflowGenJournalBatches_List": "workflowGenJournalBatches_List"
				},
				"UpdateMultiple_Result": {
					"workflowGenJournalBatches_List": "workflowGenJournalBatches_List"
				},
				"Update_Result": {
					"workflowGenJournalBatches": "workflowGenJournalBatches"
				},
				"balAccountType": "string",
				"templateType": "string",
				"workflowGenJournalBatches": {
					"Key": "string",
					"allowPaymentExport": "boolean",
					"allowVatDifference": "boolean",
					"balAccountNumber": "string",
					"balAccountType": "balAccountType",
					"bankStatementImportFormat": "string",
					"copyVatSetupToJnlLines": "boolean",
					"description": "string",
					"id": "string",
					"journalTemplateName": "string",
					"lastModifiedDatetime": "dateTime",
					"name": "string",
					"numberSeries": "string",
					"postingNumberSeries": "string",
					"reasonCode": "string",
					"recurring": "boolean",
					"suggestBalancingAmount": "boolean",
					"templateType": "templateType"
				},
				"workflowGenJournalBatches_Fields": "string",
				"workflowGenJournalBatches_Filter": {
					"Criteria": "string",
					"Field": "workflowGenJournalBatches_Fields"
				},
				"workflowGenJournalBatches_List": {
					"workflowGenJournalBatches": "workflowGenJournalBatches"
				}
			}
		},
		"workflowGenJournalLines": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"workflowGenJournalLines": "workflowGenJournalLines"
				},
				"CreateMultiple": {
					"workflowGenJournalLines_List": "workflowGenJournalLines_List"
				},
				"CreateMultiple_Result": {
					"workflowGenJournalLines_List": "workflowGenJournalLines_List"
				},
				"Create_Result": {
					"workflowGenJournalLines": "workflowGenJournalLines"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"journalBatchName": "string",
					"journalTemplateName": "string",
					"lineNumber": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"workflowGenJournalLines": "workflowGenJournalLines"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "workflowGenJournalLines_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "workflowGenJournalLines_List"
				},
				"Read_Result": {
					"workflowGenJournalLines": "workflowGenJournalLines"
				},
				"Update": {
					"workflowGenJournalLines": "workflowGenJournalLines"
				},
				"UpdateMultiple": {
					"workflowGenJournalLines_List": "workflowGenJournalLines_List"
				},
				"UpdateMultiple_Result": {
					"workflowGenJournalLines_List": "workflowGenJournalLines_List"
				},
				"Update_Result": {
					"workflowGenJournalLines": "workflowGenJournalLines"
				},
				"accountType": "string",
				"additionalCurrencyPosting": "string",
				"appliesToDocType": "string",
				"balAccountType": "string",
				"balGenPostingType": "string",
				"balVatCalculationType": "string",
				"bankPaymentType": "string",
				"documentType": "string",
				"faPostingType": "string",
				"genPostingType": "string",
				"icDirection": "string",
				"jobLineType": "string",
				"recurringMethod": "string",
				"sourceType": "string",
				"vatCalculationType": "string",
				"vatPosting": "string",
				"workflowGenJournalLines": {
					"Key": "string",
					"accountId": "string",
					"accountNumber": "string",
					"accountType": "accountType",
					"additionalCurrencyPosting": "additionalCurrencyPosting",
					"allocatedAmtLcy": "decimal",
					"allowApplication": "boolean",
					"allowZeroAmountPosting": "boolean",
					"amount": "decimal",
					"amountLcy": "decimal",
					"appliedAutomatically": "boolean",
					"appliesToDocNumber": "string",
					"appliesToDocType": "appliesToDocType",
					"appliesToExtDocNumber": "string",
					"appliesToId": "string",
					"appliesToInvoiceId": "string",
					"balAccountNumber": "string",
					"balAccountType": "balAccountType",
					"balGenBusPostingGroup": "string",
					"balGenPostingType": "balGenPostingType",
					"balGenProdPostingGroup": "string",
					"balTaxAreaCode": "string",
					"balTaxGroupCode": "string",
					"balTaxLiable": "boolean",
					"balUseTax": "boolean",
					"balVatAmount": "decimal",
					"balVatAmountLcy": "decimal",
					"balVatBaseAmount": "decimal",
					"balVatBaseAmountLcy": "decimal",
					"balVatBusPostingGroup": "string",
					"balVatCalculationType": "balVatCalculationType",
					"balVatDifference": "decimal",
					"balVatPercent": "decimal",
					"balVatProdPostingGroup": "string",
					"balanceLcy": "decimal",
					"bankPaymentType": "bankPaymentType",
					"billToPayToNumber": "string",
					"budgetedFaNumber": "string",
					"businessUnitCode": "string",
					"campaignNumber": "string",
					"checkExported": "boolean",
					"checkPrinted": "boolean",
					"checkTransmitted": "boolean",
					"comment": "string",
					"contactGraphId": "string",
					"correction": "boolean",
					"countryRegionCode": "string",
					"creditAmount": "decimal",
					"creditorNumber": "string",
					"currencyCode": "string",
					"currencyFactor": "decimal",
					"customerId": "string",
					"dataExchEntryNumber": "int",
					"dataExchLineNumber": "int",
					"debitAmount": "decimal",
					"deferralCode": "string",
					"deferralLineNumber": "int",
					"deprAcquisitionCost": "boolean",
					"deprUntilFaPostingDate": "boolean",
					"depreciationBookCode": "string",
					"description": "string",
					"dimensionSetId": "int",
					"directDebitMandateId": "string",
					"documentDate": "date",
					"documentNumber": "string",
					"documentType": "documentType",
					"dueDate": "date",
					"duplicateInDepreciationBook": "string",
					"eu3PartyTrade": "boolean",
					"expirationDate": "date",
					"exportedToPaymentFile": "boolean",
					"externalDocumentNumber": "string",
					"faAddCurrencyFactor": "decimal",
					"faErrorEntryNumber": "int",
					"faPostingDate": "date",
					"faPostingType": "faPostingType",
					"faReclassificationEntry": "boolean",
					"financialVoid": "boolean",
					"genBusPostingGroup": "string",
					"genPostingType": "genPostingType",
					"genProdPostingGroup": "string",
					"hasPaymentExportError": "boolean",
					"icDirection": "icDirection",
					"icPartnerCode": "string",
					"icPartnerGLAccNumber": "string",
					"icPartnerTransactionNumber": "int",
					"id": "string",
					"incomingDocumentEntryNumber": "int",
					"indexEntry": "boolean",
					"insuranceNumber": "string",
					"invDiscountLcy": "decimal",
					"jobCurrencyCode": "string",
					"jobCurrencyFactor": "decimal",
					"jobLineAmount": "decimal",
					"jobLineAmountLcy": "decimal",
					"jobLineDiscAmountLcy": "decimal",
					"jobLineDiscountAmount": "decimal",
					"jobLineDiscountPercent": "decimal",
					"jobLineType": "jobLineType",
					"jobNumber": "string",
					"jobPlanningLineNumber": "int",
					"jobQuantity": "decimal",
					"jobRemainingQty": "decimal",
					"jobTaskNumber": "string",
					"jobTotalCost": "decimal",
					"jobTotalCostLcy": "decimal",
					"jobTotalPrice": "decimal",
					"jobTotalPriceLcy": "decimal",
					"jobUnitCost": "decimal",
					"jobUnitCostLcy": "decimal",
					"jobUnitOfMeasureCode": "string",
					"jobUnitPrice": "decimal",
					"jobUnitPriceLcy": "decimal",
					"journalBatchId": "string",
					"journalBatchName": "string",
					"journalTemplateName": "string",
					"lastModifiedDatetime": "dateTime",
					"lineNumber": "int",
					"maintenanceCode": "string",
					"messageToRecipient": "string",
					"numberOfDepreciationDays": "int",
					"onHold": "string",
					"payerInformation": "string",
					"paymentDiscountPercent": "decimal",
					"paymentMethodCode": "string",
					"paymentReference": "string",
					"paymentTermsCode": "string",
					"pmtDiscountDate": "date",
					"postingDate": "date",
					"postingGroup": "string",
					"postingNumberSeries": "string",
					"prepayment": "boolean",
					"prodOrderNumber": "string",
					"profitLcy": "decimal",
					"quantity": "decimal",
					"reasonCode": "string",
					"recipientBankAccount": "string",
					"recurringFrequency": "string",
					"recurringMethod": "recurringMethod",
					"reversingEntry": "boolean",
					"salesPurchLcy": "decimal",
					"salespersPurchCode": "string",
					"salvageValue": "decimal",
					"sellToBuyFromNumber": "string",
					"shipToOrderAddressCode": "string",
					"shortcutDimension1Code": "string",
					"shortcutDimension2Code": "string",
					"sourceCode": "string",
					"sourceCurrVatAmount": "decimal",
					"sourceCurrVatBaseAmount": "decimal",
					"sourceCurrencyAmount": "decimal",
					"sourceCurrencyCode": "string",
					"sourceLineNumber": "int",
					"sourceNumber": "string",
					"sourceType": "sourceType",
					"systemCreatedEntry": "boolean",
					"taxAreaCode": "string",
					"taxGroupCode": "string",
					"taxLiable": "boolean",
					"transactionInformation": "string",
					"useDuplicationList": "boolean",
					"useTax": "boolean",
					"vatAmount": "decimal",
					"vatAmountLcy": "decimal",
					"vatBaseAmount": "decimal",
					"vatBaseAmountLcy": "decimal",
					"vatBaseDiscountPercent": "decimal",
					"vatBusPostingGroup": "string",
					"vatCalculationType": "vatCalculationType",
					"vatDifference": "decimal",
					"vatPercent": "decimal",
					"vatPosting": "vatPosting",
					"vatProdPostingGroup": "string",
					"vatRegistrationNumber": "string"
				},
				"workflowGenJournalLines_Fields": "string",
				"workflowGenJournalLines_Filter": {
					"Criteria": "string",
					"Field": "workflowGenJournalLines_Fields"
				},
				"workflowGenJournalLines_List": {
					"workflowGenJournalLines": "workflowGenJournalLines"
				}
			}
		},
		"workflowItems": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"workflowItems": "workflowItems"
				},
				"CreateMultiple": {
					"workflowItems_List": "workflowItems_List"
				},
				"CreateMultiple_Result": {
					"workflowItems_List": "workflowItems_List"
				},
				"Create_Result": {
					"workflowItems": "workflowItems"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"workflowItems": "workflowItems"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "workflowItems_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "workflowItems_List"
				},
				"Read_Result": {
					"workflowItems": "workflowItems"
				},
				"Update": {
					"workflowItems": "workflowItems"
				},
				"UpdateMultiple": {
					"workflowItems_List": "workflowItems_List"
				},
				"UpdateMultiple_Result": {
					"workflowItems_List": "workflowItems_List"
				},
				"Update_Result": {
					"workflowItems": "workflowItems"
				},
				"assemblyPolicy": "string",
				"costingMethod": "string",
				"flushingMethod": "string",
				"manufacturingPolicy": "string",
				"orderTrackingPolicy": "string",
				"preventNegativeInventory": "string",
				"priceProfitCalculation": "string",
				"reorderingPolicy": "string",
				"replenishmentSystem": "string",
				"reserve": "string",
				"stockoutWarning": "string",
				"type": "string",
				"workflowItems": {
					"Key": "string",
					"allowInvoiceDisc": "boolean",
					"allowOnlineAdjustment": "boolean",
					"alternativeItemNumber": "string",
					"applicationWkshUserId": "string",
					"assemblyBom": "boolean",
					"assemblyPolicy": "assemblyPolicy",
					"automaticExtTexts": "boolean",
					"baseUnitOfMeasure": "string",
					"binFilter": "string",
					"blockReason": "string",
					"blocked": "boolean",
					"budgetProfit": "decimal",
					"budgetQuantity": "decimal",
					"budgetedAmount": "decimal",
					"cogsLcy": "decimal",
					"comment": "boolean",
					"commissionGroup": "int",
					"commonItemNumber": "string",
					"componentForecast": "boolean",
					"costIsAdjusted": "boolean",
					"costIsPostedToGL": "boolean",
					"costOfOpenProductionOrders": "decimal",
					"costingMethod": "costingMethod",
					"countryRegionOfOriginCode": "string",
					"countryRegionPurchasedCode": "string",
					"createdFromNonstockItem": "boolean",
					"critical": "boolean",
					"dampenerPeriod": "string",
					"dampenerQuantity": "decimal",
					"dateFilter": "date",
					"defaultDeferralTemplateCode": "string",
					"description": "string",
					"description2": "string",
					"discreteOrderQuantity": "int",
					"dropShipmentFilter": "boolean",
					"durability": "string",
					"dutyCode": "string",
					"dutyDuePercent": "decimal",
					"dutyUnitConversion": "decimal",
					"expirationCalculation": "string",
					"flushingMethod": "flushingMethod",
					"fpOrderReceiptQty": "decimal",
					"freightType": "string",
					"genProdPostingGroup": "string",
					"globalDimension1Code": "string",
					"globalDimension1Filter": "string",
					"globalDimension2Code": "string",
					"globalDimension2Filter": "string",
					"grossWeight": "decimal",
					"gtin": "string",
					"id": "string",
					"identifierCode": "string",
					"includeInventory": "boolean",
					"indirectCostPercent": "decimal",
					"inventory": "decimal",
					"inventoryPostingGroup": "string",
					"inventoryValueZero": "boolean",
					"itemCategoryCode": "string",
					"itemDiscGroup": "string",
					"itemTrackingCode": "string",
					"lastCountingPeriodUpdate": "date",
					"lastDateModified": "date",
					"lastDatetimeModified": "dateTime",
					"lastDirectCost": "decimal",
					"lastPhysInvtDate": "date",
					"lastTimeModified": "time",
					"lastUnitCostCalcDate": "date",
					"leadTimeCalculation": "string",
					"locationFilter": "string",
					"lotAccumulationPeriod": "string",
					"lotNos": "string",
					"lotNumberFilter": "string",
					"lotSize": "decimal",
					"lowLevelCode": "int",
					"manufacturerCode": "string",
					"manufacturingPolicy": "manufacturingPolicy",
					"maximumInventory": "decimal",
					"maximumOrderQuantity": "decimal",
					"minimumOrderQuantity": "decimal",
					"negativeAdjmtLcy": "decimal",
					"negativeAdjmtQty": "decimal",
					"netChange": "decimal",
					"netInvoicedQty": "decimal",
					"netWeight": "decimal",
					"nextCountingEndDate": "date",
					"nextCountingStartDate": "date",
					"number": "string",
					"number2": "string",
					"numberOfSubstitutes": "int",
					"numberSeries": "string",
					"orderMultiple": "decimal",
					"orderTrackingPolicy": "orderTrackingPolicy",
					"overflowLevel": "decimal",
					"overheadRate": "decimal",
					"physInvtCountingPeriodCode": "string",
					"plannedOrderReceiptQty": "decimal",
					"plannedOrderReleaseQty": "decimal",
					"planningIssuesQty": "decimal",
					"planningReceiptQty": "decimal",
					"planningReleaseQty": "decimal",
					"planningTransferShipQty": "decimal",
					"planningWorksheetQty": "decimal",
					"positiveAdjmtLcy": "decimal",
					"positiveAdjmtQty": "decimal",
					"preventNegativeInventory": "preventNegativeInventory",
					"priceIncludesVat": "boolean",
					"priceProfitCalculation": "priceProfitCalculation",
					"priceUnitConversion": "int",
					"prodForecastQuantityBase": "decimal",
					"productionBomNumber": "string",
					"productionForecastName": "string",
					"profitPercent": "decimal",
					"purchReqReceiptQty": "decimal",
					"purchReqReleaseQty": "decimal",
					"purchUnitOfMeasure": "string",
					"purchasesLcy": "decimal",
					"purchasesQty": "decimal",
					"putAwayTemplateCode": "string",
					"putAwayUnitOfMeasureCode": "string",
					"qtyAssignedToShip": "decimal",
					"qtyInTransit": "decimal",
					"qtyOnAsmComponent": "decimal",
					"qtyOnAssemblyOrder": "decimal",
					"qtyOnComponentLines": "decimal",
					"qtyOnJobOrder": "decimal",
					"qtyOnProdOrder": "decimal",
					"qtyOnPurchOrder": "decimal",
					"qtyOnPurchReturn": "decimal",
					"qtyOnSalesOrder": "decimal",
					"qtyOnSalesReturn": "decimal",
					"qtyOnServiceOrder": "decimal",
					"qtyPicked": "decimal",
					"relOrderReceiptQty": "decimal",
					"reorderPoint": "decimal",
					"reorderQuantity": "decimal",
					"reorderingPolicy": "reorderingPolicy",
					"replenishmentSystem": "replenishmentSystem",
					"resQtyOnAsmComp": "decimal",
					"resQtyOnAssemblyOrder": "decimal",
					"resQtyOnInboundTransfer": "decimal",
					"resQtyOnJobOrder": "decimal",
					"resQtyOnOutboundTransfer": "decimal",
					"resQtyOnProdOrderComp": "decimal",
					"resQtyOnPurchReturns": "decimal",
					"resQtyOnReqLine": "decimal",
					"resQtyOnSalesReturns": "decimal",
					"resQtyOnServiceOrders": "decimal",
					"reschedulingPeriod": "string",
					"reserve": "reserve",
					"reservedQtyOnInventory": "decimal",
					"reservedQtyOnProdOrder": "decimal",
					"reservedQtyOnPurchOrders": "decimal",
					"reservedQtyOnSalesOrders": "decimal",
					"rolledUpCapOverheadCost": "decimal",
					"rolledUpCapacityCost": "decimal",
					"rolledUpMaterialCost": "decimal",
					"rolledUpMfgOvhdCost": "decimal",
					"rolledUpSubcontractedCost": "decimal",
					"roundingPrecision": "decimal",
					"routingNumber": "string",
					"safetyLeadTime": "string",
					"safetyStockQuantity": "decimal",
					"salesLcy": "decimal",
					"salesQty": "decimal",
					"salesUnitOfMeasure": "string",
					"scheduledNeedQty": "decimal",
					"scheduledReceiptQty": "decimal",
					"scrapPercent": "decimal",
					"searchDescription": "string",
					"serialNos": "string",
					"serialNumberFilter": "string",
					"serviceItemGroup": "string",
					"shelfNumber": "string",
					"singleLevelCapOvhdCost": "decimal",
					"singleLevelCapacityCost": "decimal",
					"singleLevelMaterialCost": "decimal",
					"singleLevelMfgOvhdCost": "decimal",
					"singleLevelSubcontrdCost": "decimal",
					"specialEquipmentCode": "string",
					"standardCost": "decimal",
					"statisticsGroup": "int",
					"stockkeepingUnitExists": "boolean",
					"stockoutWarning": "stockoutWarning",
					"substitutesExist": "boolean",
					"tariffNumber": "string",
					"taxGroupCode": "string",
					"taxGroupId": "string",
					"timeBucket": "string",
					"transOrdReceiptQty": "decimal",
					"transOrdShipmentQty": "decimal",
					"transferredLcy": "decimal",
					"transferredQty": "decimal",
					"type": "type",
					"unitCost": "decimal",
					"unitListPrice": "decimal",
					"unitOfMeasureId": "string",
					"unitPrice": "decimal",
					"unitVolume": "decimal",
					"unitsPerParcel": "decimal",
					"useCrossDocking": "boolean",
					"variantFilter": "string",
					"vatBusPostingGrPrice": "string",
					"vatProdPostingGroup": "string",
					"vendorItemNumber": "string",
					"vendorNumber": "string",
					"warehouseClassCode": "string"
				},
				"workflowItems_Fields": "string",
				"workflowItems_Filter": {
					"Criteria": "string",
					"Field": "workflowItems_Fields"
				},
				"workflowItems_List": {
					"workflowItems": "workflowItems"
				}
			}
		},
		"workflowPurchaseDocumentLines": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"workflowPurchaseDocumentLines": "workflowPurchaseDocumentLines"
				},
				"CreateMultiple": {
					"workflowPurchaseDocumentLines_List": "workflowPurchaseDocumentLines_List"
				},
				"CreateMultiple_Result": {
					"workflowPurchaseDocumentLines_List": "workflowPurchaseDocumentLines_List"
				},
				"Create_Result": {
					"workflowPurchaseDocumentLines": "workflowPurchaseDocumentLines"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"documentNumber": "string",
					"documentType": "string",
					"lineNumber": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"workflowPurchaseDocumentLines": "workflowPurchaseDocumentLines"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "workflowPurchaseDocumentLines_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "workflowPurchaseDocumentLines_List"
				},
				"Read_Result": {
					"workflowPurchaseDocumentLines": "workflowPurchaseDocumentLines"
				},
				"Update": {
					"workflowPurchaseDocumentLines": "workflowPurchaseDocumentLines"
				},
				"UpdateMultiple": {
					"workflowPurchaseDocumentLines_List": "workflowPurchaseDocumentLines_List"
				},
				"UpdateMultiple_Result": {
					"workflowPurchaseDocumentLines_List": "workflowPurchaseDocumentLines_List"
				},
				"Update_Result": {
					"workflowPurchaseDocumentLines": "workflowPurchaseDocumentLines"
				},
				"crossReferenceType": "string",
				"documentType": "string",
				"faPostingType": "string",
				"icPartnerRefType": "string",
				"jobLineType": "string",
				"planningFlexibility": "string",
				"prepmtVatCalcType": "string",
				"subtype": "string",
				"type": "string",
				"vatCalculationType": "string",
				"workflowPurchaseDocumentLines": {
					"Key": "string",
					"aRcdNotInvExVatLcy": "decimal",
					"allowInvoiceDisc": "boolean",
					"allowItemChargeAssignment": "boolean",
					"amount": "decimal",
					"amountIncludingVat": "decimal",
					"amtRcdNotInvoiced": "decimal",
					"amtRcdNotInvoicedLcy": "decimal",
					"applToItemEntry": "int",
					"area": "string",
					"attachedToLineNumber": "int",
					"binCode": "string",
					"blanketOrderLineNumber": "int",
					"blanketOrderNumber": "string",
					"budgetedFaNumber": "string",
					"buyFromVendorNumber": "string",
					"completelyReceived": "boolean",
					"crossReferenceNumber": "string",
					"crossReferenceType": "crossReferenceType",
					"crossReferenceTypeNumber": "string",
					"currencyCode": "string",
					"deferralCode": "string",
					"deprAcquisitionCost": "boolean",
					"deprUntilFaPostingDate": "boolean",
					"depreciationBookCode": "string",
					"description": "string",
					"description2": "string",
					"dimensionSetId": "int",
					"directUnitCost": "decimal",
					"documentNumber": "string",
					"documentType": "documentType",
					"dropShipment": "boolean",
					"duplicateInDepreciationBook": "string",
					"entryPoint": "string",
					"expectedReceiptDate": "date",
					"faPostingDate": "date",
					"faPostingType": "faPostingType",
					"finished": "boolean",
					"genBusPostingGroup": "string",
					"genProdPostingGroup": "string",
					"grossWeight": "decimal",
					"icPartnerCode": "string",
					"icPartnerRefType": "icPartnerRefType",
					"icPartnerReference": "string",
					"inboundWhseHandlingTime": "string",
					"indirectCostPercent": "decimal",
					"insuranceNumber": "string",
					"invDiscAmountToInvoice": "decimal",
					"invDiscountAmount": "decimal",
					"itemCategoryCode": "string",
					"jobCurrencyCode": "string",
					"jobCurrencyFactor": "decimal",
					"jobLineAmount": "decimal",
					"jobLineAmountLcy": "decimal",
					"jobLineDiscAmountLcy": "decimal",
					"jobLineDiscountAmount": "decimal",
					"jobLineDiscountPercent": "decimal",
					"jobLineType": "jobLineType",
					"jobNumber": "string",
					"jobPlanningLineNumber": "int",
					"jobRemainingQty": "decimal",
					"jobRemainingQtyBase": "decimal",
					"jobTaskNumber": "string",
					"jobTotalPrice": "decimal",
					"jobTotalPriceLcy": "decimal",
					"jobUnitPrice": "decimal",
					"jobUnitPriceLcy": "decimal",
					"leadTimeCalculation": "string",
					"lineAmount": "decimal",
					"lineDiscountAmount": "decimal",
					"lineDiscountPercent": "decimal",
					"lineNumber": "int",
					"locationCode": "string",
					"maintenanceCode": "string",
					"mpsOrder": "boolean",
					"netWeight": "decimal",
					"nonstock": "boolean",
					"number": "string",
					"operationNumber": "string",
					"orderDate": "date",
					"outstandingAmount": "decimal",
					"outstandingAmountLcy": "decimal",
					"outstandingAmtExVatLcy": "decimal",
					"outstandingQtyBase": "decimal",
					"outstandingQuantity": "decimal",
					"overheadRate": "decimal",
					"payToVendorNumber": "string",
					"plannedReceiptDate": "date",
					"planningFlexibility": "planningFlexibility",
					"postingGroup": "string",
					"prepaymentAmount": "decimal",
					"prepaymentLine": "boolean",
					"prepaymentPercent": "decimal",
					"prepaymentTaxAreaCode": "string",
					"prepaymentTaxGroupCode": "string",
					"prepaymentTaxLiable": "boolean",
					"prepaymentVatDifference": "decimal",
					"prepaymentVatIdentifier": "string",
					"prepaymentVatPercent": "decimal",
					"prepmtAmountInvInclVat": "decimal",
					"prepmtAmountInvLcy": "decimal",
					"prepmtAmtDeducted": "decimal",
					"prepmtAmtInclVat": "decimal",
					"prepmtAmtInv": "decimal",
					"prepmtAmtToDeduct": "decimal",
					"prepmtLineAmount": "decimal",
					"prepmtVatAmountInvLcy": "decimal",
					"prepmtVatBaseAmt": "decimal",
					"prepmtVatCalcType": "prepmtVatCalcType",
					"prepmtVatDiffDeducted": "decimal",
					"prepmtVatDiffToDeduct": "decimal",
					"prodOrderLineNumber": "int",
					"prodOrderNumber": "string",
					"profitPercent": "decimal",
					"promisedReceiptDate": "date",
					"purchasingCode": "string",
					"qtyAssigned": "decimal",
					"qtyInvoicedBase": "decimal",
					"qtyPerUnitOfMeasure": "decimal",
					"qtyRcdNotInvoiced": "decimal",
					"qtyRcdNotInvoicedBase": "decimal",
					"qtyReceivedBase": "decimal",
					"qtyToAssign": "decimal",
					"qtyToInvoice": "decimal",
					"qtyToInvoiceBase": "decimal",
					"qtyToReceive": "decimal",
					"qtyToReceiveBase": "decimal",
					"quantity": "decimal",
					"quantityBase": "decimal",
					"quantityInvoiced": "decimal",
					"quantityReceived": "decimal",
					"recalculateInvoiceDisc": "boolean",
					"receiptLineNumber": "int",
					"receiptNumber": "string",
					"requestedReceiptDate": "date",
					"reservedQtyBase": "decimal",
					"reservedQuantity": "decimal",
					"responsibilityCenter": "string",
					"retQtyShpdNotInvdBase": "decimal",
					"returnQtyShipped": "decimal",
					"returnQtyShippedBase": "decimal",
					"returnQtyShippedNotInvd": "decimal",
					"returnQtyToShip": "decimal",
					"returnQtyToShipBase": "decimal",
					"returnReasonCode": "string",
					"returnShipmentLineNumber": "int",
					"returnShipmentNumber": "string",
					"returnShpdNotInvd": "decimal",
					"returnShpdNotInvdLcy": "decimal",
					"returnsDeferralStartDate": "date",
					"routingNumber": "string",
					"routingReferenceNumber": "int",
					"safetyLeadTime": "string",
					"salesOrderLineNumber": "int",
					"salesOrderNumber": "string",
					"salvageValue": "decimal",
					"shortcutDimension1Code": "string",
					"shortcutDimension2Code": "string",
					"specialOrder": "boolean",
					"specialOrderSalesLineNumber": "int",
					"specialOrderSalesNumber": "string",
					"subtype": "subtype",
					"systemCreatedEntry": "boolean",
					"taxAreaCode": "string",
					"taxGroupCode": "string",
					"taxLiable": "boolean",
					"transactionSpecification": "string",
					"transactionType": "string",
					"transportMethod": "string",
					"type": "type",
					"unitCost": "decimal",
					"unitCostLcy": "decimal",
					"unitOfMeasure": "string",
					"unitOfMeasureCode": "string",
					"unitOfMeasureCrossRef": "string",
					"unitPriceLcy": "decimal",
					"unitVolume": "decimal",
					"unitsPerParcel": "decimal",
					"useDuplicationList": "boolean",
					"useTax": "boolean",
					"variantCode": "string",
					"vatBaseAmount": "decimal",
					"vatBusPostingGroup": "string",
					"vatCalculationType": "vatCalculationType",
					"vatDifference": "decimal",
					"vatIdentifier": "string",
					"vatPercent": "decimal",
					"vatProdPostingGroup": "string",
					"vendorItemNumber": "string",
					"whseOutstandingQtyBase": "decimal",
					"workCenterNumber": "string"
				},
				"workflowPurchaseDocumentLines_Fields": "string",
				"workflowPurchaseDocumentLines_Filter": {
					"Criteria": "string",
					"Field": "workflowPurchaseDocumentLines_Fields"
				},
				"workflowPurchaseDocumentLines_List": {
					"workflowPurchaseDocumentLines": "workflowPurchaseDocumentLines"
				}
			}
		},
		"workflowPurchaseDocuments": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"Delete_workflowPurchaseDocumentLines": {
					"i": "Delete_workflowPurchaseDocumentLines",
					"o": "Delete_workflowPurchaseDocumentLines_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"workflowPurchaseDocuments": "workflowPurchaseDocuments"
				},
				"CreateMultiple": {
					"workflowPurchaseDocuments_List": "workflowPurchaseDocuments_List"
				},
				"CreateMultiple_Result": {
					"workflowPurchaseDocuments_List": "workflowPurchaseDocuments_List"
				},
				"Create_Result": {
					"workflowPurchaseDocuments": "workflowPurchaseDocuments"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Delete_workflowPurchaseDocumentLines": {
					"workflowPurchaseDocumentLines_Key": "string"
				},
				"Delete_workflowPurchaseDocumentLines_Result": {
					"Delete_workflowPurchaseDocumentLines_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Purchase_Document_Line_Entity": {
					"Key": "string",
					"aRcdNotInvExVatLcy": "decimal",
					"allowInvoiceDisc": "boolean",
					"allowItemChargeAssignment": "boolean",
					"amount": "decimal",
					"amountIncludingVat": "decimal",
					"amtRcdNotInvoiced": "decimal",
					"amtRcdNotInvoicedLcy": "decimal",
					"applToItemEntry": "int",
					"area": "string",
					"attachedToLineNumber": "int",
					"binCode": "string",
					"blanketOrderLineNumber": "int",
					"blanketOrderNumber": "string",
					"budgetedFaNumber": "string",
					"buyFromVendorNumber": "string",
					"completelyReceived": "boolean",
					"crossReferenceNumber": "string",
					"crossReferenceType": "crossReferenceType",
					"crossReferenceTypeNumber": "string",
					"currencyCode": "string",
					"deferralCode": "string",
					"deprAcquisitionCost": "boolean",
					"deprUntilFaPostingDate": "boolean",
					"depreciationBookCode": "string",
					"description": "string",
					"description2": "string",
					"dimensionSetId": "int",
					"directUnitCost": "decimal",
					"documentNumber": "string",
					"documentType": "documentType_1",
					"dropShipment": "boolean",
					"duplicateInDepreciationBook": "string",
					"entryPoint": "string",
					"expectedReceiptDate": "date",
					"faPostingDate": "date",
					"faPostingType": "faPostingType",
					"finished": "boolean",
					"genBusPostingGroup": "string",
					"genProdPostingGroup": "string",
					"grossWeight": "decimal",
					"icPartnerCode": "string",
					"icPartnerRefType": "icPartnerRefType",
					"icPartnerReference": "string",
					"inboundWhseHandlingTime": "string",
					"indirectCostPercent": "decimal",
					"insuranceNumber": "string",
					"invDiscAmountToInvoice": "decimal",
					"invDiscountAmount": "decimal",
					"itemCategoryCode": "string",
					"jobCurrencyCode": "string",
					"jobCurrencyFactor": "decimal",
					"jobLineAmount": "decimal",
					"jobLineAmountLcy": "decimal",
					"jobLineDiscAmountLcy": "decimal",
					"jobLineDiscountAmount": "decimal",
					"jobLineDiscountPercent": "decimal",
					"jobLineType": "jobLineType",
					"jobNumber": "string",
					"jobPlanningLineNumber": "int",
					"jobRemainingQty": "decimal",
					"jobRemainingQtyBase": "decimal",
					"jobTaskNumber": "string",
					"jobTotalPrice": "decimal",
					"jobTotalPriceLcy": "decimal",
					"jobUnitPrice": "decimal",
					"jobUnitPriceLcy": "decimal",
					"leadTimeCalculation": "string",
					"lineAmount": "decimal",
					"lineDiscountAmount": "decimal",
					"lineDiscountPercent": "decimal",
					"lineNumber": "int",
					"locationCode": "string",
					"maintenanceCode": "string",
					"mpsOrder": "boolean",
					"netWeight": "decimal",
					"nonstock": "boolean",
					"number": "string",
					"operationNumber": "string",
					"orderDate": "date",
					"outstandingAmount": "decimal",
					"outstandingAmountLcy": "decimal",
					"outstandingAmtExVatLcy": "decimal",
					"outstandingQtyBase": "decimal",
					"outstandingQuantity": "decimal",
					"overheadRate": "decimal",
					"payToVendorNumber": "string",
					"plannedReceiptDate": "date",
					"planningFlexibility": "planningFlexibility",
					"postingGroup": "string",
					"prepaymentAmount": "decimal",
					"prepaymentLine": "boolean",
					"prepaymentPercent": "decimal",
					"prepaymentTaxAreaCode": "string",
					"prepaymentTaxGroupCode": "string",
					"prepaymentTaxLiable": "boolean",
					"prepaymentVatDifference": "decimal",
					"prepaymentVatIdentifier": "string",
					"prepaymentVatPercent": "decimal",
					"prepmtAmountInvInclVat": "decimal",
					"prepmtAmountInvLcy": "decimal",
					"prepmtAmtDeducted": "decimal",
					"prepmtAmtInclVat": "decimal",
					"prepmtAmtInv": "decimal",
					"prepmtAmtToDeduct": "decimal",
					"prepmtLineAmount": "decimal",
					"prepmtVatAmountInvLcy": "decimal",
					"prepmtVatBaseAmt": "decimal",
					"prepmtVatCalcType": "prepmtVatCalcType",
					"prepmtVatDiffDeducted": "decimal",
					"prepmtVatDiffToDeduct": "decimal",
					"prodOrderLineNumber": "int",
					"prodOrderNumber": "string",
					"profitPercent": "decimal",
					"promisedReceiptDate": "date",
					"purchasingCode": "string",
					"qtyAssigned": "decimal",
					"qtyInvoicedBase": "decimal",
					"qtyPerUnitOfMeasure": "decimal",
					"qtyRcdNotInvoiced": "decimal",
					"qtyRcdNotInvoicedBase": "decimal",
					"qtyReceivedBase": "decimal",
					"qtyToAssign": "decimal",
					"qtyToInvoice": "decimal",
					"qtyToInvoiceBase": "decimal",
					"qtyToReceive": "decimal",
					"qtyToReceiveBase": "decimal",
					"quantity": "decimal",
					"quantityBase": "decimal",
					"quantityInvoiced": "decimal",
					"quantityReceived": "decimal",
					"recalculateInvoiceDisc": "boolean",
					"receiptLineNumber": "int",
					"receiptNumber": "string",
					"requestedReceiptDate": "date",
					"reservedQtyBase": "decimal",
					"reservedQuantity": "decimal",
					"responsibilityCenter": "string",
					"retQtyShpdNotInvdBase": "decimal",
					"returnQtyShipped": "decimal",
					"returnQtyShippedBase": "decimal",
					"returnQtyShippedNotInvd": "decimal",
					"returnQtyToShip": "decimal",
					"returnQtyToShipBase": "decimal",
					"returnReasonCode": "string",
					"returnShipmentLineNumber": "int",
					"returnShipmentNumber": "string",
					"returnShpdNotInvd": "decimal",
					"returnShpdNotInvdLcy": "decimal",
					"returnsDeferralStartDate": "date",
					"routingNumber": "string",
					"routingReferenceNumber": "int",
					"safetyLeadTime": "string",
					"salesOrderLineNumber": "int",
					"salesOrderNumber": "string",
					"salvageValue": "decimal",
					"shortcutDimension1Code": "string",
					"shortcutDimension2Code": "string",
					"specialOrder": "boolean",
					"specialOrderSalesLineNumber": "int",
					"specialOrderSalesNumber": "string",
					"subtype": "subtype",
					"systemCreatedEntry": "boolean",
					"taxAreaCode": "string",
					"taxGroupCode": "string",
					"taxLiable": "boolean",
					"transactionSpecification": "string",
					"transactionType": "string",
					"transportMethod": "string",
					"type": "type",
					"unitCost": "decimal",
					"unitCostLcy": "decimal",
					"unitOfMeasure": "string",
					"unitOfMeasureCode": "string",
					"unitOfMeasureCrossRef": "string",
					"unitPriceLcy": "decimal",
					"unitVolume": "decimal",
					"unitsPerParcel": "decimal",
					"useDuplicationList": "boolean",
					"useTax": "boolean",
					"variantCode": "string",
					"vatBaseAmount": "decimal",
					"vatBusPostingGroup": "string",
					"vatCalculationType": "vatCalculationType",
					"vatDifference": "decimal",
					"vatIdentifier": "string",
					"vatPercent": "decimal",
					"vatProdPostingGroup": "string",
					"vendorItemNumber": "string",
					"whseOutstandingQtyBase": "decimal",
					"workCenterNumber": "string"
				},
				"Purchase_Document_Line_Entity_List": {
					"Purchase_Document_Line_Entity": "Purchase_Document_Line_Entity"
				},
				"Read": {
					"documentType": "string",
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"workflowPurchaseDocuments": "workflowPurchaseDocuments"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "workflowPurchaseDocuments_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "workflowPurchaseDocuments_List"
				},
				"Read_Result": {
					"workflowPurchaseDocuments": "workflowPurchaseDocuments"
				},
				"Update": {
					"workflowPurchaseDocuments": "workflowPurchaseDocuments"
				},
				"UpdateMultiple": {
					"workflowPurchaseDocuments_List": "workflowPurchaseDocuments_List"
				},
				"UpdateMultiple_Result": {
					"workflowPurchaseDocuments_List": "workflowPurchaseDocuments_List"
				},
				"Update_Result": {
					"workflowPurchaseDocuments": "workflowPurchaseDocuments"
				},
				"appliesToDocType": "string",
				"balAccountType": "string",
				"crossReferenceType": "string",
				"documentType": "string",
				"documentType_1": "string",
				"faPostingType": "string",
				"icDirection": "string",
				"icPartnerRefType": "string",
				"icStatus": "string",
				"invoiceDiscountCalculation": "string",
				"jobLineType": "string",
				"jobQueueStatus": "string",
				"planningFlexibility": "string",
				"prepmtVatCalcType": "string",
				"status": "string",
				"subtype": "string",
				"type": "string",
				"vatCalculationType": "string",
				"workflowPurchaseDocuments": {
					"Key": "string",
					"aRcdNotInvExVatLcy": "decimal",
					"amount": "decimal",
					"amountIncludingVat": "decimal",
					"amtRcdNotInvoicedLcy": "decimal",
					"appliesToDocNumber": "string",
					"appliesToDocType": "appliesToDocType",
					"appliesToId": "string",
					"area": "string",
					"assignedUserId": "string",
					"balAccountNumber": "string",
					"balAccountType": "balAccountType",
					"buyFromAddress": "string",
					"buyFromAddress2": "string",
					"buyFromCity": "string",
					"buyFromContact": "string",
					"buyFromContactNumber": "string",
					"buyFromCountryRegionCode": "string",
					"buyFromCounty": "string",
					"buyFromIcPartnerCode": "string",
					"buyFromPostCode": "string",
					"buyFromVendorName": "string",
					"buyFromVendorName2": "string",
					"buyFromVendorNumber": "string",
					"campaignNumber": "string",
					"comment": "boolean",
					"completelyReceived": "boolean",
					"compressPrepayment": "boolean",
					"correction": "boolean",
					"creditorNumber": "string",
					"currencyCode": "string",
					"currencyFactor": "decimal",
					"dimensionSetId": "int",
					"docNumberOccurrence": "int",
					"documentDate": "date",
					"documentType": "documentType",
					"dueDate": "date",
					"entryPoint": "string",
					"expectedReceiptDate": "date",
					"genBusPostingGroup": "string",
					"icDirection": "icDirection",
					"icStatus": "icStatus",
					"id": "string",
					"inboundWhseHandlingTime": "string",
					"incomingDocumentEntryNumber": "int",
					"invoice": "boolean",
					"invoiceDiscCode": "string",
					"invoiceDiscountAmount": "decimal",
					"invoiceDiscountCalculation": "invoiceDiscountCalculation",
					"invoiceDiscountValue": "decimal",
					"jobQueueEntryId": "string",
					"jobQueueStatus": "jobQueueStatus",
					"languageCode": "string",
					"lastPostingNumber": "string",
					"lastPrepaymentNumber": "string",
					"lastPrepmtCrMemoNumber": "string",
					"lastReceivingNumber": "string",
					"lastReturnShipmentNumber": "string",
					"leadTimeCalculation": "string",
					"locationCode": "string",
					"locationFilter": "string",
					"number": "string",
					"numberOfArchivedVersions": "int",
					"numberPrinted": "int",
					"numberSeries": "string",
					"onHold": "string",
					"orderAddressCode": "string",
					"orderClass": "string",
					"orderDate": "date",
					"payToAddress": "string",
					"payToAddress2": "string",
					"payToCity": "string",
					"payToContact": "string",
					"payToContactNumber": "string",
					"payToCountryRegionCode": "string",
					"payToCounty": "string",
					"payToIcPartnerCode": "string",
					"payToName": "string",
					"payToName2": "string",
					"payToPostCode": "string",
					"payToVendorNumber": "string",
					"paymentDiscountPercent": "decimal",
					"paymentMethodCode": "string",
					"paymentReference": "string",
					"paymentTermsCode": "string",
					"pendingApprovals": "int",
					"pmtDiscountDate": "date",
					"postingDate": "date",
					"postingDescription": "string",
					"postingFromWhseRef": "int",
					"postingNumber": "string",
					"postingNumberSeries": "string",
					"prepaymentDueDate": "date",
					"prepaymentNumber": "string",
					"prepaymentNumberSeries": "string",
					"prepaymentPercent": "decimal",
					"prepmtCrMemoNumber": "string",
					"prepmtCrMemoNumberSeries": "string",
					"prepmtPaymentDiscountPercent": "decimal",
					"prepmtPaymentTermsCode": "string",
					"prepmtPmtDiscountDate": "date",
					"prepmtPostingDescription": "string",
					"pricesIncludingVat": "boolean",
					"printPostedDocuments": "boolean",
					"promisedReceiptDate": "date",
					"purchaserCode": "string",
					"quoteNumber": "string",
					"reasonCode": "string",
					"recalculateInvoiceDisc": "boolean",
					"receive": "boolean",
					"receivingNumber": "string",
					"receivingNumberSeries": "string",
					"requestedReceiptDate": "date",
					"responsibilityCenter": "string",
					"returnShipmentNumber": "string",
					"returnShipmentNumberSeries": "string",
					"sellToCustomerNumber": "string",
					"sendIcDocument": "boolean",
					"ship": "boolean",
					"shipToAddress": "string",
					"shipToAddress2": "string",
					"shipToCity": "string",
					"shipToCode": "string",
					"shipToContact": "string",
					"shipToCountryRegionCode": "string",
					"shipToCounty": "string",
					"shipToName": "string",
					"shipToName2": "string",
					"shipToPostCode": "string",
					"shipmentMethodCode": "string",
					"shortcutDimension1Code": "string",
					"shortcutDimension2Code": "string",
					"status": "status",
					"taxAreaCode": "string",
					"taxLiable": "boolean",
					"transactionSpecification": "string",
					"transactionType": "string",
					"transportMethod": "string",
					"vatBaseDiscountPercent": "decimal",
					"vatBusPostingGroup": "string",
					"vatCountryRegionCode": "string",
					"vatRegistrationNumber": "string",
					"vendorAuthorizationNumber": "string",
					"vendorCrMemoNumber": "string",
					"vendorInvoiceNumber": "string",
					"vendorOrderNumber": "string",
					"vendorPostingGroup": "string",
					"vendorShipmentNumber": "string",
					"workflowPurchaseDocumentLines": "Purchase_Document_Line_Entity_List",
					"yourReference": "string"
				},
				"workflowPurchaseDocuments_Fields": "string",
				"workflowPurchaseDocuments_Filter": {
					"Criteria": "string",
					"Field": "workflowPurchaseDocuments_Fields"
				},
				"workflowPurchaseDocuments_List": {
					"workflowPurchaseDocuments": "workflowPurchaseDocuments"
				}
			}
		},
		"workflowSalesDocumentLines": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"workflowSalesDocumentLines": "workflowSalesDocumentLines"
				},
				"CreateMultiple": {
					"workflowSalesDocumentLines_List": "workflowSalesDocumentLines_List"
				},
				"CreateMultiple_Result": {
					"workflowSalesDocumentLines_List": "workflowSalesDocumentLines_List"
				},
				"Create_Result": {
					"workflowSalesDocumentLines": "workflowSalesDocumentLines"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"documentNumber": "string",
					"documentType": "string",
					"lineNumber": "int"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"workflowSalesDocumentLines": "workflowSalesDocumentLines"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "workflowSalesDocumentLines_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "workflowSalesDocumentLines_List"
				},
				"Read_Result": {
					"workflowSalesDocumentLines": "workflowSalesDocumentLines"
				},
				"Update": {
					"workflowSalesDocumentLines": "workflowSalesDocumentLines"
				},
				"UpdateMultiple": {
					"workflowSalesDocumentLines_List": "workflowSalesDocumentLines_List"
				},
				"UpdateMultiple_Result": {
					"workflowSalesDocumentLines_List": "workflowSalesDocumentLines_List"
				},
				"Update_Result": {
					"workflowSalesDocumentLines": "workflowSalesDocumentLines"
				},
				"crossReferenceType": "string",
				"documentType": "string",
				"icPartnerRefType": "string",
				"prepmtVatCalcType": "string",
				"reserve": "string",
				"subtype": "string",
				"type": "string",
				"vatCalculationType": "string",
				"workflowSalesDocumentLines": {
					"Key": "string",
					"allowInvoiceDisc": "boolean",
					"allowItemChargeAssignment": "boolean",
					"allowLineDisc": "boolean",
					"amount": "decimal",
					"amountIncludingVat": "decimal",
					"applFromItemEntry": "int",
					"applToItemEntry": "int",
					"area": "string",
					"atoWhseOutstandingQty": "decimal",
					"atoWhseOutstdQtyBase": "decimal",
					"attachedToLineNumber": "int",
					"billToCustomerNumber": "string",
					"binCode": "string",
					"blanketOrderLineNumber": "int",
					"blanketOrderNumber": "string",
					"bomItemNumber": "string",
					"completelyShipped": "boolean",
					"crossReferenceNumber": "string",
					"crossReferenceType": "crossReferenceType",
					"crossReferenceTypeNumber": "string",
					"currencyCode": "string",
					"customerDiscGroup": "string",
					"customerPriceGroup": "string",
					"deferralCode": "string",
					"deprUntilFaPostingDate": "boolean",
					"depreciationBookCode": "string",
					"description": "string",
					"description2": "string",
					"dimensionSetId": "int",
					"documentNumber": "string",
					"documentType": "documentType",
					"dropShipment": "boolean",
					"duplicateInDepreciationBook": "string",
					"exitPoint": "string",
					"faPostingDate": "date",
					"genBusPostingGroup": "string",
					"genProdPostingGroup": "string",
					"grossWeight": "decimal",
					"icPartnerCode": "string",
					"icPartnerRefType": "icPartnerRefType",
					"icPartnerReference": "string",
					"invDiscAmountToInvoice": "decimal",
					"invDiscountAmount": "decimal",
					"itemCategoryCode": "string",
					"jobContractEntryNumber": "int",
					"jobNumber": "string",
					"jobTaskNumber": "string",
					"lineAmount": "decimal",
					"lineDiscountAmount": "decimal",
					"lineDiscountPercent": "decimal",
					"lineNumber": "int",
					"locationCode": "string",
					"netWeight": "decimal",
					"nonstock": "boolean",
					"number": "string",
					"originallyOrderedNumber": "string",
					"originallyOrderedVarCode": "string",
					"outOfStockSubstitution": "boolean",
					"outboundWhseHandlingTime": "string",
					"outstandingAmount": "decimal",
					"outstandingAmountLcy": "decimal",
					"outstandingQtyBase": "decimal",
					"outstandingQuantity": "decimal",
					"planned": "boolean",
					"plannedDeliveryDate": "date",
					"plannedShipmentDate": "date",
					"postingDate": "date",
					"postingGroup": "string",
					"prepaymentAmount": "decimal",
					"prepaymentLine": "boolean",
					"prepaymentPercent": "decimal",
					"prepaymentTaxAreaCode": "string",
					"prepaymentTaxGroupCode": "string",
					"prepaymentTaxLiable": "boolean",
					"prepaymentVatDifference": "decimal",
					"prepaymentVatIdentifier": "string",
					"prepaymentVatPercent": "decimal",
					"prepmtAmountInvInclVat": "decimal",
					"prepmtAmountInvLcy": "decimal",
					"prepmtAmtDeducted": "decimal",
					"prepmtAmtInclVat": "decimal",
					"prepmtAmtInv": "decimal",
					"prepmtAmtToDeduct": "decimal",
					"prepmtLineAmount": "decimal",
					"prepmtVatAmountInvLcy": "decimal",
					"prepmtVatBaseAmt": "decimal",
					"prepmtVatCalcType": "prepmtVatCalcType",
					"prepmtVatDiffDeducted": "decimal",
					"prepmtVatDiffToDeduct": "decimal",
					"priceDescription": "string",
					"profitPercent": "decimal",
					"promisedDeliveryDate": "date",
					"purchOrderLineNumber": "int",
					"purchaseOrderNumber": "string",
					"purchasingCode": "string",
					"qtyAssigned": "decimal",
					"qtyInvoicedBase": "decimal",
					"qtyPerUnitOfMeasure": "decimal",
					"qtyShippedBase": "decimal",
					"qtyShippedNotInvdBase": "decimal",
					"qtyShippedNotInvoiced": "decimal",
					"qtyToAsmToOrderBase": "decimal",
					"qtyToAssembleToOrder": "decimal",
					"qtyToAssign": "decimal",
					"qtyToInvoice": "decimal",
					"qtyToInvoiceBase": "decimal",
					"qtyToShip": "decimal",
					"qtyToShipBase": "decimal",
					"quantity": "decimal",
					"quantityBase": "decimal",
					"quantityInvoiced": "decimal",
					"quantityShipped": "decimal",
					"recalculateInvoiceDisc": "boolean",
					"requestedDeliveryDate": "date",
					"reserve": "reserve",
					"reservedQtyBase": "decimal",
					"reservedQuantity": "decimal",
					"responsibilityCenter": "string",
					"retQtyRcdNotInvdBase": "decimal",
					"returnQtyRcdNotInvd": "decimal",
					"returnQtyReceived": "decimal",
					"returnQtyReceivedBase": "decimal",
					"returnQtyToReceive": "decimal",
					"returnQtyToReceiveBase": "decimal",
					"returnRcdNotInvd": "decimal",
					"returnRcdNotInvdLcy": "decimal",
					"returnReasonCode": "string",
					"returnReceiptLineNumber": "int",
					"returnReceiptNumber": "string",
					"returnsDeferralStartDate": "date",
					"sellToCustomerNumber": "string",
					"shipmentDate": "date",
					"shipmentLineNumber": "int",
					"shipmentNumber": "string",
					"shippedNotInvLcyNoVat": "decimal",
					"shippedNotInvoiced": "decimal",
					"shippedNotInvoicedLcy": "decimal",
					"shippingAgentCode": "string",
					"shippingAgentServiceCode": "string",
					"shippingTime": "string",
					"shortcutDimension1Code": "string",
					"shortcutDimension2Code": "string",
					"specialOrder": "boolean",
					"specialOrderPurchLineNumber": "int",
					"specialOrderPurchaseNumber": "string",
					"substitutionAvailable": "boolean",
					"subtype": "subtype",
					"systemCreatedEntry": "boolean",
					"taxAreaCode": "string",
					"taxCategory": "string",
					"taxGroupCode": "string",
					"taxLiable": "boolean",
					"transactionSpecification": "string",
					"transactionType": "string",
					"transportMethod": "string",
					"type": "type",
					"unitCost": "decimal",
					"unitCostLcy": "decimal",
					"unitOfMeasure": "string",
					"unitOfMeasureCode": "string",
					"unitOfMeasureCrossRef": "string",
					"unitPrice": "decimal",
					"unitVolume": "decimal",
					"unitsPerParcel": "decimal",
					"useDuplicationList": "boolean",
					"variantCode": "string",
					"vatBaseAmount": "decimal",
					"vatBusPostingGroup": "string",
					"vatCalculationType": "vatCalculationType",
					"vatClauseCode": "string",
					"vatDifference": "decimal",
					"vatIdentifier": "string",
					"vatPercent": "decimal",
					"vatProdPostingGroup": "string",
					"whseOutstandingQty": "decimal",
					"whseOutstandingQtyBase": "decimal",
					"workTypeCode": "string"
				},
				"workflowSalesDocumentLines_Fields": "string",
				"workflowSalesDocumentLines_Filter": {
					"Criteria": "string",
					"Field": "workflowSalesDocumentLines_Fields"
				},
				"workflowSalesDocumentLines_List": {
					"workflowSalesDocumentLines": "workflowSalesDocumentLines"
				}
			}
		},
		"workflowSalesDocuments": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"Delete_workflowSalesDocumentLines": {
					"i": "Delete_workflowSalesDocumentLines",
					"o": "Delete_workflowSalesDocumentLines_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"workflowSalesDocuments": "workflowSalesDocuments"
				},
				"CreateMultiple": {
					"workflowSalesDocuments_List": "workflowSalesDocuments_List"
				},
				"CreateMultiple_Result": {
					"workflowSalesDocuments_List": "workflowSalesDocuments_List"
				},
				"Create_Result": {
					"workflowSalesDocuments": "workflowSalesDocuments"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"Delete_workflowSalesDocumentLines": {
					"workflowSalesDocumentLines_Key": "string"
				},
				"Delete_workflowSalesDocumentLines_Result": {
					"Delete_workflowSalesDocumentLines_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"documentType": "string",
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"workflowSalesDocuments": "workflowSalesDocuments"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "workflowSalesDocuments_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "workflowSalesDocuments_List"
				},
				"Read_Result": {
					"workflowSalesDocuments": "workflowSalesDocuments"
				},
				"Sales_Document_Line_Entity": {
					"Key": "string",
					"allowInvoiceDisc": "boolean",
					"allowItemChargeAssignment": "boolean",
					"allowLineDisc": "boolean",
					"amount": "decimal",
					"amountIncludingVat": "decimal",
					"applFromItemEntry": "int",
					"applToItemEntry": "int",
					"area": "string",
					"atoWhseOutstandingQty": "decimal",
					"atoWhseOutstdQtyBase": "decimal",
					"attachedToLineNumber": "int",
					"billToCustomerNumber": "string",
					"binCode": "string",
					"blanketOrderLineNumber": "int",
					"blanketOrderNumber": "string",
					"bomItemNumber": "string",
					"completelyShipped": "boolean",
					"crossReferenceNumber": "string",
					"crossReferenceType": "crossReferenceType",
					"crossReferenceTypeNumber": "string",
					"currencyCode": "string",
					"customerDiscGroup": "string",
					"customerPriceGroup": "string",
					"deferralCode": "string",
					"deprUntilFaPostingDate": "boolean",
					"depreciationBookCode": "string",
					"description": "string",
					"description2": "string",
					"dimensionSetId": "int",
					"documentNumber": "string",
					"documentType": "documentType_1",
					"dropShipment": "boolean",
					"duplicateInDepreciationBook": "string",
					"exitPoint": "string",
					"faPostingDate": "date",
					"genBusPostingGroup": "string",
					"genProdPostingGroup": "string",
					"grossWeight": "decimal",
					"icPartnerCode": "string",
					"icPartnerRefType": "icPartnerRefType",
					"icPartnerReference": "string",
					"invDiscAmountToInvoice": "decimal",
					"invDiscountAmount": "decimal",
					"itemCategoryCode": "string",
					"jobContractEntryNumber": "int",
					"jobNumber": "string",
					"jobTaskNumber": "string",
					"lineAmount": "decimal",
					"lineDiscountAmount": "decimal",
					"lineDiscountPercent": "decimal",
					"lineNumber": "int",
					"locationCode": "string",
					"netWeight": "decimal",
					"nonstock": "boolean",
					"number": "string",
					"originallyOrderedNumber": "string",
					"originallyOrderedVarCode": "string",
					"outOfStockSubstitution": "boolean",
					"outboundWhseHandlingTime": "string",
					"outstandingAmount": "decimal",
					"outstandingAmountLcy": "decimal",
					"outstandingQtyBase": "decimal",
					"outstandingQuantity": "decimal",
					"planned": "boolean",
					"plannedDeliveryDate": "date",
					"plannedShipmentDate": "date",
					"postingDate": "date",
					"postingGroup": "string",
					"prepaymentAmount": "decimal",
					"prepaymentLine": "boolean",
					"prepaymentPercent": "decimal",
					"prepaymentTaxAreaCode": "string",
					"prepaymentTaxGroupCode": "string",
					"prepaymentTaxLiable": "boolean",
					"prepaymentVatDifference": "decimal",
					"prepaymentVatIdentifier": "string",
					"prepaymentVatPercent": "decimal",
					"prepmtAmountInvInclVat": "decimal",
					"prepmtAmountInvLcy": "decimal",
					"prepmtAmtDeducted": "decimal",
					"prepmtAmtInclVat": "decimal",
					"prepmtAmtInv": "decimal",
					"prepmtAmtToDeduct": "decimal",
					"prepmtLineAmount": "decimal",
					"prepmtVatAmountInvLcy": "decimal",
					"prepmtVatBaseAmt": "decimal",
					"prepmtVatCalcType": "prepmtVatCalcType",
					"prepmtVatDiffDeducted": "decimal",
					"prepmtVatDiffToDeduct": "decimal",
					"priceDescription": "string",
					"profitPercent": "decimal",
					"promisedDeliveryDate": "date",
					"purchOrderLineNumber": "int",
					"purchaseOrderNumber": "string",
					"purchasingCode": "string",
					"qtyAssigned": "decimal",
					"qtyInvoicedBase": "decimal",
					"qtyPerUnitOfMeasure": "decimal",
					"qtyShippedBase": "decimal",
					"qtyShippedNotInvdBase": "decimal",
					"qtyShippedNotInvoiced": "decimal",
					"qtyToAsmToOrderBase": "decimal",
					"qtyToAssembleToOrder": "decimal",
					"qtyToAssign": "decimal",
					"qtyToInvoice": "decimal",
					"qtyToInvoiceBase": "decimal",
					"qtyToShip": "decimal",
					"qtyToShipBase": "decimal",
					"quantity": "decimal",
					"quantityBase": "decimal",
					"quantityInvoiced": "decimal",
					"quantityShipped": "decimal",
					"recalculateInvoiceDisc": "boolean",
					"requestedDeliveryDate": "date",
					"reserve": "reserve_96",
					"reservedQtyBase": "decimal",
					"reservedQuantity": "decimal",
					"responsibilityCenter": "string",
					"retQtyRcdNotInvdBase": "decimal",
					"returnQtyRcdNotInvd": "decimal",
					"returnQtyReceived": "decimal",
					"returnQtyReceivedBase": "decimal",
					"returnQtyToReceive": "decimal",
					"returnQtyToReceiveBase": "decimal",
					"returnRcdNotInvd": "decimal",
					"returnRcdNotInvdLcy": "decimal",
					"returnReasonCode": "string",
					"returnReceiptLineNumber": "int",
					"returnReceiptNumber": "string",
					"returnsDeferralStartDate": "date",
					"sellToCustomerNumber": "string",
					"shipmentDate": "date",
					"shipmentLineNumber": "int",
					"shipmentNumber": "string",
					"shippedNotInvLcyNoVat": "decimal",
					"shippedNotInvoiced": "decimal",
					"shippedNotInvoicedLcy": "decimal",
					"shippingAgentCode": "string",
					"shippingAgentServiceCode": "string",
					"shippingTime": "string",
					"shortcutDimension1Code": "string",
					"shortcutDimension2Code": "string",
					"specialOrder": "boolean",
					"specialOrderPurchLineNumber": "int",
					"specialOrderPurchaseNumber": "string",
					"substitutionAvailable": "boolean",
					"subtype": "subtype",
					"systemCreatedEntry": "boolean",
					"taxAreaCode": "string",
					"taxCategory": "string",
					"taxGroupCode": "string",
					"taxLiable": "boolean",
					"transactionSpecification": "string",
					"transactionType": "string",
					"transportMethod": "string",
					"type": "type",
					"unitCost": "decimal",
					"unitCostLcy": "decimal",
					"unitOfMeasure": "string",
					"unitOfMeasureCode": "string",
					"unitOfMeasureCrossRef": "string",
					"unitPrice": "decimal",
					"unitVolume": "decimal",
					"unitsPerParcel": "decimal",
					"useDuplicationList": "boolean",
					"variantCode": "string",
					"vatBaseAmount": "decimal",
					"vatBusPostingGroup": "string",
					"vatCalculationType": "vatCalculationType",
					"vatClauseCode": "string",
					"vatDifference": "decimal",
					"vatIdentifier": "string",
					"vatPercent": "decimal",
					"vatProdPostingGroup": "string",
					"whseOutstandingQty": "decimal",
					"whseOutstandingQtyBase": "decimal",
					"workTypeCode": "string"
				},
				"Sales_Document_Line_Entity_List": {
					"Sales_Document_Line_Entity": "Sales_Document_Line_Entity"
				},
				"Update": {
					"workflowSalesDocuments": "workflowSalesDocuments"
				},
				"UpdateMultiple": {
					"workflowSalesDocuments_List": "workflowSalesDocuments_List"
				},
				"UpdateMultiple_Result": {
					"workflowSalesDocuments_List": "workflowSalesDocuments_List"
				},
				"Update_Result": {
					"workflowSalesDocuments": "workflowSalesDocuments"
				},
				"appliesToDocType": "string",
				"balAccountType": "string",
				"crossReferenceType": "string",
				"documentType": "string",
				"documentType_1": "string",
				"icDirection": "string",
				"icPartnerRefType": "string",
				"icStatus": "string",
				"invoiceDiscountCalculation": "string",
				"jobQueueStatus": "string",
				"prepmtVatCalcType": "string",
				"reserve": "string",
				"reserve_96": "string",
				"shippingAdvice": "string",
				"status": "string",
				"subtype": "string",
				"type": "string",
				"vatCalculationType": "string",
				"workflowSalesDocuments": {
					"Key": "string",
					"allowLineDisc": "boolean",
					"amount": "decimal",
					"amountIncludingVAT": "decimal",
					"amountShippedNotInvoiced": "decimal",
					"amountShippedNotInvoicedInclVat": "decimal",
					"appliesToDocNumber": "string",
					"appliesToDocType": "appliesToDocType",
					"appliesToId": "string",
					"area": "string",
					"assignedUserId": "string",
					"balAccountNumber": "string",
					"balAccountType": "balAccountType",
					"billToAddress": "string",
					"billToAddress2": "string",
					"billToCity": "string",
					"billToContact": "string",
					"billToContactNumber": "string",
					"billToCountryRegionCode": "string",
					"billToCounty": "string",
					"billToCustomerNumber": "string",
					"billToCustomerTemplateCode": "string",
					"billToIcPartnerCode": "string",
					"billToName": "string",
					"billToName2": "string",
					"billToPostCode": "string",
					"campaignNumber": "string",
					"combineShipments": "boolean",
					"comment": "boolean",
					"completelyShipped": "boolean",
					"compressPrepayment": "boolean",
					"correction": "boolean",
					"currencyCode": "string",
					"currencyFactor": "decimal",
					"customerDiscGroup": "string",
					"customerPostingGroup": "string",
					"customerPriceGroup": "string",
					"dimensionSetId": "int",
					"directDebitMandateId": "string",
					"docNumberOccurrence": "int",
					"documentDate": "date",
					"documentType": "documentType",
					"dueDate": "date",
					"eu3PartyTrade": "boolean",
					"exitPoint": "string",
					"externalDocumentNumber": "string",
					"genBusPostingGroup": "string",
					"getShipmentUsed": "boolean",
					"icDirection": "icDirection",
					"icStatus": "icStatus",
					"id": "string",
					"incomingDocumentEntryNumber": "int",
					"invoice": "boolean",
					"invoiceDiscCode": "string",
					"invoiceDiscountAmount": "decimal",
					"invoiceDiscountCalculation": "invoiceDiscountCalculation",
					"invoiceDiscountValue": "decimal",
					"jobQueueEntryId": "string",
					"jobQueueStatus": "jobQueueStatus",
					"languageCode": "string",
					"lastPostingNumber": "string",
					"lastPremtCrMemoNumber": "string",
					"lastPrepaymentNumber": "string",
					"lastReturnReceiptNumber": "string",
					"lastShippingNumber": "string",
					"lateOrderShipping": "boolean",
					"locationCode": "string",
					"locationFilter": "string",
					"number": "string",
					"numberOfArchivedVersions": "int",
					"numberPrinted": "int",
					"numberSeries": "string",
					"onHold": "string",
					"opportunityNumber": "string",
					"orderClass": "string",
					"orderDate": "date",
					"outboundWhseHandlingTime": "string",
					"packageTrackingNumber": "string",
					"paymentDiscountPercent": "decimal",
					"paymentMethodCode": "string",
					"paymentServiceSetId": "int",
					"paymentTermsCode": "string",
					"pmtDiscountDate": "date",
					"postingDate": "date",
					"postingDescription": "string",
					"postingFromWhseRef": "int",
					"postingNumber": "string",
					"postingNumberSeries": "string",
					"premptCrMemoNumber": "string",
					"prepaymentDueDate": "date",
					"prepaymentNumber": "string",
					"prepaymentNumberSeries": "string",
					"prepaymentPercent": "decimal",
					"prepmtCrMemoNumberSeries": "string",
					"prepmtPaymentDiscountPercent": "decimal",
					"prepmtPaymentTermsCode": "string",
					"prepmtPmtDiscountDate": "date",
					"prepmtPostingDescription": "string",
					"pricesIncludingVat": "boolean",
					"printPostedDocuments": "boolean",
					"promisedDeliveryDate": "date",
					"quoteAccepted": "boolean",
					"quoteAcceptedDate": "date",
					"quoteNumber": "string",
					"quoteSentToCustomer": "dateTime",
					"quoteValidUntilDate": "date",
					"reasonCode": "string",
					"recalculateInvoiceDisc": "boolean",
					"receive": "boolean",
					"requestedDeliveryDate": "date",
					"reserve": "reserve",
					"responsibilityCenter": "string",
					"returnReceiptNumber": "string",
					"returnReceiptNumberSeries": "string",
					"salespersonCode": "string",
					"sellToAddress": "string",
					"sellToAddress2": "string",
					"sellToCity": "string",
					"sellToContact": "string",
					"sellToContactNumber": "string",
					"sellToCountryRegionCode": "string",
					"sellToCounty": "string",
					"sellToCustomerName": "string",
					"sellToCustomerName2": "string",
					"sellToCustomerNumber": "string",
					"sellToCustomerTemplateCode": "string",
					"sellToIcPartnerCode": "string",
					"sellToPostCode": "string",
					"sendIcDocument": "boolean",
					"ship": "boolean",
					"shipToAddress": "string",
					"shipToAddress2": "string",
					"shipToCity": "string",
					"shipToCode": "string",
					"shipToContact": "string",
					"shipToCountryRegionCode": "string",
					"shipToCounty": "string",
					"shipToName": "string",
					"shipToName2": "string",
					"shipToPostCode": "string",
					"shipmentDate": "date",
					"shipmentMethodCode": "string",
					"shipped": "boolean",
					"shippedNotInvoiced": "boolean",
					"shippingAdvice": "shippingAdvice",
					"shippingAgentCode": "string",
					"shippingAgentServiceCode": "string",
					"shippingNumber": "string",
					"shippingNumberSeries": "string",
					"shippingTime": "string",
					"shortcutDimension1Code": "string",
					"shortcutDimension2Code": "string",
					"status": "status",
					"taxAreaCode": "string",
					"taxLiable": "boolean",
					"transactionSpecification": "string",
					"transactionType": "string",
					"transportMethod": "string",
					"vatBaseDiscountPercent": "decimal",
					"vatBusPostingGroup": "string",
					"vatCountryRegionCode": "string",
					"vatRegistrationNumber": "string",
					"workflowSalesDocumentLines": "Sales_Document_Line_Entity_List",
					"yourReference": "string"
				},
				"workflowSalesDocuments_Fields": "string",
				"workflowSalesDocuments_Filter": {
					"Criteria": "string",
					"Field": "workflowSalesDocuments_Fields"
				},
				"workflowSalesDocuments_List": {
					"workflowSalesDocuments": "workflowSalesDocuments"
				}
			}
		},
		"workflowVendors": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"workflowVendors": "workflowVendors"
				},
				"CreateMultiple": {
					"workflowVendors_List": "workflowVendors_List"
				},
				"CreateMultiple_Result": {
					"workflowVendors_List": "workflowVendors_List"
				},
				"Create_Result": {
					"workflowVendors": "workflowVendors"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"number": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"workflowVendors": "workflowVendors"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "workflowVendors_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "workflowVendors_List"
				},
				"Read_Result": {
					"workflowVendors": "workflowVendors"
				},
				"Update": {
					"workflowVendors": "workflowVendors"
				},
				"UpdateMultiple": {
					"workflowVendors_List": "workflowVendors_List"
				},
				"UpdateMultiple_Result": {
					"workflowVendors_List": "workflowVendors_List"
				},
				"Update_Result": {
					"workflowVendors": "workflowVendors"
				},
				"applicationMethod": "string",
				"blocked": "string",
				"partnerType": "string",
				"workflowVendors": {
					"Key": "string",
					"address": "string",
					"address2": "string",
					"amtRcdNotInvoiced": "decimal",
					"amtRcdNotInvoicedLcy": "decimal",
					"applicationMethod": "applicationMethod",
					"balance": "decimal",
					"balanceDue": "decimal",
					"balanceDueLcy": "decimal",
					"balanceLcy": "decimal",
					"baseCalendarCode": "string",
					"blockPaymentTolerance": "boolean",
					"blocked": "blocked",
					"budgetedAmount": "decimal",
					"buyFromNumberOfArchivedDoc": "int",
					"cashFlowPaymentTermsCode": "string",
					"city": "string",
					"comment": "boolean",
					"contact": "string",
					"countryRegionCode": "string",
					"county": "string",
					"crMemoAmounts": "decimal",
					"crMemoAmountsLcy": "decimal",
					"creditAmount": "decimal",
					"creditAmountLcy": "decimal",
					"creditorNumber": "string",
					"currencyCode": "string",
					"currencyFilter": "string",
					"currencyId": "string",
					"dateFilter": "date",
					"debitAmount": "decimal",
					"debitAmountLcy": "decimal",
					"documentSendingProfile": "string",
					"eMail": "string",
					"faxNumber": "string",
					"finChargeMemoAmountsLcy": "decimal",
					"finChargeTermsCode": "string",
					"financeChargeMemoAmounts": "decimal",
					"genBusPostingGroup": "string",
					"gln": "string",
					"globalDimension1Code": "string",
					"globalDimension1Filter": "string",
					"globalDimension2Code": "string",
					"globalDimension2Filter": "string",
					"homePage": "string",
					"icPartnerCode": "string",
					"id": "string",
					"invAmountsLcy": "decimal",
					"invDiscountsLcy": "decimal",
					"invoiceAmounts": "decimal",
					"invoiceDiscCode": "string",
					"languageCode": "string",
					"lastDateModified": "date",
					"lastModifiedDateTime": "dateTime",
					"leadTimeCalculation": "string",
					"locationCode": "string",
					"name": "string",
					"name2": "string",
					"netChange": "decimal",
					"netChangeLcy": "decimal",
					"number": "string",
					"numberOfBlanketOrders": "int",
					"numberOfCreditMemos": "int",
					"numberOfIncomingDocuments": "int",
					"numberOfInvoices": "int",
					"numberOfOrderAddresses": "int",
					"numberOfOrders": "int",
					"numberOfPstdCreditMemos": "int",
					"numberOfPstdInvoices": "int",
					"numberOfPstdReceipts": "int",
					"numberOfPstdReturnShipments": "int",
					"numberOfQuotes": "int",
					"numberOfReturnOrders": "int",
					"numberSeries": "string",
					"otherAmounts": "decimal",
					"otherAmountsLcy": "decimal",
					"ourAccountNumber": "string",
					"outstandingInvoices": "decimal",
					"outstandingInvoicesLcy": "decimal",
					"outstandingOrders": "decimal",
					"outstandingOrdersLcy": "decimal",
					"partnerType": "partnerType",
					"payToNumberOfArchivedDoc": "int",
					"payToNumberOfBlanketOrders": "int",
					"payToNumberOfCreditMemos": "int",
					"payToNumberOfInvoices": "int",
					"payToNumberOfOrders": "int",
					"payToNumberOfPstdCrMemos": "int",
					"payToNumberOfPstdInvoices": "int",
					"payToNumberOfPstdReceipts": "int",
					"payToNumberOfPstdReturnS": "int",
					"payToNumberOfQuotes": "int",
					"payToNumberOfReturnOrders": "int",
					"payToVendorNumber": "string",
					"paymentMethodCode": "string",
					"paymentMethodId": "string",
					"paymentTermsCode": "string",
					"paymentTermsId": "string",
					"payments": "decimal",
					"paymentsLcy": "decimal",
					"phoneNumber": "string",
					"pmtDiscToleranceLcy": "decimal",
					"pmtDiscountsLcy": "decimal",
					"pmtToleranceLcy": "decimal",
					"postCode": "string",
					"preferredBankAccountCode": "string",
					"prepaymentPercent": "decimal",
					"pricesIncludingVat": "boolean",
					"primaryContactNumber": "string",
					"priority": "int",
					"purchaserCode": "string",
					"purchasesLcy": "decimal",
					"refunds": "decimal",
					"refundsLcy": "decimal",
					"reminderAmounts": "decimal",
					"reminderAmountsLcy": "decimal",
					"responsibilityCenter": "string",
					"searchName": "string",
					"shipmentMethodCode": "string",
					"shippingAgentCode": "string",
					"statisticsGroup": "int",
					"taxAreaCode": "string",
					"taxLiable": "boolean",
					"telexAnswerBack": "string",
					"telexNumber": "string",
					"territoryCode": "string",
					"validateEuVatRegNumber": "boolean",
					"vatBusPostingGroup": "string",
					"vatRegistrationNumber": "string",
					"vendorPostingGroup": "string"
				},
				"workflowVendors_Fields": "string",
				"workflowVendors_Filter": {
					"Criteria": "string",
					"Field": "workflowVendors_Fields"
				},
				"workflowVendors_List": {
					"workflowVendors": "workflowVendors"
				}
			}
		},
		"workflowWebhookSubscriptions": {
			"f": {
				"Create": {
					"i": "Create",
					"o": "Create_Result"
				},
				"CreateMultiple": {
					"i": "CreateMultiple",
					"o": "CreateMultiple_Result"
				},
				"Delete": {
					"i": "Delete",
					"o": "Delete_Result"
				},
				"GetRecIdFromKey": {
					"i": "GetRecIdFromKey",
					"o": "GetRecIdFromKey_Result"
				},
				"IsUpdated": {
					"i": "IsUpdated",
					"o": "IsUpdated_Result"
				},
				"Read": {
					"i": "Read",
					"o": "Read_Result"
				},
				"ReadByRecId": {
					"i": "ReadByRecId",
					"o": "ReadByRecId_Result"
				},
				"ReadMultiple": {
					"i": "ReadMultiple",
					"o": "ReadMultiple_Result"
				},
				"Update": {
					"i": "Update",
					"o": "Update_Result"
				},
				"UpdateMultiple": {
					"i": "UpdateMultiple",
					"o": "UpdateMultiple_Result"
				}
			},
			"sp": "Page",
			"t": {
				"Create": {
					"workflowWebhookSubscriptions": "workflowWebhookSubscriptions"
				},
				"CreateMultiple": {
					"workflowWebhookSubscriptions_List": "workflowWebhookSubscriptions_List"
				},
				"CreateMultiple_Result": {
					"workflowWebhookSubscriptions_List": "workflowWebhookSubscriptions_List"
				},
				"Create_Result": {
					"workflowWebhookSubscriptions": "workflowWebhookSubscriptions"
				},
				"Delete": {
					"Key": "string"
				},
				"Delete_Result": {
					"Delete_Result": "boolean"
				},
				"GetRecIdFromKey": {
					"Key": "string"
				},
				"GetRecIdFromKey_Result": {
					"GetRecIdFromKey_Result": "string"
				},
				"IsUpdated": {
					"Key": "string"
				},
				"IsUpdated_Result": {
					"IsUpdated_Result": "boolean"
				},
				"Read": {
					"id": "string"
				},
				"ReadByRecId": {
					"recId": "string"
				},
				"ReadByRecId_Result": {
					"workflowWebhookSubscriptions": "workflowWebhookSubscriptions"
				},
				"ReadMultiple": {
					"bookmarkKey": "string",
					"filter": "workflowWebhookSubscriptions_Filter",
					"setSize": "int"
				},
				"ReadMultiple_Result": {
					"ReadMultiple_Result": "workflowWebhookSubscriptions_List"
				},
				"Read_Result": {
					"workflowWebhookSubscriptions": "workflowWebhookSubscriptions"
				},
				"Update": {
					"workflowWebhookSubscriptions": "workflowWebhookSubscriptions"
				},
				"UpdateMultiple": {
					"workflowWebhookSubscriptions_List": "workflowWebhookSubscriptions_List"
				},
				"UpdateMultiple_Result": {
					"workflowWebhookSubscriptions_List": "workflowWebhookSubscriptions_List"
				},
				"Update_Result": {
					"workflowWebhookSubscriptions": "workflowWebhookSubscriptions"
				},
				"workflowWebhookSubscriptions": {
					"Key": "string",
					"clientId": "string",
					"clientType": "string",
					"conditions": "string",
					"enabled": "boolean",
					"eventCode": "string",
					"id": "string",
					"notificationUrl": "string"
				},
				"workflowWebhookSubscriptions_Fields": "string",
				"workflowWebhookSubscriptions_Filter": {
					"Criteria": "string",
					"Field": "workflowWebhookSubscriptions_Fields"
				},
				"workflowWebhookSubscriptions_List": {
					"workflowWebhookSubscriptions": "workflowWebhookSubscriptions"
				}
			}
		}
	}
);

Services.nav_odata = nav_odata;

const OK365_Customer_List = new NavisionDataset(
	'OK365_Customer_List', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Customer_List"
	}
);

Services.OK365_Customer_List = OK365_Customer_List;

const OK365_Location_List = new NavisionDataset(
	'OK365_Location_List', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Location_List"
	}
);

Services.OK365_Location_List = OK365_Location_List;

const OK365_Shipping_Address_List = new NavisionDataset(
	'OK365_Shipping_Address_List', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Shipping_Address_List"
	}
);

Services.OK365_Shipping_Address_List = OK365_Shipping_Address_List;

const OK365_Item_List = new NavisionDataset(
	'OK365_Item_List', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Item_List"
	}
);

Services.OK365_Item_List = OK365_Item_List;

const l_outlet = new LocalTable(
	'l_outlet', 
	{
		"fields": {
			"Address": {
				"tp": "text"
			},
			"Address_2": {
				"tp": "text"
			},
			"City": {
				"tp": "text"
			},
			"Contact": {
				"tp": "text"
			},
			"Country_Region_Code": {
				"tp": "text"
			},
			"Fax_No": {
				"tp": "text"
			},
			"Location_Code": {
				"tp": "text"
			},
			"Name": {
				"tp": "text"
			},
			"Phone_No": {
				"tp": "text"
			},
			"Post_Code": {
				"tp": "text"
			},
			"name": {
				"tp": "text"
			},
			"value": {
				"tp": "text"
			}
		}
	}
);

Services.l_outlet = l_outlet;

const OK365_Posted_Sales_Invoice_List = new NavisionDataset(
	'OK365_Posted_Sales_Invoice_List', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Posted_Sales_Invoice_List"
	}
);

Services.OK365_Posted_Sales_Invoice_List = OK365_Posted_Sales_Invoice_List;

const OK365_Order_List = new NavisionDataset(
	'OK365_Order_List', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Order_List"
	}
);

Services.OK365_Order_List = OK365_Order_List;

const l_orderlist = new LocalTable(
	'l_orderlist', 
	{
		"fields": {
			"Amount": {
				"tp": "text"
			},
			"Amount_Including_VAT": {
				"tp": "text"
			},
			"Amt_Ship_Not_Inv_LCY": {
				"tp": "text"
			},
			"Amt_Ship_Not_Inv_LCY_Base": {
				"tp": "text"
			},
			"Bill_to_Country_Region_Code_Link": {
				"tp": "text"
			},
			"Bill_to_Customer_No": {
				"tp": "text"
			},
			"Bill_to_Name": {
				"tp": "text"
			},
			"Bill_to_Post_Code": {
				"tp": "text"
			},
			"Bill_to_Post_Code_Link": {
				"tp": "text"
			},
			"Completely_Shipped": {
				"tp": "text"
			},
			"Currency_Code_Link": {
				"tp": "text"
			},
			"Document_Date": {
				"tp": "text"
			},
			"Document_Type": {
				"tp": "text"
			},
			"Due_Date": {
				"tp": "text"
			},
			"Job_Queue_Status": {
				"tp": "text"
			},
			"Key": {
				"tp": "text"
			},
			"Location_Code": {
				"tp": "text"
			},
			"Location_Code_Link": {
				"tp": "text"
			},
			"Location_Filter_Link": {
				"tp": "text"
			},
			"No": {
				"tp": "text"
			},
			"Payment_Discount_Percent": {
				"tp": "text"
			},
			"Payment_Terms_Code": {
				"tp": "text"
			},
			"Payment_Terms_Code_Link": {
				"tp": "text"
			},
			"Posting_Date": {
				"tp": "text"
			},
			"Posting_Description": {
				"tp": "text"
			},
			"Requested_Delivery_Date": {
				"tp": "text"
			},
			"Salesperson_Code": {
				"tp": "text"
			},
			"Salesperson_Code_Link": {
				"tp": "text"
			},
			"Sell_to_Country_Region_Code_Link": {
				"tp": "text"
			},
			"Sell_to_Customer_Name": {
				"tp": "text"
			},
			"Sell_to_Customer_No": {
				"tp": "text"
			},
			"Sell_to_Post_Code": {
				"tp": "text"
			},
			"Sell_to_Post_Code_Link": {
				"tp": "text"
			},
			"Ship_to_Code_Link": {
				"tp": "text"
			},
			"Ship_to_Country_Region_Code_Link": {
				"tp": "text"
			},
			"Ship_to_Name": {
				"tp": "text"
			},
			"Ship_to_Post_Code": {
				"tp": "text"
			},
			"Ship_to_Post_Code_Link": {
				"tp": "text"
			},
			"Shipment_Date": {
				"tp": "text"
			},
			"Shipping_Advice": {
				"tp": "text"
			},
			"Shortcut_Dimension_1_Code": {
				"tp": "text"
			},
			"Shortcut_Dimension_2_Code": {
				"tp": "text"
			},
			"Status": {
				"tp": "text"
			}
		}
	}
);

Services.l_orderlist = l_orderlist;

const l_cart = new LocalTable(
	'l_cart', 
	{
		"fields": {
			"CustomerNo": {
				"tp": "text"
			},
			"GetPicture": {
				"tp": "text"
			},
			"ItemCode": {
				"tp": "text"
			},
			"ItemName": {
				"tp": "text"
			},
			"LineTotal": {
				"tp": "text"
			},
			"LocationCode": {
				"tp": "text"
			},
			"Price": {
				"tp": "text"
			},
			"Quantity": {
				"tp": "text"
			},
			"Type": {
				"tp": "text"
			},
			"UOM": {
				"tp": "text"
			},
			"VAT_Prod_Posting_Group": {
				"tp": "text"
			},
			"_id": {
				"tp": "text"
			}
		},
		"uniqueKey": true
	}
);

Services.l_cart = l_cart;

const OK365_Item_Unit_of_Measure = new NavisionDataset(
	'OK365_Item_Unit_of_Measure', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Item_Unit_of_Measure"
	}
);

Services.OK365_Item_Unit_of_Measure = OK365_Item_Unit_of_Measure;

const l_empty_bottle = new LocalTable(
	'l_empty_bottle', 
	{
		"fields": {
			"UOM": {
				"tp": "text"
			},
			"name": {
				"tp": "text"
			},
			"price": {
				"tp": "text"
			},
			"quantity": {
				"tp": "text"
			}
		}
	}
);

Services.l_empty_bottle = l_empty_bottle;

const OK365_Order_Entry = new NavisionDataset(
	'OK365_Order_Entry', 
	{
		"connector": "nav",
		"getDataFunction": "ReadMultiple",
		"insertDataFunction": "Create",
		"listKey": "data",
		"serviceName": "OK365_Order_Entry",
		"updateDataFunction": "Update"
	}
);

Services.OK365_Order_Entry = OK365_Order_Entry;

const l_customer = new LocalTable(
	'l_customer', 
	{
		"fields": {
			"Address": {
				"tp": "text"
			},
			"Address_2_OkFldSls": {
				"tp": "text"
			},
			"Application_Method": {
				"tp": "text"
			},
			"Available_Credit_Limit_OKFldSls": {
				"tp": "text"
			},
			"Balance_Due_LCY": {
				"tp": "text"
			},
			"Balance_LCY": {
				"tp": "text"
			},
			"Balance_LCY_OkFldSls": {
				"tp": "text"
			},
			"Blocked": {
				"tp": "text"
			},
			"City_OkFldSls": {
				"tp": "text"
			},
			"Combine_Shipments": {
				"tp": "text"
			},
			"Country_Region_Code_Link": {
				"tp": "text"
			},
			"Credit_Limit_LCY": {
				"tp": "text"
			},
			"Credit_Limit_LCY_OkFldSls": {
				"tp": "text"
			},
			"Currency_Code_Link": {
				"tp": "text"
			},
			"Currency_Filter_Link": {
				"tp": "text"
			},
			"Cust_Posting_Group_OkFldSls": {
				"tp": "text"
			},
			"Custgroup_Dimension_OkFldSls": {
				"tp": "text"
			},
			"Customer_Code": {
				"tp": "text"
			},
			"Customer_Posting_Group": {
				"tp": "text"
			},
			"Date_Filter": {
				"tp": "text"
			},
			"Friday": {
				"tp": "text"
			},
			"Gen_Bus_Posting_Group": {
				"tp": "text"
			},
			"Key": {
				"tp": "text"
			},
			"Last_Date_Modified": {
				"tp": "text"
			},
			"Line_No": {
				"tp": "text"
			},
			"Location_Code_Link": {
				"tp": "text"
			},
			"Monday": {
				"tp": "text"
			},
			"Name": {
				"tp": "text"
			},
			"No": {
				"tp": "text"
			},
			"Payment_Term_Code_OkFldSls": {
				"tp": "text"
			},
			"Payment_Terms_Code": {
				"tp": "text"
			},
			"Payment_Terms_Code_Link": {
				"tp": "text"
			},
			"Payments_LCY": {
				"tp": "text"
			},
			"Post_Code": {
				"tp": "text"
			},
			"Post_Code_Link": {
				"tp": "text"
			},
			"Post_Code_OkFldSls": {
				"tp": "text"
			},
			"Privacy_Blocked": {
				"tp": "text"
			},
			"Reserve": {
				"tp": "text"
			},
			"Route_No": {
				"tp": "text"
			},
			"Sales_LCY": {
				"tp": "text"
			},
			"Salesperson_Code": {
				"tp": "text"
			},
			"Salesperson_Code_Link": {
				"tp": "text"
			},
			"Salesperson_Code_OkFldSls": {
				"tp": "text"
			},
			"Saturday": {
				"tp": "text"
			},
			"Sequence": {
				"tp": "text"
			},
			"Ship_to_Code_Link": {
				"tp": "text"
			},
			"Shipping_Advice": {
				"tp": "text"
			},
			"Sunday": {
				"tp": "text"
			},
			"Thursday": {
				"tp": "text"
			},
			"Tuesday": {
				"tp": "text"
			},
			"VAT_Bus_Posting_Group": {
				"tp": "text"
			},
			"VAT_Buss_Posting_Grp_OkFldSls": {
				"tp": "text"
			},
			"Wednesday": {
				"tp": "text"
			}
		}
	}
);

Services.l_customer = l_customer;

const OK365_Mobile_User_Master = new NavisionDataset(
	'OK365_Mobile_User_Master', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Mobile_User_Master"
	}
);

Services.OK365_Mobile_User_Master = OK365_Mobile_User_Master;

const OK365_Sales__x0026__Receivable_Setup = new NavisionDataset(
	'OK365_Sales__x0026__Receivable_Setup', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Sales__x0026__Receivable_Setup"
	}
);

Services.OK365_Sales__x0026__Receivable_Setup = OK365_Sales__x0026__Receivable_Setup;

const OK365_Route_And_Customer_Priority = new NavisionDataset(
	'OK365_Route_And_Customer_Priority', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Route_And_Customer_Priority"
	}
);

Services.OK365_Route_And_Customer_Priority = OK365_Route_And_Customer_Priority;

const OK365_Route_Master = new NavisionDataset(
	'OK365_Route_Master', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Route_Master"
	}
);

Services.OK365_Route_Master = OK365_Route_Master;

const OK365_Order_Entry_Line = new NavisionDataset(
	'OK365_Order_Entry_Line', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Order_Entry_Line"
	}
);

Services.OK365_Order_Entry_Line = OK365_Order_Entry_Line;

const l_orderlist_details = new LocalTable(
	'l_orderlist_details', 
	{
		"fields": {
			"Description": {
				"tp": "text"
			},
			"No": {
				"tp": "text"
			},
			"Quantity": {
				"tp": "text"
			},
			"Unit_Price": {
				"tp": "text"
			},
			"Unit_of_Measure": {
				"tp": "text"
			},
			"_id": {
				"tp": "text"
			}
		},
		"uniqueKey": true
	}
);

Services.l_orderlist_details = l_orderlist_details;

const l_orderlist_empty_details = new LocalTable(
	'l_orderlist_empty_details', 
	{
		"fields": {
			"No": {
				"tp": "text"
			},
			"Price": {
				"tp": "text"
			},
			"Quantity": {
				"tp": "text"
			}
		}
	}
);

Services.l_orderlist_empty_details = l_orderlist_empty_details;

const OK365_Customer_Card = new NavisionDataset(
	'OK365_Customer_Card', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Customer_Card"
	}
);

Services.OK365_Customer_Card = OK365_Customer_Card;

const OK365_GST_Setup = new NavisionDataset(
	'OK365_GST_Setup', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_GST_Setup"
	}
);

Services.OK365_GST_Setup = OK365_GST_Setup;

const OK365_Posted_Sales_Invoice_Header = new NavisionDataset(
	'OK365_Posted_Sales_Invoice_Header', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Posted_Sales_Invoice_Header"
	}
);

Services.OK365_Posted_Sales_Invoice_Header = OK365_Posted_Sales_Invoice_Header;

const OK365_Company_Information = new NavisionDataset(
	'OK365_Company_Information', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Company_Information"
	}
);

Services.OK365_Company_Information = OK365_Company_Information;

const l_delivery = new LocalTable(
	'l_delivery', 
	{
		"fields": []
	}
);

Services.l_delivery = l_delivery;

const l_delivery_details = new LocalTable(
	'l_delivery_details', 
	{
		"fields": {
			"Description": {
				"tp": "text"
			},
			"No": {
				"tp": "text"
			},
			"Quantity": {
				"tp": "text"
			},
			"Unit_Price": {
				"tp": "text"
			},
			"Unit_of_Measure": {
				"tp": "text"
			}
		}
	}
);

Services.l_delivery_details = l_delivery_details;

const l_delivery_empty_details = new LocalTable(
	'l_delivery_empty_details', 
	{
		"fields": {
			"No": {
				"tp": "text"
			},
			"Price": {
				"tp": "text"
			},
			"Quantity": {
				"tp": "text"
			}
		}
	}
);

Services.l_delivery_empty_details = l_delivery_empty_details;

const l_Item_List = new LocalTable(
	'l_Item_List', 
	{
		"autoClear": false,
		"autoLoad": true,
		"fields": {
			"Assembly_BOM": {
				"tp": "text"
			},
			"Assembly_Policy": {
				"tp": "text"
			},
			"Base_Unit_of_Measure": {
				"tp": "text"
			},
			"Base_Unit_of_Measure_Link": {
				"tp": "text"
			},
			"Blocked": {
				"tp": "text"
			},
			"Cost_is_Adjusted": {
				"tp": "text"
			},
			"Costing_Method": {
				"tp": "text"
			},
			"Created_From_Nonstock_Item": {
				"tp": "text"
			},
			"Description": {
				"tp": "text"
			},
			"Flushing_Method": {
				"tp": "text"
			},
			"Gen_Prod_Posting_Group": {
				"tp": "text"
			},
			"GetPicture": {
				"tp": "text"
			},
			"Indirect_Cost_Percent": {
				"tp": "text"
			},
			"Inventory": {
				"tp": "text"
			},
			"Inventory_Posting_Group": {
				"tp": "text"
			},
			"Item_Category_Code_Link": {
				"tp": "text"
			},
			"Key": {
				"tp": "text"
			},
			"Last_Date_Modified": {
				"tp": "text"
			},
			"Last_Direct_Cost": {
				"tp": "text"
			},
			"Location_Filter": {
				"tp": "text"
			},
			"Location_Filter_Link": {
				"tp": "text"
			},
			"Manufacturing_Policy": {
				"tp": "text"
			},
			"No": {
				"tp": "text"
			},
			"OK365_Item_OkFldSls": {
				"tp": "text"
			},
			"Overhead_Rate": {
				"tp": "text"
			},
			"Price_Profit_Calculation": {
				"tp": "text"
			},
			"Profit_Percent": {
				"tp": "text"
			},
			"Purch_Unit_of_Measure": {
				"tp": "text"
			},
			"Purch_Unit_of_Measure_Link": {
				"tp": "text"
			},
			"Replenishment_System": {
				"tp": "text"
			},
			"Sales_Unit_of_Measure": {
				"tp": "text"
			},
			"Sales_Unit_of_Measure_Link": {
				"tp": "text"
			},
			"Search_Description": {
				"tp": "text"
			},
			"Standard_Cost": {
				"tp": "text"
			},
			"Stockkeeping_Unit_Exists": {
				"tp": "text"
			},
			"Substitutes_Exist": {
				"tp": "text"
			},
			"Type": {
				"tp": "text"
			},
			"Unit_Cost": {
				"tp": "text"
			},
			"Unit_Price": {
				"tp": "text"
			},
			"Unit_of_Measure_Filter_Link": {
				"tp": "text"
			},
			"VAT_Prod_Posting_Group": {
				"tp": "text"
			},
			"_id": {
				"tp": "text"
			}
		},
		"uniqueKey": true
	}
);

Services.l_Item_List = l_Item_List;

const l_OrderLines = new LocalTable(
	'l_OrderLines', 
	{
		"autoClear": false,
		"autoLoad": true,
		"uniqueKey": true
	}
);

Services.l_OrderLines = l_OrderLines;

const l_OrderEmptyLines = new LocalTable(
	'l_OrderEmptyLines', 
	{
		"autoClear": false,
		"autoLoad": true,
		"uniqueKey": true
	}
);

Services.l_OrderEmptyLines = l_OrderEmptyLines;

const OK365_CashReceipt = new NavisionDataset(
	'OK365_CashReceipt', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_CashReceipt"
	}
);

Services.OK365_CashReceipt = OK365_CashReceipt;

const l_Route_Master = new LocalTable(
	'l_Route_Master', 
	{
		"autoClear": false,
		"autoLoad": true,
		"uniqueKey": true
	}
);

Services.l_Route_Master = l_Route_Master;

const l_Posted_Sales_Invoice_List = new LocalTable(
	'l_Posted_Sales_Invoice_List', 
	{
		"autoClear": false,
		"autoLoad": true,
		"uniqueKey": false
	}
);

Services.l_Posted_Sales_Invoice_List = l_Posted_Sales_Invoice_List;

const l_settings = new LocalTable(
	'l_settings', 
	{
		"autoClear": false,
		"autoLoad": true,
		"fields": {
			"Company": {
				"tp": "text"
			},
			"Printer": {
				"tp": "text"
			},
			"Username": {
				"tp": "text"
			},
			"_id": {
				"tp": "text"
			},
			"lang": {
				"tp": "text"
			}
		},
		"uniqueKey": true
	}
);

Services.l_settings = l_settings;

const OK365_Item_Card = new NavisionDataset(
	'OK365_Item_Card', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Item_Card"
	}
);

Services.OK365_Item_Card = OK365_Item_Card;

const OK365_Customer_Ledger_Entries = new NavisionDataset(
	'OK365_Customer_Ledger_Entries', 
	{
		"connector": "nav",
		"getDataFunction": "ReadMultiple",
		"insertDataFunction": "CreateMultiple",
		"listKey": "data",
		"serviceName": "OK365_Customer_Ledger_Entries",
		"updateDataFunction": "UpdateMultiple"
	}
);

Services.OK365_Customer_Ledger_Entries = OK365_Customer_Ledger_Entries;

const l_cash_collection_invoice = new LocalTable(
	'l_cash_collection_invoice', 
	{
		"fields": {
			"Amount": {
				"tp": "text"
			},
			"Document_No": {
				"tp": "text"
			},
			"Key": {
				"tp": "text"
			},
			"Posting_Date": {
				"tp": "text"
			},
			"Remaining_Amount": {
				"tp": "text"
			}
		}
	}
);

Services.l_cash_collection_invoice = l_cash_collection_invoice;

const OK365_Sales_Return_Order = new NavisionDataset(
	'OK365_Sales_Return_Order', 
	{
		"connector": "nav_odata",
		"getDataFunction": "ReadMultiple",
		"insertDataFunction": "CreateMultiple",
		"listKey": "data",
		"serviceName": "OK365_Sales_Return_Order",
		"updateDataFunction": "UpdateMultiple"
	}
);

Services.OK365_Sales_Return_Order = OK365_Sales_Return_Order;

const l_Sales_Return_Order = new LocalTable(
	'l_Sales_Return_Order', 
	{
		"autoClear": false,
		"autoLoad": true,
		"fields": {
			"Key": {
				"tp": "text"
			},
			"_id": {
				"tp": "text"
			}
		},
		"uniqueKey": true
	}
);

Services.l_Sales_Return_Order = l_Sales_Return_Order;

const l_Sales_Return_Lines = new LocalTable(
	'l_Sales_Return_Lines', 
	{
		"autoClear": false,
		"autoLoad": true,
		"fields": {
			"Description": {
				"tp": "text"
			},
			"GetPicture": {
				"tp": "text"
			},
			"Key": {
				"tp": "text"
			},
			"Line_Amount": {
				"tp": "text"
			},
			"Location_Code": {
				"tp": "text"
			},
			"No": {
				"tp": "text"
			},
			"Quantity": {
				"tp": "text"
			},
			"Return_Reason_Code": {
				"tp": "text"
			},
			"Unit_Price": {
				"tp": "text"
			},
			"Unit_of_Measure_Code": {
				"tp": "text"
			},
			"_id": {
				"tp": "text"
			}
		},
		"uniqueKey": true
	}
);

Services.l_Sales_Return_Lines = l_Sales_Return_Lines;

const l_Sales_Return_Order_Lines = new LocalTable(
	'l_Sales_Return_Order_Lines', 
	{
		"autoClear": false,
		"autoLoad": true,
		"fields": {
			"Key": {
				"tp": "text"
			},
			"_id": {
				"tp": "text"
			}
		},
		"uniqueKey": true
	}
);

Services.l_Sales_Return_Order_Lines = l_Sales_Return_Order_Lines;

const OK365_Reason_code = new NavisionDataset(
	'OK365_Reason_code', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Reason_code"
	}
);

Services.OK365_Reason_code = OK365_Reason_code;

const OK365_Return_reason_code = new NavisionDataset(
	'OK365_Return_reason_code', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Return_reason_code"
	}
);

Services.OK365_Return_reason_code = OK365_Return_reason_code;

const OK365_Physical_Inventory_Journal = new NavisionDataset(
	'OK365_Physical_Inventory_Journal', 
	{
		"connector": "nav",
		"getDataFunction": "ReadMultiple",
		"insertDataFunction": "CreateMultiple",
		"listKey": "data",
		"serviceName": "OK365_Physical_Inventory_Journal",
		"updateDataFunction": "UpdateMultiple"
	}
);

Services.OK365_Physical_Inventory_Journal = OK365_Physical_Inventory_Journal;

const l_Physical_Inventory_Journal = new LocalTable(
	'l_Physical_Inventory_Journal', 
	{
		"autoClear": false,
		"autoLoad": true,
		"fields": {
			"Key": {
				"tp": "text"
			},
			"_id": {
				"tp": "text"
			}
		},
		"uniqueKey": true
	}
);

Services.l_Physical_Inventory_Journal = l_Physical_Inventory_Journal;

const OK365_Transfer_Order_List = new NavisionDataset(
	'OK365_Transfer_Order_List', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Transfer_Order_List"
	}
);

Services.OK365_Transfer_Order_List = OK365_Transfer_Order_List;

const l_Transfer_Order_List = new LocalTable(
	'l_Transfer_Order_List', 
	{
		"autoClear": false,
		"autoLoad": true,
		"fields": {
			"Key": {
				"tp": "text"
			},
			"_id": {
				"tp": "text"
			}
		},
		"uniqueKey": true
	}
);

Services.l_Transfer_Order_List = l_Transfer_Order_List;

const OK365_Transfer_Orders = new NavisionDataset(
	'OK365_Transfer_Orders', 
	{
		"connector": "nav",
		"listKey": "data",
		"serviceName": "OK365_Transfer_Orders"
	}
);

Services.OK365_Transfer_Orders = OK365_Transfer_Orders;

const l_TransferLines = new LocalTable(
	'l_TransferLines', 
	{
		"autoClear": false,
		"autoLoad": true,
		"fields": {
			"Key": {
				"tp": "text"
			},
			"_id": {
				"tp": "text"
			}
		},
		"uniqueKey": true
	}
);

Services.l_TransferLines = l_TransferLines;

const OK365_Daily_Sales_Report = new NavisionDataset(
	'OK365_Daily_Sales_Report', 
	{
		"connector": "nav",
		"getDataFunction": "ReadMultiple",
		"listKey": "data",
		"serviceName": "OK365_Daily_Sales_Report"
	}
);

Services.OK365_Daily_Sales_Report = OK365_Daily_Sales_Report;

const l_nav_setting = new LocalTable(
	'l_nav_setting', 
	{
		"autoClear": false,
		"autoLoad": true,
		"fields": {
			"company": {
				"tp": "text"
			},
			"id": {
				"tp": "text"
			},
			"password": {
				"tp": "text"
			},
			"url": {
				"tp": "text"
			},
			"username": {
				"tp": "text"
			}
		},
		"uniqueKey": false
	}
);

Services.l_nav_setting = l_nav_setting;

const test = new NavisionConnector(
	'test', 
	{
		"company": "Cronus Demo",
		"password": "DUH3zZ4kGg+j9IC+2h+i2XqcmNBaHUNHpRQt+ZwWZMk=",
		"timeOut": 30000,
		"type": "soap",
		"url": "https://api.businesscentral.dynamics.com/v1.0/425096bb-f087-4c7d-ac68-7ac4ca13116d/Sandbox",
		"user": "AJAY.BABU_MSC-CONSULTING.COM.SG"
	}, 
	{
		"company": "Cronus Demo",
		"password": "DUH3zZ4kGg+j9IC+2h+i2XqcmNBaHUNHpRQt+ZwWZMk=",
		"timeOut": 30000,
		"type": "soap",
		"url": "https://api.businesscentral.dynamics.com/v1.0/425096bb-f087-4c7d-ac68-7ac4ca13116d/Sandbox",
		"user": "AJAY.BABU_MSC-CONSULTING.COM.SG"
	}
);

Services.test = test;

const OK365_Item_List2 = new NavisionDataset(
	'OK365_Item_List2', 
	{
		"connector": "test",
		"listKey": "data",
		"serviceName": "OK365_Item_List"
	}
);

Services.OK365_Item_List2 = OK365_Item_List2;

const adsqwqeq = new NavisionConnector(
	'adsqwqeq', 
	{
		"company": "Cronus Demo",
		"password": "",
		"timeOut": 30000,
		"type": "soap",
		"url": "",
		"user": "AJAY.BABU_MSC-CONSULTING.COM.SG"
	}, 
	{
		"company": "Cronus Demo",
		"password": "",
		"timeOut": 30000,
		"type": "soap",
		"url": "",
		"user": "AJAY.BABU_MSC-CONSULTING.COM.SG"
	}
);

Services.adsqwqeq = adsqwqeq;

const OK365_Order_List2 = new NavisionDataset(
	'OK365_Order_List2', 
	{
		"connector": "adsqwqeq",
		"listKey": "data",
		"serviceName": "OK365_Order_List"
	}
);

Services.OK365_Order_List2 = OK365_Order_List2;

const l_location = new LocalTable(
	'l_location', 
	{
		"autoClear": false,
		"autoLoad": true,
		"fields": {
			"Key": {
				"tp": "text"
			}
		},
		"uniqueKey": false
	}
);

Services.l_location = l_location;

const OK365_Item_List_STD = new NavisionDataset(
	'OK365_Item_List_STD', 
	{
		"connector": "nav_odata",
		"listKey": "data",
		"serviceName": "OK365_Item_List_STD"
	}
);

Services.OK365_Item_List_STD = OK365_Item_List_STD;

const l_order_details = new LocalTable(
	'l_order_details', 
	{
		"fields": {
			"Description": {
				"tp": "text"
			},
			"No": {
				"tp": "text"
			},
			"Quantity": {
				"tp": "text"
			},
			"Unit_Price": {
				"tp": "text"
			},
			"Unit_of_Measure": {
				"tp": "text"
			},
			"_id": {
				"tp": "text"
			}
		},
		"uniqueKey": true
	}
);

Services.l_order_details = l_order_details;


/*
 * /// Team Development ///
 */

/**
 * Local Table
 * Initiate all the local table required
 * then store them to services variable.
 */
const l_users = new LocalTable(
    // Name
    'l_users',
    // Properties
    {
        autoLoad: false
    }
);
const l_data_list = new LocalTable(
    // Name
    'l_data_list',
    // Properties
    {
        "autoLoad": false,
        "autoClear": false,
        "uniqueKey": false,
        "fields": {
          "value": {
            "tp": "text"
          }
        }
    }
);
// const l_OrderLines = new LocalTable(
//     // Name
//     'l_OrderLines',
//     // Properties
//     {
//         autoLoad: true,
//         uniqueKey: true
//     }
// );

Services.l_users = l_users;
Services.l_data_list = l_data_list;

// Services.l_OrderLines = l_OrderLines; // commented for now cos of conflict with vansales

/**
 * Navision Connector
 * Initiate all the navision table required
 * then store them to services variable.
 */
const n_nav = new NavisionConnector(
    // Name
    'n_nav', 
    // Properties
    { 
        type: 'odatav4', // soap or odatav4
        local: false,
        url: 'https://api.businesscentral.dynamics.com/v1.0/425096bb-f087-4c7d-ac68-7ac4ca13116d/Sandbox',
        company: 'Orangekloud',
        user: 'AJAY.BABU_MSC-CONSULTING.COM.SG',
        password: 'HPkEyf5dVCbyoYX7v5I4jcERvBwqPf7G7fX+V0ZT8R4=',
        timeout: 10000
    }, 
    // Services
    {
        OK365_Order_Entry: {
            type: 'Page'
        }
    }
);

Services.n_nav = n_nav;

/**
 * Navision Dataset
 * Initiate all the navision table required
 * then store them to services variable.
 */
const n_item_list = new NavisionDataset(
    // Name
    'n_item_list',
    // Properties
    {
        connector: 'n_nav',
        listKey: 'data',
        serviceName: 'OK365_Item_List',
        // limit: 5,
        autoLoad: false
    }
);

Services.n_item_list = n_item_list;


const l_todo = new LocalTable(
	'l_todo', 
	{
		"fields": {
			"title": {"tp": "text"},
			"category": {"tp": "text"},
			"priority": {"tp": "text"},
			"done": {"tp": "boolean"},
		},
		"uniqueKey": true
	}
);

Services.l_todo = l_todo;

/**
 * Navision Connector
 * Initiate all the navision table required
 * then store them to services variable.
 */
 const n_rc1= new RawConnector(
    'n_rc1', 
    { 
		url: 'https://jsonplaceholder.typicode.com', 
		local: true,
		timeout: 30000,
    },
);

Services.n_rc1 = n_rc1;

// Simple RawCAll
const l_abc = new LocalTable(
	'l_abc', 
	{
		"autoClear": false,
		"autoLoad": false,
		"uniqueKey": false
	}
);

Services.l_abc = l_abc;

const rc1 = new RawConnector(
	'rc1', 
	{
		"local": true,
		"timeOut": 30000,
		url: 'https://jsonplaceholder.typicode.com', 
	}
);

Services.rc1 = rc1;

export default Services;