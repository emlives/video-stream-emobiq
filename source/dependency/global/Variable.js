/**
 * Contains all the global variables for the application.
 * 
 * To be used by the framework.
 * 
 * Note: Don't delete this file, you can edit it but make
 * sure you know what you are doing as it might break your app.
 */
 import { StateName as SamplePage } from '../../reducer/data/SamplePage';
 import { StateName as CssPage } from '../../reducer/data/CssPage';

/**
  * Global variables in the app.
  * Modifiable by the app user.
  * For the setVar and getVar function.
  */
export const Variables = {};

/**
 * Global data used in the app.
 * Non modifiable by app user.
 * Used by the framework.
 */
export const Data = {};

/**
 * Global inactivity interval data used in the app.
 * Non modifiable by app user.
 * Used by the framework.
 */
export const InactivityInterval = {};

/**
 * Global timeouts variable to store the return value of timeout functions
 * For function to use to clear all timeouts
 */
export const Timeouts = [];

/**
 * Global intervals variable to store the return value of setInterval functions
 * For function to use to clear all setInterval
 */
export const Intervals = [];

/**
 * Global device data info
 * Data here will be populated when the app is loading
 * Use by functions 'deviceManufacturer', 'deviceSerial'
 */
export const DeviceData = {};

/**
 * Global refences for elements ref
 */
export const References = {
  [SamplePage]: {},
  [CssPage]: {}
};