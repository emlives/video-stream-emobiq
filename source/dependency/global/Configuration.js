/**
 * Contains all the configurations that was obtained from eMOBIQ.
 * 
 * To be used by the framework.
 * 
 * Note: Don't delete this file, you can edit it but make
 * sure you know what you are doing as it might break your app.
 */

/**
 * Connector URLs
 * Contains the different URL default connector
 */
export const ConnectorNavision = 'https://beta.emobiq.com/connector/nav/';
export const ConnectorRaw = 'https://beta.emobiq.com/connector/raw/';