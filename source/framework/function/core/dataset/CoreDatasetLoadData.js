import { getServiceByName } from '../../../core/Service';
import { errorLog } from '../../../core/Log';
import { setComponentStateProperties } from '../../../core/Component';
import { result } from '../../../core/Format';

import reducerAction from '../../../core/reducer/Action';
import produce from 'immer';

// Function name
export const name = 'coreDatasetLoadData';

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Function - Core - Dataset - CoreDatasetLoadData - ';

/**
 * Load the data of the dataset within the component specified
 * if necessary
 * @param {object} parameters
 *                  - uuid {string}    - uuid of the component
 *                  - dataset {string} - name of the dataset
 * 
 * @return {object} - result
 */
export default run = (stateName, pageProps, parameters) => {
    // Prepare the initial variable to be used
    let logFunctionName = 'run: ';
    
    // Validate the parameters
    if (!parameters.uuid || typeof parameters.uuid !=='string' || !parameters.dataset || typeof parameters.dataset !=='string') {
        errorLog(logInformation + logFunctionName + 'The parameter uuid and dataset are required and must be both a string.');
        return result(false, null, 'The parameter uuid and dataset are required and must be both a string.');
    }

    // Get the component from the services
    let service = getServiceByName(parameters.dataset);

    // Check if the component and method exists
    if (!service || !service.load) {
        errorLog(logInformation + logFunctionName + 'The component ' + parameters.dataset + ' or the method is not found.');
        return result(false, null, 'The component ' + parameters.dataset + ' or the method is not found.');
    }

    // Check if need to load the data
    if (!service.autoLoad) {
        return result(true, data, 'Autoload disabled nothing was triggered.')
    }

    // Prepare the callback for the load method
    let successCallback = (data, loadNext) => {
        // Prepare the payload for updating the reducer
        let payload = {
            component: parameters.uuid,
            data: data,
            append: loadNext
        };

        // Dispatch it to the redux store
        pageProps.dispatch(reducerAction(name, stateName, payload));
    }

    // Trigger the load data from the component
    let data = service.load(parameters, successCallback);

    // Check if any error occured
    if (!data.success) {
        errorLog(logInformation + logFunctionName + data.message);
        return result(false, null, data.message);
    }

    return result(true, data, 'Load data successfully triggered.')
}
 
/**
 * Reducer logic
 * @param {object} state 
 * @param {object} payload 
 * 
 * @return {object}
 */
export const reducerLogic = (state, payload) => {
    try {
        return produce(state, draftState => {
            // Manipulate draftState into the intended state change
            setComponentStateProperties(draftState, { componentData: { data: payload.data, append: payload.append } }, payload.component, 'uuid');
        });
    } catch(e) {
        return { ...state };
    }
}