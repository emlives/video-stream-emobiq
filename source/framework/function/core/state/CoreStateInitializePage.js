import reducerAction from '../../../core/reducer/Action';
import produce from 'immer';

import { errorLog } from '../../../core/Log';

import { setComponentStateProperties, getEmobiqComponent } from '../../../core/Component';

// Function name
export const name = 'coreStateInitializePage';

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Function - App - CoreStateInitializePage - ';

/**
 * Run the function
 * Update the property values in the state
 * @param {string} stateName 
 * @param {object} pageProps state of the current page
 * @param {object} parameters
 *                 - uuid {string} - The uuid of the component. (R)
 *                 - data {object} - Object of properties with values to store in state. (R)
 * 
 * @return {boolean}
 */
export default run = (stateName, pageProps, parameters, pageActions) => {
    // Prepare the initial variable to be used
    let logFunctionName = 'run: ';

    // Prepare the payload
    let payload = {
        component: parameters.uuid,
        data: parameters.data,
        pageActions: pageActions
    };
    
    // Dispatch it to the redux store
    pageProps.dispatch(reducerAction(name, stateName, payload));

    return true;
}

/**
 * Reducer logic
 * @param {object} state 
 * @param {object} payload 
 * 
 * @return {object}
 */
export const reducerLogic = (state, payload) => {
    try {
        return produce(state, draftState => {
            // Manipulate draftState into the intended state change
            //setComponentStateProperties(draftState, payload.data, payload.component, 'uuid');
            for ( let key in draftState ) {
                // Skip invalid components
                if (!state[key] || !state[key].property) {
                    continue;
                }
                //console.log(draftState[key].property)
                const componentFunction = draftState[key].property?.componentFunction
                if(componentFunction && payload.pageActions) {
                    const componentPageAction = payload.pageActions[componentFunction]
                    const derivedValue = componentPageAction()
                    let emobiqComponent = getEmobiqComponent(null, null, null, draftState[key])
                    // Update componentValue and the related attributes to componentValue
                    if (typeof emobiqComponent.setComponentValue == 'function') {
                        emobiqComponent.setComponentValue(derivedValue)
                    }
                }
            }
        });
    } catch(e) {
        return { ...state };
    }
}