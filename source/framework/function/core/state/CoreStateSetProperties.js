import reducerAction from '../../../core/reducer/Action';
import produce from 'immer';

import { errorLog } from '../../../core/Log';

import { setComponentStateProperties } from '../../../core/Component';

// Function name
export const name = 'coreStateSetProperties';

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Function - App - CoreStateSetProperties - ';

/**
 * Run the function
 * Update the property values in the state
 * @param {string} stateName 
 * @param {object} pageProps state of the current page
 * @param {object} parameters
 *                 - uuid {string} - The uuid of the component. (R)
 *                 - data {object} - Object of properties with values to store in state. (R)
 * 
 * @return {boolean}
 */
export default run = (stateName, pageProps, parameters) => {
    // Prepare the initial variable to be used
    let logFunctionName = 'run: ';

    // Validate the value passed
    if (!parameters.uuid || (parameters.uuid && typeof parameters.uuid !== 'string')) {
        errorLog(logInformation + logFunctionName + 'Either uuid needs to be filled and must be a string.');
        return false;
    }
    
    if (typeof parameters.data !== 'object') {
        errorLog(logInformation + logFunctionName + 'The parameter data needs to be an object.');
        return false;
    }

    // Prepare the payload
    let payload = {
        component: parameters.uuid,
        data: parameters.data
    };
    
    // Dispatch it to the redux store
    pageProps.dispatch(reducerAction(name, stateName, payload));

    return true;
}

/**
 * Reducer logic
 * @param {object} state 
 * @param {object} payload 
 * 
 * @return {object}
 */
export const reducerLogic = (state, payload) => {
    try {
        return produce(state, draftState => {
            // Manipulate draftState into the intended state change
            setComponentStateProperties(draftState, payload.data, payload.component, 'uuid');
        });
    } catch(e) {
        return { ...state };
    }
}