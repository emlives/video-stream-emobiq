import { Variables as GVar } from '../../../dependency/global/Variable';
import { DefaultValueString } from '../../core/Constant';

// Function name
export const name = 'getVar';

/**
 * Returns the value of a global variable
 * 
 * @param {any} parameters.var - The key for variable 
 * @return {any} - The value of a global variable
 */
export default run = (parameters) => {
    const key = parameters.var

    if (!(typeof GVar[key] === 'undefined') && GVar[key] !== '') {
        return GVar[key];
    }    

    return parameters['default'];
}