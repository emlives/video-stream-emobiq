import { translate } from '../../core/Language';
import { errorLog } from '../../core/Log';

// Function name
export const name = 'languageConvertData';

/**
 * Run the function
 * Change the language setting
 * @param {object} parameters
 *                 - value {string} - The value to be converted. (R)
 *                 - language {string} - The language to be used. (O)
 * 
 * @return {promise/any} - {any}
 */
export default run = (parameters) => {
    // Check if value was passed
    if (typeof parameters.value !== 'string') {
        errorLog('Source - Framework - Function - App - LanguageConvertData - run: ' + 'The parameter value must be a be string.');
        return parameters.value;
    }

    // Translate the data through a core function
    return translate(parameters.value, parameters.language);
}