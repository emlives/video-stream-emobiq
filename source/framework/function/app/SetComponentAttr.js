import reducerAction from '../../core/reducer/Action';

import produce from "immer";

import { result } from '../../core/Format';

import { errorLog } from '../../core/Log';

import { isComponentExisting, setComponentStateAttributes } from '../../core/Component';

import { getServiceByName } from '../../core/Service';

// Function name

export const name = "setComponentAttr";

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Function - App - SetComponentAttr - ';

/**
 * Run the function
 * Update the attribute of the component
 * @param {string} stateName 
 * @param {object} pageProps state of the current page
 * @param {object} parameters
 *                 - component {string} - The name of the component. (C)
 *                 - componentId {string} - The id of the component. (C)
 *                 - attr {string} - attribute of the component. (C)
 *                 - value {string} - The value to be stored. (O)
 * 
 * @return {any} - return result format if success and false if error (to support older versions)
 */

export default run = (stateName, pageProps, parameters) => {


    // Prepare the initial variable to be used
    let logFunctionName = 'run: ';

    // Validate the value passed
    if ((!parameters.component && !parameters.componentId) || (parameters.component && typeof parameters.component !=='string')) {
        errorLog(logInformation + logFunctionName + 'Either name or id needs to be filled and must be a string.');
        return false;
    }
    
    // Validate the value passed
    if ((!parameters.component && !parameters.attr) || (parameters.component && typeof parameters.attr !=='string')) {
        errorLog(logInformation + logFunctionName + 'attr needs to be filled and must be a string.');
        return false;
    }
    
    if (typeof parameters.value === 'object') {
        errorLog(logInformation + logFunctionName + 'The parameter value is not allowed to be an object.');
        return false;
    }

    // Prepare the parameters for getting state
    let component = (parameters.componentId ? parameters.componentId : parameters.component);
    let type = (parameters.componentId ? 'id' : 'name');

    // Check if component exists
    if (!isComponentExisting(pageProps, component, type, stateName)) {
        errorLog(logInformation + logFunctionName + 'The component component/componentId - ' + component + ' is not existing.');
        return false;
    }
    
    // Try to find service first
    if (type == "name") {
        let service = getServiceByName(component)
        if (service && parameters.attr != "name") {
            service[parameters.attr] = parameters.value;
            return result(true, '', 'Successfully set the attribute of the service.');
        }
    }
    
    // Prepare the payload
    let payload = {
        component: component,
        data: {
            [parameters.attr]: parameters.value
        },
        type: type,
    };
    
    // Dispatch it to the redux store
    pageProps.dispatch(reducerAction(name, stateName, payload));

    return result(true, '', 'Successfully set the attribute of the component.');

}


export const reducerLogic = (state, payload) => {

    try {
        return produce(state, draftState => {
            // Manipulate draftState into the intended state change
            setComponentStateAttributes(draftState, payload.data, payload.component, payload.type);
        });
    } catch(err) {
        errorLog('Source - Framework - Function - App - SetComponentAttr - run: ' + err);
        return { ...state };
    }

}