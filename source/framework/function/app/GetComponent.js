import { getEmobiqComponent, isComponentExisting } from '../../core/Component';
import { DefaultValueString } from '../../core/Constant';
// Function name
export const name = "getComponent";

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Function - App - GetComponent - ';

/**
 * Returns the value of a Emobiq Component attribute
 * 
 * @param {object} pageProps - page properties
 * @param {object} parameters -  { componentName: string, attr: string }
 * @return {object} - component object in a similar structure as in cordova
 */

export default run = (pageProps, parameters) => {
    // Prepare the initial variable to be used
    let logFunctionName = 'run: ';
    
    // Validate the value passed
    if ((!parameters.component && !parameters.componentId) || (parameters.component && typeof parameters.component !== 'string') || (!parameters.name && parameters.componentId && typeof parameters.componentId !== 'string')) {
        errorLog(logInformation + logFunctionName + 'Either name or id needs to be filled and must be a string.');
        return DefaultValueString;
    }
    
    // Prepare the parameters for getting state
    let component = (parameters.componentId ? parameters.componentId : parameters.component);
    let type = (parameters.componentId ? 'id' : 'name');
    
    let emobiqComponent = getEmobiqComponent(pageProps, component, type);
    // Check if componet exists
    if (!emobiqComponent) {
        errorLog(logInformation + logFunctionName + 'The component component/componentId - ' + component + ' is not existing.');
        return false;
    }

    return emobiqComponent;

}