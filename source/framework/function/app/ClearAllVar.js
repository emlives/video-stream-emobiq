import { Variables as GVar } from '../../../dependency/global/Variable';

// Function name
export const name = "clearAllVar";

/**
 * Clears Global Variables
 * 
 * @param {any} parameters.except - The key or array of keys for variable to NOT delete
 * Note: This doesn't return result to support older version (legacy).
 * @return {boolean} - The status
 */
export default run = (parameters) => {
    let exceptionList = (parameters.except ? parameters.except : []);
    if (!Array.isArray(exceptionList)) {
        exceptionList = [exceptionList]
    }

    for (var variable in GVar) {
        if (!exceptionList.includes(variable)) {
            delete GVar[variable]
        }
    }

    return true
}