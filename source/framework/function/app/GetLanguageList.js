import { getLanguageList } from '../../core/Language';

// Function name
export const name = 'getLanguageList';

/**
 * Run the function
 * Get the available languages
 * @param {object} parameters
 *                 - callback {function} - Function to call alongside
 *                                         the expected result after processing the function.
 * 
 * @return {object}
 */
export default run = (parameters) => {
    // Prepare the result 
    let result = [];

    // Get the available langauges
    let langauges = getLanguageList();
    for (let language of langauges) {
        // Insert the language in the result
        result.push({ 
            lang: language,
            language: language
        });
    }

    // Check if any callback must be called and valid one
    if (parameters.callback && typeof(parameters.callback) === 'function') {
        parameters.callback(result);
    }

    return result;
}