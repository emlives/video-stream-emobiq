import reducerAction from '../../core/reducer/Action';
import produce from "immer";
import { result } from '../../core/Format';

// Function name
export const name = "disablePage";

/**
 * Disable page
 * 
 * @param {string} stateName 
 * @param {object} dispatch
 */
export default run = (stateName, dispatch) => {
    let payload = {name: 'page'};
    dispatch(reducerAction(name, stateName, payload));
    return result(true, '', 'Successfully disabled the page.');
}

export const reducerLogic = (state, payload) => {
    try {
        return produce(state, draftState => {
            draftState[payload.name].property.componentDisabled = true;
        });
    } catch(e) {
        return { ...state };
    }
}