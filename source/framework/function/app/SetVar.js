import { Variables as GVar } from '../../../dependency/global/Variable';
import { errorLog } from '../../core/Log';
import { result } from '../../core/Format';
import { DefaultValueString } from '../../core/Constant';

// Function name
export const name = "setVar";

/**
 * Sets a global variable
 * 
 * @param {any} parameters.var - The key for variable 
 * @param {any} parameters.value - The value for variable
 * @return {boolean} - The status of operation
 */
export default run = (parameters) => {
    const key = parameters.var;
    const value = parameters.value;

    if (!key) {
        errorLog('Source - Framework - Function - App - SetVar - run: ' + 'The parameter var is null');
        return result(false, null, 'The parameters var is null.');
    }

    GVar[key] = typeof value !== 'undefined' ? value : DefaultValueString;
    return result(true, null, 'Successfully set global variable');
}