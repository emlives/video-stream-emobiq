import { errorLog } from '../../core/Log';
import { isNumber } from '../../core/Helper'
import { Intervals as gIntervals } from '../../../dependency/global/Variable';

// Function name
export const name = "setInterval";

/**
 * Calls the callback function passed after the specified timeout passed
 * 
 * @param {number} parameters.delay - The interval to execute the function
 * @param {number} parameters.timeout - The time to stop executing the callback
 * @param {boolean} parameters.persistent - Store the return of timeout in Global for clearTimeout
 * Note: This doesn't return result to support older version (legacy).
 * @param {[function]} parameters.callback - Store the return of timeout in Global for clearTimeout
 */
export default run = (parameters) => {
    const delay = parameters.delay || 1000 // default to 1 sec
    const timeout = parameters.timeout || 0 // default to run once
    const persistent = parameters.persistent || false
    const callbacks = parameters.callback

    if (!callbacks || !isNumber(delay) || !isNumber(timeout)) {
        errorLog('Source - Framework - Function - App - SetInterval - run: ' + 'The parameters.callback is null or parameters.delay, timeout is NaN'); 
        return false;
    }

    // Start time for triggering the event
    var intervalStartDate = new Date()

    var intervalId = setInterval(function() {
        // Check first if the timeout was reached
        if (timeout !== 0) {
            if (timeout < (Math.abs(new Date() - intervalStartDate))) {
                // Stop the action looping
                clearInterval(intervalId);
                return;
            }
        }

        callbacks() }, delay);

    // Check if persistent then don't store
    if (!persistent) {
        gIntervals.push(intervalId);
    }

    return true;
}