import { errorLog } from '../../core/Log';

// Function name
export const name = "toObject";

/**
 * Convert the values to array
 * 
 * @param parameters - The parameters
 * @return {any} - Returns parameter
 */
export default run = (parameters) => {
    if (!parameters) {
        errorLog('Source - Framework - Function - App - toObject - run: ' + 'The parameter is null');
        return {}
    }

    return parameters;
}