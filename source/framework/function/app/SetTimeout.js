import { errorLog } from '../../core/Log';
import { isNumber } from '../../core/Helper'
import { Timeouts as gTimeouts } from '../../../dependency/global/Variable';

// Function name
export const name = "setTimeout";

/**
 * Calls the callback function passed after the specified timeout passed
 * 
 * @param {function} parameters.callback - The key for variable 
 * @param {any} parameters.extra - The value for variable
 * @param {number} parameters.timeout - The value for variable
 * @param {boolean} parameters.persistent - Store the return of timeout in Global for clearTimeout
 * Note: This doesn't return result to support older version (legacy).
 * @return {boolean} - The success status of function
 * 
 */
export default run = (parameters) => {
    const callback = parameters.callback
    const extra = parameters.extra
    const timeout = parameters.timeout
    const persistent = parameters.persistent || false

    if (!timeout || !isNumber(timeout)) {
        errorLog('Source - Framework - Function - App - SetTimeout - run: ' + 'The parameters.timeout is null or NaN'); 
        return false;
    }

    if (typeof callback !== 'function') {
        errorLog('Source - Framework - Function - App - SetTimeout - run: ' + 'The parameters.callback is null or not a function'); 
        return false;
    }
    
    var timeoutId = setTimeout(function() {
        callback(extra);
    }, timeout);

    // Check if persistent then don't store
    if (!persistent) {
        gTimeouts.push(timeoutId);
    }

    return true;
}