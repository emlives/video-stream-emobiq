import reducerAction from '../../core/reducer/Action';
import produce from "immer";
import { result } from '../../core/Format';
import { errorLog } from '../../core/Log';

// Function name
export const name = "hideElement";

/**
 * Hide the element either it's a component or snippet
 * 
 * @param {string} stateName 
 * @param {object} dispatch
 * @param {object} parameters
 *                 - component {string} - The name of the component. (R)
 */
export default run = (stateName, dispatch, parameters) => {
    // Validate the parameters
    if (!parameters.component || typeof parameters.component !=='string') {
        errorLog('Source - Framework - Function - App - HideElement - run: The parameter component is required and must be a string.');
    }

    parameters.name = parameters.component;
    if (!parameters.name) {
        parameters.name = parameters.componentId;
    }

    let payload = parameters;
    dispatch(reducerAction(name, stateName, payload));
    return result(true, '', 'Successfully hide the element.');
}

export const reducerLogic = (state, payload) => {
    try {
        return produce(state, draftState => {
            draftState[payload.name].property.componentVisible = false;
        });
    } catch(e) {
        return { ...state };
    }
}