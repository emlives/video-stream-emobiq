import { result } from '../../core/Format';
import { errorLog } from '../../core/Log';

// Perform console.log
export const name = "console";
/**
 * @param {object} parameters
 *                 - value {any} - the value to display in log. (R)
 * 
 * @return {any}
 */
export default run = (parameters) => {
    console.log(parameters.value)
    return parameters.value
}
