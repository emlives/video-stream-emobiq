import { getComponentStateElementAttribute, isComponentExisting } from '../../core/Component';
import { DefaultValueString } from '../../core/Constant';
import { errorLog } from '../../core/Log';


// Returns the value of an element attribute of a component
export const name = 'componentElAttr';

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Function - App - ComponentElAttr - ';

/**
 * @param {string} stateName
 * @param {object} pageProps state of the current page
 * @param {object} parameters
 *                 - component {string} - The name of the component. (R)
 *                 - componentId {string} - The id of the component. (R)
 *                 - attr {string} - name of the attribute. (O)
 * 
 * @return {string}
 */
export default run = (stateName, pageProps, parameters) => {
    // Prepare the initial variable to be used
    let logFunctionName = 'run: ';
    
    // Validate the value passed
    if ((!parameters.component && !parameters.componentId) || (parameters.component && typeof parameters.component !== 'string') || (!parameters.name && parameters.componentId && typeof parameters.componentId !== 'string')) {
        errorLog(logInformation + logFunctionName + 'Either name or id needs to be filled and must be a string.');
        return DefaultValueString;
    }
    
    // Prepare the parameters for getting state
    let component = (parameters.componentId ? parameters.componentId : parameters.component);
    let type = (parameters.componentId ? 'id' : 'name');
    
    // Check if componet exists
    if (!isComponentExisting(pageProps, component, type, stateName)) {
        errorLog(logInformation + logFunctionName + 'The component component/componentId - ' + component + ' is not existing.');
        return false;
    }
    
    let result = getComponentStateElementAttribute(pageProps, parameters.attr, component, type);

    // to handle javascript for numeric 0 where  0 = false
    if (typeof result == 'number' && result == 0) {
        return "0"
    }

    return result ? result : DefaultValueString;
}