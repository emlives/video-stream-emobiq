import { Linking } from 'react-native';
import { errorLog } from '../../core/Log';
import { result } from '../../core/Format';

// Function name
export const name = "openLink";

/**
 * Open the given url
 * 
 * @param {string} parameters.link - The url string
 */
export default run = (parameters) => {
    
    if (parameters['link']) {
        Linking.openURL(parameters['link']);
    } else {
        errorLog('Source - Framework - Function - App - OpenLink - run: ' + 'The parameter link is null'); 
        return result(false, '', 'Link is null. Unable to open link.');
    }

    return result(true, '', 'Successfully opened link.');
}