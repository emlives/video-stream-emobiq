
import { References } from '../../../dependency/global/Variable';
import { canvasFromText } from '../../core/Canvas';

import { errorLog } from '../../core/Log';
import { callback } from '../../core/Helper';
import { result } from '../../core/Format';

// Function name
export const name = 'bbCodeToCanvasSync';

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Function - App - BbCodeToCanvasSync - ';

/**
 * Convert the text into a canvas
 * with callbacks works with images.
 * @param {object} parameters
 *                  - text {string} - the text to be converted
 *                  - font {string} - the size to be used
 *                  - size {int} - the font size to be used
 *                  - canvasWidth {int} - the canvas width to use
 *                  - marginTop {int} - Add this margin to the top of canvas
 *                  - marginLeft {int} - Add this margin to the left of canvas
 *                  - marginRight {int} - Add this margin to the right of canvas
 *                  - marginBottom {int} - Add this margin to the bottom of canvas
 *                  - callback {function} - function to call if successfull
 *                  - errCallback {function} - function to call if failed
 * 
 * @return {canvas}
 */
export default run = async (parameters) => {
    // Prepare the initial variable to be used
    let logFunctionName = 'run: ';

    // Get the canvas to be used to generate the text with
    let canvas = References.canvas;

    // Check if the canvas exists
    if (!canvas) {
        errorLog(logInformation + logFunctionName + 'The canvas component does not exist.');
        callback('The canvas component does not exist.');
        return result(false, '', 'The canvas component does not exist.');
    }

    (async () => {
        // Generate the canvas
        let data = await canvasFromText(canvas, parameters);
        callback(parameters.callback, data);
    })();
}