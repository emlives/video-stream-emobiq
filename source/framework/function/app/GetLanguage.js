import { getLanguage } from '../../core/Language';

// Function name
export const name = 'getLanguage';

/**
 * Run the function
 * Get the active language
 * 
 * @return {promise} - {string}
 */
export default run = () => {
    return getLanguage();
}