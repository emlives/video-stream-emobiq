import reducerAction from '../../core/reducer/Action';
import produce from 'immer';

import { errorLog } from '../../core/Log';
import { isJson } from '../../core/Helper';
import { isComponentExisting, setComponentStateProperties } from '../../core/Component';

// Get all the data of a service
export const name = 'setComboOptions';

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Function - App - SetComboOptions - ';

/**
 * @param {string} stateName 
 * @param {object} pageProps state of the current page
 * @param {object} parameters
 *                  - combo {string} - The name of the component. (C)
 *                  - comboId {string} - The id of the component. (C)
 *                  - data {object} - The options data. (O)
 *                  - valueField {string} - key from the data object to show as value (O)
 *                  - valueSeparator {string} - separator if valueField is json (more than one field) (O)
 *                  - displayField {string} - key from the data object to show as display (O)
 *                  - displaySeparator {string} - separator if valueField is json (more than one field) (O)
 * 
 * Note: This doesn't return result to support older version (legacy).
 * @return {boolean} - The result
 */
export default run = (stateName, pageProps, parameters) => {
    // Prepare the initial variable to be used
    let logFunctionName = 'run: ';

    // Validate the value passed
    if ((!parameters.combo && !parameters.comboId) ||
        (parameters.combo && typeof parameters.combo !== 'string') ||
        (!parameters.combo && parameters.comboId && typeof parameters.comboId !== 'string') ||
        (!parameters.valueField)
    ) {
        errorLog(logInformation + logFunctionName + 'Either combo or comboId needs to be filled and must be a string. Value field must be filled.');
        return false;
    }

    // Prepare the parameters for getting state
    let component = (parameters.comboId ? parameters.comboId : parameters.combo);
    let type = (parameters.comboId ? 'id' : 'name');

    // Check if componet exists
    if (!isComponentExisting(pageProps, component, type, stateName)) {
        errorLog(logInformation + logFunctionName + 'The combo box component name/id - ' + component + ' is not existing.');
        return false;
    }

    // Check if the data passed is valid
    if (!parameters.data || !Array.isArray(parameters.data)) {
        errorLog(logInformation + logFunctionName + 'The data passed exist and must be an array.');
        return false;
    }

    // Prepare the combo box options
    let options = [];

    // Loop through the data
    for (let i = 0; i < parameters.data.length; i++) {
        // Get the current currentRecord
        let currentRecord = parameters.data[i];

        let fields = '';
        let value = '';
        // if value is json then apply separator
        if (isJson(parameters.valueField)) {
            let separator = parameters.valueSeparator ? parameters.valueSeparator : '-';
            let valueFieldArray = JSON.parse(parameters.valueField)
            if (Array.isArray(valueFieldArray)) {
                for (let index = 0; index < valueFieldArray.length; index++) {
                    if (index > 0) {
                        value += separator
                    }
                    // Get the correct value if have subfield
                    fields = valueFieldArray[index].split('.');
                    let tempValue = ''
                    for (let ctr in fields) {
                        // Check if value already have value then use it
                        if (tempValue == '') {
                            tempValue = currentRecord[fields[ctr]];
                        } else {
                            tempValue = tempValue[fields[ctr]];
                        }
                    }
                    value += tempValue
                }
            } else {
                errorLog(logInformation + logFunctionName + 'valueField should either be a string or an array in the form of JSON string.');
                return false;
            }
        } else {
            fields = parameters.valueField.split('.');
            for (let ctr in fields) {
                // Check if value already have value then use it
                if (value == '') {
                    value = currentRecord[fields[ctr]];
                } else {
                    value = value[fields[ctr]];
                }
            }
        }

        let display = '';
        // if value is json then apply separator
        if (isJson(parameters.displayField)) {
            let separator = parameters.displaySeparator ? parameters.displaySeparator : '-';
            let displayFieldArray = JSON.parse(parameters.displayField)
            if (Array.isArray(displayFieldArray)) {
                for (let index = 0; index < displayFieldArray.length; index++) {
                    if (index > 0) {
                        display += separator
                    }
                    // Get the correct value if have subfield
                    fields = displayFieldArray[index].split('.');
                    let tempDisplay = ''
                    for (let ctr in fields) {
                        // Check if display already have value then use it
                        if (tempDisplay == '') {
                            tempDisplay = currentRecord[fields[ctr]];
                        } else {
                            tempDisplay = tempDisplay[fields[ctr]];
                        }
                    }
                    display += tempDisplay
                }
            } else {
                errorLog(logInformation + logFunctionName + 'displayField should either be a string or an array in the form of JSON string.');
                return false;
            }
        } else {
            fields = parameters.displayField.split('.');
            for (let ctr in fields) {
                // Check if display already have value then use it
                if (display == '') {
                    display = currentRecord[fields[ctr]];
                } else {
                    display = display[fields[ctr]];
                }
            }
        }
        options.push({
            label: display,
            value: value
        });
    }

    // Prepare the payload
    let payload = {
        component: component,
        type: type,
        data: options
    };

    // Dispatch it to the redux store
    pageProps.dispatch(reducerAction(name, stateName, payload));

    return true;
}

/**
 * Reducer logic
 * @param {object} state 
 * @param {object} payload 
 * 
 * @return {object}
 */
export const reducerLogic = (state, payload) => {
    try {
        return produce(state, draftState => {
            // Manipulate draftState into the intended state change
            setComponentStateProperties(draftState, { componentOptions: payload.data }, payload.component, payload.type);
        });
    } catch (e) {
        console.log(e)
        return { ...state };
    }
}