import { errorLog } from '../../core/Log';
import { isNumber } from '../../core/Helper';
import { Data, InactivityInterval } from '../../../dependency/global/Variable';
import { result } from '../../core/Format';

// Function name
export const name = 'inactivityTimeout';

/**
 * Run the function
 * Run an inactivity timeout check
 * @param {object} route - The props taken from react-navigation
 * @param {object} parameters
 *                 - currentPageOnly {bool} - The page name to move to. (O)
 *                 - timeout {number} - The page name to move to. (O)
 *                 - callback {function} - The page name to move to. (O)
 * 
 * @return {result} - {obj}
 */
export default run = (route, parameters) => {
    // Default variables to be used
    let currentPageOnly = parameters.currentPageOnly,
        timeout = parameters.timeout,
        callback = null,
        page = route.name,
        useTime = new Date().getTime() / 1000;

    // Make sure valid values are passed
    // then set default if invalid
    if (currentPageOnly && typeof currentPageOnly !== 'boolean') {
        errorLog('Source - Framework - Function - App - InactivityTimeout - run: ' + 'The parameter currentPageOnly must be a be boolean.');
        currentPageOnly = false;
    }
    if (timeout && !isNumber(timeout)) {
        errorLog('Source - Framework - Function - App - InactivityTimeout - run: ' + 'The parameter timeout must be a be valid number.');
        timeout = 1000;
    }

    // Reprocess the variables
    timeout = timeout / 1000;

    // Check if any callback must be called and valid one
    if (parameters.callback && typeof(parameters.callback) === 'function') {
        callback = parameters.callback;
    }

    // Start the timeout activity check every second.
    let intervalID = setInterval(function() {
        // Check the current time difference
        let diff = (new Date().getTime() / 1000) - InactivityInterval[intervalID];

        // If need to disable if different page 
        if (currentPageOnly && (Data.currentPage && Data.currentPage != page)){
            // Clear up the interval and global interval value
            clearInterval(intervalID);
            delete InactivityInterval[intervalID];
            return;
        }

        // Make the difference between use time with current time
        if (diff > timeout){
            // Initiate the callback if it exists
            if (callback) {
                // Clear up the interval and global interval value
                clearInterval(intervalID);
                delete InactivityInterval[intervalID];
                // Trigger callback
                callback();
            }             
        }
    }, 1000);

    // Process the use time to make it global
    // needed to detect the movement or touches in the app.
    // See source/framework/hook/PanResponder.js
    InactivityInterval[intervalID] = useTime;
}