import { errorLog } from '../../core/Log';
import { isNumber } from '../../core/Helper'

// Function name
export const name = "toArray";

/**
 * Convert the values to array
 * 
 * @param parameters.length - Defaults to 10
 * @return {array} - Returns array
 */
export default run = (parameters) => {
    var length = parameters.length || 10;
    let array = []

    for (var i=1; i <=length; i++) {
        if (typeof(parameters['value' + i]) != 'undefined' && parameters['value' + i]) {
            array.push(parameters['value' + i])        
        }
    }

    return array
}