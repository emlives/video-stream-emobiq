import { errorLog } from '../../core/Log';

// Function name
export const name = "isElementShown";

/**
 * Returns a true if element is visible, else false
 * 
 * @param {string} stateName 
 * @param {object} pageProps state of the current page
 * @param {object} parameters
 *                 - name {string} - The name of the component.
 */
export default run = (pageProps, parameters) => {
    const componentName = parameters.name;

    if (!componentName) {
        errorLog('Source - Framework - Function - App - IsElementShown - run: ' + 'The parameter name is null'); 
        return false
    }
    
    if (pageProps[componentName]) {
        const result = pageProps[componentName].property.componentVisible;
        return result ? result : false
    }

    return false;
}