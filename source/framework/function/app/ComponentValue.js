import { getComponentStateProperty } from '../../core/Component';
import { DefaultValueString } from '../../core/Constant';
import { errorLog } from '../../core/Log';


// Returns the value of a component
export const name = 'componentValue';

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Function - App - ComponentValue - ';

/**
 * @param {object} pageProps state of the current page
 * @param {object} parameters
 *                 - component {string} - The name of the component. (O)
 *                 - componentId {string} - The id of the component. (O)
 * 
 * @return {string}
 */
export default run = (pageProps, parameters) => {
    // Prepare the initial variable to be used
    let logFunctionName = 'run: ';
    
    // Validate the value passed
    if ((!parameters.component && !parameters.componentId) || (parameters.component && typeof parameters.component !== 'string') || (!parameters.name && parameters.componentId && typeof parameters.componentId !== 'string')) {
        errorLog(logInformation + logFunctionName + ' - run: Either name or id needs to be filled and must be a string.');
        return DefaultValueString;
    }
    
    // Prepare the parameters for getting state
    let component = (parameters.componentId ? parameters.componentId : parameters.component);
    let type = (parameters.componentId ? 'id' : 'name');

    let result = getComponentStateProperty(pageProps, 'componentValue', component, type);
    return result ? result : DefaultValueString;

}