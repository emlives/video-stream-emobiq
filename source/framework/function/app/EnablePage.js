//disablePage
import reducerAction from '../../core/reducer/Action';
import produce from "immer";
import { result } from '../../core/Format';

// Function name
export const name = "enablePage";

/**
 * Enable page
 * 
 * @param {string} stateName 
 * @param {object} dispatch
 */
export default run = (stateName, dispatch) => {
    let payload = {name: 'page'};
    dispatch(reducerAction(name, stateName, payload));
    return result(true, '', 'Successfully enabled the page.');
}

export const reducerLogic = (state, payload) => {
    try {
        return produce(state, draftState => {
            draftState[payload.name].property.componentDisabled = false;
        });
    } catch(e) {
        return { ...state };
    }
}