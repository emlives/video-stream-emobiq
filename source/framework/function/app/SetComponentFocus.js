import reducerAction from '../../core/reducer/Action';
import produce from 'immer';

import { errorLog } from '../../core/Log';

import { isComponentExisting, setComponentStateProperties } from '../../core/Component';

// Function name
export const name = 'setComponentFocus';

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Function - App - SetComponentFocus - ';

/**
 * Run the function
 * Update the value the state to focus on the component
 * @param {string} stateName 
 * @param {object} pageProps state of the current page
 * @param {object} parameters
 *                 - name {string} - The name of the component. (C)
 *                 - id {string} - The id of the component. (C)
 *                 - selectAllText {string} - Identifies whether to select all text - true or false. (O)
 *                 - hideKeyboard {boolean} - Identified whether to show keyboard. (O)
 * 
 * @return {boolean}
 */
export default run = (stateName, pageProps, parameters) => {
    // Prepare the initial variable to be used
    let logFunctionName = 'run: ';

    // Validate the value passed
    if ((!parameters.name && !parameters.id) || (parameters.name && typeof parameters.name !== 'string')) {
        errorLog(logInformation + logFunctionName + 'Either name or id needs to be filled and must be a string.');
        return false;
    }
    
    if (typeof parameters.value === 'object') {
        errorLog(logInformation + logFunctionName + 'The parameter value is not allowed to be an object.');
        return false;
    }

    // Prepare the parameters for getting state
    let component = (parameters.id ? parameters.id : parameters.name);
    let type = (parameters.id ? 'id' : 'name');

    // Check if componet exists
    if (!isComponentExisting(pageProps, component, type, stateName)) {
        errorLog(logInformation + logFunctionName + 'The component name/id - ' + value + ' is not existing.');
        return false;
    }

    // Prepare the data
    let data = {
        componentFocusTrigger: true
    }

    if (parameters.selectAllText && parameters.selectAllText == 'true') {
        data.componentFocusSelectText = true;
    }

    if (parameters.hideKeyboard) {
        data.componentFocusShowKeyboard = false;
    }

    // Prepare the payload
    let payload = {
        component: component,
        type: type,
        data: data
    };
    
    // Dispatch it to the redux store
    pageProps.dispatch(reducerAction(name, stateName, payload));

    return true;
}

/**
 * Reducer logic
 * @param {object} state 
 * @param {object} payload 
 * 
 * @return {object}
 */
export const reducerLogic = (state, payload) => {
    try {
        return produce(state, draftState => {
            // Manipulate draftState into the intended state change
            setComponentStateProperties(draftState, payload.data, payload.component, payload.type);
        });
    } catch(e) {
        return { ...state };
    }
}