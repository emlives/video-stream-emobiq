import reducerAction from '../../core/reducer/Action';
import produce from 'immer';

import { errorLog } from '../../core/Log';
import { result } from '../../core/Format';

import { isComponentExisting, setComponentStateElementAttributes, getEmobiqComponent } from '../../core/Component';

// Function name
export const name = 'setComponentValue';

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Function - App - SetComponentValue - ';

/**
 * Run the function
 * Update the value the state
 * @param {string} stateName 
 * @param {object} pageProps state of the current page
 * @param {object} parameters
 *                 - name {string} - The name of the component. (C)
 *                 - id {string} - The id of the component. (C)
 *                 - value {string} - The value to be stored. (O)
 * 
 * @return {any} - return result format if success and false if error (to support older versions)
 */
export default run = (stateName, pageProps, parameters) => {
    // Prepare the initial variable to be used
    let logFunctionName = 'run: ';

    // Validate the value passed
    if ((!parameters.component && !parameters.componentId) || (parameters.component && typeof parameters.component !=='string')) {
        errorLog(logInformation + logFunctionName + 'Either component or componentId needs to be filled and must be a string.');
        return false;
    }
    
    if (typeof parameters.value === 'object') {
        errorLog(logInformation + logFunctionName + 'The parameter value is not allowed to be an object.');
        return false;
    }

    // Prepare the parameters for getting state
    let component = (parameters.componentId ? parameters.componentId : parameters.component);
    let type = (parameters.componentId ? 'id' : 'name');

    // Check if componet exists
    if (!isComponentExisting(pageProps, component, type, stateName)) {
        errorLog(logInformation + logFunctionName + 'The component component/componentId - ' + component + ' is not existing.');
        return false;
    }

    // Prepare the payload
    let payload = {
        component: component,
        type: type,
        data: parameters.value
    };
    
    // Dispatch it to the redux store
    pageProps.dispatch(reducerAction(name, stateName, payload));

    return result(true, '', 'Successfully set the value of the component.');
}

/**
 * Reducer logic
 * @param {object} state 
 * @param {object} payload 
 * 
 * @return {object}
 */
export const reducerLogic = (state, payload) => {
    try {
        return produce(state, draftState => {
            let emobiqComponent = getEmobiqComponent(draftState, payload.component, payload.type);
            if (emobiqComponent.setComponentValue) {
                // Manipulate draftState into the intended state change
                emobiqComponent.setComponentValue(payload.data)
            }
        });
    } catch(e) {
        return { ...state };
    }
}