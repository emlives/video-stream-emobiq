import { errorLog } from '../../core/Log';

// Function name
export const name = "callback";

/**
 * Calls the list of callback function
 * 
 * @param {any} parameters.value - The value to return
 * @param {any} parameters.extra - And extra data for callback list
 * @param {[functions]} parameters.callback - The list of callback
 * @return {any} - Returns the value of parameters.value
 * 
 */
export default run = (parameters) => {
    const callback = parameters.callback
    const extra = parameters.extra

    if (typeof callback !== 'function') {
        errorLog('Source - Framework - Function - App - Callback - run: ' + 'The parameters.callback is null or parameters.delay, timeout is NaN'); 
    }

    callbacks(extra)

    return parameters.value;
}