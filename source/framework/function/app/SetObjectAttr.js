import { Variables as GVar } from '../../../dependency/global/Variable';
import { errorLog } from '../../core/Log';

// Function name
export const name = "setObjectAttr";

/**
 * Sets a global variable
 * 
 * @param {object} parameters.object - The object for new attribute
 * @param {any} parameters.attr - The key of attribute 
 * @param {any} parameters.value - The value of variable
 * Note: This doesn't return result to support older version (legacy).
 * @return {boolean} - The result
 */
export default run = (parameters) => {
    const object = parameters.object
    const attr = parameters.attr
    const value = parameters.value

    if (!attr) {
        errorLog('Source - Framework - Function - App - SetObjectAttr - run: The parameters.attr is null');
        return false
    }

    if (!value) {
        errorLog(`Source - Framework - Function - App - SetObjectAttr - run: The parameters.value is null for attr=[${attr}]`);
        return false
    }

    if (!object) {
        errorLog(`Source - Framework - Function - App - SetObjectAttr - run: The parameters.object null while trying to set attr=[${attr}] and value=[${value}]`);
        return false
    }

   

    object[attr] = value
    return true;
}