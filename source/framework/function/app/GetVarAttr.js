import { Variables as GVar } from '../../../dependency/global/Variable';
import { errorLog } from '../../core/Log';
import { DefaultValueString } from '../../core/Constant';

// Function name
export const name = "getVarAttr";

/**
 * Returns the value for a variable attribute
 * 
 * @param {any} parameters.var - The key of object
 * @param {any} parameters.attr - The attr of object
 */
export default run = (parameters) => {
    let key = parameters.var;
    let attr = parameters.attr;

    if (!key || typeof attr === 'undefined' || attr == null) {
        errorLog('Source - Framework - Function - App - GetVarAttr - run: The parameters.var or parameters.attr is null'); 
        return parameters.default
    }

    if (typeof GVar[key] === 'undefined') {
        errorLog(`Source - Framework - Function - App - GetVarAttr - run: The key [${key}] does not exits in Global variable`); 
        return parameters.default
    }

    if (typeof GVar[key][attr] === 'undefined' || GVar[key][attr] === null) {
        errorLog(`Source - Framework - Function - App - GetVarAttr - run: The key:value pair [${key}:${attr}] does not exits in Global variable`); 
        return parameters.default
    }

    if (GVar[key][attr] !== '') {
        return GVar[key][attr];
    }

    errorLog(`Source - Framework - Function - App - GetVarAttr - run: The key:value pair [${key}:${attr}] is empty string, return default value [${parameters.default}]`); 

    return parameters.default
}