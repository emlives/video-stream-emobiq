import { errorLog } from '../../core/Log';
import { DefaultValueString } from '../../core/Constant';

// Function name
export const name = "objectAttr";

/**
 * Sets a global variable
 * 
 * @param {object} parameters.object - The object for new attribute
 * @param {any} parameters.attr - The key of attribute
 * @return {any} - Return the value of an object's attribute, false if error
 */
export default run = (parameters) => {
    const object = parameters.object
    const attr = parameters.attr

    if( isNaN(attr) && !attr) {
        errorLog('Source - Framework - Function - App - ObjectAttr - run: ' + 'The parameters.attr is null');
        return DefaultValueString
    }

    if (!object) {
        errorLog(`Source - Framework - Function - App - ObjectAttr - run: The parameters.object is null for attr [${attr}]`);
        return DefaultValueString
    }

    return object[attr]
}