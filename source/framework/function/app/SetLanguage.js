import { setLanguage } from '../../core/Language';
import { errorLog } from '../../core/Log';
import { result } from '../../core/Format';

// Function name
export const name = 'setLanguage';

/**
 * Run the function
 * Change the language setting
 * @param {object} parameters
 *                 - lang {string} - The language to be used. (R)
 * 
 * @param {promise/object} - {object}
 */
export default run = (parameters) => {
    // Check if language was passed
    if (!parameters.lang || typeof parameters.lang !== 'string') {
        errorLog('Source - Framework - Function - App - SetLanguage - run: ' + 'The parameter lang is required and must be a string.');
        return result(false, '', 'The parameter lang is required and must be a string.');
    }

    // Update the language through a core function
    return setLanguage(parameters.lang);
}