import { errorLog } from '../../core/Log';
import { isNumber } from '../../core/Helper'
import { DefaultValueNumber } from '../../core/Constant'

// Function name
export const name = "sin";

/**
 * The Math.sin() method returns a numeric value between -1 and 1
 * which represents the sine of the angle given in radians
 * 
 * @param {string} parameters.value - The value for sin operation
 * @return {number} - The sin result of a number
 */
export default run = (parameters) => {
    let value = parameters.value

    if (!isNumber(value)) {
        errorLog('Source - Framework - Function - Math - Sin - run: ' + 'The parameters value is null or NaN'); 
        return DefaultValueNumber
    }

    return Math.sin(parseFloat(value));
}