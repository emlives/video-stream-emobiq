import { errorLog } from '../../core/Log';
import { isNumber } from '../../core/Helper'
import { DefaultValueNumber } from '../../core/Constant'

// Function name
export const name = "ceil";

/**
 * Ceil operation
 * 
 * @param {string} parameters.value - The value for ceil operation
 * @return {number} - The ceil result of a number
 */
export default run = (parameters) => {
    let value = parameters.value

    if (!isNumber(value)) {
        errorLog('Source - Framework - Function - Math - Ceil - run: ' + 'The parameter value is null or NaN'); 
        return DefaultValueNumber
    }

    return Math.ceil(parseFloat(value));
}