import { errorLog } from '../../core/Log';
import {isNumber} from '../../core/Helper'
import { DefaultValueNumber } from '../../core/Constant'

// Function name
export const name = "sub";

/**
 * Returns the subtraction of two values, value1 - value2
 * 
 * @param {string} parameters.value1 - The value for sin operation
 * @param {string} parameters.value2 - The value for sin operation
 * @return {float} - The result of subtraction
 */
export default run = (parameters) => {
    var value1 = parameters.value1
    var value2 = parameters.value2

    if (!isNumber(value1) || !isNumber(value2)) {
        errorLog('Source - Framework - Function - Math - Sub - run: ' + 'The parameters value1 or value 2 is null or NaN'); 
        return DefaultValueNumber
    }

    return parseFloat(value1) - parseFloat(value2);
}