import { errorLog } from '../../core/Log';
import { isNumber } from '../../core/Helper'
import { DefaultValueNumber } from '../../core/Constant'

// Function name
export const name = "floor";

/**
 * Return the largest integer value that is less than or equal to a number.
 * It rounds a number down and returns an integer value
 * 
 * @param {string} parameters.value - The value for floor operation
 * @return {number} - The floor result of a number
 */
export default run = (parameters) => {
    let value = parameters.value

    if (!isNumber(value)) {
        errorLog('Source - Framework - Function - Math - Floor - run: ' + 'The parameter value is null or NaN'); 
        return DefaultValueNumber
    }

    return Math.floor(parseFloat(value));
}