import { errorLog } from '../../core/Log';
import { isNumber } from '../../core/Helper'

// Function name
export const name = "absolute";

/**
 * Return the absolute value of a number
 * 
 * @param {string} parameters.value - The value of the number
 * @return {float} - The result of absolution function
 */
export default run = (parameters) => {
    let value = parameters.value

    if (value == undefined || !isNumber(value)) {
        errorLog('Source - Framework - Function - Math - Absolute - run: ' + 'The parameters.value is null or NaN'); 
        return 0
    }

    return Math.abs(parseFloat(value));
}