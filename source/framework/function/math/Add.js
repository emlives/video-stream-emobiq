import { errorLog } from '../../core/Log';
import { isNumber } from '../../core/Helper'
import { DefaultValueNumber } from '../../core/Constant'

// Function name
export const name = "add";

/**
 * Add operation
 * 
 * @param {string} parameters.value1 - The first value for operation
 * @param {string} parameters.value2 - The second value for operation
 * @return {float} - The result of add
 */
export default run = (parameters) => {
    var value1 = parameters.value1
    var value2 = parameters.value2

    if (!isNumber(value1) || !isNumber(value2)) {
        errorLog('Source - Framework - Function - Math - Add - run: ' + 'The parameters value1 or value2 is null or NaN'); 
        return DefaultValueNumber
    }

    return parseFloat(value1) + parseFloat(value2);
}