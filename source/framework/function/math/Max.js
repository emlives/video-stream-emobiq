import { errorLog } from '../../core/Log';

// Function name
export const name = "max";

/**
 * Return the number with the highest value
 * 
 * @param {string} parameters.value - An array of values. E.g. [5, 10]
 * @return {any} - Returns the highest value in array passed
 */
export default run = (parameters) => {
    let value = parameters.value

    if ($.isArray(value)) {
        return Math.max.apply(null, value);
    } else {
        errorLog('Source - Framework - Function - Math - Max - run: ' + 'The parameters.value is not an array'); 
        return "Value should be an array";
    }
}