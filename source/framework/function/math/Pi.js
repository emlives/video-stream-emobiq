// Function name
export const name = "pi";

/**
 * Returns the ratio of the circumference of a circle to its diameter
 * approximately 3.14159
 * 
 * @return {float} - The sin result of a number
 */
export default run = () => {
    return Math.PI;
}