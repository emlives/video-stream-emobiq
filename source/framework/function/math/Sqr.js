import { errorLog } from '../../core/Log';
import {isNumber} from '../../core/Helper'
import { DefaultValueNumber } from '../../core/Constant'

// Function name
export const name = "sqr";

/**
 * Returns the multiplication of value-passed by itself
 * 
 * @param {string} parameters.value1 - The value to sqr
 * @return {float} - The result of sqr
 */
export default run = (parameters) => {
    let value = parameters.value

    if (!isNumber(value)) {
        errorLog('Source - Framework - Function - Math - Sqr - run: ' + 'The parameters value is null or NaN'); 
        return DefaultValueNumber
    }

    return parseFloat(value) * parseFloat(value);
}