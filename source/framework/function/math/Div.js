import { errorLog } from '../../core/Log';
import {isNumber} from '../../core/Helper'
import { DefaultValueNumber } from '../../core/Constant'

// Function name
export const name = "div";

/**
 * Division operation
 * 
 * @param {string} parameters.value1 - The dividend
 * @param {string} parameters.value2 - The divisor
 * @param {string} parameters.integer - Set true to return an integer value of the division
 * @return {number} - The result of div operation
 */
export default run = (parameters) => {
    let dividend = parameters.value1
    let divisor = parameters.value2

    if (!isNumber(dividend) || !isNumber(divisor)) {
        errorLog('Source - Framework - Function - Math - Multi - run: ' + 'The parameters value1 or value2 is null or NaN'); 
        return DefaultValueNumber
    }

    if (parameters && parameters.integer) {
        return (divisor ? parseInt(dividend / divisor) : undefined);
    } else {
        return (divisor ? (dividend / divisor) : undefined);
    }
}