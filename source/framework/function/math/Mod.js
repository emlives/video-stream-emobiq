import { errorLog } from '../../core/Log';
import { isNumber } from '../../core/Helper';
import { DefaultValueNumber } from '../../core/Constant'

// Function name
export const name = "mod";

/**
 * Returns the division remainder
 * 
 * @param {string} parameters.value1 - The first value for operation
 * @param {string} parameters.value2 - The second value for operation
 * @return {any} - Returns the division remainder
 */
export default run = (parameters) => {
    var value1 = parameters.value1
    var value2 = parameters.value2

    if (!isNumber(value1)) {
        errorLog('Source - Framework - Function - Math - Mod - run: ' + 'The parameter value1 is null or NaN'); 
        value1 = 0
    }

    if (!isNumber(value2)) {
        errorLog('Source - Framework - Function - Math - Mod - run: ' + 'The parameter value2 is null or NaN'); 
        // note: legacy is defaulting value2 to 0, which results in returning NaN
        return DefaultValueNumber
    }

    return value1 % value2;
}