import { errorLog } from '../../core/Log';
import { isNumber } from '../../core/Helper'
import { DefaultValueNumber } from '../../core/Constant'

// Function name
export const name = "cos";

/**
 * Division operation
 * 
 * @param {string} parameters.value - The value for cos operation
 * @return {number} - The cos result of a number
 */
export default run = (parameters) => {
    let value = parameters.value

    if (!isNumber(value)) {
        errorLog('Source - Framework - Function - Math - Cos - run: ' + 'The parameter value is null or NaN'); 
        return DefaultValueNumber
    }

    return Math.cos(parseFloat(value));
}