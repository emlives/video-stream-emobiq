import { errorLog } from '../../core/Log';
import { DefaultValueNumber } from '../../core/Constant'

// Function name
export const name = "min";

/**
 * Return the number with the highest value
 * 
 * @param {string} parameters.value - An array of values. E.g. [5, 10]
 * @return {number} - Returns the lowest value in array passed
 */
export default run = (parameters) => {
    let value = parameters.value

    if ($.isArray(value)) {
        return Math.min.apply(null, value);
    } else {
        errorLog('Source - Framework - Function - Math - Min - run: ' + 'The parameter value is not an array'); 
        return DefaultValueNumber
    }
}