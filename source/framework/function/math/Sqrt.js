import { errorLog } from '../../core/Log';
import {isNumber} from '../../core/Helper'
import { DefaultValueNumber } from '../../core/Constant'

// Function name
export const name = "sqrt";

/**
 * Returns the square root of a number
 * 
 * @param {string} parameters.value1 - The value to sqrt
 * @return {float} - The result of square root of a number
 */
export default run = (parameters) => {
    let value = parameters.value1

    if (!isNumber(value)) {
        errorLog('Source - Framework - Function - Math - Sqrt - run: ' + 'The parameters value1 is null or NaN'); 
        return DefaultValueNumber
    }

    return Math.sqrt(parseFloat(value));
}