import { errorLog } from '../../core/Log';
import { isNumber } from '../../core/Helper'
import { DefaultValueNumber } from '../../core/Constant'

// Function name
export const name = "atan2";

/**
 * Returns the arc tangent of two numbers
 * 
 * @param {string} parameters.x - The first value for atan2 operation
 * @param {string} parameters.y - The second value for atan2 operation
 * @return {float} - The result of atan2 operation
 */
export default run = (parameters) => {
    let x = parameters.x
    let y = parameters.y

    if (!isNumber(x) || !isNumber(y)) {
        errorLog('Source - Framework - Function - Math - Atan2 - run: ' + 'The parameters x or y is null or NaN'); 
        return DefaultValueNumber
    }

    return Math.atan2(parseFloat(y), parseFloat(x));
}