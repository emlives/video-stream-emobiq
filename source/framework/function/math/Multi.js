import { errorLog } from '../../core/Log';
import {isNumber} from '../../core/Helper'

// Function name
export const name = "multi";

/**
 * Concate an array of strings
 * 
 * @param {string} parameters.value1 - First number for multi operation
 * @param {string} parameters.value2 - Second number for multi operation
 * @return {float} - The result of multi operation of value1 and value2
 */
export default run = (parameters) => {
    var x1 = parameters.value1
    var x2 = parameters.value2

    if (!isNumber(x1)) {    
        x1 = 0
        errorLog('Source - Framework - Function - Math - Multi - run: ' + 'The parameter value1 is null.'); 
    }

    if (!isNumber(x2)) {   
        x2 = 0
        errorLog('Source - Framework - Function - Math - Multi - run: ' + 'The parameter value2 is null.'); 
    }


    var x = isNumber(x1) ? x1.toString() : x1;
    var y = isNumber(x2) ? x2.toString() : x2;
    return parseFloat(x.replace(/,/g, '')) * parseFloat(y.replace(/,/g, ''));
}