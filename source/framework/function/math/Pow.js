import { errorLog } from '../../core/Log';
import { isNumber } from '../../core/Helper'
import { DefaultValueNumber } from '../../core/Constant'

// Function name
export const name = "pow";

/**
 * The pow() method returns the value of x to the power of y (xy)
 * 
 * @param {string} parameters.base - The value of base
 * @param {string} parameters.exponent - The value of exponent
 * @return {float} - The floating point number for pow operation
 */
export default run = (parameters) => {
    let base = parameters.base
    let exponent = parameters.exponent

    if (!isNumber(base) || !isNumber(exponent)) {
        errorLog('Source - Framework - Function - Math - Pow - run: ' + 'The parameters base or exponent is null or NaN'); 
        return DefaultValueNumber
    }

    return Math.pow(parseFloat(base), parseFloat(exponent));
}