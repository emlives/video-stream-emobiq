import { errorLog } from '../../core/Log';
import { isNumber } from '../../core/Helper'
import { DefaultValueNumber } from '../../core/Constant'

// Function name
export const name = "tan";

/**
 * Returns the tangent of a number
 * 
 * @param {string} parameters.value - The value for tan operation
 * @return {number} - The tan result of a number
 */
export default run = (parameters) => {
    let value = parameters.value

    if (!isNumber(value)) {
        errorLog('Source - Framework - Function - Math - Tan - run: ' + 'The parameter value is null or NaN'); 
        return DefaultValueNumber
    }

    return Math.tan(parseFloat(value));
}