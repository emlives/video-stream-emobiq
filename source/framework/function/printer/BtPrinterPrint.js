// import { NativeModules } from 'react-native';
import { write } from '../../plugin/BtPrinter';
import { cpclToBase64EncodedString } from '../../core/Helper'
import { errorLog } from '../../core/Log';
import { callback } from '../../core/Helper';
import { result } from '../../core/Format';

// Function name
export const name = "btPrinterPrint";

/**
 * Print input content
 * @param {string} parameters.text - The print content
 * @param {functionList} parameters.callback  - Success callback
 * @param {functionList} parameters.errorCallback  - Error callback
 */

export default run = (parameters) => {
    let successCallback = () => {
        callback(parameters.callback, parameters.extra);
    }

    let errorCallback = (error) => {
        callback(parameters.errorCallback, error);
    }

    if (!parameters.text) {
        const errMessage = 'Source - Framework - Function - Printer - BtPrinterPrint - run: ' + 'The parameter text is null';
        errorLog(errMessage);
        return errorCallback(errMessage)
    }

    const text = cpclToBase64EncodedString(parameters.text);
    write(text, successCallback, errorCallback);
    return result(true, '', 'The request is being processed, validate it through the callbacks.');
}