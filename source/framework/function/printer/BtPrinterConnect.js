import { connect } from '../../plugin/BtPrinter';
import { errorLog } from '../../core/Log';
import { callback } from '../../core/Helper';
import { result } from '../../core/Format';

// Function name
export const name = "btPrinterConnect";

/**
 * Establish connection to bluetooth printer for printing service
 * @param {string} parameters.printerId - The uuid (for iOS) or address (for Android) of printer
 * @param {object} parameters.extra - Any extra parameters to success callback
 * @param {functionList} parameters.callback  - Success callback
 * @param {functionList} parameters.errorCallback  - Error callback
 */
export default run = (parameters) => {
    const printerId = parameters.printerId
    let successCallback = () => {
        callback(parameters.callback, parameters.extra);
    }

    let errorCallback = (error) => {
        callback(parameters.errorCallback, error);
    }
    
    if (!printerId) {
        const errorMessage = 'Source - Framework - Function - Printer - BtPrinterConnect - run: ' + 'The parameter printerId is null';
        errorLog(errorMessage);
        return errorCallback(errorMessage)
    }

    connect(printerId, successCallback, errorCallback);
    return result(true, '', 'The request is being processed, validate it through the callbacks.');
}