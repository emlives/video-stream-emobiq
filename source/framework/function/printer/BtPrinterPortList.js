import { scanForBLEPeripherals } from '../../plugin/BtPrinter';
import { errorLog } from '../../core/Log';
import { callback } from '../../core/Helper';
import { result } from '../../core/Format';

// Function name
export const name = "btPrinterPortList";

/**
 * Scans and displays bluetooth devices discoverable by device
 * @param {functionList} parameters.callback  - Success callback with list of discoverable bluetooth devices
 * @param {functionList} parameters.errorCallback  - Error callback
 */
export default run = (parameters) => {
    let successCallback = (data) => {
        callback(parameters.callback, data);
    }
    let errorCallback = (error) => {
        errorLog(error);
        callback(parameters.errorCallback, error);
    }

    // Check if has local authentication
    scanForBLEPeripherals(successCallback, errorCallback);

    return result(true, '', 'The request is being processed, validate it through the callbacks.');
}