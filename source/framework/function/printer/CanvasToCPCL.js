import { pcx } from '../../core/Helper';
import { errorLog } from '../../core/Log';

// Function name
export const name = 'canvasToCPCL';

/**
 * Returns the device id
 * @param {object} parameters.canvas - A reference object to React-Native-Canvas's canvas
 * @param {boolean} parameters.singlePrint  - Single Print
 * @param {number} parameters.copies  - Defaulted to 1
 * @param {boolean} parameters.blackMark  - To include black marks for separation of labels or tags
 * @param {functionList} parameters.errorCallback  - Error callback
 */

export default run = async (parameters) => {
  if (!parameters.canvas) {
    errorLog('Source - Framework - Function - Printer - CanvasToCPCL - run: ' + 'The parameter canvas is null');
    return
  }

  var copies = (parameters.copies ? parameters.copies : 1);
  const canvas = parameters.canvas;
  const context = canvas.getContext('2d');

  let imageData = await context.getImageData(0, 0, canvas.width, canvas.height);
  const canvasImageData = Object.values(imageData.data);
  const printData = pcx.toCPCL(canvas, canvasImageData, parameters.singlePrint, copies, parameters.blackMark);

  return printData
}