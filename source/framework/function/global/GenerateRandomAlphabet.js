import { errorLog } from '../../core/Log';

// Function name
export const name = 'generateRandomAlphabet';

/**
 * Run the function
 * Generate a random string
 * @param {object} parameters
 *                 - characterLength {int} - length of the generated string
 * 
 * @return {string}
 */
export default run = (parameters) => {
    // Warns the user if passed a non number
    if (isNaN(parameters.characterLength)) {
        errorLog('Framework - Function - Global - GenerateRandomAlphabet: parameter characterLength is not a number');
        parameters.characterLength = 10;
    }

    // Round up the numbers
    let length = Math.floor(parameters.characterLength || 10);
    
    // Generate the random string
    let result = '',
        chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (var i = length; i > 0; --i) {
        result += chars[Math.floor(Math.random() * chars.length)];
    };

    return result;
}