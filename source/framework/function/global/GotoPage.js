import { Data as GlobalData } from '../../../dependency/global/Variable';

import reducerAction from '../../core/reducer/Action';

import { errorLog } from '../../core/Log';
import { result } from '../../core/Format';
import { ReducerActionResetState } from '../../core/Constant';

// Function name
export const name = 'gotoPage';

/**
 * Run the function
 * Go to a certain page
 * @param {object} route - The props taken from react-navigation
 * @param {object} navigation - The navigation taken from react-navigation
 * @param {string} stateName  - the state name of the page
 * @param {object} dispatch  - the redux dispatcher
 * @param {object} parameters
 *                 - p {string} - The page name to move to. (R)
 * 
 * @return {result} - {obj}
 */
 export default run = (route, navigation, stateName, dispatch, parameters) => {
    // Check if target page was passed
    if (!parameters.p || typeof parameters.p !== 'string') {
        errorLog('Source - Framework - Function - App - GotoPage - run: ' + 'The parameter p is required and must be a string.');
        return result(false, '', 'The parameter p is required and must be a string.');
    }

    // Set prev page
    GlobalData.previousPage = route.name;

    // Navigate to the page specified
    navigation.navigate(parameters.p);

    // Set the new page
    GlobalData.currentPage = parameters.p;

    // Refresh the previous page state by
    // dispatch it to the redux store
    dispatch(reducerAction(ReducerActionResetState, stateName, {}));

    return result(true, null, 'Successfully navigated to the page.');
 }
