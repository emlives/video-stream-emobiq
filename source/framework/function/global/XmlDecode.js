import { xmlToJson } from '../../core/Helper';
import { errorLog } from '../../core/Log';
import { DefaultValueObject } from '../../core/Constant';

// Function name
export const name = 'xmlDecode';

/**
 * Run the function
 * Decode the passed string to XML
 * @param {object} parameters
 *                  - string {string} - string to be decoded
 * 
 * @return {object}
 */
export default run = (parameters) => {
    // Check if variable name is passed and it is a string
    if (!parameters.string || typeof parameters.string !== 'string') {
        errorLog('Source - Framework - Function - Array - Push - run: ' + 'The parameter string is required and must be a string.');
        return DefaultValueObject;
    }

    // Convert the xml string to object
    let result = DefaultValueObject;
    try {
        window.DOMParser = require('xmldom').DOMParser;
        const parser = new DOMParser();
        result = xmlToJson(parser.parseFromString(parameters['string'].replace(/(\r\n|\n|\r)\s+/gm,'')));
        // remove undefined property
        delete result.undefined; 
    } catch (error) {
        errorLog("Framework - Function - global - XmlDecode: " + error);
    }

    return result;
}