// Function name
export const name = 'makeId';

/**
 * Run the function
 * Create a unique ID 
 * 
 * @return {string}
 */
export default run = () => {
    let number = 1000;
    var currentDate = new Date();
    var randomNumber = Math.floor(Math.random() * number);
    return '' + currentDate.getTime() + randomNumber.toString().padStart(3, '0');
}