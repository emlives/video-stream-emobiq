import { errorLog } from '../../core/Log';
import { DefaultValueObject } from '../../core/Constant';

// Function name
export const name = 'jsonDecode';

/**
 * Run the function
 * Decode the passed string
 * @param {object} parameters
 *                  - string {string} - string to be decoded
 * 
 * @return {object}
 */
export default run = (parameters) => {
    // Check if string is passed and it is a string
    if (!parameters.string || typeof parameters.string !== 'string') {
        errorLog('Source - Framework - Function - Global - JsonDecode - run: ' + 'The parameter string is required and must be a string.');
        return DefaultValueObject;
    }

    // Make sure it can be converted to object
    let result = DefaultValueObject;
    try {
        result = JSON.parse(parameters['string']);
    } catch (error) {
        errorLog('Source - Framework - Function - Global - JsonDecode - run: ' + 'The parameter string is not a valid json object.');
    }

    return result;
}