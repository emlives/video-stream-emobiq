import { serializeJSON } from '../../core/Helper';

import { errorLog } from '../../core/Log';
import { DefaultValueString } from '../../core/Constant';

// Function name
export const name = 'jsonEncode';

/**
 * Run the function
 * Encode the passed object
 * @param {object} parameters
 *                  - object {object} - data to be encoded (R)
 *                  - indent {int} - indicate how many space characters to use as white space (O)
 * 
 * @return {string}
 */
export default run = (parameters) => {
    // Validate object value first
    let objectType = typeof(parameters['object']);
    if (objectType == 'undefined' || objectType != 'object') {
        errorLog('Framework - Function - Global - JsonEncode: parameter object is empty or data type is invalid');
        return DefaultValueString;
    }

    // Check if it is necessary to indent the string
    if (parameters['indent']) {
        let indent = parseInt(parameters['indent']);
        if (isNaN(indent)) {
            indent = null;
        }
        return JSON.stringify(parameters['object'], null, indent);
    }
    
    return serializeJSON(parameters.object)
}