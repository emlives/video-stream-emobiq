import { errorLog } from '../../core/Log';

// Function name
export const name = 'generateRandomNumber';

/**
 * Run the function
 * Generate a random number
 * @param {object} parameters
 *                 - min {int} - minimum number
 *                 - max {int} - maximum number
 * 
 * @return {number}
 */
export default run = (parameters) => {
    // Warns the user if passed a non number
    if (isNaN(parameters.min)) {
        errorLog('Framework - Function - Global - GenerateRandomNumber: parameter min is not a number');
        parameters.min = 0;
    }
    if (isNaN(parameters['max'])) {
        errorLog('Framework - Function - Global - GenerateRandomNumber: parameter max is not a number');
        parameters.max = 1000000;
    }

    // Round up the numbers
    let min = Math.ceil(parameters.min || 0),
        max = Math.floor(parameters.max || 1000000);

    // Generate the random number
    return Math.floor(Math.random() * (max-min+1)) + min;
}