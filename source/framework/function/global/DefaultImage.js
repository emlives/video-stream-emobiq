// Function name
export const name = 'defaultImage';

/**
 * Run the function
 * Get the path to default.png
 * 
 * @return {string}
 */
export default run = () => {
    return '../framework/core/asset/default.png';
}