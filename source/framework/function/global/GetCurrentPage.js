// Reload data from a service 
export const name = 'getCurrentPage';

/**
 * Run the function
 * Get the name of the current page
 * @param {object} route - The props taken from react-navigation
 * 
 * @return {string}
 */
export default run = (route) => {
    return route.name;
}