import { Data as GlobalData } from '../../../dependency/global/Variable';

// Reload data from a service 
export const name = 'getPreviousPage';

/**
 * Run the function
 * Get the name of the previous page
 * 
 * @return {string}
 */
export default run = () => {
    return GlobalData.previousPage;
}