import { errorLog } from '../../core/Log';
import { isNumber } from '../../core/Helper'
import { DefaultValueNumber } from '../../core/Constant'

// Function name
export const name = "round";

/**
 * Returns the value of a number rounded to the nearest integer
 * 
 * @param parameters.value - The input value
 * @return {number} - Returns the rounded number
 */
export default run = (parameters) => {
    var value = parameters.value

    if (value == undefined || !isNumber(value)) {
        errorLog('Source - Framework - Function - Conversion - Round - run: ' + 'The parameter value is null or NaN'); 
        return DefaultValueNumber
    }

    var x = isNumber(value) ? value.toString() : value;
    return Math.round(x * 100) / 100;
}