import { errorLog } from '../../core/Log';
import { isNumber } from '../../core/Helper'
import { DefaultValueNumber } from '../../core/Constant'

// Function name
export const name = "toInteger";

/**
 * Convert the value to Integer
 * 
 * @param parameters.value - The input value
 * @return {number} - Returns the integer number
 */
export default run = (parameters) => {
    var value = parameters.value;

    if (!isNumber(value)) {
        errorLog('Source - Framework - Function - Conversion - ToInteger - run: ' + 'The parameter value is null or NaN'); 
        return DefaultValueNumber
    }

    return parseInt(value);
}