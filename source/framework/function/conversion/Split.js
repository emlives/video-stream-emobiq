import { errorLog } from '../../core/Log';
import { DefaultValueArray, DefaultValueString } from '../../core/Constant';

// Function name
export const name = 'split';

/**
 * Run the function
 * Takes in string and split them with a separator to an array.
 * @param {object} parameters
 *                 - value {string} - The value to be splitted. (R)
 *                 - separator {string} - The separator to use for splitting the value. (O)
 * 
 * @return {array}
 */
export default run = (parameters) => {
    // Check if value is a string
    if (!parameters.value || typeof parameters.value !== 'string') {
        errorLog('Source - Framework - Function - Conversion - Split - run: ' + 'The parameter value is required and must be a string.');
        return DefaultValueArray;
    }

    // Split the string with the separator
    let separator = parameters.separator || DefaultValueString;
    let result = parameters.value.split(separator);

    return result;
}