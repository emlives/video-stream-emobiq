import { errorLog } from '../../core/Log';

// Function name
export const name = "toBoolean";

/**
 * Convert the value to Boolean type
 * 
 * @param parameters.value - The input value
 * @return {boolean} - Returns the result
 */
export default run = (parameters) => {
    var value = parameters.value;

    if (!value) {
        errorLog('Source - Framework - Function - Conversion - ToBoolean - run: ' + 'The parameters value is null'); 
        return false
    }

    if (typeof value === 'string' || value instanceof String) {
        // it's a string
        var x = (value).toLowerCase().trim();
        return !(x == 'false' || x == '0' || x == 'no')
    }
    
    return !!(value);
}