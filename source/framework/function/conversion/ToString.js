import { errorLog } from '../../core/Log';
import { DefaultValueString } from '../../core/Constant';

// Function name 
export const name = "toString";

/**
 * Convert the value to String
 * 
 * @param parameters.value - The input value
 * @return {number} - Returns the integer number
 */
export default run = (parameters) => {
    var value = parameters.value;

    if (!value) {
        errorLog('Source - Framework - Function - Conversion - ToString - run: ' + 'The parameters.value is null'); 
        return DefaultValueString
    }

    return (value).toString();
}