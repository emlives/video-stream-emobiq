import { errorLog } from '../../core/Log';
import { DefaultValueString } from '../../core/Constant';

// Function name
export const name = 'join';

/**
 * Run the function
 * Takes in array and join them with a separator.
 * @param {object} parameters
 *                 - values {array} - The array to be joined. (R)
 *                 - separator {string} - The separator to be included upon joining. (O)
 * 
 * @return {string}
 */
export default run = (parameters) => {
    // Check if values are passed and it is an array
    if (!parameters.values || !Array.isArray(parameters.values)) {
        errorLog('Source - Framework - Function - Conversion - Join - run: The parameter values is required and must be an array.');
        return DefaultValueString;
    }

    // Join the array
    let separator = parameters.separator || DefaultValueString;
    let result = parameters.values.join(separator);

    return result;
}