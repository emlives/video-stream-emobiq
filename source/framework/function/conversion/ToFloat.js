import { errorLog } from '../../core/Log';
import { isNumber } from '../../core/Helper'
import { DefaultValueNumber } from '../../core/Constant'

// Function name
export const name = "toFloat";

/**
 * Convert the value to Float
 * 
 * @param parameters.value - The input value
 * @return {number} - Returns the float number
 */
export default run = (parameters) => {
    var value = parameters.value;

    if (!isNumber(value)) {
        errorLog('Source - Framework - Function - Conversion - ToFloat - run: ' + 'The parameters value is null or NaN'); 
        return DefaultValueNumber
    }

    var x = isNumber({value: value}) ? value.toString() : '0';
    return parseFloat(x.replace(/,/g, ''));
}