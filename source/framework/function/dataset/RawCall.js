import { getServiceByName } from '../../core/Service';

import { errorLog } from '../../core/Log';
import { callback } from '../../core/Helper';
import { result } from '../../core/Format';

// Function name
export const name = 'rawCall';
const logInformation = 'Source - Framework - Function - Dataset - RawCall';

/**
 * Run the function
 * Do an api request through the navision connector.
 * @param {object} parameters
 *                  - connector {string}
 *                  - path {string}
 *                  - method {string}
 *                  - options
 *                  - header
 *                  - data
 *                  - file
 *                  - extra
 *                  - callback {function} - The array to be averaged. (O)
 *                  - errCallback {function} - The array to be averaged. (O)
 * 
 * @return {result} - {object}
 */
export default run = (parameters) => {
    // Prepare the initial variable to be used
    let message = '';

    // Make sure the connector exists
    if (!parameters.connector || typeof parameters.connector !== 'string') {
        message = 'The parameter connector is required and must be a string.';
        errorLog(`${logInformation} - run - ${message}`);
        callback(parameters.errCallback, message, parameters.extra);
        return result(false, null, message);
    }

    // Get the connector from the services list
    var connector = getServiceByName(parameters.connector);

    // Check if connector exists
    if (!connector) {
        message = 'The raw-connector is not existing.';
        errorLog(`${logInformation} - run - ${message}`);
        callback(parameters.errCallback, message, parameters.extra);
        return result(false, null, message);
    }

    const dataCallback = (response) => {
        parameters.callback(response.dt, parameters.extra)
    }

    parameters.dataCallback = dataCallback

    // Submit and process the request through the connector class
    return connector.request(parameters);
}