import { getServiceByName } from '../../core/Service';
import { errorLog } from '../../core/Log';
import { DefaultValueArray } from '../../core/Constant';
import { setComponentStateProperties } from '../../core/Component';
import { callback } from '../../core/Helper';

import reducerAction from '../../core/reducer/Action';
import produce from 'immer';

// Function name
export const name = 'loadData';

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Function - Dataset - LoadData - ';

/**
 * Get the data from the service / dataset and
 * also affects the current active page if it contains
 * the datalist of the dataset.
 * @param {object} parameters
 *                  - dataset {string} - name of the service (R)
 *                  - limit {int} - limit of data to display
 *                  - page {int} - page of data
 *                  - filter {object} - filter the data with the condition of and
 *                  - orFilter {object} - filter the data with the condition of or
 *                  - order {string} - asc or desc
 *                  - extra {any} - extra parameter to pass onto the next callback
 *                  - callback {function} - function to call if the value is fetched
 *                  - errCallback {function} - function to call if the value fetching returns error
 * 
 * @return {array}
 */
export default run = (stateName, pageProps, parameters, pageActions) => {
    // Prepare the initial variable to be used
    let logFunctionName = 'run: ';

    // Validate the parameters
    if (!parameters.dataset || typeof parameters.dataset !=='string') {
        errorLog(logInformation + logFunctionName + 'The parameter dataset is required and must be a string.');
        return DefaultValueArray;
    }
    if ((parameters.limit && isNaN(parameters.limit)) || (parameters.page && isNaN(parameters.page))) {
        errorLog(logInformation + logFunctionName + 'The parameter limit and page must be an integer.');
        return DefaultValueArray;
    }

    // Get the component from the services
    let service = getServiceByName(parameters.dataset);

    // Check if the component and method exists
    if (!service || !service.load) {
        errorLog(logInformation + logFunctionName + 'The component ' + parameters.dataset + ' or the method is not found.');
        return DefaultValueArray;
    }

    // Prepare the callback for the load method
    let successCallback = (data, loadNext) => {
        // Prepare the payload for updating the reducer
        let payload = {
            component: service.name,
            data: data,
            append: loadNext,
            pageActions: pageActions
        };

        // Dispatch it to the redux store
        pageProps.dispatch(reducerAction(name, stateName, payload));

        callback(parameters.callback, data, parameters.extra);
    }

    let errorCallback = (error) => {
        errorLog(logInformation + logFunctionName + (error.err ? error.err : error));
        callback(parameters.errCallback, error, parameters.extra);
    }

    // Trigger the load data from the component
    let result = service.load(parameters, successCallback, errorCallback);
    let data = result.data || DefaultValueArray;
 
    // Check if processing is valid
    if (!result.success) {
        errorLog(logInformation + logFunctionName + result.message);
    }
 
    return data;
}
 
/**
 * Reducer logic
 * @param {object} state 
 * @param {object} payload 
 * 
 * @return {object}
 */
export const reducerLogic = (state, payload) => {
    try {
        return produce(state, draftState => {
            // Manipulate draftState into the intended state change
            setComponentStateProperties(draftState, { componentData: { data: payload.data, append: payload.append } }, payload.component, 'dataset', payload.pageActions);
        });
    } catch(e) {
        return { ...state };
    }
}