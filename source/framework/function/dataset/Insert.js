import { getServiceByName } from '../../core/Service';
import { errorLog } from '../../core/Log';
import { DefaultValueObject } from '../../core/Constant';
import { callback } from '../../core/Helper';

// Function name
export const name = 'insert';

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Function - Dataset - Insert - ';

/**
 * Inserts data into a service / dataset
 * @param {object} parameters
 *                 - dataset {string} - The name of the service. (R)
 *                 - dt {object} - object of data to insert into the service (O)
 *                 - extra {any} - extra parameter to pass onto the next callback
 *                 - callback {function} - function to call if the value is fetched
 *                 - errCallback {function} - function to call if the value fetching returns error
 * 
 * @return {object}
 */
export default run = (parameters) => {
    // Prepare the initial variable to be used
    let logFunctionName = 'run: ';

    // Validate the input
    if (!parameters.dataset || typeof parameters.dataset !=='string') {
        errorLog(logInformation + logFunctionName + 'The parameter dataset is required and must be a string.');
        return DefaultValueObject;
    }
    if (!parameters.dt || typeof parameters.dt !=='object') {
        errorLog(logInformation + logFunctionName + 'The parameter dt is required and must be an object.');
        return DefaultValueObject;
    }

    // Get the component from the services
    let service = getServiceByName(parameters.dataset);

    // Check if the component and method exists
    if (!service || !service.insert) {
        errorLog(logInformation + logFunctionName + 'The component ' + parameters.dataset + ' or the method is not found.');
        return DefaultValueObject;
    }

    // Prepare the callback for the clear method
    let successCallback = (data) => {
        callback(parameters.callback, data, parameters.extra);
    }
    let errorCallback = (error) => {
        errorLog(logInformation + logFunctionName + (error.err ? error.err : error));
        callback(parameters.errCallback, error, parameters.extra);
    }

    // Trigger the load data from the component
    let result = service.insert(parameters.dt, true, successCallback, errorCallback);
    let data = result.data || DefaultValueObject;
 
    // Check if processing is valid
    if (!result.success) {
        errorLog(logInformation + logFunctionName + result.message);
    }
 
    return data;
}