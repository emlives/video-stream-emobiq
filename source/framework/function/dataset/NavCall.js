import { getServiceByName } from '../../core/Service';

import { errorLog } from '../../core/Log';
import { callback } from '../../core/Helper';
import { result } from '../../core/Format';

// Function name
export const name = 'navCall';

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Function - Dataset - NavCall - ';

/**
 * Run the function
 * Do an api request through the navision connector.
 * @param {object} parameters
 *                 - connector {string} - The array to be averaged. (R)
 *                 - type {string} - The array to be averaged. (O)
 *                 - entity {string} - The array to be averaged. (O)
 *                 - function {string} - The array to be averaged. (O)
 *                 - subfunction {string} - The array to be averaged. (O)
 *                 - data {object} - The array to be averaged. (O)
 *                 - extra {any} - The array to be averaged. (O)
 *                 - batch {any} - The array to be averaged. (O)
 *                 - callback {function} - The array to be averaged. (O)
 *                 - errCallback {function} - The array to be averaged. (O)
 * 
 * @return {result} - {object}
 */
export default run = (parameters) => {
    // Prepare the initial variable to be used
    let logFunctionName = 'run: ';
    let message = '';

    // Make sure the connector exists
    if (!parameters.connector || typeof parameters.connector !== 'string') {
        message = 'The parameter connector is required and must be a string.';
        errorLog(logInformation + logFunctionName + message);
        callback(parameters.errorCallback, message, parameters.extra);
        return result(false, null, message);
    }

    // Validate the type of the parameters
    if ((parameters.type && typeof parameters.type !== 'string') ||
        (parameters.entity && typeof parameters.entity !== 'string') || 
        (parameters.function && typeof parameters.function !== 'string') ||
        (parameters.subfunction && typeof parameters.subfunction !== 'string')
        ) {
        message = 'The parameter type, entity, function and subfunction must be a string.';
        errorLog(logInformation + logFunctionName + message);
        callback(parameters.errorCallback, message, parameters.extra);
        return result(false, null, message);
    } 

    // Get the connector from the services list
    var connector = getServiceByName(parameters.connector);

    // Check if connector exists
    if (!connector) {
        message = 'The navision connector is not existing.';
        errorLog(logInformation + logFunctionName + message);
        callback(parameters.errorCallback, message, parameters.extra);
        return result(false, null, message);
    }

    // Remove the unnecessary information from the parameter before doing the request
    delete parameters.connector;

    // Submit and process the request through the connector class
    return connector.request(parameters);
}