import { getServiceByName } from '../../core/Service';
import { errorLog } from '../../core/Log';
import { DefaultValueArray } from '../../core/Constant';
import { callback } from '../../core/Helper';

// Function name
export const name = 'loadNext';

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Function - Dataset - LoadNext - ';

/**
 * Load the next required data from the service / dataset and
 * also affects the current active page if it contains
 * the datalist of the dataset - through the previous load data.
 * @param {object} parameters
 *                  - dataset {string} - name of the service (R)
 *                  - datasetDisplay {boolean} - If there is another datalist component 
 *                                               make it behave like load next
 *                  - beforeCallback {function} - function to call before the loading happens
 * 
 * @return {array}
 */
export default run = (parameters) => {
    // Prepare the initial variable to be used
    let logFunctionName = 'run: ';

    // Validate the parameters
    if (!parameters.dataset || typeof parameters.dataset !== 'string') {
        errorLog(logInformation + logFunctionName + 'The parameter dataset is required and must be a string.');
        return DefaultValueArray;
    }

    // Get the component from the services
    let service = getServiceByName(parameters.dataset);

    // Check if the component and method exists
    if (!service || !service.loadNext) {
        errorLog(logInformation + logFunctionName + 'The component ' + parameters.dataset + ' or the method is not found.');
        return DefaultValueArray;
    }

    // Prepare the callback for the load next method
    let beforeCallback = () => {
        callback(parameters.beforeCallback);
    }

    // Trigger the load data from the component
    let result = service.loadNext(beforeCallback);
    let data = result.data || DefaultValueArray;
 
    // Check if processing is valid
    if (!result.success) {
        errorLog(logInformation + logFunctionName + result.message);
    }
 
    return data;
}