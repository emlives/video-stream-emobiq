import { getServiceByName } from "../../core/Service";
import { errorLog } from '../../core/Log';
import { DefaultValueArray } from '../../core/Constant';
import { callback } from '../../core/Helper';

// Function name
export const name = 'updateByMulti';

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Function - Dataset - UpdateByMulti - ';

/**
 * Update the data from the service / dataset
 * based on the filter.
 * @param {object} parameters
 *                 - dataset {string} - name of the service (R)
 *                 - filter - array of objects {f: o: v:} where f = field-to-filter, o = comparison-operator, v = compare-value 
 *                 - orFilter - array of objects {f: o: v:} where f = field-to-filter, o = comparison-operator, v = compare-value 
 *                 - first {boolean} - option to only get first data
 *                 - data {object} - value to be updated in the service
 *                 - first {boolean} - option to only delete the first data
 *                 - extra {any} - extra parameter to pass onto the next callback
 *                 - callback {function} - function to call if the value is fetched
 *                 - errCallback {function} - function to call if the value fetching returns error
 * 
 * @return {array}
 */
export default run = (parameters) => {
    // Prepare the initial variable to be used
    let logFunctionName = 'run: ';

    // Validate the parameters
    if (!parameters.dataset || typeof parameters.dataset !== 'string') {
        errorLog(logInformation + logFunctionName + 'The parameter dataset is required and must be a string.');
        return DefaultValueArray;
    }
    if (!parameters.data || typeof parameters.data !== 'object') {
        errorLog(logInformation + logFunctionName + 'The parameter data is required and must be an object.');
        return DefaultValueArray;
    }

    // Get the component from the services
    let service = getServiceByName(parameters.dataset);

    // Check if the component and method exists
    if (!service || !service.update) {
        errorLog(logInformation + logFunctionName + 'The component ' + parameters.dataset + ' or the method is not found.');
        return DefaultValueArray;
    }

     // Prepare the payload to load the data with
     let payload = {};
   
     // Prepare the filtering (filter and orFilter) options
     const filterArray = _parseFilterFromParameters(parameters.filter)
     const orFilterArray = _parseFilterFromParameters(parameters.orFilter)
 
     payload.filter = filterArray
     payload.orFilter = orFilterArray

    // Prepare to limit the result if needed
    let first = (typeof parameters.first === 'boolean' ? parameters.first : false);

    // Prepare the callback for the clear method
    let successCallback = (data) => {
        // Manipulate the data if only one record is expected
        if (first) {
            data = (data.length == 0 ? false : data[0]);
        }
        callback(parameters.callback, data, parameters.extra);
    }
    let errorCallback = (error) => {
        errorLog(logInformation + logFunctionName + (error.err ? error.err : error));
        callback(parameters.errCallback, error, parameters.extra);
    }

    // Trigger the load data from the component
    let result = service.update(parameters.data, payload, first, successCallback, errorCallback);
    let data = result.data || DefaultValueArray;
  
    // Check if processing is valid
    if (!result.success) {
        errorLog(logInformation + logFunctionName + result.message);
    }
  
    // Manipulate the data if only one record is expected
    if (first) {
        data = (data.length == 0 ? false : data[0]);
    }
  
    return data;
}

const _parseFilterFromParameters = (filterParam) => {
    let filterArray = []
    if(filterParam && Array.isArray(filterParam)) {
        filterParam.forEach(function (arrayItem) {
            var filter = {};
            filter['field'] = arrayItem.f; //field
            filter['operator'] = arrayItem.o; //operation
            filter['value'] = arrayItem.v; // value of field
            filterArray.push(filter)
        });
    }

    return filterArray
}