import { getServiceByName } from '../../core/Service';
import { errorLog } from '../../core/Log';
import { DefaultValueArray } from '../../core/Constant';
import { callback } from '../../core/Helper';

// Function name
export const name = 'dataFromString';

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Function - Dataset - DataFromString - ';

/**
 * Insert the data to the service / dataset
 * @param {object} parameters
 *                  - dataset {string} - name of the service (R)
 *                  - string {any} - the string, array or object of data to be inserted
 *                  - append {boolean} - identifies whether to overwrite or just append the data
 *                  - extra {any} - extra parameter to pass onto the next callback
 *                  - callback {function} - function to call if the value is cleared
 *                  - errCallback {function} - function to call if the clearing value returns error
 * 
 * @return {array}
 */
export default run = (parameters) => {
    // Prepare the initial variable to be used
    let logFunctionName = 'run: ';

    // Validate the parameters
    if (!parameters.dataset || typeof parameters.dataset !== 'string') {
        errorLog(logInformation + logFunctionName + 'The parameter dataset is required and must be a string.');
        return DefaultValueArray;
    }

    // Get the component from the services
    let service = getServiceByName(parameters.dataset);

    // Check if the component and method exists
    if (!service || !service.insert) {
        errorLog(logInformation + logFunctionName + 'The component ' + parameters.dataset + ' or the method is not found.');
        return DefaultValueArray;
    }

    // Prepare the append value
    let append = (typeof parameters.append === 'boolean' ? parameters.append : false);

    // Prepare the callback for the clear method
    let successCallback = (data) => {
        callback(parameters.callback, data, parameters.extra);
    }
    let errorCallback = (error) => {
        errorLog(logInformation + logFunctionName + (error.err ? error.err : error));
        callback(parameters.errCallback, error, parameters.extra);
    }

    // Trigger the load data from the component
    let result = service.insert(parameters.string, append, successCallback, errorCallback);
    let data = result.data || DefaultValueArray;

    // Check if processing is valid
    if (!result.success) {
        errorLog(logInformation + logFunctionName + result.message);
    }

    return data;
}