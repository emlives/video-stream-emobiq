import { errorLog } from '../../core/Log';

// Function name
export const name = "and";

/**
 * Returns true if and only if all of its operands are true
 * 
 * @param parameters.value1 - The first operand
 * @param parameters.value2 - The second operand
 * @return {boolean} - Returns true if and only if all of its operands are true
 */
export default run = (parameters) => {
    var value1 = parameters.value1
    var value2 = parameters.value2

    if (!value1 || !value2) {
        errorLog('Source - Framework - Function - Comparation - And - run: ' + 'The parameters value1 or value2 is null'); 
        return false
    }

    return !!(value1 && value2);
}