import { errorLog } from '../../core/Log';

// Function name
export const name = "not";

/**
 * Changes true to false, and false to true
 * 
 * @param parameters.value1 - The first operand
 * @param parameters.value2 - The second operand
 * @return {boolean} - Returns boolean result
 */
export default run = (parameters) => {
    var value = parameters.value

    if (value == undefined) {
        errorLog('Source - Framework - Function - Comparation - Not - run: ' + 'The parameters.value is null'); 
        return false
    }

    return !value;
}