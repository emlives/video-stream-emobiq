import { errorLog } from '../../core/Log';

// Function name
export const name = "xor";

/**
 * Takes two boolean operands and returns true if and only if the operands are different
 * 
 * @param parameters.value1 - The first operand
 * @param parameters.value2 - The second operand
 * @return {boolean} - Returns boolean result
 */
export default run = (parameters) => {
    var x = parameters.value1;
    var y = parameters.value2;

    if (!x || !y) {
        errorLog('Source - Framework - Function - Comparation - Xor - run: ' + 'The parameters value1 or value2 is null'); 
        return false
    }

    return (x && !y) || (!x && y);
}