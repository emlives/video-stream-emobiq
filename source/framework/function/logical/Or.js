import { errorLog } from '../../core/Log';

// Function name
export const name = "or";

/**
 * Returns true if and only if one or more of its operands is true
 * 
 * @param parameters.value1 - The first operand
 * @param parameters.value2 - The second operand
 * @return {boolean} - Returns boolean result
 */
export default run = (parameters) => {
    var value1 = parameters.value1
    var value2 = parameters.value2

    if (!value1 || !value2) {
        errorLog('Source - Framework - Function - Comparation - Or - run: ' + 'The parameters value1 or value2 is null'); 
        return false
    }

    return !!(parameters.value1 || parameters.value2);
}