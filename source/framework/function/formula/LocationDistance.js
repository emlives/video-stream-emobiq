import { errorLog } from '../../core/Log';
import { DefaultValueNumber } from '../../core/Constant'
import { isNumber, degreeToRadius } from '../../core/Helper'


// Function name
export const name = "locationDistance";

/**
 * Returns the distance between 2 points
 * 
 * @param {string} parameters.latitudeA - The latitude of first point
 * @param {string} parameters.longitudeA - The longitude of first point
 * @param {string} parameters.latitudeB - The latitude of second point
 * @param {string} parameters.longitudeB - The longitude of second point
 * @return {float} - The distance
 */
export default run = (parameters) => {
    if (!parameters.latitudeA || !parameters.longitudeA || !parameters.latitudeB || !parameters.longitudeB) {
        errorLog('Source - Framework - Function - Formula - LocationDistance - run: ' + 'Missing parameters');
        return DefaultValueNumber
    }

    // Make sure the value passed are valid numbers
    if (!isNumber(parameters.latitudeA) || !isNumber(parameters.longitudeA) || !isNumber(parameters.latitudeB) || !isNumber(parameters.longitudeB)) {
        errorLog('Source - Framework - Function - Formula - LocationDistance - run: ' + 'Some values are invalid');
        return DefaultValueNumber
    }  

    // Prepare the variables and convert to float to be safe
    var latA = parseFloat(parameters.latitudeA);
    var longA = parseFloat(parameters.longitudeA);
    var latB = parseFloat(parameters.latitudeB);
    var longB = parseFloat(parameters.longitudeB);

    // Caclulate the distance
    var result = 0;
    var earthRadius = 6371; // Radius of the earth in km
    var distanceLat = degreeToRadius(latA - latB);
    var distanceLong = degreeToRadius(longA - longB); 
    
    result = Math.sin(distanceLat / 2) * Math.sin(distanceLat / 2) + Math.cos(degreeToRadius(latA)) * Math.cos(degreeToRadius(latB)) * Math.sin(distanceLong / 2) * Math.sin(distanceLong / 2);
    result = 2 * Math.atan2(Math.sqrt(result), Math.sqrt(1 - result)); 
    result = earthRadius * result; // Distance in km

    return result;
}