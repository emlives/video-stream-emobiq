// Function name
export const name = 'equal';

/**
 * Returns true if value1 == value2. Otherwise false
 * 
 * @param {string} parameters.value1 - The value1 of the number
 * @param {string} parameters.value2 - The value2 of the number
 * @return {boolean} - The result of equal
 */
export default run = (parameters) => {
    var value1 = parameters.value1
    var value2 = parameters.value2

    return value1 == value2;
}