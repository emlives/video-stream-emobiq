// Function name
export const name = "sc";

export const scList = ('391 Alpha,392 Beta,393 Gamma,394 Delta,395 Epsilon,396 Zeta,397 Eta,398 Theta,399 Iota,39A Kappa,39B Lambda,39C Mu,39D Nu,39E Xi,39F Omicron,3A0 Pi,3A1 Rho,3A3 Sigma,3A4 Tau,3A5 Upsilon,3A6 Phi,3A7 Chi,3A8 Psi,3A9 Omega,' +
'3B1 alpha,3B2 beta,3B3 gamma,3B4 delta,3B5 epsilon,3B6 zeta,3B7 eta,3B8 theta,3B9 iota,3BA kappa,3BB lambda,3BC mu,3BD nu,3BE xi,3BF omicron,3C0 pi,3C1 rho,3C2 sigmaf,3C3 sigma,3C4 tau,3C5 upsilon,3C6 phi,3C7 chi,3C8 psi,3C9 omega,' +
'9 tab,A lineFeed,D carriage,20 space,A0 nbsp,' +
'A9 copy,AE reg,2122 tm,B0 deg,B1 plusmin,' +
'20AC euro,A3 pound,A5 yen,A2 cent').split(',');

/**
 * Returns an object
 */
export default run = (function() {
    var executed = false;
    var obj = {}
    return function() {
        if (!executed) {
            // run once
            executed = true;

            scList.map( (sc) => {
                let [code, name] = sc.split(' ');
                obj[name] = String.fromCharCode(parseInt(code, 16));
            });
        }

        return obj
    };
})();