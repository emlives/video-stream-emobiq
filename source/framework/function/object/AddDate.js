import {dbDate, addDate, isString, stringToDate} from '../../core/Helper';
import { errorLog } from '../../core/Log';

// Function name
export const name = "dbDate";

/**
 * Increase the current date
 * @param {string} parameters.date - The date string
 * @param {object} parameters.add - The increment to date
 * @param {object} parameters.type - The type as 'days','hour','hours','minute','minutes','second','seconds', defaulted to days
 * @return {date} - The increment date
 */
export default run = (parameters) => {
    let date = parameters['date'] || new Date();
    let add = parameters['add'] || 0;
    let type = parameters['type'] || 'days';
    let recommendedTypeValue = ['days','hour','hours','minute','minutes','second','seconds']
    if (type && !recommendedTypeValue.includes(type)) {
        errorLog('Source - Framework - Function - Object - AddDate - run: ' + 'The value of parameter type is supposed one of these options ("days","hour","hours","minute","minutes","second","seconds").');
    }
    // Convert the string to date if not date type
    if (isString(date)) {
        date = stringToDate(date);
    }
    
    // Check if time is needed
    let withTime = (type != 'date' && type != 'day') ? true : false

    return dbDate(addDate(add, date, type), withTime);
}