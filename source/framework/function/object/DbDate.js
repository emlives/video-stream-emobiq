import {dbDate, addDate} from '../../core/Helper';

// Function name
export const name = "dbDate";

/**
 * Increase the current date
 * @param {boolean} parameters.withTime - Whether to print the date with time of not
 * @param {boolean} parameters.add - Number to add
 * @param {boolean} parameters.type - The type of value to add the number into (days || hours || minutes || seconds). Defaulted to 'days'
 * 
 * @return {date} - The current date incremented
 */
export default run = (parameters) => {
    var withTime = parameters['withTime'] || false;
    var add = parameters['add'] || 0;
    var type = parameters['type'] || 'days';

    return dbDate(addDate(add, null, type), withTime);
}