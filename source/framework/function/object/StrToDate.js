import { stringToDate } from '../../core/Helper';
// Function name
export const name = "strToDate";

/**
 * Convert string to date
 * @param {string} parameters.date - The string to be converted to date
 * Note: This doesn't return result to support older version (legacy).
 * @return {date} - Returns date
 */
export default run = (parameters) => {
    if (!parameters['date']) {
        if (parameters['date']==null){
            return null;
        } else {
            return stringToDate();
        }
    }
    if (!(parameters['date'] instanceof Date)) {
        // Get the timezone 
        return stringToDate(parameters['date']);
    }
    return parameters['date']
}