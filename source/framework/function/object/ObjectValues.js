import { errorLog } from '../../core/Log';
import { DefaultValueArray } from '../../core/Constant';

// Return the values of an object
export const name = "objectValues";

/**
 * @param {object} parameters  
 *                  - object {object} - the object to get the values from
 */
export default run = (parameters) => {
    if (!parameters.object || typeof parameters.object !== 'object') {
        errorLog('Source - Framework - Function - Object - ObjectValues - run: ' + 'The parameter object is required and must be an object.');
        return DefaultValueArray;
    }
    let keys = [];
    for (var prop in parameters.object) {
        keys.push(parameters.object[prop]);
    }
    return keys;
}