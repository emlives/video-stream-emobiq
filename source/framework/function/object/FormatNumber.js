import { errorLog } from '../../core/Log';

// Function name
export const name = "formatNumber"

/**
 * Run the function
 * Change the number format
 * @param {object} parameters
 *                 - value {int} - minimum number
 *                 - decimals {int} - maximum number
 */
export default run = (parameters) => {
    if (isNaN(parameters['value'])) {
        errorLog('Framework - Function - Object - FormatNumber: parameter value is not a number');
        parameters['value'] = 0;
    }
    if (isNaN(parameters['decimals'])) {
        errorLog('Framework - Function - Object - FormatNumber: parameter decimals is not a number');
        parameters['decimals'] = 0;
    }
    
    // assign default value
    let number = parameters['value'] || 0;
    let decimals = (parameters['decimals'] + '') || '0';
    let decimalSep = parameters['decimalSep'] || '.';
    let thousandSep = parameters['thousandSep'] || '';
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');

    // check for Finite and data type
    number = !isFinite(+number) ? 0 : +number,
    precision = !isFinite(+parseInt(decimals)) ? 0 : Math.abs(parseInt(decimals));
    thousandSep = (typeof thousandSep === 'undefined') ? ',' : thousandSep;
    decimalSep = (typeof decimalSep === 'undefined') ? '.' : decimalSep;
    let result = '';
    let toFixedFix = function (n, precision) {
            var value = '';

            // Check for negative values
            if (n < 0) {
                value = Number(Math.round((n * -1) + 'e' + precision) + 'e-' + precision) * -1;
            } else {
                value = Number(Math.round(n + 'e' + precision) + 'e-' + precision);
            }

            return '' + value;
        };

    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    result = (precision ? toFixedFix(number, precision) : '' + Math.round(number)).split('.');

    if (result[0].length > 3) {
        result[0] = result[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, thousandSep);
    }

    if ((result[1] || '')
        .length < precision) {
        result[1] = result[1] || '';
        result[1] += new Array(precision - result[1].length + 1)
            .join('0');
    }

    return result.join(decimalSep);
}