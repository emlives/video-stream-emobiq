import { stringToDate, isString } from '../../core/Helper';
import { errorLog } from '../../core/Log';
// Function name
export const name = "dateDiff";

/**
 * Calculate date difference
 * @param {object} parameters
 *                  - date1 {string|date} - the first date string
 *                  - date2 {string|date} - the second date string
 *                  - interval {string} - the type of interval (years, months, weeks, days, hours, minutes, seconds)
 */
export default run = (parameters) => {
    let date1 = parameters['date1'] || new Date(),
        date2 = parameters['date2'] || new Date(),
        intervalOptions = ['years', 'months', 'weeks', 'days', 'hours', 'minutes', 'seconds'],
        interval = parameters['interval'] || 'days';
    // Validation
    if (interval && !intervalOptions.includes(interval)) {
        errorLog('Source - Framework - Function - Object - dateDiff - run: ' + 'The value of parameter interval is supposed one of these options (years, months, weeks, days, hours, minutes, seconds).');
    }
    
    // Convert string to date
    if (isString(date1)) {
        date1 = stringToDate(date1);
    }
    if (isString(date2)) {
        date2 = stringToDate(date2);
    }
    
    // Calculate diff
    let second = 1000,
        minute = second * 60,
        hour = minute * 60,
        day = hour * 24,
        week = day * 7;
    
    let fromDate = new Date(date1),
        toDate = new Date(date2),
        timediff = toDate - fromDate;
    if (isNaN(timediff)) return NaN;
    switch (interval) {
        case "years":
            return toDate.getFullYear() - fromDate.getFullYear();
        case "months":
            return (
                (toDate.getFullYear() * 12 + toDate.getMonth()) -
                (fromDate.getFullYear() * 12 + fromDate.getMonth())
            );
        case "weeks":
            return Math.floor(timediff / week);
        case "days":
            return Math.floor(timediff / day);
        case "hours":
            return Math.floor(timediff / hour);
        case "minutes":
            return Math.floor(timediff / minute);
        case "seconds":
            return Math.floor(timediff / second);
        default:
            return 0;
    }
}