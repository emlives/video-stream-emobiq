import { formatDate } from '../../core/Helper';
import { errorLog } from '../../core/Log';
// Function name
export const name = "formatDate";

/**
 * Change the format of a date
 * @param {object} parameters
 *                  - date {date} - the date to process
 *                  - format {string} - format of the date
 */
export default run = (parameters) => {
    if (!parameters['date']) {
        return formatDate(parameters['format'], new Date());
    }
    if (!(parameters['date'] instanceof Date)) {
        // Replace the - to / for iOS
        var date = parameters['date'].replace(/-/g, '/');
        // Remove the milliseconds if have
        if (date.indexOf('.') > -1) {
            date = date.substring(0, date.indexOf('.'));
        }
        // Get the timezone 
        return formatDate(parameters['format'], new Date(date));
    }
    return formatDate(parameters['format'], parameters['date']);
}