import { errorLog } from '../../core/Log';
import { DefaultValueArray } from '../../core/Constant';

// Function name
export const name = "objectKeys";

/**
 * Return the keys of an object
 * @param {object} parameters  
 *                  - object {object} - the object to get the keys from
 */
export default run = (parameters) => {
    if (!parameters.object || typeof parameters.object !== 'object') {
        errorLog('Source - Framework - Function - Object - ObjectKeys - run: ' + 'The parameter object is required and must be an object.');
        return DefaultValueArray;
    }
    let keys = [];
    for (var prop in parameters.object) {
        keys.push(prop);
    }
    return keys;
}