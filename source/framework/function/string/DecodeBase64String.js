import { errorLog } from '../../core/Log';
import { decode as atob } from 'base-64';

// Function name
export const name = 'decodeBase64String';

/**
 * Decode a base-64-encoded string
 * 
 * Note: This doesn't return DefaultValueString (when parameters.string is null) to support older version (legacy).
 * @param {string} parameters.string - Input string
 * 
 * @return {string} - Returns a decoded a base-64-encoded string
 */
export default run = (parameters) => {
    let value = parameters.string;

    // Log if there is no value
    if (!value) {
        errorLog('Source - Framework - Function - String - DecodeBase64String - run: ' + 'The parameters.string is null');
        value = '';
    }

    return decodeURIComponent(Array.prototype.map.call(atob(str), function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
}