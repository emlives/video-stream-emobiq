import { errorLog } from '../../core/Log';
import { DefaultValueString } from '../../core/Constant'

// Function name
export const name = "lowerCase";

/**
 * Convert the string to lowercase letters
 * 
 * @param {string} parameters.string - The input string
 * @return {string} - Lowercased of input string
 */
export default run = (parameters) => {
    let str = parameters.string
    
    if (!str) {
        errorLog('Source - Framework - Function - String - LowerCase - run: ' + 'The parameter string is null'); 
        return DefaultValueString
    }
    return str.toLowerCase();
}