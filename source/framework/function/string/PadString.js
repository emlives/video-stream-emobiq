import { errorLog } from '../../core/Log';
import { stringPad } from '../../core/Helper'

// Function name
export const name = "padString";

/**
 * Padding function for string
 * 
 * @param {string} parameters.string - The input string
 * @param {string} parameters.char - The characters to pad to satisfy the specified len
 * @param {string} parameters.type - type = left, right, center. If type=left, char will be padded on right
 * @param {number} parameters.len - The pad length
 */
export default run = (parameters) => {
    var str = parameters['string'];
    var char = parameters['char'];
    var type = parameters['type'];
    var len = parameters['len'];

    if (!str || !char || !len) {
        errorLog('Source - Framework - Function - String - PadString - run: ' + 'The parameters.string or parameters.char or parameters.len is null'); 
    }

    //validation and default values are inside helper stringPad function
    return stringPad(str, char, type, len);
} 