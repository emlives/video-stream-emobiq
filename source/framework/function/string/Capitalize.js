import { errorLog } from '../../core/Log';
import { DefaultValueString } from '../../core/Constant'

// Function name
export const name = "capitalize";

/**
 * Capitalize the string
 * 
 * @param parameters.string - The input string
 * @return {string} - Capitalized of input string
 */
export default run = (parameters) => {
    if (!parameters.string) {
        errorLog('Source - Framework - Function - String - Capitalize - run: ' + 'The parameter string is null');
        return DefaultValueString
    }
    return (parameters.string ? parameters.string.capitalize() : '');
}