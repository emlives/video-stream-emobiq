import { translate } from '../../core/Language';

// Function name
export const name = 'concat';

/**
 * Concate an array of strings
 * 
 * @param parameters.string[i] - string1, string2, string3, string4 are the input string
 * @param parameters.length - Currently not available in Editor, defaulted to 10
 * 
 * @return {string} - The concatenated strings
 */
export default run = (parameters) => {
    const length = parameters.length || 10;

    var validStrings = [];
    for (let i = 1; i <= length; i++) {
        let currentString = parameters['string' + i];
        if (currentString) {
            validStrings.push(currentString);
        }
    }

    var result = '';
    for (let str of validStrings) {
        let translatedStr = translate(str);
        result += (translatedStr || '');
    }
    
    return result;
}