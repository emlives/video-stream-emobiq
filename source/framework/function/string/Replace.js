import { errorLog } from '../../core/Log';
import { DefaultValueString } from '../../core/Constant'

// Function name
export const name = "replace";

/**
 * Searches a string for a specified value, or a regular expression, 
 * and returns a new string where the specified values are replaced.
 * 
 * @param {string} parameters.string - The input string
 * @param {string} parameters.replace - A string to search for replacement, or a regExpstring
 * @param {string} parameters.replacement - The new value to replace
 * @param {boolean} parameters.regExp - String value of 'true' to indicate parameters.replace is a regExp
 * @return {string} - Returns the replaced string
 */
export default run = (parameters) => {
    // Check if replace have value
    var str = parameters['string']; // input string
    var searchvalue = parameters['replace']; //search for 
    var newvalue = parameters['replacement']; // replace with value

    if (!str || !searchvalue) {
        errorLog('Source - Framework - Function - String - Replace - run: ' + 'The parameters string or replace or replacement is null'); 
        return DefaultValueString;
    }

    // Check if regular expression or normal string
    if (parameters['regExp'] == 'true') {
        // Convert the string into a proper regex
        var match = searchvalue.match(new RegExp('^/(.*?)/([gimy]*)$'));
        // Run the regex
        return str.replace(new RegExp(match[1], match[2]), newvalue || '');
    } 
    else {
        return str.replace(new RegExp(searchvalue, "g"), newvalue || '');
    }
} 