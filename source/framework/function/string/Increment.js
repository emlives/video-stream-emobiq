import { errorLog } from '../../core/Log';

// Function name
export const name = 'increment';

/**
 * Option to increment encoding string with base 36 digits
 * 
 * @param {string} parameters.string - Input string
 * @param {boolean} parameters.useBase36 - True to base-36 encode else set false
 * 
 * @return {string} - Returns a decoded a base-64-encoded string
 */
export default run = (parameters) => {
    if (!parameters.string || !parameters.by) {
        errorLog('Source - Framework - Function - String - Increment - run: ' + 'The parameters.string or parameters.by is null');  
        return false;
    }

    // Use base36, Default is true
    var useBase36 = parameters.useBase36 === undefined ? true : parameters.useBase36;

    if (useBase36) {
        return ((parseInt(parameters.string, 36) + parameters.by).toString(36)).replace(/[0-1]/g,'a');
    } else {
        return parameters.string.replace(/(\d+)+/g, function(match, num){
            return stringPad(parseInt(num) + parameters.by, '0', 'right', num.length);
        });
    }
}