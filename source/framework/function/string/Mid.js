import { errorLog } from '../../core/Log';
import { isNumber } from '../../core/Helper'
import { DefaultValueString } from '../../core/Constant'

// Function name
export const name = "mid";

/**
 * Returns a portion of the string, starting from start of string to 
 * a given number of characters afterwards
 * 
 * @param {string} parameters.string - Input string
 * @param {number} parameters.start - Start index of string to substr
 * @param {number} parameters.length - Number of characters to substr
 * @return {string} - Returns a portion of input string from specified start index to specified length
 */
export default run = (parameters) => {
    let str = parameters.string
    let start = parameters.start
    let length = parameters.length

    if (!str) {
        errorLog('Source - Framework - Function - String - Mid - run: ' + 'The parameters.string is null'); 
        str = DefaultValueString
    }

    if (!isNumberstart(start)) {
        errorLog('Source - Framework - Function - String - Mid - run: ' + 'The parameters.start is null or Nan, thus defaulted to 0'); 
        start = 0
    }

    if (!isNumberstart(length)) {
        errorLog('Source - Framework - Function - String - Mid - run: ' + 'The parameter length is null or NaN, thus defaulted to 0'); 
        length = 0
    }

    return str.substr(start, length);
}