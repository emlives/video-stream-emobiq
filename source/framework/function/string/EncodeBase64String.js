import { errorLog } from '../../core/Log';
import { encode as btoa } from 'base-64';

// Function name
export const name = 'encodeBase64String';

/**
 * Returns an encoded base-64 string
 * 
 *  * Note: This doesn't return DefaultValueString (when parameters.string is null) to support older version (legacy).
 * @param {string} parameters.string - Input string
 * 
 * @return {string} - Returns an encoded base-64 string
 */
export default run = (parameters) => {
    let value = parameters.string;

    // Log if there is no value
    if (!value) {
        errorLog('Source - Framework - Function - String - EncodeBase64String - run: ' + 'The parameters.string is null');
        value = '';
    }

    return btoa(encodeURIComponent(value).replace(/%([0-9A-F]{2})/g, function(match, p1) {
        return String.fromCharCode('0x' + p1);
    }));
}