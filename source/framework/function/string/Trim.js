import { errorLog } from '../../core/Log';
import { DefaultValueString } from '../../core/Constant'

// Function name
export const name = "trim";

/**
 * Removes whitespace from both ends of the input string
 * 
 * @param {string} parameters.string - The input string
 * @return {string} - Trimed of input string
 */
export default run = (parameters) => {
    if (!parameters.string) {
        errorLog('Source - Framework - Function - String - Trim - run: ' + 'The parameters.string is null'); 
        return DefaultValueString
    }

    return parameters.string.trim();
}