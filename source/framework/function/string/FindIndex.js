import { errorLog } from '../../core/Log';
import { DefaultValueString } from '../../core/Constant'

// Function name
export const name = "findIndex";

/**
 * Returns the position of the first occurrence of a specified value in a string
 * 
 * @param {string} parameters.string - Input string
 * @param {string} parameters.find - The string to search for
 * @return {number} - Returns the position
 */
export default run = (parameters) => {
    var s = parameters.string
    var find = parameters.find

    if (!s) {
        errorLog('Source - Framework - Function - String - FindIndex - run: ' + 'The parameter string is null');
        s = DefaultValueString
    }

    if (!find) {
        errorLog('Source - Framework - Function - String - FindIndex - run: ' + 'The parameter find is null, thus defaulted to 10');
        find = 10
    }

    return s.indexOf(find)
}