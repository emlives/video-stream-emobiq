import { errorLog } from '../../core/Log';
import { DefaultValueString } from '../../core/Constant';

// Function name
export const name = "upperCase";

/**
 * Convert the string to uppercase letters
 * 
 * @param {string} parameters.string - The input string
 * @return {string} - Trimed of input string
 */
export default run = (parameters) => {
    let str = parameters.string
    if (!str) {
        errorLog('Source - Framework - Function - String - UpperCase - run: ' + 'The parameter string is null'); 
        return DefaultValueString
    }
    return str.toUpperCase();
}