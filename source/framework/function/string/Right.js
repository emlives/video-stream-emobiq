import { errorLog } from '../../core/Log';
import {isNumber} from '../../core/Helper'
import { DefaultValueString } from '../../core/Constant'

// Function name
export const name = "right";

/**
 * Returns a portion of the string, starting from end of string to 
 * a given number of characters afterwards
 * 
 * @param {string} parameters.string - Input string
 * @param {number} parameters.length - Number of characters from end of string
 * @return {string} - Returns a portion of input string from end to specified length
 */
export default run = (parameters) => {
    var s = parameters.string
    var length = parameters.length

    if (!s) {
        errorLog('Source - Framework - Function - String - Right - run: ' + 'The parameter string is null'); 
        return DefaultValueString
    }

    if (!length || !isNumber(length)) {
        errorLog('Source - Framework - Function - String - Right - run: ' + 'The parameter length is null or NaN, thus defaulted to 0'); 
        length = 0
    }

    return s.substr(s.length - length, length);
}