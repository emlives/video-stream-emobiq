import { errorLog } from '../../core/Log';

// Function name
export const name = "length";

/**
 * Returns length of a string
 * 
 * @param {string} parameters.string - Input string
 * Note: This doesn't return DefaultValueNumber to support older version (legacy).
 * @return {number} - Returns the length of string
 */
export default run = (parameters) => {
    var s = parameters.string

    if (!s) {
        errorLog('Source - Framework - Function - String - Length - run: ' + 'The parameters.string is null'); 
        return 0
    }

    return s.length
}