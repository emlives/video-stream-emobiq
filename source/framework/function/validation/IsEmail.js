// Function name
export const name = "isEmail";

/**
 * Validates an email
 * 
 * @param {string} parameters.value - The input string
 * @return {boolean} - Result of check
 */
export default run = (parameters) => {
    // var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    // return (parameters.value.match(mailformat) ? true : false);
    var input = parameters.value;
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(input).toLowerCase());
}


