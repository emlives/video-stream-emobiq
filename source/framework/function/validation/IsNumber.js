import { isNumber } from '../../core/Helper'

// Function name
export const name = "isNumber";

/**
 * Determines whether a value is a number
 * 
 * @param {string} parameters.value - The input string
 * @return {boolean} - Result of check
 */
export default run = (parameters) => {
    return isNumber(parameters.value)
}