import { errorLog } from '../../core/Log';
import { DefaultValueString } from '../../core/Constant'

// Function name
export const name = "valueFromURL"

/**
 * Get the value of certain parameter in the URL string
 * @param {object} parameters     
 *                  - url {string} - the URL string
 *                  - parameters {string} - the parameter to get the value from
 */
export default run = (parameters) => {
    let url = parameters['url'],
        name = parameters['parameter'];

    if (!url || !name) {
        errorLog('Source - Framework - Function - Browser - ValueFromURL - run: ' + 'The parameter or url is null).');
        return DefaultValueString
    }

    // Prepare the nae
    name = name.replace(/[\[\]]/g, "\\$&");
    // Prepare the regex
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
    // Find the param through regex
    var results = regex.exec(url);

    // Check if it contains data
    if (!results) {
        return null;
    } else if (!results[2]) {
        return DefaultValueString;
    } else {
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
}