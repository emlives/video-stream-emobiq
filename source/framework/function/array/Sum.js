import { errorLog } from '../../core/Log';
import { DefaultValueNumber } from '../../core/Constant';

// Function name
export const name = 'sum';

/**
 * Run the function
 * Loop through an array and add it up (sum)
 * @param {object} parameters
 *                 - values {array} - The array to be calculated. (R)
 * 
 * @return {float}
 */
export default run = (parameters) => {
    // Check if values is passed and in array type
    if (!parameters.values || !Array.isArray(parameters.values)) {
        errorLog('Source - Framework - Function - Array - Sum - run: The parameter values is required and must be an array.');
        return DefaultValueNumber;
    }

    // Prepare the result value
    let result = 0;

    // Go through the array
    for (let value of parameters.values) {
        // Parse the value
        let converted = parseFloat(value);

        // Check the type if not a number then return default
        if (Number.isNaN(converted)) {
            errorLog('Source - Framework - Function - Array - Sum - run: ' + 'One of values passed is not a valid number.');
            return DefaultValueNumber;
        }

        // Add the value
        result += converted;
    }

    return result;
}