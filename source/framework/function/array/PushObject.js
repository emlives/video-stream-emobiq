import { errorLog } from '../../core/Log';
import { DefaultValueObject } from '../../core/Constant';

import { Variables } from '../../../dependency/global/Variable';

// Function name
export const name = 'pushObject';

/**
 * Run the function
 * Create the key/value pair for the global variable specified
 * @param {object} parameters
 *                 - variableName {string} - The name of the object/array. (R)
 *                 - key {string} - The key of the value. (R)
 *                 - value {any} - The value to be pushed in the object/array. (R)
 * 
 * @return {object/array}
 */
export default run = (parameters) => {
    // Check if variable name is passed and it is a string
    if (!parameters.variableName || typeof parameters.variableName !== 'string' || !parameters.key || typeof parameters.key !== 'string') {
        errorLog('Source - Framework - Function - Array - PushObject - run: ' + 'The parameter variableName and key are required and must be a string.');
        return DefaultValueObject;
    }

    // Check if the object is existing and is an object/array in the global variable
    if (!Variables[parameters.variableName] || (!Array.isArray(Variables[parameters.variableName]) && typeof Variables[parameters.variableName] !== 'object')) {
        errorLog('Source - Framework - Function - Array - PushObject- run: ' + 'The object/array variable name does not exist or is not an array or object.');
        return DefaultValueObject;
    }

    // Generate the new key/value inside the o global variables
    Variables[parameters.variableName][parameters.key] = parameters.value;

    return Variables[parameters.variableName];
}