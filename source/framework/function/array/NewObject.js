import { errorLog } from '../../core/Log';
import { DefaultValueObject } from '../../core/Constant';

import { Variables } from '../../../dependency/global/Variable';

// Function name
export const name = 'newObject';

/**
 * Run the function
 * Create a named new object in the global variables
 * @param {object} parameters
 *                 - variableName {string} - The name to be assigned for the object. (R)
 * 
 * @return {object}
 */
export default run = (parameters) => {
    // Check if variable name is passed and it is a string
    if (!parameters.variableName || typeof parameters.variableName !== 'string') {
        errorLog('Source - Framework - Function - Array - NewObject - run: ' + 'The parameter variableName is required and must be a string.');
        return DefaultValueObject;
    }

    // Generate the new object inside the global variables
    Variables[parameters.variableName] = {};

    return Variables[parameters.variableName];
}