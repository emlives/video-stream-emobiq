import { errorLog } from '../../core/Log';
import { DefaultValueArray } from '../../core/Constant';

import { Variables } from '../../../dependency/global/Variable';

// Function name
export const name = 'shift';

/**
 * Run the function
 * Remove the first element within an array in the global variables
 * @param {object} parameters
 *                 - variableName {string} - The name of the array. (R)
 * 
 * @return {array}
 */
export default run = (parameters) => {
    // Check if variable name is passed and it is a string
    if (!parameters.variableName || typeof parameters.variableName !== 'string') {
        errorLog('Source - Framework - Function - Array - Shift - run: ' + 'The parameter variableName is required and must be a string.');
        return DefaultValueArray;
    }

    // Check if the array is existing and is an array in the global variable
    if (!Variables[parameters.variableName] || !Array.isArray(Variables[parameters.variableName])) {
        errorLog('Source - Framework - Function - Array - Shift - run: ' + 'The array variable name does not exist or is not an array.');
        return DefaultValueArray;
    }

    // Remove the first element from the array in the global variables
    Variables[parameters.variableName].shift();

    return Variables[parameters.variableName];
}