import { errorLog } from '../../core/Log';
import { DefaultValueArray } from '../../core/Constant';

import { Variables } from '../../../dependency/global/Variable';

// Function name
export const name = 'push';

/**
 * Run the function
 * Push the value in an array within the global variables
 * @param {object} parameters
 *                 - var {string} - The name of the array. (R)
 *                 - value {any} - The value to be pushed in the array. (O)
 * 
 * @return {array}
 */
export default run = (parameters) => {
    // Check if variable name is passed and it is a string
    if (!parameters.var || typeof parameters.var !== 'string') {
        errorLog('Source - Framework - Function - Array - Push - run: ' + 'The parameter var is required and must be a string.');
        return DefaultValueArray;
    }

    // Check if the array is existing and is an array in the global variable
    if (!Variables[parameters.var] || !Array.isArray(Variables[parameters.var])) {
        errorLog('Source - Framework - Function - Array - Push - run: ' + 'The array variable name does not exist or is not an array.');
        return DefaultValueArray;
    }

    // Generate the new array inside the global variables
    Variables[parameters.var].push(parameters.value);

    return Variables[parameters.var];
}