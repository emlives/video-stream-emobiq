import { errorLog } from '../../core/Log';
import { DefaultValueArray } from '../../core/Constant';

// Function name
export const name = 'sort';

/**
 * Run the function
 * Sort the array passed
 * @param {object} parameters
 *                 - values {array} - The array to be sorted. (R)
 * 
 * @return {array}
 */
export default run = (parameters) => {
    // Check if values is passed and in array type
    if (!parameters.values || !Array.isArray(parameters.values)) {
        errorLog('Source - Framework - Function - Array - Sort - run: The parameter values is required and must be an array.');
        return DefaultValueArray;
    }

    // Sort the array (descending order)
    let result = parameters.values.sort(
        function(a, b) {
            return a < b;
        }
    );

    return result;
}