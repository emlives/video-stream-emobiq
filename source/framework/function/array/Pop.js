import { errorLog } from '../../core/Log';
import { DefaultValueArray } from '../../core/Constant';

import { Variables } from '../../../dependency/global/Variable';

// Function name
export const name = 'pop';

/**
 * Run the function
 * Do a pop function for the global variable specified
 * which must be an array and it will remove the last element.
 * @param {object} parameters
 *                 - variableName {string} - The name of the object/array. (R)
 *                 - key {string} - The key of the value. (R)
 *                 - value {any} - The value to be pushed in the object/array. (R)
 * 
 * @return {array}
 */
export default run = (parameters) => {
    // Check if variable name is passed and it is a string
    if (!parameters.variableName || typeof variableName.variableName !== 'string') {
        errorLog('Source - Framework - Function - Array - Pop - run: ' + 'The parameter variableName is required and must be a string.');
        return DefaultValueArray;
    }

    // Check if the array is existing and is an array in the global variable
    if (!Variables[parameters.variableName] || !Array.isArray(Variables[parameters.variableName])) {
        errorLog('Source - Framework - Function - Array - Pop- run: ' + 'The array variable name does not exist or is not an array.');
        return DefaultValueArray;
    }

    // Do the pop function on the array from global variables
    Variables[parameters.variableName].pop();

    return Variables[parameters.variableName];
}