import { Variables } from '../../../dependency/global/Variable';

// Function name
export const name = 'unshift';

/**
 * Run the function
 * Add a new element in the first position of an array.
 * @param {object} parameters
 *                 - variableName {string} - The variable name of the array. (C)
 *                 - data {any} - The data to be used if no variable name passed. (C)
 *                 - value {any} - The value to be pushed in the array. (O)
 * 
 * @return {array}
 */
export default run = (parameters) => {
    // Check if name of variable is passed
    if (parameters.variableName || typeof parameters.variableName === 'string') {
        // Check if the variable exists
        if (Variables[parameters.variableName] || Array.isArray(Variables[parameters.variableName])){
            Variables[parameters.variableName].unshift(parameters.value);
            return Variables[parameters.variableName];
        }   
    }

    // If not a global variable use the data
    var data = parameters.data;
    if (!Array.isArray(data)) {
        data = [data];
    }
    data.unshift(parameters.value);
    return data;
}