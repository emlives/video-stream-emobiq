import { errorLog } from '../../core/Log';
import { DefaultValueNumber } from '../../core/Constant';

// Function name
export const name = 'count';

/**
 * Run the function
 * Count the content of an array
 * @param {object} parameters
 *                 - values {array} - The array to be counted. (R)
 * 
 * @return {float}
 */
export default run = (parameters) => {
    // Check if values is passed and in array type
    if (!parameters.values || !Array.isArray(parameters.values)) {
        errorLog('Source - Framework - Function - Array - Count - run: The parameter values is required and must be an array.');
        
        // To support legacy count function, returned 0 instead of DefaultValueNumber (of -1)
        return 0;
    }

    return parameters.values.length;
}