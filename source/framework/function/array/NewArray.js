import { errorLog } from '../../core/Log';
import { DefaultValueArray } from '../../core/Constant';

import { Variables } from '../../../dependency/global/Variable';

// Function name
export const name = 'newArray';

/**
 * Run the function
 * Create a named new array in the global variables
 * @param {object} parameters
 *                 - variableName {string} - The name to be assigned for the array. (R)
 * 
 * @return {array}
 */
export default run = (parameters) => {
    // Check if variable name is passed and it is a string
    if (!parameters.var || typeof parameters.var !== 'string') {
        errorLog('Source - Framework - Function - Array - NewArray - run: ' + 'The parameter var is required and must be a string.');
        return DefaultValueArray
    }

    // Generate the new array inside the global variables
    Variables[parameters.var] = [];

    return Variables[parameters.var];
}