import { errorLog } from '../../core/Log';

import { Variables } from '../../../dependency/global/Variable';

// Function name
export const name = 'findArrayIndex';

/**
 * Run the function
 * Find if the value exist within the array in the global variables
 * @param {object} parameters
 *                 - variableName {string} - The name of the array. (R)
 *                 - value {any} - The name of the array. (O)
 * 
 * @return {boolean}
 */
export default run = (parameters) => {
    // Check if variable name is passed and it is a string
    if (!parameters.variableName || typeof parameters.variableName !== 'string') {
        errorLog('Source - Framework - Function - Array - FindArrayIndex - run: ' + 'The parameter variableName is required and must be a string.');
        return false;
    }

    // Check if the array is existing and is an array in the global variable
    if (!Variables[parameters.variableName] || !Array.isArray(Variables[parameters.variableName])) {
        errorLog('Source - Framework - Function - Array - FindArrayIndex - run: ' + 'The array variable name does not exist or is not an array.');
        return false;
    }

    // Check if the value exist in the array
    if (Variables[parameters.variableName].indexOf(parameters.value) === -1) {
        return false;
    }

    // It passed everything means it exists
    return true;
}