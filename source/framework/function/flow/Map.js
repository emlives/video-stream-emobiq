import { errorLog } from '../../core/Log';

// Function name
export const name = "map";

/**
 * A function to be run for each element in the array
 * 
 * @param {array} parameters.values - The latitude of first point
 * @param {any} parameters.extra - The extra parameters
 * @param {function} parameters.callback - The function
 * Note: This doesn't return result to support older version (legacy).
 * @return {boolean} - An Array containing the results of calling the provided function for each element in the original array
 */
export default run = (parameters) => {
    let values = parameters.values
    let extra = parameters.extra
    let callback = parameters.callback

    if (!Array.isArray(values)) {
        errorLog('Source - Framework - Function - Flow - Map - run: ' + 'The parameters.values is null');
        return false
    }

    if (typeof callback !== 'function') {
        errorLog('Source - Framework - Function - Flow - Map - run: ' + 'The parameters.callback is not a function');
        return false
    }
    var i = 0;
    for (i = 0; i < values.length; i++) {
        callback(values[i], extra);
    }
    return values
}