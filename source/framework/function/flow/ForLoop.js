import { errorLog } from '../../core/Log';
import {isNumber} from '../../core/Helper'
import { result } from '../../core/Format';

// Function name
export const name = "forLoop";

/**
 * A function to be run for each element in the array for the specified number of times
 * 
 * @param {array} parameters.start - The start index of loop
 * @param {any} parameters.end - The end index of loop
 * @param {function} parameters.callback - The function
 * @return {boolean} - Returns a boolean status of process
 */
export default run = (parameters) => {
    let start = parameters.start
    let end = parameters.end
    let callback = parameters.callback

    if (typeof callback !== 'function') {
        errorLog('Source - Framework - Function - Flow - ForLoop - run: ' + 'The parameter callback is null or not a function');
        return result(false, null, 'The parameter callback is null or not a function');
    }

    if (!isNumber(start) || !isNumber(end)) {
        errorLog('Source - Framework - Function - Flow - ForLoop - run: ' + 'The parameters start or end is null or NaN');
        return result(false, null, 'The parameters start or end is null or NaN');
    }

    for (var i = start; i <= end; i++) { 
        callback(i)
    }

    return result(true, null, 'Successfully completed the forLoop operation');
}