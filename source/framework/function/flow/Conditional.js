import { errorLog } from '../../core/Log';
import { callback } from '../../core/Helper';

// Function name
export const name = 'conditional';

/**
 * Construct condition to invoke callback
 * 
 * @param {array} parameters.condition - The condition to invoke callback
 * @param {function} parameters.extra - Extra parameters for callback
 * @param {function} parameters.yesCallback - The callback if condition is valid
 * @param {any} parameters.yesValue - - The return value if condition is valid
 * @param {function} parameters.noCallback - The callback if condition is invalid
 * @param {function} parameters.noValue - The return value if condition is invalid
 * @return {boolean} - Returns a boolean status of process
 */
export default run = (parameters) => {
    let condition = parameters.condition;
    let extra = parameters.extra;
    let yesCallback = parameters.yesCallback;
    let yesValue = parameters.yesValue || '';
    let noCallback = parameters.noCallback;
    let noValue = parameters.noValue || '';

    if (condition && typeof condition != 'undefined') {
        callback(yesCallback, null, extra);
        return yesValue;
    } else {
        callback(noCallback, null, extra);
        return noValue;
    }
}