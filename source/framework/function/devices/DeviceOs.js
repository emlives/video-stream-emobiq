import { Platform } from 'react-native'

// Function name
export const name = "deviceOS";

/**
 * Return the OS of the phone
 */
export default run = () => {
    return Platform.OS;
}