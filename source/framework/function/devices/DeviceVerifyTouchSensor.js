import { authenticate } from '../../plugin/LocalAuthentication';

import { errorLog } from '../../core/Log';
import { callback } from '../../core/Helper';
import { result } from '../../core/Format';

// Function name
export const name = 'deviceVerifyTouchSensor';

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Function - Device - DeviceVerifyTouchSensor - ';

/**
 * Authenticate the user
 * @param {object} parameters
 *                 - key {function} - A message that is shown alongside the TouchID or FaceID prompt. (O)
 *                 - message {function} - Allows to customize the default Use Passcode label shown after several failed authentication attempts. (O)
 *                 - callback {function} - Triggers if it is successful. (O)
 * 
* @return return 
 */
export default run = (parameters) => {
    // Prepare the initial variable to be used
    let logFunctionName = 'run: ';

    // Check if need to generate additional options
    let options = {}
    if (parameters.key && parameters.message) {
        options = {
            promptMessage: parameters.key,
            cancelLabel: "Cancel",
            fallbackLabel: parameters.message,
            disableDeviceFallback: false    
        }
    }

    // Prepare the callback for the function
    let successCallback = (data) => {
        callback(parameters.callback, data);
    }
    let errorCallback = (error) => {
        errorLog(logInformation + logFunctionName + error);
        callback(parameters.errorCallback, error);
    }

    // Trigger the local authentication
    authenticate(options, successCallback, errorCallback);

    return result(true, '', 'The local authentication is triggered, validate it through the callbacks.');
}