// Open Camera in the device
export const name = 'captureImage';

/**
 * Capture image using camera device
 * @param {object} navigation - The navigation taken from react-navigation
 * 
 * @return {result} - {object}
 */
export default run = (navigation) => {
    navigation.navigate('CameraPlugin');
}