import { BackHandler } from 'react-native';

// Function Name
export const name = "exitApp"

/**
 * Run the function
 * Exits the app (only works for android)
 */

export default run = () => {
    BackHandler.exitApp();
}