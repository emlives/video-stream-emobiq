import DeviceInfo from 'react-native-device-info';
import { errorLog } from '../../core/Log';
import { DefaultValueString } from '../../core/Constant'

// Function name
export const name = "deviceId";

 /**
  * Returns the device id
  * @return {string} - Returns device id
  * 
  */
export default run = (parameters) => {
  const deviceID = DeviceInfo.getDeviceId();

  if (!deviceID) {
    errorLog('Source - Framework - Function - Devices - DeviceID- run: ' + 'Unable to retrieve device id'); 
    return DefaultValueString
  }

  return deviceID;
}