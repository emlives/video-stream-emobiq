import { DeviceData } from "../../../dependency/global/Variable";

// Function name
export const name = 'deviceManufacturer';

/**
 * Return the manufacturer of the phone
 * 
 * @return {promise} - {string}
 */
export default run = () => {
    return DeviceData.manufacturer;
}