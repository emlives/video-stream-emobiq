import { DeviceData } from "../../../dependency/global/Variable";

// Function name
export const name = 'deviceSerial';

/**
 * Return the serial of the phone
 * 
 * @return {promise} - {string}
 */
export default run = () => {
    return DeviceData.serialNumber;
}