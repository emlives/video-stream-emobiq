import DeviceInfo from 'react-native-device-info';
import { errorLog } from '../../core/Log';
import { DefaultValueString } from '../../core/Constant'

// Function name
export const name = "deviceName";

 /**
  * Returns the device name
  * @return {string} - Returns device name
  */
export default run = (parameters) => {
    const deviceName = DeviceInfo.getDeviceName();

    if (!deviceName) {
      errorLog('Source - Framework - Function - Devices - DeviceName - run: ' + 'Unable to retrieve device name'); 
      return DefaultValueString
    }

    return deviceName;
}