import DeviceInfo from 'react-native-device-info';

// Function name
export const name = "deviceOSVersion";

/**
 * Return the version of the phone
 */
export default run = () => {
    return DeviceInfo.getSystemVersion();
}