import { BackHandler } from 'react-native';
import { Data } from '../../../dependency/global/Variable';
import { errorLog } from '../../core/Log';
import { result } from '../../core/Format';

// Function name
export const name = 'onBackButton';

/**
 * Run the function
 * Register the function to be triggered upon pressing back button 
 * for android devices only. 
 * @param {object} parameters
 *                 - callback {function} - The function to register for the back button listener. (O)
 * 
 * @return {result} - {obj}
 */
export default run = (parameters) => {
    // Check if old callback exists then clean it up from the listener
    // Added this function to make it cleaner
    if (Data.onBackButtonAction) {
        BackHandler.removeEventListener('hardwareBackPress', Data.onBackButtonAction);
    }

    // Set the callback based on the parameter
    let callback = parameters.callback;

    // Check if any callback must be called and valid one
    if (!parameters.callback || typeof(parameters.callback) !== 'function') {
        errorLog('Source - Framework - Function - Devices - OnBackButton - run: ' + 'The parameter callback is not passed or is not a valid function.');
        callback = null;
    }

    // Store the callback to the global data
    // Done to clear up when new listener is added
    Data.onBackButtonAction = function() {
        // Make sure there is a callback 
        if (callback) {
            callback();
        }

        // When true is returned the event will not be bubbled up
        // & no other back action will execute
        return true;
    }

    // Add the event listener
    BackHandler.addEventListener('hardwareBackPress', Data.onBackButtonAction);

    return result(true, '', 'The listener for the back button was updated.');
}