import { hasAuthentication } from '../../plugin/LocalAuthentication';

import { callback } from '../../core/Helper';
import { result } from '../../core/Format';

// Function name
export const name = 'deviceHasTouchSensor';

/**
 * Check if device has local authentication and enabled
 * @param {object} parameters
 *                 - callback {function} - Triggers if it is successful. (O)
 *                 - errorCallback {function} - Triggers if it failed. (O)
 * 
* @return return 
 */
export default run = (parameters) => {
    // Prepare the callback for the function
    let successCallback = (data) => {
        callback(parameters.callback, data);
    }
    let errorCallback = (error) => {
        callback(parameters.errorCallback, error);
    }

    // Check if has local authentication
    hasAuthentication(successCallback, errorCallback);

    return result(true, '', 'The request is being processed, validate it through the callbacks.');
}