import { Keyboard } from 'react-native'; 

// Function name
export const name = "keyboardHide";

 /**
  * Hides the keyboard
  */
export default run = (parameters) => {
    Keyboard.dismiss();
}