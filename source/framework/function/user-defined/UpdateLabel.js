// Function name
export const name = "updateLabel";

/**
 * Run the function
 * @param {object} functions
 * @param {object} parameters
 */
export default run = (functions, parameters) => {
    functions.setComponentValue({component: parameters.componentName, value: "Updated through user defined function!!!"});
    return "Test"; // Supposedly getVar later... :) - See eMOBIQ flow
}