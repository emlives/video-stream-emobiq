// Function name
export const name = "globalModalQuestion";

/**
 * Run the function
 * @param {object} functions
 * @param {object} parameters
 */
export default run = (functions, parameters) => {
	functions.toArray({
		"value1": functions.setComponentValue({
			"component": "lblModalTitle",
			"value": "Loading"
		}),
		"value2": functions.hideElement({
			"component": "pnlModalBodyTitle"
		}),
		"value3": functions.setComponentValue({
			"component": "lblModalMessage",
			"value": "Loading ..."
		}),
		"value4": functions.hideElement({
			"component": "pnlModalBodyMessage"
		}),
		"value5": functions.hideElement({
			"component": "pnlModalBodyLoading"
		}),
		"value6": functions.setComponentValue({
			"component": "lblModalPositive",
			"value": "POSITIVE"
		}),
		"value7": functions.setObjectAttr({
			"object": null,
			"attr": "borderRight",
			"value": ""
		}),
		"value8": functions.hideElement({
			"component": "pnlModalBodyButtonPositive"
		}),
		"value9": functions.setComponentValue({
			"component": "lblModalNegative",
			"value": "NEGATIVE"
		}),
		"value10": functions.toArray({
			"value1": functions.hideElement({
				"component": "pnlModalBodyButtonNegative"
			}),
			"value2": functions.hideElement({
				"component": "pnlModalBodyButtons"
			}),
			"value3": functions.setObjectAttr({
				"object": null,
				"attr": "word-wrap",
				"value": "break-word"
			}),
			"value4": ""
		})
	});
	functions.toArray({
		"value1": functions.setComponentValue({
			"component": "lblModalTitle",
			"value": parameters.title
		}),
		"value2": functions.showElement({
			"component": "pnlModalBodyTitle"
		}),
		"value3": functions.setComponentValue({
			"component": "lblModalMessage",
			"value": parameters.message
		}),
		"value4": functions.showElement({
			"component": "pnlModalBodyMessage"
		}),
		"value5": functions.hideElement({
			"component": "pnlModalBodyLoading"
		}),
		"value6": functions.setComponentValue({
			"component": "lblModalPositive",
			"value": "Yes"
		}),
		"value7": functions.showElement({
			"component": "pnlModalBodyButtonPositive"
		}),
		"value8": functions.setComponentValue({
			"component": "lblModalNegative",
			"value": "No"
		}),
		"value9": functions.showElement({
			"component": "pnlModalBodyButtonNegative"
		}),
		"value10": functions.toArray({
			"value1": functions.showElement({
				"component": "pnlModalBodyButtons"
			}),
			"value2": functions.setObjectAttr({
				"object": null,
				"attr": "word-wrap",
				"value": "break-word"
			}),
			"value3": functions.showElement({
				"component": "pnlModal"
			})
		})
	});

}