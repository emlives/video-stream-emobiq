// Function name
export const name = "globalModalHide";

/**
 * Run the function
 * @param {object} functions
 * @param {object} parameters
 */
export default run = (functions, parameters) => {
	functions.toArray({
		"value1": functions.setComponentValue({
			"component": "lblModalTitle",
			"value": "Loading"
		}),
		"value2": functions.hideElement({
			"component": "pnlModalBodyTitle"
		}),
		"value3": functions.setComponentValue({
			"component": "lblModalMessage",
			"value": "Loading ..."
		}),
		"value4": functions.hideElement({
			"component": "pnlModalBodyMessage"
		}),
		"value5": functions.hideElement({
			"component": "pnlModalBodyLoading"
		}),
		"value6": functions.setComponentValue({
			"component": "lblModalPositive",
			"value": "POSITIVE"
		}),
		"value7": functions.setObjectAttr({
			"object": null,
			"attr": "borderRight",
			"value": ""
		}),
		"value8": functions.hideElement({
			"component": "pnlModalBodyButtonPositive"
		}),
		"value9": functions.setComponentValue({
			"component": "lblModalNegative",
			"value": "NEGATIVE"
		}),
		"value10": functions.toArray({
			"value1": functions.hideElement({
				"component": "pnlModalBodyButtonNegative"
			}),
			"value2": functions.hideElement({
				"component": "pnlModalBodyButtons"
			}),
			"value3": functions.setObjectAttr({
				"object": null,
				"attr": "word-wrap",
				"value": "break-word"
			}),
			"value4": functions.hideElement({
				"component": "pnlModal"
			})
		})
	});

}