// Function name
export const name = "globalLogout";

/**
 * Run the function
 * @param {object} functions
 * @param {object} parameters
 */
export default run = (functions, parameters) => {
	functions.setVar({
		"var": "vAction",
		"value": "logout"
	});
	functions.toArray({
		"value1": functions.setComponentValue({
			"component": "lblModalTitle",
			"value": "Logout"
		}),
		"value2": functions.showElement({
			"component": "pnlModalBodyTitle"
		}),
		"value3": functions.setComponentValue({
			"component": "lblModalMessage",
			"value": "Are you sure you want to logout?"
		}),
		"value4": functions.showElement({
			"component": "pnlModalBodyMessage"
		}),
		"value5": functions.setComponentValue({
			"component": "lblModalPositive",
			"value": "OK"
		}),
		"value6": functions.showElement({
			"component": "pnlModalBodyButtonPositive"
		}),
		"value7": functions.setComponentValue({
			"component": "lblModalNegative",
			"value": "CANCEL"
		}),
		"value8": functions.showElement({
			"component": "pnlModalBodyButtonNegative"
		}),
		"value9": functions.showElement({
			"component": "pnlModalBodyButtons"
		}),
		"value10": functions.showElement({
			"component": "pnlModal"
		})
	});

}