// Function name
export const name = "globalCalculateTotal";

/**
 * Run the function
 * @param {object} functions
 * @param {object} parameters
 */
export default run = (functions, parameters) => {
	functions.setVar({
		"var": "vTotalEx",
		"value": functions.sub({
			"value1": parameters.subTotal,
			"value2": parameters.emptyTotal
		})
	});
	functions.console({
		"value": functions.getVar({
			"var": "vTotalEx"
		})
	});
	functions.setVar({
		"var": "vTotalGst",
		"value": functions.multi({
			"value1": functions.sub({
				"value2": parameters.emptyTotal,
				"value1": parameters.subTotal
			}),
			"value2": functions.div({
				"value1": parameters.gstRate,
				"value2": 100
			})
		})
	});
	functions.console({
		"value": functions.getVar({
			"var": "vTotalGst"
		})
	});
	functions.setVar({
		"var": "vTotalAmount",
		"value": functions.add({
			"value2": functions.getVar({
				"var": "vTotalGst"
			}),
			"value1": functions.getVar({
				"var": "vTotalEx"
			})
		})
	});
	functions.console({
		"value": functions.getVar({
			"var": "vTotalAmount"
		})
	});
	functions.toArray({
		"value1": functions.setComponentValue({
			"component": "lblDataSubtotal",
			"value": functions.concat({
				"string1": "$ ",
				"string2": functions.formatNumber({
					"value": parameters.subTotal,
					"decimals": 2,
					"decimalSep": ".",
					"thousandSep": ","
				})
			})
		}),
		"value2": functions.setComponentValue({
			"component": "lblDataEmptyBottleAmount",
			"value": functions.concat({
				"string1": "$ ",
				"string2": functions.formatNumber({
					"value": parameters.emptyTotal,
					"decimals": 2,
					"decimalSep": ".",
					"thousandSep": ","
				})
			})
		}),
		"value3": functions.setComponentValue({
			"component": "lblDataTotalAmountExclGST",
			"value": functions.concat({
				"string1": "$ ",
				"string2": functions.formatNumber({
					"value": functions.getVar({
						"var": "vTotalEx"
					}),
					"decimals": 2,
					"decimalSep": ".",
					"thousandSep": ","
				})
			})
		}),
		"value4": functions.setComponentValue({
			"component": "lblDataGSTAmount",
			"value": functions.concat({
				"string1": "$ ",
				"string2": functions.formatNumber({
					"value": functions.getVar({
						"var": "vTotalGst"
					}),
					"decimals": 2,
					"decimalSep": ".",
					"thousandSep": ","
				})
			})
		}),
		"value5": functions.setComponentValue({
			"component": "lblDataTotalAmount",
			"value": functions.concat({
				"string1": "$ ",
				"string2": functions.formatNumber({
					"decimals": 2,
					"decimalSep": ".",
					"thousandSep": ",",
					"value": functions.getVar({
						"var": "vTotalAmount"
					})
				})
			})
		}),
		"value6": "",
		"value7": "",
		"value8": "",
		"value9": ""
	});

}