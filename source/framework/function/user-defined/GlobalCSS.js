// Function name
export const name = "globalCSS";

/**
 * Run the function
 * @param {object} functions
 * @param {object} parameters
 */
export default run = (functions, parameters) => {
	functions.setVar({
		"var": "vResult",
		"value": "<style> *{ font-family: calibri !important; } label { line-height: 1.2; } .line-space-1 { line-height: 1; } .line-space-0 { line-height: 0; } .ellipsis { white-space: nowrap; text-overflow: ellipsis; } .nowrap { white-space: nowrap; } </style>"
	});

}