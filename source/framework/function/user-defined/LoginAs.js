// Function name
export const name = "LoginAs";

/**
 * Run the function
 * @param {object} functions
 * @param {object} parameters
 */
export default run = (functions, parameters) => {
	functions.setComponentValue({
		"component": "lblLoginAs",
		"value": functions.conditional({
			"condition": functions.getVar({
				"var": "vUsername",
				"default": ""
			}),
			"yesValue": functions.getVar({
				"var": "vUsername",
				"default": ""
			}),
			"noValue": "Unknown"
		})
	});

}