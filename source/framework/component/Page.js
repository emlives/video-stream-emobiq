import React, { useEffect } from 'react';
import { View, ScrollView, Dimensions } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

import { usePanResponder } from '../hook/PanResponder';
import { hideLog } from '../hook/HideLog';

import { getPropertyValue, generateComponentWithInheritedStyle } from '../core/Component';
import { getStyle } from '../core/Style';
import { useStyle } from '../hook/Style';
import { filterInheritableStyles } from '../core/Style';
import { initContentHeight } from '../core/Style';

const Page = (props) => {
    // Styling css
    const pageStyle = getStyle('page', props, "View");
    const inheritedStyle = useStyle(filterInheritableStyles(pageStyle));

    // Check if to display the component
    const isHidden = pageStyle?.display == 'none';
    const isVisible = getPropertyValue('componentVisible', props, true);
    if (!isVisible || isHidden) {
        return null;
    }

    // Triggers load event
    useEffect(() => {
        // Check if there's load event
        if (typeof props.load === 'function') {
            props.load();
        }
        if (typeof props.initializePage === 'function') {
            props.initializePage({
            });
        }
    }, [props.currentTime]);

    // Properties of the component
    const componentIsDisabled = getPropertyValue('componentDisabled', props);
    const pointerEventsValue = (componentIsDisabled) ? 'none' : 'auto';

    // Get the custom hook to create the pan responder
    const panResponder = usePanResponder();

    // Hide React-Native's warning boxes
    hideLog();

    // Pass inheritable styles to all children
    const children = generateComponentWithInheritedStyle(props.children, inheritedStyle);

    // Get View and ScrollView geight
    let scrollableHeight = 0;
    const windowHeight = Dimensions.get('window').height;
    if (props.allProps) {
        scrollableHeight = initContentHeight(props.allProps, windowHeight);
    }

    return (
        <SafeAreaView style={pageStyle} {...panResponder.panHandlers}>
            <ScrollView
                pointerEvents={pointerEventsValue}
                contentContainerStyle={{ flexGrow: 1, height: scrollableHeight }}
                nestedScrollEnabled={true}>                
                    {children}
            </ScrollView>
        </SafeAreaView>
    );
}

export default React.memo(Page);
