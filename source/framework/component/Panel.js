import React from 'react';
import { ScrollView, TouchableOpacity, View } from 'react-native';

import { getPropertyValue, generateComponentWithInheritedStyle } from '../core/Component';
import { getStyle } from '../core/Style';
import { filterInheritableStyles } from '../core/Style'
import { useStyle } from '../hook/Style';

function ScrollabelPanel(props) {
    return <TouchableOpacity 
        style={props.style}
        disabled={props.disableTouch}
        onPress={props.action}>
        <ScrollView 
            style={props.scrollViewContainerstyle}
            contentContainerStyle={props.scrollViewContentStyle}
            nestedScrollEnabled={true}>     
                {props.children}
        </ScrollView>
    </TouchableOpacity>;
}

function NonScrollabelPanel(props) {
    return <TouchableOpacity 
        style={props.style}
        disabled={props.disableTouch}
        onPress={props.action}>
        <View style={props.contentStyle}>
            {props.children}
        </View>
    </TouchableOpacity>;
}

const Panel = (props) => {
    
    // For css styling
    let panelStyle = getStyle('panel', props);
    let inheritedStyle = useStyle(filterInheritableStyles(panelStyle.original));
    
    // Pass inheritable styles to all children
    const children = generateComponentWithInheritedStyle(props.children, inheritedStyle);
    
    // Check if it is touchable
    const disableTouch = (props.action) ? false : true;

     // Identifies whether to display the component
     const isHidden = panelStyle?.style?.display == "none";
     const isVisible = getPropertyValue('componentVisible', props, true);
     if (!isVisible || isHidden) {
         return null;
     }

    // Refector formating logic into a function to return multiple values 
    // (i.e. styling for ScrollView's style and contentContainerStyle) using ES6 array-destructuring
    // note: not a pure function, cos it introduces side-effects
     const formatPanelLayout = (panelStyle) => {
        let scrollViewContentStyle = {} 
        let scrollViewContainerstyle = {}

         // only when overflow != scroll, we restrict the scrollview's style with a maxHeight = input-height
         if (panelStyle.height) {
            if (panelStyle.overflow && panelStyle.overflow != 'scroll') {
                scrollViewContainerstyle.maxHeight = panelStyle.height
            }
        }

        if(panelStyle) {
            if(panelStyle.justifyContent){
                scrollViewContentStyle.justifyContent = panelStyle.justifyContent
                delete panelStyle.justifyContent // side-effect
            }

            if(panelStyle.alignItems){
                scrollViewContentStyle.alignItems = panelStyle.alignItems
                delete panelStyle.alignItems // side-effect
            }

            if(panelStyle.flexDirection){
                scrollViewContentStyle.flexDirection = panelStyle.flexDirection
                delete panelStyle.flexDirection // side-effect
            }

            if(panelStyle.flexGrow){
                scrollViewContentStyle.flexGrow = panelStyle.flexGrow
                // delete panelStyle.style.flexGrow
            }else {
                scrollViewContentStyle.flexGrow = 1
            }

            if(typeof panelStyle.flexShrink == "undefined"){
                panelStyle.flexShrink = 1
            }
        }
        return [scrollViewContentStyle, scrollViewContainerstyle]
    }
    
    const [scrollViewContentStyle, scrollViewContainerstyle] = formatPanelLayout(panelStyle.style)
    const panelOverflowValue = panelStyle.style.overflow

    // flatlist's onEndReached property will not trigger if flatlist is contained in a scrollview
    // so use conditonal-rendering to toggle between using scrollview/view
    if(panelOverflowValue && panelOverflowValue === "scroll"){
         return <ScrollabelPanel
                    style={panelStyle.style}
                    disableTouch={disableTouch}
                    action={props.action}
                    scrollViewContainerstyle={scrollViewContainerstyle}
                    scrollViewContentStyle={scrollViewContentStyle}
                    children={children}
                />
    }
   
    return <NonScrollabelPanel
                style={panelStyle.style}
                disableTouch={disableTouch}
                action={props.action}
                contentStyle={scrollViewContentStyle}
                children={children}
            />

}

export default React.memo(Panel);