import React from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';
import { LIVE_STATUS } from '../../../dependency/stream/utils/constants';
import * as Utility from '../../../dependency/stream/utils/utility';


const LiveStreamActionButton = ({ currentLiveStatus, onPress }) => {
  let backgroundColor = '#9b59b6';
  let text = 'Start live video';
  if (Number(currentLiveStatus) === Number(LIVE_STATUS.ON_LIVE)) {
    backgroundColor = '#e74c3c';
    text = 'Stop live stream';
  }
  return (
    <TouchableOpacity onPress={onPress} style={[styles.btnBeginLiveStream, { backgroundColor }]}>
      <Text style={styles.beginLiveStreamText}>{text}</Text>
    </TouchableOpacity>
  );
};

LiveStreamActionButton.propTypes = {
  navigation: PropTypes.shape({
    goBack: PropTypes.func,
  }),
  currentLiveStatus: PropTypes.number,
  onPress: PropTypes.func,
};

LiveStreamActionButton.defaultProps = {
  navigation: {
    goBack: () => null,
  },
  currentLiveStatus: LIVE_STATUS.PREPARE,
  onPress: () => null,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3498db',
  },
  contentWrapper: { flex: 1 },
  header: { flex: 0.1, justifyContent: 'space-around', flexDirection: 'row' },
  footer: { flex: 0.1 },
  center: { flex: 0.8 },
  streamerView: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: Utility.screenHeight,
    width: Utility.screenWidth,
  },
  btnClose: { position: 'absolute', top: 15, left: 15 },
  icoClose: { width: 28, height: 28 },
  bottomGroup: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
  },
  btnBeginLiveStream: {
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 30,
    paddingVertical: 5,
  },
  beginLiveStreamText: {
    fontSize: 20,
    fontWeight: '600',
    color: 'white',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
});

export default LiveStreamActionButton;
