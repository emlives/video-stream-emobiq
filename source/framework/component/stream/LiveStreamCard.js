import React from 'react';
import PropTypes from 'prop-types';
import { Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import get from 'lodash/get';
import { LIVE_STATUS } from '../../../dependency/stream/utils/constants';

const styles = StyleSheet.create({
  card: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 15,
    paddingHorizontal: 25,
    borderRadius: 1,
    marginBottom: 15,
  },
  roomName: {
    fontWeight: '600',
    fontSize: 17,
  },
  liveStatus: {
    fontSize: 20,
    fontWeight: '600',
    marginTop: 5,
  },
  statusIcon: {
    width: 25,
    height: 25,
  },
  onLiveIcon: {
    width: 35,
    height: 35,
  },
});

const LiveStreamCard = ({ data, onPress }) => {
  const roomName = get(data, 'roomName', '');
  const liveStatus = get(data, 'liveStatus', LIVE_STATUS.PREPARE);
  let statusIcon = null;
  switch (liveStatus) {
    case LIVE_STATUS.PREPARE:
      statusIcon = (
        <Image source={require(`../../../asset/public/stream/icons/ico_wait.png`)} style={styles.statusIcon} />
      );
      break;
    case LIVE_STATUS.ON_LIVE:
      statusIcon = (
        <Image source={require(`../../../asset/public/stream/icons/ico_live.png`)} style={styles.onLiveIcon} />
      );
      break;
    case LIVE_STATUS.FINISH:
      statusIcon = (
        <Image source={require(`../../../asset/public/stream/icons/ico_replay.png`)} style={styles.statusIcon} />
      );
      break;
    default:
      statusIcon = (
        <Image source={require(`../../../asset/public/stream/icons/ico_wait.png`)} style={styles.statusIcon} />
      );
      break;
  }
  return (
    <TouchableOpacity style={styles.card} onPress={() => onPress(data)}>
      <Text style={styles.roomName}>{roomName}</Text>
      {statusIcon}
    </TouchableOpacity>
  );
};

LiveStreamCard.propTypes = {
  data: PropTypes.shape({}),
  onPress: PropTypes.func,
};

LiveStreamCard.defaultProps = {
  data: null,
  onPress: () => null,
};
export default LiveStreamCard;
