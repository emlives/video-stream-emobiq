import React from 'react';
import ReactNativeCheckBox from '@react-native-community/checkbox';
import { View } from 'react-native'
import { getPropertyValue } from '../core/Component';
import { getStyle } from '../core/Style';
import { useInitialValue } from '../hook/InitialValue';

const CheckBox = (props) => {
    const isVisible = getPropertyValue('componentVisible', props, true);
    if (!isVisible) {
        return null;
    }

    // Generate the initial value only once
    useInitialValue(props.value, getPropertyValue('componentUUID', props), props.functions, props.currentTime);

    return (
        <View style={getStyle('checkBox', props).view.style}>
            <ReactNativeCheckBox
                value={getPropertyValue('componentValue', props)}
                onValueChange={(itemValue) =>
                    props.functions.setComponentValue({ name: getPropertyValue('componentName', props), value: itemValue })
                }
                tintColors={{ true: '#2199e8' }}
                boxType="square"
            >
            </ReactNativeCheckBox>
        </View>
    );
}

export default React.memo(CheckBox);