import React from 'react';
import ReactNativeCheckBox from '@react-native-community/checkbox';
import { getStyle } from '../core/Style';
import { getPropertyValue } from '../core/Component';

const CheckBox = (props) => {
    const style = getStyle('checkbox', props);
    
    const isHidden = style?.display == 'none';
    const isVisible = getPropertyValue('componentVisible', props, true);
    if (!isVisible || isHidden) {
        return null;
    }

    // Prepare reusable variables
    let componentUUID = getPropertyValue('componentUUID', props);
    
    let value = props.value
    if( typeof props.value == "function" && typeof props["value_data"] !== "undefined" ) {
        value = props.value(props.value_data)
    }

    // Get the component value
    let componentValue = getPropertyValue('componentValue', props, false);
    componentValue = typeof componentValue == 'boolean' ? componentValue : false;

    return (
        <ReactNativeCheckBox
            value={componentValue}
            onValueChange={(itemValue) => {
                if (props.functions) {
                    props.functions.coreStateSetProperties({
                        uuid: componentUUID,
                        data: {
                            componentValue: itemValue
                        }
                    });
                    props.change?.()
                }
            }}
            tintColors={{ true: '#2199e8' }}
            boxType="square"
        />
    );
}

export default React.memo(CheckBox);