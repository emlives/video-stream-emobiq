import React from 'react';
import { ScrollView, TouchableOpacity } from 'react-native';

import { getPropertyValue, generateComponentWithInheritedStyle } from '../core/Component';
import { getStyle } from '../core/Style';
import { filterInheritableStyles } from '../core/Style'
import { useElementAttribute } from '../hook/ElementAttribute';

const Panel = (props) => {
    // For css styling
    const panelStyle = getStyle('panel', props);
    const inheritedStyle = filterInheritableStyles(panelStyle.unfilteredStyle);
    
    // Identifies whether to display the component
    const isHidden = panelStyle?.scrollView?.style?.display == "none";
    if (isHidden) {
        return null;
    }

    // Prepare reusable variables
    let componentUUID = getPropertyValue('componentUUID', props);

    // Initialize elementAttribute
    useElementAttribute(componentUUID, props.functions, props.currentTime);
    
    // Pass inheritable styles to all children
    const children = generateComponentWithInheritedStyle(props.children, inheritedStyle);
    
    // Check if it is touchable
    const disableTouch = (props.action) ? false : true;
    return (
        <TouchableOpacity 
            disabled={disableTouch}
            style={panelStyle.view.style}
            onPress={props.action}>
            <ScrollView contentContainerStyle={{...panelStyle.scrollView.style/*, minHeight:"100%"*/}}>
                {children}
            </ScrollView>
        </TouchableOpacity>
    );
}

export default React.memo(Panel);