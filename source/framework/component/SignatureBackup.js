import React from 'react';

import { getPropertyValue } from '../core/Component';
import { getStyle } from '../core/Style';
import SignatureCapture from 'react-native-signature-capture';
import { useElementAttribute } from '../hook/ElementAttribute';

const Signature = (props) => {
    // For css styling
    const signatureStyle = getStyle('signature', props);
    
    // Identifies whether to display the component
    const isHidden = signatureStyle?.display == "none";
    if (isHidden) {
        return null;
    }
    
    // Prepare reusable variables
    let componentUUID = getPropertyValue('componentUUID', props);

    // Initialize elementAttribute
    useElementAttribute(componentUUID, props.functions, props.currentTime);

    let _onSaveEvent = (result) => {
        console.log(result)
    }
    
    let _onDragEvent = (result) => {
        console.log(result)
    }
    
    return (
        <SignatureCapture
            style={signatureStyle.signatureCapture.style}
            ref={props.inputRef}
            onSaveEvent={_onSaveEvent}
            onDragEvent={_onDragEvent}
            saveImageFileInExtStorage={false}
            showNativeButtons={false}
            showTitleLabel={false}
            backgroundColor={signatureStyle.signatureCapture.style.backgroundColor ?? 'white'}
            strokeColor="black"
            minStrokeWidth={4}
            maxStrokeWidth={4}
            viewMode={"portrait"}
            showBorder={false}
        />
    )
}

export default React.memo(Signature);