import React from 'react';
import { ScrollView } from 'react-native';

import { getStyle } from '../core/Style';
import { getPropertyValue } from '../core/Component';

const Snippet = (props) => {
    const style = getStyle('snippet', props);

    const isHidden = style?.display == 'none';
    const isVisible = getPropertyValue('componentVisible', props, true);
    if (!isVisible || isHidden) {
        return null;
    }

    return (
        <ScrollView style={style} nestedScrollEnabled={true}>
            {props.children}
        </ScrollView>
    )
}

export default React.memo(Snippet);