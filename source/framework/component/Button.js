import React from 'react';
import { 
    TouchableOpacity, Text 
} from 'react-native';
import { getPropertyValue } from '../core/Component';
import { getStyle } from '../core/Style';
import { translate } from '../core/Language';

const Button = (props) => {
    const buttonStyle = getStyle('button', props);
    
    // Identifies whether to display the component
    const isHidden = buttonStyle?.display == "none";
    const isVisible = getPropertyValue('componentVisible', props, true);
    if (!isVisible || isHidden) {
        return null;
    }
        
    return (
        <TouchableOpacity 
            onPress={props.action}>
            <Text style={buttonStyle}>{translate(getPropertyValue('componentValue', props))}</Text>
        </TouchableOpacity>
    )    
}

export default React.memo(Button);