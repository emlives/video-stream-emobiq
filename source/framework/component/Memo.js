import React from 'react';
import { TextInput, View } from 'react-native';

import { getPropertyValue } from '../core/Component';
import { getStyle } from '../core/Style';

const Memo = (props) => {
    let memoStyle = getStyle('memo', props);

    // Identifies whether to display the component
    const isHidden = memoStyle?.display == "none";
    const isVisible = getPropertyValue('componentVisible', props, true);
    if (!isVisible || isHidden) {
        return null;
    }

    // Prepare reusable variables
    let componentUUID = getPropertyValue('componentUUID', props);
    
    let value = props.value
    if( typeof props.value == "function" && typeof props["value_data"] !== "undefined" ) {
        value = props.value(props.value_data)
    }

    // Update the state value based on the changed text
    const updateStateChangeText = (value) => {
        if (props.functions) {
            props.functions.coreStateSetProperties({
                uuid: componentUUID,
                data: {
                    componentValue: value
                }
            });
        }
    };

    return (
        <TextInput
            defaultValue={getPropertyValue('componentValue', props)}
            placeholder={getPropertyValue('placeHolder', props)}
            onChangeText={updateStateChangeText}
            style={memoStyle}
            editable
            multiline={true}
        />
    )
}

export default React.memo(Memo);