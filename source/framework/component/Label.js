import React from 'react';
import { TouchableOpacity, Text } from 'react-native';

import { getPropertyValue } from '../core/Component';
import { getStyle } from '../core/Style';
import { translate } from '../core/Language';
import { isEmptyString, removeHtmlTags } from '../core/Helper';

const Label = (props) => {
    // Translate the display value
    let displayedValue = translate(getPropertyValue('componentValue', props));

    const style = getStyle('label', props);
    const disableTouch = props.action ? false : true;

    // Identifies whether to display the component
    const isHidden = style?.display == "none";
    const isVisible = getPropertyValue('componentVisible', props, true);
    if (!isVisible || isHidden) {
        return null;
    }

    if (isEmptyString(displayedValue)) {
        return null;
    }

    // displayedValue = removeHtmlTags(displayedValue)
    
    return (
        <TouchableOpacity disabled={disableTouch} onPress={props.action}>
            <Text style={style}>{displayedValue}</Text>
        </TouchableOpacity>
    )
}

export default React.memo(Label);