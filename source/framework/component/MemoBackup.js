import React from 'react';
import { TextInput, View } from 'react-native';

import { getPropertyValue } from '../core/Component';
import { getStyle } from '../core/Style';
import { useInitialValue } from '../hook/InitialValue';
import { useElementAttribute } from '../hook/ElementAttribute';

const Memo = (props) => {
    let memoStyle = getStyle('memo', props);
    // Identifies whether to display the component
    const isHidden = memoStyle?.view?.style?.display == "none";
    if (isHidden) {
        return null;
    }

    // Prepare reusable variables
    let componentUUID = getPropertyValue('componentUUID', props);

    // Initialize elementAttribute
    useElementAttribute(componentUUID, props.functions, props.currentTime);

    // Generate the initial value only once
    useInitialValue(props.value, componentUUID, props.functions, props.currentTime);

    // Update the state value based on the changed text
    const updateStateChangeText = (value) => {
        if (props.functions) {
            props.functions.coreStateSetProperties({
                uuid: componentUUID,
                data: {
                    componentValue: value
                }
            });
        }
    };

    return (
        <View style={memoStyle.view.style}>
            <TextInput
                defaultValue={getPropertyValue('componentValue', props)}
                onChangeText={updateStateChangeText}
                style={memoStyle.textInput.style}
                editable
                multiline={true}
            />
        </View>
    )
}

export default React.memo(Memo);