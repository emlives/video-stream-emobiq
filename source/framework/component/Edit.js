import React, { useEffect, useCallback } from 'react';
import { TextInput } from 'react-native';

import { getPropertyValue } from '../core/Component';
import { getStyle } from '../core/Style';

import { useElement } from '../hook/Element';

import debounce from 'lodash/debounce';

const Edit = (props) => {
    let editStyle = getStyle('edit', props);

    // Identifies whether to display the component
    const isHidden = editStyle?.display == "none";
    const isVisible = getPropertyValue('componentVisible', props, true);
    if (!isVisible || isHidden) {
        return null;
    }

    // Store the element reference
    const element = useElement();

    // Prepare reusable variables
    let componentUUID = getPropertyValue('componentUUID', props);
    
    // The following creates a memoized function that debounced the Edit component's onChange property
    const changeTextAction = (value) => {
        props.changing?.(value)
    }

    const debouncedtTextChangeAction = useCallback(
		debounce(nextValue => changeTextAction(nextValue), 1000),
		[], // will be created only once initially
	);

    const handleChangeText = (text) => {
        debouncedtTextChangeAction(text);
    }

    
    // Update the state value based on the changed text
    const updateStateChangeText = (value) => {
        if (props.functions) {
            props.functions.coreStateSetProperties({
                uuid: componentUUID,
                data: {
                    componentValue: value
                }
            });
            handleChangeText(value)
        }
    }

    // Store some default state for setComponentFocus
    let stateDefaultFocus = {
        componentFocusTrigger: false,
        componentFocusSelectText: false,
        componentFocusShowKeyboard: true
    }

    // Onblur revert to the default state for setComponentFocus
    const resetStateDefaultFocus = () => {
         if (props.functions) {
            props.functions.coreStateSetProperties({
                uuid: componentUUID,
                data: stateDefaultFocus
            });
            props.change?.()
        }
    }

    // Get the focus trigger value from state
    let focusTrigger = getPropertyValue('componentFocusTrigger', props, false);

    useEffect(() => {
        // Focus on the input if needed
        if (element.current && focusTrigger) {
            element.current.focus();
        }
    });

    let defaultValue = getPropertyValue('componentValue', props)
    let editType = getPropertyValue('type', props) 

    // Check if this edit component is a password or to launch a number keyboard
    let secure = false;
    if (editType == 'password') {
        secure = true;
    } 
    else if (editType === 'number') {   
        if (defaultValue !== undefined && defaultValue !== null && !isNaN(defaultValue)) {
             // If TextInput has a numeric keyboard, it's defaultValue is expected to be a string 
            defaultValue = defaultValue.toString()
        }
    }

    return (
        <TextInput
            defaultValue={defaultValue}
            placeholder={getPropertyValue('placeHolder', props)}
            secureTextEntry={secure}
            onChangeText={updateStateChangeText}
            style={editStyle}
            selectionColor='black' // caretColor, there's no caretColor property in emobiq
            selectTextOnFocus={getPropertyValue('componentFocusSelectText', props, false)} // For setComponentFocus - selectAllText
            showSoftInputOnFocus={getPropertyValue('componentFocusShowKeyboard', props, true)} // For setComponentFocus - hideKeyboard
            onBlur={resetStateDefaultFocus} // For setComponentFocus - Reset state
            ref={element}
        />
    )
}

export default React.memo(Edit);