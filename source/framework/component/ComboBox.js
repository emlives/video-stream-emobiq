import React, {useState} from 'react';
import { 
    View
} from 'react-native';
import { getPropertyValue } from '../core/Component';
import { getStyle } from '../core/Style';
import {Platform} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';

const ComboBox = (props) => {
    let comboBoxStyle = getStyle('comboBox', props);
    
    // Identifies whether to display the component
    const isHidden = comboBoxStyle?.display == "none";
    const isVisible = getPropertyValue('componentVisible', props, true);
    if (!isVisible || isHidden) {
        return null;
    }

    // Prepare reusable variables
    let componentUUID = getPropertyValue('componentUUID', props);

    let componentValue = getPropertyValue('componentValue', props);
    let componentOptions = getPropertyValue('componentOptions', props, []);
    const [selection, setSelection] = useState(componentValue); // added

    return (
        <View style={comboBoxStyle}>
            <RNPickerSelect
                // for value change on android
                onValueChange={(itemValue) => {
                    setSelection(itemValue)
                    if(Platform.OS === 'android') {
                        if (props.functions) {
                            props.functions.coreStateSetProperties({
                                uuid: componentUUID,
                                data: {componentValue: selection ? selection : ''}
                            });
                            props.change?.()
                        }
                    }
                }}
                // for ios, change value only when user press done
                onDonePress={() => {
                    if (props.functions) {
                        props.functions.coreStateSetProperties({
                            uuid: componentUUID,
                            data: {componentValue: selection ? selection : ''}
                        });
                        props.change?.()
                    }
                }}
                pickerProps={{
                    mode: "dropdown",
                }}
                items={componentOptions}
                value={selection}
                style={{inputAndroid: { color: 'black' }}}
            />
        </View>
    );
}

export default React.memo(ComboBox);