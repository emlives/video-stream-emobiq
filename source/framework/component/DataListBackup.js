import React, { useEffect } from 'react';
import { FlatList, TouchableOpacity } from 'react-native';

import { getPropertyValue } from '../core/Component';
import { getStyle, filterInheritableStyles } from '../core/Style';
import { useElementAttribute } from '../hook/ElementAttribute';

const DataList = (props) => {
    // For css styling
    const dataListStyle = getStyle('datalist', props, "Text").flatList.style;
    const inheritStyle = getStyle('datalist', props, "Text").unfilteredStyle;
    const parentStyle = filterInheritableStyles(inheritStyle);
    
    // Identifies whether to display the component
    const isHidden = dataListStyle?.display == "none";
    if (isHidden) {
        return null;
    }

    // Prepare reusable variables
    let componentUUID = getPropertyValue('componentUUID', props);

    // Initialize elementAttribute
    useElementAttribute(componentUUID, props.functions, props.currentTime);

    // For autoloading
    useEffect(() => { 
        // Check if autoload is enabled
        if (getPropertyValue('autoLoad', props)) {
            // Get the component value (dataset)
            let componentName = getPropertyValue('componentName', props);
            let componentValue = getPropertyValue('componentValue', props);
    
            // Trigger the function to autoload if possible
            if (componentName && componentValue && props.functions) {
                props.functions.coreDatasetLoadData({
                    uuid: componentUUID,
                    dataset: componentValue
                });
            }
        }
    }, [props.currentTime]);

    // Prepare the template 
    let itemTemplate = props.children;

    // Check if data item have action
    if (props.children && props.actionItem) {
        const disableTouch = (props.actionItem) ? false : true;
        // Wrap the data item display with touchable component
        itemTemplate = (parameters) => (
            <TouchableOpacity 
                disabled={disableTouch}
                onPress={props.actionItem(parameters.item._data)}>
                    {props.children(parameters)}
            </TouchableOpacity>
        );
    }

    return (
        <FlatList
            style={dataListStyle}
            data={getPropertyValue('componentData', props)}
            renderItem={(parameters) => itemTemplate ? itemTemplate({ ...parameters, parentStyle }) : null}
            keyExtractor={(_, index) => index.toString()}
        />
    )
}

export default React.memo(DataList);