import React from 'react';

import { getPropertyValue } from '../core/Component';
import { getStyle } from '../core/Style';
import DropDownPicker from 'react-native-dropdown-picker';
import { useInitialValue } from '../hook/InitialValue';
import { useElementAttribute } from '../hook/ElementAttribute';

const ComboBox = (props) => {
    let comboBoxStyle = getStyle('comboBox', props);
    // Identifies whether to display the component
    const isHidden = comboBoxStyle?.dropdownPicker?.containerStyle?.display == "none";
    if (isHidden) {
        return null;
    }

    // Prepare reusable variables
    let componentUUID = getPropertyValue('componentUUID', props);

    // Initialize elementAttribute
    useElementAttribute(componentUUID, props.functions, props.currentTime);
    
    // Generate the initial value only once
    useInitialValue(props.value, componentUUID, props.functions, props.currentTime);

    let componentValue = getPropertyValue('componentValue', props);
    let componentOptions = getPropertyValue('componentOptions', props, []);

    // Validate the default value if it exists
    let validDefaultValue = false
    for (let index in componentOptions) {
        if (componentOptions[index].value == componentValue) {
            validDefaultValue = true;
            break;
        }
    }
    if (!validDefaultValue) {
        componentValue = null;
    }

    return (
        <DropDownPicker
            containerStyle={comboBoxStyle.dropdownPicker.containerStyle}
            defaultValue={componentValue}
            onChangeItem={(itemValue) =>
                props.functions.setComponentValue({ component: getPropertyValue('componentName', props), value: (itemValue.value ? itemValue.value : '') })
            }
            items={componentOptions}
            showArrow={false}
            placeholder=""
            style={comboBoxStyle.dropdownPicker.style}
        />
    );
}

export default React.memo(ComboBox);