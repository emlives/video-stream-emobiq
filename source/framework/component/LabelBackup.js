import React from 'react';
import { TouchableOpacity, ImageBackground, Text } from 'react-native';
// import { LinearGradient } from 'expo-linear-gradient';

import { getPropertyValue } from '../core/Component';
import { getStyle } from '../core/Style';
import { useTranslate } from '../hook/Translate';
import { useElementAttribute } from '../hook/ElementAttribute';
import { useInitialValue } from '../hook/InitialValue';

const Label = (props) => {
    // Prepare reusable variables
    let componentUUID = getPropertyValue('componentUUID', props);
    // const renderCount = useRef(0);
    // const prev = useRef(props);
    // useEffect(() => {
    //     const changedProps = Object.entries(props).reduce((ps, [k, v]) => {
    //     if (prev.current[k] !== v) {
    //         ps[k] = [prev.current[k], v];
    //     }
    //     return ps;
    //     }, {});
    //     if (Object.keys(changedProps).length > 0) {
    //     console.log(getPropertyValue('componentName', props) + " " + getPropertyValue('componentUUID', props)+ ' ' + renderCount.current ++ + ' Changed props:', changedProps);
    //     }
    //     prev.current = props;
    // });
    // Initialize elementAttribute

    // Initialize elementAttribute
    useElementAttribute(componentUUID, props.functions, props.currentTime);
    
    // Generate the initial value only once
    useInitialValue(props.value, componentUUID, props.functions, props.currentTime);

    // Translate the display value
    let displayedValue = useTranslate(getPropertyValue('componentValue', props));

    const labelComponent = getStyle('label', props);
    // const imageBackground = labelComponent.imageBackground;
    // const linearGradient = labelComponent.linearGradient;
    const disableTouch = props.action ? false : true;
    
    // Identifies whether to display the component
    const isHidden = labelComponent?.view?.style?.display == "none";
    if (isHidden) {
        return null;
    }
    
    return (
        <TouchableOpacity disabled={disableTouch} style={labelComponent.view.style} onPress={props.action} >
            {/* <LinearGradient
                colors={linearGradient.colors}
                start={linearGradient.point.start}
                end={linearGradient.point.end}
                style={linearGradient.style}/>
            <ImageBackground 
                source={imageBackground.source} 
                resizeMethod='resize' 
                imageStyle={imageBackground.imageStyle} 
                style={imageBackground.style} /> */}
            <Text style={labelComponent.text.style}>{displayedValue}</Text>
        </TouchableOpacity>

    )
}

export default React.memo(Label);