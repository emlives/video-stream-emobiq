import React, { useEffect } from 'react';
import { TextInput, View } from 'react-native';

import { getPropertyValue } from '../core/Component';
import { getStyle } from '../core/Style';

import { useElement } from '../hook/Element';
import { useInitialValue } from '../hook/InitialValue';
import { useElementAttribute } from '../hook/ElementAttribute';

const Edit = (props) => {
    let editStyle = getStyle('edit', props, "View");
    // Identifies whether to display the component
    const isHidden = editStyle?.view?.style?.display == "none";
    if (isHidden) {
        return null;
    }

    // Store the element reference
    const element = useElement();

    // Prepare reusable variables
    let componentUUID = getPropertyValue('componentUUID', props);

    // Generate the initial value only once
    useInitialValue(props.value, componentUUID, props.functions, props.currentTime);

    // Initialize elementAttribute
    useElementAttribute(componentUUID, props.functions, props.currentTime);
    
    // Update the state value based on the changed text
    const updateStateChangeText = (value) => {
        if (props.functions) {
            props.functions.coreStateSetProperties({
                uuid: componentUUID,
                data: {
                    componentValue: value
                }
            });
        }
    }

    // Store some default state for setComponentFocus
    let stateDefaultFocus = {
        componentFocusTrigger: false,
        componentFocusSelectText: false,
        componentFocusShowKeyboard: true
    }

    // Onblur revert to the default state for setComponentFocus
    const resetStateDefaultFocus = () => {
         if (props.functions) {
            props.functions.coreStateSetProperties({
                uuid: componentUUID,
                data: stateDefaultFocus
            });
        }
    }

    // Get the focus trigger value from state
    let focusTrigger = getPropertyValue('componentFocusTrigger', props, false);

    useEffect(() => {
        // Focus on the input if needed
        if (element.current && focusTrigger) {
            element.current.focus();
        }
    });

    // Check if this edit component is a password
    let secure = false;
    if (getPropertyValue('type', props) == 'password') {
        secure = true;
    }
    
    return (
        <View style={editStyle.view.style}>
            <TextInput
                defaultValue={getPropertyValue('componentValue', props)}
                secureTextEntry={secure}
                onChangeText={updateStateChangeText}
                onChange={props.actionChange}
                style={editStyle.textInput.style}
                selectionColor={editStyle.textInput.caretColor}
                selectTextOnFocus={getPropertyValue('componentFocusSelectText', props, false)} // For setComponentFocus - selectAllText
                showSoftInputOnFocus={getPropertyValue('componentFocusShowKeyboard', props, true)} // For setComponentFocus - hideKeyboard
                onBlur={resetStateDefaultFocus} // For setComponentFocus - Reset state
                ref={element}
            />
        </View>
    )
}

export default React.memo(Edit);