import React, {useEffect} from 'react';
import { Image as ReactNativeImage, TouchableOpacity, Dimensions } from 'react-native';

import { getPropertyValue } from '../core/Component';
import { getStyle } from '../core/Style';
import EmobiqAssets from '../../dependency/global/Asset'
import { getExternalImageSize } from '../core/Helper'

const Image = (props) => {
    // For css styling
    const disableTouch = props.action ? false : true;
    const style = getStyle('image', props);

    // Identifies whether to display the component
    const isHidden = style?.display == "none";
    const isVisible = getPropertyValue('componentVisible', props, true);
    if (!isVisible || isHidden) {
        return null;
    }

    // Get the source image
    let source = getPropertyValue('componentValue', props);

    // Check if there's no source image path
    // Get the default image
    if (source) {
        if ((typeof source === 'string' || source instanceof String) && source.includes("http")) {
            source = EmobiqAssets[source] !== undefined ? EmobiqAssets[source] : {uri: source}
        }
        else if (typeof source == "object" && !source.uri) {
            source = {uri: source}
        }
    } else {
        source = require('../core/asset/default.png');
    }
    
    const formatImageDimension = async () => {
        const isPercent = (input) => {
            if (/^\d+(\.\d+)?%$/.test(input)) {
                return true;
            } else {
                return false;
            }
        }
    
        let image = ReactNativeImage.resolveAssetSource(source);
        if(source.uri){
            image = await getExternalImageSize(source.uri)
        }
        const imageWidth = image.width;
        const imageHeight = image.height;

        const windowWidth = Dimensions.get('window').width;
        const windowHeight = Dimensions.get('window').height;
        
        let outputWidth = 0;
        let outputHeight = 0;
        
        if(style.width && (typeof style.height == 'undefined')) {
            if(isPercent(style.width)){
                outputWidth = parseInt(windowWidth * (parseFloat(style.width)/100));
                outputHeight = parseInt((outputWidth/imageWidth) * imageHeight);
            } else {
                outputWidth = style.width;
                outputHeight = parseInt((style.width/imageWidth) * imageHeight);
            }
        }

        if(style.height && (typeof style.width == 'undefined')) {
            if(isPercent(style.height)){
                outputHeight = parseInt((windowHeight / 100.0) * parseFloat(style.height));
                outputWidth = parseInt((imageWidth/imageHeight) * outputHeight);
            } else {
                outputHeight = style.height;
                outputWidth = parseInt((imageWidth/imageHeight) * style.height);
            }
        }

        style.width = (outputWidth == 0)?style.width:outputWidth;
        style.height = (outputHeight == 0)?style.height:outputHeight;
    }

    if (source && !isNaN(source)) {
        formatImageDimension();
    }

    let imageStyle = {}

    if(style.width) {
        imageStyle.width = style.width
    }

    if(style.height) {
        imageStyle.height = style.height
    }

    if(!style.resizeMode) {
        imageStyle.resizeMode = 'contain'
    }


    return (
        <TouchableOpacity
            style={style}
            disabled={disableTouch}
            onPress={props.action}>
            <ReactNativeImage name={getPropertyValue('name', props)} style={imageStyle}
                source={source ? source : null} />
        </TouchableOpacity>
    )
}

export default React.memo(Image);