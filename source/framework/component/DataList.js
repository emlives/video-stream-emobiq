import React, { useEffect } from 'react';
import { FlatList, TouchableOpacity, Dimensions } from 'react-native';

import { getPropertyValue } from '../core/Component';
import { getStyle, filterInheritableStyles } from '../core/Style';
import { useStyle } from '../hook/Style';
import { useScrollBottom } from '../hook/ScrollBottom';

const DataList = (props) => {
    // For css styling
    const dataListStyle = getStyle('datalist', props, "Text");
    const style = dataListStyle.style;
    const parentStyle = useStyle(filterInheritableStyles(dataListStyle.original));
    
    // Identifies whether to display the component
    const isHidden = style?.display == "none";
    const isVisible = getPropertyValue('componentVisible', props, true);
    if (!isVisible || isHidden) {
        return null;
    }

    // For autoloading
    useEffect(() => { 
        const componentUUID = getPropertyValue('componentUUID', props);
        // Check if autoload is enabled
        if (getPropertyValue('autoLoad', props)) {
            // Get the component value (dataset)
            let componentName = getPropertyValue('componentName', props);
            let componentValue = getPropertyValue('componentValue', props);
    
            // Trigger the function to autoload if possible
            if (componentName && componentValue && props.functions) {
                props.functions.coreDatasetLoadData({
                    uuid: componentUUID,
                    dataset: componentValue
                });
            }
        }
    }, [props.currentTime]);

    const propsAreEqual = (prevProps, nextProps) => {
        let isSame = true
        for (var componentName in nextProps.item) {
            if(componentName == "_data") {
                continue
            }

            const nextProp = nextProps.item[componentName]
            const prevProp = prevProps.item[componentName]
            if(prevProp.property.componentValue != nextProp.property.componentValue) {
                isSame = false
                break
            }
        }
        return isSame    
    }

    const MemoizeItem = React.memo(( {index, item, parentStyle, currentTime}) => {
        console.log(`- renderItem ${index}`)
        const disableTouch = (props.actionItem) ? false : true;    
        if(props.template){
            return (
                <TouchableOpacity 
                        disabled={disableTouch}
                        onPress={props.actionItem?.(item._data)}>
                    {props.template({item, parentStyle, currentTime})}

                </TouchableOpacity>
            );
        }
        return null
    }, propsAreEqual);

    const memoizeRenderItem = React.useCallback(({item, index}) => {
        return (
            <MemoizeItem 
                index={index}
                item={item} 
                parentStyle={parentStyle}
                currentTime={props.currentTime}
            />
        )}, [])

    // 3.0 - To handle onEndReached event of FlatList
    // 3.1 - Defined a flag (scrollStarted) to ensure that it is a user-triggered onEndReached event
    const [handlenOnMomentumScrollBegin, handleOnEndReached] = useScrollBottom(props.scrollBottom);

    return (
        <FlatList
            style={style}
            data={getPropertyValue('componentData', props)}
            renderItem={memoizeRenderItem}
            keyExtractor={(_, index) => index.toString()}
            onEndReached={handleOnEndReached}
            onEndReachedThreshold={0.1}
            onMomentumScrollBegin={handlenOnMomentumScrollBegin}
            windowSize={Dimensions.get('window').height*2}  //to prevent empty areas
        />
    )
}

// export default React.memo(DataList);
export default DataList;