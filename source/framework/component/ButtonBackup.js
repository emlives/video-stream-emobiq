import React from 'react';
import { 
    TouchableOpacity, Text 
} from 'react-native';
import { getPropertyValue } from '../core/Component';
import { getStyle } from '../core/Style';
import { useTranslate } from '../hook/Translate';
import { useElementAttribute } from '../hook/ElementAttribute';

const Button = (props) => {
    const buttonStyle = getStyle('button', props);
    
    // Identifies whether to display the component
    const isHidden = buttonStyle?.touchableOpacity?.style?.display == "none";
    if (isHidden) {
        return null;
    }
    
    // Prepare reusable variables
    let componentUUID = getPropertyValue('componentUUID', props);

    // Initialize elementAttribute
    useElementAttribute(componentUUID, props.functions, props.currentTime);
        
    return (
        <TouchableOpacity 
            style={buttonStyle.touchableOpacity.style} 
            onPress={props.action}>
            <Text style={buttonStyle.text.style}>{useTranslate(getPropertyValue('componentValue', props))}</Text>
        </TouchableOpacity>
    )    
}

export default React.memo(Button);