import { useState } from 'react';
import { getManufacturer, getSerialNumber } from 'react-native-device-info';

import { DeviceData } from '../../dependency/global/Variable';
import { loadDataToGlobal } from '../plugin/AsyncStorage';

/**
 * Load the device info's manufacturer and serial number
 */
const loadDeviceInfo = async () => {
    // Get the manufacturer
    DeviceData.manufacturer = await getManufacturer();

    // Get the device serial
    DeviceData.serialNumber = await getSerialNumber();
};

/**
 * Hook that loads all data from async storage into non-persistent global variable
 * This also get the manufacturer and serial number of a device
 * 
 * @returns {boolean}
 */
export const loadData = () => {
    const [loading, setLoading] = useState(true);

    (async () => {
        // Wait for the data to be loaded
        await loadDataToGlobal();

        // Load the device info
        await loadDeviceInfo();

        // Whether success or not, need to set this to false
        setLoading(false);
    })();

    return loading;
};