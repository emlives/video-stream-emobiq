import { useState, useEffect, useRef } from 'react';
import isEqual from 'lodash/isEqual';

/**
 * A hook that translate the values to the 
 * right language.
 * 
 * @param {string} value 
 */
export const useStyle = (value) => {
    // Prepare the local state
    const [style, setStyle] = useState();
    const isFirst = useRef(true);
    const prevStyle = useRef(value);
    // Upon rendering do deep checking on the passed object first
    useEffect(
        () => {
            const isFirstEffect = isFirst.current;
            const isSame = isEqual(prevStyle.current, value)

            isFirst.current = false;
            prevStyle.current = value;
            // if first run or if the style is different, then update the style
            if (isFirstEffect || !isSame) {
                let inheritedStyle = value;
                setStyle(inheritedStyle);
            }
        },
        [value]
    );

    return style;
}