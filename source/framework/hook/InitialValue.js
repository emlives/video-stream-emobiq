import { useEffect } from 'react';

/**
 * A hook that get the value from async function
 * 
 * @param {function} value        - the value to be used
 * @param {string} componentUUID  - component uuid
 * @param {object} functions      - the function that contains the state and core functions
 * @param {date} currentTime      - the timestamp that would be used to identify whether to 
 *                                - trigger this hook as well as the componentUUID
 */
export const useInitialValue = (value, componentUUID, functions, currentTime) => {
    // Upon rendering do translation
    useEffect(
        () => {
            (async () => {
                // Make sure it contains valid parameters
                if (!componentUUID || !functions || typeof value !== 'function') {
                    return;
                }

                // Trigger the function first
                let data = await value();

                // Make sure the data is string
                if (typeof data !== 'string') {
                    return;
                }

                // Set the value to the state
                functions.coreStateSetProperties({
                    uuid: componentUUID,
                    data: {
                        componentValue: data
                    }
                });
            })();
        },
        [currentTime, componentUUID]
    );
}