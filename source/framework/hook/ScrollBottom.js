import { useRef } from 'react';

/**
 * A hook that marks the start of a scroll
 * 
 */
export const useScrollBottom = (scrollBottom) => {
    // 3.0 - To handle onEndReached event of FlatList
    // 3.1 - Defined a flag (scrollStarted) to ensure that it is a user-triggered onEndReached event
    const scrollStarted = useRef(false);
    const scrollBottomTriggered = useRef(false);

    // 3.2 Sets the flag (scrollStarted) to true
    const handlenOnMomentumScrollBegin = () => {
        if(!scrollStarted.current) {
            scrollStarted.current = true
        }
    }
    
    // 3.3 - Invoked the DataList implementation of scrollBottom
    // added scrollBottomTriggered flag to prevent any possible re-triggering of scrollBottom() within 1 sec
    const handleOnEndReached = () => {
        if(scrollStarted.current && !scrollBottomTriggered.current){
            scrollStarted.current = false
            scrollBottom?.()
            scrollBottomTriggered.current = true
            setTimeout(function(){ scrollBottomTriggered.current = false }, 1000);
        }
     }
    
    return [ handlenOnMomentumScrollBegin, handleOnEndReached ]
}