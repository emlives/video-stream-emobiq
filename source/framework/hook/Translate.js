import { useState, useEffect } from 'react';

import { translate } from '../core/Language';

/**
 * A hook that translate the values to the 
 * right language.
 * 
 * @param {string} value 
 */
export const useTranslate = (value) => {
    // Prepare the local state
    const [translated, setTranslated] = useState(value);

    // Upon rendering do translation
    useEffect(
        () => {
            (async () => {
                setTranslated(await translate(value));
            })();
        },
        [value]
    );

    return translated;
}