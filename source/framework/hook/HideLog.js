import { useEffect } from 'react';
import { LogBox } from 'react-native';

/**
 * A hook that initialized element attribute of components
 * to enable legacy functions.
 * @param {string} componentUUID  - component uuid
 * @param {object} functions      - the function that contains the state and core functions
 * @param {date} currentTime      - the timestamp that would be used to identify whether to 
 *                                - trigger this hook as well as the componentUUID
 * 
 */
export const hideLog = () => {
    useEffect(() => {
        LogBox.ignoreLogs(['VirtualizedLists should never be nested', 'Setting a timer']);
        // LogBox.ignoreAllLogs();
    }, [])
}