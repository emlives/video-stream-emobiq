import { useState, useEffect } from 'react';

/**
 * A hook that listens to the movement of navigator
 * and return the time.
 * 
 * @param {object} navigation - the navigation object
 */
export const useNavigateResponder = (navigation) => {
    // Prepare the local state
    const [currentTime, setCurrentTime] = useState(new Date());

    // Disable the first time updating because there is a default value already
    let initialize = true;

    // Upon rendering do translation
    useEffect(
        () => {
            // To trigger render on some components every time screen changes
            const unsubscribe = navigation.addListener('focus', () => {
                // Don't update time if it is the initial trigger
                if (!initialize) {
                    setCurrentTime(new Date());
                    return;
                }
                initialize = false;
            });
            // Return the function to unsubscribe from the event so it gets removed on unmount
            return unsubscribe;
        },
        [navigation]
    );

    return currentTime;
}