import { useRef } from 'react';

/**
 * A hook that generates and store the element 
 * reference of the component.
 */
export const useElement = () => {
    // Prepare the local state
    const element = useRef();

    return element;
}