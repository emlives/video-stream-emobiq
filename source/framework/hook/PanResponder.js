import React from 'react';
import { PanResponder } from 'react-native';
import { InactivityInterval } from '../../dependency/global/Variable';

/**
 * A hook that handles the touch event in the app
 */
export const usePanResponder = () => {
    // Create the panResponder object through ref
    // so that it sticks through out the whole runtime.
    const panResponder = React.useRef(
        PanResponder.create({
            // Ask to be the responder:
            onStartShouldSetPanResponderCapture: (evt, gestureState) => {
                // Refresh all the inactivity interval upon touch in the screen
                let useTime = new Date().getTime() / 1000;
                for (let key in InactivityInterval) {
                    InactivityInterval[key] = useTime;
                }
            }
        })
    ).current;

    return panResponder;
}