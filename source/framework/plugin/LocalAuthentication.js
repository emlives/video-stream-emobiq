import * as LocalAuthentication from 'expo-local-authentication';

import { resultPlugin } from '../core/Format';

/**
 * Check if authentication is available and enabled
 * @param callback {function} - Triggers if it is successful.
 * @param errorCallback {function} - Triggers if it failed.
 * 
 * @return {promise} - {object}
 */
 export const hasAuthentication = async (callback, errorCallback) => {
    // Prepare the default value
    let enabled = false;

    // Validate now
    const isCompatible = await LocalAuthentication.hasHardwareAsync();
    const isEnrolled = await LocalAuthentication.isEnrolledAsync();
    if (isCompatible && isEnrolled) {
        enabled = true;
    }

    // Not enabled
    if (!enabled) {
        errorCallback('No Touch Sensor / Inactive');
        return resultPlugin(false, null, 'No Touch Sensor / Inactive');
    }

    callback('Touch Sensor Active');
    return resultPlugin(true, null, 'Touch Sensor Active');
}

/**
 * Authenticate
 * @param options {object} - The options to be passed in the authentication.
 * @param callback {function} - Triggers if it is successful.
 * @param errorCallback {function} - Triggers if it failed.
 * 
 * @return {promise} - {object}
 */
 export const authenticate = async (options, callback, errorCallback) => {
    // Authenticate 
    let result = await LocalAuthentication.authenticateAsync(options);

    // Failed
    if (!result.success) {
        errorCallback('Failed to verify Touch/Face ID.');
        return resultPlugin(false, null, 'Failed to verify Touch/Face ID.');
    }

    callback('Touch/Face ID verified.');
    return resultPlugin(true, null, 'Touch/Face ID verified.');
}