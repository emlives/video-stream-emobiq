import { NativeModules } from 'react-native';
import { resultPlugin } from '../core/Format';

const { BtPrinter } = NativeModules;

/**
 * Scans and displays bluetooth devices discoverable by device
 * @param {functionList} callback  - Success callback with list of discoverable bluetooth devices
 * @param {functionList} errorCallback  - Error callback
 */
export const scanForBLEPeripherals = async (callback, errorCallback) => {

    console.log('... plugin.BtPrinter.scanForBLEPeripherals...')
    const timeout = 5
    // const { BtPrinter } = NativeModules;
    BtPrinter.scanForBLEPeripherals(
        timeout,
        (error, devices) => {
            if (error) {
                errorCallback(error);
                return resultPlugin(false, null, error);
            }

            if (devices) {
                callback(devices);
                console.log('... success callback', devices)
                return resultPlugin(true, devices, 'Successfully scanned and returned list of BLE devices');
            }
        }
    );
}

/**
 * Establish connection to bluetooth printer for printing service
 * @param {string} printerId - The uuid (for iOS) or address (for Android) of printer
 * @param {functionList} callback  - Success callback
 * @param {functionList} errorCallback  - Error callback
 */
export const connect = async (printerId, callback, errorCallback) => {
    // const { BtPrinter } = NativeModules;
    BtPrinter.connect(
        printerId,
        (error, success) => {
            if (error) {
                errorCallback(error)
                return resultPlugin(false, null, error);
            }

            if (success) {
                callback()
                return resultPlugin(true, null, `Successfully connected to device ${printerId}`);
            }
        }
    );
}

/**
 * Print input content
 * @param {string} text - The print content
 * @param {functionList} callback  - Success callback
 * @param {functionList} errorCallback  - Error callback
 */
export const write = async (text, callback, errorCallback) => {
    // const { BtPrinter } = NativeModules;
    BtPrinter.write(
        text,
        (error, devices) => {
            if (error) {
                errorCallback(error)
                return resultPlugin(false, null, error);
            }

            if (devices) {
                callback(devices)
                return resultPlugin(true, devices, 'Successfully printed content');
            }
        }
    );
}