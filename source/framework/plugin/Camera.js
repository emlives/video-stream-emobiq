import React, { useState, useEffect } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';

import { Camera as ExpoCamera } from 'expo-camera';

const Camera = () => {

    const [hasPermission, setHasPermission] = useState(null);
    const [type, setType] = useState(ExpoCamera.Constants.Type.back);

    useEffect(() => {
        (async () => {
            const { status } = await ExpoCamera.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);

    if (hasPermission === null) {
        return <View />;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }

    return (
        <View style={{ height: '100%', width: "100%", flex: 1}}>
            <ExpoCamera style={{ flex: 1 }} type={type}>
                <View>
                    <TouchableOpacity
                        onPress={() => {
                        setType(
                            type === ExpoCamera.Constants.Type.back
                            ? ExpoCamera.Constants.Type.front
                            : ExpoCamera.Constants.Type.back
                        );
                        }}>
                        <Text> Flip </Text>
                    </TouchableOpacity>
                </View>
             </ExpoCamera>
        </View>
    );
}

export default Camera;