import ExpoAsyncStorage from '@react-native-async-storage/async-storage';

import { resultPlugin } from '../core/Format';
import { addQueue } from '../core/Queue';

import {
    PrefixFramework,
    NameSetting
} from '../core/Constant';

// Contains all the system based keys
let systemKeys = [
    PrefixFramework + NameSetting
]

/**
 * The global storage data that will hold the same data as in asyncstorage
 */
export const globalStoreData = {};

/**
 * Load the data from async storage to non-persistent global variable
 * 
 * @returns {promise} - {object}
 */
export const loadDataToGlobal = async () => {
    // Get all keys
    let keys = await ExpoAsyncStorage.getAllKeys();

    // Get all the values
    let data = await ExpoAsyncStorage.multiGet(keys);

    // Load the values to global variable
    // value format: 
    //     value[0] - key
    //     value[1] - value
    for (let value of data) {
        globalStoreData[value[0]] = value[1];
    }

    return resultPlugin(true, null, null);
};

/**
 * Store the data to the async storage
 * @param {*} key 
 * @param {*} value
 * @param {bool} system - Set whether to filter out the
 *                        keys from the system keys
 * 
 * @return {object}
 */
export const storeData = (key, value, system = false) => {
    // Make sure the key is not a system protected
    if (!system && isSystemKey(key)) {
        return resultPlugin(false, null, 'The key is a system protected one, please use another key.');
    }

    // Save in non-persistent variable
    globalStoreData[key] = value;

    addQueue((complete) => {
        (async () => {
            await ExpoAsyncStorage.setItem(key, value);

            // Run this function so that this will be completed and run the next item in queue
            complete();
        })();
    });

    return resultPlugin(true, null, null);
}

/**
 * Get the data to from async storage
 * @param {string} key 
 * 
 * @return {mixed}
 */
export const getData = (key) => {
    // Get the data from non-persistent global variable
    let value = typeof globalStoreData[key] !== 'undefined' ? globalStoreData[key] : null;
    return resultPlugin(true, value, null);
}

/**
 * Remove the data from async storage
 * @param {string} key 
 * 
 * @return {object}
 */
export const removeData = (key) => {
    // Remove the key from non-persistent global variable
    if (typeof globalStoreData[key] !== 'undefined') {
        delete globalStoreData[key];
    }

    addQueue((complete) => {
        (async () => {
            // Remove from async storage
            await ExpoAsyncStorage.removeItem(key);

            // Run this function so that this will be completed and run the next item in queue
            complete();
        })();
    });

    return resultPlugin(true, null, null);
}

/*****
 * Private Functions
 *****/

/**
 * Check if the key is within the system key
 * @param {string} key 
 * 
 * @return {boolean}
 */
function isSystemKey(key) {
    return systemKeys.includes(key);
}