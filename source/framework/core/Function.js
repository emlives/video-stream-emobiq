import captureImage from '../function/devices/CaptureImage';

// Global - Page
import gotoPage from '../function/global/GotoPage';
import getPreviousPage from '../function/global/GetPreviousPage';
import getCurrentPage from '../function/global/GetCurrentPage';

// Global - Standard
import makeId from '../function/global/MakeId';
import jsonEncode from '../function/global/JsonEncode';
import jsonDecode from '../function/global/JsonDecode';
import xmlDecode from '../function/global/XmlDecode';
import defaultImage from '../function/global/DefaultImage';
import generateRandomNumber from '../function/global/GenerateRandomNumber';
import generateRandomAlphabet from '../function/global/GenerateRandomAlphabet';
import generateRandomAlphanumeric from '../function/global/GenerateRandomAlphanumeric';

// Browser
import valueFromURL from '../function/browser/ValueFromUrl';

// String
import increment from '../function/string/Increment';
import lowerCase from '../function/string/LowerCase';
import upperCase from '../function/string/UpperCase';
import capitalize from '../function/string/Capitalize';
import trim from '../function/string/Trim';
import left from '../function/string/Left';
import right from '../function/string/Right';
import mid from '../function/string/Mid';
import concat from '../function/string/Concat';
import findIndex from '../function/string/FindIndex';
import replace from '../function/string/Replace';
import length from '../function/string/Length';
import padString from  '../function/string/PadString';
import encodeBase64String from '../function/string/EncodeBase64String';
import decodeBase64String from '../function/string/DecodeBase64String';

// Math
import multi from '../function/math/Multi';
import div from '../function/math/Div';
import mod from '../function/math/Mod';
import add from '../function/math/Add';
import sub from '../function/math/Sub';
import sqr from '../function/math/Sqr';
import sqrt from '../function/math/Sqrt';
import pow from '../function/math/Pow';
import absolute from '../function/math/Absolute';
import atan2 from '../function/math/Atan2';
import sin from '../function/math/Sin';
import tan from '../function/math/Tan';
import cos from '../function/math/Cos';
import pi from '../function/math/Pi';
import ceil from '../function/math/Ceil';
import floor from '../function/math/Floor';
import min from '../function/math/Min';
import max from '../function/math/Max';

// Formula
import locationDistance from '../function/formula/LocationDistance';

// Array - Calculation
import sum from '../function/array/Sum';
import count from '../function/array/Count';
import avg from '../function/array/Avg';
import sort from '../function/array/Sort';

// Array + Object - Manipulation
import newArray from '../function/array/NewArray';
import newObject from '../function/array/NewObject';
import push from '../function/array/Push';
import pushObject from '../function/array/PushObject';
import pop from '../function/array/Pop';
import unshift from '../function/array/Unshift';
import shift from '../function/array/Shift';

// Array - General
import findArrayIndex from '../function/array/FindArrayIndex';

// Conversion
import toString from '../function/conversion/ToString';
import toBoolean from '../function/conversion/ToBoolean';
import toInteger from '../function/conversion/ToInteger';
import toFloat from '../function/conversion/ToFloat';
import round from '../function/conversion/Round';
import join from '../function/conversion/Join';
import split from '../function/conversion/Split';

// Logical
import not from '../function/logical/Not';
import or from '../function/logical/Or';
import and from '../function/logical/And';
import xor from '../function/logical/Xor';

// Comparation
import equal from '../function/comparation/Equal';
import notEqual from '../function/comparation/NotEqual';
import greater from '../function/comparation/Greater';
import less from '../function/comparation/Less';
import equalOrGreater from '../function/comparation/EqualOrGreater';
import equalOrLess from '../function/comparation/EqualOrLess';

// Validation
import isNumber from '../function/validation/IsNumber';
import isEmail from '../function/validation/IsEmail';

// Object
import objectKeys from '../function/object/ObjectKeys';
import objectValues from '../function/object/ObjectValues';
import dbDate from '../function/object/DbDate';
import addDate from '../function/object/AddDate';
import formatDate from '../function/object/FormatDate';
import strToDate from '../function/object/StrToDate';
import formatNumber from '../function/object/FormatNumber';
import dateDiff from '../function/object/DateDiff';

// Special Character
import specialCharacter, { scList } from '../function/special-character/SpecialCharacter';

// Flow
import conditional from '../function/flow/Conditional';
import map from '../function/flow/Map';
import forLoop from '../function/flow/ForLoop';

// App
import setVar from '../function/app/SetVar';
import getVar from '../function/app/GetVar';
import getVarAttr from '../function/app/GetVarAttr';
import clearAllVar from  '../function/app/ClearAllVar';

import enablePage from '../function/app/EnablePage';
import disablePage from './../function/app/DisablePage';
import isElementShown from '../function/app/IsElementShown';
import hideElement from './../function/app/HideElement';
import showElement from './../function/app/ShowElement';
import setComponentValue from './../function/app/SetComponentValue';
import setComponentFocus from './../function/app/SetComponentFocus';
import setComponentElAttr from './../function/app/SetComponentElAttr';
import setComponentAttr from './../function/app/SetComponentAttr';
import openLink from '../function/app/OpenLink';
import getComponent from '../function/app/GetComponent';
import componentValue from '../function/app/ComponentValue';
import componentElAttr from '../function/app/ComponentElAttr';
import componentAttr from '../function/app/ComponentAttr';
import objectAttr from '../function/app/ObjectAttr';
import setObjectAttr from '../function/app/SetObjectAttr';
import toArray from '../function/app/ToArray';
import toObject from '../function/app/ToObject';
import setTimeout from '../function/app/SetTimeout';
import setInterval from '../function/app/SetInterval';
import callback from '../function/app/Callback';
import bbCodeToCanvas from '../function/app/BbCodeToCanvas';
import bbCodeToCanvasSync from '../function/app/BbCodeToCanvasSync';
import inactivityTimeout from '../function/app/InactivityTimeout';
import console from '../function/app/Console';
import setComboOptions from '../function/app/SetComboOptions';

import languageConvertData from '../function/app/LanguageConvertData';
import getLanguage from '../function/app/GetLanguage';
import getLanguageList from '../function/app/GetLanguageList';
import setLanguage from '../function/app/SetLanguage';

// Dataset
import selectAll from '../function/dataset/SelectAll';
import selectBy from '../function/dataset/SelectBy';
import updateBy from '../function/dataset/UpdateBy';
import deleteBy from '../function/dataset/DeleteBy';
import insert from '../function/dataset/Insert';
import dataFromString from '../function/dataset/DataFromString';
import clearData from '../function/dataset/ClearData';
import loadData from '../function/dataset/LoadData';
import loadNext from '../function/dataset/LoadNext';
import navCall from '../function/dataset/NavCall';
import selectByMulti from '../function/dataset/SelectByMulti';
import deleteByMulti from '../function/dataset/DeleteByMulti';
import updateByMulti from '../function/dataset/UpdateByMulti';
import rawCall from '../function/dataset/RawCall';

// Device
import keyboardHide from '../function/devices/KeyboardHide';
import onBackButton from '../function/devices/OnBackButton';
import exitApp from '../function/devices/ExitApp';
import deviceId from '../function/devices/DeviceID';
import deviceName from '../function/devices/DeviceName';
import deviceSerial from '../function/devices/DeviceSerial';
import deviceOS from '../function/devices/DeviceOs';
import deviceOSVersion from '../function/devices/DeviceOsVersion';
import deviceManufacturer from '../function/devices/DeviceManufacturer';
import deviceHasTouchSensor from '../function/devices/DeviceHasTouchSensor';
import deviceVerifyTouchSensor from '../function/devices/DeviceVerifyTouchSensor';

// Printer
import btPrinterPortList from '../function/printer/BtPrinterPortList';
import btPrinterConnect from '../function/printer/BtPrinterConnect';
import btPrinterPrint from '../function/printer/BtPrinterPrint';
import canvasToCPCL from '../function/printer/CanvasToCPCL';

// User Defined - Global Functions
import updateLabel, { name as updateLabelName, } from '../function/user-defined/UpdateLabel';
import LoginAs, { name as LoginAsName } from '../function/user-defined/LoginAs';
import globalModalInfo, { name as globalModalInfoName } from '../function/user-defined/GlobalModalInfo';
import globalLogout, { name as globalLogoutName } from '../function/user-defined/GlobalLogout';
import globalModalLoading, { name as globalModalLoadingName } from '../function/user-defined/GlobalModalLoading';
import globalCalculateTotal, { name as globalCalculateTotalName } from '../function/user-defined/GlobalCalculateTotal';
import globalModalQuestion, { name as globalModalQuestionName } from '../function/user-defined/GlobalModalQuestion';
import globalModalHide, { name as globalModalHideName } from '../function/user-defined/GlobalModalHide';
import globalCSS, { name as globalCSSName } from '../function/user-defined/GlobalCSS';

// Functions used within the framework itself
// Core - Dataset 
import coreDatasetLoadData from '../function/core/dataset/CoreDatasetLoadData';
// Core - State
import coreStateSetProperties from '../function/core/state/CoreStateSetProperties';
import coreStateSetElementAttributes from '../function/core/state/CoreStateSetElementAttributes';
import coreStateInitializePage from '../function/core/state/CoreStateInitializePage';

/**
 * Function class
 * Initiated per page in the application.
 */
class Function {
    stateName =  '';
    pageProps = '';

    constructor(stateName) {
        this.stateName = stateName;
        this.initializeSpecialCharacterFunctions();
    }

    /**
     * Adds the functions to get the special characters to this object
     */
    initializeSpecialCharacterFunctions = () => {
        // Get the special character function 
        let specialChar = specialCharacter();

        // Loop through all special character functions
        scList.map(sc => {
            // Split the item by space
            // Ignore the code, just get the name of the special character
            let [, name] = sc.split(' ');

            // Add the function
            this['sc_' + name] = () => {
                return specialChar()[name];
            };
        });
    }

    // Global - Page
    gotoPage = (parameters) => {
        return gotoPage(this.pageProps.route, this.pageProps.navigation, this.stateName, this.pageProps.dispatch, parameters);
    }
    getPreviousPage = () => {
        return getPreviousPage();
    }
    getCurrentPage = () => {
        return getCurrentPage(this.pageProps.route);
    }

    // Global - Standard
    makeId = () => {
        return makeId();
    }
    jsonEncode = (parameters) => {
        return jsonEncode(parameters);
    }
    jsonDecode = (parameters) => {
        return jsonDecode(parameters);
    }
    xmlDecode = (parameters) => {
        return xmlDecode(parameters);
    }
    defaultImage = () => {
        return defaultImage;
    }
    generateRandomNumber = (parameters) => {
        return generateRandomNumber(parameters);
    }
    generateRandomAlphabet = (parameters) => {
        return generateRandomAlphabet(parameters);
    }
    generateRandomAlphanumeric = (parameters) => {
        return generateRandomAlphanumeric(parameters);
    }

    // Browser 
    valueFromURL = (parameters) => {
        return valueFromURL(parameters);
    }

    // String
    increment = (parameters) => {
        return increment(parameters);
    }
    lowerCase = (parameters) => {
        return lowerCase(parameters);
    }
    upperCase = (parameters) => {
        return upperCase(parameters);
    }
    capitalize = (parameters) => {
        return capitalize(parameters);
    }
    trim = (parameters) => {
        return trim(parameters);
    }
    left = (parameters) => {
        return left(parameters);
    }
    right = (parameters) => {
        return right(parameters);
    }
    mid = (parameters) => {
        return mid(parameters);
    }
    concat = (parameters) => {
        return concat(parameters);
    }
    findIndex = (parameters) => {
        return findIndex(parameters);
    }
    replace = (parameters) => {
        return replace(parameters);
    }
    length = (parameters) => {
        return length(parameters);
    }
    padString = (parameters) => {
        return padString(parameters);
    }
    encodeBase64String = (parameters) => {
        return encodeBase64String(parameters);
    }
    decodeBase64String = (parameters) => {
        return decodeBase64String(parameters);
    }

    // Math
    multi = (parameters) => {
        return multi(parameters);
    }
    div = (parameters) => {
        return div(parameters);
    }
    mod = (parameters) => {
        return mod(parameters);
    }
    add = (parameters) => {
        return add(parameters);
    }
    sub = (parameters) => {
        return sub(parameters);
    }
    sqr = (parameters) => {
        return sqr(parameters);
    }
    sqrt = (parameters) => {
        return sqrt(parameters);
    }
    pow = (parameters) => {
        return pow(parameters);
    }
    absolute = (parameters) => {
        return absolute(parameters);
    }
    atan2 = (parameters) => {
        return atan2(parameters);
    }
    sin = (parameters) => {
        return sin(parameters);
    }
    tan = (parameters) => {
        return tan(parameters);
    }
    cos = (parameters) => {
        return cos(parameters);
    }
    pi = () => {
        return pi();
    }
    ceil = (parameters) => {
        return ceil(parameters);
    }
    floor = (parameters) => {
        return floor(parameters);
    }
    min = (parameters) => {
        return min(parameters);
    }
    max = (parameters) => {
        return max(parameters);
    }

    // Formula
    locationDistance = (parameters) => {
        return locationDistance(parameters);
    }

    // Array - Calculation
    sum = (parameters) => {
        return sum(parameters);
    }
    count = (parameters) => {
        return count(parameters);
    }
    avg = (parameters) => {
        return avg(parameters);
    }
    sort = (parameters) => {
        return sort(parameters);
    }

    // Array + Object - Manipulation
    newArray = (parameters) => {
        return newArray(parameters);
    }
    newObject = (parameters) => {
        return newObject(parameters);
    }
    push = (parameters) => {
        return push(parameters);
    }
    pushObject = (parameters) => {
        return pushObject(parameters);
    }
    pop = (parameters) => {
        return pop(parameters);
    }
    unshift = (parameters) => {
        return unshift(parameters);
    }
    shift = (parameters) => {
        return shift(parameters);
    }

    // Array - General
    findArrayIndex = (parameters) => {
        return findArrayIndex(parameters);
    }

    // Conversion
    toString = (parameters) => {
        return toString(parameters);
    }
    toBoolean = (parameters) => {
        return toBoolean(parameters);
    }
    toInteger = (parameters) => {
        return toInteger(parameters);
    }
    toFloat = (parameters) => {
        return toFloat(parameters);
    }
    round = (parameters) => {
        return round(parameters);
    }
    join = (parameters) => {
        return join(parameters);
    }
    split = (parameters) => {
        return split(parameters);
    }

    // Logical
    not = (parameters) => {
        return not(parameters);
    }
    or = (parameters) => {
        return or(parameters);
    }
    and = (parameters) => {
        return and(parameters);
    }
    xor = (parameters) => {
        return xor(parameters);
    }

    // Comparation
    equal = (parameters) => {
        return equal(parameters);
    }
    notEqual = (parameters) => {
        return notEqual(parameters);
    }
    greater = (parameters) => {
        return greater(parameters);
    }
    less = (parameters) => {
        return less(parameters);
    }
    equalOrGreater = (parameters) => {
        return equalOrGreater(parameters);
    }
    equalOrLess = (parameters) => {
        return equalOrLess(parameters);
    }

    // Validation
    isNumber = (parameters) => {
        return isNumber(parameters);
    }
    isEmail = (parameters) => {
        return isEmail(parameters);
    }

    // Object
    objectKeys = (parameters) => {
        return objectKeys(parameters);
    }
    objectValues = (parameters) => {
        return objectValues(parameters);
    }
    dbDate = (parameters) => {
        return dbDate(parameters);
    }
    addDate = (parameters) => {
        return addDate(parameters);
    }
    formatDate = (parameters) => {
        return formatDate(parameters);
    }
    strToDate = (parameters) => {
        return strToDate(parameters);
    }
    formatNumber = (parameters) => {
        return formatNumber(parameters);
    }
    dateDiff = (parameters) => {
        return dateDiff(parameters);
    }

    // Flow
    conditional = (parameters) => {
        return conditional(parameters);
    }
    map = (parameters) => {
        return map(parameters);
    }
    forLoop = (parameters) => {
        return forLoop(parameters);
    }

    // App
    setVar = (parameters) => {
        return setVar(parameters);
    }
    getVar = (parameters) => {
        return getVar(parameters);
    }
    getVarAttr = (parameters) => {
        return getVarAttr(parameters);
    }
    clearAllVar = (parameters) => {
        return clearAllVar(parameters);
    }
    enablePage = () => {
        return enablePage(this.stateName, this.pageProps.dispatch);
    }
    disablePage = () => {
        return disablePage(this.stateName, this.pageProps.dispatch);
    }
    isElementShown = (parameters) => {
        return isElementShown(this.pageProps, parameters);
    }
    hideElement = (parameters) => {
        return hideElement(this.stateName, this.pageProps.dispatch, parameters);
    }
    showElement = (parameters) => {
        return showElement(this.stateName, this.pageProps.dispatch, parameters);
    }
    setComponentValue = (parameters) => {
        return setComponentValue(this.stateName, this.pageProps, parameters);
    }
    setComponentFocus = (parameters) => {
        return setComponentFocus(this.stateName, this.pageProps, parameters);
    }
    setComponentElAttr = (parameters) => {
        return setComponentElAttr(this.stateName, this.pageProps, parameters);
    }
    setComponentAttr = (parameters) => {
        return setComponentAttr(this.stateName, this.pageProps, parameters);
    }
    openLink = (parameters) => {
        return openLink(parameters);
    }
    getComponent = (parameters) => {
        return getComponent(this.pageProps, parameters);
    }
    componentValue = (parameters) => {
        return componentValue(this.pageProps, parameters);
    }
    componentElAttr = (parameters) => {
        return componentElAttr(this.stateName, this.pageProps, parameters);
    }
    componentAttr = (parameters) => {
        return componentAttr(this.stateName, this.pageProps, parameters);
    }
    objectAttr = (parameters) => {
        return objectAttr(parameters);
    }
    setObjectAttr = (parameters) => {
        return setObjectAttr(parameters);
    }
    toArray = (parameters) => {
        return toArray(parameters);
    }
    toObject = (parameters) => {
        return toObject(parameters);
    }
    setTimeout = (parameters) => {
        return setTimeout(parameters);
    }
    setInterval = (parameters) => {
        return setInterval(parameters);
    }
    callback = (parameters) => {
        return callback(parameters);
    }
    bbCodeToCanvas = (parameters) => {
        return bbCodeToCanvas(parameters); // Async - Await ~ Return promise
    }
    bbCodeToCanvasSync = (parameters) => {
        return bbCodeToCanvasSync(parameters); // Async - Await ~ Return promise
    }
    inactivityTimeout = (parameters) => {
        return inactivityTimeout(this.pageProps.route, parameters);
    }
    console = (parameters) => {
        return console(parameters);
    }
    setComboOptions = (parameters) => {
        return setComboOptions(this.stateName, this.pageProps, parameters);
    }
    languageConvertData = (parameters) => {
        return languageConvertData(parameters); // Async - Await ~ Return promise
    }
    getLanguage = () => {
        return getLanguage(); // Async - Await ~ Return promise
    }
    getLanguageList = (parameters) => {
        return getLanguageList(parameters);
    }
    setLanguage = (parameters) => {
        return setLanguage(parameters); // Async - Await ~ Return promise
    }

    // Dataset
    selectAll = (parameters) => {
        return selectAll(parameters); // Async - Await ~ Return promise
    }
    selectBy = (parameters) => {
        return selectBy(parameters); // Async - Await ~ Return promise
    }
    updateBy = (parameters) => {
        return updateBy(parameters); // Async - Await ~ Return promise
    }
    deleteBy = (parameters) => {
        return deleteBy(parameters); // Async - Await ~ Return promise
    }
    insert = (parameters) => {
        return insert(parameters); // Async - Await ~ Return promise
    }
    dataFromString = (parameters) => {
        return dataFromString(parameters); // Async - Await ~ Return promise
    }
    clearData = (parameters) => {
        return clearData(parameters); // Async - Await ~ Return promise
    }
    loadData = (parameters) => {
		return loadData(this.stateName, this.pageProps, parameters, this.pageActions);  // Async - Await ~ Return promise
	}
    loadNext = (parameters) => {
        return loadNext(parameters); // Async - Await ~ Return promise
    }
    navCall = (parameters) => {
        return navCall(parameters);
    }
    selectByMulti = (parameters) => {
        return selectByMulti(parameters); // Async - Await ~ Return promise
    }
    deleteByMulti = (parameters) => {
        return deleteByMulti(parameters); // Async - Await ~ Return promise
    }
    updateByMulti = (parameters) => {
        return updateByMulti(parameters); // Async - Await ~ Return promise
    }
    rawCall = (parameters) => {
        return rawCall(parameters);
    }

    // Device
    keyboardHide = () => {
        return keyboardHide();
    }
    onBackButton = (parameters) => {
        return onBackButton(parameters);
    }
    exitApp = () => {
        return exitApp();
    }
    deviceId = () => {
        return deviceId();
    }
    deviceName = () => {
        return deviceName();
    }
    deviceSerial = () => {
        return deviceSerial(); // Async - Await ~ Return promise
    }
    deviceOS = () => {
        return deviceOS();
    }
    deviceOSVersion = () => {
        return deviceOSVersion();
    }
    deviceManufacturer = () => {
        return deviceManufacturer(); // Async - Await ~ Return promise
    }
    deviceHasTouchSensor = (parameters) => {
        return deviceHasTouchSensor(parameters);
    }
    deviceVerifyTouchSensor = (parameters) => {
        return deviceVerifyTouchSensor(parameters);
    }

    // Printer
    btPrinterPortList = (parameters) => {
        return btPrinterPortList(parameters);
    }
    btPrinterConnect = (parameters) => {
        return btPrinterConnect(parameters);
    }
    btPrinterPrint = (parameters) => {
        return btPrinterPrint(parameters);
    }
    canvasToCPCL = (parameters) => {
        return canvasToCPCL(parameters);
    }

    // Not Finished
    captureImage = () => {
        return captureImage(this.pageProps.navigation);
    }

    // Global Functions
     // User Defined - Global Functions
	userDefined = (name, parameters) => {
		// Find which user defined function to trigger
		switch(name) {
			case globalModalLoadingName:
				return globalModalLoading(this, parameters);
			case globalCalculateTotalName:
				return globalCalculateTotal(this, parameters);
			case globalModalQuestionName:
				return globalModalQuestion(this, parameters);
			case globalModalHideName:
				return globalModalHide(this, parameters);
			case globalCSSName:
				return globalCSS(this, parameters);
			case LoginAsName:
				return LoginAs(this, parameters);
			case globalModalInfoName:
				return globalModalInfo(this, parameters);
			case globalLogoutName:
				return globalLogout(this, parameters);
            case updateLabelName:
                return updateLabel(this, parameters);
		}
	}

    // Functions used within the framework itself
    // Core - Dataset 
    coreDatasetLoadData = (parameters) => {
        return coreDatasetLoadData(this.stateName, this.pageProps, parameters); // Async - Await ~ Return promise
    }
    // Core - State 
    coreStateSetProperties = (parameters) => {
        return coreStateSetProperties(this.stateName, this.pageProps, parameters);
    }
    coreStateSetElementAttributes = (parameters) => {
        return coreStateSetElementAttributes(this.stateName, this.pageProps, parameters);
    }
    coreStateInitializePage = (parameters) => {
        return coreStateInitializePage(this.stateName, this.pageProps, parameters, this.pageActions);
    }

}
  
export default Function;