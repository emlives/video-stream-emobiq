import { Image } from 'react-native-canvas';

/**
 * Convert the text into a canvas.
 * @param {canvas} canvas
 * @param {object} parameters
 *                  - text {string} - the text to be converted
 *                  - font {string} - the size to be used
 *                  - size {int} - the font size to be used
 *                  - canvasWidth {int} - the canvas width to use
 *                  - marginTop {int} - Add this margin to the top of canvas
 *                  - marginLeft {int} - Add this margin to the left of canvas
 *                  - marginRight {int} - Add this margin to the right of canvas
 *                  - marginBottom {int} - Add this margin to the bottom of canvas
 * 
 * @todo Messy and slow, like it is rendering twice, so need to redo the flow and simplify it.
 * @return {canvas}
 */
export const canvasFromText = async (canvas, parameters) => {
    // A variable used in legacy code, probably to adjust the pixel in mobile devices
    let fx = 1;

    // Prepare the default values if not passed
    parameters.font = parameters.font || 'monospace';
    parameters.size = parameters.size || '23px';
    parameters.marginTop = parameters.marginTop || 0;
    parameters.marginLeft = parameters.marginLeft || 0;
    parameters.marginRight = parameters.marginRight || 0;
    parameters.marginBottom = parameters.marginBottom || 0;
    parameters.listImage = [];
    parameters.canvasWidth = parameters.canvasWidth || 576;

    // Set the canvas width
    canvas.width = parameters.canvasWidth;

    // Get the canvas context
    let context = canvas.getContext('2d');

    // Clear the canvas first
    context.clearRect(0, 0, canvas.width, canvas.height);

    // Process the desired canvas
    let myText = new CanvasText({
        x: parameters.marginLeft,
        y: parameters.marginTop + 24 + 6
    }, {
        width: (canvas.width - parameters.marginRight),
        height: canvas.height - parameters.marginBottom
    }, canvas, context, fx);

    let style = {
        bold: false,
        italic: false
    };

    let decoration = {
        underline: false,
        strikethrough: false
    };

    function setStyle(k, v) {
        style[k] = v;
        return getStyle();
    }

    function getStyle() {
        parameters.text = '';
        parameters.text += style.bold ? 'bold' : '';
        parameters.text += style.italic ? ' italic' : '';
        parameters.text = parameters.text.replace(/^\s/, '');
        return parameters.text ? parameters.text : 'normal';
    }

    function setDecoration(k, v) {
        decoration[k] = v;
        return getDecoration();
    }

    function getDecoration() {
        parameters.text = '';
        parameters.text += decoration.underline ? 'underline' : '';
        parameters.text += decoration.strikethrough ? ' strikethrough' : '';
        parameters.text = parameters.text.replace(/^\s/, '');
        return parameters.text ? parameters.text : '';
    }

    const preImage = (sources) => new Promise((resolve) => {
        let images = [];
        let loadedImages = 0;
        let numImages = 0;

        for (let src in sources) {
            numImages++;
        }
        for (let src in sources) {
            images[src] = new Image(canvas);
            images[src].crossOrigin = 'Anonymous';
            images[src].addEventListener('load', async () => {
                if (++loadedImages >= numImages) {
                    resolve(images)
                }
            });
            images[src].src = sources[src];
        }
    })

    let _tg = [];
    parameters.text.replace(/\[\/?[\w\=\"|'-]+\]/g, function (x) {
        _tg.push(x)
        return '';
    });

    let img = parameters.text;
    let spl = parameters.text.split(/\[\/?[\w\=\"|'-]+\]/g);
    myText.family(parameters.font).size(parameters.size);

    let renderImage = false;
    for (let i = 0; i < spl.length; i++) {
        // Seperate the text by new lines
        if (spl[i]) {
            if (spl[i] != " ") {
                let xi = spl[i].split('\n');
                for (let ix = 0; ix < xi.length; ix++) {
                    myText.append(xi[ix]);
                    if (ix < xi.length - 1) {
                        myText.append('\n');
                    }
                }
            }
        };

        // Update the font options
        if (_tg[i]) {
            if (_tg[i].match(/\[b/)) {
                myText.style(setStyle('bold', true));
            } else if (_tg[i].match(/\[\/b/)) {
                myText.style(setStyle('bold', false));
            } else if (_tg[i].match(/\[img/)) {
                myText.image(true);
                renderImage = true;
            } else if (_tg[i].match(/\[\/img/)) {
                myText.image(false);
            } else if (_tg[i].match(/\[i/)) {
                myText.style(setStyle('italic', true));
            } else if (_tg[i].match(/\[\/i/)) {
                myText.style(setStyle('italic', false));
            } else if (_tg[i].match(/\[font=/)) {
                let fam = _tg[i].split(/\[font="|'/);
                myText.family(fam[1]);
            } else if (_tg[i].match(/\[\/font/)) {
                myText.family(parameters.font);
            } else if (_tg[i].match(/\[size=/)) {
                let sz = _tg[i].split(/\[size="|'/);
                myText.size(sz[1]);
            } else if (_tg[i].match(/\[\/size/)) {
                myText.size(parameters.size);
            } else if (_tg[i].match(/\[linespace=/)) {
                let sz = _tg[i].split(/\[linespace="|'/);
                myText.spacing(sz[1]);
            } else if (_tg[i].match(/\[\/linespace/)) {
                myText.spacing(0);
            } else if (_tg[i].match(/\[alignment=/)) {
                let sz = _tg[i].split(/\[alignment="|'/);
                myText.alignment(sz[1].toLowerCase());
            } else if (_tg[i].match(/\[\/alignment/)) {
                myText.alignment('left');
            } else if (_tg[i].match(/\[u/)) {
                myText.decoration(setDecoration('underline', true));
            } else if (_tg[i].match(/\[\/u/)) {
                myText.decoration(setDecoration('underline', false));
            } else if (_tg[i].match(/\[s/)) {
                myText.decoration(setDecoration('strikethrough', true));
            } else if (_tg[i].match(/\[\/s/)) {
                myText.decoration(setDecoration('strikethrough', false));
            }
        }
    }

    await myText.render();
    canvas.width = parameters.canvasWidth * fx;
    canvas.height = (parameters.marginTop + myText._size.height + parameters.marginBottom) * fx;
    await myText.render();

    // Need to render image
    if (renderImage) {
        let listImage = img.match(/\[img(.*?)\[\/img]/g)
        let sources = [];
        parameters.listImage = [];

        let tmpImage = {};
        listImage.map(function (imgs) {
            let attribute = {};
            let imageAttr = imgs.match(/(sx|sy|swidth|sheight|x|y|width|height)=(\d*)/g);
            let imageData = imgs.replace(/(\[.*?])/g, '');
            if (imageAttr != null) {
                for (let i = 0; i < imageAttr.length; i++) {
                    let imgRes = imageAttr[i].split('=');
                    attribute[imgRes[0]] = parseInt(imgRes[1]);
                }
                tmpImage[imageData] = attribute;
            }
            parameters.listImage.push(imageData);
            sources.push(imageData);
        });

        let images = await preImage(sources)
        for (let i = 0; i < images.length; i++) {
            let imgResult = images[i];
            if (Object.keys(tmpImage).length === 0 && tmpImage.constructor === Object) {
                let x = 0;
                let y = 0;
                context.drawImage(imgResult, x, y, imgResult.width, imgResult.height);
            } else {
                let imageWithAttr = tmpImage[imgResult.src];
                if (imageWithAttr !== undefined) {
                    let x = (imageWithAttr.x === undefined ? 0 : imageWithAttr.x);
                    let y = (imageWithAttr.y === undefined ? 0 : imageWithAttr.y);

                    if (imageWithAttr.width !== undefined &&
                        imageWithAttr.height !== undefined
                    ) {
                        context.drawImage(
                            imgResult,
                            x,
                            y,
                            imageWithAttr.width,
                            imageWithAttr.height
                        );
                    } else if (imageWithAttr.sx !== undefined &&
                        imageWithAttr.sy !== undefined &&
                        imageWithAttr.swidth !== undefined &&
                        imageWithAttr.sheight !== undefined &&
                        imageWithAttr.width !== undefined &&
                        imageWithAttr.height !== undefined
                    ) {
                        context.drawImage(
                            imgResult,
                            imageWithAttr.sx,
                            imageWithAttr.sy,
                            imageWithAttr.swidth,
                            imageWithAttr.sheight,
                            x,
                            y,
                            imageWithAttr.width,
                            imageWithAttr.height
                        );
                    } else {
                        context.drawImage(imgResult, x, y, imgResult.width, imgResult.height);
                    }
                } else {
                    context.drawImage(imgResult, 0, 0, imgResult.width, imgResult.height);
                }
            }
        }
    }

    return canvas;
}

/**
 * A class that handles text and convert it into a
 * proper canvas image.
 */
class CanvasText {
    constructor(position = { x: 0, y: 0 }, size = { width: 100, height: 100 }, canvas = null, context = null, fx = 1) {
        this._position = position;
        this._size = size;
        this._textStack = [];
        this._currentOptionSet = null;
        this._canvas = canvas
        this._context = context
        this._fx = fx
    }

    _newOptionSet = () => {
        if (this._currentOptionSet != null) {
            return;
        }
        this._currentOptionSet = {
            text: '',
            family: '',
            size: '',
            style: '',
            decoration: '',
            image: '',
            spacing: 0,
            alignment: ''
        }
    }
    position = (x, y) => {
        this._position.x = x;
        this._position.y = y;
    }
    family = (family) => {
        this._newOptionSet();
        this._currentOptionSet.family = family;
        return this;
    }
    size = (size) => {
        this._newOptionSet();
        this._currentOptionSet.size = size;
        return this;
    }
    style = (style) => {
        this._newOptionSet();
        this._currentOptionSet.style = style;
        return this;
    }
    image = (image) => {
        this._newOptionSet();
        this._currentOptionSet.image = image;
        return this;
    }
    spacing = (spacing) => {
        this._newOptionSet();
        this._currentOptionSet.spacing = spacing;
        return this;
    }
    alignment = (alignment) => {
        this._newOptionSet();
        this._currentOptionSet.alignment = alignment;
        return this;
    }
    decoration = (decoration) => {
        this._newOptionSet();
        this._currentOptionSet.decoration = decoration;
        return this;
    }
    append = (text) => {
        this._newOptionSet();
        this._currentOptionSet.text = text;
        if (this._currentOptionSet.image) {
            this._currentOptionSet.text = '';
        }
        this._textStack.push(this._currentOptionSet);
        this._currentOptionSet = JSON.parse(JSON.stringify(this._currentOptionSet));
        return this;
    }
    newLine = () => {
        this.append('\n');
        return this;
    }
    render = async () => {
        if (this._textStack.length == 0) {
            return;
        }
        let previousFontOptions = {
            text: '',
            family: '',
            size: '',
            style: '',
            decoration: '',
            spacing: 0,
            alignment: ''
        };
        let textAdjustment = {
            xLeft: 0,
            xRight: 0,
            y: 0
        };
        let canvas = this._canvas
        let context = this._context
        let fx = this._fx;
        context.save();
        context.fillStyle = "#FFFFFF";
        context.fillRect(0, 0, canvas.width, canvas.height);
        for (let i = 0, textStackLen = this._textStack.length; i < textStackLen; i++) {

            // Set the properties of the text to be displayed
            let currentFontOptions = this._textStack[i];
            if (currentFontOptions.family) {
                previousFontOptions.family = currentFontOptions.family;
            }
            if (currentFontOptions.size) {
                previousFontOptions.size = currentFontOptions.size;
            }
            if (currentFontOptions.style) {
                previousFontOptions.style = currentFontOptions.style;
            }

            context.font = previousFontOptions.style.trim() + ' ' + previousFontOptions.size.trim() + ' ' + previousFontOptions.family.trim();

            if (currentFontOptions.decoration) {
                previousFontOptions.decoration = currentFontOptions.decoration;
            }
            if (currentFontOptions.spacing) {
                previousFontOptions.spacing = currentFontOptions.spacing;
            }
            if (currentFontOptions.alignment) {
                previousFontOptions.alignment = currentFontOptions.alignment;
            }

            // Start rendering the words
            let textToDraw = currentFontOptions.text;
            let wordsArray = textToDraw.split(' ');

            // Check if it's right aligned
            if (previousFontOptions.alignment == 'right') {
                wordsArray.reverse();
            }

            for (let j = 0, wordsArrayLen = wordsArray.length; j < wordsArrayLen; j++) {
                let currentWord = wordsArray[j];

                // Add space if it's not the first one
                if (j > 0) {
                    if (previousFontOptions.alignment == 'right') {
                        currentWord = currentWord + ' ';
                    } else {
                        currentWord = ' ' + currentWord;
                    }
                }

                let measureWord = await context.measureText(currentWord);
                let currentWordWidth = measureWord.width;

                if (textAdjustment.xLeft + textAdjustment.xRight + currentWordWidth > this._size.width || textToDraw == '\n') {
                    textAdjustment.xLeft = 0;
                    textAdjustment.xRight = 0;
                    textAdjustment.y += (parseInt(previousFontOptions.size) + (Number(previousFontOptions.spacing)) * fx); //parseInt(previousFontOptions.size, 15);
                }

                if (textToDraw == '\n') {
                    continue;
                }

                // Set the properties before displaying the text
                context.fillStyle = '#000';

                let xPosition = this._position.x + textAdjustment.xLeft;
                context.textAlign = 'start';

                if (previousFontOptions.alignment == 'right') {
                    xPosition = this._size.width - textAdjustment.xRight;
                    context.textAlign = 'end';
                }

                context.fillText(
                    currentWord, // text
                    xPosition,
                    this._position.y + textAdjustment.y // y position
                );

                if (currentFontOptions.decoration) {
                    let dc = currentFontOptions.decoration;
                    //let textWidth = context.measureText(currentFontOptions.text).width;
                    let textWidth = currentWordWidth;
                    let startX = 0;
                    let startY = '';

                    if (dc == "underline") {
                        startY = this._position.y + textAdjustment.y + (parseInt(previousFontOptions.size) / 10);
                    } else if (dc == "strikethrough") {
                        startY = this._position.y + textAdjustment.y - (parseInt(previousFontOptions.size) / 3);
                    }

                    let endX = 0;
                    let endY = startY;
                    let underlineHeight = parseInt(previousFontOptions.size) / 15;

                    if (underlineHeight < 1) {
                        underlineHeight = 1;
                    }
                    context.beginPath();
                    context.lineWidth = underlineHeight >= 1 ? underlineHeight : 1;

                    startX = (xPosition) * fx; //+ 0;//4;
                    endX = (xPosition + textWidth) * fx; //+ 0;//4;

                    if (previousFontOptions.alignment == 'right') {
                        endX = (xPosition - textWidth) * fx; //+ 0;//4;
                    }

                    context.lineWidth = underlineHeight;
                    context.moveTo(startX, startY);
                    context.lineTo(endX, endY);
                    context.stroke();
                }

                if (previousFontOptions.alignment == 'right') {
                    textAdjustment.xRight += currentWordWidth;
                } else {
                    textAdjustment.xLeft += currentWordWidth;
                }
            }
        }
        this._size.height = (textAdjustment.y + 50 + fx);
        context.restore();
    }
}