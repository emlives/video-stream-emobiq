import 'react-native-get-random-values';
import { nanoid } from 'nanoid';
import { Image as ReactNativeImage } from 'react-native';

/**
 * Validate the callback passed and trigger it
 * if it is a valid one.
 * @param {function} callback - The callback to be triggered
 * @param {any} data - The result data to be passed in the callback
 * @param {any} extra - The extra data to be passed in the callback
 */
export const callback = (callback, data, extra = null) => {
    // Check if any callback must be called and valid one
    if (callback && typeof (callback) === 'function') {
        callback(data, extra);
    }
}

/**
 * Validate the callback passed and trigger it
 * if it is a valid one.
 * @param {function} callback - The callback to be triggered
 * @param {any} data - The result data to be passed in the callback
 * @param {any} extra - The extra data to be passed in the callback
 */
 export const callbackAsync = async (callback, data, extra = null) => {
    // Check if any callback must be called and valid one
    if (callback && typeof (callback) === 'function') {
        await callback(data, extra);
    }
}

/**
 * Encode the passed object to a valid string form body
 * for web service http calls.
 * 
 * @param {object} object - The object to be encoded
 * 
 * @return {string}
 */
export const encodeRequestParameterForm = (object) => {
    let body = [];

    // Go through all the object
    for (var key in object) {
        // Store the value
        let value = object[key];

        // Check if value is undefined then skip
        if (typeof value === 'undefined') {
            continue;
        }

        // Check if the value is object then try to stringify
        if (typeof value === 'object') {
            value = JSON.stringify(value);
        }

        // Encode both key and value
        var encodedKey = encodeURIComponent(key);
        var encodedValue = encodeURIComponent(value);

        // Push in the body array
        body.push(encodedKey + '=' + encodedValue);
    }

    // Join the body to form a string
    let result = body.join('&');

    return result;
}

/**
 * Encode the passed object to a valid string form body
 * for web service http calls.
 * @param {string} xml - The xml to encode
 * 
 * @return {string}
 */
export const encodeXMLValue = (xml) => {
    // Skip if it's not a string
    if (typeof xml !== 'string') {
        return xml;
    }

    return xml.replace(/[<>&'"]/g, function (c) {
        switch (c) {
            case '<': return '&lt;';
            case '>': return '&gt;';
            case '&': return '&amp;';
            case '\'': return '&apos;';
            case '"': return '&quot;';
        }
    });
}

/**
 * Converts xml to json object
 * @param {string} xml - The xml to encode to json
 * 
 * @return {object}
 */
export const convertXMLtoJSON = (xml) => {
    // Make sure it is a proper xml object
    if (typeof xml === 'string') {
        // Convert string to xml
        window.DOMParser = require('xmldom').DOMParser;
        var parser = new DOMParser();
        xml = parser.parseFromString(xml, 'text/xml');
    }

    // Convert the xml to json
    return xmlToJson(xml);
}

/**
 * Originally from http://davidwalsh.name/convert-xml-json
 * This is a version that provides a JSON object without the attributes and places textNodes as values
 * rather than an object with the textNode in it.
 * 27/11/2012
 * Ben Chidgey
 *
 * @param {string} xml - the string to be encoded into xml
 * @return {*}
 */
export const xmlToJson = (xml) => {

    // Create the return object
    var obj = {};

    // Text node
    if (4 === xml.nodeType) {
        obj = xml.nodeValue;
    }

    if (xml.hasChildNodes()) {
        for (var i = 0; i < xml.childNodes.length; i++) {
            var TEXT_NODE_TYPE_NAME = '#text',
                item = xml.childNodes.item(i),
                nodeName = item.nodeName,
                content;
            if (TEXT_NODE_TYPE_NAME === nodeName) {
                //single textNode or next sibling has a different name
                if ((null === xml.nextSibling) || (xml.localName !== xml.nextSibling.localName)) {
                    content = xml.textContent;

                    //we have a sibling with the same name
                } else if (xml.localName === xml.nextSibling.localName) {
                    //if it is the first node of its parents childNodes, send it back as an array
                    content = (xml.parentNode.childNodes[0] === xml) ? [xml.textContent] : xml.textContent;
                }
                return content;
            } else {
                if ('undefined' === typeof (obj[nodeName])) {
                    obj[nodeName] = xmlToJson(item);
                } else {
                    // Check if it's an array
                    if (!Array.isArray(obj[nodeName])) {
                        var old = obj[nodeName];
                        obj[nodeName] = [];
                        obj[nodeName].push(old);
                    }
                    // recursive
                    obj[nodeName].push(xmlToJson(item));
                }
            }
        }
    }

    // Check if it's empty
    // Cause it's returning {} instead of blank
    if (Object.keys(obj).length === 0 && obj.constructor === Object) {
        obj = '';
    }

    return obj;
}

/**
 * Get the radius for the lat/long
 * @param {*} degree 
 */
export const degreeToRadius = (degree) => {
    return degree * (Math.PI / 180);
}

/**
 * Check if string is number
 * 
 * @param {string} - the string to check if isNumber
 * @return {boolean} 
 */
export const isNumber = (n) => {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

/**
 * Add capitalize methods to String object
 */
String.prototype.capitalize = function () {
    return this.replace(/(^|\s)([a-z])/g, function (m, p1, p2) {
        return p1 + p2.toUpperCase();
    });
};

/**
 * Pad the number passed with zeros in front
 * e.g. pad('8', 3) returns '008'
 * 
 * @param {num} any - the number to pad with zeros
 * @param {size} number - to return size
 */
function pad(num, size) {
    if (isNaN(size)) {
        return false;
    }

    var s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
}

/**
 * Check if input is a String
 * 
 * @param {any} myVar - first value
 */
export const isString = (myVar) => {
    if (typeof myVar === 'string' || myVar instanceof String)
        return true
    else
        return false
}

/**
 * Increments the date passed
 * 
 * @param {add} date - first value
 * @param {date} date - second value
 * @param {string} type - third value
 */
export const addDate = (add, date, type) => {
    if (isNaN(add)) {
        return false
    }

    // Prepare the needed information, default type to 'days'
    type = type || 'days';
    if (!date) {
        date = new Date();
    }

    // Prepare the time to add to the date
    switch (type.toLowerCase()) {
        case 'hour':
        case 'hours':
            add = (add * 60 * 60 * 1000);
            break;
        case 'minute':
        case 'minutes':
            add = (add * 60 * 1000);
            break;
        case 'second':
        case 'seconds':
            add = (add * 1000);
            break;
        default: // days
            add = (add * 24 * 60 * 60 * 1000);
    }

    // Add the time
    date.setTime(date.getTime() + add);

    return date;
}

/**
 * Returns the date as string with option to display time
 * 
 * @param {date} date - Will return current date if null
 * @param {boolean} wTime - Set to true to display date with time
 */
export const dbDate = (date, wTime) => {
    wTime = wTime || false;
    if (!date) {
        date = new Date();
    }
    var result = date.getFullYear() +
        '-' + pad(date.getMonth() + 1, 2) +
        '-' + pad(date.getDate(), 2);
    if (wTime) {
        result += ' ' + pad(date.getHours(), 2) +
            ':' + pad(date.getMinutes(), 2) +
            ':' + pad(date.getSeconds(), 2);
    }
    return result;
}

/**
 * Create a padding function for string
 */
export const stringPad = (value, char, type, length) => {
    // Create a string repeater function
    function stringRepeater(char, length) {
        var value = '';
        for (var i = 0; i < length; i++) {
            value += char;
        }
        return value;
    }

    // Prepare the default values
    value = value || '';
    value = value.toString();
    char = char || ' ';
    type = type || 'right';
    length = length || 0;
    length = parseInt(length);

    // Skip this function if negative length was passed
    if (length <= 0) {
        return value;
    }

    // Prepare the value
    var result = [];
    var data = [];

    // Seperate the data by new line first
    data = value.split('\n');

    // Go through the data first
    for (var i in data) {
        var record = data[i];

        // Reitirate the process to handle longer padding length
        var finished = false;
        var includeNewLine = false;
        do {
            // Prepare the value to be appended
            var finalValue = record;

            // Check if it's the last value
            if (record.length <= length) {
                // Complete the process
                finished = true;
            }

            // Check if the string is more than the padding length
            if (record.length > length) {
                // Add new line whenever it's stored
                includeNewLine = true;

                // Get the value that is within the length
                finalValue = record.substr(0, length);

                // Check if there are words that can be seperated
                var spaceIndex = finalValue.lastIndexOf(' '),
                    extraSpace = 0;
                if (spaceIndex >= 0) {
                    // Seperate the space
                    finalValue = record.substring(0, spaceIndex);
                    extraSpace++;
                }

                // Update the record data
                record = record.substr(finalValue.length + extraSpace);
            }

            // Get the pad length
            var padLength = (length - finalValue.length);

            // Generate the string based on the type (position)
            if (type == 'left') {
                result.push(finalValue + stringRepeater(char, padLength));
            } else if (type == 'center') {
                var padLengthHalf = Math.floor(padLength / 2);
                result.push(stringRepeater(char, padLengthHalf) + finalValue + stringRepeater(char, padLength - padLengthHalf));
            } else {
                result.push(stringRepeater(char, padLength) + finalValue);
            }

            // Check if need to add new line
            if (includeNewLine && !finished) {
                result.push('\n');
            }
        } while (finished != true);
    }

    // Return the result
    return result.join('');
};

/*
 * Converts a string into a date object
 * 
 * @param {date} date - Will return current date if null
 * @param {boolean} wTime - Set to true to display date with time
 * 
 * @return {date} - date result
 */
export const stringToDate = (dateString) => {
    if (!dateString || typeof dateString == 'undefined') {
        return new Date();
    }
    dateString = dateString.replace(/[Tt]/, ' ').trim();
    var regex = /((\d{4})-(\d{1,2})-(\d{1,2}))?(\s*)((\d{1,2}):?(\d{1,2})?:?(\d{1,2})?)?/;
    var currentDate = new Date();
    var dateResult = [currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate(), 0, 0, 0];
    var regexResult = dateString.match(regex);
    if (regexResult && regexResult.length) {
        if (regexResult[1]) {
            dateResult.splice(0, 3, regexResult[2], regexResult[3], regexResult[4]);
        }
        if (regexResult[6]) {
            dateResult[3] = regexResult[7];
            if (regexResult[8]) {
                dateResult[4] = regexResult[8];
            }
            if (regexResult[9]) {
                dateResult[5] = regexResult[9];
            }
        }
    }
    return new Date(dateResult[0], dateResult[1] - 1, dateResult[2], dateResult[3], dateResult[4], dateResult[5]);
}

/**
 * Sort the data
 * @param {array} order - order {array of objects}
 *                         - f|field : the field to base the sorting on
 *                         - v|order : asc or desc
 * @param {array} data   - data to be filtered
 * 
 * @return {array} - filtered data
 */
export const sortData = (order, data) => {
    var result = data;

    // Sort the data
    result.sort(
        function (a, b) {
            // Holder for the expression
            var expression = "";

            // Loop through all of the sorting data
            for (var key in order) {
                var sortData = order[key];

                // Make sure the sortData is not null
                if (sortData) {
                    // Set to case insensitive
                    var sortField1 = a[sortData.f] || a[sortData.field];
                    var sortField2 = b[sortData.f] || b[sortData.field];
                    var val1 = (sortField1 ? sortField1.toString().toUpperCase() : '');
                    var val2 = (sortField2 ? sortField2.toString().toUpperCase() : '');

                    // Convert to date if it's a valid date
                    if (isNaN(val1) && !isNaN(Date.parse(val1.replace(/([a-z])\W+(\d)/ig, "$1$2")))) {
                        val1 = Date.parse(val1).toString();
                    }
                    if (isNaN(val2) && !isNaN(Date.parse(val2.replace(/([a-z])\W+(\d)/ig, "$1$2")))) {
                        val2 = Date.parse(val2).toString();
                    }

                    // Regex for alphanumeric sorting
                    var reA = /[^a-zA-Z]/g;
                    var reN = /[^0-9|^.|^-]/g;

                    // Aplhanumeric Sorting
                    var aA = val1.replace(reA, "");
                    var bA = val2.replace(reA, "");

                    var sortOrder = sortData.v || sortData.order;
                    sortOrder = sortOrder.toUpperCase();
                    if (aA === bA) {
                        var aN = parseFloat(val1.replace(reN, ""), 10);
                        var bN = parseFloat(val2.replace(reN, ""), 10);
                        expression = expression + (aN === bN ? 0 : aN > bN ? (sortOrder == 'DESC' ? -1 : 1) : (sortOrder == 'DESC' ? 1 : -1)).toString() + " || ";
                    } else {
                        expression = expression + (val1 === val2 ? 0 : val1 > val2 ? (sortOrder == 'DESC' ? -1 : 1) : (sortOrder == 'DESC' ? 1 : -1)).toString() + " || ";
                    }
                }
            }

            // Evaluate the combined expression
            return eval(expression.substr(0, expression.length - 4));
        }
    );

    return result;
}

/**
 * Filter the data
 * @param {object} parameters 
 *                   - filter {array of objects}
 *                      - f|field : the field to base the sorting on
 *                      - o|operator : operator of the comparison
 *                      - v|value : value to be compared against the data
 *                   - orFilter {array of objects}
 *                      - f|field : the field to base the sorting on
 *                      - o|operator : operator of the comparison
 *                      - v|value : value to be compared against the data
 * @param {array} data - data to be filtered
 * 
 * @return {array} - filtered data
 */
export const filterData = (parameters, data) => {
    var result = [];
    var filter = [];
    var orFilter = [];

    // Filter for 'and'
    if (parameters.filter) {
        filter = processFilterData(parameters.filter);
    }

    // Filter for 'or'
    if (parameters.orFilter) {
        orFilter = processFilterData(parameters.orFilter);
    }

    // Filter the data
    result = data.filter(
        // Loop through the record
        function (record) {
            var valid = true;

            // Filter for 'and'
            if (filter.length > 0) {
                // Loop through all of the filters
                for (var i = 0; i < filter.length; i++) {
                    // Check if the data is accepted in the filter criteria
                    valid = filterRecord(record, filter[i]);

                    // If already false then stop the loop
                    if (!valid) {
                        return false;
                    }
                }
            }

            // Filter for 'or' if 'and' filter is ok
            if (valid) {
                if (orFilter.length > 0) {
                    // Loop through all of the filters
                    var orValid = false;
                    for (var i = 0; i < orFilter.length; i++) {
                        // Check if the data is accepted in the filter criteria
                        if (filterRecord(record, orFilter[i])) {
                            orValid = true;
                            // Skip already since true
                            continue;
                        }
                    }

                    if (!orValid) {
                        valid = false;
                    }
                }
            }

            return valid;
        }
    );

    return result;
}

/**
 * Update the filtered data from the list
 * @param {object} parameters 
 *                         - filter {array of objects}
 *                           - f|field : the field to base the sorting on
 *                           - o|operator : operator of the comparison
 *                           - v|value : value to be compared against the data
 *                         - orFilter {array of objects}
 *                           - f|field : the field to base the sorting on
 *                           - o|operator : operator of the comparison
 *                           - v|value : value to be compared against the data
 * @param {array} data     - data to be filtered
 * @param {object} value   - values to be updated
 * @param {boolean} first  - identify whether only to remove the first matched data
 * 
 * @return {object} - contains the result nededed 
 *                   processed - processed data
 *                   updated - updated data only
 */
export const filterUpdateData = (parameters, data, value, first = false) => {
    var updated = [];
    let filter = [];
    let orFilter = [];

    // Filter for 'and'
    if (parameters.filter) {
        filter = processFilterData(parameters.filter);
    }

    // Filter for 'or'
    if (parameters.orFilter) {
        orFilter = processFilterData(parameters.orFilter);
    }

    // Loop through the data 
    for (let key in data) {
        // Process the record
        let record = data[key];

        var valid = true;

        // Filter for 'and'
        if (filter.length > 0) {
            // Loop through all of the filters
            for (var i = 0; i < filter.length; i++) {
                // Check if the data is accepted in the filter criteria
                valid = filterRecord(record, filter[i]);

                // If already false then stop the loop
                if (!valid) {
                    // continue;
                    break
                }
            }
        }

        // Filter for 'or' if 'and' filter is ok
        if (valid) {
            if (orFilter.length > 0) {
                // Loop through all of the filters
                var orValid = false;
                for (var i = 0; i < orFilter.length; i++) {
                    // Check if the data is accepted in the filter criteria
                    if (filterRecord(record, orFilter[i])) {
                        orValid = true;
                        // Skip already since true
                        // continue;
                        break
                    }
                }

                if (!orValid) {
                    valid = false;
                }
            }
        }

        // Update the data if necessary
        if (valid) {
            // Update the data
            Object.assign(record, value);
            // Store the updated data
            updated.push(record);
            // Check if only process the first found record
            if (first) {
                break;
            }
        }
    }

    // Prepare the result
    let result = {
        processed: data,
        updated: updated
    }

    return result;
}

/**
 * Remove the filtered data from the list
 * return both the new list and removed data
 * @param {object} parameters 
 *                         - filter {array of objects}
 *                           - f|field : the field to base the sorting on
 *                           - o|operator : operator of the comparison
 *                           - v|value : value to be compared against the data
 *                         - orFilter {array of objects}
 *                           - f|field : the field to base the sorting on
 *                           - o|operator : operator of the comparison
 *                           - v|value : value to be compared against the data
 * @param {array} data     - data to be filtered
 * @param {boolean} first  - identify whether only to remove the first matched data
 * 
 * @return {object} - contains the result nededed 
 *                   processed - cleaned up data
 *                   removed - removed data
 */
export const filterRemoveData = (parameters, data, first = false) => {
    var filter = [];
    var orFilter = [];

    // Filter for 'and'
    if (parameters.filter) {
        filter = processFilterData(parameters.filter);
    }

    // Filter for 'or'
    if (parameters.orFilter) {
        orFilter = processFilterData(parameters.orFilter);
    }

    // Prepare the variables needed to process
    var processed = [];
    var removed = [];
    var validate = true;
    processed = data.filter(
        // Loop through the record
        function (record) {
            // Check if need to still validate
            if (!validate) {
                return true;
            }

            var valid = true;

            // Filter for 'and'
            if (filter.length > 0) {
                // Loop through all of the filters
                for (var i = 0; i < filter.length; i++) {
                    // Check if the data is accepted in the filter criteria
                    valid = filterRecord(record, filter[i]);

                    // If already false then stop the loop
                    if (!valid) {
                        return true;
                    }
                }
            }

            // Filter for 'or' if 'and' filter is ok
            if (valid) {
                if (orFilter.length > 0) {
                    // Loop through all of the filters
                    var orValid = false;
                    for (var i = 0; i < orFilter.length; i++) {
                        // Check if the data is accepted in the filter criteria
                        if (filterRecord(record, orFilter[i])) {
                            orValid = true;
                            // Skip already since true
                            continue;
                        }
                    }

                    if (!orValid) {
                        valid = false;
                    }
                }
            }

            // Check if it is valid
            if (valid) {
                // Store the removed data
                removed.push(record);
                // First record only
                validate = (first ? false : true);
            }

            // Negate to remove the data that passed through the filter
            return !valid;
        }
    );

    // Prepare the result
    let result = {
        processed: processed,
        removed: removed
    }

    return result;
}

/**
 * Encode json
 * 
 * @param {object} object - data to be encoded
 */
export const serializeJSON = (object) => {
    var dataType = typeof (object);
    if (dataType == "undefined") {
        return "null";
    } else if (dataType == "number" || dataType == "boolean") {
        return object + "";
    } else if (object === null) {
        return "null";
    }
    if (dataType == "string") {
        return processEscapedCharacters(object);
    }
    if (dataType == 'object' && object.getFullYear) {
        return processDate(object);
    }
    if (dataType != "function" && typeof (object.length) == "number") {
        var result = [];
        for (var i = 0; i < object.length; i++) {
            let valueString = serializeJSON(object[i]);
            if (typeof (valueString) != "string") {
                valueString = "undefined";
            }
            result.push(valueString);
        }
        return "[" + result.join(",") + "]";
    }
    // it's a function with no adapter, bad
    if (dataType == "function") {
        return null;
    }
    result = [];
    for (var key in object) {
        var keyString;
        if (typeof (key) == "number") {
            keyString = '"' + key + '"';
        } else if (typeof (key) == "string") {
            keyString = processEscapedCharacters(key);
        } else {
            // skip non-string or number keys
            continue;
        }
        let valueString = serializeJSON(object[key]);
        if (typeof (valueString) != "string") {
            // skip non-serializable values
            continue;
        }
        result.push(keyString + ":" + valueString);
    }
    return "{" + result.join(",") + "}";
}

/**
 * replace escaped character with the proper one for encoding purposes
 * 
 * @param {string} value - data to be processed
 */
export const processEscapedCharacters = (value) => {
    return ('"' + value.replace(/(["\\])/g, '\\$1') + '"'
    ).replace(/[\f]/g, "\\f"
    ).replace(/[\b]/g, "\\b"
    ).replace(/[\n]/g, "\\n"
    ).replace(/[\t]/g, "\\t"
    ).replace(/[\r]/g, "\\r");
}

/**
 * return proper date string
 * 
 * @param {date} date - data to be processed
 */
export const processDate = (date) => {
    var year = date.getUTCFullYear();
    var dd = date.getUTCDate();
    var mm = date.getUTCMonth() + 1;

    var leadingZero = function (timeString) {
        if (timeString < 10) timeString = "0" + timeString;
        return timeString;
    }

    return '"' + year + '-' + mm + '-' + dd + 'T' + leadingZero(date.getUTCHours()) + ':' + leadingZero(date.getUTCMinutes()) + ':' + leadingZero(date.getUTCSeconds()) + '"';
}

/**
 * Change the format of a date
 * 
 * @param {string} format - preferred format
 * @param {date|string} timestamp - string or date to be converted
 * 
 * @return {date} - date result
 */
export const formatDate = (format, timestamp) => {
    let jsdate, formatCharacter
    let txtWords = [
        'Sun', 'Mon', 'Tues', 'Wednes', 'Thurs', 'Fri', 'Satur',
        'January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'
    ]
    // trailing backslash -> (dropped)
    // a backslash followed by any character (including backslash) -> the character
    // empty string -> empty string
    let regex = /\\?(.?)/gi
    let replaceCallback = function (matchedCharacter, s) {
        return formatCharacter[matchedCharacter] ? formatCharacter[matchedCharacter]() : s
    }
    let _pad = function (n, c) {
        n = String(n)
        while (n.length < c) {
            n = '0' + n
        }
        return n
    }
    // the properties of formatCharacter are each character in the format string (e.g. M-D-YYYY)
    formatCharacter = {
        // Get total days
        q: function () {
            return new Date(jsdate.getFullYear(), _pad(formatCharacter.n(), 2), 0).getDate();
        },
        // Day
        d: function () {
            // Day of month w/leading 0; 01..31
            return _pad(formatCharacter.j(), 2)
        },
        D: function () {
            // Shorthand day name; Mon...Sun
            return formatCharacter.l()
                .slice(0, 3)
        },
        j: function () {
            // Day of month; 1..31
            return jsdate.getDate()
        },
        l: function () {
            // Full day name; Monday...Sunday
            return txtWords[formatCharacter.w()] + 'day'
        },
        N: function () {
            // ISO-8601 day of week; 1[Mon]..7[Sun]
            return formatCharacter.w() || 7
        },
        S: function () {
            // Ordinal suffix for day of month; st, nd, rd, th
            let j = formatCharacter.j()
            let i = j % 10
            if (i <= 3 && parseInt((j % 100) / 10, 10) === 1) {
                i = 0
            }
            return ['st', 'nd', 'rd'][i - 1] || 'th'
        },
        w: function () {
            // Day of week; 0[Sun]..6[Sat]
            return jsdate.getDay()
        },
        z: function () {
            // Day of year; 0..365
            let a = new Date(formatCharacter.Y(), formatCharacter.n() - 1, formatCharacter.j())
            let b = new Date(formatCharacter.Y(), 0, 1)
            return Math.round((a - b) / 864e5)
        },

        // Week
        W: function () {
            // ISO-8601 week number
            let a = new Date(formatCharacter.Y(), formatCharacter.n() - 1, formatCharacter.j() - formatCharacter.N() + 3)
            let b = new Date(a.getFullYear(), 0, 4)
            return _pad(1 + Math.round((a - b) / 864e5 / 7), 2)
        },

        // Month
        F: function () {
            // Full month name; January...December
            return txtWords[6 + formatCharacter.n()]
        },
        m: function () {
            // Month w/leading 0; 01...12
            return _pad(formatCharacter.n(), 2)
        },
        M: function () {
            // Shorthand month name; Jan...Dec
            return formatCharacter.F()
                .slice(0, 3)
        },
        n: function () {
            // Month; 1...12
            return jsdate.getMonth() + 1
        },
        t: function () {
            // Days in month; 28...31
            return (new Date(formatCharacter.Y(), formatCharacter.n(), 0))
                .getDate()
        },

        // Year
        L: function () {
            // Is leap year?; 0 or 1
            let j = formatCharacter.Y()
            return j % 4 === 0 & j % 100 !== 0 | j % 400 === 0
        },
        o: function () {
            // ISO-8601 year
            let n = formatCharacter.n()
            let W = formatCharacter.W()
            let Y = formatCharacter.Y()
            return Y + (n === 12 && W < 9 ? 1 : n === 1 && W > 9 ? -1 : 0)
        },
        Y: function () {
            // Full year; e.g. 1980...2010
            return jsdate.getFullYear()
        },
        y: function () {
            // Last two digits of year; 00...99
            return formatCharacter.Y()
                .toString()
                .slice(-2)
        },

        // Time
        a: function () {
            // am or pm
            return jsdate.getHours() > 11 ? 'pm' : 'am'
        },
        A: function () {
            // AM or PM
            return formatCharacter.a()
                .toUpperCase()
        },
        B: function () {
            // Swatch Internet time; 000..999
            let H = jsdate.getUTCHours() * 36e2
            // Hours
            let i = jsdate.getUTCMinutes() * 60
            // Minutes
            // Seconds
            let s = jsdate.getUTCSeconds()
            return _pad(Math.floor((H + i + s + 36e2) / 86.4) % 1e3, 3)
        },
        g: function () {
            // 12-Hours; 1..12
            return formatCharacter.G() % 12 || 12
        },
        G: function () {
            // 24-Hours; 0..23
            return jsdate.getHours()
        },
        h: function () {
            // 12-Hours w/leading 0; 01..12
            return _pad(formatCharacter.g(), 2)
        },
        H: function () {
            // 24-Hours w/leading 0; 00..23
            return _pad(formatCharacter.G(), 2)
        },
        i: function () {
            // Minutes w/leading 0; 00..59
            return _pad(jsdate.getMinutes(), 2)
        },
        s: function () {
            // Seconds w/leading 0; 00..59
            return _pad(jsdate.getSeconds(), 2)
        },
        u: function () {
            // Microseconds; 000000-999000
            return _pad(jsdate.getMilliseconds() * 1000, 6)
        },

        // Timezone
        e: function () {
            // Timezone identifier; e.g. Atlantic/Azores, ...
            // The following works, but requires inclusion of the very large
            // timezone_abbreviations_list() function.
            /*              return that.date_default_timezone_get();
             */
            let msg = 'Not supported (see source code of date() for timezone on how to add support)'
            throw new Error(msg)
        },
        I: function () {
            // DST observed?; 0 or 1
            // Compares Jan 1 minus Jan 1 UTC to Jul 1 minus Jul 1 UTC.
            // If they are not equal, then DST is observed.
            let a = new Date(formatCharacter.Y(), 0)
            // Jan 1
            let c = Date.UTC(formatCharacter.Y(), 0)
            // Jan 1 UTC
            let b = new Date(formatCharacter.Y(), 6)
            // Jul 1
            // Jul 1 UTC
            let d = Date.UTC(formatCharacter.Y(), 6)
            return ((a - c) !== (b - d)) ? 1 : 0
        },
        O: function () {
            // Difference to GMT in hour format; e.g. +0200
            let tzo = jsdate.getTimezoneOffset()
            let a = Math.abs(tzo)
            return (tzo > 0 ? '-' : '+') + _pad(Math.floor(a / 60) * 100 + a % 60, 4)
        },
        P: function () {
            // Difference to GMT w/colon; e.g. +02:00
            let O = formatCharacter.O()
            return (O.substr(0, 3) + ':' + O.substr(3, 2))
        },
        T: function () {
            // The following works, but requires inclusion of the very
            // large timezone_abbreviations_list() function.
            /*              var abbr, i, os, _default;
            if (!tal.length) {
              tal = that.timezone_abbreviations_list();
            }
            if ($locutus && $locutus.default_timezone) {
              _default = $locutus.default_timezone;
              for (abbr in tal) {
                for (i = 0; i < tal[abbr].length; i++) {
                  if (tal[abbr][i].timezone_id === _default) {
                    return abbr.toUpperCase();
                  }
                }
              }
            }
            for (abbr in tal) {
              for (i = 0; i < tal[abbr].length; i++) {
                os = -jsdate.getTimezoneOffset() * 60;
                if (tal[abbr][i].offset === os) {
                  return abbr.toUpperCase();
                }
              }
            }
            */
            return 'UTC'
        },
        Z: function () {
            // Timezone offset in seconds (-43200...50400)
            return -jsdate.getTimezoneOffset() * 60
        },

        // Full Date/Time
        c: function () {
            // ISO-8601 date.
            return 'Y-m-d\\TH:i:sP'.replace(regex, replaceCallback)
        },
        r: function () {
            // RFC 2822
            return 'D, d M Y H:i:s O'.replace(regex, replaceCallback)
        },
        U: function () {
            // Seconds since UNIX epoch
            return jsdate / 1000 | 0
        }
    }

    jsdate = (timestamp === undefined ? new Date() // Not provided
        :
        (timestamp instanceof Date) ? new Date(timestamp) // JS Date()
            :
            new Date(timestamp * 1000) // UNIX timestamp (auto-convert to int)
    )

    return format.replace(regex, replaceCallback)
}

/**
 * CPCL data image conversion
 */
export const pcx = function () {
    function strHexToArray(s) {
        var a = [];
        (s.split(' ')).forEach(function (v) {
            a.push(parseInt(v, 16))
        });
        return a;
    }

    var pcxRleMask = 0xC0;
    var pcxPaletteMarker = 0x0C;

    var mStream = function (input) {
        this.input = input;
        this.i = 0;
    }
    mStream.prototype.readByte = function () {
        return this.input[this.i++];
    }
    mStream.prototype.writeByte = function (value) {
        return this.input.push(value);
    }
    mStream.prototype.readWord = function () {
        var x1 = this.input[this.i++];
        var x2 = this.input[this.i++];
        return (x2 << 8) | x1;
    }
    mStream.prototype.writeWord = function (value) {
        var x1 = (value & 0xff00) >> 8;
        var x2 = (value & 0xff);
        this.input.push.apply(this.input, [x2, x1]);
    }
    mStream.prototype.read = function (le) {
        var r = this.input.slice(this.i, this.i + le); //The original array will not be changed
        this.i += le;
        return r;
    }
    mStream.prototype.write = function (value) {
        this.input.push.apply(this.input, value);
    }

    function parseHeader(inp) {
        var h = new mStream(inp);
        return {
            identifier: h.readByte(),
            version: h.readByte(),
            encoding: h.readByte(),
            bitsPerPixel: h.readByte(),
            xStart: h.readWord(),
            yStart: h.readWord(),
            xEnd: h.readWord(),
            yEnd: h.readWord(),
            horzRes: h.readWord(),
            vertRes: h.readWord(),
            palette: h.read(48),
            reserved1: h.readByte(),
            numBitPlanes: h.readByte(),
            bytesPerLine: h.readWord(),
            paletteType: h.readWord(),
            horzScreenSize: h.readWord(),
            vertScreenSize: h.readWord(),
            reserved2: h.read(54)
        }
    }

    function fill(a, v, r) {
        for (var i = 0; i < r; i++) {
            a.push(v);
        }
        return a;
    }

    function writeHeader(o) {
        var a = [];
        var m = new mStream(a);
        m.writeByte(10); //identifier
        m.writeByte(5); //version
        m.writeByte(1); //encoding
        m.writeByte(1); //bitsPerPixel
        m.writeWord(0); //xStart
        m.writeWord(0); //yStart
        m.writeWord((o.width || 576) - 1); //xEnd
        m.writeWord((o.height || 1) - 1); //yEnd
        m.writeWord(75); //horzRes
        m.writeWord(75); //vertRes
        m.write(fill(fill([], 0, 3), 255, 48 - 3)); //palette
        m.writeByte(0); //reserved1
        m.writeByte(1); //numBitPlanes
        m.writeWord(72); //bytesPerLine
        m.writeWord(18); //paletteType
        m.writeWord(120); //horzScreenSize
        m.writeWord(0); //vertScreenSize
        m.write(fill([], 0, 54)); //reserved2
        return a;
    }

    var pcxRleByteReader = function (input) {
        this.m_stream = new mStream(input);
        this.m_count = 0;
        this.m_rleValue = 0;
    }

    pcxRleByteReader.prototype.readByte = function () {
        if (this.m_count > 0) {
            this.m_count--;
            return this.m_rleValue;
        } else {
            var code = this.m_stream.readByte();

            if ((code & pcxRleMask) == pcxRleMask) {
                this.m_count = (code & (pcxRleMask ^ 0xff));
                this.m_rleValue = this.m_stream.readByte();

                this.m_count--;
                return this.m_rleValue;
            } else {
                return code;
            }
        }
    }

    var pcxIndexReader = function (reader) {
        this.m_reader = reader;
        this.m_bitsPerPixel = 1;
        this.m_bitMask = 1;
        this.m_bitsRemaining = 0;
        this.m_byteRead = null;
    }
    pcxIndexReader.prototype.readIndex = function () {
        if (this.m_bitsRemaining == 0) {
            this.m_byteRead = this.m_reader.readByte();
            this.m_bitsRemaining = 8;
        }
        var index = (this.m_byteRead >> (8 - this.m_bitsPerPixel)) & this.m_bitMask;
        this.m_byteRead = this.m_byteRead << this.m_bitsPerPixel;
        this.m_bitsRemaining -= this.m_bitsPerPixel;
        return index;
    }

    function readAll(inp, height, width) {
        var reader = new pcxRleByteReader(inp);
        var indexReader = new pcxIndexReader(reader);
        var out = [];
        for (var y = 0; y < height; y++) {
            var outy = [];
            for (var x = 0; x < width; x++) {
                outy.push(indexReader.readIndex());
            }
            out.push(outy);
        }
        return out;
    }

    var pcxIndexWriter = function (writer) {
        this.m_writer = writer;
        this.m_bitsPerPixel = 1;
        this.m_bitMask = 1;

        this.m_bitsUsed = 0;
        this.m_byteAccumulated = 0;
    }
    pcxIndexWriter.prototype.writeIndex = function (index) {
        this.m_byteAccumulated = (this.m_byteAccumulated << this.m_bitsPerPixel) | (index & this.m_bitMask);
        this.m_bitsUsed += this.m_bitsPerPixel;

        if (this.m_bitsUsed == 8) {
            this.flush();
        }
    }
    pcxIndexWriter.prototype.flush = function () {
        if (this.m_bitsUsed > 0) {
            this.m_writer.writeByte(this.m_byteAccumulated);
            this.m_byteAccumulated = 0;
            this.m_bitsUsed = 0;
        }
    }

    var pcxRleByteWriter = function (output) {
        if (output) {
            if (output instanceof Array) {
                this.m_stream = new mStream(output);
            } else {
                this.m_stream = output;
            }
        } else {
            this.m_stream = new mStream();
        }
        this.m_lastValue;
        this.m_count = 0;
    }
    pcxRleByteWriter.prototype.writeByte = function (value) {
        if (this.m_count == 0 || this.m_count == 63 || value != this.m_lastValue) {
            this.flush();
            this.m_lastValue = value;
            this.m_count = 1;
        } else {
            this.m_count++;
        }
    }
    pcxRleByteWriter.prototype.flush = function () {
        if (this.m_count == 0) {
            return;
        } else if ((this.m_count > 1) || ((this.m_count == 1) && ((this.m_lastValue & pcxRleMask) == pcxRleMask))) {
            this.m_stream.writeByte(pcxRleMask | this.m_count);
            this.m_stream.writeByte(this.m_lastValue);
            this.m_count = 0;
        } else {
            this.m_stream.writeByte(this.m_lastValue);
            this.m_count = 0;
        }
    }

    function writeAll(a) {
        var out = [];
        var byteWriter = new pcxRleByteWriter(out);
        var indexWriter = new pcxIndexWriter(byteWriter);
        for (var y = 0; y < a.length; y++) {
            for (var x = 0; x < a[y].length; x++) {
                indexWriter.writeIndex(a[y][x]);
            }
            indexWriter.flush();
            byteWriter.flush();
        }
        indexWriter.flush();
        byteWriter.flush();
        return out;
    }

    function toStr(a) {
        var s = '';
        for (var x = 0; x < a.length; x++) {
            var sm = String.fromCharCode(a[x]);
            s += sm;
        }
        return s;
        canvas
    };

    function canvasToPcx(canvas, dt) {
        var out = writeHeader({
            height: canvas.height
        });
        var byteWriter = new pcxRleByteWriter(out);
        var indexWriter = new pcxIndexWriter(byteWriter);

        //var context = canvas.getContext('2d');
        //var dt = context.getImageData(0, 0, canvas.width, canvas.height).data;

        var yy = -1;
        for (var i = 0; i < dt.length; i += canvas.width * 4) {
            yy++;
            var xx = -1;
            for (var j = 0; j < canvas.width * 4; j += 4) {
                xx++;
                var g = Math.floor((dt[yy * canvas.width * 4 + (xx * 4)] + dt[yy * canvas.width * 4 + (xx * 4) + 1] + dt[yy * canvas.width * 4 + (xx * 4) + 2]) / 3);
                indexWriter.writeIndex(g < 128 ? 0 : 1);
            }
            indexWriter.flush();
            byteWriter.flush();
        }

        indexWriter.flush();
        byteWriter.flush();

        return toStr(out);
    }

    function save(ou) {
        window.location.href = 'data:image/pcx;base64,' + btoa(ou);
    }

    function toCPCL(ou, height, single, copies, blackMark) {
        // Prepare the settings
        var settings = "";


        // Check the black
        if (blackMark) {
            blackMark = "FORM\r\n";
            settings = "! UTILITIES\r\nIN-INCHES\r\nSETFF 12 0\r\nPRINT\r\n";
        } else {
            blackMark = "";
        }

        // Check if single or multiple
        if (single) {
            return settings + "! 0 200 200 " + Math.floor(height) + " " + copies + "\r\nPW 575\r\nTONE 0\r\nSPEED 3\r\nON-FEED IGNORE\r\nNO-PACE\r\nBAR-SENSE\r\nPCX 0 0 " + ou + "\r\n" + blackMark + "PRINT\r\n";
        } else {
            return settings + "! 0 200 200 " + Math.floor(height) + " " + copies + "\r\nPW 575\r\nTONE 0\r\nSPEED 3\r\nON-FEED REPRINT\r\nNO-PACE\r\nBAR-SENSE\r\nPCX 0 0 " + ou + "\r\n" + blackMark + "PRINT\r\n";
        }
    }

    return {
        fromCanvas: canvasToPcx,
        toCPCL: function (canvas, canvasImageData, single, copies, blackMark) {
            return toCPCL(canvasToPcx(canvas, canvasImageData), canvas.height, single, copies, blackMark);
        },
        save: save
    }
}();

/**
 * CPCL to base64 encoded string
 * @param {string} data 
 * 
 * @returns {string}
 */
export const cpclToBase64EncodedString = (data) => {

    var stringToArrayBuffer = function (str) {
        var ret = new Uint8Array(str.length);
        for (var i = 0; i < str.length; i++) {
            ret[i] = str.charCodeAt(i);
        }
        return ret.buffer;
    };

    // convert to ArrayBuffer
    if (typeof data === 'string') {
        data = stringToArrayBuffer(data);
    } else if (data instanceof Array) {
        // assuming array of interger
        data = new Uint8Array(data).buffer;
    } else if (data instanceof Uint8Array) {
        data = data.buffer;
    }

    /* This code is based on the performance tests at http://jsperf.com/b64tests
    * This 12-bit-at-a-time algorithm was the best performing version on all
    * platforms tested.
    */

    var b64_6bit = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
    var b64_12bit;

    var b64_12bitTable = function () {
        b64_12bit = [];
        for (var i = 0; i < 64; i++) {
            for (var j = 0; j < 64; j++) {
                b64_12bit[i * 64 + j] = b64_6bit[i] + b64_6bit[j];
            }
        }
        b64_12bitTable = function () { return b64_12bit; };
        return b64_12bit;
    };

    function uint8ToBase64(rawData) {
        var numBytes = rawData.byteLength;
        var output = '';
        var segment;
        var table = b64_12bitTable();
        for (var i = 0; i < numBytes - 2; i += 3) {
            segment = (rawData[i] << 16) + (rawData[i + 1] << 8) + rawData[i + 2];
            output += table[segment >> 12];
            output += table[segment & 0xfff];
        }
        if (numBytes - i === 2) {
            segment = (rawData[i] << 16) + (rawData[i + 1] << 8);
            output += table[segment >> 12];
            output += b64_6bit[(segment & 0xfff) >> 6];
            output += '=';
        } else if (numBytes - i === 1) {
            segment = (rawData[i] << 16);
            output += table[segment >> 12];
            output += '==';
        }
        return output;
    }

    var array = new Uint8Array(data);
    var convertedData = uint8ToBase64(array);

    return convertedData;
}

/**
 * Check if the passed string is json
 * 
 * @param {string} value 
 * 
 * @return {boolean}
 */
export const isJson = (value) => {
    try {
        JSON.parse(value);
    } catch (e) {
        return false;
    }
    return true;
}

/**
 * Generate a uuid using nanoid package
 * 
 * @return {string}
 */
 export const generateUUID = () => {
    return nanoid();
}

/*****
 * Private Functions
 * Used within the file.
 *****/

/**
 * Process the filter parameters by 
 * cleaning up null values
 * @param {array} filter - the array of filter object
 * 
 * @return {array}
 */
function processFilterData(filter) {
    // Prepare result
    let result = [];

    // Get the length of the filters
    let length = filter.length || 10;

    // Get all the filter data and put it in an array
    for (let i = 0; i < length; i++) {
        if (filter[i]) {
            result.push(filter[i]);
        }
    }

    return result;
}

/**
 * Check whether the record matches the filter
 * @param {object} record 
 * @param {object} filter 
 * 
 * @return {boolean}
 */
function filterRecord(record, filter) {
    // Check if the data is accepted in the filter criteria
    let fieldValue = record[filter.f] || record[filter.field];
    let operator = filter.o || filter.operator || '=';
    let value = filter.v || filter.value;
    return compare(fieldValue, value, operator);
}

/**
 * Used to compare/filter the two data passed
 * 
 * @param {any} mainValue - first value
 * @param {any} compareValue - second value
 * @param {any} operator - operator value
 * 
 * @return {boolean}
 */
function compare(mainValue, compareValue, operator) {
    operator = operator || '=';

    // Check if it's an object then stringify it
    if (typeof mainValue === 'object') {
        mainValue = JSON.stringify(mainValue);
    }
    if (typeof compareValue === 'object') {
        compareValue = JSON.stringify(compareValue);
    }

    var xs = mainValue || '';
    var ys = compareValue || '';

    return (operator == '=' || operator == '==') ? mainValue == compareValue : (
        operator == '!=' ? mainValue != compareValue : (
            operator == '<>' ? mainValue != compareValue : (
                operator == '<=' ? mainValue <= compareValue : (
                    operator == '>=' ? mainValue >= compareValue : (
                        operator == '<' ? mainValue < compareValue : (
                            operator == '>' ? mainValue > compareValue : (
                                operator == 'like' ? xs.toString().search(ys.toString()) > -1 : (
                                    operator == 'ilike' ? xs.toString().toLowerCase().search(ys.toString().toLowerCase()) > -1 : false))))))));
}

export function httpBuildQuery(data) {
    var result = [];
    for (var i in data) {
        if (!data[i]) {
            continue;
        }
        
        result.push(i + '=' + encodeURI(data[i]));
    }
    return result.join('&');
}

/**
 * Validate some important parameters
 *
 * @return {bool/array} - return true if all valid else returns
 *                        an array the contains the invalid fields
 */
export function validateParameters(required, parameters) {
    // Prepare the basic result information
    var valid = true;
    var missing = [];

    // Check if all the param are existing
    for (var index in required) {
        if (!parameters[required[index]]) {
            valid = false;
            missing.push(required[index]);
        }
    }

    // Response back if it's missing some parameters
    if (!valid) {
        return missing;
    }

    return valid;
}

/**
 * Parse a json string, if not a json will return the data
 * 
 * @param {string} data
 * @returns {Array|Object}
 */
export function jsonDecode(data) {
    try {
        return JSON.parse(data);
    } catch (e) {
        return data;
    }
}

/**
 * Check whether the the object is empty
 * @param {object} obj 
 * 
 * @return {boolean}
 */
export const objectIsEmpty = (obj) => {
    if (!obj || obj.constructor !== Object || Object.keys(obj).length === 0) {
        return true;
    }
    return false;
}

export const safeEval = (txt) => {
    if (typeof txt != 'string') {
        return txt;
    }
    var invert = false;
    if (txt.match(/^(\!+)/)) {
        invert = true;
        txt = txt.replace(/^\!+/, '');
    }
    var r = '';
    try {
        r = JSON.parse(txt);
    } catch (e) {
        var o = typeof exports != 'undefined' ? exports : window;
        try {
            r = jsonQueryGet(o, txt);
        } catch (ee) {
            console.error('gagal EVAL==>', txt)
            return undefined;
        }
    }
    r = invert ? !r : r;
    return r;
}

export const isEmptyString = (str) => {
    if (str === undefined || str === null) {
        return true
    }
    if (typeof str === 'string' || str instanceof String) {
        if (str.trim().length === 0 ) {
            return true
        }
        // check for blank
        if (/^\s*$/.test(str)) {
            return true
        }
    }
    return false
}

export const removeHtmlTags = (str) => {
    if (str === undefined || str === null) {
        return str
    }
    if (typeof str === 'string' || str instanceof String) {
        const regex = /(<([^>]+)>)/ig;
        return str.replace(regex, '');
    }
    return str
}

/**
 * Converts the size of image using uri
 * @param {string} uri image source
 * 
 * @return {Promise}
 */
export const getExternalImageSize = async (uri) => new Promise((resolve) => {
    ReactNativeImage.getSize(uri, (width, height) => {
      resolve({"width":width, "height":height});
    },(error) => {
        console.log(error)
        resolve(null)
    });
})
