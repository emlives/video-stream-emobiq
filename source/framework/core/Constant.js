/**
 * Constant values to be used throughout the app
 * mainly in framework.
 */

export const PrefixFramework = 'framework_';
export const PrefixLocalTable = 'local_table_';

/**
 * Reducer Built In Actions
 */
export const ReducerActionResetState = 'RESET_STATE';

 /**
  * Names of the core files.
  */

 // source/framework/core/Setting.js
export const NameSetting = 'settings';

 /**
  * Constant for the default values
  * for different types.
  */
export const DefaultValueNumber = -1;
export const DefaultValueString = '';
export const DefaultValueArray = [];
export const DefaultValueObject = {};