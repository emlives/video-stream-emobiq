/**
 * Reusable functions for components
 */
import Theme from '../../dependency/style/Theme';
import transform from 'css-to-react-native';

/**
 * Get the style of the specified component
 * - First from the props
 * - Then from the theme
 * @param {string} name 
 * @param {object} props
 * 
 * @return {any}
 */
export const getStyle = (name, props) => {
    // Get from the props passed
    const data = props && props.data ? props.data : null;
    const style = data ? (data.style ? data.style : {}) : {};

    // Get the name from base style
    let combinedStyle = {};
    if (Theme[name] && props.parentStyle) {
        combinedStyle = { ...props.parentStyle, ...Theme[name], ...style };
    } else if (Theme[name]) {
        combinedStyle = { ...Theme[name], ...style };
    } else if (props.parentStyle) {
        combinedStyle = { ...props.parentStyle, ...style };
    } else {
        combinedStyle = Object.keys(style).length > 0 ? style : {};
    }

    let RNStyle = cssToRNStyle(combinedStyle, "Text", { emSize: 16, ptSize: 1});

    switch (name) {
        case 'panel':
        case 'datalist':
            return {
                original: combinedStyle,
                style: RNStyle
            }
        default:
            return RNStyle;
    }
}

// Style properties for RN Image Component
const ImageStylePropTypes = [
    "display",
    "width",
    "height",
    "start",
    "end",
    "top",
    "left",
    "right",
    "bottom",
    "minWidth",
    "maxWidth",
    "minHeight",
    "maxHeight",
    "margin",
    "marginVertical",
    "marginHorizontal",
    "marginTop",
    "marginBottom",
    "marginLeft",
    "marginRight",
    "marginStart",
    "marginEnd",
    "padding",
    "paddingVertical",
    "paddingHorizontal",
    "paddingTop",
    "paddingBottom",
    "paddingLeft",
    "paddingRight",
    "paddingStart",
    "paddingEnd",
    "borderWidth",
    "borderTopWidth",
    "borderStartWidth",
    "borderEndWidth",
    "borderRightWidth",
    "borderBottomWidth",
    "borderLeftWidth",
    "position",
    "flexDirection",
    "flexWrap",
    "justifyContent",
    "alignItems",
    "alignSelf",
    "alignContent",
    "overflow",
    "flex",
    "flexGrow",
    "flexShrink",
    "flexBasis",
    "aspectRatio",
    "zIndex",
    "direction",
    "shadowColor",
    "shadowOffset",
    "shadowOpacity",
    "shadowRadius",
    "transform",
    "transformMatrix",
    "decomposedMatrix",
    "scaleX",
    "scaleY",
    "rotation",
    "translateX",
    "translateY",
    "resizeMode",
    "backfaceVisibility",
    "backgroundColor",
    "borderColor",
    "borderRadius",
    "tintColor",
    "opacity",
    "overlayColor",
    "borderTopLeftRadius",
    "borderTopRightRadius",
    "borderBottomLeftRadius",
    "borderBottomRightRadius",
];

// Style properties for RN View Component
const ViewStylePropTypes = [
    "boxShadow",
    "filter",
    "color",
    "display",
    "width",
    "height",
    "start",
    "end",
    "top",
    "left",
    "right",
    "bottom",
    "minWidth",
    "maxWidth",
    "minHeight",
    "maxHeight",
    "margin",
    "marginVertical",
    "marginHorizontal",
    "marginTop",
    "marginBottom",
    "marginLeft",
    "marginRight",
    "marginStart",
    "marginEnd",
    "padding",
    "paddingVertical",
    "paddingHorizontal",
    "paddingTop",
    "paddingBottom",
    "paddingLeft",
    "paddingRight",
    "paddingStart",
    "paddingEnd",
    "border",
    "borderWidth",
    "borderTopWidth",
    "borderStartWidth",
    "borderEndWidth",
    "borderRightWidth",
    "borderBottomWidth",
    "borderLeftWidth",
    "position",
    "flexDirection",
    "flexWrap",
    "justifyContent",
    "alignItems",
    "alignSelf",
    "alignContent",
    "overflow",
    "flex",
    "flexGrow",
    "flexShrink",
    "flexBasis",
    "aspectRatio",
    "zIndex",
    "direction",
    "shadowColor",
    "shadowOffset",
    "shadowOpacity",
    "shadowRadius",
    "transform",
    "transformMatrix",
    "decomposedMatrix",
    "scaleX",
    "scaleY",
    "rotation",
    "translateX",
    "translateY",
    "backfaceVisibility",
    "backgroundColor",
    "border",
    "borderColor",
    "borderTopColor",
    "borderRightColor",
    "borderBottomColor",
    "borderLeftColor",
    "borderStartColor",
    "borderEndColor",
    "borderRadius",
    "borderTopLeftRadius",
    "borderTopRightRadius",
    "borderTopStartRadius",
    "borderTopEndRadius",
    "borderBottomLeftRadius",
    "borderBottomRightRadius",
    "borderBottomStartRadius",
    "borderBottomEndRadius",
    "borderStyle",
    "opacity",
    "elevation",
    "fontSize"
];

// Style properties for RN Text Component
const TextStylePropTypes = [
    "textShadow",
    "display",
    "width",
    "height",
    "start",
    "end",
    "top",
    "left",
    "right",
    "bottom",
    "minWidth",
    "maxWidth",
    "minHeight",
    "maxHeight",
    "margin",
    "marginVertical",
    "marginHorizontal",
    "marginTop",
    "marginBottom",
    "marginLeft",
    "marginRight",
    "marginStart",
    "marginEnd",
    "padding",
    "paddingVertical",
    "paddingHorizontal",
    "paddingTop",
    "paddingBottom",
    "paddingLeft",
    "paddingRight",
    "paddingStart",
    "paddingEnd",
    "borderWidth",
    "borderTopWidth",
    "borderStartWidth",
    "borderEndWidth",
    "borderRightWidth",
    "borderBottomWidth",
    "borderLeftWidth",
    "position",
    "flexDirection",
    "flexWrap",
    "justifyContent",
    "alignItems",
    "alignSelf",
    "alignContent",
    "overflow",
    "flex",
    "flexGrow",
    "flexShrink",
    "flexBasis",
    "aspectRatio",
    "zIndex",
    "direction",
    "shadowColor",
    "shadowOffset",
    "shadowOpacity",
    "shadowRadius",
    "transform",
    "transformMatrix",
    "decomposedMatrix",
    "scaleX",
    "scaleY",
    "rotation",
    "translateX",
    "translateY",
    "backfaceVisibility",
    "backgroundColor",
    "borderColor",
    "borderTopColor",
    "borderRightColor",
    "borderBottomColor",
    "borderLeftColor",
    "borderStartColor",
    "borderEndColor",
    "borderRadius",
    "borderTopLeftRadius",
    "borderTopRightRadius",
    "borderTopStartRadius",
    "borderTopEndRadius",
    "borderBottomLeftRadius",
    "borderBottomRightRadius",
    "borderBottomStartRadius",
    "borderBottomEndRadius",
    "borderStyle",
    "opacity",
    "elevation",
    "color",
    "fontFamily",
    "fontSize",
    "fontStyle",
    "fontWeight",
    "fontVariant",
    "textShadowOffset",
    "textShadowRadius",
    "textShadowColor",
    "letterSpacing",
    "lineHeight",
    "textAlign",
    "textAlignVertical",
    "includeFontPadding",
    "textDecorationLine",
    "textDecorationStyle",
    "textDecorationColor",
    "textTransform",
    "writingDirection",
];

// Three main component types based on style
const stylePropTypes = {
    "View": ViewStylePropTypes,
    "Text": TextStylePropTypes,
    "Image": ImageStylePropTypes,
};

// properties that support percentage value
const PercentSupportedStyles = [
    "width",
    "height",
    "top",
    "bottom",
    "left",
    "right",
    "margin",
    "marginBottom",
    "marginTop",
    "marginLeft",
    "marginRight",
    "marginHorizontal",
    "marginVertical",
    "padding",
    "paddingBottom",
    "paddingTop",
    "paddingLeft",
    "paddingRight",
    "paddingHorizontal",
    "paddingVertical",
];

const inheritableProperties = [ 
    "color",
    "cursor",
    "direction",
    "elevation",
    "font-family",
    "fontFamily",
    "font-size",
    "fontSize",
    "font-style",
    "fontStyle",
    "font-variant",
    "fontVariant",
    "font-weight",
    "fontWeight",
    "font",
    "letter-spacing",
    "letterSpacing",
    "line-height",
    "lineHeight",
    "text-align",
    "textAlign",
    "text-transform",
    "textTransform",
    "textShadow",
    "textDecorationLine",
    "textDecorationStyle",
    "textDecorationColor",
    "textShadowOffset",
    "textShadowRadius",
    "textShadowColor",
];

export const AbsoluteFontSize = {
    "medium": 14,
    "xx-small": 8.5,
    "x-small": 10,
    "small": 12,
    "large": 17,
    "x-large": 20,
    "xx-large": 24,
    "smaller": 13.3,
    "larger": 16,
    "length": null,
    "initial": null,
    "inherit": null,
    "unset": null
};

/**
 * @param {string} key: the key of style
 * @param {string} value: the value of style
 * @return {array}
 */
function mapAbsoluteFontSize(key, value) {
    let fontSize = value;
    if (AbsoluteFontSize.hasOwnProperty(value)) {
        fontSize = AbsoluteFontSize[value];
    }
    return [key, fontSize];
}

/**
 * Converts a html style to its equivalent react native style
 * @param {object} css: object of key value css strings
 * @param {string} styleset: the styleset to convert the styles against
 * @param {object} { parentTag, emSize }
 * @returns {object}
 */
const cssToRNStyle = (css, styleset, { emSize, ptSize }) => {
    const styleProps = stylePropTypes[styleset];

    return Object.keys(css)
        .map((key) => [key, css[key]])
        .map(([key, value]) => {
            // Key convert
            return [
                key
                .split("-")
                .map((item, index) =>
                    index === 0 ? item : item[0].toUpperCase() + item.substr(1)
                )
                .join(""),
                value,
            ];
        })
        .map(([key, value]) => {
            if (styleProps.indexOf(key) === -1) {
                return undefined;
            }
            if (typeof value === "string") {
                value = value.replace(/\s*!\s*important/, "");
                if (!value) {
                    return undefined 
                }
                // css margin/padding shorthand is handled in reduce
                if (key === 'margin' || key === 'padding' || key === 'boxShadow' || key === 'textShadow' || key === 'border') {
                    return [key, value];
                }
                if (key === "display" && value === "block") {
                    return ["flexDirection", "column"];
                }
                if (key === "display" && (value === "inline" || value === "inline-block" || value === "table")) {
                    return ["flexDirection", "row"];
                }
                if (key === "display" && ["flex", "none"].indexOf(value) === -1) {
                    return [key, "flex"];
                }
                if (key === "position" && ["absolute", "relative"].indexOf(value) === -1) {
                    return undefined;
                }
                if (key === "textAlign") {
                    if (
                        ["left", "right", "justify", "auto", "center"].indexOf(value) !== -1
                    ) {
                        return [key, value];
                    }
                    if (value === "start") {
                        return [key, "left"];
                    }
                    if (value === "end") {
                        return [key, "right"];
                    }
                    return undefined;
                }
                if (
                    value
                        .replace(/[-_]/g, "")
                        .search(/\binherit\b|\bnormal\b|\binitial\b|\bauto\b|(calc|var)\(.*\)/) !== -1
                ) {
                    return undefined;
                }
                // See if we can use the percentage directly
                if (
                    value.search(/[\d.]+%/) !== -1 &&
                    PercentSupportedStyles.indexOf(key) !== -1
                ) {
                    return [key, value];
                }
                if (value.search(/[\d.]+em/) !== -1) {
                    const pxSize = parseFloat(value.replace("em", "")) * emSize;
                    return [key, pxSize];
                }
                if (value.search(/[\d.]+pt/) !== -1) {
                    const pxSize = parseFloat(value.replace("pt", "")) * ptSize;
                    return [key, pxSize];
                }
                // See if we can convert a 20px to a 20 automagically
                const numericValue = parseFloat(value.replace("px", ""));
                if (key !== "fontWeight" && !isNaN(numericValue)) {
                    if (styleProps.indexOf(key) !== -1) {
                        return [key, numericValue];
                    }
                }
                if (key === "fontSize") {
                    return mapAbsoluteFontSize(key, value);
                }
                if (key === "fontVariant") { // if fontVariant is string, convert to array because RN expects array as fontVariant value
                    return [key, [value]]
                }
            }
            return [key, value];
        })
        .filter((prop) => prop !== undefined)
        .reduce((acc, [key, value]) => {
            // handles margin/padding shorthand
            if ((key === 'margin' || key === 'padding' || key === 'textShadow' || key === 'border') && typeof value === "string") {
                try {
                    let spaces = transform([[key, value]]);
                    for (var side in spaces) {
                        if (spaces.hasOwnProperty(side)) {
                            acc[side] = spaces[side];
                        }
                    }
                } catch(err) {console.log(err)}
            } else if (key === 'boxShadow') {
                try {
                    // if the boxShadow value includes spread (e.q. 3px 3px 3px 6px black) remove the spread, react native doesn't support that
                    valueArray = value.split(" ");
                    if (valueArray.length == 5) {
                        valueArray.splice(3,1)
                    } else if (valueArray.length == 4 && /\d/.test(valueArray[3]) ) {
                        valueArray.splice(3,1)
                    }
                    let newValue = valueArray.join(" ")
                    let spaces = transform([[key, newValue]]);
                    for (var side in spaces) {
                        if (spaces.hasOwnProperty(side)) {
                            acc[side] = spaces[side];
                        }
                    }
                    
                } catch(err) {console.log(err)}
            } else {
                acc[key] = value;
            }
            return acc;
        }, {});
}

/**
 * Filter passed style and only return it with inheritable properties
 * @param {object} styleObject: object of key value RN style
 * @returns {object}
 */
export const filterInheritableStyles = (styleObject) => {
    return Object.keys(styleObject)
        .filter(key => inheritableProperties.includes(key))
        .reduce((obj, key) => {
            obj[key] = styleObject[key];
            return obj;
        }, {});
}

const toPx = (input, windowHeight) => {
    let output = null;
    if (typeof input === "string") {
        if (input.indexOf('%') > -1) {
            output = (windowHeight/100) * Number(input.replace('%', '').trim());
        } else if (input.indexOf('px') > -1) {
            output = Number(input.replace('px', '').trim());
        } 
    }else if (Number(input)) {
        output = Number(input);
    }
    return output;
}

/**
 * Return the calculated height of the ScrollView
 * @param {object} allProps: all props that belong to the page
 * @param {number} windowHeight: height of the visible screen/window
 * @returns {number}
 */
export const initContentHeight = (allProps, windowHeight) => {
    let scrollableHeight = 0;
    Object.keys(allProps).forEach(function(componentName) {
        const compDefaultHeight = 40;
        const compMarginBottom = 0;
        const style = allProps[componentName].style;
        if (style?.top) {
            const componentHeight = style.height?(toPx(style.height, windowHeight)+compMarginBottom):compDefaultHeight;
            const componentBottom = toPx(style.top, windowHeight) + componentHeight;

            if (componentBottom > scrollableHeight) {
                scrollableHeight = componentBottom;
            }
        }
    });

    scrollableHeight = scrollableHeight;
    return scrollableHeight;
}

/**
 * Formats the parent style layout
 * @param {object} props The component props
 */
export const formatParentStyleLayout = (props) => {

    // const floatHandler = (props, component) => {
    //     if (typeof props[component.parent] === 'undefined' || typeof props[component.parent].style === 'undefined') {
    //         return;
    //     }

    //     switch (component.style.float) {
    //         case 'left':
    //         case 'right':
    //             props[component.parent].style.flexDirection = 'row';
    //             break;
    //         default:
    //             props[component.parent].style.flexDirection = 'column';
    //             break;
    //     }
    // };

    // const displayHandler = (props, component) => {
    //     if (typeof props[component.parent] === 'undefined' || typeof props[component.parent].style === 'undefined') {
    //         return;
    //     }

    //     switch(component.style.display) {
    //         case 'table-cell':
    //         case 'inline-block':
    //             props[component.parent].style.flexDirection = 'row'
    //             break;
    //         default:
    //             props[component.parent].style.flexDirection = 'column'
    //             break;
    //     }
    // };

    // const zIndexHandler = (component) => {
    //     component.style.position = 'absolute'
    // };

    // // Loop through component props
    // Object.keys(props).forEach(name => {
    //     const component = props[name];
    //     if (component.style?.float && component.parent) {
    //         floatHandler(props, component)
    //     }
    //     if (component.style?.display && component.parent) {
    //         displayHandler(props, component)
    //     }
    //     if (component.style?.zIndex) {
    //         zIndexHandler(component)
    //     }
    // });
}