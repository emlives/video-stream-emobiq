import { objectIsEmpty } from '../Helper'

/**

* Base Class of all UIComponents.

* @param {componentProps} componentProps - state of a component in a page

*/
class UIComponent {
    constructor(componentProps) {
        this.attr = componentProps.attribute
        this._el = objectIsEmpty(componentProps.elementAttribute) ? {} : componentProps.elementAttribute
        this.name = componentProps?.property?.componentName ? componentProps?.property?.componentName : null
        this.componentState = componentProps
        
    }
}

export default UIComponent;