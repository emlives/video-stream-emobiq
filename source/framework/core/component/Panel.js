import UIComponent from './UIComponent';
import { objectIsEmpty } from '../Helper'

/**

* Base Class to mimic Emobiq Panel element.

* @param {componentProps} componentProps - state of a component in a page

*/

class Panel extends UIComponent {

    constructor(componentProps) {
        super(componentProps)
        if ( objectIsEmpty(componentProps.elementAttribute) ) {
            this.initializeElementAttributes(componentProps)
        } else {
            this._el = componentProps.elementAttribute
        }
    }
         
    /**
     * Adds the functions to get the special characters to this object
     */
    initializeElementAttributes = (componentProps) => {
        let componentValue = componentProps?.property?.componentValue ? componentProps.property.componentValue : null;
        let name = componentProps?.property?.componentName ? componentProps?.property?.componentName : null;
        this._el = {
            accessKey: "",
            ariaAtomic: null,
            ariaAutoComplete: null,
            ariaBusy: null,
            ariaChecked: null,
            ariaColCount: null,
            ariaColIndex: null,
            ariaColSpan: null,
            ariaCurrent: null,
            ariaDescription: null,
            ariaDisabled: null,
            ariaExpanded: null,
            ariaHasPopup: null,
            ariaHidden: null,
            ariaKeyShortcuts: null,
            ariaLabel: null,
            ariaLevel: null,
            ariaLive: null,
            ariaModal: null,
            ariaMultiLine: null,
            ariaMultiSelectable: null,
            ariaOrientation: null,
            ariaPlaceholder: null,
            ariaPosInSet: null,
            ariaPressed: null,
            ariaReadOnly: null,
            ariaRelevant: null,
            ariaRequired: null,
            ariaRoleDescription: null,
            ariaRowCount: null,
            ariaRowIndex: null,
            ariaRowSpan: null,
            ariaSelected: null,
            ariaSetSize: null,
            ariaSort: null,
            ariaValueMax: null,
            ariaValueMin: null,
            ariaValueNow: null,
            ariaValueText: null,
            assignedSlot: null,
            //attributeStyleMap: StylePropertyMap {size: 0},
            //attributes: NamedNodeMap {0: class, class: class, length: 1},
            autocapitalize: "",
            autofocus: false,
            baseURI: "http://localhost/EMOBIQ/kent-emobiq-v5-platform/www3/?appid=161475267179886#{%22rnd%22:%221618802040161743%22,%22p%22:%22pgLandingPage%22}",
            childElementCount: 0,
            //childNodes: NodeList [text],
            //children: HTMLCollection [],
            //classList: DOMTokenList(5) ["c-Label", "n-Label579", "addedClass", "hide", "fs-stored", value: "c-Label n-Label579 addedClass hide fs-stored"],
            className: "c-Panel n-"+name+" addedClass hide fs-stored",
            clientHeight: 0,
            clientLeft: 0,
            clientTop: 0,
            clientWidth: 0,
            contentEditable: "inherit",
            //dataset: DOMStringMap {}
            dir: "",
            draggable: false,
            elementTiming: "",
            enterKeyHint: "",
            //firstChild: text,
            firstElementChild: null,
            hidden: false,
            id: "",
            // to do list, generate innerHTML and innerText for panel
            innerHTML: "<img class=\'c-Image n-Image387 fs-stored\' src=\'https://i.imgur.com/P2A4PB6.jpg\'><textarea class=\'c-Memo n-Memo768 fs-stored\' name=\'Memo768\'></textarea><div class=\'c-Signature n-Signature551 fs-stored\'><canvas class=\'fs-stored\' width=\'111\' style=\'width: 100%; min-height: 30px; cursor: pointer; height: 100%;\' height=\'99\'></canvas></div><label class=\'c-Label n-Label990 fs-stored\'>Label</label><span class=\'c-Panel n-Panel248 fs-stored\'><label class=\'c-Label n-Label990879 fs-stored\'>Label</label></span>",
            innerText: null,
            inputMode: "",
            isConnected: true,
            isContentEditable: false,
            lang: "",
            //lastChild: text,
            lastElementChild: null,
            localName: "span",
            namespaceURI: "http://www.w3.org/1999/xhtml",
            //nextElementSibling: span.c-RowContainer.n-DataList467.fs-stored,
            //nextSibling: span.c-RowContainer.n-DataList467.fs-stored,
            nodeName: "SPAN",
            nodeType: 1,
            nodeValue: null,
            nonce: "",
            offsetHeight: 0,
            offsetLeft: 0,
            offsetParent: null,
            offsetTop: 0,
            offsetWidth: 0,
            onabort: null,
            onanimationend: null,
            onanimationiteration: null,
            onanimationstart: null,
            onauxclick: null,
            onbeforecopy: null,
            onbeforecut: null,
            onbeforepaste: null,
            onbeforexrselect: null,
            onblur: null,
            oncancel: null,
            oncanplay: null,
            oncanplaythrough: null,
            onchange: null,
            onclick: null,
            onclose: null,
            oncontextmenu: null,
            oncopy: null,
            oncuechange: null,
            oncut: null,
            ondblclick: null,
            ondrag: null,
            ondragend: null,
            ondragenter: null,
            ondragleave: null,
            ondragover: null,
            ondragstart: null,
            ondrop: null,
            ondurationchange: null,
            onemptied: null,
            onended: null,
            onerror: null,
            onfocus: null,
            onformdata: null,
            onfullscreenchange: null,
            onfullscreenerror: null,
            ongotpointercapture: null,
            oninput: null,
            oninvalid: null,
            onkeydown: null,
            onkeypress: null,
            onkeyup: null,
            onload: null,
            onloadeddata: null,
            onloadedmetadata: null,
            onloadstart: null,
            onlostpointercapture: null,
            onmousedown: null,
            onmouseenter: null,
            onmouseleave: null,
            onmousemove: null,
            onmouseout: null,
            onmouseover: null,
            onmouseup: null,
            onmousewheel: null,
            onpaste: null,
            onpause: null,
            onplay: null,
            onplaying: null,
            onpointercancel: null,
            onpointerdown: null,
            onpointerenter: null,
            onpointerleave: null,
            onpointermove: null,
            onpointerout: null,
            onpointerover: null,
            onpointerrawupdate: null,
            onpointerup: null,
            onprogress: null,
            onratechange: null,
            onreset: null,
            onresize: null,
            onscroll: null,
            onsearch: null,
            onseeked: null,
            onseeking: null,
            onselect: null,
            onselectionchange: null,
            onselectstart: null,
            onstalled: null,
            onsubmit: null,
            onsuspend: null,
            ontimeupdate: null,
            ontoggle: null,
            ontransitioncancel: null,
            ontransitionend: null,
            ontransitionrun: null,
            ontransitionstart: null,
            onvolumechange: null,
            onwaiting: null,
            onwebkitanimationend: null,
            onwebkitanimationiteration: null,
            onwebkitanimationstart: null,
            onwebkitfullscreenchange: null,
            onwebkitfullscreenerror: null,
            onwebkittransitionend: null,
            onwheel: null,
            // to do list: construct outerHTML and outerText for panel
            outerHTML: "<span class=\"c-Panel n-"+name+" fs-stored\"><img class=\"c-Image n-Image387 fs-stored\" src=\"https://i.imgur.com/P2A4PB6.jpg\"><textarea class=\"c-Memo n-Memo768 fs-stored\" name=\"Memo768\"></textarea><div class=\"c-Signature n-Signature551 fs-stored\"><canvas class=\"fs-stored\" width=\"111\" style=\"width: 100%; min-height: 30px; cursor: pointer; height: 100%;\" height=\"99\"></canvas></div><label class=\"c-Label n-Label990 fs-stored\">Label</label><span class=\"c-Panel n-Panel248 fs-stored\"><label class=\"c-Label n-Label990879 fs-stored\">Label</label></span></span>",
            outerText: "Label\nLabel",
            //ownerDocument: document,
            //parentElement: div#wrap_pgLandingPage.wrap.cbs.fs-stored,
            //parentNode: div#wrap_pgLandingPage.wrap.cbs.fs-stored,
            //part: DOMTokenList [value: ""],
            prefix: null,
            //previousElementSibling: input.c-Checkbox.n-Checkbox505.fs-stored,
            //previousSibling: input.c-Checkbox.n-Checkbox505.fs-stored,
            scrollHeight: 0,
            scrollLeft: 0,
            scrollTop: 0,
            scrollWidth: 0,
            shadowRoot: null,
            slot: "",
            spellcheck: true,
            style: componentProps.style,
            tabIndex: -1,
            tagName: "SPAN",
            textContent: componentValue,
            title: "",
            translate: true,
        }
    }
    
    setElementAttributes = (data) => {
        if (!this.componentState.elementAttribute || this.componentState.elementAttribute.constructor !== Object || Object.keys(this.componentState.elementAttribute).length === 0 || Object.keys(data).length === 0) {
            this.componentState.elementAttribute = this._el
        }
        // Loop through the attributes to be inserted/updated
        for (let attribute in data) {
            // Check if the value must affect something else in the state
            switch (attribute) {
                case 'style':
                        let allStyles = data[attribute].split(";");
                        allStyles.map((style) => {
                            let styleArray = style.split(":");
                            this.componentState.style[styleArray[0]] = styleArray[1]
                        })
                        break;
                default :
                        // Update the elementAttribute
                        this.componentState.elementAttribute[attribute] = data[attribute];
                    break;
            }
        }

        // also set initial attribute to empty object to avoid future errors
        if (!this.componentState.attribute || this.componentState.attribute.constructor !== Object || Object.keys(this.componentState.attribute).length === 0 || Object.keys(data).length === 0) {
            this.componentState.attribute = {}
        }
        return this.componentState
    }
    
    setAttributes = (data) => {
        // Loop through the attributes to be inserted/updated
        for (let attribute in data) {
            this.componentState.attribute[attribute] = data[attribute];
        }
        return this.componentState
    }

}

export default Panel;