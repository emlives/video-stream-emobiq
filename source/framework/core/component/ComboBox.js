import UIComponent from './UIComponent';
import { objectIsEmpty } from '../Helper'

/**

* Base Class to mimic Emobiq Combo Box element.

* @param {componentProps} componentProps - state of a component in a page

*/

class ComboBox extends UIComponent {

    constructor(componentProps) {
        super(componentProps)
        if ( objectIsEmpty(componentProps.elementAttribute) ) {
            this.initializeElementAttributes(componentProps)
        } else {
            this._el = componentProps.elementAttribute
        }
        this._value = componentProps.property.componentValue
    }
         
    /**
     * Adds the functions to get the special characters to this object
     */
    initializeElementAttributes = (componentProps) => {
        let name = componentProps?.property?.componentName ? componentProps?.property?.componentName : null;
        this._el = {
            // to do: generate the options attribute
            accessKey: "",
            ariaAtomic: null,
            ariaAutoComplete: null,
            ariaBusy: null,
            ariaChecked: null,
            ariaColCount: null,
            ariaColIndex: null,
            ariaColSpan: null,
            ariaCurrent: null,
            ariaDescription: null,
            ariaDisabled: null,
            ariaExpanded: null,
            ariaHasPopup: null,
            ariaHidden: null,
            ariaKeyShortcuts: null,
            ariaLabel: null,
            ariaLevel: null,
            ariaLive: null,
            ariaModal: null,
            ariaMultiLine: null,
            ariaMultiSelectable: null,
            ariaOrientation: null,
            ariaPlaceholder: null,
            ariaPosInSet: null,
            ariaPressed: null,
            ariaReadOnly: null,
            ariaRelevant: null,
            ariaRequired: null,
            ariaRoleDescription: null,
            ariaRowCount: null,
            ariaRowIndex: null,
            ariaRowSpan: null,
            ariaSelected: null,
            ariaSetSize: null,
            ariaSort: null,
            ariaValueMax: null,
            ariaValueMin: null,
            ariaValueNow: null,
            ariaValueText: null,
            assignedSlot: null,
            //attributeStyleMap: StylePropertyMap {size: 0},
            //attributes: NamedNodeMap {0: class, 1: name, class: class, name: name, length: 2},
            autocapitalize: "",
            autocomplete: "",
            autofocus: false,
            baseURI: "http://localhost/EMOBIQ/kent-emobiq-v5-platform/www3/?appid=161475267179886#{%22rnd%22:%221618802040161743%22,%22p%22:%22pgLandingPage%22}",
            childElementCount: 0,	
            //childNodes: NodeList []	
            //children: HTMLCollection []	
            //classList: DOMTokenList(3) ["c-Edit", "n-Edit219", "fs-stored", value: "c-Edit n-Edit219 fs-stored"],	
            className: "c-ComboBox n-"+name+" fs-stored"	,
            clientHeight: 25,
            clientLeft: 1,
            clientTop: 1,
            clientWidth: 133,
            contentEditable: "inherit",
            //dataset: DOMStringMap {}
            dir: "",
            disabled: false,
            draggable: false,
            elementTiming: "",
            enterKeyHint: "",
            events: null	,
            firstChild: null,
            firstElementChild: null,
            form: null,
            hidden: false,
            id: "",
            innerHTML: "",
            innerText: "",
            inputMode: "",
            isConnected: true,
            isContentEditable: false,
            //labels: NodeList []
            length: 0,
            localName: "select",	
            multiple: false,
            name: name,
            namespaceURI: "http://www.w3.org/1999/xhtml",
            //nextElementSibling: a.c-FButton.n-Button682.button.fs-stored
            //nextSibling: a.c-FButton.n-Button682.button.fs-stored
            nodeName: "SELECT",
            nodeType: 1,
            nodeValue: null,
            nonce: "",
            offsetHeight: 0,
            offsetLeft: 0,
            offsetParent: null,
            offsetTop: 0,
            offsetWidth: 0,
            onabort: null,
            onanimationend: null,
            onanimationiteration: null,
            onanimationstart: null,
            onauxclick: null,
            onbeforecopy: null,
            onbeforecut: null,
            onbeforepaste: null,
            onbeforexrselect: null,
            onblur: null,
            oncancel: null,
            oncanplay: null,
            oncanplaythrough: null,
            onchange: null,
            onclick: null,
            onclose: null,
            oncontextmenu: null,
            oncopy: null,
            oncuechange: null,
            oncut: null,
            ondblclick: null,
            ondrag: null,
            ondragend: null,
            ondragenter: null,
            ondragleave: null,
            ondragover: null,
            ondragstart: null,
            ondrop: null,
            ondurationchange: null,
            onemptied: null,
            onended: null,
            onerror: null,
            onfocus: null,
            onformdata: null,
            onfullscreenchange: null,
            onfullscreenerror: null,
            ongotpointercapture: null,
            oninput: null,
            oninvalid: null,
            onkeydown: null,
            onkeypress: null,
            onkeyup: null,
            onload: null,
            onloadeddata: null,
            onloadedmetadata: null,
            onloadstart: null,
            onlostpointercapture: null,
            onmousedown: null,
            onmouseenter: null,
            onmouseleave: null,
            onmousemove: null,
            onmouseout: null,
            onmouseover: null,
            onmouseup: null,
            onmousewheel: null,
            onpaste: null,
            onpause: null,
            onplay: null,
            onplaying: null,
            onpointercancel: null,
            onpointerdown: null,
            onpointerenter: null,
            onpointerleave: null,
            onpointermove: null,
            onpointerout: null,
            onpointerover: null,
            onpointerrawupdate: null,
            onpointerup: null,
            onprogress: null,
            onratechange: null,
            onreset: null,
            onresize: null,
            onscroll: null,
            onsearch: null,
            onseeked: null,
            onseeking: null,
            onselect: null,
            onselectionchange: null,
            onselectstart: null,
            onstalled: null,
            onsubmit: null,
            onsuspend: null,
            ontimeupdate: null,
            ontoggle: null,
            ontransitioncancel: null,
            ontransitionend: null,
            ontransitionrun: null,
            ontransitionstart: null,
            onvolumechange: null,
            onwaiting: null,
            onwebkitanimationend: null,
            onwebkitanimationiteration: null,
            onwebkitanimationstart: null,
            onwebkitfullscreenchange: null,
            onwebkitfullscreenerror: null,
            onwebkittransitionend: null,
            onwheel: null,
            //options: HTMLOptionsCollection [selectedIndex: -1],
            outerHTML: "<select class='c-ComboBox n-"+name+" fs-stored' name='"+name+"'></select>",
            outerText: "",
            //ownerDocument: document,
            //parentElement: div#wrap_pgLandingPage.wrap.cbs.fs-stored,
            //parentNode: div#wrap_pgLandingPage.wrap.cbs.fs-stored,
            //part: DOMTokenList [value: ""],
            prefix: null,
            //previousElementSibling: input.c-Edit.n-Edit219.fs-stored,
            //previousSibling: input.c-Edit.n-Edit219.fs-stored,
            required: false,
            scrollHeight: 25,
            scrollLeft: 0,
            scrollTop: 0,
            scrollWidth: 718,
            selectedIndex: -1,
            //selectedOptions: HTMLCollection [],
            shadowRoot: null,
            size: 0,
            slot: "",
            spellcheck: true,
            style: componentProps.style,
            tabIndex: 0,
            tagName: "SELECT",	
            textContent: "",
            title: "",
            translate: true,
            type: "select-one",
            validationMessage: "",
            //validity: ValidityState {valueMissing: false, typeMismatch: false, patternMismatch: false, tooLong: false, tooShort: false, …}	
            value: "",
            willValidate: true,
            _wipe_guid: 2
        }
    }
    
    setElementAttributes = (data) => {
        if (!this.componentState.elementAttribute || this.componentState.elementAttribute.constructor !== Object || Object.keys(this.componentState.elementAttribute).length === 0 || Object.keys(data).length === 0) {
            this.componentState.elementAttribute = this._el
        }
        // Loop through the attributes to be inserted/updated
        for (let attribute in data) {
            // Check if the value must affect something else in the state
            switch (attribute) {
                case 'value':
                    this.setComponentValue(data[attribute])
                    break;
                case 'style':
                    let allStyles = data[attribute].split(";");
                    allStyles.map((style) => {
                        let styleArray = style.split(":");
                        this.componentState.style[styleArray[0]] = styleArray[1]
                    })
                    break;
                default :
                    // Update the elementAttribute
                    this.componentState.elementAttribute[attribute] = data[attribute];
                    break;
            }
        }

        // also set initial attribute to empty object to avoid future errors
        if (!this.componentState.attribute || this.componentState.attribute.constructor !== Object || Object.keys(this.componentState.attribute).length === 0 || Object.keys(data).length === 0) {
            this.componentState.attribute = {}
        }
        return this.componentState
    }
    
    setAttributes = (data) => {
        // Loop through the attributes to be inserted/updated
        for (let attribute in data) {
            this.componentState.attribute[attribute] = data[attribute];
            // Check if the value must affect something else in the state
            switch (attribute) {
                case 'value':
                    this.setComponentValue(data[attribute])
                    break;
                case 'type':
                    this.componentState.elementAttribute[attribute] = data[attribute];
                    break;
                case 'options':
                    this.options = data[attribute];
                    break;
            }
        }
        return this.componentState
    }
    
    setComponentValue = (data) => {
        this.componentState.property['componentValue'] = data;
        this.componentState.elementAttribute['value'] = data;
        return this.componentState
    }

}

export default ComboBox;