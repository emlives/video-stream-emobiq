import UIComponent from './UIComponent';
import { objectIsEmpty } from '../Helper'

/**

* Base Class to mimic Emobiq Signature element.

* @param {componentProps} componentProps - state of a component in a page

*/

class Signature extends UIComponent {

    constructor(componentProps) {
        super(componentProps)
        if ( objectIsEmpty(componentProps.elementAttribute) ) {
            this.initializeElementAttributes(componentProps)
        } else {
            this._el = componentProps.elementAttribute
        }
    }
         
    /**
     * Adds the functions to get the special characters to this object
     */
    initializeElementAttributes = (componentProps) => {
        let componentValue = componentProps?.property?.componentValue ? componentProps.property.componentValue.uri : null;
        let name = componentProps?.property?.componentName ? componentProps?.property?.componentName : null;
        this._el = {
            accessKey: "",
            align: "",
            ariaAtomic: null,
            ariaAutoComplete: null,
            ariaBusy: null,
            ariaChecked: null,
            ariaColCount: null,
            ariaColIndex: null,
            ariaColSpan: null,
            ariaCurrent: null,
            ariaDescription: null,
            ariaDisabled: null,
            ariaExpanded: null,
            ariaHasPopup: null,
            ariaHidden: null,
            ariaKeyShortcuts: null,
            ariaLabel: null,
            ariaLevel: null,
            ariaLive: null,
            ariaModal: null,
            ariaMultiLine: null,
            ariaMultiSelectable: null,
            ariaOrientation: null,
            ariaPlaceholder: null,
            ariaPosInSet: null,
            ariaPressed: null,
            ariaReadOnly: null,
            ariaRelevant: null,
            ariaRequired: null,
            ariaRoleDescription: null,
            ariaRowCount: null,
            ariaRowIndex: null,
            ariaRowSpan: null,
            ariaSelected: null,
            ariaSetSize: null,
            ariaSort: null,
            ariaValueMax: null,
            ariaValueMin: null,
            ariaValueNow: null,
            ariaValueText: null,
            assignedSlot: null,
            //attributeStyleMap: StylePropertyMap {size: 0},
            //attributes: NamedNodeMap {0: class, class: class, length: 1},
            autocapitalize: "",
            autofocus: false,
            baseURI: "http://localhost/EMOBIQ/kent-emobiq-v5-platform/www3/?appid=161475267179886#{%22rnd%22:%221618802040161743%22,%22p%22:%22pgLandingPage%22}",
            border: "",
            childElementCount: 0,
            //childNodes: NodeList [],
            //children: HTMLCollection [],
            //classList: DOMTokenList(5) ["c-Label", "n-Label579", "addedClass", "hide", "fs-stored", value: "c-Label n-Label579 addedClass hide fs-stored"],
            className: "c-Image n-"+name+" fs-stored",
            clientHeight: 0,
            clientLeft: 0,
            clientTop: 0,
            clientWidth: 0,
            complete: true,
            contentEditable: "inherit",
            crossOrigin: null,
            currentSrc: componentValue,
            //dataset: DOMStringMap {}
            dir: "",
            draggable: true,
            elementTiming: "",
            enterKeyHint: "",
            firstChild: null,
            firstElementChild: null,
            height: 71,
            hidden: false,
            hspace: 0,
            id: "",
            innerHTML: "",
            innerText: "",
            inputMode: "",
            isConnected: true,
            isContentEditable: false,
            isMap: false,
            lang: "",
            lastChild: null,
            lastElementChild: null,
            loading: "auto",
            localName: "img",
            longDesc: "",
            lowsrc: "",
            name: "",
            namespaceURI: "http://www.w3.org/1999/xhtml",
            naturalHeight: 263,
            naturalWidth: 581,
            //nextElementSibling: textarea.c-Memo.n-Memo768.fs-stored,
            //nextSibling: textarea.c-Memo.n-Memo768.fs-stored,
            nodeName: "IMG",
            nodeType: 1,
            nodeValue: null,
            nonce: "",
            offsetHeight: 0,
            offsetLeft: 0,
            //offsetParent: span.c-Panel.n-Panel518.fs-stored,
            offsetTop: 0,
            offsetWidth: 0,
            onabort: null,
            onanimationend: null,
            onanimationiteration: null,
            onanimationstart: null,
            onauxclick: null,
            onbeforecopy: null,
            onbeforecut: null,
            onbeforepaste: null,
            onbeforexrselect: null,
            onblur: null,
            oncancel: null,
            oncanplay: null,
            oncanplaythrough: null,
            onchange: null,
            onclick: null,
            onclose: null,
            oncontextmenu: null,
            oncopy: null,
            oncuechange: null,
            oncut: null,
            ondblclick: null,
            ondrag: null,
            ondragend: null,
            ondragenter: null,
            ondragleave: null,
            ondragover: null,
            ondragstart: null,
            ondrop: null,
            ondurationchange: null,
            onemptied: null,
            onended: null,
            onerror: null,
            onfocus: null,
            onformdata: null,
            onfullscreenchange: null,
            onfullscreenerror: null,
            ongotpointercapture: null,
            oninput: null,
            oninvalid: null,
            onkeydown: null,
            onkeypress: null,
            onkeyup: null,
            onload: null,
            onloadeddata: null,
            onloadedmetadata: null,
            onloadstart: null,
            onlostpointercapture: null,
            onmousedown: null,
            onmouseenter: null,
            onmouseleave: null,
            onmousemove: null,
            onmouseout: null,
            onmouseover: null,
            onmouseup: null,
            onmousewheel: null,
            onpaste: null,
            onpause: null,
            onplay: null,
            onplaying: null,
            onpointercancel: null,
            onpointerdown: null,
            onpointerenter: null,
            onpointerleave: null,
            onpointermove: null,
            onpointerout: null,
            onpointerover: null,
            onpointerrawupdate: null,
            onpointerup: null,
            onprogress: null,
            onratechange: null,
            onreset: null,
            onresize: null,
            onscroll: null,
            onsearch: null,
            onseeked: null,
            onseeking: null,
            onselect: null,
            onselectionchange: null,
            onselectstart: null,
            onstalled: null,
            onsubmit: null,
            onsuspend: null,
            ontimeupdate: null,
            ontoggle: null,
            ontransitioncancel: null,
            ontransitionend: null,
            ontransitionrun: null,
            ontransitionstart: null,
            onvolumechange: null,
            onwaiting: null,
            onwebkitanimationend: null,
            onwebkitanimationiteration: null,
            onwebkitanimationstart: null,
            onwebkitfullscreenchange: null,
            onwebkitfullscreenerror: null,
            onwebkittransitionend: null,
            onwheel: null,
            outerHTML: "<img class='c-Image n-"+name+" fs-stored' src='"+componentValue+"'>",
            outerText: "",
            //ownerDocument: document,
            //parentElement: span.c-Panel.n-Panel518.fs-stored,
            //parentNode: span.c-Panel.n-Panel518.fs-stored,
            //part: DOMTokenList [value: ""],
            prefix: null,
            previousElementSibling: null,
            previousSibling: null,
            referrerPolicy: "",
            scrollHeight: 71,
            scrollLeft: 0,
            scrollTop: 0,
            scrollWidth: 0,
            shadowRoot: null,
            sizes: "",
            slot: "",
            spellcheck: true,
            src: componentValue,
            srcset: "",
            style: componentProps.style,
            tabIndex: -1,
            tagName: "IMG",
            textContent: "",
            title: "",
            translate: true,
        }
    }
    
    setElementAttributes = (data) => {
        if (!this.componentState.elementAttribute || this.componentState.elementAttribute.constructor !== Object || Object.keys(this.componentState.elementAttribute).length === 0 || Object.keys(data).length === 0) {
            this.componentState.elementAttribute = this._el
        }
        // also set initial attribute to empty object to avoid future errors
        if (!this.componentState.attribute || this.componentState.attribute.constructor !== Object || Object.keys(this.componentState.attribute).length === 0 || Object.keys(data).length === 0) {
            this.componentState.attribute = {}
        }
        // Loop through the attributes to be inserted/updated
        for (let attribute in data) {
            // Check if the value must affect something else in the state
            switch (attribute) {
                case 'src':
                    this.setComponentValue(data[attribute])
                    break;
                case 'style':
                    let allStyles = data[attribute].split(";");
                    allStyles.map((style) => {
                        let styleArray = style.split(":");
                        this.componentState.style[styleArray[0]] = styleArray[1]
                    })
                    break;
                default :
                    // Update the elementAttribute
                    this.componentState.elementAttribute[attribute] = data[attribute];
                    break;
            }
        }
        
        return this.componentState
    }
    
    setAttributes = (data) => {
        // Loop through the attributes to be inserted/updated
        for (let attribute in data) {
            this.componentState.attribute[attribute] = data[attribute];
            // Check if the value must affect something else in the state
            switch (attribute) {
                case 'url':
                case 'externalURL':
                    this.setComponentValue(data[attribute])
                    break;
            }
        }
        return this.componentState
    }
    
    setComponentValue = (data) => {
        this.componentState.property['componentValue'] = data;
        this.componentState.elementAttribute['src'] = data;
        this.componentState.elementAttribute['currentSrc'] = data;
        this.componentState.elementAttribute['outerHTML'] = "<img class='c-Image n-"+this.name+" fs-stored' src='"+data+"'>";
        return this.componentState
    }

}

export default Signature;