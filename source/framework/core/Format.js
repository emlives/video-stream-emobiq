/**
 * Contains the different format to be used for the framework
 */

/**
  * Return a standard formatted result
  * @param {bool} success 
  * @param {any} data
  * @param {string} message
  * 
  * @return {object}
  */
export const result = (success, data, message) => {
    return {
        success: success,
        data: data,
        message: message
    }
}

 /**
  * Return a plugin formatted result
  * @param {bool} success 
  * @param {any} data
  * @param {string} message
  * 
  * @return {object}
  */
export const resultPlugin = (success, data, message) => {
    return {
        success: success,
        data: data,
        message: message
    }
}

 /**
  * Return the connector formatted api result
  * following the eMOBIQ format
  * 
  * Used by: Navision Connector - Only
  * @param {bool} success 
  * @param {string} service
  * @param {string} action
  * @param {any} data
  * 
  * @return {object}
  */
 export const resultConnector = (success, service, action, data) => {
    // Prepare the basic result information
    var result = {
        'act': service,
        'a': action,
        's': (success ? 1 : 0)
    };

    // Check if it's successfull or failed
    if (success) {
        result.dt = data;
    } else {
        result.err = data;
    }

    return result;
}