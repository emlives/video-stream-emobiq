/**
 * Reducer Action to be passed to reducer (payload)
 * @param {string} type - The type/function to be triggered
 * @param {object} stateName - The state name affected
 * @param {object} payload - Payload to be used
 * 
 * @return {object}
 */
export default action = (type, stateName, payload) => (
    {
        type: type,
        stateName: stateName,
        payload: payload
    }
)