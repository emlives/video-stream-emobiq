/**
 * Core functions needed for redux
 */
// Built in actions
import { ReducerActionResetState } from '../Constant';

// Function used outside the framework
import { name as setComponentValue, reducerLogic as reducerLogicSetComponentValue } from '../../function/app/SetComponentValue';
import { name as setComponentFocus, reducerLogic as reducerLogicSetComponentFocus } from '../../function/app/SetComponentFocus';
import { name as setComponentElAttr, reducerLogic as reducerLogicSetComponentElAttr } from '../../function/app/SetComponentElAttr';
import { name as setComponentAttr, reducerLogic as reducerLogicSetComponentAttr } from '../../function/app/SetComponentAttr';
import { name as hideElement, reducerLogic as reducerLogicHideElement } from '../../function/app/HideElement';
import { name as showElement, reducerLogic as reducerLogicShowElement } from '../../function/app/ShowElement';
import { name as disablePage, reducerLogic as reducerLogicDisablePage } from '../../function/app/DisablePage';
import { name as enablePage, reducerLogic as reducerLogicEnablePage } from '../../function/app/EnablePage';
import { name as loadData, reducerLogic as reducerLogicLoadData } from '../../function/dataset/LoadData';
import { name as setComboOptions, reducerLogic as reducerLogicSetComboOptions } from '../../function/app/SetComboOptions';

// Functions used within the framework itself
import { name as coreDatasetLoadData, reducerLogic as reducerLogicCoreDatasetLoadData } from '../../function/core/dataset/CoreDatasetLoadData';
import { name as coreStateSetProperties, reducerLogic as reducerLogicCoreStateSetProperties } from '../../function/core/state/CoreStateSetProperties';
import { name as coreStateSetElementAttributes, reducerLogic as reducerLogicCoreStateSetElementAttributes } from '../../function/core/state/CoreStateSetElementAttributes';
import { name as coreStateInitializePage, reducerLogic as reducerLogicCoreStateInitializePage } from '../../function/core/state/CoreStateInitializePage';

/**
 * Wrapper to manipulate reducer state that is used eMOBIQ framework
 * @param {string} stateName - The state name to be used as an identifier for reducer logic
 * @param {object} realState - The real state from reducer
 * 
 * @return {object}
 */
 export default wrapper = (stateName, realState) => (state = realState, action) => {
    // Make sure to touch only the right state data
    if (stateName !== action.stateName) {
        return state;
    }

    // Trigger the function that was called
    switch (action.type) {
        // Normal functions
        case setComponentValue: 
            return reducerLogicSetComponentValue(state, action.payload);
        case setComponentFocus: 
            return reducerLogicSetComponentFocus(state, action.payload);
        case setComponentElAttr: 
            return reducerLogicSetComponentElAttr(state, action.payload);
        case setComponentAttr: 
            return reducerLogicSetComponentAttr(state, action.payload);
        case hideElement: 
            return reducerLogicHideElement(state, action.payload);
        case showElement: 
            return reducerLogicShowElement(state, action.payload);
        case disablePage: 
            return reducerLogicDisablePage(state, action.payload);
        case enablePage: 
            return reducerLogicEnablePage(state, action.payload);
        case loadData: 
            return reducerLogicLoadData(state, action.payload);
        case setComboOptions: 
            return reducerLogicSetComboOptions(state, action.payload);
        // Functions used within the framework itself
        case coreDatasetLoadData: 
            return reducerLogicCoreDatasetLoadData(state, action.payload);
        case coreStateSetProperties: 
            return reducerLogicCoreStateSetProperties(state, action.payload);
        case coreStateSetElementAttributes: 
            return reducerLogicCoreStateSetElementAttributes(state, action.payload);
        case coreStateInitializePage: 
            return reducerLogicCoreStateInitializePage(state, action.payload);
        // Reset the state to initial state
        case ReducerActionResetState:
            return realState;
        default:
            return state;
    }
}