/**
 * Contains the functionality to get and 
 * manipulate the settings of the app framework.
 */

import { PrefixFramework, NameSetting as name } from './Constant';

import { getData as getDataAsyncStorage, storeData as storeDataAsyncStorage } from '../plugin/AsyncStorage';
import { errorLog } from './Log';

/**
 * Get the specific settings of the framework
 * from the async storage.
 * @param {string} setting
 * 
 * @return {promise} - {string/object}
 */
export const getSetting = (setting) => {
    let resultAyncStorage = getDataAsyncStorage(PrefixFramework + name);

    // Check if the plugin have no issue
    if (!resultAyncStorage.success) {
        errorLog('Framework - Utility - Setting - setSetting: ' + resultAyncStorage.message);
        return false;
    }

    // Prepare the settings object
    let settings = {};

    // Check if there is any data then use that
    if (resultAyncStorage.data) {
        settings = JSON.parse(resultAyncStorage.data);
    }

    return typeof settings[setting] !== 'undefined' ? settings[setting] : null;
}

/**
 * Set the settings of the framework
 * from the async storage.
 * @param {string} setting
 * @param {string} value
 * 
 * @return {promise} - {bool}
 */
export const setSetting = (setting, value) => {
    // Get the whole settings information in the async storage
    let resultAyncStorage = getDataAsyncStorage(PrefixFramework + name);

    // Check if the plugin have no issue
    if (!resultAyncStorage.success) {
        errorLog('Framework - Utility - Setting - setSetting: ' + resultAyncStorage.message);
        return false;
    }

    // Prepare the settings to be inserted
    let settingsData = '';
    try {
        // Prepare the settings object
        let settings = {};

        // Check if there is any data then use that
        if (resultAyncStorage.data) {
            settings = JSON.parse(resultAyncStorage.data);
        }

        // Update the setting specified
        settings[setting] = value;

        // Convert to string
        settingsData = JSON.stringify(settings);
    } catch (error) {
        errorLog('Framework - Utility - Setting - setSetting: ' + error);
        return false;
    }

    // Insert the update to the async storage
    resultAyncStorage = storeDataAsyncStorage(PrefixFramework + name, settingsData, true);

    // Check if the plugin have no issue
    if (!resultAyncStorage.success) {
        errorLog('Framework - Utility - Setting - setSetting: ' + resultAyncStorage.message);
        return false;
    }

    return true;
}