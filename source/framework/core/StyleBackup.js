/**
 * Reusable functions for components
 */
import Theme from '../../dependency/style/Theme';
import transform from 'css-to-react-native';

/**
 * Get the style of the specified component
 * - First from the props
 * - Then from the theme
 * @param {string} name 
 * @param {object} props
 * 
 * @return {any}
 */
export const getStyle = (name, props) => {
    // Get from the props passed
    const data = props && props.data ? props.data : null;
    const style = data ? (data.style ? data.style : {}) : {};
    
    // Get the name from base style
    let combinedStyle = {}
    if (Theme[name] && props.parentStyle) {
        combinedStyle = { ...props.parentStyle, ...Theme[name], ...style };
    } else if (Theme[name]) {
        combinedStyle = { ...Theme[name], ...style };
    } else if (props.parentStyle) {
        combinedStyle = { ...props.parentStyle, ...style };
    } else {
        combinedStyle = Object.keys(style).length > 0 ? style : '';
    }
    
    switch(name) {
        case 'label':
            return getLabelStyleComponent(combinedStyle);
        case 'button':
            return getButtonStyleComponent(combinedStyle);
        case 'checkBox':
            return getCheckBoxStyleComponent(combinedStyle);
        case 'comboBox':
            return getComboBoxStyleComponent(combinedStyle);
        case 'datalist':
            return getDataListStyleComponent(combinedStyle);
        case 'edit':
            return getEditStyleComponent(combinedStyle);
        case 'image':
            return getImageStyleComponent(combinedStyle);
        case 'memo':
            return getMemoStyleComponent(combinedStyle);
        case 'page':
            return getPageStyleComponent(combinedStyle);
        case 'panel':
            return getPanelStyleComponent(combinedStyle);
        case 'signature':
            return getSignatureStyleComponent(combinedStyle);
        case 'snippet':
        case 'snippetPopUp':
            return getSnippetStyleComponent(combinedStyle);
        default:
            return combinedStyle;
    }
}

// converts css dimensions e.g %/em/px/pt to react-native
const cssToRnDim = (value) => {
    const emSize = 16; 
    const ptSize = 1;

    if (value.search(/[\d.]+%/) !== -1){
        return `${value.trim()}`;
    }

    if (value.search(/[\d.]+em/) !== -1) {
        return parseFloat(value.replace("em", "")) * emSize;
    }

    if (value.search(/[\d.]+px/) !== -1){
        return parseFloat(value.replace("px", ""));
    }

    if (value.search(/[\d.]+pt/) !== -1) {
        return parseFloat(value.replace("pt", "")) * ptSize;
    }

    return 0;
}

const fillComponentStyleWithSupportedAttr = (style, componentStyle, supportedAttributesForStyle) => {
    supportedAttributesForStyle.forEach(function(attributeName) {
        if (componentStyle[attributeName]) {
            style[attributeName] = componentStyle[attributeName];
        }
    });

    return style
}

/**
 * Act as a protocol to handle css's background-size attribute
 * https://www.w3schools.com/cssref/playit.asp?filename=playcss_background-size&preval=auto
 * @param {string} backgroundSizeInput - The value of css's background-size attribute
 * @param {function} widthHandler - Calls the caller's implemention e.g when background-size:200px
 * @param {function} widthHeightHandler - Calls the caller's implemention e.g when background-size:10px 150%
 */
const backgroundSizeHandler = ( backgroundSizeInput, widthHandler, widthHeightHandler) => {
    if (backgroundSizeInput === "") {
        return;
    }

    const sizes = backgroundSizeInput.trim().split(/\s+/)//input.trim().split(/\s+/).length
    if (sizes.length == 1) {
        const w = cssToRnDim(sizes[0]);
        widthHandler(w);
    } else if (sizes.length == 2) {
        const w = cssToRnDim(sizes[0]);
        const h = cssToRnDim(sizes[1]);
        widthHeightHandler(w, h);
    }
}

/**
 * Helper function to handle css's background-image attribute, when its value is e.g. linear-gradient(to right, red, blue)
 * https://www.w3schools.com/css/css3_gradients.asp
 * @param {obj} componentStyle - The state properties of a component
 * @param {function} widthHandler - Calls the caller's implemention when background-size:200px
 * @param {function} widthHeightHandler - Calls the caller's implemention when background-size:10px 150px
 */
const getLinearGradient = (backgroundImage, backgroundSize) => {
    let gradient = {
        colors:['transparent', 'transparent'],
        point: {start:{x: 0, y: 0},end:{x: 0, y: 0}},
        style: {flex: 1, position:'absolute', width:'100%', height: '100%'}
    }

    const widthHandler = (width) => {
        gradient.style.width = width
    }

    const widthHeightHandler = (width, height) => {
        gradient.style.width = width
        gradient.style.height = height
    }

    if (!backgroundImage?.uri) {
        let gradientAttributes = []
        if (backgroundImage.indexOf("linear-gradient") > -1) {
            const gradientInputString = backgroundImage.match(/\(([^)]+)\)/)[1] // get value in parenthesis  ()
            if (gradientInputString) {
                gradientAttributes = gradientInputString.split(",").map(item => item.trim());
                const firstAttribute = gradientAttributes[0];

                if (firstAttribute && (firstAttribute.indexOf('to right')>-1 || firstAttribute.indexOf('to left')>-1)) {
                    if (gradientAttributes.length >= 3) {
                        gradientAttributes.shift(); // remove the first element in array, leaving only the colors
                        gradient.colors = gradientAttributes
                        switch (firstAttribute) {
                            case 'to right':
                                gradient.point = {start:{ x: 0, y: 0.5 },end:{ x: 1, y: 0.5}};
                                break;
                            case 'to left':
                                gradient.point = {start:{x: 1, y: 0.5},end:{x: 0, y: 0.5}};
                                break;
                        }
                    }
                } else {
                    // colors flow top to bottom
                    gradient.colors = gradientAttributes;
                    gradient.point = {start:{  x: 0.5, y: 0 },end:{  x: 0.5, y: 1}};
                }
            }
        }

        backgroundSizeHandler(backgroundSize, widthHandler, widthHeightHandler);
    }

    return gradient
}
/**
 * Helper function to handle css's background-image attribute, when its value not linear-gradient
 * https://www.w3schools.com/cssref/playit.asp?filename=playcss_background-size&preval=50%25
 * @param {string} backgroundImage - Value of css's background-image attribute
 * @param {string} backgroundRepeat - Value of css's background-repeat attribute
 * @param {string} backgroundSize - Value of css's background-size attribute
 */
const getImageBackground = (backgroundImage, backgroundRepeat, backgroundSize) => {
    let bg = {
        source: null,
        resizeMethod: 'auto',
        imageStyle: {
            resizeMode: 'cover',
        },
        style: {flex: 1, position:'absolute', width:'100%', height: '100%'}
    };

    const widthHandler = (width) => {
        bg.imageStyle.width = width
    }

    const widthHeightHandler = (width, height) => {
        bg.imageStyle.width = width
        bg.imageStyle.height = height
        bg.imageStyle.resizeMode = 'stretch'
    }
    
    bg.source =  {uri:backgroundImage?.uri};

    if (backgroundRepeat == 'repeat' ) {
        bg.imageStyle.resizeMode = 'repeat';
    } else if (backgroundSize == 'cover' || backgroundSize == 'contain' ) {
        bg.imageStyle.resizeMode = backgroundSize;
    } else {
        backgroundSizeHandler(backgroundSize, widthHandler, widthHeightHandler);
    }

    return bg;
}

const paddingHandler = (componentStyle, subcomponentStyleProperties) => {
    let viewPaddingStyle = {}
    if(componentStyle['padding']) {
        const paddingTuple = ['padding', componentStyle['padding']] //e.g. padding: 10px 10px => ['padding', '10px 10px']
        let paddingValues = transform([paddingTuple]);
        for (var currentDirection in paddingValues) {
            if (paddingValues.hasOwnProperty(currentDirection)) {
                viewPaddingStyle[currentDirection] = paddingValues[currentDirection]; //e.g. value of paddingTop
            }
        }
    }

    let viewStyleWithNoPadding = {}
    const paddingsPropertyOrder = ['paddingTop', 'paddingRight', 'paddingBottom', 'paddingLeft']
    subcomponentStyleProperties.forEach(property => {
        if (componentStyle.hasOwnProperty(property)) {
            if (!paddingsPropertyOrder.includes(property)) {
                viewStyleWithNoPadding[property] = componentStyle[property];
            } else {
                viewPaddingStyle[property] = componentStyle[property]
            }
        }
    });

   return { ...viewStyleWithNoPadding, ...viewPaddingStyle};
}

const TextStyleAttributes = [
    "letterSpacing", 
    "textTransform", 
    "fontSize", 
    "color", 
    "fontFamily", 
    "fontStyle", 
    "fontWeight", 
    "fontVariant", 
    "textShadowOffset", 
    "textShadowRadius", 
    "textShadowColor", 
    "lineHeight", 
    "textAlign", 
    "textAlignVertical", 
    "includeFontPadding", 
    "textDecorationLine", 
    "textDecorationStyle", 
    "textDecorationColor", 
    "writingDirection", 
    "textShadow"
];

/**
 * Get style for Label component
 * @param {object} componentStyle
 * @return {object}
 */
const getLabelStyleComponent = (componentStyle) => {
    let textStyle = {};
    let viewStyle = {};
   
    const textAttributes = [...TextStyleAttributes,
        'padding', 'paddingTop', 'paddingRight', 'paddingBottom', 'paddingLeft'];
    const allAttributes = Object.keys(componentStyle);
    const viewAttributes = allAttributes.filter(x => !textAttributes.includes(x));

    viewStyle = fillComponentStyleWithSupportedAttr(viewStyle, componentStyle, viewAttributes);
    textStyle = fillComponentStyleWithSupportedAttr(textStyle, componentStyle, textAttributes);

    viewStyle = cssToRNStyle(viewStyle, "View", { emSize: 16, ptSize: 1});

    const textStyleProperties = Object.keys(textStyle)
    textStyle = paddingHandler(componentStyle, textStyleProperties)
    textStyle = cssToRNStyle(textStyle, "Text", { emSize: 16, ptSize: 1});

    // const bgImage = componentStyle['backgroundImage']?componentStyle['backgroundImage']:'';
    // const bgRepeat = componentStyle['backgroundRepeat'];
    // const bgSize = componentStyle['backgroundSize']?componentStyle['backgroundSize']:'';

    // let imageBackground = getImageBackground(bgImage, bgRepeat, bgSize);
    // let linearGradient = getLinearGradient(bgImage, bgSize);

    // let data = {
    //     view: {style: viewStyle},
    //     text: {style: textStyle},
    //     imageBackground: imageBackground,
    //     linearGradient: linearGradient
    // };

    let data = {
        view: {style: viewStyle},
        text: {style: textStyle},
    };
    
    return data;
}

/**
 * Get style for Button component
 * @param {object} componentStyle
 * @return {object}
 */
const getButtonStyleComponent = (componentStyle) => {

    let textStyle = {};
    let touchableOpacityStyle = {};

    for (var key in componentStyle) {
        if (componentStyle.hasOwnProperty(key)) {
            if (TextStyleAttributes.includes(key)) {
                textStyle[key] = componentStyle[key];
            } else {
                touchableOpacityStyle[key] = componentStyle[key];
            }
        }
    }

    let data = {
        text: { style: cssToRNStyle(textStyle, "Text", { emSize: 16, ptSize: 1}) },
        touchableOpacity: { style: cssToRNStyle(touchableOpacityStyle, "View", { emSize: 16, ptSize: 1}) }
    };
    
    return data;
}

/**
 * Get style for CheckBox component
 * @param {object} componentStyle
 * @return {object}
 */
const getCheckBoxStyleComponent = (componentStyle) => {

    let data = {
        view: { style: cssToRNStyle(componentStyle, "View", { emSize: 16, ptSize: 1 }) }
    };
    
    return data;
}

/**
 * Get style for ComboBox component
 * @param {object} componentStyle
 * @return {object}
 */
const getComboBoxStyleComponent = (componentStyle) => {

    let containerStyle = {};
    let style = {};
    const containerStyleAttributes = [
        "width", 
        "height", 
        "position", 
        "top", 
        "bottom", 
        "right", 
        "left", 
        "display"
    ];

    for (var key in componentStyle) {
        if (componentStyle.hasOwnProperty(key)) {
            if (containerStyleAttributes.includes(key)) {
                containerStyle[key] = componentStyle[key];
            } else {
                style[key] = componentStyle[key];
            }
        }
    }

    let data = {
        dropdownPicker: {
            containerStyle: cssToRNStyle(containerStyle, "View", { emSize: 16, ptSize: 1}),
            style: cssToRNStyle(style, "Text", { emSize: 16, ptSize: 1})
        }
    };
    
    return data;
}

/**
 * Get style for DataList component
 * @param {object} componentStyle
 * @return {object}
 */
const getDataListStyleComponent = (componentStyle) => {

    let data = {
        unfilteredStyle: componentStyle,
        flatList: { style: cssToRNStyle(componentStyle, "Text", { emSize: 16, ptSize: 1}) }
    };
    
    return data;
}

/**
 * Get style for Edit component
 * @param {object} componentStyle
 * @return {object}
 */
const getEditStyleComponent = (componentStyle) => {

    let viewStyle = {};
    let textInputStyle = {};
    let caretColor = "black";
    const textAttributes = [...TextStyleAttributes,
        'padding', 'paddingTop', 'paddingRight', 'paddingBottom', 'paddingLeft', 'margin', 'marginTop', 'marginRight', 'marginBottom', 'marginLeft'];
    for (var key in componentStyle) {
        if (componentStyle.hasOwnProperty(key)) {
            if (key == "caretColor") {
                caretColor = componentStyle[key];
            } else if (textAttributes.includes(key)) {
                textInputStyle[key] = componentStyle[key];
            } else {
                viewStyle[key] = componentStyle[key];
            }
        }
    }

    let data = {
        view: { style: cssToRNStyle(viewStyle, "View", { emSize: 16, ptSize: 1}) },
        textInput: { 
            style: cssToRNStyle(textInputStyle, "Text", { emSize: 16, ptSize: 1}),
            caretColor: caretColor 
        }
    };
    
    return data;
}

/**
 * Get style for Image component
 * @param {object} componentStyle
 * 
 * @return {object}
 */
const getImageStyleComponent = (componentStyle) => {
    // 1. handles viewStyle
    let viewStyle = { width:100, height:100 } // default dimensions for image component in editor
    const imageAttributes = ['backgroundImage', 'backgroundSize', 'backgroundPosition', 'backgroundAttachment','backgroundImage', 'backgroundRepeat', 'backgroundAttachment'];
    const allAttributes = Object.keys(componentStyle);
    const viewAttributes = allAttributes.filter(x => !imageAttributes.includes(x));

    viewStyle = fillComponentStyleWithSupportedAttr(viewStyle, componentStyle, viewAttributes);

    // 2. handles background-size for imageStyle
    let imageStyle = { width: '100%', height: '100%' } // default dimensions for image component in editor
    const imageWidthHandler = (width) => {
        imageStyle.width = width;
    };

    const imageWidthHeightHandler = (width, height) => {
        imageStyle.width = width;
        imageStyle.height = height;
    };

    const backgroundSize = componentStyle['backgroundSize'];
    if (backgroundSize) {
        backgroundSizeHandler(backgroundSize, imageWidthHandler, imageWidthHeightHandler);
    }

    const textAlign = componentStyle['textAlign']
    if (textAlign) { // View doesn't support textAlign but it supports alignSelf
        let alignMapper = {
            left: 'flex-start',
            right: 'flex-end',
            center: 'center',
            justify: 'flex-start',
            auto: 'auto'
        };
        if (["left", "right", "justify", "auto", "center"].indexOf(textAlign) !== -1) {
            viewStyle["alignSelf"] = alignMapper[textAlign];
        }
        if (textAlign === "start") {
            viewStyle["alignSelf"] = alignMapper["left"];
        }
        if (textAlign === "end") {
            viewStyle["alignSelf"] = alignMapper["right"];
        }
    }

    // 3. transform values to RN format
    viewStyle = cssToRNStyle(viewStyle, "View", { emSize: 16, ptSize: 1});
    imageStyle = cssToRNStyle(imageStyle, "Image", { emSize: 16, ptSize: 1});

    let data = {
        view: {style: viewStyle},
        image: { style: imageStyle},
    };
    
    return data;
}

/**
 * Get style for Memo component
 * @param {object} componentStyle
 * @return {object}
 */
const getMemoStyleComponent = (componentStyle) => {

    let viewStyle = {};
    let textInputStyle = {};
    const textInputStyleAttributes = [...TextStyleAttributes, "margin", "marginTop", "marginLeft", "marginBottom", "marginRight", "padding", "paddingTop", "paddingLeft", "paddingBottom", "paddingRight"];

    for (var key in componentStyle) {
        if (componentStyle.hasOwnProperty(key)) {
            if (textInputStyleAttributes.includes(key)) {
                textInputStyle[key] = componentStyle[key];
            } else {
                viewStyle[key] = componentStyle[key];
            }
        }
    }

    let data = {
        view: { style: cssToRNStyle(viewStyle, "View", { emSize: 16, ptSize: 1}) },
        textInput: { style: cssToRNStyle(textInputStyle, "Text", { emSize: 16, ptSize: 1}) }
    };
    
    return data;
}

/**
 * Get style for Page component
 * @param {object} componentStyle
 * @return {object}
 */
const getPageStyleComponent = (componentStyle) => {

    let data = {
        view: { style: cssToRNStyle(componentStyle, "View", { emSize: 16, ptSize: 1}) }
    };
    
    return data;
}

/**
 * Get style for Panel component
 * @param {object} componentStyle
 * @return {object}
 */
const getPanelStyleComponent = (componentStyle) => {

    let viewStyle = {};
    let scrollViewStyle = {};
    const scrollviewContainerStyleAttributes = ["flexWrap", "flexDirection", "display", "justifyContent"];

    for (var key in componentStyle) {
        if (componentStyle.hasOwnProperty(key)) {
            if (["height"].includes(key)) {
                scrollViewStyle[key] = componentStyle[key];
                viewStyle[key] = componentStyle[key];
            }
            else if (scrollviewContainerStyleAttributes.includes(key)) {
                scrollViewStyle[key] = componentStyle[key];
            } else {
                viewStyle[key] = componentStyle[key];
            }
        }
    }

    // complete list of css's vertical-align values:
    // baseline, length, %, sub, super, top, text-top, bottom, middle, text-bottom, initial, inherit
    const cssVerticalAlignValue = componentStyle['verticalAlign'];
    if (cssVerticalAlignValue) { 
        let rnJustifyContentDict = {
            //CSS.verticalAlign.value = RN.justifyContent.value
            top: "flex-start",
            middle: "center",
            bottom: "flex-end",
        };

        if(Object.keys(rnJustifyContentDict).indexOf(cssVerticalAlignValue) !== -1) {
            scrollViewStyle.justifyContent = rnJustifyContentDict[cssVerticalAlignValue]
        }
    }

    // handle padding
    const viewStyleProperties = Object.keys(viewStyle)
    viewStyle = paddingHandler(componentStyle, viewStyleProperties)

    let data = {
        unfilteredStyle: componentStyle,
        view: { style: cssToRNStyle(viewStyle, "View", { emSize: 16, ptSize: 1}) },
        scrollView: { style: cssToRNStyle(scrollViewStyle, "View", { emSize: 16, ptSize: 1}) }
    };
    
    return data
}

/**
 * Get style for Signature component
 * @param {object} componentStyle
 * @return {object}
 */
const getSignatureStyleComponent = (componentStyle) => {

    let data = {
        signatureCapture: { 
            style: cssToRNStyle(componentStyle, "Text", { emSize: 16, ptSize: 1}) 
        }
    };
    
    return data;
}

/**
 * Get style for Snippet component
 * @param {object} componentStyle
 * @return {object}
 */
const getSnippetStyleComponent = (componentStyle) => {

    let data = {
        view: { style: cssToRNStyle(componentStyle, "View", { emSize: 16, ptSize: 1}) }
    };
    
    return data;
}

// Style properties for RN Image Component
const ImageStylePropTypes = [
    "display",
    "width",
    "height",
    "start",
    "end",
    "top",
    "left",
    "right",
    "bottom",
    "minWidth",
    "maxWidth",
    "minHeight",
    "maxHeight",
    "margin",
    "marginVertical",
    "marginHorizontal",
    "marginTop",
    "marginBottom",
    "marginLeft",
    "marginRight",
    "marginStart",
    "marginEnd",
    "padding",
    "paddingVertical",
    "paddingHorizontal",
    "paddingTop",
    "paddingBottom",
    "paddingLeft",
    "paddingRight",
    "paddingStart",
    "paddingEnd",
    "borderWidth",
    "borderTopWidth",
    "borderStartWidth",
    "borderEndWidth",
    "borderRightWidth",
    "borderBottomWidth",
    "borderLeftWidth",
    "position",
    "flexDirection",
    "flexWrap",
    "justifyContent",
    "alignItems",
    "alignSelf",
    "alignContent",
    "overflow",
    "flex",
    "flexGrow",
    "flexShrink",
    "flexBasis",
    "aspectRatio",
    "zIndex",
    "direction",
    "shadowColor",
    "shadowOffset",
    "shadowOpacity",
    "shadowRadius",
    "transform",
    "transformMatrix",
    "decomposedMatrix",
    "scaleX",
    "scaleY",
    "rotation",
    "translateX",
    "translateY",
    "resizeMode",
    "backfaceVisibility",
    "backgroundColor",
    "borderColor",
    "borderRadius",
    "tintColor",
    "opacity",
    "overlayColor",
    "borderTopLeftRadius",
    "borderTopRightRadius",
    "borderBottomLeftRadius",
    "borderBottomRightRadius",
];

// Style properties for RN View Component
const ViewStylePropTypes = [
    "boxShadow",
    "filter",
    "color",
    "display",
    "width",
    "height",
    "start",
    "end",
    "top",
    "left",
    "right",
    "bottom",
    "minWidth",
    "maxWidth",
    "minHeight",
    "maxHeight",
    "margin",
    "marginVertical",
    "marginHorizontal",
    "marginTop",
    "marginBottom",
    "marginLeft",
    "marginRight",
    "marginStart",
    "marginEnd",
    "padding",
    "paddingVertical",
    "paddingHorizontal",
    "paddingTop",
    "paddingBottom",
    "paddingLeft",
    "paddingRight",
    "paddingStart",
    "paddingEnd",
    "border",
    "borderWidth",
    "borderTopWidth",
    "borderStartWidth",
    "borderEndWidth",
    "borderRightWidth",
    "borderBottomWidth",
    "borderLeftWidth",
    "position",
    "flexDirection",
    "flexWrap",
    "justifyContent",
    "alignItems",
    "alignSelf",
    "alignContent",
    "overflow",
    "flex",
    "flexGrow",
    "flexShrink",
    "flexBasis",
    "aspectRatio",
    "zIndex",
    "direction",
    "shadowColor",
    "shadowOffset",
    "shadowOpacity",
    "shadowRadius",
    "transform",
    "transformMatrix",
    "decomposedMatrix",
    "scaleX",
    "scaleY",
    "rotation",
    "translateX",
    "translateY",
    "backfaceVisibility",
    "backgroundColor",
    "border",
    "borderColor",
    "borderTopColor",
    "borderRightColor",
    "borderBottomColor",
    "borderLeftColor",
    "borderStartColor",
    "borderEndColor",
    "borderRadius",
    "borderTopLeftRadius",
    "borderTopRightRadius",
    "borderTopStartRadius",
    "borderTopEndRadius",
    "borderBottomLeftRadius",
    "borderBottomRightRadius",
    "borderBottomStartRadius",
    "borderBottomEndRadius",
    "borderStyle",
    "opacity",
    "elevation",
    "fontSize"
];

// Style properties for RN Text Component
const TextStylePropTypes = [
    "textShadow",
    "display",
    "width",
    "height",
    "start",
    "end",
    "top",
    "left",
    "right",
    "bottom",
    "minWidth",
    "maxWidth",
    "minHeight",
    "maxHeight",
    "margin",
    "marginVertical",
    "marginHorizontal",
    "marginTop",
    "marginBottom",
    "marginLeft",
    "marginRight",
    "marginStart",
    "marginEnd",
    "padding",
    "paddingVertical",
    "paddingHorizontal",
    "paddingTop",
    "paddingBottom",
    "paddingLeft",
    "paddingRight",
    "paddingStart",
    "paddingEnd",
    "borderWidth",
    "borderTopWidth",
    "borderStartWidth",
    "borderEndWidth",
    "borderRightWidth",
    "borderBottomWidth",
    "borderLeftWidth",
    "position",
    "flexDirection",
    "flexWrap",
    "justifyContent",
    "alignItems",
    "alignSelf",
    "alignContent",
    "overflow",
    "flex",
    "flexGrow",
    "flexShrink",
    "flexBasis",
    "aspectRatio",
    "zIndex",
    "direction",
    "shadowColor",
    "shadowOffset",
    "shadowOpacity",
    "shadowRadius",
    "transform",
    "transformMatrix",
    "decomposedMatrix",
    "scaleX",
    "scaleY",
    "rotation",
    "translateX",
    "translateY",
    "backfaceVisibility",
    "backgroundColor",
    "borderColor",
    "borderTopColor",
    "borderRightColor",
    "borderBottomColor",
    "borderLeftColor",
    "borderStartColor",
    "borderEndColor",
    "borderRadius",
    "borderTopLeftRadius",
    "borderTopRightRadius",
    "borderTopStartRadius",
    "borderTopEndRadius",
    "borderBottomLeftRadius",
    "borderBottomRightRadius",
    "borderBottomStartRadius",
    "borderBottomEndRadius",
    "borderStyle",
    "opacity",
    "elevation",
    "color",
    "fontFamily",
    "fontSize",
    "fontStyle",
    "fontWeight",
    "fontVariant",
    "textShadowOffset",
    "textShadowRadius",
    "textShadowColor",
    "letterSpacing",
    "lineHeight",
    "textAlign",
    "textAlignVertical",
    "includeFontPadding",
    "textDecorationLine",
    "textDecorationStyle",
    "textDecorationColor",
    "textTransform",
    "writingDirection",
];

// Three main component types based on style
const stylePropTypes = {
    ["View"]: ViewStylePropTypes,
    ["Text"]: TextStylePropTypes,
    ["Image"]: ImageStylePropTypes,
};

// properties that support percentage value
const PercentSupportedStyles = [
    "width",
    "height",
    "top",
    "bottom",
    "left",
    "right",
    "margin",
    "marginBottom",
    "marginTop",
    "marginLeft",
    "marginRight",
    "marginHorizontal",
    "marginVertical",
    "padding",
    "paddingBottom",
    "paddingTop",
    "paddingLeft",
    "paddingRight",
    "paddingHorizontal",
    "paddingVertical",
];

const inheritableProperties = [ 
    "color",
    "cursor",
    "direction",
    "elevation",
    "font-family",
    "fontFamily",
    "font-size",
    "fontSize",
    "font-style",
    "fontStyle",
    "font-variant",
    "fontVariant",
    "font-weight",
    "fontWeight",
    "font",
    "letter-spacing",
    "letterSpacing",
    "line-height",
    "lineHeight",
    "text-align",
    "textAlign",
    "text-transform",
    "textTransform",
    "textShadow",
    "textDecorationLine",
    "textDecorationStyle",
    "textDecorationColor",
    "textShadowOffset",
    "textShadowRadius",
    "textShadowColor",
];

export const AbsoluteFontSize = {
    "medium": 14,
    "xx-small": 8.5,
    "x-small": 10,
    "small": 12,
    "large": 17,
    "x-large": 20,
    "xx-large": 24,
    "smaller": 13.3,
    "larger": 16,
    "length": null,
    "initial": null,
    "inherit": null,
    "unset": null
};

/**
 * @param {string} key: the key of style
 * @param {string} value: the value of style
 * @return {array}
 */
function mapAbsoluteFontSize(key, value) {
    let fontSize = value;
    if (AbsoluteFontSize.hasOwnProperty(value)) {
        fontSize = AbsoluteFontSize[value];
    }
    return [key, fontSize];
}

/**
 * Converts a html style to its equivalent react native style
 * @param {object} css: object of key value css strings
 * @param {string} styleset: the styleset to convert the styles against
 * @param {object} { parentTag, emSize }
 * @returns {object}
 */
const cssToRNStyle = (css, styleset, { emSize, ptSize }) => {
    const styleProps = stylePropTypes[styleset];

    return Object.keys(css)
        .map((key) => [key, css[key]])
        .map(([key, value]) => {
            // Key convert
            return [
                key
                .split("-")
                .map((item, index) =>
                    index === 0 ? item : item[0].toUpperCase() + item.substr(1)
                )
                .join(""),
                value,
            ];
        })
        .map(([key, value]) => {
            if (styleProps.indexOf(key) === -1) {
                return undefined;
            }
            if (typeof value === "string") {
                value = value.replace(/\s*!\s*important/, "");
                if (!value) {
                    return undefined 
                }
                // css margin/padding shorthand is handled in reduce
                if (key === 'margin' || key === 'padding' || key === 'boxShadow' || key === 'textShadow' || key === 'border') {
                    return [key, value];
                }
                if (key === "display" && value === "block") {
                    return ["flexDirection", "column"];
                }
                if (key === "display" && (value === "inline" || value === "inline-block" || value === "table")) {
                    return ["flexDirection", "row"];
                }
                if (key === "display" && ["flex", "none"].indexOf(value) === -1) {
                    return [key, "flex"];
                }
                if (key === "position" && ["absolute", "relative"].indexOf(value) === -1) {
                    return undefined;
                }
                if (key === "textAlign") {
                    if (
                        ["left", "right", "justify", "auto", "center"].indexOf(value) !== -1
                    ) {
                        return [key, value];
                    }
                    if (value === "start") {
                        return [key, "left"];
                    }
                    if (value === "end") {
                        return [key, "right"];
                    }
                    return undefined;
                }
                if (
                    value
                        .replace(/[-_]/g, "")
                        .search(/\binherit\b|\bnormal\b|\binitial\b|\bauto\b|(calc|var)\(.*\)/) !== -1
                ) {
                    return undefined;
                }
                // See if we can use the percentage directly
                if (
                    value.search(/[\d.]+%/) !== -1 &&
                    PercentSupportedStyles.indexOf(key) !== -1
                ) {
                    return [key, value];
                }
                if (value.search(/[\d.]+em/) !== -1) {
                    const pxSize = parseFloat(value.replace("em", "")) * emSize;
                    return [key, pxSize];
                }
                if (value.search(/[\d.]+pt/) !== -1) {
                    const pxSize = parseFloat(value.replace("pt", "")) * ptSize;
                    return [key, pxSize];
                }
                // See if we can convert a 20px to a 20 automagically
                const numericValue = parseFloat(value.replace("px", ""));
                if (key !== "fontWeight" && !isNaN(numericValue)) {
                    if (styleProps.indexOf(key) !== -1) {
                        return [key, numericValue];
                    }
                }
                if (key === "fontSize") {
                    return mapAbsoluteFontSize(key, value);
                }
                if (key === "fontVariant") { // if fontVariant is string, convert to array because RN expects array as fontVariant value
                    return [key, [value]]
                }
            }
            return [key, value];
        })
        .filter((prop) => prop !== undefined)
        .reduce((acc, [key, value]) => {
            // handles margin/padding shorthand
            if ((key === 'margin' || key === 'padding' || key === 'textShadow' || key === 'border') && typeof value === "string") {
                try {
                    let spaces = transform([[key, value]]);
                    for (var side in spaces) {
                        if (spaces.hasOwnProperty(side)) {
                            acc[side] = spaces[side];
                        }
                    }
                } catch(err) {console.log(err)}
            } else if (key === 'boxShadow') {
                try {
                    // if the boxShadow value includes spread (e.q. 3px 3px 3px 6px black) remove the spread, react native doesn't support that
                    valueArray = value.split(" ");
                    if (valueArray.length == 5) {
                        valueArray.splice(3,1)
                    } else if (valueArray.length == 4 && /\d/.test(valueArray[3]) ) {
                        valueArray.splice(3,1)
                    }
                    let newValue = valueArray.join(" ")
                    let spaces = transform([[key, newValue]]);
                    for (var side in spaces) {
                        if (spaces.hasOwnProperty(side)) {
                            acc[side] = spaces[side];
                        }
                    }
                    
                } catch(err) {console.log(err)}
            } else {
                acc[key] = value;
            }
            return acc;
        }, {});
}

/**
 * Filter passed style and only return it with inheritable properties
 * @param {object} styleObject: object of key value RN style
 * @returns {object}
 */
export const filterInheritableStyles = (styleObject) => {
    return Object.keys(styleObject)
        .filter(key => inheritableProperties.includes(key))
        .reduce((obj, key) => {
            obj[key] = styleObject[key];
            return obj;
        }, {});
}

const toPx = (input, windowHeight) => {
    let output = null;
    if (typeof input === "string") {
        if (input.indexOf('%') > -1) {
            output = (windowHeight/100) * Number(input.replace('%', '').trim());
        } else if (input.indexOf('px') > -1) {
            output = Number(input.replace('px', '').trim());
        } 
    }else if (Number(input)) {
        output = Number(input);
    }
    return output;
}

/**
 * Return the calculated height of the ScrollView
 * @param {object} allProps: all props that belong to the page
 * @param {number} windowHeight: height of the visible screen/window
 * @returns {number}
 */
export const initContentHeight = (allProps, windowHeight) => {
    let scrollableHeight = 0;
    Object.keys(allProps).forEach(function(componentName) {
        const compDefaultHeight = 40;
        const compMarginBottom = 0;
        const style = allProps[componentName].style;
        if (style?.top) {
            const componentHeight = style.height?(toPx(style.height, windowHeight)+compMarginBottom):compDefaultHeight;
            const componentBottom = toPx(style.top, windowHeight) + componentHeight;

            if (componentBottom > scrollableHeight) {
                scrollableHeight = componentBottom;
            }
        }
    });

    scrollableHeight = scrollableHeight;
    return scrollableHeight;
}

/**
 * Formats the parent style layout
 * @param {object} props The component props
 */
export const formatParentStyleLayout = (props) => {

    // const floatHandler = (props, component) => {
    //     if (typeof props[component.parent] === 'undefined' || typeof props[component.parent].style === 'undefined') {
    //         return;
    //     }

    //     switch (component.style.float) {
    //         case 'left':
    //         case 'right':
    //             props[component.parent].style.flexDirection = 'row';
    //             break;
    //         default:
    //             props[component.parent].style.flexDirection = 'column';
    //             break;
    //     }
    // };

    // const displayHandler = (props, component) => {
    //     if (typeof props[component.parent] === 'undefined' || typeof props[component.parent].style === 'undefined') {
    //         return;
    //     }

    //     switch(component.style.display) {
    //         case 'table-cell':
    //         case 'inline-block':
    //             props[component.parent].style.flexDirection = 'row'
    //             break;
    //         default:
    //             props[component.parent].style.flexDirection = 'column'
    //             break;
    //     }
    // };

    // const zIndexHandler = (component) => {
    //     component.style.position = 'absolute'
    // };

    // // Loop through component props
    // Object.keys(props).forEach(name => {
    //     const component = props[name];
    //     if (component.style?.float && component.parent) {
    //         floatHandler(props, component)
    //     }
    //     if (component.style?.display && component.parent) {
    //         displayHandler(props, component)
    //     }
    //     if (component.style?.zIndex) {
    //         zIndexHandler(component)
    //     }
    // });
}