/**
 * Reusable functions for services
 */
import GlobalServices from '../../dependency/global/Service';

/**
 * Used to find service object by name
 * @param {any} name - identifier of the component
 * 
 * @return {object} - the class instance of the service
 */    
export const getServiceByName = (name) => {
    return typeof GlobalServices[name] !== 'undefined' ? GlobalServices[name] : null;
}