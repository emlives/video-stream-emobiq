/**
 * Reusable functions for components
 */
import React from 'react';
import DefaultSnippetState from '../../snippet/DefaultSnippetState';
import { getServiceByName } from '../core/Service';

import { errorLog } from '../core/Log';
import { generateUUID } from '../core/Helper';
import coreStateSetElementAttributes from '../function/core/state/CoreStateSetElementAttributes';

import EmobiqLabel from './component/Label'
import EmobiqButton from './component/Button'
import EmobiqEdit from './component/Edit'
import EmobiqPanel from './component/Panel'
import EmobiqComboBox from './component/ComboBox'
import EmobiqCheckBox from './component/CheckBox'
import EmobiqMemo from './component/Memo'
import EmobiqDataList from './component/DataList'
import EmobiqImage from './component/Image'
import EmobiqSignature from './component/Signature'

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Core - Component - ';

/**
 * Get the property value
 * @param {string} name 
 * @param {object} props 
 * @param {any} defaultValue - default value
 * 
 * @return {any}
 */
export const getPropertyValue = (name, props, defaultValue = null) => {
    // Get from the props passed
    const data = props.data ? props.data : null;

    if (data) {
        // Check if there's a data property for this component
        if (data.property !== undefined && typeof data.property[name] !== 'undefined') {
            return data.property[name];
        } else if (data.elementAttribute !== undefined && typeof data.elementAttribute[name] !== 'undefined') {
            return data.elementAttribute[name];
        }
    } 

    // Use the property of the component
    return props[name] ? props[name] : defaultValue;
}

/**
 * The function to recreate the component with the inherited style
 * @todo - This will keep rendering the whole children even when only one of the 
 *         component props was changed.
 * 
 * @param {object} components - the components to receive the inherited style
 * @param {object} style      - the style to inherit
 * 
 * @return {array} - the modified component
 */
 export const generateComponentWithInheritedStyle = (components, style) => {
    return React.Children.map(components, component => {
        // checking isValidElement is the safe way and avoids a typescript error too
        if (React.isValidElement(component)) {
            return React.cloneElement(component, { parentStyle: style });
        }
        return component;
    });
}

/**
 * Find if it is a component exists from the state
 * @param {object} state     - the state to be used either 
 *                             non mutable or mutable state (immerse.js)
 * @param {string} component - the component value to be used for the processing type
 * @param {string} type      - the processing type that is being requested
 *                           - default: name
 *                           -   Options:
 *                           -     name - find through component name 
 *                           -     uuid - find through the component uuid value (auto-generated)
 *                           -     id - find through the component id value
 *                           -     dataset - find through the component dataset value
 * @param {string} stateName - name of the page, used in initializing attribute and elementAttribute of the component
 * @return {boolean} - if it is a valid component or not
 */
export const isComponentExisting = (state, component, type = 'name', stateName) => {
    if (type == "name") {
        let service = getServiceByName(component)
        if (service) {
            return true;
        }
    }
    // Find the component state
    let componentState = findComponentState(state, component, type);

    // Check if it exist or not
    if (!componentState) {
        return false;
    }
    
    // if component doesn't have attribute or elementAttribute, initialize
    if (!componentState.attribute || !componentState.elementAttribute) {
        coreStateSetElementAttributes(stateName, state, {uuid: componentState.property.componentUUID, data:{}})
    }
    return true;
}

/**
 * Get the property of the component from the state
 * @param {object} state        - the state to be used either 
 *                                non mutable or mutable state (immerse.js)
 * @param {string} property     - the property to be taken 
 * @param {string} component    - the component value to be used for the processing type
 * @param {string} type         - the processing type that is being requested
 *                              - default: name
 *                              -   Options:
 *                              -     name - find through component name 
 *                              -     uuid - find through the component uuid value (auto-generated)
 *                              -     id - find through the component id value
 *                              -     dataset - find through the component dataset value
 * 
 * @return {any} - component state property
 */
export const getComponentStateProperty = (state, property, component, type = 'name') => {
    // Find the component state
    let componentState = findComponentState(state, component, type);

    // Validate the state, property and the specified property
    if (!componentState && !componentState.property) {
        return null;
    }

    if (componentState.property.componentType && componentState.property.componentType == "image") {
        return componentState.property[property].uri;
    }
    
    return componentState.property[property];
}

/**
 * 
 * Get the property of the component from the state
 * @param {object} state       - the state to be used either 
 *                               mutable state (immerse.js)
 * @param {object} data        - the property (key) and the data (value) to be updatdd in to the state
 * @param {string} component   - the component value to be used for the processing type
 * @param {string} type        - the processing type that is being requested
 *                             - default: name
 *                             -   Options:
 *                             -     name - find through component name 
 *                             -     uuid - find through the component uuid value (auto-generated)
 *                             -     id - find through the component id value
 *                             -     dataset - find through the component dataset value
 * 
 * @return {bool} - returns true if success and false if failed
 */
 export const setComponentStateProperties = (state, data, component, type = 'name', pageActions = {}) => {
    // Find the component state
    let componentState = findComponentState(state, component, type);
    
    // Validate the state and property
    if (!componentState && !componentState.property) {
        return false;
    }

    // Get the component type
    let componentType = '';
    if (componentState.property.componentType) {
        componentType = componentState.property.componentType;
    }

    // Convert to lowercase
    componentType = componentType.toLowerCase();

    // Loop through the properties to be inserted/updated
    for (let property in data) {
        // Check if the value must affect something else in the state
        switch (property) {
            case 'componentValue':
                // Snippet value
                if (componentType == 'snippet') {
                    componentState.property.componentData = DefaultSnippetState[data[property]];
                }
                
                // get the component to update related attributes
                let emobiqComponent = getEmobiqComponent(state, component, type);
                
                if (emobiqComponent && emobiqComponent.setComponentValue) {
                    emobiqComponent.setComponentValue(data[property]);
                } else {
                    continue;
                }
                  
                break;
            case 'componentData':
                // Datalist value 
                // No need to update the property itself
                if (componentType == 'datalist') {
                    // Get the data for the datalist
                    let records = (data[property].data ? data[property].data : data[property]);
                    let append = (data[property].append ? data[property].append : false);

                    // Validate the data and data item state template
                    if (!Array.isArray(records) || !componentState.template || !componentState.template.componentDataItemState) {
                        continue;
                    }

                    // Generate the datalist state
                    let dataListItems = populateDataListItems(componentState.template.componentDataItemState, records, pageActions);

                    // Make sure there is no error in populating
                    if (!dataListItems) {
                        continue;
                    }

                    // Replace the data to the item listing check if append
                    data[property] = (!append ? dataListItems : componentState.property[property].concat(dataListItems));
                }
                break;
        }

        // Update the property
        componentState.property[property] = data[property];
    }

    return true;
}

/**
 * Get the element attribute of the component from the state
 * @param {object} state        - the state to be used either 
 *                                non mutable or mutable state (immerse.js)
 * @param {string} attribute     - the attribute to be taken 
 * @param {string} component    - the component value to be used for the processing type
 * @param {string} type         - the processing type that is being requested
 *                              - default: name
 *                              -   Options:
 *                              -     name - find through component name 
 *                              -     id - find through the component id value
 *                              -     dataset - find through the component dataset value
 * 
 * @return {any} - component state element attribute
 */
export const getComponentStateElementAttribute = (state, attribute, component, type = 'name') => {
    // Find the component state
    let componentState = findComponentState(state, component, type);

    // Validate the state, attribute and the specified attribute
    if (!componentState || !componentState.elementAttribute) {
        return null;
    }

    return componentState.elementAttribute[attribute];
}

/**
 * 
 * Set the elementAttribute of the component from the state
 * @param {object} state       - the state to be used either 
 *                               mutable state (immerse.js)
 * @param {object} data        - the data (value) to be updated in to the state
 * @param {string} component   - the component value to be used for the processing type
 * @param {string} type        - the processing type that is being requested
 *                             - default: name
 *                             -   Options:
 *                             -     name - find through component name 
 *                             -     id - find through the component id value
 *                             -     dataset - find through the component dataset value
 * 
 * @return {bool} - returns true if success and false if failed
 */
 export const setComponentStateElementAttributes = (state, data={}, component, type = 'name') => {
    // get the component object
    let emobiqComponent = getEmobiqComponent(state, component, type);
    
    if (emobiqComponent && emobiqComponent.setElementAttributes) {
        emobiqComponent.setElementAttributes(data);
    } else {
        return false;
    }

    return true;
}

/**
 * Get the attribute of the component from the state
 * @param {object} state        - the state to be used either 
 *                                non mutable or mutable state (immerse.js)
 * @param {string} attribute     - the attribute to be taken 
 * @param {string} component    - the component value to be used for the processing type
 * @param {string} type         - the processing type that is being requested
 *                              - default: name
 *                              -   Options:
 *                              -     name - find through component name 
 *                              -     id - find through the component id value
 *                              -     dataset - find through the component dataset value
 * 
 * @return {any} - component state attribute
 */
 export const getComponentStateAttribute = (state, attribute, component, type = 'name') => {
    // Find the component state
    let componentState = findComponentState(state, component, type);

    // Validate the state, attribute and the specified attribute
    if (!componentState && !componentState.attribute) {
        return null;
    }

    return componentState.attribute[attribute];
}

/**
 * 
 * Set the attributes of the component from the state
 * @param {object} state       - the state to be used either 
 *                               mutable state (immerse.js)
 * @param {object} data        - the attribute (key) and the data (value) to be updatdd in to the state
 * @param {string} component   - the component value to be used for the processing type
 * @param {string} type        - the processing type that is being requested
 *                             - default: name
 *                             -   Options:
 *                             -     name - find through component name 
 *                             -     id - find through the component id value
 *                             -     dataset - find through the component dataset value
 * 
 * @return {bool} - returns true if success and false if failed
 */
 export const setComponentStateAttributes = (state, data, component, type = 'name') => {
    // get the component object
    let emobiqComponent = getEmobiqComponent(state, component, type);
    if (emobiqComponent && emobiqComponent.setAttributes) {
        emobiqComponent.setAttributes(data);
    } else {
        return false;
    }

    return true;
}

/*****
 * Private functions used within this library
 *****/

/**
 * Find the component state information
 * Only the first component if there are same 
 * condition for multiple components
 * @param {object} state      - the state to be used either 
 *                              non mutable or mutable state (immerse.js)
 * @param {string} component  - the component value to be used for the processing type
 * @param {string} type       - the processing type that is being requested
 *                            - default: name
 *                            -   Options:
 *                            -     name - find through component name 
 *                            -     uuid - find through the component uuid value (auto-generated)
 *                            -     id - find through the component id value
 *                            -     dataset - find through the component dataset value
 * 
 * @return {object} - component state
 */
const findComponentState = (state, component, type = 'name') => {
    // Prepare variables to be used
    let result = null;
    let key = '';

    // Go through all the components
    for (key in state) {
        // Skip invalid components
        if (!state[key] || !state[key].property) {
            continue;
        }

        // Get the component property
        let componentProperty = state[key].property;

        // Do based on the processing type
        switch (type) {
            // UUID
            case 'uuid':
                // Validate if it contains uuid
                if (!componentProperty.componentUUID) {
                    break;
                }

                // Check the id
                if (componentProperty.componentUUID == component) {
                    return state[key];
                }
                break;
            // ID 
            case 'id':
                // Validate if it contains id
                if (!componentProperty.componentID) {
                    break;
                }

                // Check the id
                if (componentProperty.componentID == component) {
                    return state[key];
                }
                break;
            // Dataset Name
            case 'dataset':
                // Validate if it is a datalist
                if (!componentProperty.componentType || componentProperty.componentType.toLowerCase() != 'datalist') {
                    break;
                }

                // Check the dataset value
                if (componentProperty.componentValue && componentProperty.componentValue == component) {
                    return state[key];
                }
                break;
            // Component Name
            case 'name':
            default:
                // Check the name
                if (key == component) {
                    return state[key];
                }
                break;
        }

        // Loop through the nested component states 
        // Skip unecessary components
        if (!componentProperty.componentData) {
            continue;
        }

        // Datalist
        if (Array.isArray(componentProperty.componentData)) {
            // Go through all the components inside the datalist
            for (let i in componentProperty.componentData) {
                result = findComponentState(componentProperty.componentData[i], component, type);
                if (result) {
                    return result;
                }
            }
        }

        // Snippets
        if (typeof componentProperty.componentData === 'object') {
            result = findComponentState(componentProperty.componentData, component, type);
            if (result) {
                return result;
            }
        }
    }

    return result;
}

/**
 * Populate datalist template with the data passed
 * @param {string} itemTemplateState - the item state template state
 * @param {array} data               - the data to be processed through
 * 
 * @return {any} - it will return the array list or false if something happened
 */
const populateDataListItems = (itemTemplateState, data, pageActions) => {
    // Prepare the item listing
    let items = [];

    // Catch if there is any issue in parsing
    try {
        // Go through the data and populate the datalist state template
        for (let record of data) {
            // Clone the template for every record
            let itemTemplate = JSON.parse(JSON.stringify(itemTemplateState));

            // Go through the item components
            for (let component in itemTemplate) {
                // Check if it contains property field
                if (!itemTemplate[component].property) {
                    continue;
                }

                // Get the property of the component
                let componentProperty = itemTemplate[component].property;

                // Generate the UUID of the component
                componentProperty.componentUUID = generateUUID();

                const componentFunction = componentProperty?.componentFunction
                if(componentFunction && pageActions) {
                    const componentPageAction = pageActions[componentFunction]
                    const derivedValue = componentPageAction(record)()
                    componentProperty.componentValue = derivedValue
                }else if(itemTemplate[component].property.field){
                    // Check if component have field property
                    // Populate the field value
                    componentProperty.componentValue = record[componentProperty.field];

                    // Check wether id is to be generated
                    if (!componentProperty.idPrefix || !componentProperty.idField) {
                        continue;
                    }

                    // Generate the component id
                    componentProperty.componentID = componentProperty.idPrefix + record[componentProperty.idField];
                } else {
                   continue;
                }
            }

            // Include the record to the state
            itemTemplate._data = record;

            // Push the generated item template
            items.push(itemTemplate);
        }
    } catch (error) {
        errorLog(logInformation + 'populateDataListItems: ' + error.message);
        return false;
    }

    return items;
}

/**

 * The following function returns a class that mimics a Emobiq editor component

 * The returned class is based on the componentType (specified in state of a component in a page)

 * @param {object} state     - the state to be used either 
 *                             non mutable or mutable state (immerse.js)
 * @param {string} component - the component value to be used for the processing type
 * @param {string} type      - the processing type that is being requested
 * @param {object} componentProps - in case the the componentProps is passed, no need to findComponent anymore

 * @return {<GenericEmobiqComponent>} - returns an implementation of GenericEmobiqComponent. (e.g EmobiqLabel, EmobiqButton)

 */

 export const getEmobiqComponent = (state, component, type, componentProps = null) => {
    if (!componentProps) {
        if (type == "name") {
            let service = getServiceByName(component)
            if (service) {
                return service;
            }
        } 
        
        componentProps = findComponentState(state, component, type)
        if (!componentProps) {
            return null;
        }
    }
    const componentType = componentProps.property.componentType;
    switch (componentType) {
        case 'label':
            return new EmobiqLabel(componentProps)
        case 'button':
            return new EmobiqButton(componentProps)
        case 'edit':
            return new EmobiqEdit(componentProps)
        case 'panel':
            return new EmobiqPanel(componentProps)
        case 'combobox':
            return new EmobiqComboBox(componentProps)
        case 'checkbox':
            return new EmobiqCheckBox(componentProps)
        case 'memo':
            return new EmobiqMemo(componentProps)
        case 'datalist':
            return new EmobiqDataList(componentProps)
        case 'image':
            return new EmobiqImage(componentProps)
        case 'signature':
            return new EmobiqSignature(componentProps)
    }

}