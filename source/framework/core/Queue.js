/**
 * Contains all items that need to be run
 * FIFO (First In First Out)
 * 
 * Format:
 * {
 *    func: () => {}  -> function to run 
 *    running: false  -> Boolean value that tells if the queue is running or not
 * }
 */
const queue = [];

// Current item being process
let current = {};

// Whether the queue already started or not
let started = false;

// The id of the timer that was returned by setTimeout
let timer;

/**
 * Add an item to the queue
 * @param {function} item 
 */
export const addQueue = (item) => {
    // Check if the item is a function
    if (typeof item !== 'function') {
        return;
    }

    // Add to queue
    queue.push({
        func: item,
        running: false
    });

    start();
};

/**
 * Starts running the queue
 */
const start = () => {
    // Check if already started
    if (started) {
        return;
    }

    started = true;
    run();
};

/**
 * Stop the queue from running
 */
const stop = () => {
    // Reset to false
    started = false;

    // Clear the current item
    current = {};

    // Clear timeout
    if (timer) {
        clearTimeout(timer);
        timer = undefined;
    }
};

/**
 * Get the next item in the queue
 */
const next = () => {
    if (queue.length > 0) {
        return queue.shift();
    }

    // Stop the queue
    stop();
};

/**
 * Get an item from the queue and then run it
 */
const run = () => {
    // Check the current item if still running
    if (typeof current.item !== 'undefined' && current.item.running) {
        wait(run);
        return;
    }

    // Get the next item from the queue
    let item = next();

    // Check if there's item
    if (typeof item !== 'undefined') {
        current.item = item;

        // Set the running to true
        current.item.running = true;

        // Run the item
        current.item.func(() => {
            current.item.running = false;
        });

        // Wait 1000ms before running the next item in the queue
        wait(run);
    }
};

/**
 * Wait before running a function
 * @param {function} run 
 * @param {int} timeOut 
 */
const wait = (run, timeOut) => {
    timer = setTimeout(run, timeOut || 500);
};