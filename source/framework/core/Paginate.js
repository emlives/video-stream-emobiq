class Paginate {
    constructor(data, perPage) {
        this.data = data;
        this.perPage = perPage || 10;
        this.currentPage = 0;
        this.totalPages = Math.ceil(this.data.length / this.perPage);
    }

    offset = () => { 
        return ((this.currentPage - 1) * this.perPage); 
    }
    
    page = (pageNumber) => { 
        if (pageNumber < 1) {
            pageNumber = 1;
        }
        if (pageNumber > this.totalPages) {
            pageNumber = this.totalPages;
        }
        this.currentPage = pageNumber;
        let start = this.offset(),
            end = start + this.perPage
        return this.data.slice(start, end);
    }
}

export default Paginate;