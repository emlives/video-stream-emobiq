/**
 * Used to log and/or show error response
 * @param {string} error - error message
 */    
export const errorLog = (error) => {
    console.log('ERROR LOG: ' + error);
}

/**
 * Used to log and/or show information response
 * @param {string} value - message
 */    
export const informationLog = (value) => {
    console.log('INFORMATION LOG: ' + value);
}