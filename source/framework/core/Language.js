/**
 * Functions for handling the languages
 * within the application.
 */
import { Default as LanguageDefault, Languages } from '../../dependency/global/Language';

import { getSetting, setSetting } from './Setting';
import { errorLog } from './Log';
import { result } from './Format';

let name = 'language';

/**
 * Get the current language the app is using.
 * 
 * @return {promise} - {string}
 */
export const getLanguage = () => {
    // Check if there is any existing language settings
    let language = getSetting(name);

    // If no language settings use default
    language = language || LanguageDefault;

    return language;
}

/**
 * Set the current language the app is using.
 * @param {string} language - The language to be used
 * 
 * @return {promise} - {object}
 */
export const setLanguage = (language) => {
    // Lower case the language
    language = language.toLowerCase();

    // Check if there is any existing language settings
    if (!Languages[language]) {
        errorLog('Source - Framework - Core - Language - setLanguage: ' + language + ' - Language is not supported.');
        return result(false, null, 'Language is not supported.');
    }

    // Update the language in the settings
    let success = setSetting(name, language);

    // Check if no issue in updating the language
    if (!success) {
        errorLog('Source - Framework - Core - Language - setLanguage: ' + language + ' - Setting the language failed, please check the log.');
        return result(false, null, 'Setting the language failed, please check the log.');
    }

    return result(true, null, 'Successfully updated the language.');
}

/**
 * Translate the value from the language dictionary.
 * @param {string} value    - The value to be converted
 * @param {string} language - Specify the language, leave it 
 *                            blank to use default language.
 * 
 * @return {promise} - {any}
 */
export const translate = (value, language) => {
    // Validate that the value is string
    if (typeof value !== 'string') {
        return value;
    }

    // Prepare the language
    language = language || getLanguage();
    language = language.toLowerCase();

    // Find the language in the dictionary
    let key = value.toLowerCase();
    if (Languages[language] && Languages[language][key]) {
        value = Languages[language][key];
    }

    return value;
}

/**
 * Get all the languages available
 * 
 * @return {array}
 */
export const getLanguageList = () => {
    return Object.keys(Languages);
}