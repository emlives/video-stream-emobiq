import Dataset from '../../base/service/Dataset';

import { getServiceByName } from '../../core/Service';
import { result } from '../../core/Format';

/**
 * Navision dataset service
 * Contains all the generic properties of the dataset.
 */
class NavisionDataset extends Dataset {
    // Define all the required properties
    name; // The name of this dataset
    connector; // The navision connector
    listKey; // The key/field that contains the data from the api call
    service; // Service name or so called page in navision
    limit; // Limit of the data to display
    autoLoad; // Autoload the loading of the data

    // Properties used within this class
    _lastConfig = {
        parameters: {},
        successCallback: null,
        errorCallback: null,
        bookmark: null
    }; // Used by load next
    _processing = false;
    _completed = false;

    /**
     * Constructor initialized all the properties
     * @param {string} name 
     * @param {object} properties 
     *                 - connector {string} - if there is any connector url
     *                 - listKey {string} - the key of the data
     *                 - service {string} - service name of this dataset
     *                 - limit {integer} - if any limit passed
     *                 - autoLoad {bool} - autoload feature
     */
    constructor(name, properties) {
        super();
        this.name = name;
        this.connector = properties.connector;
        this.listKey = properties.listKey;
        this.service = properties.serviceName; // edited from service to serviceName to match legacy property naming
        this.limit = properties.limit;
        this.autoLoad = (typeof properties.autoLoad === 'boolean' ? properties.autoLoad : false);
    }

    /**
     * Load the data from the service
     * used by 'loadData' function of eMOBIQ.
     * @param {object} parameters
     *                  - limit {int} - limit of data to display
     *                  - page {int} - page of data
     *                  - filter {object} - filter the data with the condition of and
     *                  - orFilter {object} - filter the data with the condition of or
     *                  - order {string} - asc or desc
     *                  -  {string} - asc or desc
     * @param {function} successCallback - successful callback
     * @param {function} errorCallback   - error callback
     * @param {boolean} loadNext - identifies whether it is triggered from loadNext
     * 
     * @return {promise} - result format
     */
    load = (parameters, successCallback, errorCallback, loadNext = false) => {
        // Get the connector from the services
        let connector = getServiceByName(this.connector);

        // Check if it exists
        if (!connector) {
            return result(false, null, 'The connector ' + this.connector + ' is not found.');
        }

        // Store the configurations 
        this._lastConfig.parameters = parameters;
        this._lastConfig.successCallback = successCallback;
        this._lastConfig.errorCallback = errorCallback;

        // Prepare the payload for the connector request
        let payload = {
            // entity: this.service,
            ent: this.service,
            type: 'data'
        }

        // Limit
        if (parameters.limit || this.limit) {
            payload.limit = (parameters.limit ? parameters.limit : this.limit);
        }

        // Page
        if (parameters.page) {
            payload.page = parameters.page;
        }

        // Filter
        if (parameters.filter) {
            payload.filter = parameters.filter;
        }

        // Order
        if (parameters.order) {
            payload.order = parameters.order;
        }

        // Append the bookmark if necessary
        if (loadNext && this._lastConfig.bookmark) {
            payload.bookmark = this._lastConfig.bookmark;
        }

        // Prepare the callback for the load method
        if (successCallback) {
            payload.callback = (data) => {
                // Get the data list by using the right key/field
                data = this.listKey ? data[this.listKey] : data;

                // Check if there is data returned and update the necessary information
                if (Array.isArray(data) && data.length > 0) {
                    this._lastConfig.bookmark = data?.[data.length - 1]?.['Key'];
                    this._completed = false;
                } else {
                    this._completed = true;
                }  

                this._processing = false;
                successCallback(data, loadNext);
            };
        }
        if (errorCallback) {
            payload.errorCallback = (error) => {
                this._processing = false;
                errorCallback(error);
            }
        }

        // Submit and process the request through the connector class
        return connector.request(payload);
    }

    /**
     * Load the following data from the service
     * based on the previous configuration done by loadData 
     * function.
     * Used by 'loadNext' of eMOBIQ.
     * @param {function} beforeCallback  - before callback triggered before anything 
     *                                     else unless it is completed already
     * 
     * @return {promise} - result format
     */
    loadNext = (beforeCallback) => {
        // Check if still loading then delay it
        if (this._processing) {
            return result(false, null, 'The dataset is still processing.');
        }

        // Only trigger the before callback if there is data
        if (!this._completed) {
            beforeCallback();
        }

        // Prepare the parameter for loading the data
        let config = this._lastConfig;
        config.parameters.page = (config.parameters.page ? config.parameters.page + 1 : 2);

        // Start processing
        this._processing = true;

        // Trigger the load data with the parameters
        return this.load(config.parameters, config.successCallback, config.errorCallback, true);
    }
}

export default NavisionDataset;