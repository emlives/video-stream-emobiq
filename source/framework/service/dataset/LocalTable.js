import Dataset from '../../base/service/Dataset';
import {
    getData as AsyncStorageGetData,
    storeData as AsyncStorageStoreData,
    removeData as AsyncStorageRemoveData,
} from '../../plugin/AsyncStorage';

import { PrefixLocalTable, DefaultValueArray } from '../../core/Constant';
import { sortData, filterData, filterUpdateData, filterRemoveData, objectIsEmpty } from '../../core/Helper';
import Paginate from '../../core/Paginate';
import { result } from '../../core/Format';

/**
 * Local Table
 * Contains all the generic properties and methods
 * of local table service.
 */
class LocalTable extends Dataset {
    // Define all the required properties
    name;
    autoLoad;
    uniqueKey;

    // Properties used within this class
    _lastConfig = {
        parameters: {},
        successCallback: null,
        errorCallback: null,
        bookmark: null
    }; // Used by load next
    _processing = false;
    _completed = false;

    /**
     * Constructor initialized all the properties
     * @param {string} name 
     * @param {object} properties 
     *                 - autoClear (boolean) - identifies whether this class is auto cleared
     *                 - autoLoad {boolean} - identifies whether this class auto load.
     *                 - uniqueKey {boolean} - identifies whether this class auto generate unique key.
     */
    constructor(name, properties) {
        super(properties);
        this.name = name;
        this.autoClear = (typeof properties.autoClear === 'boolean' ? properties.autoClear : false);
        this.autoLoad = (typeof properties.autoLoad === 'boolean' ? properties.autoLoad : false);
        this.uniqueKey = (typeof properties.uniqueKey === 'boolean' ? properties.uniqueKey : false);
    }

    /**
     * Load the data from the local table
     * @param {object} parameters
     *                  - limit {int} - limit of data to display
     *                  - page {int} - page of data
     *                  - filter {object} - filter the data with the condition of and
     *                  - orFilter {object} - filter the data with the condition of or
     *                  - order {string} - asc or desc
     * @param {function} successCallback - successfull callback
     * @param {function} errorCallback   - error callback
     * @param {boolean} loadNext - identifies whether it is triggered from loadNext
     * 
     * @return {promise} - result format  
     */
    load = (parameters, successCallback, errorCallback, loadNext = false) => {
        // Make sure to catch unhandled errors
        try {
            // Get all the data of the local table
            let data = AsyncStorageGetData(PrefixLocalTable + this.name);

            // Check if any issue in getting the data
            if (!data.success) {
                errorCallback(data.message);
                return result(false, null, data.message);
            }

            // Store the configurations 
            this._lastConfig.parameters = parameters;
            this._lastConfig.successCallback = successCallback;
            this._lastConfig.errorCallback = errorCallback;

            // Process the data base on the parameters
            data = data.data != null ? JSON.parse(data.data) : [];

            // Check if there are any filters included
            if (parameters.filter || parameters.orFilter) {
                data = filterData(parameters, data);
            }

            // Check if there is order parameters
            if (parameters.order) {
                data = sortData(parameters.order, data);
            }

            // Pagination
            if (parameters.limit) {
                let pageNumber = parameters.page || 1;
                let paginatedData = new Paginate(data, parameters.limit);
                if (pageNumber <= paginatedData.totalPages) {
                    data = paginatedData.page(pageNumber);
                } else {
                    data = DefaultValueArray;
                }
            } else {
                if (parameters.page > 1) {
                    data = DefaultValueArray;
                }
            }

            // Check if there is data returned and update the necessary information
            if (Array.isArray(data) && data.length > 0) {
                this._completed = false;
            } else {
                this._completed = true;
            }

            this._processing = false;
            successCallback(data, loadNext);
            return result(true, data, null);
        } catch (error) {
            this._processing = false;
            errorCallback(data);
            return result(false, null, error);
        }
    }

    /**
     * Load the following data from the service
     * based on the previous configuration done by loadData 
     * function.
     * Used by 'loadNext' of eMOBIQ.
     * @param {function} beforeCallback  - before callback triggered before anything 
     *                                     else unless it is completed already
     * 
     * @return {promise} - result format
     */
     loadNext = (beforeCallback) => {
        // Check if still loading then delay it
        if (this._processing) {
            return result(false, null, 'The dataset is still processing.');
        }

        // Only trigger the before callback if there is data
        if (!this._completed) {
            beforeCallback();
        }

        // Prepare the parameter for loading the data
        let config = this._lastConfig;
        config.parameters.page = (config.parameters.page ? config.parameters.page + 1 : 2);

        // Start processing
        this._processing = true;

        // Trigger the load data with the parameters
        return this.load(config.parameters, config.successCallback, config.errorCallback, true);
    }

    /**
     * Insert record(s) to the local table
     * @param {any} data                 - data to be inserted might be string, object or array
     * @param {boolean} append           - whether to overwrite or append the data
     * @param {function} successCallback - successfull callback
     * @param {function} errorCallback   - error callback
     * 
     * @return {promise} - result format  
     */
    insert = (data, append, successCallback, errorCallback) => {
        // Make sure to catch unhandled errors
        try {
            // Process the data first depending on the value passed
            if (typeof data === 'string') {
                data = JSON.parse(data);
            } else if (typeof data === 'object' && !Array.isArray(data)) {
                data = [data];
            }

            // Make sure the data is an array
            if (!Array.isArray(data)) {
                errorCallback('The data passed is invalid.');
                return result(false, null, 'The data passed is invalid.');
            }

            // Prepare the final data
            let finalData = [];

            // Check if it is only appending the data
            let resultStorage;
            if (append) {
                // Get all the data of the local table
                resultStorage = AsyncStorageGetData(PrefixLocalTable + this.name);

                // Check if any issue in getting the data
                if (!resultStorage.success) {
                    errorCallback(data.message);
                    return result(false, null, data.message);
                }

                // Process the data base on the parameters
                finalData = resultStorage.data != null ? JSON.parse(resultStorage.data) : [];
            }

            // Generate unique key if needed
            if (this.uniqueKey) {
                let uniqueId = 1;
                // Check if there is a unique id from the last record
                if (finalData.length > 0) {
                    let record = finalData[finalData.length - 1];
                    if (record._id) {
                        uniqueId = parseInt(record._id) + 1;
                    }
                }

                // Append the unique key to the new values
                for (let key in data) {
                    delete data[key]._id;
                    data[key]._id = uniqueId++;
                }
            }

            // Append the data to the finalData
            finalData = finalData.concat(data);

            // Write the data
            resultStorage = AsyncStorageStoreData(PrefixLocalTable + this.name, JSON.stringify(finalData));

            // Check if any issue in storing the data
            if (!resultStorage.success) {
                errorCallback(data.message);
                return result(false, null, data.message);
            }

            successCallback(data);
            return result(true, data, null);
        } catch (error) {
            errorCallback(error);
            return result(false, null, error);
        }
    }

    /**
     * Uodate data from the local table based
     * on the filter passed
     * @param {object} value             - the data to be stored as the updated values
     * @param {object} parameters
     *                                   - filter {object} - filter the data with the condition of and
     *                                   - orFilter {object} - filter the data with the condition of or
     * 
     * @param {boolean} first            - identify whether to delete only the first record found
     * @param {function} successCallback - successfull callback
     * @param {function} errorCallback   - error callback
     * 
     * @return {promise} - result format  
     */
    update = (value, parameters, first, successCallback, errorCallback) => {
        // Set the default values if necessary
        first = (typeof first === 'boolean' ? first : false);

        // Make sure to catch unhandled errors
        try {
            // Get all the data of the local table
            let data = AsyncStorageGetData(PrefixLocalTable + this.name);

            // Check if any issue in getting the data
            if (!data.success) {
                errorCallback(data.message);
                return result(false, null, data.message);
            }

            // Process the data base on the parameters
            data = data.data != null ? JSON.parse(data.data) : [];

            // Check if there are any filters included
            let updatedData = [];
            if (parameters.filter || parameters.orFilter) {
                let filterResult = filterUpdateData(parameters, data, value, first);
                data = filterResult.processed;
                updatedData = filterResult.updated;
            }

            // Write the data
            let resultStorage = AsyncStorageStoreData(PrefixLocalTable + this.name, JSON.stringify(data));

            // Check if any issue in storing the data
            if (!resultStorage.success) {
                errorCallback(data.message);
                return result(false, null, data.message);
            }

            successCallback(updatedData);
            return result(true, updatedData, null);
        } catch (error) {
            errorCallback(error);
            return result(false, null, error);
        }
    }

    /**
     * Delete data from the local table based
     * on the filter passed
     * @param {object} parameters
     *                                   - filter {object} - filter the data with the condition of and
     *                                   - orFilter {object} - filter the data with the condition of or
     * @param {boolean} first            - identify whether to delete only the first record found
     * @param {function} successCallback - successfull callback
     * @param {function} errorCallback   - error callback
     * 
     * @return {promise} - result format  
     */
    delete = (parameters, first, successCallback, errorCallback) => {
        // Set the default values if necessary
        first = (typeof first === 'boolean' ? first : false);

        // Make sure to catch unhandled errors
        try {
            // Get all the data of the local table
            let data = AsyncStorageGetData(PrefixLocalTable + this.name);

            // Check if any issue in getting the data
            if (!data.success) {
                errorCallback(data.message);
                return result(false, null, data.message);
            }

            // Process the data base on the parameters
            data = data.data != null ? JSON.parse(data.data) : [];

            // Check if there are any filters included
            let removedData = [];
            if (parameters.filter || parameters.orFilter) {
                let filterResult = filterRemoveData(parameters, data, first);
                data = filterResult.processed;
                removedData = filterResult.removed;
            }

            // Write the data
            let resultStorage = AsyncStorageStoreData(PrefixLocalTable + this.name, JSON.stringify(data));

            // Check if any issue in storing the data
            if (!resultStorage.success) {
                errorCallback(data.message);
                return result(false, null, data.message);
            }

            successCallback(removedData);
            return result(true, removedData, null);
        } catch (error) {
            errorCallback(error);
            return result(false, null, error);
        }
    }

    /**
     * Clear data from the local table
     * @param {function} successCallback - successful callback
     * @param {function} errorCallback   - error callback
     * 
     * @return {promise} - result format  
    */
    clear = (successCallback, errorCallback) => {
        // Get all the data of the dataset
        let data = AsyncStorageRemoveData(PrefixLocalTable + this.name);

        // Check if any issue in getting the data
        if (!data.success) {
            errorCallback(data.message);
            return result(false, null, data.message);
        }

        successCallback(DefaultValueArray);
        return result(true, DefaultValueArray, null);
    }
}

export default LocalTable;