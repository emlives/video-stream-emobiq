import CookieManager from '@react-native-cookies/cookies';
import Connector from '../../base/service/Connector';
import { ConnectorRaw } from '../../../dependency/global/Configuration';
import { errorLog } from '../../core/Log';
import { callback as globalCallback, httpBuildQuery, validateParameters, jsonDecode, encodeRequestParameterForm, safeEval } from '../../core/Helper';
import { result, resultConnector } from '../../core/Format';
import { encode as btoa } from 'base-64';

const logInformation = 'Source - Framework - Service - Connector - RawConnector';
const useWebKit = true;

class RawConnector extends Connector {

    /**
     * Constructor initialized all the properties
     * @param {string} - name of connector instance
     * @param {object} properties 
     *                 - connector {string} - if there is any connector url
     *                 - local {bool} - defines whether to use direct connector 
     *                                    or through an online connector
     *                 - url {string} - url of navision web service
     *                 - timeout {int} - the duration of waiting time on consuming nav api
     * @param {object} services - the services available from the navision web service
     */
    constructor(name, properties) {
        super();
        this.name = name
        this.connector = properties.connector || ''
        this.url = properties.url || '';
        this.local = (typeof properties.local === 'boolean' ? properties.local : true)
        this.timeOut = properties.timeOut || 30000
    }

    /**
     * Handles the request and toggle between local and direct request
     */
    request = (prm) => {
        if (this.local) {
            this.sendRequest(Object.assign({}, prm, { url: this.url }), (resp) => {
                var response = safeEval(resp);
                if (response) {
                    if (response.s) {
                        if (prm.callback) {
                            // include the extra
                            if (prm.extra) {
                                response.dt.extra = prm.extra;
                            }
                            prm.dataCallback(response);
                        }
                    } else {
                        globalCallback(prm.errCallback, response)
                    }
                } else {
                    globalCallback(prm.errCallback, {
                        s: false,
                        err: resp
                    })
                }
            });
        } else {
            // Prepare the main parameters to be passed
            var xprm = { api: 'data', a: 'call', path: prm.path }
            if (prm.method) { xprm.method = prm.method }

            // if there's a file then use FormData
            var odt;
            if (prm.file) {
                var opts = typeof (opts = (prm.options ? prm.options : {})) === 'object' ? JSON.stringify(opts) : opts;
                odt = new FormData();
                odt.append('connector', this.connector);
                odt.append('url', this.url);
                odt.append('data', typeof prm.data === 'object' ? JSON.stringify(prm.data) : prm.data);
                odt.append('options', opts);
                odt.append('timeout', this.timeOut || 5000);
                for (var i in prm.file) {
                    odt.append(i, prm.file[i]);
                }
                if (prm.header) {
                    odt.append('header', typeof prm.header === 'object' ? JSON.stringify(prm.header) : prm.header);
                }
            } else {
                odt = {
                    'connector': this.connector,
                    'url': this.url,
                    'data': prm.data,
                    'options': prm.options ? prm.options : {},
                    'timeout': this.timeOut || 5000
                };
                if (prm.header) {
                    odt.header = prm.header;
                }
            }

            this.rawService(xprm, odt)
                .then(response => {
                    if (response.err) {
                        errorLog(`${logInformation} - request: ${response.err}`);
                        return globalCallback(prm.errCallback, response.err)
                    }

                    prm.dataCallback(response);
                }).catch(error => {
                    errorLog(`${logInformation} - request: ${error}`);
                    globalCallback(prm.errCallback, error)
                })

            return result(true, null, 'rawCall request was sent to the server.');
        }
    }

    /**
     * Handles the request when local = true
     */
    sendRequest = (parameters, callback) => {
        parameters.api = parameters.api || 'data';
        parameters.a = parameters.a || 'call';

        // Standard validation
        let valid = validateParameters(['url', 'path'], parameters);
        if (valid !== true) {
            data = resultConnector(false, parameters['api'], parameters['a'], 'Missing the following parameters: ' + valid.join(',') + '.');
            callback(data);
            return;
        }

        switch (parameters.api) {
            case 'data':
                this.serviceData(parameters, callback);
                break;
        }
    }

    /**
     * Actual logic to handle request when local = true
     */
    serviceData = (parameters, callback) => {
        var url = parameters.url + '/' + parameters.path;
        var data = parameters.data || [];

        switch (parameters.a) {
            case 'get':
                if (!validateParameters(['data'], parameters, callback)) {
                    return;
                }

                // Standard validation
                let valid = validateParameters(['data'], parameters);
                if (valid !== true) {
                    data = resultConnector(false, parameters['api'], parameters['a'], 'Missing the following parameters: ' + valid.join(',') + '.');
                    callback(data);
                    return;
                }

                this.sendAPIRequest(url, parameters, data, (result) => {
                    var result = resultConnector(true, parameters.api, parameters.a, jsonDecode(resp));
                    callback(result);
                }, callback);
                break;
            case 'call':
                var options = parameters.options || {};

                if (!parameters.method) {
                    parameters.method = 'post';
                }

                // Process file
                if (parameters.file) {
                    // Raw query
                    if (options.rawQuery || options.raw_query) {
                        this.sendAPIRequestAndFormat(url, parameters, data, options, callback);
                    } else {
                        // process the files
                        data = this.processFiles(parameters.file, data);
                        if (data instanceof Promise) {
                            data.then(function (arrBuffer) {
                                this.sendAPIRequestAndFormat(url, parameters, arrBuffer, options, callback);
                            });
                        } else {
                            this.sendAPIRequestAndFormat(url, parameters, data, options, callback);
                        }
                    }
                } else {
                    this.sendAPIRequestAndFormat(url, parameters, data, options, callback);
                }

                break;
        }
    }

    /**
     * A wrapper function to format reponse from function sendAPIRequest
     */
    sendAPIRequestAndFormat = (url, parameters, data, options, callback) => {
        this.sendAPIRequest(url, parameters, data, (response) => {
            var result;
            switch (options.result) {
                case 'raw':
                    result = resultConnector(true, parameters.api, parameters.a, { data: response });
                    break;
                case 'json':
                    result = resultConnector(true, parameters.api, parameters.a, { data: jsonDecode(response) });
                    break;
                case 'image':
                    result = resultConnector(true, parameters.api, parameters.a, { data: btoa(response) });
                    break;
                default:
                    result = resultConnector(true, parameters.api, parameters.a, jsonDecode(response));
            }

            if (options.info) {
                if (typeof result.dt !== 'object') {
                    result.dt = { data: result.dt };
                }
                if (typeof response.status !== 'undefined') {
                    response.http_code = response.status;
                }
                result.dt.info = response;
            }

            if (options.getCookies) {
                if (typeof result.dt !== 'object') {
                    result.dt = { data: result.dt };
                }
                result.dt.cookies = typeof response.headers['set-cookie'] !== 'undefined' ? response.headers['set-cookie'] : undefined;
            }

            callback(result);
        }, parameters.errCallback);
    }


    /**
     * Handles the request when local = false
     */
    rawService = async (param, dt) => {
        let prm = param || {};
        let queryStringEncoded = encodeRequestParameterForm(prm);
        var connector = dt instanceof FormData ? dt.get('connector') : dt.connector;
        var url = (connector && connector != "") ? connector + '?' + queryStringEncoded : ConnectorRaw + '?' + queryStringEncoded;

        var parameters = {
            type: 'POST',
            url: url,
            timeout: 60000
        };

        if (dt instanceof FormData) {
            parameters.processData = false;
            parameters.contentType = false;
        }
        else {
            for (var i in dt) {
                if (typeof dt[i] === 'object') {
                    dt[i] = JSON.stringify(dt[i]);
                }
            }
        }

        parameters.data = dt;
        let bodyEncoded = encodeRequestParameterForm(parameters.data);

        return this.fetchWithTimeout(parameters.url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: bodyEncoded,
            timeout: parseFloat(this.timeOut)
        })
            .then(response => response.json())
            .then((jsonResponse) => {
                return jsonResponse
            })
            .catch((error) => {
                if(error.name === "AbortError") {
                    errorLog(logInformation + 'requestConnector' + ': ' + 'Request Timeout');
                }
                errorLog(`${logInformation} - rawService: ${error}`);
                return error
            });
    }

    /**
    * Function to prepare the file for upload. Handles different file datatypes
    */
    processFiles = (files, data) => {
        if (files instanceof Blob) {
            // returns a Promise object or the Blob if ArrayBuffer is not available
            return typeof files.arrayBuffer !== 'undefined' ? files.arrayBuffer() : files;
        } else if ((files instanceof ArrayBuffer) || (files instanceof Uint8Array)) {
            return files;
        } else {
            if (Object.keys(files).length > 0) {
                let formData = new FormData();

                for (var i in files) {
                    formData.append(i, files[i]);
                }

                for (var i in data) {
                    formData.append(i, data[i]);
                }

                return formData;
            } else {
                return data;
            }
        }
    }

    /**
     * Handles the request when local = false without any formatting of response
     */
    sendAPIRequest = async (url, parameters, data, callback, errorCallback) => {
        const methods = ['get', 'post', 'put', 'patch', 'head', 'delete', 'options'];
        const dataMethods = ['post', 'put', 'patch'];
        var headers = {};
        if (parameters.header) {
            for (var key in parameters.header) {
                if (parameters.header[key]) {
                    var header = parameters.header[key].split(':', 2);
                    headers[header[0]] = header[1].trim();
                }
            }
        }

        var method = 'get';
        if (parameters.method && methods.includes(parameters.method.toLowerCase())) {
            method = parameters.method.toLowerCase();
        }

        // Check if not an instance of FormData and ArrayBuffer
        if (!(data instanceof FormData) && !(data instanceof ArrayBuffer) && !(data instanceof Uint8Array)) {
            // if post,put,patch
            if (dataMethods.includes(method)) {
                var contentType = headers['Content-Type'] || headers['content-type'];
                if (contentType && contentType.match(/application\/json/)) {
                    data = JSON.stringify(data);
                } else if (contentType && contentType.match(/multipart\/mixed/)) {
                    // nothing to do
                } else {
                    data = httpBuildQuery(data);
                }
            } else {
                url += (url.indexOf('?') === -1 ? '?' : '&') + httpBuildQuery(data);
                data = undefined;
            }
        }

        // decode URI first, just in case this is already encoded
        url = encodeURI(decodeURI(url));

        // Get the parameter options
        var paramOptions = parameters.options || {};

        await CookieManager.clearAll(useWebKit);

        if (paramOptions.setCookies) {
            var cookiesString = paramOptions.cookies;
            if (cookiesString != null && cookiesString != '' && cookiesString != undefined) {
                var cookies = cookiesString.split(";");
                // Add all the cookies
                for (var i in cookies) {
                    await CookieManager.set(url, cookies[i], useWebKit);
                }
            }
        }

        // Include auth
        if (paramOptions.setAuth) {
            headers.Authorization = 'Basic ' + btoa(paramOptions.setAuthValue);
        }

        // Prepare the request information
        var options = {
            method: method,
            data: data,
            headers: headers,
            timeout: parameters.timeout || 5000
        };

        if (!(data instanceof FormData) && !(data instanceof ArrayBuffer) && !(data instanceof Uint8Array)) {
            options.serializer = 'utf8';
        }

        // set the trust mode to "nocheck"
        // for local testing, edit file @ node_modules/react-native/Libraries/Network/RCTHTTPRequestHandler.mm
        // remember to undo the edit

        this.fetchWithTimeout(url, {
            method: options.method,
            headers: options.headers,
            body: options.data,
            timeout: parseFloat(this.timeOut)
        })
            .then(response => response.json())
            .then((jsonResponse) => {
                callback(jsonResponse);
            })
            .catch((error) => {
                if(error.name === "AbortError") {
                    errorLog(logInformation + 'sendAPIRequest' + ': ' + 'Request Timeout');
                }
                errorLog(logInformation + 'sendAPIRequest' + ': ' + error);
                // if (response.status === -7) { return;}
                globalCallback(errorCallback, error)
            });
    }
}

export default RawConnector;