import Connector from '../../base/service/Connector';

import { ConnectorNavision } from '../../../dependency/global/Configuration';
import { errorLog } from '../../core/Log';
import { callback as globalCallback, encodeRequestParameterForm, encodeXMLValue, convertXMLtoJSON, sortData } from '../../core/Helper';
import { oDataGenerateFilter } from '../../core/OData';
import { result, resultConnector } from '../../core/Format';

import { encode as btoa } from 'base-64';

// logInformation - Display where this file is located for logging
const logInformation = 'Source - Framework - Service - Connector - NavisionConnector - ';

/**
 * Navision connector service
 * Contains all the generic properties and functions
 * shared among all the navision connector services.
 */
class NavisionConnector extends Connector {
    // Define all the required properties
    name;
    connector;
    type;
    local;
    url;
    company;
    user;
    password;
    timeout;
    services; // Available services from navision web service

    /**
     * Constructor initialized all the properties
     * @param {string} name 
     * @param {object} properties 
     *                 - connector {string} - if there is any connector url
     *                 - type {string} - the type of the web service soap/odata
     *                 - local {bool} - defines whether to use direct connector 
     *                                    or through an online connector
     *                 - url {string} - url of navision web service
     *                 - company {string} - default company name
     *                 - username {string} - default username of the web service
     *                 - password {string} - default password of the web service
     *                 - timeout {int} - the duration of waiting time on consuming nav api
     * @param {object} services - the services available from the navision web service
     */
    constructor(name, properties, services) {
        super();
        this.name = name;
        this.connector = properties.navconnector; // edited connector to navconnector to match legacy property naming
        this.type = (properties.type ? properties.type.toLowerCase() : 'soap');
        this.local = (typeof properties.local === 'boolean' ? properties.local : true);
        this.url = properties.url;
        this.company = properties.company;
        this.user = properties.user; // edited username to user to match legacy property naming
        this.password = properties.password;
        this.timeout = properties.timeOut || 5000; //edited timeout to timeOut
        this.services = services || {};
    }

    /**
     * Process the request to be sent to the navision api
     * @param {object} parameters
     *                 - type {string} - The api type request. (O)
     *                 - entity {string} - The entity or object name from navision. (O)
     *                 - function {string} - The function to be triggered. (O)
     *                 - subfunction {string} - If there is any sub function from web service. (O)
     *                 - data {object} - The data to be submitted. (O)
     *                 - extra {any} - The extra data to be passed back in callback. (O)
     *                 - batch {any} - To do a batch request (OData). (O)
     *                 - callback {function} - Successfull callback. (O)
     *                 - errCallback {function} - Error callback. (O)
     *                 Found in Load Data
     *                 - limit {int} - Used to limit the records. (O)
     *                 - page {int} - Used to set the page based on limit. (O)
     *                 - filter {object} - Used for filtering. (O)
     *                 - order {object} - Used for sorting. (O)
     * 
     * @return {result} - {object}
     */
    request = (parameters) => {
        // Process the parameters passed
        // let entity = parameters.entity;
        let entity = parameters.ent;
        let type = 'data';
        let action = 'call';
        let subpath = '';

        // Process the letiables based on passed parameters
        if (parameters.type) {
            // Check if it's an api type or nav path
            switch(parameters.type.toLowerCase()) {
                case 'page':
                    subpath = 'Page';
                    break;
                case 'codeunit':
                    subpath = 'Codeunit';
                    break;
                default:
                    type = parameters.type;
                    action = 'get';
            }
        }

        // Check whether the entity type exist then use that instead
        // for subpath
        // taken from the navision web service list 
        // that happened when using it in eMOBIQ platform
        if (this.services && this.services[entity] && this.services[entity].sp) {
            subpath = this.services[entity].sp;
        }

        // Update the parameters
        parameters.type = type;
        parameters.action = action;
        parameters.subpath = subpath;

        // Check the local settings and prepare the necessary url and body
        // for the request
        if (this.local) {
            return this.requestDirect(parameters);
        } 
        return this.requestConnector(parameters);
    }

    /**
     * Do a connection to the navision web service through an eMOBIQ connector
     * Process the request to be sent to the navision api
     * @param {object} parameters
     *                 - type {string} - The api type request. (O)
     *                 - entity {string} - The entity or object name from navision. (O)
     *                 - action {string} - The web service connection action. (O)
     *                 - subpath {string} - The subpath to be used in web service. (O)
     *                 - function {string} - The function to be triggered. (O)
     *                 - subfunction {string} - If there is any sub function from web service. (O)
     *                 - data {object} - The data to be submitted. (O)\(O)
     *                 - batch {any} - To do a batch request (OData). (O)
     *                 - callback {function} - Successfull callback. (O)
     *                 - errorCallback {function} - Error callback. (O)
     *                 Found in Load Data
     *                 - limit {int} - Used to limit the records. (O)
     *                 - page {int} - Used to set the page based on limit. (O)
     *                 - filter {object} - Used for filtering. (O)
     *                 - order {object} - Used for sorting. (O)
     * 
     * @return {result} - {object}
     */
    requestConnector = (parameters) => {
        // Prepare the query string
        let queryStringData = {
            // From user and function
            api: parameters.type,
            // ent: parameters.entity,
            ent: parameters.ent,
            a: parameters.action,
            subpath: parameters.subpath,
            function: parameters.function,
            extra: parameters.extra,
            batch: parameters.batch,
            limit: parameters.limit,
            page: parameters.page
        }

        // Encode the query string
        let queryStringEncoded = encodeRequestParameterForm(queryStringData);

        // Update the url
        let url = (this.connector ? this.connector : ConnectorNavision);
        url += '?' + queryStringEncoded;

        // Prepare the body parameter
        let body = {
            subfunction: parameters.subfunction,
            data: parameters.data,
            filter: parameters.filter,
            order: parameters.order,
            // From connector
            url: this.url,
            _user: this.user,
            _password: this.password,
            company: this.company,
            type: this.type
        };

        // Encode the body
        let bodyEncoded = encodeRequestParameterForm(body);

        // Send the request
        this.fetchWithTimeout(url, 
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: bodyEncoded,
                timeout: parseFloat(this.timeout)
            })
            .then((response) => response.json())
            .then((data) => {
                this.processCallback(data, parameters.extra, parameters.callback, parameters.errorCallback);
            })
            .catch((error) => {
                if(error.name === "AbortError") {
                    errorLog(logInformation + 'requestConnector' + ': ' + 'Request Timeout');
                }
                let data = resultConnector(false, parameters.type, parameters.action, error.message);
                this.processCallback(data, parameters.extra, parameters.callback, parameters.errorCallback);
            }
        );

        return result(true, null, 'Navision request was sent to the server.');
    }

    /**
     * Direct connection to the navision web service
     * Process the request to be sent to the navision api
     * @param {object} parameters
     *                 - type {string} - The api type request. (O)
     *                 - entity {string} - The entity or object name from navision. (O)
     *                 - action {string} - The web service connection action. (O)
     *                 - subpath {string} - The subpath to be used in web service. (O)
     *                 - function {string} - The function to be triggered. (O)
     *                 - subfunction {string} - If there is any sub function from web service. (O)
     *                 - data {object} - The data to be submitted. (O)
     *                 - extra {any} - The extra data to be passed back in callback. (O)
     *                 - batch {any} - To do a batch request (OData). (O)
     *                 - callback {function} - Successfull callback. (O)
     *                 - errorCallback {function} - Error callback. (O)
     *                 Found in Load Data
     *                 - limit {int} - Used to limit the records. (O)
     *                 - page {int} - Used to set the page based on limit. (O)
     *                 - filter {object} - Used for filtering. (O)
     *                 - order {object} - Used for sorting. (O)
     * 
     * @return {result} - {object}
     */
    requestDirect = (parameters) => {
        // Find the correct api
        switch(this.type + '-' + parameters.type.toLowerCase()) {
            // Soap - Data services
            case 'soap-data':
                return this.serviceSOAPData(parameters);
            // Soap - Companies
            case 'soap-company':
                return this.serviceSOAPCompany(parameters);
            // Odatav4 - Data services
            case 'odatav4-data':
                return this.serviceODataV4Data(parameters);
            // Odatav4 - Companies
            case 'odatav4-company':
                return this.serviceODataV4Company(parameters);
            // Not found
            default:
                return this.processError('Service requested or connector type is not existing.', 'requestDirect', parameters.extra, parameters.type, parameters.action, parameters.callback, parameters.errorCallback);
        }
    }

    /*****
     * SERVICES - DIRECT CONNECTION - SOAP
     *****/

    /**
     * Soap Connection
     * Get the available for company
     * @param {object} parameters
     *                 - type {string} - The api type request. (O)
     *                 - entity {string} - The entity or object name from navision. (O)
     *                 - action {string} - The web service connection action. (O)
     *                 - subpath {string} - The subpath to be used in web service. (O)
     *                 - function {string} - The function to be triggered. (O)
     *                 - subfunction {string} - If there is any sub function from web service. (O)
     *                 - data {object} - The data to be submitted. (O)
     *                 - extra {any} - The extra data to be passed back in callback. (O)
     *                 - batch {any} - To do a batch request (OData). (O)
     *                 - callback {function} - Successfull callback. (O)
     *                 - errorCallback {function} - Error callback. (O)
     *                 Found in Load Data
     *                 - limit {int} - Used to limit the records. (O)
     *                 - page {int} - Used to set the page based on limit. (O)
     *                 - filter {object} - Used for filtering. (O)
     *                 - order {object} - Used for sorting. (O)
     * 
     * @return {result} - {object}
     */
    serviceSOAPCompany = (parameters) => {
        // Find the correct action
        switch(parameters.action) {
            case 'get':
                // Generate the proper url 
                let url = this.url;
                url += '/WS/SystemService';

                // Prepare the soap body
                let namespace = 'nav/system';
                let body = this.generateSOAPBody('header', {namespace: namespace});
                body += this.generateSOAPBody('start', {name: 'Companies'});
                body += this.generateSOAPBody('end', {name: 'Companies'});
                body += this.generateSOAPBody('footer', {});

                // Send the request
                this.sendSOAP(url, body, 
                    (data) => {
                        data = (data.Companies_Result ? data.Companies_Result : data);
                        data = (data.return_value ? data.return_value : data);
                        data = resultConnector(true, parameters.type, parameters.action, data);
                        this.processCallback(data, parameters.extra, parameters.callback, parameters.errorCallback);
                    }, 
                    (error) => {
                        let data = resultConnector(false, parameters.type, parameters.action, error);
                        this.processCallback(data, parameters.extra, parameters.callback, parameters.errorCallback);
                    }
                );
                break;
            // Not found
            default:
                return this.processError('Action requested is not existing.', 'serviceSOAPCompany', parameters.extra, parameters.type, parameters.action, parameters.callback, parameters.errorCallback);
        }

        return result(true, null, 'Navision request was sent to the server.');
    }

    /**
     * Soap Connection
     * Trigger some data calling to navision web service
     * Such as listing, creating, updating and triggering a code unit
     * @param {object} parameters
     *                 - type {string} - The api type request. (O)
     *                 - entity {string} - The entity or object name from navision. (O)
     *                 - action {string} - The web service connection action. (O)
     *                 - subpath {string} - The subpath to be used in web service. (O)
     *                 - function {string} - The function to be triggered. (O)
     *                 - subfunction {string} - If there is any sub function from web service. (O)
     *                 - data {object} - The data to be submitted. (O)
     *                 - extra {any} - The extra data to be passed back in callback. (O)
     *                 - batch {any} - To do a batch request (OData). (O)
     *                 - callback {function} - Successfull callback. (O)
     *                 - errorCallback {function} - Error callback. (O)
     *                 Found in Load Data
     *                 - limit {int} - Used to limit the records. (O)
     *                 - page {int} - Used to set the page based on limit. (O)
     *                 - filter {object} - Used for filtering. (O)
     *                 - order {object} - Used for sorting. (O)
     * 
     * @return {result} - {object}
     */
    serviceSOAPData = (parameters) => {
        // Find the correct action
        switch(parameters.action) {
            case 'get':
                // Set default values
                parameters.subpath = parameters.subpath || 'Page';

                // Generate the proper url 
                var url = this.url;
                url += '/WS/' + this.company + '/' + parameters.subpath + '/' + parameters.entity;

                // If there is a filter
                let filter = '';
                if (parameters.filter) {
                    // Loop through all the filters to be added
                    let filters = parameters.filter;
                    for (let index in filters) {
                        let field = filters[index].f || filters[index].field || '';
                        let operator = filters[index].o || filters[index].operator || '';
                        let value = filters[index].v || filters[index].value || '';
                        filter += this.generateSOAPBody('filter', { field: field, criteria: operator + value });
                    }
                }

                // If there is a limit
                let limit = '';
                let limitCount = 0;
                if (parameters.limit) {
                    limit += this.generateSOAPBody('size', { size: parameters.limit });
                    limitCount = parameters.limit;
                }

                // For pagination/bookmarking
                let bookmark = '';
                let bookmarkInclude = false;
                var skipRecordCount = 0;
                if (parameters.page) {
                    // Check if there was a bookmark passed
                    bookmark = parameters.bookmark || '';

                    // Check if it requires to load some pages to get the bookmark
                    let page = parameters.page;
                    if (!bookmark && page > 1) {
                        // Update the limit 
                        limit = limitCount * page;
                        limit += this.generateSOAPBody('size', { size: limit });
                        // Set the number of record to be skipped
                        skipRecordCount = limitCount * (page - 1);
                    } else {
                        bookmarkInclude = true;

                         // Generate the bookmark body 
                        bookmark = this.generateSOAPBody('bookmark', { bookmark: bookmark });
                    }                   
                }

                // Prepare the soap body
                var entity = parameters.entity || '';
                var namespace = 'page/' + entity.toLowerCase();
                var body = '';
                body += this.generateSOAPBody('header', {namespace: namespace});
                body += this.generateSOAPBody('start', {name: 'ReadMultiple'});
                body += filter;
                body += limit;
                body += (bookmarkInclude ? bookmark : '');
                body += this.generateSOAPBody('end', {name: 'ReadMultiple'});
                body += this.generateSOAPBody('footer', {});

                // Send the request
                this.sendSOAP(url, body, 
                    (data) => {
                        // Variables to be used
                        let total = 0;

                        // Get the valid data for this request 
                        data = (data.ReadMultiple_Result ? data.ReadMultiple_Result : data);
                        data = (data.ReadMultiple_Result ? data.ReadMultiple_Result : data);

                        // Check if have data
                        if (data) {
                            data = (data[entity] ? data[entity] : data);

                            // Skip the records needed to be skipped
                            data = (Array.isArray(data) ? data : [data]);
                            data.splice(0, skipRecordCount);
        
                            // Convert the images to base64 src
                            // @todo: enhance this part, temporary to make it run
                            for (let index in data) {
                                for(let field in data[index]) {
                                    if (field.includes('Picture')) {
                                        data[index][field] = 'data:image/png;base64,' + data[index][field];
                                    }
                                }
                            }
        
                            // Sort the data if needed
                            if (parameters.order) {
                                data = sortData(parameters.order, data);
                            }

                            // Get the total records
                            total = data.length;
                        } else {
                            // Set to undefined if no data
                            data = undefined;
                        }

                        // Prepare the result/return
                        data = {data: data, limit: limitCount, total: total};
                        data = resultConnector(true, parameters.type, parameters.action, data);
                        this.processCallback(data, parameters.extra, parameters.callback, parameters.errorCallback);
                    }, 
                    (error) => {
                        let data = resultConnector(false, parameters.type, parameters.action, error);
                        this.processCallback(data, parameters.extra, parameters.callback, parameters.errorCallback);
                    }
                );
                break;
            // Generic call
            case 'call':
                // Set default values
                parameters.function = parameters.function || 'Create';
                parameters.subpath = parameters.subpath || 'Page';

                // Generate the proper url 
                var url = this.url;
                url += '/WS/' + this.company + '/' + parameters.subpath + '/' + parameters.entity;

                // Manipulate the data to be passed if needed
                if (parameters.subpath == 'Page') {
                    // Check if it's one of the function to be updated
                    let parametersData = {};
                    switch(parameters.function) {
                        // Create + Update + Create Multiple + Update Multiple
                        case 'Create':
                        case 'Update':
                        case 'CreateMultiple':
                        case 'UpdateMultiple':
                            parametersData[parameters.entity] = parameters.data;
                            break;
                        // Default
                        default:
                            parametersData = parameters.data;
                    }
                    // Replace the current data
                    parameters.data = parametersData;
                }

                // Check if have subfunction 
                if (parameters.subfunction) { 
                    // Make sure both are objects
                    if (typeof parameters.data === 'object' &&
                        typeof parameters.subfunction === 'object') {
                        Object.assign(parameters.data, parameters.subfunction);
                    }
                }

                // Prepare the soap body
                let subpath = parameters.subpath.toLowerCase();
                var namespace = subpath + '/' + parameters.entity;
                if (subpath === 'page') {
                    namespace = subpath + '/' + parameters.entity.toLowerCase();
                }

                var body = '';
                body += this.generateSOAPBody('header', {namespace: namespace});
                body += this.generateSOAPBody('start', {name: parameters.function});
                body += this.generateSOAPBody('data', {data: parameters.data});
                body += this.generateSOAPBody('end', {name: parameters.function});
                body += this.generateSOAPBody('footer', {});

                // Send the request
                this.sendSOAP(url, body, 
                    (data) => {
                        // Get the valid data for this request 
                        data = data[parameters.function + '_Result'];

                        // Prepare the result/return
                        data = resultConnector(true, parameters.type, parameters.action, data);
                        this.processCallback(data, parameters.extra, parameters.callback, parameters.errorCallback);
                    }, 
                    (error) => {
                        let data = resultConnector(false, parameters.type, parameters.action, error);
                        this.processCallback(data, parameters.extra, parameters.callback, parameters.errorCallback);
                    }
                );

                break;
            // Not found
            default:
                return this.processError('Action requested is not existing.', 'serviceSOAPData', parameters.extra, parameters.type, parameters.action, parameters.callback, parameters.errorCallback);
        }

        return result(true, null, 'Navision request was sent to the server.');
    }

    /*****
     * SERVICES - DIRECT CONNECTION - ODATAV4
     *****/

    /**
     * ODataV4 Connection
     * Get the available for company
     * @param {object} parameters
     *                 - type {string} - The api type request. (O)
     *                 - entity {string} - The entity or object name from navision. (O)
     *                 - action {string} - The web service connection action. (O)
     *                 - subpath {string} - The subpath to be used in web service. (O)
     *                 - function {string} - The function to be triggered. (O)
     *                 - subfunction {string} - If there is any sub function from web service. (O)
     *                 - data {object} - The data to be submitted. (O)
     *                 - extra {any} - The extra data to be passed back in callback. (O)
     *                 - batch {any} - To do a batch request (OData). (O)
     *                 - callback {function} - Successfull callback. (O)
     *                 - errorCallback {function} - Error callback. (O)
     *                 Found in Load Data
     *                 - limit {int} - Used to limit the records. (O)
     *                 - page {int} - Used to set the page based on limit. (O)
     *                 - filter {object} - Used for filtering. (O)
     *                 - order {object} - Used for sorting. (O)
     * 
     * @return {result} - {object}
     */
    serviceODataV4Company = (parameters) => {
        // Find the correct action
        switch(parameters.action) {
            case 'get':
                // Generate the proper url 
                let url = this.url;
                url += '/ODataV4/Company';

                // Send the request
                this.sendODataV4(url, 'get', null, null, 
                    (data) => {
                        // Loop through the data to get the companies
                        var companies = [];
                        for (let key in data.value) {
                            companies.push(data.value[key].Name);
                        }
                        data = resultConnector(true, parameters.type, parameters.action, companies);
                        this.processCallback(data, parameters.extra, parameters.callback, parameters.errorCallback);
                    }, 
                    (error) => {
                        let data = resultConnector(false, parameters.type, parameters.action, error);
                        this.processCallback(data, parameters.extra, parameters.callback, parameters.errorCallback);
                    }
                );
                break;
            // Not found
            default:
                return this.processError('Action requested is not existing.', 'serviceODataV4Company', parameters.extra, parameters.type, parameters.action, parameters.callback, parameters.errorCallback);
        }

        return result(true, null, 'Navision request was sent to the server.');
    }

    /**
     * ODataV4 Connection
     * Trigger some data calling to navision web service
     * Such as listing, creating, updating and triggering a code unit
     * @param {object} parameters
     *                 - type {string} - The api type request. (O)
     *                 - entity {string} - The entity or object name from navision. (O)
     *                 - action {string} - The web service connection action. (O)
     *                 - subpath {string} - The subpath to be used in web service. (O)
     *                 - function {string} - The function to be triggered. (O)
     *                 - subfunction {string} - If there is any sub function from web service. (O)
     *                 - data {object} - The data to be submitted. (O)
     *                 - extra {any} - The extra data to be passed back in callback. (O)
     *                 - batch {any} - To do a batch request (OData). (O)
     *                 - callback {function} - Successfull callback. (O)
     *                 - errorCallback {function} - Error callback. (O)
     *                 Found in Load Data
     *                 - limit {int} - Used to limit the records. (O)
     *                 - page {int} - Used to set the page based on limit. (O)
     *                 - filter {object} - Used for filtering. (O)
     *                 - order {object} - Used for sorting. (O)
     * 
     * @return {result} - {object}
     */
    serviceODataV4Data = (parameters) => {
        // Find the correct action
        switch(parameters.action) {
            case 'get':
                // Set default values
                parameters.subpath = parameters.subpath || 'Page';

                // Query string
                let queryString = [];

                // If there is a filter
                if (parameters.filter) {
                    try {
                        let filter = oDataGenerateFilter(parameters.filter);
                        if (filter) {
                            queryString.push('$filter=' + filter);
                        }
                    } catch(error) {
                        return this.processError(error.message, 'serviceODataV4Data', parameters.extra, parameters.type, parameters.action, parameters.callback, parameters.errorCallback);
                    }
                }

                // If there is a limit
                let limitCount = 0;
                if (parameters.limit) {
                    limitCount = parameters.limit;
                }
                if (limitCount) {
                    queryString.push('$top=' + limitCount);
                }

                // If there is a page control
                if (parameters.page) {
                    queryString.push('$skip=' + (limitCount * (parseInt(parameters.page) - 1)));
                }

                // For sorting
                if (parameters.order) {
                    let order = [];
                    for (let key in parameters.order) {
                        let field = parameters.order[key].f || parameters.order[key].field;
                        let value = parameters.order[key].v || parameters.order[key].value; 

                        if (value === 'desc' || value === 'asc') {
                            order.push(field + ' ' + value);
                        } else {
                            order.push(field + ' asc');
                        }
                    }
                    if (order.length > 0) {
                        queryString.push('$orderby=' + order.join(','));
                    }
                }

                // include all lines
                queryString.push('$expand=*');

                // Generate the proper url 
                var url = this.url;
                url += '/ODataV4/Company(\'' + this.company + '\')/' + parameters.entity;
                if (queryString.length > 0) {
                    url += '?' + queryString.join('&');
                }

                // Send the request
                this.sendODataV4(url, 'get', body, 
                    (data) => {
                        // Variables to be used
                        let total = 0;

                        // Process the data
                        data = this.processODataV4Result(data.value);

                        // Check if have data
                        if (data) {
                            // Get the total records
                            total = data.length;
                        } else {
                            // Set to undefined if no data
                            data = undefined;
                        }

                        // Prepare the result/return
                        data = {data: data, limit: limitCount, total: total};
                        data = resultConnector(true, parameters.type, parameters.action, data);
                        this.processCallback(data, parameters.extra, parameters.callback, parameters.errorCallback);
                    }, 
                    (error) => {
                        let data = resultConnector(false, parameters.type, parameters.action, error);
                        this.processCallback(data, parameters.extra, parameters.callback, parameters.errorCallback);
                    }
                );
                break;
            // Generic call
            case 'call':
                // Set default values
                parameters.function = parameters.function || 'Create';
                parameters.subpath = parameters.subpath || 'Page';

                // Skip if codeunit as odata doesn't support it
                if (parameters.subpath === 'Codeunit') {
                    return this.processError('Codeunit not supported in odatav4.', 'serviceODataV4Data', parameters.extra, parameters.type, parameters.action, parameters.callback, parameters.errorCallback);
                }

                // Get the method to use 
                let validFunctions = {
                    'create': 'post',
                    'update': 'patch',
                    'delete': 'delete'
                };

                let method = validFunctions[parameters.function.toLowerCase()];
                if (!validFunctions) {
                    return this.processError('This function is not supported in odatav4.', 'serviceODataV4Data', parameters.extra, parameters.type, parameters.action, parameters.callback, parameters.errorCallback);
                }

                // Generate the proper url 
                var url = this.url + '/ODataV4',
                    urlCompanyWithOutEnt = url + '/Company(\'' + this.company + '\')',
                    urlCompany = urlCompanyWithOutEnt + '/' + parameters.entity;

                // Headers
                let headers = {};

                // Check if it is batch 
                if (parameters.batch) {
                    let batchId = 'batch_' + (new Date().getTime());
                    url += '/$batch';
                    headers['Content-Type'] = 'multipart/mixed; boundary=' + batchId;
                    parameters.data = this.generateOdataV4Body(parameters.data, batchId, method, urlCompanyWithOutEnt, parameters.entity);
                    method = 'post';
                } else {
                    url = urlCompany;
                }

                // Check other processsing needed
                if (method === 'patch' || method === 'post') {
                    headers.Prefer = 'return=representation';
                }
                
                if (parameters.data.Key && !parameters.batch) {
                    headers['If-Match'] = parameters.data.Key;
                    if (method === 'patch') {
                        delete parameters.data.Key;
                    }
                }
                
                // Stringify the object data
                let body = '';
                try {
                    body = JSON.stringify(parameters.data)
                } catch(error) {
                    return this.processError(error, 'serviceODataV4Data', parameters.extra, parameters.type, parameters.action, parameters.callback, parameters.errorCallback);
                }
                
                // Send the request
                this.sendODataV4(url, method, body, headers,
                    (data) => {
                        // Get the valid data for this request 
                        for (let field in data) {
                            // remove element with space only   
                            if (data[field] === '') {
                                delete data[field];
                            }

                            // change the etag to Key
                            // will be use in updating/deleting
                            if (field === '@odata.etag') {
                                data.Key = data[field];
                                delete data[field];
                            }
                        }

                        var finalData = {};
                        if (parameters.entity) {
                            finalData[parameters.entity] = data;
                        } else {
                            finalData = data;
                        }

                        // Prepare the result/return
                        finalData = resultConnector(true, parameters.type, parameters.action, finalData);
                        this.processCallback(finalData, parameters.extra, parameters.callback, parameters.errorCallback);
                    }, 
                    (error) => {
                        let data = resultConnector(false, parameters.type, parameters.action, error);
                        this.processCallback(data, parameters.extra, parameters.callback, parameters.errorCallback);
                    }
                );

                break;
            // Not found
            default:
                return this.processError('Action requested is not existing.', 'serviceODataV4Data', parameters.extra, parameters.type, parameters.action, parameters.callback, parameters.errorCallback);
        }

        return result(true, null, 'Navision request was sent to the server.');
    }

    /*****
     * REUSABLE FUNCTIONS
     *****/

    /**
     * Process the callback based on the standard result of this class
     * @param {object} data            - The standard result of navision call
     * @param {any} extra              - The data to be passed in any callback triggered
     * @param {function} callback      - The successfull callback
     * @param {function} errorCallback - The failure callback
     */
    processCallback = (data, extra, callback, errorCallback) => {
        // Check if it is successfull the api result is successfull
        if (!data.s) {
            globalCallback(errorCallback, data, extra);
            return;
        }
        globalCallback(callback, data.dt, extra);
    }

    /**
     * Process error handling within this class
     * @param {string} message         - The error message
     * @param {string} functionName    - The function name where the error happened
     * @param {any} extra              - The data to be passed in any callback triggered
     * @param {string} type            - The type of call 
     * @param {string} action          - The action of call
     * @param {function} callback      - The successfull callback 
     * @param {function} errorCallback - The failure callback
     * 
     * @return {result} - {object}
     */
    processError = (message, functionName, extra, type, action, callback, errorCallback) => {
        errorLog(logInformation + functionName + ': ' + message);
        let data = resultConnector(false, type, action, message);
        this.processCallback(data, extra, callback, errorCallback);
        return result(false, null, message);
    }

    /*****
     * SOAP FUNCTIONS
     *****/

    /**
     * Send the soap request
     * @param {string} url 
     * @param {any} body 
     * @param {function} callback 
     * @param {function} errorCallback 
     */
    sendSOAP = (url, body, callback, errorCallback) => {
        // Encode the url
        url = encodeURI(url);

        // Send the request
        this.fetchWithTimeout(url, 
            {
                method: 'POST',
                headers: {
                    'Authorization': 'Basic ' + btoa(this.user + ':' + this.password),
                    'Content-Type': 'application/xml; charset=utf-8',
                    'SoapAction': 'urn:microsoft-dynamics-schemas'
                },
                body: body,
                timeout: parseFloat(this.timeout)
            })
            .then((response) => response.text())
            .then((data) => {
                // Process the xml result
                data = convertXMLtoJSON(data);

                // Identify the prefix of the soap
                // somehow it changes depends on the situation
                let prefix = 'Soap:';
                if (data['SOAP-ENV:Envelope']) {
                    prefix = 'SOAP-ENV:';
                } else if (data['s:Envelope']) {
                    prefix = 's:';
                }

                // Get the body
                data = data[prefix + 'Envelope'][prefix + 'Body'];

                // Check if it error
                if (data[prefix + 'Fault']) {
                    data = data[prefix + 'Fault'];
                    errorCallback({global: data['faultcode'] + ':' + data['faultstring']});
                    return;
                }
                callback(data);
            })
            .catch((error) => {
                if(error.name === "AbortError") {
                    errorLog(logInformation + 'sendSOAP' + ': ' + 'Request Timeout');
                }
                errorCallback(error.message);
            }
        );
    }

    /**
     * Generate part of the required soap body
     * @param {string} location - Identify which part of the soap request
     * @param {object} options - Identify which part of the soap request
     * 
     * @return {string}
     */
    generateSOAPBody = (location, options) => {
        // Find the correct location and generate the data
        let data = '';
        switch(location) {
            // Header
            case 'header':
                data += '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:get="urn:microsoft-dynamics-schemas/' + options.namespace + '">';
                data += '<soap:Body>';
                break;
            // Footer
            case 'footer':
                data += '</soap:Body>';
                data += '</soap:Envelope>';
                break;
            // Start a grouping
            case 'start':
                data += '<get:' + options.name + '>';
                break;
            // End a grouping
            case 'end':
                data += '</get:' + options.name + '>';
                break;
             // Filter
             case 'filter':
                data += '<get:filter>';
                data += '<get:Field>' + encodeXMLValue(options.field) + '</get:Field>';
                data += '<get:Criteria>' + encodeXMLValue(options.criteria) + '</get:Criteria>';
                data += '</get:filter>';
                break;
            // Size
            case 'size':
                data += '<get:setSize>';
                data += encodeXMLValue(options.size);
                data += '</get:setSize>';
                break;
            // Bookmark
            case 'bookmark':
                data += '<get:bookmarkKey>';
                data += encodeXMLValue(options.bookmark);
                data += '</get:bookmarkKey>';
                break;
             // Data
             case 'data':
                // Process through the data
                data += this.processDataToSOAP(options.data, null);
                break;
            // Not found
            default:
        }
        // Return the final result
        return data;
    }

    /**
     * Process the data into a soap body
     * @param {object/array} data - contains all the data to be translated into SOAP
     * @param {string} parent - parent tag to be used for arrays
     * 
     * @return {string}
     */
    processDataToSOAP = (data, parent) => {
        let result = '';
        parent = parent || '';

        // If not an object or array just return
        if (typeof data !== 'object') {
            return encodeXMLValue(data);
        }

        // Go through all the data of the object
        let useParent = Array.isArray(data);
        for (let key in data) {
            // If it's a null then don't include
            if (typeof data[key] === 'undefined') {
                continue;
            }
            
            // Don't include if the value is array
            let isArray = Array.isArray(data[key]);
            if (!isArray) {
                // Opening tag
                result += '<get:' + (useParent ? parent : key) + '>';
            }
            // Item
            result += this.processDataToSOAP(data[key], key);
            if (!isArray) {
                // Closing tag
                result += '</get:' + (useParent ? parent : key) + '>';
            }
        }

        return result;
    }

    /*****
     * ODATAV4 FUNCTIONS
     *****/

    /**
     * Send the odatav4 request
     * @param {string} url 
     * @param {string} method 
     * @param {any} body 
     * @param {object} headers 
     * @param {function} callback 
     * @param {function} errorCallback 
     */
    sendODataV4 = (url, method, body, headers, callback, errorCallback) => {
        // Prepare the headers
        let finalHeaders = {
            'Authorization': 'Basic ' + btoa(this.user + ':' + this.password),
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
            'OData-Version': '4.0',
            'OData-MaxVersion': '4.0'
        };

        // Check if need to update some header information
        if (headers) {
            Object.assign(finalHeaders, headers);
        }

        // Encode the url
        url = encodeURI(url);
        
        // Send the request
        this.fetchWithTimeout(url,  
            {
                method: method.toUpperCase(),
                headers: finalHeaders,
                body: body,
                timeout: parseFloat(this.timeout)
            })
            .then((response) => response.text())
            .then((data) => {
                try {
                    data = JSON.parse(data);
                    callback(data);
                } catch (error) {
                    errorCallback(error.message);
                }
            })
            .catch((error) => {
                if(error.name === "AbortError") {
                    errorLog(logInformation + 'requestConnector' + ': ' + 'Request Timeout');
                }
                errorCallback(error);
            }
        );
    }

    /**
     * Generates a body to be submitted for batch request
     * 
     * @param {array} data
     * @param {string} batchId
     * @param {string} method
     * @param {string} url
     * @param {string} ent
     */
    generateOdataV4Body = (data, batchId, method, url, ent) => {
        var result = '';
        
        // make sure data is an array
        if (!Array.isArray(data)) {
            data = [data];
        }
        
        // loop through all data
        for (var i in data) {
            var changeset = 'changeset_' + (new Date().getTime()) + i;

            // Get the Key
            var key = this.getOdataV4KeyForBatch(data[i], method);

            // Get the url
            url = this.getOdataV4UrlForBatch(data[i], url, ent);
            
            result += "--" + batchId + "\r\n";
            result += "Content-Type: multipart/mixed; boundary="  + changeset + "\r\n";
            result += "\r\n";
            result += "--"  + changeset + "\r\n";
            result += "Content-Type: application/http\r\n";
            result += "Content-Transfer-Encoding: binary\r\n";
            result += "Content-ID: " + (parseInt(i) + 1) + "\r\n";
            result += "\r\n";
            result += method.toUpperCase() + " " + encodeURI(url) + " HTTP/1.1\r\n";
            result += "Content-Type: application/json; type=entry\r\n";
            result += key ? "If-Match: " + key + "\r\n" : "";
            result += "\r\n";
            result += JSON.stringify(data[i]) + "\r\n";
            result += "--"  + changeset + "--\r\n";
            result += "\r\n";
        }
        
        result += "--" + batchId + "--\r\n";
        
        return result;
    }
    
    /**
     * Get the Key for batch request
     * 
     * @param {object} data 
     * @param {string} method 
     * @return {string}
     */
    getOdataV4KeyForBatch = (data, method) => {
        var key;

        // Get the Key from data
        if (method.toUpperCase() === 'PATCH' && typeof data.Key !== 'undefined') {
            if (data.Key) {
                key = data.Key;
            }

            // Remove Key from data
            delete data.Key;
        }

        return key;
    }

    /**
     * Get the url for batch request
     * 
     * @param {object} data 
     * @param {string} url 
     * @param {string} ent 
     */
    getOdataV4UrlForBatch = (data, url, ent) => {
        if (typeof data.Ent !== 'undefined' && data.Ent) {
            url += '/' + data.Ent;
        } else {
            url += '/' + ent;
        }
        return url;
    }

    /**
     * Process the odata return list recursively, 
     * replacing all @odata.etag to Key and remove @odata.context
     * @param {object} data - contains all the result of data
     * 
     * @return {object}
     */
    processODataV4Result = (data) => {
        // Remove entries that are undefined
        let i = 0;
        while(i < data.length) {
            if (typeof data[i] === 'undefined') {
                data.splice(i, 1)
            } else {
                ++i;
            }
        }

        for (let index in data) {
            for(let field in data[index]) {
                // remove element with space only
                // remove @odata.context
                if (data[index][field] === '' || data[index][field] === '@odata.context') {
                    delete data[index][field];
                }

                // check if an array
                if (Array.isArray(data[index][field])) {
                    this.processODataV4Result(data[index][field]);
                    continue;
                }

                // change the etag to Key
                // will be use in updating/deleting
                if (field === '@odata.etag') {
                    data[index].Key = data[index][field];
                    delete data[index][field];
                }

                // Convert the images to base64 src
                // @todo: enhance this part, temporary to make it run
                if (field.includes('Picture') && typeof data[index][field] !== 'undefined') {
                    data[index][field] = 'data:image/png;base64,' + data[index][field];
                }
            }
        }

        return data;
    }
}

export default NavisionConnector; 