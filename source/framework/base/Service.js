/**
 * The base class for services.
 * Contains all the generic properties and functions
 * shared among the different services.
 */

class Service {
    // Define all the required properties
    attr;

    /**
     * Constructor initialized all the properties
     * @param {object} properties 
     *                 - attr (object) - attribute object
     */
    constructor(properties) {
        this.attr = (properties && properties.attr && Object.keys(properties.attr).length === 0 && properties.attr.constructor === Object) ? properties.attr : {};
    }

    /**
     * Timeout a fetch request
     * @param {url} - URL to fetch
     * @param {object} - additional headers, method, data and timeout
     * 
     * @return {response}
     */
    fetchWithTimeout = async (url, options) => {
        const { timeout = 6000 } = options;
        const controller = new AbortController();
        const id = setTimeout(() => controller.abort(), timeout);
    
        const response = await fetch(url, {
            ...options,
            signal: controller.signal  
        });
    
        clearTimeout(id);
    
        return response;
    }
}

export default Service;