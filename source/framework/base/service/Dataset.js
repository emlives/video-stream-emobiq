/**
 * Dataset service
 * Contains all the generic properties and functions
 * shared among all the dataset services.
 */

import Service from '../Service';

class Dataset extends Service {

    /**
     * Constructor initialized all the properties
     * @param {object} properties 
     *                 - attr (object) - attribute object
     */
    constructor(properties) {
        super(properties);
    }
}

export default Dataset;