/**
 * Connector service
 * Contains all the generic properties and functions
 * shared among all the connector services.
 */

import Service from '../Service';

class Connector extends Service {

}

export default Connector;