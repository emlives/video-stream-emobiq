import React from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/FlatListEndReachedPage';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import DataList from '../framework/component/DataList';
import Label from '../framework/component/Label';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {

    handleDataList30ScrollBottom: () => {
        console.log('------- end reached -----')
    },

	handlePanel309Action: () => {
		functions.gotoPage({
			"p": "TestPage"
		});
	},

	handlePgTestLoad: () => {
		functions.userDefined('globalModalLoading', {});
		functions.rawCall({
			"method": "GET",
			"callback": (input, extra) => {
				functions.dataFromString({
					"string": input,
					"callback": (input, extra) => {
						functions.userDefined('globalModalHide', {});
						functions.loadData({
							"dataset": "l_abc",
							"callback": null,
							"errCallback": null
						});
					},
					"errCallback": null,
					"dataset": "l_abc"
				});
			},
			"errCallback": null,
			"connector": "rc1",
			"path": "posts?_start=0&_limit=10"
		});
	}
};

const FlatListEndReachedPage = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;

    // Manipulate the props to support css styles
    formatParentStyleLayout(props);

    return (
		<Page data={props.pgTest} load={actions.handlePgTestLoad} currentTime={currentTime}>


				<Panel data={props.pnlHeader} functions={functions} currentTime={currentTime}>
					<Panel data={props.Panel309} action={actions.handlePanel309Action} currentTime={currentTime} functions={functions}/>
				</Panel>

            	<Panel data={props.pnlContent} functions={functions} currentTime={currentTime}>


				<DataList data={props.DataList30} scrollBottom={actions.handleDataList30ScrollBottom} currentTime={currentTime} functions={functions}>
						{({ item, parentStyle }) => (					
							<Panel data={item.Panel676} functions={functions} currentTime={currentTime} parentStyle={parentStyle}>
								<Panel data={item.Panel847} functions={functions} currentTime={currentTime} parentStyle={parentStyle}>
									<Label data={item.Label806} functions={functions} currentTime={currentTime} parentStyle={parentStyle}/>
								</Panel>
							</Panel>
						)}
					</DataList>


                </Panel>
			
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(FlatListEndReachedPage);