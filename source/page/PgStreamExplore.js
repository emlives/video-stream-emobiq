import React from 'react';
import { connect } from 'react-redux';

import Page from '../framework/component/Page';
import Label from '../framework/component/Label';
import Edit from '../framework/component/Edit';
import Button from '../framework/component/Button';

import Functions from '../framework/core/Function';

import { StateName } from '../reducer/data/AppPage';

let functions = new Functions(StateName);

const actions = {
    handleClickInactivityTimeout: () => {
        functions.inactivityTimeout({
            currentPageOnly: false,
            timeout: 5000,
            callback: function() {
                alert('first');
            }
        });
    },
    handleClickGoToPage: () => {
        functions.gotoPage({ p: 'ArrayPage' });
    },
    handleClickGoToSamplePage: () => {
        functions.gotoPage({ p: 'SamplePage' });
    },
    handleClickGoToCssPage: () => {
        functions.gotoPage({ p: 'CssPage' });
    },
    handleClickGoToStylePage: () => {
        functions.gotoPage({ p: 'StylePage' });
    },
    handleClickGoToOrderHistoryDetailsPage: () => {
        functions.gotoPage({ p: 'OrderHistoryDetails' });
    },
    handleClickSetComponentFocus: () => {
        functions.console({
            value: functions.setComponentFocus({
                name: 'editFocus',
                // id: '',
                selectAllText: 'true',
                // hideKeyboard: true
            })
        });
    },
    handleClickBBCodeToCanvas: async() => {
        let test = await functions.bbCodeToCanvas({
            text: 'Hello There \n\n\n\n Haha \n\n\n\n Hi There :3 boooyaaah',
            // font: "",
            // size: 200,
            // canvasWidth: 150,
            // marginTop: 5,
            // marginLeft: 5,
            // marginRight: 5,
            // marginBottom: 5,
        });

        functions.console({
            value: await test.toDataURL()
        });
    },
    handleClickBBCodeToCanvasSync: async () => {
        functions.bbCodeToCanvasSync({
            text: 'Hi :3 [img|x=0|y=30|width=150|height=150]data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAACWCAYAAAA8AXHiAAAezUlEQVR4Xu1dCVhTx9o+wyJg2AME2QVFS2VRcCkotvxqFZerlhZtXdrrwqbiQq3aqkVRrxuoRXBthdbWrSItQqvVuq+oIKUIRRADJJCQELawme9/pj/cnyJwspyEkOQ8D4/LzLe98zIzZ87MN4jQPBoE5IAAkoNOjUoNAoSGWBoSyAUBDbHkAqtGqYZYCuIAAGgRBBFFEEQtQRCFBEE8RghVKci8ws1oiKUgyAHgraKiol9PnjypPWnSpEZHR8fGxsbGEkNDw0ILC4ubBEFcRgi9UJA7cjejIZbcIf4/AwDw8enTpw/MmTPHqN2kg4ODaMyYMcT06dPrfHx8qvX19UudnJyutZEM/9lnHw2xFNR0ALBj/fr1n/3nP//pFnNHR0fR5MmTibCwsCoHB4cGhNC3pqam5xBC2QpykzIzGmJRBmXPijgczpWlS5e+k5KSIhbmdDpdFBQU1PDRRx9xPDw8Gk1MTPYhhI4oyF2ZzYgVpMxWNAqI6urql76+vnZ//vmnxJh7eHiIoqKi6gICAupoNNohU1PTOIRQnTLDKnGQyhyMMvsmEole6erqolevXkmNua6urig0NFS4YsWKSgaDccHIyGgjQqheGeOWOkhlDEZZfQIAl8rKyscMBsOYKh9XrlxZv2vXLl2hULjLxMRkI1V6qdKjIRZVSPagBwACb9++nTx27Fg61eY2bNhQHxYWVmFnZ7cBIXSaav3S6tMQS1rkJJADgNVxcXF7Vq9eLRe88RC5d+/eiunTp2c7OTktQwg9l8A9uVSVS6By8bQPK62vr/9m1apVC48cOSJXvH18fESHDh0qc3Z2PmRubr69NyGTa6C9GVh3tgGgP0EQDgRB2BIEgec8NgRBaBMEYUIQBF685BIEUUMQxCuCIIQEQTQQBMFv+382QogjaVxVVVVPZs+e7Xn9+nWF4L1161ZeREREmZmZ2QSEUKWk/lJRXyGBUuGopDoAQIcgCG+CIEYRBOFcWlrqbmVl5VNSUsLV1tamV1VVif74449afX197ezsbBsAwKvjiMPhiFpaWlr09PSQnp4emJub862srARGRka6tra2AlNTUzAyMmKZmpqm2djYHBbHr7q6OsGgQYOM2Gy2wvAePny46NatWw39+/dfhBA6I46fVNZRWKBUOt1DbzSBIAivurq6+TQa7c3U1NQKLpdLu3nzptGzZ8+I4uJiTByZY543b15jbGxsjpWVFSZtjw8AGDc1NVXq6+vrkdWVR3lKSgr3rbfe+tHa2jpUHvq70ykzyIp0trMtAOhHEMSckpKSKXQ6fUpBQUFTUlJS/wcPHtDu3bsnt9giIyNh+/bth2g0WjhZ/AAwMi8v7zc3NzfKlhrIbHYuDw8Pr9uyZUumhYXFO5LKSltfbuBL65A4cgDwLw6HM1NbWzvw2rVrOkeOHDH99ddf8bYUhTxHjx5tWrx48UqE0CEygwAw78KFC/tnzZplTlZXnuUjR44UZWRkvKTT6d4IIZ48bWHdfYZYAGDG5/Nxdx6Rm5urtWPHDkZ6errCyNSxITIzM/ne3t7vIYR+J2sgAIj54osvNmzbtq3XsTYwMBDl5OSwXVxc8KQ+j8x3Wcp7PVgy5/GqtVAo3IwQCtqwYYPoxIkTBnw+v1cI1e4rj8drMDMzG4wQKifzv7Ky8lJ4ePiEc+fOKQ3WmZmZTG9v75kIocdk/ktbrjTBdjF/cuZwOJuLi4vHfP3117aHDx+mSRsklXKGhoZQVVWF3xrFmozX1NQ89/PzG5iTk6NUWD9+/Jg5fPjwN+T1rVGpgsUEAAA9Npu9T0tL66Nly5bRzp4926u9U2dSent7w9WrVwtMTEyGikPY1tbW1v79+2s1NzcrHdZZWVmlnp6eeEh/IE4sktRRqmABICI/P3/V7t27GcePHzeUJBBF1f3www8hNjY2w9raeiqZTQBw4nA4eFlCKWPB/j958qTUy8vrXYTQn2TxSFKuFMQCgOH19fXnjx07Zrxy5cpefXsiA2/Lli2wcePGLxFCW8jqAsC7d+/e/d7X11epY8rPz69wdXUdjhBikcUkbnmvE4vP53+Zl5f38eLFix2k2QQnbqBU1bt48SI/MDAwVJzVbABYkZiYuDM8PFyfKvvy0KOtrQ1cLpdlZmaGP3NR8vQasQDAuby8PCUmJsY5MTFRaYeKzigXFBTUDR48eKw4+9Dr6+uPfvrpp4sSEhJ6DWdxWfLGG2+Ibty4kWVpaYk/g8n89ErAALCgqqpq/6hRo4yLioqUanJOhmhLS0urjo6OEUKokawuh8N5FBwcPPzq1au9gjOZf53LFyxYULdt27YMe3v7DySV7Vxf4QGXl5cfunLlyuz58+dbyuq8ouUdHBwgNzeXZ2RkZCGO7bq6Ot6QIUNMy8rKFI6zOP51Veebb76pnDZt2g5LS8t90urAcgoNuKCg4NrWrVtHfPvtt/89WyeL84qWnThxIpw8efKRlZXVSDLbAEBraWnh9evXD3/P7FMPk8mstrOz85PlTVEhxAIAenl5+a3Jkye75uTk9KmhryMjli1bBjt37jxKo9FCyJgCACPy8/OvDR06tM/9Etna2ory8vLKjY2N7cni7K5c7sQCADc2m53h5ORk19TU1GdJhQFMTExsCg0NjUIIxZMBDgBz09LS4qdPn67USw3dxRESEoJ3RHzHYDDCyGLtqlyuxAKA0SUlJRednJwoP0QgTbCyyty7d483evToOQihy2S6AODL6OjoTV9++aVcMSbzQ5byixcvsgIDAz9ACN2SVI/cggYAw+Li4mJnZ2exJrqSOt4b9blcbj2dTsff15hk9isqKjIiIyPfPXXqlNwwJvNB1nI7Ozu8C/Whk5PTGEl1ySVovLmtoKDgpyFDhlhL6pCy1jcwMICamppWXV1dsSbjAoHgr/Hjx7tkZWXJBWNF4bRu3TpuVFTUHgsLi52S2KQ8aDynKi0tvW1vb28qiSPKXtfLywuuX7/+3MTEZLA4vra0tDQbGxvrCIVCyjEWxz6VdfLz84tcXV1HSZLPi9Kg8dsfl8vNsbS0HEBlYMqgKzg4GA4cOHCJwWBMJvMHAOz5fH6uubl5n3sj7Cq2oKAg0eHDh0/T6fQPyWJvL6eUWGw2+y8HBwfn5ubmPv321xV4mzZtgujo6K0Ioc1k4ALAhPv3758ZM2aMGVndvlJeXFzMc3Jy+h+EUJY4PlNGrIKCgt+DgoL8nz59qnKkwkD++OOPdbNnzw5BCH1PBize/nPkyJE9ISEhSv3xmSyOjuVTpkwRnT59OsPY2HiaOHKUEKusrOyr9evXL0xOTlaJrr8r4PLy8mqHDh36DkLoERmwAHBgxYoVy7766itK8CWzp6jymzdvFo8dOxZvaX5KZlPmwAHgo2+//TZ2wYIFVmTG+nJ5c3Nzs66urrk4W3krKysfzJs3z+fSpUsy46tMmE2fPl109OjRb6ytrReT+SVT4ADgyOPxntLp9F47M0cWIBXltra28OzZM3waWqw5U21tLWfYsGH0kpISmfClwneqdTx69KhsxIgR+DtiSU+6ZQqczWbn+Pv7uxUUFKjkvKoduICAADh9+vQTcfYqAYB+a2trra6uLj7ir3JPZGSkKCYmJsHIyGi5XIjF5/M3bdy4cW18fLxSnJ6RZwuGhYXBnj17vqbRaKRDAAB4FhUVXXdxccFJRlTuwfksGhoaWrW1tXtcKJaqxwKAYXfu3PnZz8/PSeWQ6yKg/fv3w4oVK1YhhPaTxQsAH6Snpx+aOnWqWMMmmT5lLD927FjFokWLliOEznbnn1TEqqmp+WvcuHEu2dnZUskrI1g9+XT79m2er6/vRwihX8h8B4CNMTEx0Rs3blRZbPz8/ESnTp1Ksbe3D6KMWAAQevz48S8XL17MIANZVcorKytrLS0tPRFCxWQx8fn8lGXLlv3r5MmTKkssjEF+fn6lq6ure3f5tyQKHuePev78edGgQYPUYgjEAPbr1w/PKUQ6OjpiTcarq6ufTZgwwTUzM1MibMkIq2zlhw8fFixduhSnPuhyeiBR8Ewm80BMTMwnhw8f7jOnamRtEHd3d7hx48ZLMzMzsX6ZmpqaGi0sLPrV1tZKhK2sfipaHg+HqampN7pLjSR28ABgx+PxntHpdJV/C+zYSEFBQZCQkPCblZXVJLLGAwAbgUCQb2pqqha/eHl5eYVDhw716mrRWGxi1dfXHwkJCVn03XffqfSaVWfyfP755xATE7MdIfSFGMR659GjRz/6+Pio7BthRwySkpI4CxYsWIwQ+qkzNmIRCwAcsrKyfsHZScjAVbXyM2fO1L3//vvh+MIkstjwi82xY8f2LVmyRKxMNGT6lL186dKlEBcXl0Sj0T6Rili1tbWJ69atm3fw4EG16OI7gpSbm1vj5uY2UZyMLAAQt2rVqsh9+/aJ9Qur7MQh88/FxUV0//795xYWFq4SEwunr25paanu16+fLpkhVSzHt1Xq6elZI4QEZPGx2ex7n3zyyaiMjAy1IBbGIycnJ3/YsGHjOqcpJwUA7y1at27dzp07d6rVpB2DZm1tDYWFhbWGhoZifZ6pqamp8PLysiwqKiLFlYykfaX8/Pnz5bNmzcIpv/+xeEwKQEVFxV+enp7ObDZbrSbtuGHHjx8P58+fz6bT6cPJGhrnlReJRE3a2tpqhVN0dDRs2rQpGiEU3RGjHomF8zs9fPgwadSoUWqzyt4RnJ4mp52JBgBvlpSU3HFyclLpLUSd4542bRreD/+Tra3tLLGJVVtbe3DBggWhKSkpavVb2A5QbGwsrFq1Cp98jhWjx3rvt99+S5w4cWKfS3ZCFltP5c7OzqLLly/fdXFxGSs2sbhcLtvCwkIteysM0o0bN6rGjRu3ECF0kQx8ANiwY8eOmA0bNpBOL8h09bXyhoYGfLXKP+bg3YKgDts/yBqwvLy8bsCAATiFYiFZXT6ffy4yMnJ2cnKy2hGLw+EILCwsRiCEitpx6hYENpt9fOnSpR//9NNPajkMamlpQWtrK6GlpSVW/Pgc4eTJk9+4f/++2hErJSWFNXPmzH/keOgWBB6PV2Zra2stFArFApbsN7qvlbu5ucGdO3dKTU1N8RV0pE9jY2ODtbW1fnV1tdoRKykpiblgwQJ8Bcz5HnssAPDLzc1NxQcCSBFV0QqzZs2C+Pj4W7a2tv5kIQKAVV1dXbGRkRG+C1HlHxqNhq/bI9p/Vq9eTUybNm0NQiiOjFgr1q5du3337t1qtyjaDsy6detgx44dOxFC68mYAgDjsrKyfho+fHifyleB717sSJCOf6fT6YSlpWWLnZ1djYmJia6pqakOLqfRaPoikehVY2NjPUEQVU1NTbUtLS31AwYMwOcNq3okFo/Huzlz5kzfGzduqOUwiMH54Ycf6ubMmYP3dZ8Qg1hLvvvuuz3z589X+BqWjo7Oa+QwMzP7uzfB5MB/2tvbV1tYWAAmh7GxsS7+fwMDAwP8uaqlpaVGJBLxm5uba4RCYa2Dg8MzgiDw7WDtP/h22Y7/5iGEmskw6XI+8OrVq1Z9fX2tlpYWtZsvtAP29OlTgbu7+xSE0F0yEAFgT1RU1Oq9e/dKjRdOk9RT74HLnJycuCYmJjptvQfCJNHR0dEVCoX4iuEqTA7cg+C/W1tb4wvH/0GIjv+W99VyrwGBjy/9+uuvP02ePFmsSSsZ6H21vL6+vrF///624jQAi8W6s2TJkjFpaWnIxMSkxx5kwIABeAmjCZMDDzGYMIaGhnoAAHh4AQAe7kUaGxtrzczMXhgYGLB76kEQQphISvd0RaxFZ86c2RkcHKy2E3c8bDCZzHoDAwOxclHU1tb+oq2tPU5fX9+gubm5qbm5uRYTBPceTU1NNXZ2dgU99R64DCHUpHTskMGhroi1MyIiIiohIUFt51djx46F1NTUP+h0uoc42OJ7n/G5izaCiMSRUfU6rxGrrKwsZe7cuTPUeeK+aNEi2LVr1xk6nT5H1Qkgr/heI1ZDQwPLzc2N8eLFC6knovJyVlF6d+/eDVFRUZ8hhHYryqaq2XmNPMXFxS8HDhwodeJ4VQDo2rVr3PHjx+PNa68dElCF+BQRwz+IhXNnvnz58omjo6PaTtwx6KWlpbW2trYjEUL5imgEVbTRmVi+T58+Tff09BRrK64qAoJjAgACIaS2UwEq2rUzsaYmJycnLly4UG2HQldXV3j06FG5kZGRHRUAK6sOnC6BIAh8/hFfydLxh4YQOiqr352J9cmFCxe2zZo1S+XSaYsL1IwZM+DQoUN3bGxs/rEjUlx5RdfDl7N3IEZXRDEvLS111dPTM9bT0zPS1tY21dHRMdHT09NvbGwU8vl8Ef4RCAQtXC4X6erq0iZPnjwPIXROllg6E2tJQkLChoiICLHyFMhiWFllP/30U7zUsAchtFaRPgIAXozt3Hv8lyhCodC6urraERNEV1cX16UbGBj0RwhpNTQ0CLlcrlZNTU1LdXV1K5vN1isvLzesqqoieDwewefz//6z409323uCg4MFp06dwltgSL+R9oRPZ2Kt3Lx5894tW7ao7eLoiRMnmhYuXBiBEDouKbEAAOPWZa/R/v/V1dX2TU1NA/r16/d3D9JGEH18cysmBo/HA9x7YIIwmUwTDofTr50g7cToSJSGhgZK54JTp04VpKWlbUAIJUgaf8f6nYm1ecuWLZs3b95MqbOyOKho2SdPngi8vLzwiRP8Roh7kG6HF319fSNMkA7DSz+hUNjYcXjhcDiIyWSaYTJ0JkhHorS2tioF5m+//bbgwIEDhz08PD6TBfvOxFq1fPnyPfHx8WrbY125coXv7+9vJBQKGzgcDh5eWnEPUl5ers9isXocXgQCgVKQQxZCBAQECGNjY7/38vIizbcqyVCo9j2WtrY2vHr1qs8TRFpyzZkzBxITE9PMzMxmSKsDy3XusVZ8/vnncdu3b1fbHksWMFVBNjg4GOcXPYAQWiVLPJ2JFXro0KHPwsLC1PatUBYwVUF20aJFomPHjsUihD6VJZ7OxPp3SkpKzOzZs9V2HUsWMFVBlqrlls7EmpacnHxw4cKFar17VBUIIm0Mq1evFu3duzdSnAvVJZm8+2VnZ1/08vJS62+F0jaKKsidPn266oMPPsBbhiRex+tpHcsxPz//wdChQ1X6Ji9VIIC8YkhPT2dPmTJlAULosiw2Og+FWmVlZUw7OzsbWZRqZPsuAg8ePGCOHDlyEkIIHwOT+nltvUYoFFa4urpaMplMtV3LkRpNFRBsbm5u0dXVNUUINcgSzmvkYTKZ5+fOnfuvW7duadayZEG2D8oyGAzRw4cPHzs4OIyU1f2uTunsCw8PX56YmKghlqzo9jH5gIAA0blz5+6am5vLvGWoK2ItTU5O3rNw4UKxztT1Mew07vaAwKpVqyA2NnYfQmi1rEB1RawRGRkZ5wMDAx1lVa6R71sIxMXFVa9cuXKJrJv8cNRdTtABQITzjbVtX+1b6Gi8lRoBFoslsLa2HoUQwie3ZXq6JBaHw7k2Y8aMcXfv3tXMs2SCt+8Im5mZiQoLC8vodDolX12667HWrFmzJjo2NlZt82P1HUpQ42lgYKDoxIkTyVZWVq/diyONhe6I9XZWVtbZ4cOHW0ijVCPT9xBITk7mzp8/H19sKdOW5PbIu10EZbPZpba2tjbqvOmt79FDeo//+OOPkjfffPNthNAL6bX8v2S3xCopKTkdERERlJaWpplnUYG0EutwdXUV/fbbb5kODg6jqXKzW2IBwEepqakHZs6ciQ8UaB4VRiA6Olq4adOmvQihjVSF2ROxtFksVpGNjQ0lbwlUOazRQz0C9+7dKxk9enQgQuhPqrT3+KG5srIy6d///vc8zXBIFdzKp2fIkCGiq1ev3rW1tZX5M07H6HokFgBMv3PnzmE/Pz/NVmXl4wQlHn311VeCZcuWbaM6Fxjp1piysrICDw8Pl6qqKs0knpKmVC4lhYWF5S4uLsMQQjjtNmUPKbF4PN76bdu2fb53717NYillsCuHohkzZuATOSesrKwWUe0RKbEAwKyxsbEMJ5yn2rhGX+8ikJWVJfD09JyKELpNtSekxMIGa2trD0dGRn749ddfq90t9lQDriz6vL29Renp6ZkMBoOytSuxJ+/tFQFg0M2bNy/5+/sPVBZgNH7IhkBycjJz/vz5eIvMr7Jp6lparB4Li/L5/DMhISHvnTlzRjOJl0dLKFCnu7u76Pr163nm5ubD5GVWbGLhXquiouKRtbW1wi8iklfw6qr3+vXrVf7+/h8jhNLkhYHYxMIOsFisg2vWrAn9/vvvNb2WvFpEznpHjhwpOnPmzJWBAwdOkqcpiYiF0xn+9ddf2a6urpq5ljxbRY66L1++zJwwYcIshNAjOZrpemtyTwYB4LP9+/evX7lypeYYvjxbRg66Fy9e3LBz5850Op3+vhzU/0OlRD1Wu2RdXd0LLy8vh8LCQqnk5R2URn/XCLx69apFS0tLDyEE8sZIKmIAwKiMjIyzgYGBmp0P8m4hivTHx8ezIyIiViGETlGkskc1UhELaywtLY2Njo5ecvToUc2iqSJaSgYb/v7+ouPHj6cPHjx4ugxqJBKVmljYSnV19Ys33njDnsViad4SJYJdsZWrq6sFJiYmNrLmY5DEa5mIBQBuLBbrho2NjVpf6iQJ4Ique+XKFU5AQEAoQui8Im3LRCzsKAAsjo+P37p8+XJrRTqusUWOQGhoaN2uXbuOGBsbryGvTW0NmYmF3SkqKkqKioqaff78ec18i9r2kVqbm5ubKCMj43dHR8cJUiuRQZASYmH7z549exgYGDiiqKhIM9+SoUGoEi0sLHzh4uLirIilha58poxY+B4ZHo/3gk6nq+2VdFSRQlY9mZmZpd7e3u9SeThCUp8oI1bbfGtgaWnpQ3t7e81kXtKWoKj+7du3S3x9fcMQQhkUqZRKDaXEaiPXWwUFBReGDBmiSZArVZNIL3T27Fm8a2ETg8Gg5Ji89J50k8ZIFoVt5Jpw586dE35+fray6tLIi4fAyZMnOZMmTdphaWkZJ56EfGtR3mO1u3vw4MFBb7311sMRI0aYyjcEjfZvvvmmwsPD431vb++byoKG3IjV1nO9m5ube2LYsGGaNS45tfjZs2d5AQEBW+l0+j45mZBKrVyJ1UYu39zc3B815JKqfXoUevjwYamDg8N2BoORSL122TTKnVht5Br88uXLjLFjxw5kMpmadS7Z2uxv6ezs7DIPD4+lCKF0CtRRrkIhxGojl15BQcHtqKgo159//lmTkVnKpsQHIVJTU0sHDhyI16lkuj1CShfEElMYsdq9efny5Q9paWnjw8PDNfkgxGqi/68UFhZWv3bt2jvy3q8uoVtdVlc4sbAXXC53bU1NzVofHx8zHo+nGRrFaMnff/+d6+3t/bWxsbFMl4CLYYqSKr1CrLahcURFRcXZNWvWWJ08eVLz8bqb5sS3RaSnpzfp6enNRQilUtLqClDSa8Rqj43FYh3IzMwMCgkJsS4vL+91fxSAudgmEhIS2BMnTnw4aNCgIIRQs9iCSlBRKRoSAMYLhcLzu3fvJjZv3qz2qSnDwsIa4uLiCD09PXxv4I9KwBOJXVAKYrV7zefztzCZzA82bdo0+MKFC2o39/Lz8xPt2rXrpZeX13UajfaxxK2pRAJKRay2uReDy+Xurq2tDQwNDTW7dOmSyhPMx8cHDhw4wGcwGPecnZ3XI4SeKhFHpHJF6YjVHgUAjODxeF88efJkWHx8/KALFy4ora9SIU8QhK+vr2j58uXl7777brWZmVmUvDK/SOufLHJK31iYYDU1NTsaGxvHrFu3zjA5ORn19UsN3n//fdH27dt5pqam2RYWFjsQQldkaURllFV6YnXowVwEAsEnCKHwy5cvQ1xcnOnt27f7zDDp6ekpCg8Pr5s0aZLAwMDgFwaDcRQh9FAZSUGFT32GWB2DBYCFfD5/ikAg8P/ll19oSUlJhvfu3VM6kmEyBQcHN7/zzjsVgwcPLqLT6RcIgjimyPN9VJBEGh19klgdejEbgiA+KCoqmmhsbDzq8ePHoqSkJIvMzExUUFCg8NhsbGxEY8aMIebOnct3c3Orp9FoLEdHx4sEQZym4g5AaRq4t2QUDr68AgUAvHofSBCEe11dHR4yzVNTU6uKiopsHz16BHl5ecTz589Ra2srJTE7OTmJ3N3dkbu7OwwcOJD73nvvGbS2tpbRaLQr/fv3xymCfkEIseQVr7LrpQRkZQwSAPCVeMMJghjD4/HebGpqsre2th6dk5NT2NzcbMnn819lZWW1NDU1DWCz2dDS0vL3jbI1NTVgZGSEXxDA2NiYMDc3R1paWnwfH59WfOusvb29cOjQoQ4sFuumsbFxs6Gh4R2CIPBcKRshxFRGLHrDJ5UlVndgAgA+nmZHEATej497OfxvEUEQZgRB4DSYHIIg8EEQdpuOVpwDhSCI+rY/cS9UjBDCMpqnGwTUjlgaJigGAQ2xFIOz2lnREEvtmlwxAWuIpRic1c7K/wKzUsk8UFAV3wAAAABJRU5ErkJggg==[/img]\n\n\n\n\n\n\n\n',
            // font: "",
            // size: 200,
            // canvasWidth: 150,
            // marginTop: 5,
            // marginLeft: 5,
            // marginRight: 5,
            // marginBottom: 5,
            callback: async function(input) {
                functions.console({value: 'Success Canvas Image'});
                functions.console({value: await input.toDataURL()});
            },
            errCallback: async function(input) {
                functions.console({value: 'Fail Canvas Image'});
                functions.console({value: input});
            }
        });
    }
}

const PgStreamExplore = (props) => {
    // Append the dispatch to the function instance
    functions.pageProps = props;

    return (
        <Page data={props.page}>
            <Label data={props.lblResult}/>
            <Label componentValue="Explore"/>
            <Button data={props.btnInactivityTimeout} action={actions.handleClickInactivityTimeout} functions={functions}/>
            <Button data={props.btnGoToArrayPage} action={actions.handleClickGoToPage} functions={functions}/>
            <Button data={props.btnGoToSamplePage} action={actions.handleClickGoToSamplePage} functions={functions}/>
            <Button data={props.btnGoToCssPage} action={actions.handleClickGoToCssPage} functions={functions}/>
            <Button data={props.btnGoToStylePage} action={actions.handleClickGoToStylePage} functions={functions}/>
            <Button data={props.btnGoToOrderHistoryDetails} action={actions.handleClickGoToOrderHistoryDetailsPage} functions={functions}/>
            <Edit data={props.editFocus} functions={functions}/>
            <Button data={props.btnSetComponentFocus} action={actions.handleClickSetComponentFocus} functions={functions}/>
            <Button data={props.btnBBCodeToCanvas} action={actions.handleClickBBCodeToCanvas} functions={functions}/>
            <Button data={props.btnBBCodeToCanvasSync} action={actions.handleClickBBCodeToCanvasSync} functions={functions}/>
        </Page>
    )
}

// Bind the state to the props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
}

export default connect(mapStateToProps)(PgStreamExplore);