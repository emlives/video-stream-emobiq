import React from 'react';
import { connect } from 'react-redux';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Label from '../framework/component/Label';
import Button from '../framework/component/Button';
import DataList from '../framework/component/DataList';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

import Functions from '../framework/core/Function';

import { StateName } from '../reducer/data/NavisionPage';

let functions = new Functions(StateName);

const NavisionPage = (props) => {
    // For datalist autoload handling when to render
    const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;

    let handleClickOne = async () => {
        console.log(functions.navCall({
            connector: 'n_nav',
            entity: 'OK365_Order_Entry',
            function: 'Create',
            data: {
                Sell_to_Customer_No: 'CSESTR-005',
                // SalesLines: {
                //     Sales_Order_Line: [
                //         { 
                //             No: 'A01-BADAM',
                //             Quantity: '1',
                //             Type: 'Item'
                //         },
                //         { 
                //             No: 'A01-DRYFRUITS',
                //             Quantity: '1',
                //             Type: 'Item'
                //         }
                //     ]
                // }
            },
            callback: function(data) {
                console.log('Success: ', data);
            },
            errorCallback: function(data) {
                console.log('Error: ', data);
            }
        }));
    }

    let handleClickTwo = async () => {
        console.log(functions.navCall({
            connector: 'n_nav',
            type: 'codeunit',
            entity: 'OK365_GetItemSalesPrice',
            function: 'fnGetItemSalesPrice',
            data: {
                itemNo: 'A01-BADAM',
                customerNo: 'CSESTR-001',
                orderDate: '2021-02-15',
                uOM: 'KG',
                currencyCode: '',
                itemVariantCode: ''
            },
            callback: function(data) {
                console.log('Success: ', data);
            },
            errorCallback: function(data) {
                console.log('Error: ', data);
            }
        }));
    }

    let handleClickThree = async () => {
        console.log(functions.navCall({
            connector: 'n_nav',
            type: 'company',
            callback: function(data) {
                console.log('Success: ', data);
            },
            errorCallback: function(data) {
                console.log('Error: ', data);
            }
        }));
    }

    let handleClickFour = async() => {
        functions.console({value: await functions.loadData(
            {
                dataset: 'n_item_list',
                // order:[
                //     {
                //         "f": "No",
                //         "v": "desc"
                //     }
                // ],
                limit: 5,
                // page: 2,
                // filter:[
                //     {
                //         "f": "No",
                //         "o": "=",
                //         "v": "A01-BADAM"
                //     }
                // ],
                callback: (data) => {
                    functions.console({value: data});
                }
            }
        )});
    }

    let handleClickFive = async() => {
        functions.console({value: await functions.loadNext(
            {
                dataset: 'n_item_list',
                datasetDisplay: '',
                beforeCallback: () => {
                    functions.console({value: "Before loading the next data..."});
                }
            }
        )});
    }

    return (
        <Page>
            <Label data={props.lblResult}/>
            <Button data={props.btnCreateOrder} action={handleClickOne}/>
            <Button data={props.btnGetItemPrice} action={handleClickTwo}/>
            <Button data={props.btnGetCompany} action={handleClickThree}/>
            <Button data={props.btnLoadData} action={handleClickFour}/>
            <Button data={props.btnLoadNext} action={handleClickFive}/>
            <DataList data={props.dataList1} functions={functions} currentTime={currentTime}>
                {({ item, parentStyle }) => (
                    <Panel>
                        <Label data={item.LabelKey} parentStyle={parentStyle} />
                        <Label data={item.LabelValue} parentStyle={parentStyle} />
                    </Panel>
                )}
            </DataList>
        </Page>
    )
}

// Bind the state to the props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
}

export default connect(mapStateToProps)(NavisionPage);