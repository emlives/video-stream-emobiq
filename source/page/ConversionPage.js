import React from 'react';
import { connect } from 'react-redux';

import Page from '../framework/component/Page';
import Label from '../framework/component/Label';
import Button from '../framework/component/Button';

import Functions from '../framework/core/Function';

import { StateName } from '../reducer/data/ConversionPage';

let functions = new Functions(StateName);

const ConversionPage = (props) => {
    // Append the dispatch to the function instance
    functions.pageProps = props;

    let handleClickOne = () => {
        functions.setComponentValue({
            name: 'lblResult',
            value: functions.join({ values: ['1', 'Kevin'], separator: '-<3' })
        });
    }

    let handleClickTwo = () => {
        functions.console(
            functions.split({ value: 'Haha ~ HAha ~ Kevin', separator: '~' })
        )
    }

    return (
        <Page>
            <Label data={props.lblResult}/>
            <Button data={props.btnJoin} action={handleClickOne}/>
            <Button data={props.btnSplit} action={handleClickTwo}/>
        </Page>
    )
}

// Bind the state to the props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
}

export default connect(mapStateToProps)(ConversionPage);