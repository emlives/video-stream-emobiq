import React, { useEffect }  from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgOrderItem';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import Edit from '../framework/component/Edit';
import DataList from '../framework/component/DataList';
import ComboBox from '../framework/component/ComboBox';
import Button from '../framework/component/Button';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handlePnlMainHeaderRight518Click: async () => {
		functions.userDefined('globalModalLoading', {});
		functions.conditional({
			"condition": functions.equal({
				"value1": null,
				"value2": "All"
			}),
			"yesCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "lblAll",
					"value": "My"
				});
			},
			"noCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "lblAll",
					"value": "All"
				});
			}
		});
	},

	handlePnlProductBodyRightClick: async () => {
		functions.setVar({
			"var": "vItemDetails",
			"value": data
		});
		functions.userDefined('globalModalLoading', {});
		await functions.loadData({
			"callback": async (data, extra) => {
				functions.setVar({
					"var": "vUOM",
					"value": data
				});
				functions.setComboOptions({
					"combo": "ComboUOMItemList",
					"data": data,
					"valueField": "Code",
					"displayField": "Code"
				});
				functions.setComponentValue({
					"component": "ComboUOMItemList",
					"value": functions.getVarAttr({
						"var": "vItemDetails",
						"attr": "Base_Unit_of_Measure"
					})
				});
				await functions.navCall({
					"connector": "nav",
					"ent": "OK365_GetItemSalesPrice",
					"function": "fnGetItemSalesPrice",
					"data": functions.toObject({
						"itemNo": functions.getVarAttr({
							"var": "vItemDetails",
							"attr": "No"
						}),
						"customerNo": functions.getVarAttr({
							"var": "vCurrOder",
							"attr": "Sell_to_Customer_No"
						}),
						"orderDate": functions.dbDate({}),
						"uOM": functions.componentValue({
							"component": "ComboUOMItemList"
						}),
						"currencyCode": functions.getVarAttr({
							"var": "vCurrOder",
							"attr": "Currency_Code"
						}),
						"itemVariantCode": functions.getVarAttr({
							"var": "vItemDetails",
							"attr": "itemVariantCode"
						})
					}),
					"callback": async (data, extra) => {
						functions.userDefined('globalModalHide', {});
						functions.console({
							"value": functions.objectAttr({
								"object": data,
								"attr": "return_value"
							})
						});
						functions.console({
							"value": data
						});
						functions.setVar({
							"var": "var_itemSalesPriceSelected",
							"value": functions.objectAttr({
								"object": data,
								"attr": "return_value"
							})
						});
						await functions.loadData({
							"dataset": "l_orderlist_details",
							"filter": functions.toArray({
								"value1": functions.toObject({
									"f": "No",
									"o": "=",
									"v": functions.getVarAttr({
										"var": "vItemDetails",
										"attr": "No"
									})
								}),
								"value2": functions.toObject({
									"f": "Unit_of_Measure",
									"o": "=",
									"v": functions.componentValue({
										"component": "ComboUOMItemList"
									})
								})
							}),
							"callback": async (data, extra) => {
								functions.setVar({
									"var": "vCurrCartItem",
									"value": data
								});
								functions.conditional({
									"condition": functions.greater({
										"value1": functions.count({
											"values": data
										}),
										"value2": 0
									}),
									"yesCallback": async (data, extra) => {
										functions.console({
											"value": "yes"
										});
										functions.toArray({
											"value1": functions.setComponentValue({
												"component": "imgUpdateCartImage",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "GetPicture"
												})
											}),
											"value2": functions.setComponentValue({
												"component": "lblUpdateCartItemCodeValueIL",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "No"
												})
											}),
											"value3": functions.setComponentValue({
												"component": "lblUpdateCartItemNameValueIL",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "Description"
												})
											}),
											"value4": functions.setComponentValue({
												"component": "lblUpdateCartPriceValueIL",
												"value": functions.concat({
													"string1": "$ ",
													"string2": functions.formatNumber({
														"value": functions.getVar({
															"var": "var_itemSalesPriceSelected"
														}),
														"decimals": 2
													})
												})
											}),
											"value5": functions.setComponentValue({
												"component": "txtQuantity",
												"value": functions.objectAttr({
													"object": functions.getVarAttr({
														"var": "vCurrCartItem",
														"attr": 0
													}),
													"attr": "Quantity"
												})
											}),
											"value6": null,
											"value7": functions.showElement({
												"component": "pnlUpdateCart"
											}),
											"value8": functions.setComponentValue({
												"component": "lblBtnUpdate",
												"value": "UPDATE"
											}),
											"value9": "",
											"value10": ""
										});
									},
									"noCallback": async (data, extra) => {
										functions.console({
											"value": "no"
										});
										functions.toArray({
											"value1": functions.setComponentValue({
												"component": "imgUpdateCartImage",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "GetPicture"
												})
											}),
											"value2": functions.setComponentValue({
												"component": "lblUpdateCartItemCodeValueIL",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "No"
												})
											}),
											"value3": functions.setComponentValue({
												"component": "lblUpdateCartItemNameValueIL",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "Description"
												})
											}),
											"value4": functions.setComponentValue({
												"component": "lblUpdateCartPriceValueIL",
												"value": functions.concat({
													"string1": "$ ",
													"string2": functions.formatNumber({
														"value": functions.getVar({
															"var": "var_itemSalesPriceSelected"
														}),
														"decimals": 2
													})
												})
											}),
											"value5": functions.setComponentValue({
												"component": "txtQuantity",
												"value": 0
											}),
											"value6": null,
											"value7": functions.showElement({
												"component": "pnlUpdateCart"
											}),
											"value8": functions.setComponentValue({
												"component": "lblBtnUpdate",
												"value": "ADD"
											}),
											"value9": "",
											"value10": ""
										});
									}
								});
							},
							"errCallback": ""
						});
					},
					"errCallback": async (data, extra) => {
						functions.userDefined('globalModalHide', {});
						functions.setVar({
							"var": "vAction"
						});
						functions.userDefined('globalModalInfo', {
							"title": "Internal Navision Server Error",
							"message": functions.objectAttr({
								"object": functions.objectAttr({
									"object": data,
									"attr": "err"
								}),
								"attr": "global"
							})
						});
					}
				});
			},
			"errCallback": async (data, extra) => {
				functions.userDefined('globalModalHide', {});
				functions.setVar({
					"var": "error",
					"value": data["err"]
				});
				functions.userDefined('globalModalInfo', {
					"title": "Error",
					"message": functions.conditional({
						"condition": functions.getVarAttr({
							"var": "error",
							"attr": "global"
						}),
						"yesValue": functions.getVarAttr({
							"var": "error",
							"attr": "global"
						}),
						"noValue": "Cannot connect to Navision Server."
					})
				});
			},
			"dataset": "OK365_Item_Unit_of_Measure",
			"filter": functions.toArray({
				"value1": functions.toObject({
					"v": functions.getVarAttr({
						"var": "vItemDetails",
						"attr": "No"
					}),
					"f": "Item_No"
				}),
				"value2": ""
			})
		});
	},

	handlePanel186Click: async () => {
		functions.gotoPage({
			"p": "pgOrderDetails"
		});
	},

	handleImgUpdateCartClose133Click: async () => {
		functions.setComponentValue({
			"component": "txtQuantity",
			"value": ""
		});
		functions.hideElement({
			"component": "pnlUpdateCart"
		});
	},

	handleImgMinus345Click: async () => {
		functions.conditional({
			"condition": functions.greater({
				"value1": null,
				"value2": 1
			}),
			"yesCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"value": functions.sub({
						"value1": null,
						"value2": 1
					})
				});
			},
			"noCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"value": 0
				});
			}
		});
	},

	handleTxtQuantityClick: async () => {
		functions.setComponentFocus({
			"component": "txtQuantity",
			"selectAllText": "true"
		});
	},

	handleImgPlus266Click: async () => {
		functions.conditional({
			"condition": functions.greater({
				"value1": null,
				"value2": -1
			}),
			"yesCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"value": functions.add({
						"value2": 1,
						"value1": null
					})
				});
			},
			"noCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"value": 0
				});
			}
		});
	},

	handleBtnUpdateClick: async () => {
		functions.conditional({
			"condition": null,
			"yesCallback": async (data, extra) => {
				functions.conditional({
					"condition": functions.greater({
						"value1": null,
						"value2": functions.toFloat({
							"value": -1
						})
					}),
					"yesCallback": async (data, extra) => {
						functions.conditional({
							"condition": functions.greater({
								"value1": null,
								"value2": 0
							}),
							"yesCallback": async (data, extra) => {
								functions.userDefined('globalModalLoading', {});
								await functions.loadData({
									"errCallback": "",
									"dataset": "l_orderlist_details",
									"filter": functions.toArray({
										"value1": functions.toObject({
											"f": "No",
											"o": "=",
											"v": functions.getVarAttr({
												"var": "vItemDetails",
												"attr": "No"
											})
										}),
										"value2": functions.toObject({
											"f": "Unit_of_Measure",
											"o": "=",
											"v": functions.componentValue({
												"component": "ComboUOMItemList"
											})
										})
									}),
									"callback": async (data, extra) => {
										functions.setVar({
											"var": "vCurrCartItem",
											"value": data
										});
										functions.console({
											"value": data
										});
										functions.conditional({
											"condition": functions.greater({
												"value1": functions.count({
													"values": data
												}),
												"value2": 0
											}),
											"yesCallback": async (data, extra) => {
												functions.console({
													"value": functions.toArray({
														"value1": functions.toObject({
															"f": "ItemCode",
															"o": "=",
															"v": functions.objectAttr({
																"object": functions.getVarAttr({
																	"var": "vCurrCartItem",
																	"attr": 0
																}),
																"attr": "ItemCode"
															})
														}),
														"value2": functions.toObject({
															"f": "UOM",
															"o": "=",
															"v": functions.componentValue({
																"component": "ComboUOMItemList"
															})
														})
													})
												});
												functions.console({
													"value": functions.toObject({
														"Price": functions.getVar({
															"var": "var_itemSalesPriceSelected",
															"default": ""
														}),
														"Quantity": null
													})
												});
												await functions.loadData({
													"callback": async (data, extra) => {
														functions.console({
															"value": data
														});
													},
													"errCallback": null,
													"dataset": "l_orderlist_details"
												});
												await functions.loadData({
													"dataset": "l_Item_List",
													"callback": async (data, extra) => {
														functions.userDefined('globalModalHide', {});
														functions.hideElement({
															"component": "pnlUpdateCart"
														});
													},
													"errCallback": null
												});
												functions.console({
													"value": "a"
												});
											},
											"noCallback": async (data, extra) => {
												await functions.insert({
													"errCallback": "",
													"dataset": "l_orderlist_details",
													"dt": functions.toObject({
														"Unit_Price": functions.getVar({
															"var": "var_itemSalesPriceSelected",
															"default": ""
														}),
														"Unit_of_Measure": functions.componentValue({
															"component": "ComboUOMItemList"
														}),
														"Quantity": null,
														"No": functions.getVarAttr({
															"var": "vItemDetails",
															"attr": "No"
														}),
														"Description": functions.getVarAttr({
															"var": "vItemDetails",
															"attr": "Description"
														})
													}),
													"callback": ""
												});
												await functions.loadData({
													"dataset": "l_orderlist_details",
													"callback": async (data, extra) => {
														functions.console({
															"value": data
														});
														functions.setVar({
															"var": data,
															"value": "vAdditionalOrder"
														});
													},
													"errCallback": ""
												});
												functions.console({
													"value": "b"
												});
												await functions.loadData({
													"dataset": "l_Item_List",
													"callback": async (data, extra) => {
														functions.userDefined('globalModalHide', {});
														functions.hideElement({
															"component": "pnlUpdateCart"
														});
													},
													"errCallback": null
												});
											}
										});
									}
								});
							},
							"noCallback": ""
						});
					},
					"noCallback": async (data, extra) => {
						functions.userDefined('globalModalInfo', {
							"title": "Invalid Value",
							"message": "Please enter a valid value/amount."
						});
					}
				});
			},
			"noCallback": async (data, extra) => {
				functions.userDefined('globalModalInfo', {
					"title": "No Value",
					"message": "Enter the quantity."
				});
			}
		});
	},

	handlePnlFooterNavBtnUpdateTableClick: async () => {
		functions.conditional({
			"condition": null,
			"yesCallback": async (data, extra) => {
				functions.conditional({
					"condition": functions.greater({
						"value1": null,
						"value2": functions.toFloat({
							"value": -1
						})
					}),
					"yesCallback": async (data, extra) => {
						functions.conditional({
							"condition": functions.greater({
								"value1": null,
								"value2": 0
							}),
							"yesCallback": async (data, extra) => {
								functions.userDefined('globalModalLoading', {});
								await functions.loadData({
									"filter": functions.toArray({
										"value1": functions.toObject({
											"f": "No",
											"o": "=",
											"v": functions.getVarAttr({
												"var": "vItemDetails",
												"attr": "No"
											})
										}),
										"value2": functions.toObject({
											"v": functions.componentValue({
												"component": "ComboUOMItemList"
											}),
											"f": "Unit_of_Measure",
											"o": "="
										})
									}),
									"callback": async (data, extra) => {
										functions.setVar({
											"var": "vCurrCartItem",
											"value": data
										});
										functions.console({
											"value": data
										});
										functions.conditional({
											"condition": functions.greater({
												"value1": functions.count({
													"values": data
												}),
												"value2": 0
											}),
											"yesCallback": async (data, extra) => {
												functions.console({
													"value": functions.toArray({
														"value1": functions.toObject({
															"f": "ItemCode",
															"o": "=",
															"v": functions.objectAttr({
																"object": functions.getVarAttr({
																	"var": "vCurrCartItem",
																	"attr": 0
																}),
																"attr": "ItemCode"
															})
														}),
														"value2": functions.toObject({
															"f": "UOM",
															"o": "=",
															"v": functions.componentValue({
																"component": "ComboUOMItemList"
															})
														})
													})
												});
												functions.console({
													"value": functions.toObject({
														"Price": functions.getVar({
															"var": "var_itemSalesPriceSelected",
															"default": ""
														}),
														"Quantity": null
													})
												});
												await functions.loadData({
													"errCallback": null,
													"dataset": "l_orderlist_details",
													"callback": async (data, extra) => {
														functions.console({
															"value": data
														});
													}
												});
												await functions.loadData({
													"dataset": "l_Item_List",
													"callback": async (data, extra) => {
														functions.userDefined('globalModalHide', {});
														functions.hideElement({
															"component": "pnlUpdateCart"
														});
													},
													"errCallback": null
												});
												functions.console({
													"value": "a"
												});
											},
											"noCallback": async (data, extra) => {
												await functions.insert({
													"dataset": "l_orderlist_details",
													"dt": functions.toObject({
														"Quantity": null,
														"No": functions.getVarAttr({
															"var": "vItemDetails",
															"attr": "No"
														}),
														"Description": functions.getVarAttr({
															"var": "vItemDetails",
															"attr": "Description"
														}),
														"Unit_Price": functions.getVar({
															"var": "var_itemSalesPriceSelected",
															"default": ""
														}),
														"Unit_of_Measure": functions.componentValue({
															"component": "ComboUOMItemList"
														})
													}),
													"callback": "",
													"errCallback": ""
												});
												await functions.loadData({
													"dataset": "l_orderlist_details",
													"callback": async (data, extra) => {
														functions.console({
															"value": data
														});
														functions.setVar({
															"var": data,
															"value": "vAdditionalOrder"
														});
													},
													"errCallback": ""
												});
												functions.console({
													"value": "b"
												});
												await functions.loadData({
													"dataset": "l_Item_List",
													"callback": async (data, extra) => {
														functions.userDefined('globalModalHide', {});
														functions.hideElement({
															"component": "pnlUpdateCart"
														});
													},
													"errCallback": null
												});
											}
										});
									},
									"errCallback": "",
									"dataset": "l_orderlist_details"
								});
							},
							"noCallback": ""
						});
					},
					"noCallback": async (data, extra) => {
						functions.userDefined('globalModalInfo', {
							"title": "Invalid Value",
							"message": "Please enter a valid value/amount."
						});
					}
				});
			},
			"noCallback": async (data, extra) => {
				functions.userDefined('globalModalInfo', {
					"title": "No Value",
					"message": "Enter the quantity."
				});
			}
		});
	},

	handleBtnModalPositiveClick: async () => {
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "action"
				}),
				"value2": "exit_application"
			}),
			"yesCallback": async (data, extra) => {
				functions.setVar({
					"var": "action"
				});
				functions.exitApp({});
			},
			"noCallback": ""
		});
		functions.userDefined('globalModalHide', {});
	},

	handleBtnModalNegativeClick: async () => {
		functions.setVar({
			"var": "action"
		});
		functions.toArray({
			"value1": functions.setComponentValue({
				"component": "lblModalTitle",
				"value": "Loading"
			}),
			"value2": functions.hideElement({
				"component": "pnlModalBodyTitle"
			}),
			"value3": functions.setComponentValue({
				"component": "lblModalMessage",
				"value": "Loading ..."
			}),
			"value4": functions.hideElement({
				"component": "pnlModalBodyMessage"
			}),
			"value5": functions.hideElement({
				"component": "pnlModalBodyLoading"
			}),
			"value6": null,
			"value7": functions.hideElement({
				"component": "pnlModalBodyButtonPositive"
			}),
			"value8": null,
			"value9": functions.hideElement({
				"component": "pnlModalBodyButtonNegative"
			}),
			"value10": functions.toArray({
				"value1": functions.setObjectAttr({
					"object": null,
					"attr": "word-wrap",
					"value": "break-word"
				}),
				"value2": functions.setObjectAttr({
					"object": null,
					"attr": "display",
					"value": "none"
				})
			})
		});
	},

	handlePgOrderItemLoad: async () => {
		functions.onBackButton({
			"callback": async (data, extra) => {
				functions.gotoPage({
					"p": "pgMainMenu"
				});
			}
		});
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalLoading', {});
		functions.setTimeout({
			"timeout": 100,
			"extra": "",
			"callback": async (data, extra) => {
				await functions.clearData({
					"callback": "",
					"errCallback": null,
					"dataset": "l_Item_List"
				});
				await functions.loadData({
					"dataset": "OK365_Item_List",
					"limit": 10,
					"filter": functions.toArray({
						"value1": functions.toObject({
							"f": "Location_Filter",
							"v": functions.objectAttr({
								"object": functions.getVarAttr({
									"var": "var_loginCnt",
									"attr": 0,
									"default": ""
								}),
								"attr": "Location_Code"
							})
						}),
						"value2": "",
						"value3": "",
						"value4": "",
						"value5": "",
						"value6": "",
						"value7": "",
						"value8": "",
						"value9": "",
						"value10": ""
					}),
					"order": "",
					"callback": async (data, extra) => {
						functions.setVar({
							"var": "vTotalCount",
							"value": functions.count({
								"values": data
							})
						});
						functions.setVar({
							"var": "vLoopCount",
							"value": 0
						});
						functions.map({
							"values": data,
							"extra": "",
							"callback": async (data, extra) => {
								functions.setVar({
									"var": "vLoopCount",
									"value": functions.add({
										"value1": functions.getVar({
											"var": "vLoopCount",
											"default": ""
										}),
										"value2": 1
									})
								});
								functions.conditional({
									"condition": functions.equal({
										"value1": functions.count({
											"values": await functions.selectBy({
												"by": "No",
												"operator": "=",
												"value": data["No"],
												"callback": null,
												"errCallback": null,
												"dataset": "l_Item_List"
											})
										}),
										"value2": 0
									}),
									"yesValue": "",
									"noValue": "",
									"extra": data,
									"yesCallback": async (data, extra) => {
										await functions.insert({
											"dataset": "l_Item_List",
											"dt": extra,
											"callback": async (data, extra) => {
												functions.conditional({
													"condition": functions.equal({
														"value1": functions.getVar({
															"var": "vTotalCount",
															"default": ""
														}),
														"value2": functions.getVar({
															"var": "vLoopCount",
															"default": ""
														})
													}),
													"yesValue": "",
													"noValue": "",
													"extra": "",
													"yesCallback": async (data, extra) => {
														await functions.loadData({
															"dataset": "l_Item_List",
															"callback": async (data, extra) => {
																functions.userDefined('globalModalHide', {});
															},
															"errCallback": null
														});
													},
													"noCallback": ""
												});
											},
											"errCallback": ""
										});
									},
									"noCallback": async (data, extra) => {
										functions.conditional({
											"condition": functions.equal({
												"value1": functions.getVar({
													"var": "vTotalCount",
													"default": ""
												}),
												"value2": functions.getVar({
													"var": "vLoopCount",
													"default": ""
												})
											}),
											"yesValue": "",
											"noValue": "",
											"extra": "",
											"yesCallback": async (data, extra) => {
												await functions.loadData({});
											},
											"noCallback": ""
										});
									}
								});
							}
						});
						functions.conditional({
							"condition": functions.equal({
								"value2": 0,
								"value1": functions.count({
									"values": data
								})
							}),
							"yesValue": "",
							"noValue": "",
							"extra": "",
							"yesCallback": async (data, extra) => {
								await functions.loadData({});
							},
							"noCallback": ""
						});
					},
					"errCallback": async (data, extra) => {
						functions.setVar({
							"var": "error",
							"value": data["err"]
						});
						functions.userDefined('globalModalInfo', {
							"title": "Error",
							"message": functions.conditional({
								"condition": functions.getVarAttr({
									"var": "error",
									"attr": "global"
								}),
								"yesValue": functions.getVarAttr({
									"var": "error",
									"attr": "global"
								}),
								"noValue": "Cannot connect to Navision Server."
							})
						});
						functions.console({
							"value": data
						});
					}
				});
			}
		});
	}
};

const PgOrderItem = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;
	formatParentStyleLayout(props);

	useEffect(() => {
		actions.handlePgOrderItemLoad();
	}, [currentTime]);
    
    return (
		<Page data={props.pgOrderItem}>
			<Panel data={props.pnlHolder}>
				<Panel data={props.pnlMain}>
					<Panel data={props.pnlMainHeader}>
						<Panel data={props.pnlMainHeaderLeft495}>
							<Panel data={props.pnlMainHeaderLeftTable917}>
								<Panel data={props.pnlMainHeaderLeftCell594}>
									<Image data={props.imgBack65} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderMiddle21}>
							<Panel data={props.pnlMainHeaderMiddleTable107}>
								<Panel data={props.pnlMainHeaderMiddleCell893}>
									<Label data={props.lblMainTitle309} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderRight518} action={actions.handlePnlMainHeaderRight518Click}>
							<Panel data={props.pnlMainHeaderRightTable18} />
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBody}>
						<Panel data={props.pnlMainBody165}>
							<Panel data={props.pnlMainBodySearch290}>
								<Edit data={props.txtSearch} />
							</Panel>
						</Panel>
						<Panel data={props.pnlMainBody2358}>
							<DataList data={props.dlProducts541} functions={functions} currentTime={currentTime}>
								{({ item, parentStyle }) => (							
									<Panel data={item.pnlProduct} parentStyle={parentStyle}>
										<Panel data={item.pnlProductBody} parentStyle={parentStyle}>
											<Panel data={item.pnlProductBodyLeft} parentStyle={parentStyle}>
												<Image data={item.imgItem} parentStyle={parentStyle} />
											</Panel>
											<Panel data={item.pnlProductBodyMiddle} parentStyle={parentStyle}>
												<Panel data={item.pnlProductBodyMiddle1} parentStyle={parentStyle}>
													<Label data={item.lblItemCode} parentStyle={parentStyle} />
													<Label data={item.lblItemName} parentStyle={parentStyle} />
													<Label data={item.lblStockOnHand} parentStyle={parentStyle} />
													<Label data={item.lblCurrentQtyOrder} parentStyle={parentStyle} />
												</Panel>
											</Panel>
											<Panel data={item.pnlProductBodyRight} action={actions.handlePnlProductBodyRightClick} parentStyle={parentStyle}>
												<Panel data={item.pnlProductBodyRight1Cell} parentStyle={parentStyle}>
													<Image data={item.imgAddItem} parentStyle={parentStyle} />
												</Panel>
											</Panel>
										</Panel>
									</Panel>
								)}
							</DataList>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBodyButtonsCheckout}>
						<Panel data={props.Panel186} action={actions.handlePanel186Click}>
							<Label data={props.Label752} />
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlUpdateCart}>
					<Panel data={props.pnlUpdateCartModalTableItemListing}>
						<Panel data={props.pnlUpdateCartModalCellItemListing}>
							<Panel data={props.pnlUpdateCartModalBodyItemListing}>
								<Panel data={props.pnlUpdateCartModalBodyItemListingImage}>
									<Image data={props.imgUpdateCartImage} />
									<Image data={props.imgUpdateCartClose133} action={actions.handleImgUpdateCartClose133Click} />
								</Panel>
								<Panel data={props.pnlUpdateCartModalBodyItemListingCode}>
									<Label data={props.lblUpdateCartItemCodeValueIL} />
									<Label data={props.lblUpdateCartItemNameValueIL} />
									<Label data={props.lblUpdateCartPriceValueIL} />
								</Panel>
								<Panel data={props.Panel524}>
									<Panel data={props.pnlUpdateCartModa}>
										<Panel data={props.pnlUpdateCartModalBodyUOMIL707}>
											<Label data={props.lblUpdateCartQuantityIL386S260} />
										</Panel>
										<Panel data={props.pnlUpdateCartModalBodyUOMValueIL403}>
											<ComboBox data={props.ComboUOMItemList} functions={functions} />
										</Panel>
									</Panel>
									<Panel data={props.pnlUpdateCartModalBodyItemListingQty}>
										<Panel data={props.pnlUpdateCartModalBodyQtyValueIL}>
											<Panel data={props.pnlUpdateCartModalBodyLeft}>
												<Image data={props.imgMinus345} action={actions.handleImgMinus345Click} />
											</Panel>
											<Panel data={props.pnlUpdateCartModalBodyMiddle}>
												<Edit data={props.txtQuantity} action={actions.handleTxtQuantityClick} />
											</Panel>
											<Panel data={props.pnlUpdateCartModalBodyRight}>
												<Image data={props.imgPlus266} action={actions.handleImgPlus266Click} />
											</Panel>
										</Panel>
										<Panel data={props.pnlUpdateCartModalBodyQtyIL}>
											<Label data={props.lblUpdateCartQuantityIL552} />
										</Panel>
									</Panel>
								</Panel>
								<Panel data={props.pnlUpdateCartModalBodyButtonItemListing}>
									<Button data={props.btnUpdate} action={actions.handleBtnUpdateClick} />
									<Panel data={props.pnlFooterNavBtnUpdateTable} action={actions.handlePnlFooterNavBtnUpdateTableClick}>
										<Panel data={props.pnlFooterNavBtnUpdateTableCell}>
											<Label data={props.lblBtnUpdate} />
										</Panel>
									</Panel>
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal}>
					<Panel data={props.pnlModalTable}>
						<Panel data={props.pnlModalCell}>
							<Panel data={props.pnlModalBody}>
								<Panel data={props.pnlModalBodyTitle}>
									<Label data={props.lblModalTitle} />
								</Panel>
								<Panel data={props.pnlModalBodyMessage}>
									<Label data={props.lblModalMessage} />
								</Panel>
								<Panel data={props.pnlModalBodyLoading}>
									<Image data={props.imgModalLoading} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonPositive}>
									<Button data={props.btnModalPositive} action={actions.handleBtnModalPositiveClick} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonNegative}>
									<Button data={props.btnModalNegative} action={actions.handleBtnModalNegativeClick} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlCSS}>
					<Label data={props.lblStyle} />
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgOrderItem);