import React from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgRegisterNAVConnectionInfo';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Label from '../framework/component/Label';
import Edit from '../framework/component/Edit';
import Image from '../framework/component/Image';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handlePnlHeaderAction: () => {
		functions.navCall({
			"connector": "nav",
			"type": "",
			"ent": "OK365_ExportImportPicture",
			"function": "GetItemPicture",
			"subfunction": "",
			"data": functions.toObject({
				"itemNo": "BRKAYA"
			}),
			"callback": (input, extra) => {
				functions.console({
					"value": input
				});
			},
			"errCallback": (input, extra) => {
				functions.console({
					"value": functions.objectAttr({
						"object": functions.objectAttr({
							"object": input,
							"attr": "err"
						}),
						"attr": "global"
					})
				});
			}
		});
	},

	getTxtURLValue: () => {
		return functions.componentAttr({
			"component": "nav",
			"componentId": "",
			"attr": "url"
		});
	},

	getTxtUsernameValue: () => {
		return functions.componentAttr({
			"component": "nav",
			"componentId": "",
			"attr": "user"
		});
	},

	getTxtPasswordValue: () => {
		return functions.componentAttr({
			"component": "nav",
			"componentId": "",
			"attr": "password"
		});
	},

	getTxtCompanyValue: () => {
		return functions.componentAttr({
			"component": "nav",
			"componentId": "",
			"attr": "company"
		});
	},

	handlePnlLoginButtonAction: () => {
		functions.userDefined('globalModalLoading', {});
		functions.setComponentAttr({
			"component": "nav",
			"componentId": "",
			"attr": "url",
			"value": functions.componentElAttr({
				"component": "txtURL",
				"componentId": "",
				"attr": "value"
			})
		});
		functions.setComponentAttr({
			"component": "nav",
			"componentId": "",
			"attr": "user",
			"value": functions.componentElAttr({
				"component": "txtUsername",
				"componentId": "",
				"attr": "value"
			})
		});
		functions.setComponentAttr({
			"component": "nav",
			"componentId": "",
			"attr": "password",
			"value": functions.componentElAttr({
				"component": "txtPassword",
				"attr": "value"
			})
		});
		functions.setComponentAttr({
			"component": "nav",
			"componentId": "",
			"attr": "company",
			"value": functions.componentElAttr({
				"component": "txtCompany",
				"attr": "value"
			})
		});
		functions.setComponentAttr({
			"component": "nav_odata",
			"componentId": "",
			"attr": "url",
			"value": functions.componentElAttr({
				"component": "txtURL",
				"attr": "value"
			})
		});
		functions.setComponentAttr({
			"component": "nav_odata",
			"componentId": "",
			"attr": "user",
			"value": functions.componentElAttr({
				"component": "txtUsername",
				"attr": "value"
			})
		});
		functions.setComponentAttr({
			"component": "nav_odata",
			"componentId": "",
			"attr": "password",
			"value": functions.componentElAttr({
				"component": "txtPassword",
				"attr": "value"
			})
		});
		functions.setComponentAttr({
			"component": "nav_odata",
			"componentId": "",
			"attr": "company",
			"value": functions.componentElAttr({
				"component": "txtCompany",
				"componentId": "",
				"attr": "value"
			})
		});
		functions.loadData({
			"callback": (input, extra) => {
				functions.clearData({
					"dataset": "l_nav_setting",
					"callback": (input, extra) => {
						functions.insert({
							"dataset": "l_nav_setting",
							"dt": functions.toObject({
								"username": functions.componentElAttr({
									"component": "txtUsername",
									"attr": "value"
								}),
								"password": functions.componentElAttr({
									"component": "txtPassword",
									"attr": "value"
								}),
								"company": functions.componentElAttr({
									"component": "txtCompany",
									"attr": "value"
								}),
								"id": 1,
								"url": functions.componentElAttr({
									"component": "txtURL",
									"componentId": "",
									"attr": "value"
								})
							}),
							"callback": (input, extra) => {
								functions.userDefined('globalModalHide', {});
								functions.gotoPage({
									"p": "pgLogin"
								});
							},
							"errCallback": null
						});
					},
					"errCallback": null
				});
			},
			"errCallback": (input, extra) => {
				functions.setVar({
					"var": "error",
					"value": input["err"]
				});
				functions.userDefined('globalModalInfo', {
					"title": "Connection Failed",
					"message": "Please check Service URL and Connection.",
					"btnCaption": "Ok"
				});
			},
			"dataset": "OK365_Mobile_User_Master",
			"limit": 1,
			"filter": functions.toArray({
				"value1": "",
				"value2": "",
				"value3": "",
				"value4": "",
				"value5": "",
				"value6": "",
				"value7": "",
				"value8": "",
				"value9": "",
				"value10": ""
			}),
			"order": ""
		});
	},

	handlePnlModalBodyButtonPositiveAction: () => {
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "vAction",
					"default": ""
				}),
				"value2": "NoCustomer"
			}),
			"yesValue": "",
			"noValue": "",
			"extra": "",
			"yesCallback": (input, extra) => {
				functions.userDefined('globalModalHide', {});
			},
			"noCallback": (input, extra) => {
				functions.conditional({
					"condition": functions.equal({
						"value1": functions.getVar({
							"var": "vAction",
							"default": ""
						}),
						"value2": "logout"
					}),
					"yesValue": "",
					"noValue": "",
					"extra": "",
					"yesCallback": (input, extra) => {
						functions.clearAllVar({});
						functions.setVar({
							"var": "vAction",
							"value": ""
						});
						functions.gotoPage({
							"p": "pgLogin"
						});
					},
					"noCallback": (input, extra) => {
						functions.setVar({
							"var": "vAction",
							"value": ""
						});
						functions.userDefined('globalModalHide', {});
					}
				});
			}
		});
	},

	handlePnlModalBodyButtonNegativeAction: () => {
		functions.setVar({
			"var": "vAction",
			"value": ""
		});
		functions.userDefined('globalModalHide', {});
	},

	handlePgRegisterNAVConnectionInfoLoad: () => {
		functions.selectBy({
			"dataset": "l_settings",
			"by": "id",
			"operator": "",
			"value": 1,
			"first": true,
			"callback": (input, extra) => {
				functions.setVar({
					"var": "v_def_setting",
					"value": input
				});
				functions.conditional({
					"condition": functions.greater({
						"value1": functions.count({
							"values": functions.getVar({
								"var": "v_def_setting",
								"default": ""
							})
						}),
						"value2": 0
					}),
					"yesValue": "",
					"noValue": "",
					"extra": "",
					"yesCallback": (input, extra) => {
						const hello = functions.getVarAttr({
							"var": "v_def_setting",
							"attr": "url"
						})

						functions.setComponentAttr({
							"component": "nav",
							"componentId": "",
							"attr": "url",
							"value": functions.getVarAttr({
								"var": "v_def_setting",
								"attr": "url"
							})
						});
						functions.setComponentAttr({
							"component": "nav",
							"componentId": "",
							"attr": "user",
							"value": functions.getVarAttr({
								"var": "v_def_setting",
								"attr": "username",
								"default": ""
							})
						});
						functions.setComponentAttr({
							"component": "nav",
							"attr": "password",
							"value": functions.getVarAttr({
								"var": "v_def_setting",
								"attr": "password"
							})
						});
						functions.setComponentAttr({
							"component": "nav",
							"componentId": "",
							"attr": "company",
							"value": functions.getVarAttr({
								"var": "v_def_setting",
								"attr": "company",
								"default": ""
							})
						});
						functions.userDefined('globalModalLoading', {});
						functions.loadData({
							"dataset": "OK365_Item_List",
							"limit": 1,
							"filter": functions.toArray({
								"value1": "",
								"value2": "",
								"value3": ""
							}),
							"order": "",
							"callback": (input, extra) => {
								functions.userDefined('globalModalHide', {});
								functions.gotoPage({
									"p": "pgLogin"
								});
							},
							"errCallback": (input, extra) => {
								functions.setVar({
									"var": "error",
									"value": input["err"]
								});
								functions.userDefined('globalModalHide', {});
							}
						});
					},
					"noCallback": null
				});
			},
			"errCallback": null
		});
	}
};

const PgRegisterNAVConnectionInfo = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;

    // Manipulate the props to support css styles
    formatParentStyleLayout(props);

    return (
		<Page data={props.pgRegisterNAVConnectionInfo} load={actions.handlePgRegisterNAVConnectionInfoLoad} currentTime={currentTime}>
			<Panel data={props.pnlWrapper} functions={functions} currentTime={currentTime}>
				<Panel data={props.pnlMain} currentTime={currentTime} functions={functions}>
					<Panel data={props.pnlMainBody} functions={functions} currentTime={currentTime}>
						<Panel data={props.pnlHeader} action={actions.handlePnlHeaderAction} functions={functions} currentTime={currentTime}>
							<Label data={props.lblSubTitle} currentTime={currentTime} functions={functions}/>
						</Panel>
						<Panel data={props.pnlServiceURL} functions={functions} currentTime={currentTime}>
							<Panel data={props.pnServiceUrlCaption} currentTime={currentTime} functions={functions}>
								<Label data={props.lblServiceURL} functions={functions} currentTime={currentTime}/>
							</Panel>
							<Panel data={props.pnlUsernameText} functions={functions} currentTime={currentTime}>
								<Edit data={props.txtURL} functions={functions} currentTime={currentTime}/>
							</Panel>
						</Panel>
						<Panel data={props.pnlUsername} currentTime={currentTime} functions={functions}>
							<Panel data={props.pnlUsernameCaption} functions={functions} currentTime={currentTime}>
								<Label data={props.lblUsername} functions={functions} currentTime={currentTime}/>
							</Panel>
							<Panel data={props.pnlUsernameText934} functions={functions} currentTime={currentTime}>
								<Edit data={props.txtUsername} functions={functions} currentTime={currentTime}/>
							</Panel>
						</Panel>
						<Panel data={props.pnlPassword} functions={functions} currentTime={currentTime}>
							<Panel data={props.pnlPasswordCaption} functions={functions} currentTime={currentTime}>
								<Label data={props.lblPassword} functions={functions} currentTime={currentTime}/>
							</Panel>
							<Panel data={props.pnlUsernameText934766} functions={functions} currentTime={currentTime}>
								<Edit data={props.txtPassword} functions={functions} currentTime={currentTime}/>
							</Panel>
						</Panel>
						<Panel data={props.pnlDefaultCompany} functions={functions} currentTime={currentTime}>
							<Panel data={props.pnlDefaultCompanyCaption} functions={functions} currentTime={currentTime}>
								<Label data={props.lblCompany} functions={functions} currentTime={currentTime}/>
							</Panel>
							<Panel data={props.pnlCompanyText} functions={functions} currentTime={currentTime}>
								<Edit data={props.txtCompany} functions={functions} currentTime={currentTime}/>
							</Panel>
						</Panel>
						<Panel data={props.pnlLogin} functions={functions} currentTime={currentTime}>
							<Panel data={props.pnlLoginButton} action={actions.handlePnlLoginButtonAction} functions={functions} currentTime={currentTime}>
								<Label data={props.lblSave} currentTime={currentTime} functions={functions}/>
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainFooterMsg} currentTime={currentTime} functions={functions}>
						<Label data={props.Label123} functions={functions} currentTime={currentTime}/>
						<Label data={props.Label145} functions={functions} currentTime={currentTime}/>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal} currentTime={currentTime} functions={functions}>
					<Panel data={props.pnlModalBodyMain} functions={functions} currentTime={currentTime}>
						<Panel data={props.pnlModalBodyTitle} functions={functions} currentTime={currentTime}>
							<Label data={props.lblModalTitle} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlModalBodyMessage} currentTime={currentTime} functions={functions}>
							<Label data={props.lblModalMessage} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlModalBodyLoading} functions={functions} currentTime={currentTime}>
							<Image data={props.imgModalLoading} functions={functions} currentTime={currentTime}/>
						</Panel>
					</Panel>
					<Panel data={props.pnlModalBodyButtons} functions={functions} currentTime={currentTime}>
						<Panel data={props.pnlModalBodyButtonPositive} action={actions.handlePnlModalBodyButtonPositiveAction} functions={functions} currentTime={currentTime}>
							<Label data={props.lblModalPositive} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlModalBodyButtonNegative} action={actions.handlePnlModalBodyButtonNegativeAction} functions={functions} currentTime={currentTime}>
							<Label data={props.lblModalNegative} functions={functions} currentTime={currentTime}/>
						</Panel>
					</Panel>
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgRegisterNAVConnectionInfo);