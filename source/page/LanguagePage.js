import React from 'react';
import { connect } from 'react-redux';

import Page from '../framework/component/Page';
import Label from '../framework/component/Label';
import Button from '../framework/component/Button';

import Functions from '../framework/core/Function';

import { StateName } from '../reducer/data/LanguagePage';

import { globalStoreData } from '../framework/plugin/AsyncStorage';

let functions = new Functions(StateName);

const LanguagePage = (props) => {
    // Append the dispatch to the function instance
    functions.pageProps = props;

    let handleClickOne = () => {
        functions.setComponentValue({
            component: 'lblFirst', 
            value: 'Ok'
        });
    }

    let handleClickTwo = () => {
        functions.setComponentValue({
            component: 'lblFirst', 
            value: functions.getLanguage()
        });
    }

    let handleClickThree = () => {
        functions.console({
            value: functions.setLanguage({
                lang: 'Chinese'
            })
        });
    }

    let handleClickFour = () => {
        functions.setComponentValue({
            component: 'lblFirst', 
            value: functions.languageConvertData({
                value: 'Ok', 
                language: 'Chinese'
            })
        });
    }

    let handleClickFive = () => {
        functions.console({
            value: functions.getLanguageList({ 
                callback: function(data) {
                    functions.console({
                        value: data
                    });
                } 
            })
        });
    }

    let handleClickSave = () => {
        functions.setVar({
            var: 'vdata',
            value: functions.toArray({
                value1: functions.toObject({
                    user: 'abc',
                    name: 'abcd'
                }),
                value2: functions.toObject({
                    user: 'def',
                    name: 'defg'
                }),
                value3: functions.toObject({
                    user: 'hij',
                    name: 'hijk'
                })
            })
        });
        functions.console({
            value: functions.getVar({
                var: 'vdata'
            })
        });
        functions.map({
            values: functions.getVar({
                var: 'vdata'
            }),
            extra: null,
            callback: (input, extra) => {
                functions.insert({
                    dataset: 'l_users',
                    dt: input,
                    extra: '',
                    callback: (input, extra) => {
                        functions.console({
                            value: input
                        });
                        functions.console({
                            value: 'Successful insert'
                        });
                    },
                    errCallback: (input, extra) => {
                        functions.console({
                            value: input 
                        });
                    }
                });
            }
        });
    };

    let handleClickClear = () => {
        functions.clearData({
            dataset: 'l_users',
            extra: '',
            callback: (input, extra) => {
                functions.console({
                    value: 'Data cleared!'
                });
            },
            errCallback: (input, extra) => {
                functions.console({
                    value: input 
                });
            }
        });
        functions.console({
            value: globalStoreData
        });
    };

    let handleLoad = () => {
        functions.console({
            value: functions.concat({
                string1: functions.deviceManufacturer(),
                string2: ' ',
                string3: functions.deviceSerial()
            })
        });
        functions.console({
            value: globalStoreData
        });
    };

    return (
        <Page load={handleLoad}>
            <Label data={props.lblFirst}/>
            <Button data={props.btnChangeCaption} action={handleClickOne}/>
            <Button data={props.btnGetLanguage} action={handleClickTwo}/>
            <Button data={props.btnSetLanguage} action={handleClickThree}/>
            <Button data={props.btnLangaugeConvertData} action={handleClickFour}/>
            <Button data={props.btnGetLanguageList} action={handleClickFive}/>
            <Button data={props.btnSave} action={handleClickSave} />
            <Button data={props.btnClear} action={handleClickClear} />
        </Page>
    )

}

// Bind the state to the props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
}

export default connect(mapStateToProps)(LanguagePage);