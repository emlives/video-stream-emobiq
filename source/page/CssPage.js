import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux'; 
import Page from '../framework/component/Page';
import Label from '../framework/component/Label';
import Panel from '../framework/component/Panel';
import Button from '../framework/component/Button';
import Edit from '../framework/component/Edit';
import ComboBox from '../framework/component/ComboBox';
import DataList from '../framework/component/DataList';
import Image from '../framework/component/Image';
import Signature from '../framework/component/Signature';
import Memo from '../framework/component/Memo';
import CheckBox from '../framework/component/CheckBox';
import SnippetWrapper from '../wrapper/component/SnippetWrapper';

import { References } from '../dependency/global/Variable';
import Functions from '../framework/core/Function';

import { StateName } from '../reducer/data/CssPage';
import Service from '../dependency/global/Service'
import { useNavigateResponder } from '../framework/hook/NavigateResponder';
import ExpoAsyncStorage from '@react-native-async-storage/async-storage';
import EmobiqAssets from '../dependency/global/Asset'

let functions = new Functions(StateName);

const template = {
    DataList467: ({ item, parentStyle, currentTime }) => (							
		<View>
            <Label data={item.Label439} parentStyle={parentStyle} functions={functions} currentTime={currentTime}/>
        </View>
	)
}

const actions = {
	handleClick: async () => {
        functions.setComponentValue({
            "component": "Image387",
            "value": "icon/_add.png",
        })
        functions.setComponentElAttr({component:"Panel583", attr:"style", value:"color:black"});
        // functions.setLanguage({
		// 	"lang": "chinese"
		// });
        // for (i = 0; i < dataArray.length; i++) {
        //     let value = await ExpoAsyncStorage.getItem("local_table_l_data_list");
        //     // Process the data base on the parameters
        //     let finalData = value != null ? JSON.parse(value) : [];
        //     // Append the data to the finalData
        //     finalData = finalData.concat(dataArray[i]);
        //     console.log("finaldata")
        //     console.log(finalData)
        //     await ExpoAsyncStorage.setItem("local_table_l_data_list", JSON.stringify(finalData));
        //     console.log("setItemDone")
        // }
        // functions.map({
        //     "values": [{
        //         'key': 'name' + Math.random(),
        //         'value': 'Kent ' + Math.random()
        //     },
        //     {
        //         'key': 'name' + Math.random(),
        //         'value': 'Alvin ' + Math.random()
        //     }],
        //     "callback": async (data, extra) => {
        //         // let value = await ExpoAsyncStorage.getItem("local_table_l_data_list");
        //         // // Process the data base on the parameters
        //         // let finalData = value != null ? JSON.parse(value) : [];
        //         // // Append the data to the finalData
        //         // finalData = finalData.concat(data);
        //         // console.log("finaldata")
        //         // console.log(finalData)
        //         // await ExpoAsyncStorage.setItem("local_table_l_data_list", JSON.stringify(finalData));
        //         // console.log("setItemDone")
        //         await functions.insert(
        //             {
        //                 dataset: 'l_data_list',
        //                 dt: data,
        //                 extra: 'Extra data here...',
        //                 callback: function (input, extra) {
        //                     functions.console({ value: 'Success:' });
        //                     functions.console({ value: input });
        //                     functions.console({ value: extra });
        //                 },
        //                 errCallback: function (error, extra) {
        //                     functions.console({ value: 'Fail:' });
        //                     functions.console({ value: error });
        //                     functions.console({ value: extra });
        //                 }
        //             }
        //         )
        //     }
        // })
        
        // functions.map({
        //     "values": [{
        //         "label": "Joko",
        //         "value": 25
        //     },
        //     {
        //         "label": "Kent",
        //         "value": 24
        //     }],
        //     "callback": async (data, extra) => {
        //         functions.setComponentValue({
        //             "component": "ComboBox520",
        //             "value": data,
        //         })
        //     }
        // })
        
        // [{
        //     "label": "Joko",
        //     "value": 25
        // },
        // {
        //     "label": "Kent",
        //     "value": 24
        // }].map(
        //     (val) => {
        //         functions.setComponentValue({
        //             "component": "ComboBox520",
        //             "value": val,
        //         })
        //     }
        // );
		// functions.setComboOptions({combo:"ComboBox520",
        //     data:[
        //         {
        //             "name": "Joko",
        //             "gender": "male",
        //             "age": 25
        //         },
        //         {
        //             "name": "Kent",
        //             "gender": "male",
        //             "age": 24
        //         },
        //     ],
        //     valueField:"age",
        //     valueSeparator:"",
        //     displayField:"name",
        //     displaySeparator:""
        // })
        // functions.setComponentValue({
		// 	"component": "Checkbox505",
		// 	"value": false
        // })
        //functions.console({value: functions.setComponentAttr({component:"nav_odata", attr:"connector", value:"nav_odata123"})});
        // functions.console({
        //     value: await functions.insert(
        //         {
        //             dataset: 'l_data_list',
        //             dt: {
        //                 'key': 'name' + Math.random(),
        //                 'value': 'Alvin ' + Math.random()
        //             },
        //             extra: 'Extra data here...',
        //             callback: function (input, extra) {
        //                 functions.console({ value: 'Success:' });
        //                 functions.console({ value: input });
        //                 functions.console({ value: extra });
        //             },
        //             errCallback: function (error, extra) {
        //                 functions.console({ value: 'Fail:' });
        //                 functions.console({ value: error });
        //                 functions.console({ value: extra });
        //             }
        //         }
        //     )
        // });
	},

    handleClick2: async() => {
        functions.selectAll(
            {
                dataset: 'l_data_list',
                extra: 'Extra data here...',
                callback: function (input, extra) {
                    functions.console({ value: 'Success:' });
                    functions.console({ value: input });
                    functions.console({ value: extra });
                },
                errCallback: function (error, extra) {
                    functions.console({ value: 'Fail:' });
                    functions.console({ value: error });
                    functions.console({ value: extra });
                }
            }
        )
        // functions.console({value:functions.componentAttr({component:"OK365_Customer_List",attr:"connector"})});
        // functions.console({value:functions.componentValue({component:"Image387",attr:"caption"})});
        // functions.console({value:functions.getComponent({component:"Label579"})});
        // functions.console({value:functions.getComponent({component:"n_rc1"})});
        functions.console({value:functions.getComponent({component:"Label579"})});
        functions.console({value:functions.getComponent({component:"n_rc1"})});
        functions.setComponentValue({
			"component": "Checkbox505",
			"value": true
        })
        // functions.setComponentElAttr({component:"ComboBox520", attr:"componentValue", value:24});
        // functions.setComponentAttr({component:"Image387", attr:"url", value:"https://i.imgur.com/KXpxBPe.jpg"});
        // functions.console({value:functions.componentElAttr({component:"btnSecond1", attr:"caption", value:"bla"})});
        // functions.setComponentValue({
		// 	"component": "Label579",
		// 	"value": "kent"
        
        
        // })
    },
    
    handleClick3: async() => {
        functions.console({value: 'handleClick3 invoked'})
    },

	handleCssPageLoad: async () => {
		// functions.console( {value: await functions.loadData(
        //     {
            
            
        //         dataset: "l_data_list",
        //         // order: [
        //         //     {
        //         //         "field": "value",
        //         //         "order": "asc"
        //         //     }
        //         // ],
        //         limit: 2,
        //         // page: 1,
        //         filter: [
        //             // {
        //             //     "field": "value",
        //             //     "o": "=",
        //             //     "value": "kevin"
        //             // }
        //         ],
        //         callback: (data) => {
        //             functions.console({ value: data });
        //         }
        //     }
        // )} )
	}
};

const CssPage = (props) => {
    // For datalist autoload handling when to render
    const currentTime = useNavigateResponder(props.navigation);
    useEffect(() => {
		actions.handleCssPageLoad();
	}, [currentTime]);
    
    // Append the dispatch to the function instance
    functions.pageProps = props;
    


    return (
        // <Provider value={{testContext: "test"}}>
        // <Consumer>
        // { value => {
        //     console.log("Provider value")
        //     console.log(value)
        // return (
        <Page data={props.page} allProps={props}>
            <Label data={props.Label579} parentData={props.page} functions={functions}></Label>
            <CheckBox functions={functions} data={props.Checkbox505}></CheckBox>
            <Button action={actions.handleClick} data={props.Button682} functions={functions}></Button>
            <Button action={actions.handleClick2} data={props.Button854} functions={functions}></Button>
            <Edit data={props.Edit219}// Inherit any props passed to it; e.g., multiline, numberOfLines below
                editable
                maxLength={40}
                functions={functions}
                actionChange={actions.handleClick3}
                functions={functions}
            />
            <ComboBox
                functions={functions}
                data={props.ComboBox520}
            />
            <DataList data={props.DataList467} functions={functions} currentTime = {currentTime} template={template.DataList467}/>

            <Panel data={props.Panel518} functions={functions}>
                <Image data={props.Image387} functions={functions}/>
                <Signature data={props.Signature551} inputRef={el => References[StateName].Signature551 = el} functions={functions}></Signature>
                <Memo data={props.Memo768} onChangeText={(text) => functions.setComponentValue({component:"Memo768",value:text})} functions={functions}></Memo>
            </Panel>
            <Panel data={props.Panel583} functions={functions}>
                <Label data={props.Label870} functions={functions}></Label>
                <Label data={props.Label871} functions={functions}></Label>
                <Label data={props.Label872} functions={functions}></Label>
            </Panel>
            <Label data={props.Label403} functions={functions}></Label>
            <Button action={actions.handleClick2} data={props.Button774} functions={functions}></Button>
            <SnippetWrapper data={props.snippetA} functions={functions}></SnippetWrapper>
        </Page>
        // )
        // }}
        // </Consumer>
        // </Provider>
    )
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] }
};

export default connect(mapStateToProps)(CssPage);