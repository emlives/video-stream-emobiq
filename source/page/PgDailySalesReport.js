import React, { useEffect }  from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgDailySalesReport';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import Edit from '../framework/component/Edit';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handleImgMenuClick: async () => {
		functions.gotoPage({
			"p": "pgMainMenu"
		});
	},

	handlePnlMainBodyDateSearchTblCell2Click: async () => {
		functions.userDefined('globalModalLoading', {});
		await functions.navCall({
			"ent": "OK365_GetItemSalesPrice",
			"function": "UpdateDailySalesReportDateFilter",
			"data": functions.toObject({
				"dateFilter": null
			}),
			"callback": async (data, extra) => {
				await functions.loadData({
					"dataset": "OK365_Daily_Sales_Report",
					"filter": functions.toArray({
						"value1": functions.toObject({
							"f": "Route_Code",
							"v": functions.getVar({
								"var": "v_current_route_code"
							})
						}),
						"value2": functions.toObject({
							"f": "Date_Filter",
							"v": null
						})
					}),
					"callback": async (data, extra) => {
						functions.console({
							"value": data
						});
						functions.console({
							"value": "LaodnOK365_Daily_Sales_Report"
						});
						functions.setVar({
							"var": "v_daily_sales_report",
							"value": functions.objectAttr({
								"object": data,
								"attr": 0
							})
						});
						functions.toArray({
							"value1": functions.setComponentValue({
								"component": "lblTodayCashSales_data",
								"value": functions.concat({
									"string1": "$ ",
									"string2": functions.formatNumber({
										"value": functions.getVarAttr({
											"var": "v_daily_sales_report",
											"attr": "Cash_Sales_Amount"
										}),
										"decimals": 2,
										"decimalSep": ".",
										"thousandSep": ","
									})
								})
							}),
							"value2": functions.setComponentValue({
								"component": "lblTodayCreditSales_data",
								"value": functions.concat({
									"string1": "$ ",
									"string2": functions.formatNumber({
										"decimalSep": ".",
										"thousandSep": ",",
										"value": functions.getVarAttr({
											"var": "v_daily_sales_report",
											"attr": "Credit_Sales_Amount"
										}),
										"decimals": 2
									})
								})
							}),
							"value3": functions.setComponentValue({
								"component": "lblTotalSalesToDay_data",
								"value": functions.concat({
									"string1": "$ ",
									"string2": functions.formatNumber({
										"value": functions.add({
											"value1": functions.getVarAttr({
												"var": "v_daily_sales_report",
												"attr": "Cash_Sales_Amount"
											}),
											"value2": functions.getVarAttr({
												"var": "v_daily_sales_report",
												"attr": "Credit_Sales_Amount"
											})
										}),
										"decimals": 2,
										"decimalSep": ".",
										"thousandSep": ","
									})
								})
							}),
							"value4": functions.setComponentValue({
								"component": "lblCashCollectionForToday_data",
								"value": functions.concat({
									"string1": "$ ",
									"string2": functions.formatNumber({
										"value": functions.getVarAttr({
											"var": "v_daily_sales_report",
											"attr": "Cash_Collection_Amount"
										}),
										"decimals": 2,
										"decimalSep": ".",
										"thousandSep": ","
									})
								})
							}),
							"value5": functions.setComponentValue({
								"component": "lblChequeCollectionForToday_data",
								"value": functions.concat({
									"string1": "$ ",
									"string2": functions.formatNumber({
										"value": functions.getVarAttr({
											"var": "v_daily_sales_report",
											"attr": "Cheque_Collection_Amount"
										}),
										"decimals": 2,
										"decimalSep": ".",
										"thousandSep": ","
									})
								})
							})
						});
						functions.userDefined('globalModalHide', {});
					},
					"errCallback": async (data, extra) => {
						functions.toArray({
							"value1": functions.setVar({
								"var": "vAction"
							}),
							"value2": functions.userDefined('globalModalHide', {}),
							"value3": functions.userDefined('globalModalInfo', {
								"title": "Internal Navision Server Error",
								"message": functions.objectAttr({
									"attr": "global",
									"object": functions.objectAttr({
										"object": data,
										"attr": "err"
									})
								}),
								"btnCaption": "OK"
							})
						});
					}
				});
			},
			"errCallback": async (data, extra) => {
				functions.toArray({
					"value1": functions.setVar({
						"var": "vAction"
					}),
					"value2": functions.userDefined('globalModalHide', {}),
					"value3": functions.userDefined('globalModalInfo', {
						"title": "Internal Navision Server Error",
						"message": functions.objectAttr({
							"object": functions.objectAttr({
								"object": data,
								"attr": "err"
							}),
							"attr": "global"
						}),
						"btnCaption": "OK"
					})
				});
			},
			"connector": "nav"
		});
	},

	handlePnlModalBodyButtonPositiveClick: async () => {
		functions.conditional({
			"condition": functions.equal({
				"value2": "new_return_order",
				"value1": functions.getVar({
					"var": "vAction"
				})
			}),
			"yesCallback": async (data, extra) => {
				functions.userDefined('globalModalLoading', {});
				functions.setVar({
					"var": "vAction"
				});
				await functions.navCall({
					"connector": "nav",
					"ent": "OK365_Transfer_Orders",
					"function": "Create",
					"data": functions.toObject({
						"Transfer_from_Code": functions.objectAttr({
							"object": await functions.selectBy({
								"dataset": "l_Route_Master",
								"by": "_id",
								"value": 1,
								"first": true,
								"callback": null,
								"errCallback": null
							}),
							"attr": "Location_Code"
						}),
						"Transfer_to_Code": "MAIN",
						"Posting_Date": functions.dbDate({})
					}),
					"callback": async (data, extra) => {
						functions.console({
							"value": data
						});
						functions.setVar({
							"var": "v_transfer_order_selected",
							"value": functions.objectAttr({
								"object": data,
								"attr": "OK365_Transfer_Orders"
							})
						});
						await functions.loadData({
							"dataset": "OK365_Transfer_Orders",
							"filter": functions.toArray({
								"value1": functions.toObject({
									"f": "No",
									"v": functions.getVarAttr({
										"var": "v_transfer_order_selected",
										"attr": "No"
									})
								})
							}),
							"callback": async (data, extra) => {
								functions.setVar({
									"var": "v_transfer_order_data",
									"value": functions.objectAttr({
										"object": data,
										"attr": 0
									})
								});
								functions.setVar({
									"var": "v_transfer_order_lines_data",
									"value": functions.objectAttr({
										"object": functions.objectAttr({
											"object": functions.objectAttr({
												"object": data,
												"attr": 0
											}),
											"attr": "TransferLines"
										}),
										"attr": "OK365_Transfer_Line_Line"
									})
								});
								await functions.clearData({
									"dataset": "l_TransferLines",
									"callback": null,
									"errCallback": null
								});
								functions.gotoPage({
									"p": "pgTransferStockDetails"
								});
							},
							"errCallback": async (data, extra) => {
								functions.userDefined('globalModalHide', {});
								functions.userDefined('globalModalInfo', {
									"btnCaption": "OK",
									"title": "Navision Server Error",
									"message": functions.objectAttr({
										"object": functions.objectAttr({
											"object": data,
											"attr": "err"
										}),
										"attr": "global"
									})
								});
							}
						});
					},
					"errCallback": async (data, extra) => {
						functions.userDefined('globalModalHide', {});
						functions.userDefined('globalModalInfo', {
							"title": "Navision Server Error",
							"message": functions.objectAttr({
								"object": functions.objectAttr({
									"object": data,
									"attr": "err"
								}),
								"attr": "global"
							}),
							"btnCaption": "OK"
						});
					}
				});
			},
			"noCallback": async (data, extra) => {
				functions.setVar({
					"var": "vAction"
				});
				functions.userDefined('globalModalHide', {});
			}
		});
	},

	handlePnlModalBodyButtonNegativeClick: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalHide', {});
	},

	handlePgDailySalesReportLoad: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.onBackButton({
			"callback": async (data, extra) => {
				functions.gotoPage({
					"p": "pgMainMenu"
				});
			}
		});
		functions.setComponentValue({
			"component": "edtDateSearch",
			"value": functions.dbDate({})
		});
		functions.userDefined('globalModalLoading', {});
		functions.setTimeout({
			"callback": async (data, extra) => {
				await functions.navCall({
					"connector": "nav",
					"ent": "OK365_GetItemSalesPrice",
					"function": "UpdateDailySalesReportDateFilter",
					"data": functions.toObject({
						"dateFilter": null
					}),
					"callback": async (data, extra) => {
						await functions.loadData({
							"callback": async (data, extra) => {
								functions.console({
									"value": data
								});
								functions.console({
									"value": "LaodnOK365_Daily_Sales_Report"
								});
								functions.setVar({
									"var": "v_daily_sales_report",
									"value": functions.objectAttr({
										"object": data,
										"attr": 0
									})
								});
								functions.toArray({
									"value1": functions.setComponentValue({
										"component": "lblTodayCashSales_data",
										"value": functions.concat({
											"string1": "$ ",
											"string2": functions.formatNumber({
												"thousandSep": ",",
												"value": functions.getVarAttr({
													"var": "v_daily_sales_report",
													"attr": "Cash_Sales_Amount"
												}),
												"decimals": 2,
												"decimalSep": "."
											})
										})
									}),
									"value2": functions.setComponentValue({
										"component": "lblTodayCreditSales_data",
										"value": functions.concat({
											"string1": "$ ",
											"string2": functions.formatNumber({
												"value": functions.getVarAttr({
													"var": "v_daily_sales_report",
													"attr": "Credit_Sales_Amount"
												}),
												"decimals": 2,
												"decimalSep": ".",
												"thousandSep": ","
											})
										})
									}),
									"value3": functions.setComponentValue({
										"component": "lblTotalSalesToDay_data",
										"value": functions.concat({
											"string1": "$ ",
											"string2": functions.formatNumber({
												"value": functions.add({
													"value1": functions.getVarAttr({
														"var": "v_daily_sales_report",
														"attr": "Cash_Sales_Amount"
													}),
													"value2": functions.getVarAttr({
														"var": "v_daily_sales_report",
														"attr": "Credit_Sales_Amount"
													})
												}),
												"decimals": 2,
												"decimalSep": ".",
												"thousandSep": ","
											})
										})
									}),
									"value4": functions.setComponentValue({
										"component": "lblCashCollectionForToday_data",
										"value": functions.concat({
											"string1": "$ ",
											"string2": functions.formatNumber({
												"value": functions.getVarAttr({
													"var": "v_daily_sales_report",
													"attr": "Cash_Collection_Amount"
												}),
												"decimals": 2,
												"decimalSep": ".",
												"thousandSep": ","
											})
										})
									}),
									"value5": functions.setComponentValue({
										"component": "lblChequeCollectionForToday_data",
										"value": functions.concat({
											"string1": "$ ",
											"string2": functions.formatNumber({
												"value": functions.getVarAttr({
													"var": "v_daily_sales_report",
													"attr": "Cheque_Collection_Amount"
												}),
												"decimals": 2,
												"decimalSep": ".",
												"thousandSep": ","
											})
										})
									})
								});
								functions.userDefined('globalModalHide', {});
							},
							"errCallback": async (data, extra) => {
								functions.toArray({
									"value1": functions.setVar({
										"var": "vAction"
									}),
									"value2": functions.userDefined('globalModalHide', {}),
									"value3": functions.userDefined('globalModalInfo', {
										"title": "Internal Navision Server Error",
										"message": functions.objectAttr({
											"object": functions.objectAttr({
												"object": data,
												"attr": "err"
											}),
											"attr": "global"
										}),
										"btnCaption": "OK"
									})
								});
							},
							"dataset": "OK365_Daily_Sales_Report",
							"filter": functions.toArray({
								"value1": functions.toObject({
									"f": "Route_Code",
									"v": functions.getVar({
										"var": "v_current_route_code"
									})
								}),
								"value2": functions.toObject({
									"f": "Date_Filter",
									"v": null
								})
							})
						});
					},
					"errCallback": async (data, extra) => {
						functions.toArray({
							"value1": functions.setVar({
								"var": "vAction"
							}),
							"value2": functions.userDefined('globalModalHide', {}),
							"value3": functions.userDefined('globalModalInfo', {
								"title": "Internal Navision Server Error",
								"message": functions.objectAttr({
									"object": functions.objectAttr({
										"object": data,
										"attr": "err"
									}),
									"attr": "global"
								}),
								"btnCaption": "OK"
							})
						});
					}
				});
			},
			"timeout": 100
		});
	}
};

const PgDailySalesReport = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;
	formatParentStyleLayout(props);

	useEffect(() => {
		actions.handlePgDailySalesReportLoad();
	}, [currentTime]);
    
    return (
		<Page data={props.pgDailySalesReport}>
			<Panel data={props.pnlHolder}>
				<Panel data={props.pnlMain}>
					<Panel data={props.pnlMainHeader}>
						<Panel data={props.pnlMainHeaderLeft}>
							<Panel data={props.pnlMainHeaderLeftTbl}>
								<Panel data={props.pnlMainHeaderLeftTblCell}>
									<Image data={props.imgMenu} action={actions.handleImgMenuClick} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderMiddle}>
							<Panel data={props.pnlMainHeaderMiddleTbl}>
								<Panel data={props.pnlMainHeaderMiddleTblCell}>
									<Label data={props.lblMainTitle} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderRight}>
							<Panel data={props.pnlMainHeaderRightTbl}>
								<Panel data={props.pnlMainHeaderRightTblCell}>
									<Label data={props.lblReturn} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainSearch}>
						<Panel data={props.pnlMainBodyDateSearchTbl}>
							<Panel data={props.pnlMainBodyDateSearchTblCell1}>
								<Edit data={props.edtDateSearch} />
							</Panel>
							<Panel data={props.pnlMainBodyDateSearchTblCell2} action={actions.handlePnlMainBodyDateSearchTblCell2Click}>
								<Image data={props.imgSearch} />
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBody}>
						<Panel data={props.pnlBody}>
							<Panel data={props.pnlBodyContent}>
								<Panel data={props.pnlMainSalesSummary}>
									<Panel data={props.pnlSalesSummary}>
										<Label data={props.lblSalesSummary} />
									</Panel>
									<Panel data={props.pnlSalesSummaryDetails}>
										<Panel data={props.pnlTodayCashSales}>
											<Label data={props.lblTodayCashSales} />
											<Label data={props.lblTodayCashSales_colon} />
											<Label data={props.lblTodayCashSales_data} />
										</Panel>
										<Panel data={props.pnlTodayCreditSales}>
											<Label data={props.lblTodayCreditSales} />
											<Label data={props.lblTodayCreditSales_colon} />
											<Label data={props.lblTodayCreditSales_data} />
										</Panel>
										<Panel data={props.pnlTotalSalesToDay}>
											<Label data={props.lblTotalSalesToDay} />
											<Label data={props.lblTotalSalesToDay_colon} />
											<Label data={props.lblTotalSalesToDay_data} />
										</Panel>
									</Panel>
								</Panel>
								<Panel data={props.pnlMainCollectionSummary}>
									<Panel data={props.pnlCollectionSummary}>
										<Label data={props.lblCollectionSummary} />
									</Panel>
									<Panel data={props.pnlCollectionSummaryDetails}>
										<Panel data={props.pnlCashCollectionForToday}>
											<Label data={props.lblCashCollectionForToday} />
											<Label data={props.lblCashCollectionForToday_colon} />
											<Label data={props.lblCashCollectionForToday_data} />
										</Panel>
										<Panel data={props.pnlChequeCollectionForToday}>
											<Label data={props.lblChequeCollectionForToday} />
											<Label data={props.lblChequeCollectionForToday_colon} />
											<Label data={props.lblChequeCollectionForToday_data} />
										</Panel>
									</Panel>
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal}>
					<Panel data={props.pnlModalTable}>
						<Panel data={props.pnlModalCell}>
							<Panel data={props.pnlModalBodyMain}>
								<Panel data={props.pnlModalBodyTitle}>
									<Label data={props.lblModalTitle} />
								</Panel>
								<Panel data={props.pnlModalBodyMessage}>
									<Label data={props.lblModalMessage} />
								</Panel>
								<Panel data={props.pnlModalBodyLoading}>
									<Image data={props.imgModalLoading} />
								</Panel>
							</Panel>
							<Panel data={props.pnlModalBodyButtons}>
								<Panel data={props.pnlModalBodyButtonPositive} action={actions.handlePnlModalBodyButtonPositiveClick}>
									<Label data={props.lblModalPositive} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonNegative} action={actions.handlePnlModalBodyButtonNegativeClick}>
									<Label data={props.lblModalNegative} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlCSS}>
					<Label data={props.lblStyle} />
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgDailySalesReport);