import React, { useEffect }  from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgOrderHistory';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import Edit from '../framework/component/Edit';
import DataList from '../framework/component/DataList';
import ComboBox from '../framework/component/ComboBox';
import Button from '../framework/component/Button';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handleImgBackClick: async () => {
		functions.gotoPage({
			"p": "pgMainMenu"
		});
	},

	handleLblMainTitleClick: async () => {
		functions.setComponentValue({
			"component": "cmbCustomer",
			"value": functions.concat({
				"string1": functions.getVarAttr({
					"var": "vCustomer_selected",
					"attr": "No"
				}),
				"string2": "-",
				"string3": functions.getVarAttr({
					"var": "vCustomer_selected",
					"attr": "Name"
				})
			})
		});
	},

	handleImgSearchFiltersClick: async () => {
		functions.userDefined('globalModalLoading', {});
		functions.setComponentValue({
			"component": "dpDateFrom",
			"value": functions.dbDate({})
		});
		functions.setComponentValue({
			"component": "dpDateTo",
			"value": functions.dbDate({
				"add": 1
			})
		});
		await functions.loadData({
			"order": functions.toArray({
				"value1": functions.toObject({
					"f": "Name",
					"v": "asc"
				})
			}),
			"callback": async (data, extra) => {
				functions.console({
					"value": data
				});
				await functions.dataFromString({
					"dataset": "l_customer",
					"string": data,
					"callback": "",
					"errCallback": ""
				});
				await functions.loadData({
					"dataset": "l_customer",
					"callback": "",
					"errCallback": ""
				});
				functions.newArray({
					"var": "arrCustomerList"
				});
				functions.push({
					"var": "arrCustomerList",
					"value": functions.toObject({
						"Code": "All",
						"No": "All"
					})
				});
				functions.map({
					"values": await functions.selectAll({
						"dataset": "l_customer",
						"callback": "",
						"errCallback": null
					}),
					"callback": async (data, extra) => {
						functions.setVar({
							"var": "mapCustomerListData",
							"value": data
						});
						functions.push({
							"var": "arrCustomerList",
							"value": functions.toObject({
								"Code": functions.conditional({
									"condition": functions.concat({
										"string1": functions.getVarAttr({
											"var": "mapCustomerListData",
											"attr": "No"
										}),
										"string2": "-",
										"string3": functions.getVarAttr({
											"var": "mapCustomerListData",
											"attr": "Name"
										})
									}),
									"yesValue": functions.concat({
										"string1": functions.getVarAttr({
											"var": "mapCustomerListData",
											"attr": "No"
										}),
										"string2": "-",
										"string3": functions.getVarAttr({
											"var": "mapCustomerListData",
											"attr": "Name"
										})
									}),
									"noValue": functions.concat({
										"string1": functions.getVarAttr({
											"var": "vCustomer_selected",
											"attr": "No"
										}),
										"string2": "-",
										"string3": functions.getVarAttr({
											"var": "vCustomer_selected",
											"attr": "Name"
										})
									}),
									"yesCallback": "",
									"noCallback": ""
								}),
								"No": functions.getVarAttr({
									"var": "mapCustomerListData",
									"attr": "No"
								})
							})
						});
					}
				});
				functions.setComboOptions({
					"combo": "cmbCustomer",
					"data": functions.getVar({
						"var": "arrCustomerList"
					}),
					"valueField": "No",
					"displayField": "Code"
				});
				functions.showElement({
					"component": "pnlModalFilter"
				});
				functions.userDefined('globalModalHide', {});
			},
			"errCallback": async (data, extra) => {
				functions.setVar({
					"var": "error",
					"value": data["err"]
				});
				functions.userDefined('globalModalInfo', {
					"message": functions.conditional({
						"condition": functions.getVarAttr({
							"var": "error",
							"attr": "global"
						}),
						"yesValue": functions.getVarAttr({
							"var": "error",
							"attr": "global"
						}),
						"noValue": "Cannot connect to Navision Server."
					}),
					"title": "Error"
				});
			},
			"dataset": "OK365_Customer_List",
			"filter": ""
		});
	},

	handleImgUpdateCartCloseClick: async () => {
		functions.setComponentValue({
			"component": "txtQuantity",
			"value": ""
		});
		functions.hideElement({
			"component": "pnlUpdateCartModal"
		});
	},

	handleLblSelectCustomerClick: async () => {
		functions.console({
			"value": functions.greater({
				"value1": functions.count({
					"values": await functions.selectAll({
						"dataset": "l_customer",
						"callback": null,
						"errCallback": null
					})
				}),
				"value2": 0
			})
		});
		functions.setComponentValue({
			"component": "lblAll",
			"value": "All"
		});
		functions.conditional({
			"condition": "",
			"yesCallback": async (data, extra) => {
				functions.userDefined('globalModalLoading', {});
				await functions.loadData({
					"errCallback": "",
					"dataset": "l_customer",
					"filter": functions.toArray({
						"value1": functions.toObject({
							"f": "CardCode",
							"o": "!=",
							"v": "[object Object]"
						})
					}),
					"orFilter": functions.toArray({
						"value1": functions.toObject({
							"f": "CardCode",
							"o": "ilike",
							"v": null
						}),
						"value2": functions.toObject({
							"f": "CardName",
							"o": "ilike",
							"v": null
						})
					}),
					"order": functions.toArray({
						"value1": functions.toObject({
							"f": "CardCode",
							"v": "asc"
						})
					}),
					"callback": async (data, extra) => {
						functions.userDefined('globalModalHide', {});
						functions.showElement({
							"component": "pnlSelectCustomerModal"
						});
					}
				});
			},
			"noCallback": async (data, extra) => {
				functions.userDefined('globalModalLoading', {});
			}
		});
	},

	handleLblSelectOutletCodeClick: async () => {
		functions.userDefined('globalModalLoading', {});
		functions.conditional({
			"condition": functions.equal({
				"value1": null,
				"value2": "My"
			}),
			"yesCallback": "",
			"noCallback": ""
		});
		functions.toArray({
			"value1": "",
			"value2": functions.setComponentValue({
				"component": "txtShipToCode"
			}),
			"value3": functions.setComponentValue({
				"component": "etAddress"
			})
		});
	},

	handleBtnSearchClick: async () => {
		functions.conditional({
			"condition": functions.getVarAttr({
				"var": "vCurrSalesBP",
				"attr": "CardCode"
			}),
			"yesCallback": async (data, extra) => {
				functions.conditional({
					"condition": functions.equal({
						"value1": functions.getVar({
							"var": "vType"
						}),
						"value2": "draft"
					}),
					"yesCallback": async (data, extra) => {
						functions.toArray({
							"value1": functions.userDefined('globalModalLoading', {}),
							"value2": null
						});
					},
					"noCallback": async (data, extra) => {
						functions.conditional({
							"condition": functions.equal({
								"value1": functions.getVar({
									"var": "vType"
								}),
								"value2": "open"
							}),
							"yesCallback": async (data, extra) => {
								functions.toArray({
									"value1": functions.userDefined('globalModalLoading', {}),
									"value2": null
								});
							},
							"noCallback": async (data, extra) => {
								functions.toArray({
									"value1": functions.userDefined('globalModalLoading', {}),
									"value2": null
								});
							}
						});
					}
				});
			},
			"noCallback": async (data, extra) => {
				functions.userDefined('globalModalInfo', {
					"title": "Error Filter",
					"message": "Please select a customer."
				});
			}
		});
	},

	handlePnlModalBodyButtonPositiveClick: async () => {
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "vAction"
				}),
				"value2": "Invalid date range"
			}),
			"yesCallback": async (data, extra) => {
				functions.setVar({
					"var": "vAction"
				});
				functions.userDefined('globalModalHide', {});
				functions.showElement({
					"component": "pnlModalFilter"
				});
			},
			"noCallback": async (data, extra) => {
				functions.setVar({
					"var": "vAction"
				});
				functions.userDefined('globalModalHide', {});
			}
		});
	},

	handlePnlModalBodyButtonNegativeClick: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalHide', {});
	},

	handleButton986Click: async () => {
		functions.showElement({
			"component": "pnlSelectCustomerModal"
		});
		await functions.loadData({
			"order": functions.toArray({
				"value1": functions.toObject({
					"f": "Name",
					"v": "asc"
				})
			}),
			"callback": async (data, extra) => {
				functions.console({
					"value": data
				});
				await functions.dataFromString({
					"errCallback": null,
					"dataset": "l_customer",
					"string": data,
					"callback": null
				});
				await functions.loadData({
					"errCallback": "",
					"dataset": "l_customer",
					"callback": async (data, extra) => {
						functions.userDefined('globalModalHide', {});
					}
				});
			},
			"errCallback": async (data, extra) => {
				functions.setVar({
					"var": "error",
					"value": data["err"]
				});
				functions.userDefined('globalModalInfo', {
					"message": functions.conditional({
						"condition": functions.getVarAttr({
							"var": "error",
							"attr": "global"
						}),
						"yesValue": functions.getVarAttr({
							"var": "error",
							"attr": "global"
						}),
						"noValue": "Cannot connect to Navision Server."
					}),
					"title": "Error"
				});
			},
			"dataset": "MSC_Customer_List",
			"filter": ""
		});
		functions.hideElement({
			"component": "pnlModalFilter"
		});
	},

	handlePnlModalBodyButtonPositive165Click: async () => {
		functions.console({
			"value": functions.dateDiff({
				"value1": functions.componentValue({
					"component": "dpDateFrom"
				}),
				"value2": functions.componentValue({
					"component": "dpDateTo"
				}),
				"interval": ""
			})
		});
		functions.console({
			"value": null
		});
		functions.console({
			"value": null
		});
		functions.conditional({
			"condition": functions.equalOrGreater({
				"value1": functions.dateDiff({
					"value1": functions.conditional({
						"condition": functions.componentValue({
							"component": "dpDateFrom"
						}),
						"yesValue": functions.formatDate({
							"date": functions.componentValue({
								"component": "dpDateFrom"
							}),
							"format": "d-m-Y"
						}),
						"noValue": "01-01-1901",
						"yesCallback": ""
					}),
					"value2": functions.conditional({
						"condition": functions.componentValue({
							"component": "dpDateTo"
						}),
						"yesValue": functions.formatDate({
							"date": functions.componentValue({
								"component": "dpDateTo"
							}),
							"format": "d-m-Y"
						}),
						"noValue": "31-12-2154",
						"yesCallback": ""
					}),
					"interval": ""
				}),
				"value2": 0
			}),
			"yesValue": "",
			"yesCallback": async (data, extra) => {
				functions.console({
					"value": "valid date range"
				});
				functions.userDefined('globalModalLoading', {});
				functions.conditional({
					"condition": functions.equal({
						"value2": "All",
						"value1": null
					}),
					"yesCallback": async (data, extra) => {
						functions.console({
							"value": "Customer Filter All"
						});
						functions.conditional({
							"condition": functions.equal({
								"value1": null,
								"value2": "ALL"
							}),
							"yesCallback": async (data, extra) => {
								functions.console({
									"value": "All Route"
								});
								await functions.loadData({
									"errCallback": async (data, extra) => {
										functions.userDefined('globalModalHide', {});
									},
									"dataset": "l_Posted_Sales_Invoice_List",
									"filter": functions.toArray({
										"value1": functions.toObject({
											"f": "Posting_Date",
											"v": functions.formatDate({
												"format": "Y-m-d",
												"date": functions.componentValue({
													"component": "dpDateFrom"
												})
											}),
											"o": ">="
										}),
										"value2": functions.toObject({
											"f": "Posting_Date",
											"v": functions.formatDate({
												"date": functions.componentValue({
													"component": "dpDateTo"
												}),
												"format": "Y-m-d"
											}),
											"o": "<="
										}),
										"value3": "",
										"value4": ""
									}),
									"callback": async (data, extra) => {
										functions.userDefined('globalModalHide', {});
										functions.hideElement({
											"component": "pnlModalFilter"
										});
									}
								});
							},
							"noCallback": async (data, extra) => {
								functions.console({
									"value": "Filter Route"
								});
								await functions.loadData({
									"filter": functions.toArray({
										"value1": functions.toObject({
											"f": "Posting_Date",
											"v": functions.formatDate({
												"date": functions.componentValue({
													"component": "dpDateFrom"
												}),
												"format": "Y-m-d"
											}),
											"o": ">="
										}),
										"value2": functions.toObject({
											"f": "Posting_Date",
											"v": functions.formatDate({
												"date": functions.componentValue({
													"component": "dpDateTo"
												}),
												"format": "Y-m-d"
											}),
											"o": "<="
										}),
										"value3": functions.toObject({
											"f": "Location_Code",
											"v": null,
											"o": "="
										}),
										"value4": ""
									}),
									"callback": async (data, extra) => {
										functions.userDefined('globalModalHide', {});
										functions.hideElement({
											"component": "pnlModalFilter"
										});
									},
									"errCallback": async (data, extra) => {
										functions.userDefined('globalModalHide', {});
									},
									"dataset": "l_Posted_Sales_Invoice_List"
								});
							}
						});
					},
					"noCallback": async (data, extra) => {
						functions.console({
							"value": "Customer Filter By"
						});
						functions.console({
							"value": functions.concat({
								"string1": "Filter by <br>",
								"string2": null,
								"string3": "<br>From Date :",
								"string4": functions.concat({
									"string1": functions.formatDate({
										"date": null,
										"format": "d-m-Y"
									}),
									"string2": "<br>To Date: ",
									"string3": functions.formatDate({
										"date": null,
										"format": "d-m-Y"
									})
								})
							})
						});
						functions.conditional({
							"condition": functions.equal({
								"value2": "ALL",
								"value1": null
							}),
							"yesCallback": async (data, extra) => {
								await functions.loadData({
									"dataset": "l_Posted_Sales_Invoice_List",
									"filter": functions.toArray({
										"value1": functions.toObject({
											"f": "Bill_to_Customer_No",
											"o": "=",
											"v": null
										}),
										"value2": functions.toObject({
											"f": "Posting_Date",
											"v": functions.formatDate({
												"date": functions.componentValue({
													"component": "dpDateFrom"
												}),
												"format": "Y-m-d"
											}),
											"o": ">="
										}),
										"value3": functions.toObject({
											"f": "Posting_Date",
											"v": functions.formatDate({
												"format": "Y-m-d",
												"date": functions.componentValue({
													"component": "dpDateTo"
												})
											}),
											"o": "<="
										}),
										"value4": ""
									}),
									"order": functions.toArray({
										"value1": functions.toObject({
											"f": "No",
											"v": "desc"
										})
									}),
									"callback": async (data, extra) => {
										functions.userDefined('globalModalHide', {});
										functions.hideElement({
											"component": "pnlModalFilter"
										});
									},
									"errCallback": async (data, extra) => {
										functions.userDefined('globalModalHide', {});
									}
								});
							},
							"noCallback": async (data, extra) => {
								await functions.loadData({
									"dataset": "l_Posted_Sales_Invoice_List",
									"filter": functions.toArray({
										"value1": functions.toObject({
											"f": "Bill_to_Customer_No",
											"o": "=",
											"v": null
										}),
										"value2": functions.toObject({
											"f": "Posting_Date",
											"v": functions.formatDate({
												"date": functions.componentValue({
													"component": "dpDateFrom"
												}),
												"format": "Y-m-d"
											}),
											"o": ">="
										}),
										"value3": functions.toObject({
											"f": "Posting_Date",
											"v": functions.formatDate({
												"format": "Y-m-d",
												"date": functions.componentValue({
													"component": "dpDateTo"
												})
											}),
											"o": "<="
										}),
										"value4": functions.toObject({
											"f": "Location_Code",
											"v": null,
											"o": "="
										})
									}),
									"order": functions.toArray({
										"value1": functions.toObject({
											"f": "No",
											"v": "desc"
										})
									}),
									"callback": async (data, extra) => {
										functions.userDefined('globalModalHide', {});
										functions.hideElement({
											"component": "pnlModalFilter"
										});
									},
									"errCallback": async (data, extra) => {
										functions.userDefined('globalModalHide', {});
									}
								});
							}
						});
					}
				});
			},
			"noCallback": async (data, extra) => {
				functions.console({
					"value": "invalid date range"
				});
				functions.hideElement({
					"component": "pnlModalFilter"
				});
				functions.setVar({
					"var": "vAction",
					"value": "Invalid date range"
				});
				functions.userDefined('globalModalInfo', {
					"title": "Invalid Filter",
					"message": "Date To cannot be earlier than Date From, please change and try again."
				});
			}
		});
	},

	handlePnlModalBodyButtonPositive165757Click: async () => {
		functions.hideElement({
			"component": "pnlModalFilter"
		});
	},

	handlePnlModalFilterTblCellClick: async () => {
		functions.conditional({
			"condition": functions.concat({
				"string1": functions.getVarAttr({
					"var": "mapCustomerListData",
					"attr": "No"
				}),
				"string2": "-",
				"string3": functions.getVarAttr({
					"var": "mapCustomerListData",
					"attr": "Name"
				})
			}),
			"yesValue": functions.concat({
				"string1": functions.getVarAttr({
					"var": "mapCustomerListData",
					"attr": "No"
				}),
				"string2": "-",
				"string3": functions.getVarAttr({
					"var": "mapCustomerListData",
					"attr": "Name"
				})
			}),
			"noValue": functions.concat({
				"string1": functions.getVarAttr({
					"var": "vCustomer_selected",
					"attr": "No"
				}),
				"string2": "-",
				"string3": functions.getVarAttr({
					"var": "vCustomer_selected",
					"attr": "Name"
				})
			}),
			"yesCallback": ""
		});
	},

	handleImgSelectCustomerBackClick: async () => {
		functions.hideElement({
			"component": "pnlSelectCustomerModal"
		});
		functions.showElement({
			"component": "pnlModalFilter"
		});
	},

	handlePnlSelectCustomerMainHeaderRightClick: async () => {
		functions.userDefined('globalModalLoading', {});
		functions.conditional({
			"condition": functions.equal({
				"value1": null,
				"value2": "All"
			}),
			"yesCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "lblAll",
					"value": "My"
				});
			},
			"noCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "lblAll",
					"value": "All"
				});
			}
		});
	},

	handlePgOrderHistoryLoad: async () => {
		functions.userDefined('globalModalLoading', {});
		functions.onBackButton({
			"callback": async (data, extra) => {
				functions.gotoPage({
					"p": "pgMainMenu"
				});
			}
		});
		functions.setVar({
			"var": "vAction"
		});
		functions.setTimeout({
			"callback": async (data, extra) => {
				await functions.loadData({
					"dataset": "OK365_Posted_Sales_Invoice_List",
					"filter": functions.toArray({
						"value1": functions.toObject({
							"o": "=",
							"v": functions.getVarAttr({
								"var": "vRouteMaster",
								"attr": "Location_Code",
								"default": ""
							}),
							"f": "Location_Code"
						}),
						"value2": "",
						"value3": "",
						"value4": "",
						"value5": "",
						"value6": "",
						"value7": "",
						"value8": "",
						"value9": "",
						"value10": ""
					}),
					"order": functions.toArray({
						"value1": functions.toObject({
							"f": "No",
							"v": "desc"
						}),
						"value2": "",
						"value3": "",
						"value4": "",
						"value5": "",
						"value6": "",
						"value7": "",
						"value8": "",
						"value9": "",
						"value10": ""
					}),
					"callback": async (data, extra) => {
						functions.console({
							"value": data
						});
						await functions.dataFromString({
							"string": data,
							"callback": "",
							"errCallback": "",
							"dataset": "l_Posted_Sales_Invoice_List"
						});
						functions.conditional({
							"condition": functions.getVar({
								"var": "vCurrCustomer",
								"default": ""
							}),
							"yesValue": "",
							"noValue": "",
							"extra": "",
							"yesCallback": async (data, extra) => {
								functions.setComponentValue({
									"component": "txtSearch",
									"value": functions.getVarAttr({
										"var": "vCurrCustomer",
										"attr": "Customer_Code"
									})
								});
								await functions.loadData({
									"errCallback": async (data, extra) => {
										functions.userDefined('globalModalHide', {});
									},
									"dataset": "l_Posted_Sales_Invoice_List",
									"filter": functions.toArray({
										"value1": functions.toObject({
											"f": "Sell_to_Customer_No",
											"v": functions.getVarAttr({
												"var": "vCurrCustomer",
												"attr": "Customer_Code"
											})
										})
									}),
									"callback": async (data, extra) => {
										functions.userDefined('globalModalHide', {});
										await functions.loadData({
											"errCallback": async (data, extra) => {
												functions.setVar({
													"var": "error",
													"value": data["err"]
												});
											},
											"dataset": "OK365_Location_List",
											"callback": async (data, extra) => {
												functions.console({
													"value": data
												});
												functions.setVar({
													"var": "vLocationListData",
													"value": data
												});
												functions.setVar({
													"var": "vLocationListDataCnt",
													"value": functions.count({
														"values": functions.getVar({
															"var": "vLocationListData"
														})
													})
												});
												functions.setVar({
													"var": ""
												});
												functions.newArray({
													"var": "arrLocationList"
												});
												functions.push({
													"var": "arrLocationList",
													"value": functions.toObject({
														"Code": "ALL",
														"Name": "ALL"
													})
												});
												functions.conditional({
													"condition": functions.getVar({
														"var": "vLocationListDataCnt"
													}),
													"yesCallback": async (data, extra) => {
														functions.map({
															"values": functions.getVar({
																"var": "vLocationListData"
															}),
															"callback": async (data, extra) => {
																functions.setVar({
																	"var": "vTmpData",
																	"value": data
																});
																functions.push({
																	"var": "arrLocationList",
																	"value": functions.toObject({
																		"Name": functions.getVarAttr({
																			"var": "vTmpData",
																			"attr": "Name"
																		}),
																		"Code": functions.getVarAttr({
																			"var": "vTmpData",
																			"attr": "Code"
																		})
																	})
																});
															}
														});
													},
													"noCallback": ""
												});
												functions.setComboOptions({
													"data": functions.getVar({
														"var": "arrLocationList"
													}),
													"valueField": "Code",
													"displayField": "Name",
													"combo": "cmbRoute"
												});
												functions.userDefined('globalModalHide', {});
											}
										});
									}
								});
							},
							"noCallback": async (data, extra) => {
								functions.setComponentValue({
									"component": "txtSearch",
									"value": ""
								});
								await functions.loadData({
									"dataset": "l_Posted_Sales_Invoice_List",
									"callback": async (data, extra) => {
										functions.userDefined('globalModalHide', {});
										await functions.loadData({
											"dataset": "OK365_Location_List",
											"callback": async (data, extra) => {
												functions.console({
													"value": data
												});
												functions.setVar({
													"var": "vLocationListData",
													"value": data
												});
												functions.setVar({
													"var": "vLocationListDataCnt",
													"value": functions.count({
														"values": functions.getVar({
															"var": "vLocationListData"
														})
													})
												});
												functions.setVar({
													"var": ""
												});
												functions.newArray({
													"var": "arrLocationList"
												});
												functions.push({
													"var": "arrLocationList",
													"value": functions.toObject({
														"Code": "ALL",
														"Name": "ALL"
													})
												});
												functions.conditional({
													"condition": functions.getVar({
														"var": "vLocationListDataCnt"
													}),
													"yesCallback": async (data, extra) => {
														functions.map({
															"values": functions.getVar({
																"var": "vLocationListData"
															}),
															"callback": async (data, extra) => {
																functions.setVar({
																	"var": "vTmpData",
																	"value": data
																});
																functions.push({
																	"var": "arrLocationList",
																	"value": functions.toObject({
																		"Code": functions.getVarAttr({
																			"var": "vTmpData",
																			"attr": "Code"
																		}),
																		"Name": functions.getVarAttr({
																			"var": "vTmpData",
																			"attr": "Name"
																		})
																	})
																});
															}
														});
													},
													"noCallback": ""
												});
												functions.setComboOptions({
													"valueField": "Code",
													"displayField": "Name",
													"combo": "cmbRoute",
													"data": functions.getVar({
														"var": "arrLocationList"
													})
												});
												functions.userDefined('globalModalHide', {});
											},
											"errCallback": async (data, extra) => {
												functions.setVar({
													"var": "error",
													"value": data["err"]
												});
											}
										});
									},
									"errCallback": async (data, extra) => {
										functions.userDefined('globalModalHide', {});
									}
								});
							}
						});
					},
					"errCallback": async (data, extra) => {
						functions.setVar({
							"var": "error",
							"value": data["err"]
						});
						functions.userDefined('globalModalInfo', {
							"title": "Error",
							"message": functions.conditional({
								"condition": functions.getVarAttr({
									"var": "error",
									"attr": "global"
								}),
								"yesValue": functions.getVarAttr({
									"var": "error",
									"attr": "global"
								}),
								"noValue": "Cannot connect to Navision Server."
							})
						});
					}
				});
			},
			"timeout": 100,
			"extra": ""
		});
	}
};

const PgOrderHistory = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;
	formatParentStyleLayout(props);

	useEffect(() => {
		actions.handlePgOrderHistoryLoad();
	}, [currentTime]);
    
    return (
		<Page data={props.pgOrderHistory}>
			<Panel data={props.pnlHolder}>
				<Panel data={props.pnlMain}>
					<Panel data={props.pnlMainHeader}>
						<Panel data={props.pnlMainHeaderLeft}>
							<Panel data={props.pnlMainHeaderLeftTable}>
								<Panel data={props.pnlMainHeaderLeftCell}>
									<Image data={props.imgBack} action={actions.handleImgBackClick} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderMiddle}>
							<Panel data={props.pnlMainHeaderMiddleTable}>
								<Panel data={props.pnlMainHeaderMiddleCell}>
									<Label data={props.lblMainTitle} action={actions.handleLblMainTitleClick} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderRight}>
							<Panel data={props.pnlMainHeaderRightTable}>
								<Panel data={props.pnlMainHeaderRightCell}>
									<Image data={props.imgSearchFilters} action={actions.handleImgSearchFiltersClick} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainSearch}>
						<Panel data={props.pnlMainBodySearch}>
							<Edit data={props.txtSearch} />
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBody}>
						<Panel data={props.pnlBody}>
							<DataList data={props.dlPostedSalesInvoiceList} functions={functions} currentTime={currentTime}>
								{({ item, parentStyle }) => (							
									<Panel data={item.pnlMainDataContainer} parentStyle={parentStyle}>
										<Panel data={item.pnlDataTbl} parentStyle={parentStyle}>
											<Panel data={item.Panel212} parentStyle={parentStyle}>
												<Label data={item.lblNo} parentStyle={parentStyle} />
												<Label data={item.lblCustomer} parentStyle={parentStyle} />
												<Label data={item.lblPostingDate} parentStyle={parentStyle} />
												<Label data={item.lblAmount} parentStyle={parentStyle} />
											</Panel>
											<Panel data={item.Panel212641} parentStyle={parentStyle}>
												<Image data={item.Image318} parentStyle={parentStyle} />
											</Panel>
										</Panel>
									</Panel>
								)}
							</DataList>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlUpdateCartModal}>
					<Panel data={props.pnlUpdateCartModalTable}>
						<Panel data={props.pnlUpdateCartModalCell}>
							<Panel data={props.pnlUpdateCartModalBody}>
								<Panel data={props.pnlUpdateCartModalBody1}>
									<Image data={props.imgUpdateCartImage} />
									<Image data={props.imgUpdateCartClose} action={actions.handleImgUpdateCartCloseClick} />
								</Panel>
								<Panel data={props.pnlUpdateCartModalBody2}>
									<Panel data={props.pnlMainBodyForm1}>
										<Panel data={props.pnlMainBodyForm1Label}>
											<Label data={props.lblCustomerName} />
											<Label data={props.lblSelectCustomer} action={actions.handleLblSelectCustomerClick} />
										</Panel>
										<Panel data={props.pnlMainBodyForm1Input}>
											<Edit data={props.txtCustomerName} />
										</Panel>
									</Panel>
									<Panel data={props.pnlMainBodyForm1481}>
										<Panel data={props.pnlMainBodyForm1Label551333}>
											<Label data={props.lblOutletCode} />
											<Label data={props.lblSelectOutletCode} action={actions.handleLblSelectOutletCodeClick} />
										</Panel>
										<Panel data={props.pnlMainBodyForm1Input180830}>
											<Edit data={props.txtOutletCode} />
										</Panel>
									</Panel>
									<Panel data={props.pnlMainBodyForm2}>
										<Panel data={props.pnlMainBodyForm2Label}>
											<Label data={props.lblDeliveryDateFrom} />
										</Panel>
										<Panel data={props.pnlMainBodyForm2Input}>
											<Edit data={props.txtDateFrom} />
											<Label data={props.lblDocumentDateTo} />
											<Edit data={props.txtDateTo} />
										</Panel>
									</Panel>
									<Panel data={props.pnlMainBodyForm2498}>
										<Panel data={props.pnlMainBodyForm2Label820995}>
											<Label data={props.lblDocumentDate} />
										</Panel>
										<Panel data={props.pnlMainBodyForm2Input739198}>
											<Edit data={props.txtDocumentDateFrom} />
											<Label data={props.lblDocumentDateTo851} />
											<Edit data={props.txtDocumentDateTo} />
										</Panel>
									</Panel>
									<Panel data={props.pnlMainBodyForm3}>
										<Panel data={props.pnlMainBodyForm1Label554794}>
											<Label data={props.lblCustomerName22} />
										</Panel>
										<Panel data={props.pnlMainBodyForm1Input696752}>
											<ComboBox data={props.cmbSalesperson} functions={functions} />
										</Panel>
									</Panel>
								</Panel>
								<Panel data={props.pnlUpdateCartModalBody5}>
									<Button data={props.btnSearch} action={actions.handleBtnSearchClick} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal}>
					<Panel data={props.pnlModalTable}>
						<Panel data={props.pnlModalCell}>
							<Panel data={props.pnlModalBodyMain}>
								<Panel data={props.pnlModalBodyTitle}>
									<Label data={props.lblModalTitle} />
								</Panel>
								<Panel data={props.pnlModalBodyMessage}>
									<Label data={props.lblModalMessage} />
								</Panel>
								<Panel data={props.pnlModalBodyLoading}>
									<Image data={props.imgModalLoading} />
								</Panel>
							</Panel>
							<Panel data={props.pnlModalBodyButtons}>
								<Panel data={props.pnlModalBodyButtonPositive} action={actions.handlePnlModalBodyButtonPositiveClick}>
									<Label data={props.lblModalPositive} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonNegative} action={actions.handlePnlModalBodyButtonNegativeClick}>
									<Label data={props.lblModalNegative} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlCSS}>
					<Label data={props.lblStyle} />
				</Panel>
				<Panel data={props.pnlModalFilter}>
					<Panel data={props.pnlModalFilterTbl}>
						<Panel data={props.pnlModalFilterTblCell} action={actions.handlePnlModalFilterTblCellClick}>
							<Panel data={props.pnlModalFilterTblBody}>
								<Panel data={props.pnlModalBodyTitle267}>
									<Label data={props.lblModalTitle437} />
								</Panel>
								<Panel data={props.pnlModalBodyMessage512}>
									<Panel data={props.Panel819}>
										<Label data={props.lblModalMessage602} />
										<Label data={props.Label377} />
										<ComboBox data={props.cmbCustomer} functions={functions} />
										<Button data={props.Button986} action={actions.handleButton986Click} />
									</Panel>
									<Panel data={props.Panel819474}>
										<Label data={props.lblModalMessage602144736} />
										<Label data={props.Label377810993} />
										<ComboBox data={props.cmbRoute} functions={functions} />
									</Panel>
									<Panel data={props.Panel617}>
										<Label data={props.Label411} />
										<Edit data={props.dpDateFrom} />
									</Panel>
									<Panel data={props.Panel617854}>
										<Label data={props.Label41195247} />
										<Edit data={props.dpDateTo} />
									</Panel>
								</Panel>
							</Panel>
							<Panel data={props.pnlModalBodyButtons738}>
								<Panel data={props.pnlModalBodyButtonPositive165} action={actions.handlePnlModalBodyButtonPositive165Click}>
									<Label data={props.lblModalPositive564} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonPositive165757} action={actions.handlePnlModalBodyButtonPositive165757Click}>
									<Label data={props.lblModalPositive564183217} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlSelectCustomerModal}>
					<Panel data={props.pnlSelectCustomerModalTable}>
						<Panel data={props.pnlSelectCustomerModalCell}>
							<Panel data={props.pnlSelectCustomerModalBody}>
								<Panel data={props.pnlSelectCustomerMainHeader}>
									<Panel data={props.pnlSelectCustomerMainHeaderLeft}>
										<Panel data={props.pnlSelectCustomerMainHeaderLeftTable}>
											<Panel data={props.pnlSelectCustomerMainHeaderLeftCell}>
												<Image data={props.imgSelectCustomerBack} action={actions.handleImgSelectCustomerBackClick} />
											</Panel>
										</Panel>
									</Panel>
									<Panel data={props.pnlSelectCustomerMainHeaderMiddle}>
										<Panel data={props.pnlSelectCustomerMainHeaderMiddleTable}>
											<Panel data={props.pnlSelectCustomerMainHeaderMiddleCell}>
												<Label data={props.lblSelectCustomerMainTitle} />
											</Panel>
										</Panel>
									</Panel>
									<Panel data={props.pnlSelectCustomerMainHeaderRight} action={actions.handlePnlSelectCustomerMainHeaderRightClick}>
										<Panel data={props.pnlSelectCustomerMainHeaderRightTable}>
											<Panel data={props.pnlSelectCustomerMainHeaderRightCell}>
												<Label data={props.lblAll} />
											</Panel>
										</Panel>
									</Panel>
								</Panel>
								<Panel data={props.pnlSelectCustomerMainBody1}>
									<Panel data={props.pnlSelectCustomerMainBodySearch}>
										<Edit data={props.txtSelectCustomerSearch} />
									</Panel>
								</Panel>
								<Panel data={props.pnlSelectCustomerMainBody2}>
									<DataList data={props.dlCustomerList} functions={functions} currentTime={currentTime}>
										{({ item, parentStyle }) => (									
											<Panel data={item.pnlSelectCustomerListing} parentStyle={parentStyle}>
												<Panel data={item.pnlSelectCustomerBodyLeft} parentStyle={parentStyle}>
													<Panel data={item.pnlSelectCustomerBodyLeft1} parentStyle={parentStyle}>
														<Label data={item.lblCardCode} parentStyle={parentStyle} />
														<Label data={item.lblName} parentStyle={parentStyle} />
														<Label data={item.lblPaymTerms} parentStyle={parentStyle} />
													</Panel>
												</Panel>
												<Panel data={item.pnlSelectCustomerBodyMiddle} parentStyle={parentStyle}>
													<Panel data={item.pnlSelectCustomerBodyMiddle1} parentStyle={parentStyle} />
												</Panel>
												<Panel data={item.pnlSelectCustomerBodyRight} parentStyle={parentStyle}>
													<Panel data={item.pnlSelectCustomerBodyRight1} parentStyle={parentStyle}>
														<Panel data={item.pnlSelectCustomerRight1Table} parentStyle={parentStyle}>
															<Panel data={item.pnlSelectCustomerRight1Cell} parentStyle={parentStyle}>
																<Image data={item.imgGo} parentStyle={parentStyle} />
															</Panel>
														</Panel>
													</Panel>
												</Panel>
											</Panel>
										)}
									</DataList>
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgOrderHistory);