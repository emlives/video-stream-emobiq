import React from 'react';

import { connect } from 'react-redux';

import Page from '../framework/component/Page';
import Label from '../framework/component/Label';
import Button from '../framework/component/Button';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

import Functions from '../framework/core/Function';

import { StateName } from '../reducer/data/RenderPage';

const functions = new Functions(StateName);

const actions = {

    /**
     * START - Button gotoPage
     */
    handleButtonChangeName: () => {
        functions.setComponentValue({
            component: 'labelFirstName',
            value: 'Second Page'
        });
    },
    handleButtonGoToPage: () => {
        functions.gotoPage({
            p: 'ShiftingPage'
        });
    },
    /**
     * END - Normal Actions
     */
}

const RenderPage = (props) => {

    // For datalist autoload handling when to render
    const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;

    return (
        <Page currentTime={currentTime}>
            <Label data={props.labelFirstName} functions={functions} currentTime={currentTime}></Label>
            <Label data={props.labelLastName} functions={functions} currentTime={currentTime}></Label>
            <Button data={props.buttonChangeName} action={actions.handleButtonChangeName} currentTime={currentTime}></Button>
            <Button data={props.buttonGoToPage} action={actions.handleButtonGoToPage} currentTime={currentTime}></Button>
        </Page>
    )
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] }
};

export default connect(mapStateToProps)(RenderPage);