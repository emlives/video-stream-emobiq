import React, { useEffect }  from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgOrderHistoryDetails';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import DataList from '../framework/component/DataList';
import ComboBox from '../framework/component/ComboBox';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handleImgBackClick: async () => {
		functions.gotoPage({
			"p": "pgOrderHistory"
		});
	},

	handleImgBack555407Click: async () => {
		functions.showElement({
			"component": "pnlPrinter"
		});
	},

	handlePnlMainFooterTblCell1Click: async () => {
		functions.setVar({
			"var": "vInvoicedLines",
			"value": functions.objectAttr({
				"object": functions.objectAttr({
					"attr": "SalesInvLines",
					"object": functions.getVar({
						"var": "vPostedSalesInvoiceHeader"
					})
				}),
				"attr": "Posted_Sales_Invoice_Line"
			})
		});
		await functions.navCall({
			"connector": "nav",
			"ent": "GetItemSalesPrice",
			"function": "getSigNaturePic",
			"data": functions.toObject({
				"intTab": 112,
				"cdDocode": functions.getVarAttr({
					"var": "vPostedSalesInvoiceHeader",
					"attr": "No"
				})
			}),
			"callback": async (data, extra) => {
				functions.setVar({
					"var": "vSignature",
					"value": functions.objectAttr({
						"object": data,
						"attr": "return_value"
					})
				});
				functions.console({
					"value": data
				});
				functions.console({
					"value": "output result for signature"
				});
				await functions.loadData({
					"dataset": "MSC_Company_Information",
					"filter": "",
					"callback": async (data, extra) => {
						functions.setVar({
							"var": "vCompany",
							"value": data[0]
						});
						functions.console({
							"value": functions.getVar({
								"var": "vCompany"
							})
						});
						functions.console({
							"value": "************ START RECEIPT *********"
						});
						functions.toArray({
							"value1": functions.setVar({
								"var": "vGSTValue",
								"value": functions.getVar({
									"var": "vGstTotalRate"
								})
							}),
							"value2": functions.setVar({
								"var": "width",
								"value": 38
							}),
							"value3": functions.setVar({
								"var": "center",
								"value": 19
							}),
							"value4": functions.setVar({
								"var": "margin_left",
								"value": 0
							}),
							"value5": functions.setVar({
								"var": "width_center",
								"value": 10
							})
						});
						functions.toArray({
							"value1": functions.setVar({
								"var": "vHeader",
								"value": functions.concat({
									"string1": functions.concat({
										"string1": functions.padString({
											"len": functions.getVar({
												"var": "margin_left"
											}),
											"type": "left",
											"char": " "
										}),
										"string2": functions.concat({
											"string1": "[size='24pt']",
											"string2": functions.padString({
												"string": functions.getVarAttr({
													"var": "vCompany",
													"attr": "Name"
												}),
												"len": 30,
												"type": "center"
											}),
											"string3": "[/size]",
											"string4": ""
										}),
										"string3": null,
										"string4": ""
									}),
									"string2": null,
									"string3": functions.concat({
										"string1": functions.concat({
											"string1": functions.padString({
												"len": functions.getVar({
													"var": "left_margin"
												}),
												"type": "left",
												"char": " "
											}),
											"string2": functions.padString({
												"len": functions.getVar({
													"var": "width"
												}),
												"type": "center",
												"string": functions.concat({
													"string1": functions.getVarAttr({
														"var": "vCompany",
														"attr": "Address"
													}),
													"string2": "",
													"string3": functions.getVarAttr({
														"var": "vCompany",
														"attr": "Address2"
													}),
													"string4": ""
												})
											}),
											"string3": null
										}),
										"string2": functions.concat({
											"string1": functions.padString({
												"len": functions.getVar({
													"var": "left_margin"
												}),
												"type": "left",
												"char": " "
											}),
											"string2": functions.padString({
												"type": "center",
												"string": functions.concat({
													"string1": functions.getVarAttr({
														"var": "vCompany",
														"attr": "City"
													}),
													"string2": " ",
													"string3": functions.getVarAttr({
														"var": "vCompany",
														"attr": "Post_Code"
													})
												}),
												"len": functions.getVar({
													"var": "width"
												})
											}),
											"string3": null
										}),
										"string3": functions.concat({
											"string1": functions.padString({
												"len": functions.sub({
													"value1": functions.getVar({
														"var": "center"
													}),
													"value2": 16
												}),
												"type": "center",
												"char": " "
											}),
											"string2": functions.concat({
												"string1": "Tel",
												"string2": ": ",
												"string3": functions.padString({
													"string": functions.getVarAttr({
														"var": "vCompany",
														"attr": "Phone_No"
													}),
													"len": 10,
													"type": "left",
													"char": ""
												}),
												"string4": ""
											}),
											"string3": "",
											"string4": functions.concat({
												"string1": "Fax",
												"string2": ": ",
												"string3": functions.padString({
													"string": 63826045,
													"len": 10,
													"type": "left",
													"char": ""
												}),
												"string4": functions.concat({
													"string1": null,
													"string2": null
												})
											})
										}),
										"string4": functions.concat({
											"string1": functions.concat({
												"string1": functions.padString({
													"len": functions.sub({
														"value2": 11,
														"value1": functions.getVar({
															"var": "center"
														})
													}),
													"type": "center",
													"char": " "
												}),
												"string2": "CO.REG.No",
												"string3": ": ",
												"string4": functions.padString({
													"string": functions.getVarAttr({
														"var": "vCompany",
														"attr": "VAT_Registration_No"
													}),
													"len": functions.sub({
														"value1": functions.getVar({
															"var": "width_center"
														}),
														"value2": 10
													}),
													"type": "center"
												})
											}),
											"string2": null,
											"string3": functions.concat({
												"string1": functions.padString({
													"char": " ",
													"len": functions.sub({
														"value1": functions.getVar({
															"var": "center"
														}),
														"value2": 12
													}),
													"type": "center"
												}),
												"string2": "GST.REG.No",
												"string3": ": ",
												"string4": functions.padString({
													"string": functions.getVarAttr({
														"var": "vCompany",
														"attr": "VAT_Registration_No"
													}),
													"len": functions.sub({
														"value1": functions.getVar({
															"var": "width_center"
														}),
														"value2": 10
													}),
													"type": "center"
												})
											})
										})
									}),
									"string4": ""
								})
							}),
							"value2": functions.setVar({
								"var": "vSubHeader",
								"value": functions.concat({
									"string1": functions.concat({
										"string1": functions.concat({
											"string1": functions.padString({
												"type": "left",
												"char": " ",
												"len": functions.getVar({
													"var": "left_margin"
												})
											}),
											"string2": null,
											"string3": ""
										}),
										"string2": functions.concat({
											"string1": functions.concat({
												"string1": functions.padString({
													"type": "left",
													"char": " ",
													"len": functions.getVar({
														"var": "margin_left"
													})
												}),
												"string2": functions.padString({
													"char": "-",
													"len": functions.getVar({
														"var": "width"
													}),
													"type": "left"
												}),
												"string3": null
											}),
											"string2": functions.concat({
												"string1": functions.concat({
													"string1": functions.padString({
														"len": functions.getVar({
															"var": "left_margin"
														}),
														"type": "left",
														"char": " "
													}),
													"string2": functions.padString({
														"string": functions.concat({
															"string1": null
														}),
														"len": functions.getVar({
															"var": "width"
														}),
														"type": "center"
													}),
													"string3": null
												}),
												"string2": functions.concat({
													"string1": functions.padString({
														"len": functions.getVar({
															"var": "margin_left"
														}),
														"type": "left",
														"char": " "
													}),
													"string2": functions.padString({
														"len": functions.getVar({
															"var": "width"
														}),
														"type": "left",
														"char": "-"
													}),
													"string3": null
												})
											})
										})
									}),
									"string2": functions.concat({
										"string1": functions.padString({
											"len": functions.getVar({
												"var": "margin_left"
											}),
											"type": "left",
											"char": " "
										}),
										"string2": functions.concat({
											"string1": functions.concat({
												"string1": "Invoice No.",
												"string2": "",
												"string3": ""
											}),
											"string2": functions.padString({
												"len": functions.sub({
													"value1": functions.getVar({
														"var": "center"
													}),
													"value2": 24
												}),
												"char": " "
											}),
											"string3": ": ",
											"string4": ""
										}),
										"string3": functions.getVarAttr({
											"var": "vPostedSalesInvoiceHeader",
											"attr": "No"
										}),
										"string4": ""
									}),
									"string3": functions.concat({
										"string1": functions.concat({
											"string1": null,
											"string2": functions.padString({
												"string": "",
												"len": functions.sub({
													"value1": functions.getVar({
														"var": "center"
													}),
													"value2": 24
												}),
												"type": "left",
												"char": " "
											})
										}),
										"string2": functions.concat({
											"string1": "Date",
											"string2": functions.padString({
												"char": " ",
												"len": functions.sub({
													"value1": functions.getVar({
														"var": "center"
													}),
													"value2": 12
												})
											}),
											"string3": ": ",
											"string4": ""
										}),
										"string3": functions.formatDate({
											"date": functions.getVarAttr({
												"var": "vPostedSalesInvoiceHeader",
												"attr": "Posting_Date"
											}),
											"format": "d/m/Y"
										}),
										"string4": null
									}),
									"string4": functions.concat({
										"string1": functions.concat({
											"string1": functions.padString({
												"len": functions.getVar({
													"var": "margin_left"
												}),
												"type": "left",
												"char": " "
											}),
											"string2": functions.concat({
												"string1": "Salesman",
												"string2": functions.padString({
													"len": functions.sub({
														"value1": functions.getVar({
															"var": "center"
														}),
														"value2": 16
													}),
													"char": " "
												}),
												"string3": ": ",
												"string4": functions.concat({
													"string1": functions.getVarAttr({
														"var": "vPostedSalesInvoiceHeader",
														"attr": "Location_Code"
													}),
													"string2": "",
													"string3": null
												})
											}),
											"string3": "",
											"string4": ""
										}),
										"string2": functions.concat({
											"string1": functions.padString({
												"len": functions.getVar({
													"var": "margin_left"
												}),
												"type": "left",
												"char": " "
											}),
											"string2": functions.concat({
												"string1": "Terms",
												"string2": functions.padString({
													"string": "",
													"len": functions.sub({
														"value1": functions.getVar({
															"var": "width"
														}),
														"value2": 32
													}),
													"type": "",
													"char": ""
												}),
												"string3": ": ",
												"string4": ""
											}),
											"string3": functions.getVarAttr({
												"var": "vPostedSalesInvoiceHeader",
												"attr": "Payment_Terms_Code"
											}),
											"string4": ""
										}),
										"string3": null,
										"string4": functions.concat({
											"string1": functions.padString({
												"len": functions.getVar({
													"var": "margin_left"
												}),
												"type": "left",
												"char": " "
											}),
											"string2": functions.concat({
												"string1": "Customer",
												"string2": functions.padString({
													"len": functions.sub({
														"value1": functions.getVar({
															"var": "center"
														}),
														"value2": 16
													}),
													"char": " "
												}),
												"string3": functions.concat({
													"string1": ": ",
													"string2": functions.getVarAttr({
														"var": "vPostedSalesInvoiceHeader",
														"attr": "Bill_to_Customer_No"
													}),
													"string3": null,
													"string4": functions.concat({
														"string1": functions.concat({
															"string1": null,
															"string2": functions.getVarAttr({
																"var": "vPostedSalesInvoiceHeader",
																"attr": "Bill_to_Name"
															}),
															"string3": null,
															"string4": ""
														}),
														"string3": functions.conditional({
															"condition": functions.getVarAttr({
																"var": "vPostedSalesInvoiceHeader",
																"attr": "Sell_to_Address"
															}),
															"yesValue": functions.concat({
																"string1": "",
																"string2": functions.getVarAttr({
																	"var": "vPostedSalesInvoiceHeader",
																	"attr": "Bill_to_Address"
																}),
																"string3": null,
																"string4": ""
															}),
															"noValue": "",
															"yesCallback": ""
														})
													})
												}),
												"string4": ""
											}),
											"string3": "",
											"string4": ""
										})
									})
								})
							}),
							"value3": functions.setVar({
								"var": "vContentHeader",
								"value": functions.concat({
									"string1": functions.concat({
										"string1": functions.concat({
											"string1": functions.padString({
												"len": functions.getVar({
													"var": "left_margin"
												}),
												"type": "left",
												"char": " "
											}),
											"string2": null,
											"string3": ""
										}),
										"string2": functions.concat({
											"string1": functions.concat({
												"string1": functions.padString({
													"len": functions.getVar({
														"var": "margin_left"
													}),
													"type": "left",
													"char": " "
												}),
												"string2": functions.padString({
													"len": functions.getVar({
														"var": "width"
													}),
													"type": "left",
													"char": "-"
												}),
												"string3": null
											}),
											"string2": functions.concat({
												"string1": functions.concat({
													"string1": functions.padString({
														"len": functions.getVar({
															"var": "left_margin"
														}),
														"type": "left",
														"char": " "
													}),
													"string2": functions.padString({
														"len": 3,
														"type": "left",
														"string": "No"
													}),
													"string3": functions.padString({
														"string": "Description",
														"len": 12,
														"type": "left"
													}),
													"string4": functions.concat({
														"string1": functions.padString({
															"string": "Price",
															"len": 6,
															"type": "left",
															"char": ""
														}),
														"string2": functions.padString({
															"char": "",
															"string": "UOM",
															"len": 4,
															"type": "left"
														}),
														"string3": functions.padString({
															"string": "Qty.",
															"len": 9,
															"type": "left",
															"char": ""
														}),
														"string4": functions.concat({
															"string1": functions.padString({
																"type": "left",
																"char": "",
																"string": "Amount",
																"len": 10
															}),
															"string2": null
														})
													})
												}),
												"string2": functions.concat({
													"string1": functions.padString({
														"len": functions.getVar({
															"var": "margin_left"
														}),
														"type": "left",
														"char": " "
													}),
													"string2": functions.padString({
														"char": "-",
														"len": functions.getVar({
															"var": "width"
														}),
														"type": "left"
													}),
													"string3": null
												})
											})
										})
									}),
									"string2": "",
									"string3": "",
									"string4": ""
								})
							}),
							"value4": functions.setVar({
								"var": "vContentBody",
								"value": ""
							}),
							"value5": functions.setVar({
								"var": "vInvoicedLineCounter",
								"value": 1
							}),
							"value6": functions.setVar({
								"var": "vFooterTotal",
								"value": functions.concat({
									"string1": null,
									"string2": null,
									"string3": functions.concat({
										"string1": functions.padString({
											"string": functions.concat({
												"string1": "Sub-Total",
												"string2": ""
											}),
											"len": 27,
											"type": "right",
											"char": ""
										}),
										"string2": functions.padString({
											"type": "right",
											"string": functions.concat({
												"string1": functions.formatNumber({
													"value": functions.getVarAttr({
														"var": "vPostedSalesInvoiceLineFirst",
														"attr": "Total_Amount_Excl_VAT"
													}),
													"decimals": 2,
													"decimalSep": ".",
													"thousandSep": ","
												}),
												"string2": ""
											}),
											"len": 11
										}),
										"string3": "",
										"string4": ""
									}),
									"string4": functions.concat({
										"string1": null,
										"string2": functions.concat({
											"string1": functions.padString({
												"string": functions.concat({
													"string1": "GST ",
													"string2": functions.padString({
														"len": 3
													}),
													"string3": functions.getVar({
														"var": "vGSTValue"
													}),
													"string4": "%"
												}),
												"len": 27,
												"type": "right",
												"char": ""
											}),
											"string2": functions.padString({
												"string": functions.concat({
													"string1": functions.formatNumber({
														"value": functions.getVarAttr({
															"var": "vPostedSalesInvoiceLineFirst",
															"attr": "Total_VAT_Amount"
														}),
														"decimals": 2,
														"decimalSep": ".",
														"thousandSep": ","
													}),
													"string2": ""
												}),
												"len": 11,
												"type": "right"
											})
										}),
										"string3": null,
										"string4": functions.concat({
											"string1": functions.padString({
												"string": " ",
												"len": 18,
												"type": "left"
											}),
											"string2": "[size='18pt'][b]",
											"string3": functions.concat({
												"string1": functions.concat({
													"string1": "TOTAL",
													"string2": ""
												}),
												"string2": functions.padString({
													"string": functions.concat({
														"string1": functions.formatNumber({
															"value": functions.getVarAttr({
																"var": "vPostedSalesInvoiceLineFirst",
																"attr": "Total_Amount_Incl_VAT"
															}),
															"decimals": 2,
															"decimalSep": ".",
															"thousandSep": ","
														}),
														"string2": ""
													}),
													"len": 14,
													"type": "right"
												}),
												"string3": "",
												"string4": ""
											}),
											"string4": "[/b][/size]"
										})
									})
								})
							}),
							"value7": functions.setVar({
								"var": "vFooterText",
								"value": functions.concat({
									"string1": functions.concat({
										"string1": null,
										"string2": null,
										"string3": null,
										"string4": ""
									})
								})
							}),
							"value8": functions.setVar({
								"var": "vFooterLast",
								"value": functions.concat({
									"string1": functions.concat({
										"string1": functions.padString({
											"string": "",
											"len": functions.getVar({
												"var": "left_Margin"
											}),
											"type": "left"
										}),
										"string2": functions.padString({
											"string": "",
											"len": 25,
											"type": "left",
											"char": "-"
										}),
										"string3": ""
									}),
									"string2": functions.concat({
										"string1": functions.concat({
											"string1": null,
											"string2": functions.padString({
												"string": "SIGNATURE & COMPANY CHOP",
												"len": functions.getVar({
													"var": "width"
												}),
												"type": "left"
											}),
											"string3": "",
											"string4": ""
										})
									}),
									"string3": functions.concat({
										"string1": functions.concat({
											"string1": null,
											"string2": null,
											"string3": functions.concat({
												"string1": functions.padString({
													"string": "THIS IS A COMPUTER GENERATED INVOICE",
													"len": functions.getVar({
														"var": "width"
													}),
													"type": "left"
												}),
												"string2": null,
												"string3": functions.padString({
													"string": "NO SIGNATURE REQUIRED.",
													"len": functions.getVar({
														"var": "width"
													}),
													"type": "left"
												}),
												"string4": functions.concat({
													"string1": null,
													"string2": null
												})
											}),
											"string4": ""
										})
									}),
									"string4": functions.concat({
										"string1": functions.concat({
											"string1": functions.padString({
												"len": functions.getVar({
													"var": "margin_left"
												}),
												"char": " "
											}),
											"string2": functions.padString({
												"string": functions.concat({
													"string1": "Re-Printed : ",
													"string2": functions.formatDate({
														"date": functions.dbDate({
															"withTime": true
														}),
														"format": "d/m/Y H:i:s "
													})
												}),
												"len": functions.getVar({
													"var": "width"
												}),
												"type": "left"
											}),
											"string3": null,
											"string4": ""
										}),
										"string2": "",
										"string3": ""
									})
								})
							}),
							"value9": functions.setVar({
								"var": "vFooterSignature",
								"value": functions.concat({
									"string1": "[img|x=10|y=10|width=200|height=100]",
									"string2": functions.getVar({
										"var": "vSignature"
									}),
									"string3": "[/img]",
									"string4": functions.concat({
										"string1": null,
										"string2": null,
										"string3": null,
										"string4": functions.concat({
											"string1": null,
											"string2": "",
											"string3": "",
											"string4": ""
										})
									})
								})
							}),
							"value10": functions.setVar({
								"var": "vInvoicedLineCount",
								"value": functions.count({
									"values": functions.getVar({
										"var": "vInvoicedLines"
									})
								})
							})
						});
						functions.setVar({
							"var": "vInvoicedLinesCount",
							"value": functions.count({
								"values": functions.getVar({
									"var": "vInvoicedLines"
								})
							})
						});
						functions.conditional({
							"condition": functions.greater({
								"value1": functions.getVar({
									"var": "vInvoicedLinesCount"
								}),
								"value2": 1
							}),
							"yesCallback": async (data, extra) => {
								functions.map({
									"values": functions.getVar({
										"var": "vInvoicedLines"
									}),
									"callback": async (data, extra) => {
										functions.setVar({
											"var": "mapInvoicedLines",
											"value": data
										});
										functions.setVar({
											"var": "vContentBody",
											"value": functions.concat({
												"string1": functions.getVar({
													"var": "vContentBody"
												}),
												"string2": functions.padString({
													"len": functions.getVar({
														"var": "left_margin"
													}),
													"type": "left",
													"char": " "
												}),
												"string3": functions.concat({
													"string1": "",
													"string2": functions.concat({
														"string1": null,
														"string2": " - ",
														"string3": functions.conditional({
															"condition": functions.equal({
																"value1": functions.getVarAttr({
																	"var": "mapInvoicedLines",
																	"attr": "No"
																}),
																"value2": "510-1015"
															}),
															"yesValue": "Empty Bottles",
															"noValue": functions.getVarAttr({
																"var": "mapInvoicedLines",
																"attr": "Description"
															})
														})
													}),
													"string3": ""
												}),
												"string4": functions.concat({
													"string1": functions.concat({
														"string1": null,
														"string2": functions.padString({
															"len": 6,
															"type": "left",
															"char": " "
														}),
														"string3": functions.absolute({
															"value": functions.getVarAttr({
																"var": "mapInvoicedLines",
																"attr": "Quantity"
															})
														}),
														"string4": functions.concat({
															"string1": functions.padString({
																"string": "",
																"len": 1,
																"type": "left",
																"char": " "
															}),
															"string2": functions.padString({
																"string": functions.conditional({
																	"condition": functions.equal({
																		"value1": functions.getVarAttr({
																			"var": "mapInvoicedLines",
																			"attr": "No"
																		}),
																		"value2": "510-1015"
																	}),
																	"yesValue": "PCS",
																	"noValue": functions.getVarAttr({
																		"var": "mapInvoicedLines",
																		"attr": "Unit_of_Measure"
																	})
																}),
																"len": 8,
																"type": "left",
																"char": ""
															}),
															"string3": functions.padString({
																"string": "$ ",
																"len": 12,
																"type": "right",
																"char": " "
															}),
															"string4": ""
														})
													}),
													"string2": functions.concat({
														"string1": functions.padString({
															"char": " ",
															"string": functions.formatNumber({
																"value": functions.getVarAttr({
																	"var": "mapInvoicedLines",
																	"attr": "Line_Amount"
																}),
																"decimals": 2,
																"decimalSep": ".",
																"thousandSep": ","
															}),
															"len": 10,
															"type": "right"
														})
													}),
													"string3": null,
													"string4": null
												})
											})
										});
										functions.setVar({
											"var": "vInvoicedLineCounter",
											"value": functions.add({
												"value1": functions.getVar({
													"var": "vInvoicedLineCounter"
												}),
												"value2": 1
											})
										});
										functions.conditional({
											"condition": functions.greater({
												"value2": functions.getVar({
													"var": "vInvoicedLineCount"
												}),
												"value1": functions.getVar({
													"var": "vInvoicedLineCounter"
												})
											}),
											"yesCallback": async (data, extra) => {
												functions.setVar({
													"var": "vOutput",
													"value": functions.concat({
														"string1": functions.getVar({
															"var": "vHeader"
														}),
														"string2": functions.getVar({
															"var": "vSubHeader"
														}),
														"string3": functions.concat({
															"string1": functions.concat({
																"string1": null,
																"string2": functions.padString({
																	"len": functions.getVar({
																		"var": "width"
																	}),
																	"type": "left",
																	"char": "-"
																}),
																"string3": null
															}),
															"string2": null,
															"string3": functions.concat({
																"string1": null,
																"string2": functions.padString({
																	"len": functions.getVar({
																		"var": "width"
																	}),
																	"type": "left",
																	"char": "-"
																}),
																"string3": null
															})
														}),
														"string4": functions.concat({
															"string1": functions.getVar({
																"var": "vContentBody"
															}),
															"string2": "",
															"string3": functions.getVar({
																"var": "vFooterTotal"
															}),
															"string4": functions.concat({
																"string1": functions.getVar({
																	"var": "vFooterText"
																}),
																"string2": null,
																"string3": "",
																"string4": ""
															})
														})
													})
												});
												functions.console({
													"value": functions.getVar({
														"var": "vOutput"
													})
												});
											},
											"noCallback": ""
										});
									}
								});
							},
							"noCallback": async (data, extra) => {
								functions.setVar({
									"var": "mapInvoicedLines",
									"value": functions.getVar({
										"var": "vInvoicedLines"
									})
								});
								functions.setVar({
									"var": "vContentBody",
									"value": functions.concat({
										"string1": functions.getVar({
											"var": "vContentBody"
										}),
										"string2": functions.padString({
											"len": functions.getVar({
												"var": "left_margin"
											}),
											"type": "left",
											"char": " "
										}),
										"string3": functions.concat({
											"string1": "",
											"string2": functions.concat({
												"string1": null,
												"string2": " - ",
												"string3": functions.conditional({
													"condition": functions.equal({
														"value1": functions.getVarAttr({
															"var": "mapInvoicedLines",
															"attr": "No"
														}),
														"value2": "510-1015"
													}),
													"yesValue": "Empty Bottles",
													"noValue": functions.getVarAttr({
														"var": "mapInvoicedLines",
														"attr": "Description"
													})
												})
											}),
											"string3": ""
										}),
										"string4": functions.concat({
											"string1": functions.concat({
												"string1": null,
												"string2": functions.padString({
													"char": " ",
													"len": 6,
													"type": "left"
												}),
												"string3": functions.absolute({
													"value": functions.getVarAttr({
														"var": "mapInvoicedLines",
														"attr": "Quantity"
													})
												}),
												"string4": functions.concat({
													"string1": functions.padString({
														"len": 1,
														"type": "left",
														"char": " ",
														"string": ""
													}),
													"string2": functions.padString({
														"string": functions.conditional({
															"condition": functions.equal({
																"value1": functions.getVarAttr({
																	"var": "mapInvoicedLines",
																	"attr": "No"
																}),
																"value2": "510-1015"
															}),
															"yesValue": "PCS",
															"noValue": functions.getVarAttr({
																"var": "mapInvoicedLines",
																"attr": "Unit_of_Measure"
															})
														}),
														"len": 8,
														"type": "left",
														"char": ""
													}),
													"string3": functions.padString({
														"type": "right",
														"char": " ",
														"string": "$ ",
														"len": 12
													}),
													"string4": ""
												})
											}),
											"string2": functions.concat({
												"string1": functions.padString({
													"string": functions.formatNumber({
														"value": functions.getVarAttr({
															"var": "mapInvoicedLines",
															"attr": "Line_Amount"
														}),
														"decimals": 2,
														"decimalSep": ".",
														"thousandSep": ","
													}),
													"len": 10,
													"type": "right",
													"char": " "
												})
											}),
											"string3": null,
											"string4": null
										})
									})
								});
								functions.setVar({
									"var": "vOutput",
									"value": functions.concat({
										"string1": functions.getVar({
											"var": "vHeader"
										}),
										"string2": functions.getVar({
											"var": "vSubHeader"
										}),
										"string3": functions.concat({
											"string1": functions.concat({
												"string1": null,
												"string2": functions.padString({
													"len": functions.getVar({
														"var": "width"
													}),
													"type": "left",
													"char": "-"
												}),
												"string3": null
											}),
											"string2": null,
											"string3": functions.concat({
												"string1": null,
												"string2": functions.padString({
													"len": functions.getVar({
														"var": "width"
													}),
													"type": "left",
													"char": "-"
												}),
												"string3": null
											})
										}),
										"string4": functions.concat({
											"string1": functions.getVar({
												"var": "vContentBody"
											}),
											"string2": "",
											"string3": functions.getVar({
												"var": "vFooterTotal"
											}),
											"string4": functions.concat({
												"string1": functions.getVar({
													"var": "vFooterText"
												}),
												"string2": null,
												"string3": "",
												"string4": ""
											})
										})
									})
								});
								functions.console({
									"value": functions.getVar({
										"var": "vOutput"
									})
								});
							}
						});
						functions.toArray({
							"value1": "",
							"value2": functions.setVar({
								"var": "vOutput2",
								"value": functions.concat({
									"string1": functions.getVar({
										"var": "vFooterSignature"
									}),
									"string2": functions.getVar({
										"var": "vFooterLast"
									}),
									"string3": functions.concat({
										"string1": null,
										"string2": null
									}),
									"string4": ""
								})
							}),
							"value3": functions.console({
								"value": functions.getVar({
									"var": "vOutput2"
								})
							})
						});
						functions.setVar({
							"var": "vTempLang",
							"value": functions.getLanguage({})
						});
						functions.setLanguage({
							"lang": "English"
						});
						functions.setLanguage({
							"lang": functions.getVar({
								"var": "vTempLang"
							})
						});
						functions.console({
							"value": "************ END RECEIPT *********"
						});
						functions.userDefined('globalModalInfo', {
							"title": "Re-print",
							"message": "Re-print receipt completed, please wait."
						});
					},
					"errCallback": async (data, extra) => {
						functions.userDefined('globalModalInfo', {
							"title": "Connection Error",
							"message": "There's an error connecting to Navision Web Services (Load Company Information)"
						});
						functions.userDefined('globalModalHide', {});
					}
				});
			},
			"errCallback": async (data, extra) => {
				functions.console({
					"value": data["err"]
				});
				functions.userDefined('globalModalInfo', {
					"title": "Connection Error",
					"message": "There's an error connecting to Navision Web Services (Get Signature)"
				});
				functions.userDefined('globalModalHide', {});
			}
		});
	},

	handlePnlModalBodyButtonPositiveClick: async () => {
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "vAction"
				}),
				"value2": "updateDraft"
			}),
			"yesCallback": async (data, extra) => {
				functions.setVar({
					"var": "vAction"
				});
				functions.userDefined('globalModalLoading', {});
			},
			"noCallback": async (data, extra) => {
				functions.conditional({
					"condition": functions.equal({
						"value1": functions.getVar({
							"var": "vAction"
						}),
						"value2": "convertToSO"
					}),
					"yesCallback": async (data, extra) => {
						functions.conditional({
							"condition": true,
							"yesCallback": async (data, extra) => {
								functions.setVar({
									"var": "vAction"
								});
								functions.userDefined('globalModalLoading', {});
							},
							"noCallback": ""
						});
					},
					"noCallback": async (data, extra) => {
						functions.conditional({
							"condition": functions.equal({
								"value1": functions.getVar({
									"var": "vAction"
								}),
								"value2": "redirect"
							}),
							"yesCallback": async (data, extra) => {
								functions.gotoPage({
									"p": "pgSalesDocument"
								});
								functions.setVar({
									"var": "vAction"
								});
							},
							"noCallback": async (data, extra) => {
								functions.conditional({
									"condition": functions.equal({
										"value2": "deleteLine",
										"value1": functions.getVar({
											"var": "vAction"
										})
									}),
									"yesCallback": async (data, extra) => {
										await functions.deleteBy({
											"dataset": "l_salesDocumentLines",
											"by": "ItemCode",
											"value": functions.getVarAttr({
												"var": "vItemDetails",
												"attr": "ItemCode"
											}),
											"callback": null,
											"errCallback": null
										});
									},
									"noCallback": async (data, extra) => {
										functions.conditional({
											"condition": functions.equal({
												"value1": functions.getVar({
													"var": "vAction"
												}),
												"value2": "updateResubmit"
											}),
											"yesCallback": async (data, extra) => {
												functions.setVar({
													"var": "vAction"
												});
												functions.userDefined('globalModalLoading', {});
											},
											"noCallback": async (data, extra) => {
												functions.conditional({
													"condition": functions.equal({
														"value1": functions.getVar({
															"var": "vAction"
														}),
														"value2": "updateSO"
													}),
													"yesCallback": async (data, extra) => {
														functions.setVar({
															"var": "vAction"
														});
														functions.userDefined('globalModalLoading', {});
													},
													"noCallback": async (data, extra) => {
														functions.conditional({
															"condition": functions.equal({
																"value1": functions.getVar({
																	"var": "vAction"
																}),
																"value2": "copyOrder"
															}),
															"yesCallback": async (data, extra) => {
																functions.console({
																	"value": 111
																});
																functions.setVar({
																	"var": "vCurrCustomer",
																	"value": functions.toObject({
																		"CardCode": functions.getVarAttr({
																			"var": "vSalesDocument",
																			"attr": "CardCode"
																		}),
																		"Rate": functions.multi({
																			"value1": functions.div({
																				"value1": functions.getVarAttr({
																					"var": "vSalesDocument",
																					"attr": "VatSum"
																				}),
																				"value2": functions.sub({
																					"value1": functions.getVarAttr({
																						"var": "vSalesDocument",
																						"attr": "DocTotal"
																					}),
																					"value2": functions.getVarAttr({
																						"var": "vSalesDocument",
																						"attr": "VatSum"
																					})
																				})
																			}),
																			"value2": 100
																		}),
																		"CardName": functions.getVarAttr({
																			"var": "vSalesDocument",
																			"attr": "CardName"
																		})
																	})
																});
															},
															"noCallback": async (data, extra) => {
																functions.setVar({
																	"var": "vAction"
																});
																functions.userDefined('globalModalHide', {});
															}
														});
													}
												});
											}
										});
									}
								});
							}
						});
					}
				});
			}
		});
	},

	handlePnlModalBodyButtonNegativeClick: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalHide', {});
	},

	handlePanel37680600Click: async () => {
		functions.setVar({
			"var": "vSettingCount",
			"value": functions.count({
				"values": await functions.selectAll({
					"dataset": "l_settings",
					"callback": "",
					"errCallback": null
				})
			})
		});
		functions.conditional({
			"condition": functions.getVar({
				"var": "vSettingCount"
			}),
			"yesCallback": async (data, extra) => {
				functions.console({
					"value": "update..."
				});
				await functions.updateBy({
					"data": functions.toObject({
						"Printer": null
					}),
					"callback": null,
					"errCallback": null,
					"dataset": "l_settings",
					"by": "_id",
					"operator": "=",
					"value": 1
				});
			},
			"noCallback": async (data, extra) => {
				functions.console({
					"value": "insert..."
				});
				await functions.insert({
					"dataset": "l_settings",
					"dt": functions.toObject({
						"Printer": null
					})
				});
			}
		});
		functions.setVar({
			"var": "vPrinter",
			"value": functions.objectAttr({
				"object": await functions.selectBy({
					"dataset": "l_settings",
					"by": "_id",
					"value": 1,
					"first": true,
					"callback": null,
					"errCallback": null
				}),
				"attr": "Printer"
			})
		});
		functions.hideElement({
			"component": "pnlPrinter"
		});
	},

	handlePgOrderHistoryDetailsLoad: async () => {
		functions.userDefined('globalModalLoading', {});
		functions.onBackButton({
			"callback": async (data, extra) => {
				functions.gotoPage({
					"p": "pgOrderHistory"
				});
			}
		});
		functions.setVar({
			"var": "vAction",
			"value": ""
		});
		functions.toArray({
			"value1": functions.setComponentValue({
				"component": "lblOrderNo_data",
				"componentId": "",
				"value": functions.getVarAttr({
					"var": "vPostedSalesInvoiceHeader",
					"attr": "No",
					"default": ""
				})
			}),
			"value2": functions.setComponentValue({
				"component": "lblCustomerCodeName_data",
				"componentId": "",
				"value": functions.concat({
					"string1": functions.getVarAttr({
						"var": "vPostedSalesInvoiceHeader",
						"attr": "Bill_to_Customer_No"
					}),
					"string2": " - ",
					"string3": functions.getVarAttr({
						"var": "vPostedSalesInvoiceHeader",
						"attr": "Bill_to_Name"
					})
				})
			}),
			"value3": functions.setComponentValue({
				"component": "lblAddress_data",
				"componentId": "",
				"value": functions.getVarAttr({
					"var": "vPostedSalesInvoiceHeader",
					"attr": "Bill_to_Address"
				})
			}),
			"value4": functions.setComponentValue({
				"component": "lblOrderDate_data",
				"componentId": "",
				"value": functions.formatDate({
					"date": functions.getVarAttr({
						"var": "vPostedSalesInvoiceHeader",
						"attr": "Posting_Date"
					}),
					"format": "d-m-Y"
				})
			}),
			"value5": functions.setComponentValue({
				"component": "lblRemarks",
				"componentId": "",
				"value": functions.getVarAttr({
					"var": "vPostedSalesInvoiceHeader",
					"attr": "Remarks"
				})
			}),
			"value6": "",
			"value7": "",
			"value8": "",
			"value9": "",
			"value10": ""
		});
		functions.toArray({
			"value1": functions.setComponentValue({
				"component": "lblSubtotal_data",
				"componentId": "",
				"value": functions.concat({
					"string1": "$ ",
					"string2": functions.formatNumber({
						"decimals": 2,
						"decimalSep": ".",
						"thousandSep": ",",
						"value": functions.getVarAttr({
							"var": "vPostedSalesInvoiceHeader",
							"attr": "Amount_OkFldSls",
							"default": ""
						})
					}),
					"string3": "",
					"string4": ""
				})
			}),
			"value2": functions.setComponentValue({
				"component": "lblGST_data",
				"componentId": "",
				"value": functions.concat({
					"string1": "$ ",
					"string2": functions.formatNumber({
						"value": functions.sub({
							"value1": functions.getVarAttr({
								"var": "vPostedSalesInvoiceHeader",
								"attr": "Amount_Including_VAT_OkFldSls",
								"default": ""
							}),
							"value2": functions.getVarAttr({
								"var": "vPostedSalesInvoiceHeader",
								"attr": "Amount_OkFldSls",
								"default": ""
							})
						}),
						"decimals": 2,
						"decimalSep": ".",
						"thousandSep": ","
					}),
					"string3": "",
					"string4": ""
				})
			}),
			"value3": functions.setComponentValue({
				"component": "lblTotalAmount_data",
				"componentId": "",
				"value": functions.concat({
					"string1": "$ ",
					"string2": functions.formatNumber({
						"value": functions.getVarAttr({
							"var": "vPostedSalesInvoiceHeader",
							"attr": "Amount_Including_VAT_OkFldSls",
							"default": ""
						}),
						"decimals": 2,
						"decimalSep": ".",
						"thousandSep": ","
					}),
					"string3": "",
					"string4": ""
				})
			}),
			"value4": "",
			"value5": "",
			"value6": "",
			"value7": "",
			"value8": "",
			"value9": "",
			"value10": ""
		});
		functions.conditional({
			"condition": functions.getVarAttr({
				"var": "vPostedSalesInvoiceHeader",
				"attr": "Remarks"
			}),
			"yesCallback": async (data, extra) => {
				functions.showElement({
					"component": "pnlMainBodyFormRemarks"
				});
			},
			"noCallback": async (data, extra) => {
				functions.hideElement({
					"component": "pnlMainBodyFormRemarks"
				});
			}
		});
		await functions.loadData({
			"dataset": "l_OrderLines",
			"filter": functions.toArray({
				"value1": functions.toObject({
					"f": "Type",
					"v": "Item"
				})
			}),
			"callback": async (data, extra) => {
				functions.console({
					"value": 1
				});
				await functions.loadData({
					"dataset": "l_OrderEmptyLines",
					"filter": "",
					"callback": async (data, extra) => {
						functions.console({
							"value": 2
						});
						functions.setVar({
							"var": "vItemNo",
							"value": functions.objectAttr({
								"object": await functions.selectBy({
									"first": true,
									"callback": null,
									"errCallback": null,
									"dataset": "l_OrderLines",
									"by": "_id",
									"value": 1
								}),
								"attr": "No"
							})
						});
						await functions.loadData({
							"filter": functions.toArray({
								"value1": functions.toObject({
									"v": functions.objectAttr({
										"object": await functions.selectBy({
											"dataset": "l_OrderLines",
											"by": "_id",
											"value": 1,
											"first": true,
											"callback": null,
											"errCallback": null
										}),
										"attr": "No"
									}),
									"f": "No"
								})
							}),
							"callback": async (data, extra) => {
								functions.console({
									"value": 3
								});
								functions.setVar({
									"var": "vHistoryItemData",
									"value": functions.objectAttr({
										"object": data,
										"attr": 0
									})
								});
								await functions.loadData({
									"callback": async (data, extra) => {
										functions.console({
											"value": 4
										});
										functions.console({
											"value": data
										});
										functions.console({
											"value": "result"
										});
										functions.setVar({
											"var": "vHistoryCustomerData",
											"value": functions.objectAttr({
												"object": data,
												"attr": 0
											})
										});
										await functions.loadData({
											"callback": async (data, extra) => {
												functions.console({
													"value": 5
												});
												functions.console({
													"value": "gst rate"
												});
												functions.console({
													"value": functions.objectAttr({
														"object": functions.objectAttr({
															"object": data,
															"attr": 0
														}),
														"attr": "VAT_Percent"
													})
												});
												functions.setVar({
													"var": "vGstTotalRate",
													"value": functions.objectAttr({
														"object": functions.objectAttr({
															"object": data,
															"attr": 0
														}),
														"attr": "VAT_Percent"
													})
												});
												functions.setComponentValue({
													"component": "lblGSTValue",
													"value": functions.concat({
														"string1": "GST",
														"string2": " ",
														"string3": functions.getVar({
															"var": "vGstTotalRate"
														}),
														"string4": "%"
													})
												});
												functions.userDefined('globalModalHide', {});
											},
											"errCallback": async (data, extra) => {
												functions.setVar({
													"var": "error",
													"value": data["err"]
												});
												functions.userDefined('globalModalHide', {});
											},
											"dataset": "OK365_GST_Setup",
											"filter": functions.toArray({
												"value1": functions.toObject({
													"f": "VAT_Prod_Posting_Group",
													"v": functions.getVarAttr({
														"var": "vHistoryItemData",
														"attr": "VAT_Prod_Posting_Group"
													})
												}),
												"value2": functions.toObject({
													"f": "VAT_Bus_Posting_Group",
													"v": functions.getVarAttr({
														"var": "vHistoryCustomerData",
														"attr": "VAT_Bus_Posting_Group"
													})
												})
											})
										});
									},
									"errCallback": async (data, extra) => {
										functions.setVar({
											"var": "error",
											"value": data["err"]
										});
										functions.userDefined('globalModalInfo', {
											"title": "Error",
											"message": functions.conditional({
												"condition": functions.getVarAttr({
													"var": "error",
													"attr": "global"
												}),
												"yesValue": functions.getVarAttr({
													"var": "error",
													"attr": "global"
												}),
												"noValue": "Cannot connect to Navision Server."
											})
										});
									},
									"dataset": "OK365_Customer_Card",
									"filter": functions.toArray({
										"value1": functions.toObject({
											"o": "=",
											"v": functions.getVarAttr({
												"var": "vPostedSalesInvoiceHeader",
												"attr": "Bill_to_Customer_No_OkFldSls"
											}),
											"f": "No"
										})
									})
								});
							},
							"errCallback": async (data, extra) => {
								functions.console({
									"value": "Error Load: MSC_Item_Card"
								});
							},
							"dataset": "OK365_Item_Card",
							"limit": 1
						});
					},
					"errCallback": null
				});
			},
			"errCallback": ""
		});
	}
};

const PgOrderHistoryDetails = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;
	formatParentStyleLayout(props);

	useEffect(() => {
		actions.handlePgOrderHistoryDetailsLoad();
	}, [currentTime]);
    
    return (
		<Page data={props.pgOrderHistoryDetails}>
			<Panel data={props.pnlHolder}>
				<Panel data={props.pnlMain}>
					<Panel data={props.pnlMainHeader}>
						<Panel data={props.pnlMainHeaderLeft}>
							<Panel data={props.pnlMainHeaderLeftTable}>
								<Panel data={props.pnlMainHeaderLeftCell}>
									<Image data={props.imgBack} action={actions.handleImgBackClick} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderMiddle}>
							<Panel data={props.pnlMainHeaderMiddleTable}>
								<Panel data={props.pnlMainHeaderMiddleCell}>
									<Label data={props.lblMainTitle} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderRight233}>
							<Panel data={props.pnlMainHeaderRightTable2}>
								<Panel data={props.pnlMainHeaderRightCell877}>
									<Image data={props.imgBack555407} action={actions.handleImgBack555407Click} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBody}>
						<Panel data={props.pnlMainBodyForm}>
							<Panel data={props.pnlMainBodyFormCustomerCode}>
								<Panel data={props.pnlMainBodyForm3LabelCustomerCode}>
									<Label data={props.lblOrderNo_data} />
									<Label data={props.lblCustomerCodeName_data} />
									<Label data={props.lblAddress_data} />
									<Label data={props.lblOrderDate_data} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainBodyItemsCO1}>
							<Label data={props.lblItems} />
						</Panel>
						<Panel data={props.pnlMainBodyItems}>
							<Panel data={props.pnlMainBodyItemsCO2}>
								<DataList data={props.dlltem} functions={functions} currentTime={currentTime}>
									{({ item, parentStyle }) => (								
										<Panel data={item.pnlProduct} parentStyle={parentStyle}>
											<Panel data={item.pnlProductBodyLeft} parentStyle={parentStyle}>
												<Image data={item.imgItem} parentStyle={parentStyle} />
											</Panel>
											<Panel data={item.pnlProductBodyMiddle} parentStyle={parentStyle}>
												<Panel data={item.pnlProductBodyMiddle1} parentStyle={parentStyle}>
													<Label data={item.lblItemCode_data} parentStyle={parentStyle} />
													<Label data={item.lblItemName_data} parentStyle={parentStyle} />
													<Label data={item.lblItemsQtyUOM} parentStyle={parentStyle} />
													<Label data={item.lblPrice_data} parentStyle={parentStyle} />
													<Label data={item.lblDiscCO} parentStyle={parentStyle} />
												</Panel>
											</Panel>
										</Panel>
									)}
								</DataList>
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBodyDetails}>
						<Panel data={props.pnlSubtotal}>
							<Label data={props.lblSubTotal} />
							<Label data={props.lblSubtotal_data} />
						</Panel>
						<Panel data={props.pnlDetailsDiscount}>
							<Label data={props.lblGSTValue} />
							<Label data={props.lblGST_data} />
						</Panel>
						<Panel data={props.pnlMainBodyDetails1}>
							<Label data={props.lblTotalAmountExclGST} />
							<Label data={props.lblTotalAmount_data} />
						</Panel>
					</Panel>
					<Panel data={props.pnlMainFooter}>
						<Panel data={props.pnlMainFooterTbl}>
							<Panel data={props.pnlMainFooterTblCell1} action={actions.handlePnlMainFooterTblCell1Click}>
								<Label data={props.lblBtnOrder} />
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal}>
					<Panel data={props.pnlModalTable}>
						<Panel data={props.pnlModalCell}>
							<Panel data={props.pnlModalBodyMain}>
								<Panel data={props.pnlModalBodyTitle}>
									<Label data={props.lblModalTitle} />
								</Panel>
								<Panel data={props.pnlModalBodyMessage}>
									<Label data={props.lblModalMessage} />
								</Panel>
								<Panel data={props.pnlModalBodyLoading}>
									<Image data={props.imgModalLoading} />
								</Panel>
							</Panel>
							<Panel data={props.pnlModalBodyButtons}>
								<Panel data={props.pnlModalBodyButtonPositive} action={actions.handlePnlModalBodyButtonPositiveClick}>
									<Label data={props.lblModalPositive} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonNegative} action={actions.handlePnlModalBodyButtonNegativeClick}>
									<Label data={props.lblModalNegative} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlCSS}>
					<Label data={props.lblStyle} />
				</Panel>
				<Panel data={props.pnlPrinter}>
					<Panel data={props.pnlConfirmPaymentMethodDetailsTable695203598148}>
						<Panel data={props.pnlConfirmPaymentMethodDetailsCell206351}>
							<Panel data={props.pnlConfirmPaymentMethodDetailsBody264652}>
								<Panel data={props.Panel103885987}>
									<Panel data={props.pnlMainBodypnlConfirmPaymentMethodDetailsSign105386}>
										<Label data={props.lblSelectPrinter} />
										<ComboBox data={props.cmbPrinter} functions={functions} />
									</Panel>
								</Panel>
								<Panel data={props.pnlUpdateCartModalBody589675}>
									<Panel data={props.Panel37680600} action={actions.handlePanel37680600Click}>
										<Label data={props.Label894367403} />
									</Panel>
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgOrderHistoryDetails);