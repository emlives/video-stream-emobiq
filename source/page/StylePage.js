import React from 'react';
import { connect } from 'react-redux';

import Page from '../framework/component/Page';
import Label from '../framework/component/Label';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';

import { StateName } from '../reducer/data/StylePage';

const StylePage = (props) => {
    return (
        <Page data={props.page} allProps={props}>
            <Label data={props.label276}></Label>
            <Label data={props.label146}></Label>
            <Label data={props.label141}></Label>
            <Label data={props.label343}></Label>
            <Image data={props.Image387}></Image>
        </Page>
    )
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] }
};

export default connect(mapStateToProps)(StylePage);