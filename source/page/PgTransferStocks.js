import React, { useEffect }  from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgTransferStocks';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import Edit from '../framework/component/Edit';
import DataList from '../framework/component/DataList';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handleImgMenuClick: async () => {
		functions.gotoPage({
			"p": "pgMainMenu"
		});
	},

	handleLblReturnClick: async () => {
		functions.setVar({
			"var": "vAction",
			"value": "new_return_order"
		});
		functions.userDefined('globalModalQuestion', {
			"title": "Return Order",
			"message": "Do you want to create new Return Order?"
		});
	},

	handlePnlDataContentTblCell2Click: async () => {
		functions.userDefined('globalModalLoading', {});
		functions.setVar({
			"var": "v_transfer_order_selected",
			"value": data
		});
		await functions.loadData({
			"dataset": "OK365_Transfer_Orders",
			"filter": functions.toArray({
				"value1": functions.toObject({
					"f": "No",
					"v": functions.getVarAttr({
						"var": "v_transfer_order_selected",
						"attr": "No"
					})
				})
			}),
			"callback": async (data, extra) => {
				functions.setVar({
					"var": "v_transfer_order_data",
					"value": functions.objectAttr({
						"object": data,
						"attr": 0
					})
				});
				functions.setVar({
					"var": "v_transfer_order_lines_data",
					"value": functions.objectAttr({
						"object": functions.objectAttr({
							"object": functions.objectAttr({
								"object": data,
								"attr": 0
							}),
							"attr": "TransferLines"
						}),
						"attr": "OK365_Transfer_Line_OkFldSls"
					})
				});
				await functions.clearData({
					"dataset": "l_TransferLines",
					"callback": "",
					"errCallback": null
				});
				await functions.dataFromString({
					"dataset": "l_TransferLines",
					"string": functions.getVar({
						"var": "v_transfer_order_lines_data"
					}),
					"callback": "",
					"errCallback": null
				});
				functions.gotoPage({
					"p": "pgTransferStockDetails"
				});
			},
			"errCallback": async (data, extra) => {
				functions.userDefined('globalModalHide', {});
				functions.userDefined('globalModalInfo', {
					"btnCaption": "OK",
					"title": "Navision Server Error",
					"message": functions.objectAttr({
						"object": functions.objectAttr({
							"object": data,
							"attr": "err"
						}),
						"attr": "global"
					})
				});
			}
		});
	},

	handlePnlModalBodyButtonPositiveClick: async () => {
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "vAction"
				}),
				"value2": "new_return_order"
			}),
			"yesCallback": async (data, extra) => {
				functions.userDefined('globalModalLoading', {});
				functions.setVar({
					"var": "vAction"
				});
				await functions.navCall({
					"data": functions.toObject({
						"Posting_Date": functions.dbDate({}),
						"Transfer_from_Code": functions.objectAttr({
							"object": await functions.selectBy({
								"dataset": "l_Route_Master",
								"by": "_id",
								"value": 1,
								"first": true,
								"callback": null,
								"errCallback": null
							}),
							"attr": "Location_Code"
						}),
						"Transfer_to_Code": "MAIN"
					}),
					"callback": async (data, extra) => {
						functions.console({
							"value": data
						});
						functions.setVar({
							"var": "v_transfer_order_selected",
							"value": functions.objectAttr({
								"object": data,
								"attr": "OK365_Transfer_Orders"
							})
						});
						await functions.loadData({
							"filter": functions.toArray({
								"value1": functions.toObject({
									"f": "No",
									"v": functions.getVarAttr({
										"var": "v_transfer_order_selected",
										"attr": "No"
									})
								})
							}),
							"callback": async (data, extra) => {
								functions.setVar({
									"var": "v_transfer_order_data",
									"value": functions.objectAttr({
										"object": data,
										"attr": 0
									})
								});
								functions.setVar({
									"var": "v_transfer_order_lines_data",
									"value": functions.objectAttr({
										"object": functions.objectAttr({
											"attr": "TransferLines",
											"object": functions.objectAttr({
												"object": data,
												"attr": 0
											})
										}),
										"attr": "OK365_Transfer_Line_Line"
									})
								});
								await functions.clearData({
									"dataset": "l_TransferLines",
									"callback": null,
									"errCallback": null
								});
								functions.gotoPage({
									"p": "pgTransferStockDetails"
								});
							},
							"errCallback": async (data, extra) => {
								functions.userDefined('globalModalHide', {});
								functions.userDefined('globalModalInfo', {
									"title": "Navision Server Error",
									"message": functions.objectAttr({
										"attr": "global",
										"object": functions.objectAttr({
											"object": data,
											"attr": "err"
										})
									}),
									"btnCaption": "OK"
								});
							},
							"dataset": "OK365_Transfer_Orders"
						});
					},
					"errCallback": async (data, extra) => {
						functions.userDefined('globalModalHide', {});
						functions.userDefined('globalModalInfo', {
							"title": "Navision Server Error",
							"message": functions.objectAttr({
								"object": functions.objectAttr({
									"object": data,
									"attr": "err"
								}),
								"attr": "global"
							}),
							"btnCaption": "OK"
						});
					},
					"connector": "nav",
					"ent": "OK365_Transfer_Orders",
					"function": "Create"
				});
			},
			"noCallback": async (data, extra) => {
				functions.setVar({
					"var": "vAction"
				});
				functions.userDefined('globalModalHide', {});
			}
		});
	},

	handlePnlModalBodyButtonNegativeClick: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalHide', {});
	},

	handlePgTransferStocksLoad: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalLoading', {});
		functions.onBackButton({
			"callback": async (data, extra) => {
				functions.gotoPage({
					"p": "pgMainMenu"
				});
			}
		});
		functions.setTimeout({
			"timeout": 100,
			"callback": async (data, extra) => {
				await functions.loadData({
					"filter": functions.toArray({
						"value1": functions.toObject({
							"f": "Status",
							"v": "Open"
						}),
						"value2": ""
					}),
					"orFilter": functions.toArray({
						"value1": functions.toObject({
							"f": "Transfer_from_Code",
							"v": functions.objectAttr({
								"attr": "Location_Code",
								"object": await functions.selectBy({
									"value": 1,
									"first": true,
									"callback": null,
									"errCallback": null,
									"dataset": "l_Route_Master",
									"by": "_id"
								})
							})
						}),
						"value2": functions.toObject({
							"f": "Transfer_to_Code",
							"v": functions.objectAttr({
								"object": await functions.selectBy({
									"errCallback": null,
									"dataset": "l_Route_Master",
									"by": "_id",
									"value": 1,
									"first": true,
									"callback": null
								}),
								"attr": "Location_Code"
							})
						})
					}),
					"order": functions.toArray({
						"value1": functions.toObject({
							"f": "No",
							"v": "desc"
						})
					}),
					"callback": async (data, extra) => {
						functions.console({
							"value": data
						});
						functions.setVar({
							"var": "v_transfer_order_list",
							"value": data
						});
						await functions.dataFromString({
							"dataset": "l_Transfer_Order_List",
							"string": data,
							"callback": "",
							"errCallback": null
						});
						await functions.loadData({
							"filter": "",
							"order": "",
							"callback": async (data, extra) => {
								functions.console({
									"value": data
								});
								functions.console({
									"value": "Retrieved Transfer Order Lists"
								});
								functions.userDefined('globalModalHide', {});
							},
							"errCallback": "",
							"dataset": "l_Transfer_Order_List"
						});
					},
					"errCallback": async (data, extra) => {
						functions.setVar({
							"var": "error",
							"value": data["err"]
						});
						functions.userDefined('globalModalInfo', {
							"title": "Error",
							"message": functions.conditional({
								"condition": functions.getVarAttr({
									"var": "error",
									"attr": "global"
								}),
								"yesValue": functions.getVarAttr({
									"var": "error",
									"attr": "global"
								}),
								"noValue": "Cannot connect to Navision Server."
							})
						});
					},
					"dataset": "OK365_Transfer_Order_List"
				});
			}
		});
	}
};

const PgTransferStocks = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;
	formatParentStyleLayout(props);

	useEffect(() => {
		actions.handlePgTransferStocksLoad();
	}, [currentTime]);
    
    return (
		<Page data={props.pgTransferStocks}>
			<Panel data={props.pnlHolder}>
				<Panel data={props.pnlMain}>
					<Panel data={props.pnlMainHeader}>
						<Panel data={props.pnlMainHeaderLeft}>
							<Panel data={props.pnlMainHeaderLeftTbl}>
								<Panel data={props.pnlMainHeaderLeftTblCell}>
									<Image data={props.imgMenu} action={actions.handleImgMenuClick} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderMiddle}>
							<Panel data={props.pnlMainHeaderMiddleTbl}>
								<Panel data={props.pnlMainHeaderMiddleTblCell}>
									<Label data={props.lblMainTitle} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderRight}>
							<Panel data={props.pnlMainHeaderRightTbl}>
								<Panel data={props.pnlMainHeaderRightTblCell}>
									<Label data={props.lblReturn} action={actions.handleLblReturnClick} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainSearch}>
						<Panel data={props.pnlMainBodySearch}>
							<Edit data={props.txtSearch} />
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBody}>
						<Panel data={props.pnlBody}>
							<Panel data={props.pnlBodyContent}>
								<DataList data={props.dtTransferLists} currentTime={currentTime} functions={functions}>
									{({ item, parentStyle }) => (								
										<Panel data={item.pnlMainDataContent} parentStyle={parentStyle}>
											<Panel data={item.pnlDataContentTbl} parentStyle={parentStyle}>
												<Panel data={item.pnlDataContentTblCell1} parentStyle={parentStyle}>
													<Panel data={item.pnlDataContentNo} parentStyle={parentStyle}>
														<Label data={item.lblDataContentNo_data} parentStyle={parentStyle} />
													</Panel>
													<Panel data={item.pnlDataContentToLocation} parentStyle={parentStyle}>
														<Label data={item.lblDataContentToLocation_data} parentStyle={parentStyle} />
													</Panel>
													<Panel data={item.pnlDataContentPostingDate} parentStyle={parentStyle}>
														<Label data={item.lblDataContentPostingDate_data} parentStyle={parentStyle} />
													</Panel>
													<Panel data={item.pnlDataContentStatus} parentStyle={parentStyle}>
														<Label data={item.lblDataContentStatus_data} parentStyle={parentStyle} />
													</Panel>
												</Panel>
												<Panel data={item.pnlDataContentTblCell2} action={actions.handlePnlDataContentTblCell2Click} parentStyle={parentStyle}>
													<Image data={item.imgArrowDetails} parentStyle={parentStyle} />
												</Panel>
											</Panel>
										</Panel>
									)}
								</DataList>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal}>
					<Panel data={props.pnlModalTable}>
						<Panel data={props.pnlModalCell}>
							<Panel data={props.pnlModalBodyMain}>
								<Panel data={props.pnlModalBodyTitle}>
									<Label data={props.lblModalTitle} />
								</Panel>
								<Panel data={props.pnlModalBodyMessage}>
									<Label data={props.lblModalMessage} />
								</Panel>
								<Panel data={props.pnlModalBodyLoading}>
									<Image data={props.imgModalLoading} />
								</Panel>
							</Panel>
							<Panel data={props.pnlModalBodyButtons}>
								<Panel data={props.pnlModalBodyButtonPositive} action={actions.handlePnlModalBodyButtonPositiveClick}>
									<Label data={props.lblModalPositive} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonNegative} action={actions.handlePnlModalBodyButtonNegativeClick}>
									<Label data={props.lblModalNegative} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlCSS}>
					<Label data={props.lblStyle} />
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgTransferStocks);