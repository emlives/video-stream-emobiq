import React, { useEffect }  from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgSalesReturnDetailsUpdate';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import Edit from '../framework/component/Edit';
import DataList from '../framework/component/DataList';
import Button from '../framework/component/Button';
import ComboBox from '../framework/component/ComboBox';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handlePnlMainHeaderLeftTableClick: async () => {
		functions.setVar({
			"var": "vAction",
			"value": "back_sales_return_details"
		});
		functions.userDefined('globalModalQuestion', {
			"title": "Sales Return Details",
			"message": "Are you sure to go in previous screen, all changes will be lost?"
		});
	},

	handleImage553564Click: async () => {
		functions.gotoPage({
			"p": "pgSelectItem"
		});
	},

	handlePanel489Click: async () => {
		functions.setVar({
			"var": "v_current_item_selected",
			"value": data
		});
		functions.toArray({
			"value1": functions.setComponentValue({
				"component": "imgUpdateQtyItemImage",
				"value": functions.getVarAttr({
					"var": "v_current_item_selected",
					"attr": "GetPicture"
				})
			}),
			"value2": functions.setComponentValue({
				"component": "lblUpdateQtyItemNo",
				"value": functions.getVarAttr({
					"var": "v_current_item_selected",
					"attr": "No"
				})
			}),
			"value3": functions.setComponentValue({
				"component": "lblUpdateQtyItemName",
				"value": functions.getVarAttr({
					"var": "v_current_item_selected",
					"attr": "Description"
				})
			}),
			"value4": functions.setComponentValue({
				"component": "lblUpdateQtyItemUnitPrice",
				"value": functions.getVarAttr({
					"var": "v_current_item_selected",
					"attr": "Unit_Price"
				})
			}),
			"value5": functions.setComponentValue({
				"component": "lblUpdateQtyContentInputUOM",
				"value": functions.getVarAttr({
					"var": "v_current_item_selected",
					"attr": "Unit_of_Measure_Code"
				})
			}),
			"value6": functions.setComponentValue({
				"component": "txtQuantity",
				"value": functions.getVarAttr({
					"var": "v_current_item_selected",
					"attr": "Quantity"
				})
			})
		});
		functions.showElement({
			"component": "pnlUpdateQty"
		});
	},

	handlePanel489504Click: async () => {
		functions.console({
			"value": "pssst"
		});
		functions.setVar({
			"var": "v_current_item_selected",
			"value": data
		});
		functions.showElement({
			"component": "pnlSelectReasonCode"
		});
	},

	handlePanel489850Click: async () => {
		functions.setVar({
			"var": "v_current_item_selected",
			"value": data
		});
		functions.setVar({
			"var": "vAction",
			"value": "remove_item"
		});
		functions.userDefined('globalModalQuestion', {
			"message": functions.concat({
				"string1": "Do want to remove",
				"string2": functions.concat({
					"string1": " ",
					"string2": "<b>",
					"string3": data["No"],
					"string4": functions.concat({
						"string1": " - ",
						"string2": data["Description"],
						"string3": "</b> "
					})
				}),
				"string3": "?"
			}),
			"title": "Remove Item"
		});
	},

	handleBtnSAVE691Click: async () => {
		functions.conditional({
			"condition": functions.greater({
				"value1": functions.count({
					"values": await functions.selectAll({
						"callback": null,
						"errCallback": null,
						"dataset": "l_cart"
					})
				}),
				"value2": 0
			}),
			"yesCallback": async (data, extra) => {
				functions.setVar({
					"var": "vAction",
					"value": "submit_order"
				});
				functions.userDefined('globalModalQuestion', {
					"title": "Submit Order",
					"message": "Are you sure to proceed?"
				});
			},
			"noCallback": async (data, extra) => {
				functions.userDefined('globalModalInfo', {
					"title": "Empty Field",
					"message": "Please add item into a cart."
				});
			}
		});
	},

	handleBtnOrder743881Click: async () => {
		functions.conditional({
			"condition": functions.greater({
				"value1": functions.count({
					"values": await functions.selectAll({
						"callback": null,
						"errCallback": null,
						"dataset": "l_cart"
					})
				}),
				"value2": 0
			}),
			"yesCallback": async (data, extra) => {
				functions.gotoPage({
					"p": "pgSummary"
				});
			},
			"noCallback": async (data, extra) => {
				functions.userDefined('globalModalInfo', {
					"title": "Empty Field",
					"message": "Please add item into a cart."
				});
			}
		});
	},

	handlePnlMainFooterTblCell1865Click: async () => {
		functions.conditional({
			"condition": functions.greater({
				"value2": 0,
				"value1": functions.count({
					"values": await functions.selectAll({
						"dataset": "l_ Sales_Return_Lines",
						"callback": null,
						"errCallback": null
					})
				})
			}),
			"yesCallback": async (data, extra) => {
				functions.setVar({
					"var": "vAction",
					"value": "submit_sales_return"
				});
				functions.userDefined('globalModalQuestion', {
					"title": "Submit Sales Return",
					"message": "Do you want to proceed submit sales return?"
				});
			},
			"noCallback": async (data, extra) => {
				functions.userDefined('globalModalInfo', {
					"title": "No Return Item ",
					"message": "There's no return item to submit, please add return item to continue."
				});
			}
		});
	},

	handlePnlModalBodyButtonPositiveClick: async () => {
		functions.conditional({
			"condition": functions.equal({
				"value2": "delete_item",
				"value1": functions.getVar({
					"var": "vAction"
				})
			}),
			"yesCallback": async (data, extra) => {
				await functions.deleteBy({
					"callback": async (data, extra) => {
						functions.hideElement({
							"component": "pnlModal"
						});
					},
					"errCallback": "",
					"dataset": "l_cart",
					"by": "_id",
					"value": functions.getVarAttr({
						"var": "vItemDetails",
						"attr": "_id"
					})
				});
				functions.toArray({
					"value1": await functions.loadData({
						"dataset": "l_cart",
						"callback": async (data, extra) => {
							functions.setVar({
								"var": "vTotalAmountItem",
								"value": 0
							});
							functions.map({
								"values": await functions.selectAll({
									"dataset": "l_cart",
									"callback": "",
									"errCallback": null
								}),
								"callback": async (data, extra) => {
									functions.console({
										"value": data
									});
									functions.setVar({
										"var": "vItemDetails_Order",
										"value": data
									});
									functions.setVar({
										"var": "item_amount",
										"value": functions.multi({
											"value1": functions.getVarAttr({
												"var": "vItemDetails_Order",
												"attr": "Quantity"
											}),
											"value2": functions.getVarAttr({
												"var": "vItemDetails_Order",
												"attr": "Price"
											})
										})
									});
									functions.setVar({
										"var": "vTotalAmountItem",
										"value": functions.add({
											"value1": functions.getVar({
												"var": "vTotalAmountItem",
												"default": ""
											}),
											"value2": functions.getVar({
												"var": "item_amount",
												"default": ""
											})
										})
									});
								}
							});
						},
						"errCallback": ""
					}),
					"value2": await functions.loadData({
						"callback": async (data, extra) => {
							functions.console({
								"value": data
							});
							functions.setVar({
								"var": "vTotalAmountEmpty",
								"value": 0
							});
							functions.map({
								"values": await functions.selectAll({
									"dataset": "l_empty_bottle",
									"callback": "",
									"errCallback": ""
								}),
								"callback": async (data, extra) => {
									functions.console({
										"value": data
									});
									functions.setVar({
										"var": "vEmptyDetails_Order_Amount",
										"value": data
									});
									functions.setVar({
										"var": "item_amount_empty",
										"value": functions.multi({
											"value1": functions.getVarAttr({
												"var": "vEmptyDetails_Order_Amount",
												"attr": "quantity"
											}),
											"value2": functions.getVarAttr({
												"var": "vEmptyDetails_Order_Amount",
												"attr": "price"
											})
										})
									});
									functions.setVar({
										"var": "vTotalAmountEmpty",
										"value": functions.add({
											"value1": functions.getVar({
												"var": "vTotalAmountEmpty",
												"default": ""
											}),
											"value2": functions.getVar({
												"var": "item_amount_empty",
												"default": ""
											})
										})
									});
									functions.console({
										"value": functions.getVar({
											"var": "item_amount_empty"
										})
									});
									functions.console({
										"value": functions.getVar({
											"var": "vTotalAmountEmpty",
											"default": ""
										})
									});
								}
							});
						},
						"errCallback": "",
						"dataset": "l_empty_bottle"
					}),
					"value3": functions.userDefined('globalCalculateTotal', {
						"subTotal": functions.getVar({
							"var": "vTotalAmountItem"
						}),
						"emptyTotal": functions.getVar({
							"var": "vTotalAmountEmpty"
						}),
						"gstRate": functions.getVar({
							"var": "vGstTotalRate"
						})
					}),
					"value4": "",
					"value5": "",
					"value6": "",
					"value7": "",
					"value8": "",
					"value9": "",
					"value10": ""
				});
			},
			"noCallback": async (data, extra) => {
				functions.conditional({
					"condition": functions.equal({
						"value2": "remove_item",
						"value1": functions.getVar({
							"var": "vAction"
						})
					}),
					"yesCallback": async (data, extra) => {
						functions.setVar({
							"var": "vAction"
						});
						await functions.deleteBy({
							"value": functions.getVarAttr({
								"var": "v_current_item_selected",
								"attr": "_id"
							}),
							"callback": "",
							"errCallback": "",
							"dataset": "l_ Sales_Return_Lines",
							"by": "_id"
						});
						await functions.loadData({
							"errCallback": null,
							"dataset": "l_ Sales_Return_Lines",
							"callback": async (data, extra) => {
								functions.userDefined('globalModalInfo', {
									"message": "Item successfully removed.",
									"btnCaption": "OK",
									"title": "Message"
								});
							}
						});
					},
					"noCallback": async (data, extra) => {
						functions.conditional({
							"condition": functions.equal({
								"value1": functions.getVar({
									"var": "vAction"
								}),
								"value2": "submit_sales_return"
							}),
							"yesCallback": async (data, extra) => {
								functions.userDefined('globalModalLoading', {});
								functions.setVar({
									"var": "vAction",
									"value": ""
								});
								functions.newArray({
									"var": "sales_return_line_data"
								});
								functions.map({
									"values": await functions.selectAll({
										"dataset": "l_ Sales_Return_Lines",
										"callback": "",
										"errCallback": ""
									}),
									"callback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.setVar({
											"var": "v_map_Sales_Return_Lines",
											"value": data
										});
										functions.push({
											"var": "sales_return_line_data",
											"value": functions.toObject({
												"No": functions.getVarAttr({
													"var": "v_map_Sales_Return_Lines",
													"attr": "No"
												}),
												"Quantity": functions.getVarAttr({
													"var": "v_map_Sales_Return_Lines",
													"attr": "Quantity"
												}),
												"Unit_Price": functions.getVarAttr({
													"var": "v_map_Sales_Return_Lines",
													"attr": "Unit_Price"
												}),
												"Type": "Item",
												"Unit_of_Measure": functions.getVarAttr({
													"var": "v_map_Sales_Return_Lines",
													"attr": "Unit_of_Measure_Code"
												})
											})
										});
									}
								});
								functions.console({
									"value": functions.getVar({
										"var": "sales_return_line_data"
									})
								});
								await functions.navCall({
									"connector": "nav",
									"ent": "OK365_Sales_Return_Order",
									"function": "Create",
									"data": functions.toObject({
										"Location_Code": functions.objectAttr({
											"object": functions.getVarAttr({
												"var": "var_loginCnt",
												"attr": 0
											}),
											"attr": "Location_Code"
										}),
										"Mobile_User_ID": functions.objectAttr({
											"object": functions.getVarAttr({
												"var": "var_loginCnt",
												"attr": 0
											}),
											"attr": "Mobile_Login_User_ID"
										}),
										"Sell_to_Customer_No": functions.getVarAttr({
											"var": "vCurrCustomerDetails",
											"attr": "No"
										}),
										"Order_Date": functions.dbDate({}),
										"SalesLines": functions.toObject({
											"Sales_Return_Order_Line": functions.getVar({
												"var": "sales_return_line_data"
											})
										})
									}),
									"callback": async (data, extra) => {
										functions.setVar({
											"var": "v_result_sales_return_order",
											"value": functions.objectAttr({
												"object": data,
												"attr": "OK365_Sales_Return_Order"
											})
										});
										functions.setVar({
											"var": "v_result_sales_return_order_line",
											"value": functions.objectAttr({
												"object": functions.objectAttr({
													"object": functions.objectAttr({
														"attr": "OK365_Sales_Return_Order",
														"object": data
													}),
													"attr": "SalesLines"
												}),
												"attr": "Sales_Return_Order_Line"
											})
										});
										await functions.dataFromString({
											"dataset": "l_Sales_Return_Order",
											"string": functions.getVar({
												"var": "v_result_sales_return_order"
											}),
											"callback": "",
											"errCallback": ""
										});
										await functions.dataFromString({
											"dataset": "l_Sales_Return_Order_Lines",
											"string": functions.getVar({
												"var": "v_result_sales_return_order_line"
											}),
											"callback": null,
											"errCallback": null
										});
										functions.userDefined('globalModalHide', {});
										functions.console({
											"value": data
										});
										functions.setVar({
											"var": "vOrderNo",
											"value": functions.objectAttr({
												"object": functions.getVarAttr({
													"var": "vOrderSubmitResult",
													"attr": "OK365_Order_Entry"
												}),
												"attr": "No"
											})
										});
										functions.console({
											"value": functions.getVar({
												"var": "vOrderNo"
											})
										});
										functions.setVar({
											"var": "vAction",
											"value": "redirect"
										});
										functions.userDefined('globalModalInfo', {
											"title": "Sales Return",
											"message": functions.concat({
												"string1": "Sales Return No.",
												"string2": "",
												"string3": functions.concat({
													"string1": "<b>",
													"string2": functions.getVarAttr({
														"var": "v_result_sales_return_order",
														"attr": "No"
													}),
													"string3": "</b>"
												}),
												"string4": " has been successfully submitted."
											}),
											"btnCaption": "OK"
										});
										await functions.clearData({
											"dataset": "l_ Sales_Return_Lines",
											"callback": "",
											"errCallback": ""
										});
									},
									"errCallback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.console({
											"value": 2
										});
										functions.setVar({
											"var": "vAction",
											"value": ""
										});
										functions.userDefined('globalModalInfo', {
											"title": "Connection Error",
											"message": "Connection to server failed, please try it again."
										});
									}
								});
							},
							"noCallback": async (data, extra) => {
								functions.conditional({
									"condition": functions.equal({
										"value1": functions.getVar({
											"var": "vAction"
										}),
										"value2": "redirect"
									}),
									"yesCallback": async (data, extra) => {
										functions.gotoPage({
											"p": "pgMainMenu"
										});
										functions.setVar({
											"var": "vAction"
										});
									},
									"noCallback": async (data, extra) => {
										functions.conditional({
											"condition": functions.equal({
												"value1": functions.getVar({
													"var": "vAction"
												}),
												"value2": "back_sales_return_details"
											}),
											"yesCallback": async (data, extra) => {
												functions.setVar({
													"var": "vAction"
												});
												await functions.clearData({
													"dataset": "l_Sales_Return_Order",
													"callback": null,
													"errCallback": null
												});
												await functions.clearData({
													"dataset": "l_ Sales_Return_Lines",
													"callback": null,
													"errCallback": null
												});
												functions.gotoPage({
													"p": "pgSalesReturnLists"
												});
											},
											"noCallback": async (data, extra) => {
												functions.conditional({
													"condition": functions.equal({
														"value1": functions.getVar({
															"var": "vAction"
														}),
														"value2": "goto_invoice"
													}),
													"yesCallback": async (data, extra) => {
														functions.setVar({
															"var": "vAction",
															"value": ""
														});
														functions.setVar({
															"var": "vNewCheckout",
															"value": "true"
														});
														functions.gotoPage({
															"p": "pgSummary"
														});
													},
													"noCallback": async (data, extra) => {
														functions.userDefined('globalModalHide', {});
													}
												});
											}
										});
									}
								});
							}
						});
					}
				});
			}
		});
	},

	handlePnlModalBodyButtonNegativeClick: async () => {
		functions.console({
			"value": "Hello"
		});
		functions.console({
			"value": functions.getVar({
				"var": "vAction"
			})
		});
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "vAction"
				}),
				"value2": "goto_invoice"
			}),
			"yesCallback": async (data, extra) => {
				functions.console({
					"value": "Hello yes"
				});
				functions.toArray({
					"value1": functions.setVar({
						"var": "vAction",
						"value": ""
					}),
					"value2": functions.setVar({
						"var": "vCurrCustomer"
					}),
					"value3": functions.setVar({
						"var": "vCurrCustomerAddress"
					}),
					"value4": functions.setVar({
						"var": "vCurrCustomerDetails"
					})
				});
				functions.gotoPage({
					"p": "pgMainMenu"
				});
			},
			"noCallback": async (data, extra) => {
				functions.console({
					"value": "Hello Cancel"
				});
				functions.setVar({
					"var": "vAction"
				});
				functions.toArray({
					"value1": functions.setVar({
						"var": "vAction",
						"value": ""
					}),
					"value2": functions.setVar({
						"var": "vCurrCustomer"
					}),
					"value3": functions.setVar({
						"var": "vCurrCustomerAddress"
					}),
					"value4": functions.setVar({
						"var": "vCurrCustomerDetails"
					})
				});
				functions.console({
					"value": "Hello yes oh mo"
				});
				functions.userDefined('globalModalHide', {});
			}
		});
	},

	handleImgUpdateQtyCloseClick: async () => {
		functions.setComponentValue({
			"component": "txtQuantity",
			"value": ""
		});
		functions.hideElement({
			"component": "pnlUpdateQty"
		});
	},

	handleImgMinusClick: async () => {
		functions.conditional({
			"condition": functions.greater({
				"value1": null,
				"value2": 1
			}),
			"yesCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"value": functions.sub({
						"value1": null,
						"value2": 1
					})
				});
			},
			"noCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"value": 0
				});
			}
		});
	},

	handleTxtQuantityClick: async () => {
		functions.setComponentFocus({
			"component": "txtQuantity",
			"selectAllText": "true"
		});
	},

	handleImgPlusClick: async () => {
		functions.conditional({
			"condition": functions.greater({
				"value1": null,
				"value2": -1
			}),
			"yesCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"value": functions.add({
						"value1": null,
						"value2": 1
					})
				});
			},
			"noCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"value": 0
				});
			}
		});
	},

	handleBtnUpdateQtyUpdateClick: async () => {
		functions.conditional({
			"condition": null,
			"yesCallback": async (data, extra) => {
				functions.conditional({
					"condition": functions.greater({
						"value1": null,
						"value2": functions.toFloat({
							"value": -1
						})
					}),
					"yesCallback": async (data, extra) => {
						functions.conditional({
							"condition": functions.greater({
								"value1": null,
								"value2": 0
							}),
							"yesCallback": async (data, extra) => {
								await functions.loadData({
									"dataset": "l_ Sales_Return_Lines",
									"callback": async (data, extra) => {
										functions.hideElement({
											"component": "pnlUpdateQty"
										});
									},
									"errCallback": null
								});
							},
							"noCallback": ""
						});
					},
					"noCallback": async (data, extra) => {
						functions.userDefined('globalModalInfo', {
							"title": "Invalid Value",
							"message": "Please enter a valid value/amount."
						});
					}
				});
			},
			"noCallback": async (data, extra) => {
				functions.userDefined('globalModalInfo', {
					"title": "No Value",
					"message": "Enter the quantity."
				});
			}
		});
	},

	handlePanel960Click: async () => {
		functions.hideElement({
			"component": "pnlSelectReasonCode"
		});
		await functions.updateBy({
			"dataset": "l_ Sales_Return_Lines",
			"by": "_id",
			"operator": "=",
			"value": functions.getVarAttr({
				"var": "v_current_item_selected",
				"attr": "_id"
			}),
			"data": functions.toObject({
				"Return_Reason_Code": null
			}),
			"callback": null,
			"errCallback": null
		});
	},

	handlePanel561Click: async () => {
		functions.hideElement({
			"component": "pnlSelectReasonCode"
		});
	},

	handlePgSalesReturnDetailsUpdateLoad: async () => {
		functions.onBackButton({
			"callback": async (data, extra) => {
				functions.gotoPage({
					"p": "pgCart"
				});
			}
		});
		functions.setVar({
			"var": "vAction"
		});
		functions.setVar({
			"var": "vCheckout",
			"value": functions.toObject({})
		});
		functions.userDefined('globalModalLoading', {});
		functions.toArray({
			"value1": functions.setComponentValue({
				"component": "lblCustomerCodeName",
				"value": functions.concat({
					"string1": functions.getVarAttr({
						"var": "vCurrCustomerDetails",
						"attr": "No"
					}),
					"string2": " - ",
					"string3": functions.getVarAttr({
						"var": "vCurrCustomerDetails",
						"attr": "Name"
					})
				})
			}),
			"value2": functions.setComponentValue({
				"component": "txtCustomerName",
				"value": functions.getVarAttr({
					"var": "vCurrCustomerDetails",
					"attr": "Name"
				})
			}),
			"value3": functions.setComponentValue({
				"component": "lblOrderDate",
				"value": functions.formatDate({
					"date": functions.dbDate({}),
					"format": "Y-m-d"
				})
			}),
			"value4": functions.setComponentValue({
				"component": "txtInvoiceDate",
				"value": functions.formatDate({
					"date": functions.dbDate({}),
					"format": "d-m-Y"
				})
			}),
			"value5": functions.setComponentValue({
				"component": "txtRemarks",
				"value": functions.getVarAttr({
					"var": "vCheckout",
					"attr": "Comments"
				})
			}),
			"value6": functions.setComponentValue({
				"component": "lblAddress",
				"value": functions.getVarAttr({
					"var": "vCurrCustomerDetails",
					"attr": "Address"
				})
			})
		});
		await functions.loadData({
			"errCallback": null,
			"dataset": "l_ Sales_Return_Lines",
			"callback": async (data, extra) => {
				functions.userDefined('globalModalHide', {});
			}
		});
		await functions.loadData({
			"dataset": "OK365_Return_reason_code",
			"callback": async (data, extra) => {
				functions.setVar({
					"var": "v_return_reason_code",
					"value": data
				});
				functions.setComboOptions({
					"displayField": "Description",
					"combo": "cmbReasonCode",
					"data": functions.getVar({
						"var": "v_return_reason_code"
					}),
					"valueField": "Code"
				});
			},
			"errCallback": null
		});
	}
};

const PgSalesReturnDetailsUpdate = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;
	formatParentStyleLayout(props);

	useEffect(() => {
		actions.handlePgSalesReturnDetailsUpdateLoad();
	}, [currentTime]);
    
    return (
		<Page data={props.pgSalesReturnDetailsUpdate}>
			<Panel data={props.pnlHolder}>
				<Panel data={props.pnlMain}>
					<Panel data={props.pnlMainHeader}>
						<Panel data={props.pnlMainHeaderLeft}>
							<Panel data={props.pnlMainHeaderLeftTable} action={actions.handlePnlMainHeaderLeftTableClick}>
								<Panel data={props.pnlMainHeaderLeftCell}>
									<Image data={props.imgBack} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderMiddle}>
							<Panel data={props.pnlMainHeaderMiddleTable}>
								<Panel data={props.pnlMainHeaderMiddleCell}>
									<Label data={props.lblMainTitle} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderRight}>
							<Panel data={props.pnlMainHeaderRightTable}>
								<Panel data={props.pnlMainHeaderRightCell} />
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBody}>
						<Panel data={props.pnlMainBodyHeader}>
							<Panel data={props.pnlBodyMainContent}>
								<Panel data={props.pnlBodyContentHeader}>
									<Label data={props.lblCustomerCodeName} />
									<Label data={props.lblAddress} />
									<Label data={props.lblOrderDate} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainBodyHeaderRemarks}>
							<Panel data={props.pnlMainBodyForm2LabelRemarks767}>
								<Label data={props.lblRemarks706} />
								<Label data={props.lblRemarksChar395} />
							</Panel>
							<Panel data={props.pnlMainBodyForm2InputValue32}>
								<Edit data={props.txtRemarks709} />
							</Panel>
						</Panel>
						<Panel data={props.pnlMainBodyHeaderCaption}>
							<Label data={props.lblItems509} />
							<Image data={props.Image553564} action={actions.handleImage553564Click} />
						</Panel>
						<Panel data={props.pnlMainBodyItems84}>
							<Panel data={props.pnlMainBodyItemsCO2656}>
								<DataList data={props.dtSales_Return_Lines} functions={functions} currentTime={currentTime}>
									{({ item, parentStyle }) => (								
										<Panel data={item.Panel17} parentStyle={parentStyle}>
											<Panel data={item.pnlProduct} parentStyle={parentStyle}>
												<Panel data={item.pnlProductBodyLeft} parentStyle={parentStyle}>
													<Image data={item.imgItem} parentStyle={parentStyle} />
												</Panel>
												<Panel data={item.pnlProductBodyMiddle} parentStyle={parentStyle}>
													<Panel data={item.pnlProductBodyMiddle1} parentStyle={parentStyle}>
														<Label data={item.lblItemCode} parentStyle={parentStyle} />
														<Label data={item.lblItemName} parentStyle={parentStyle} />
														<Label data={item.lblPrice} parentStyle={parentStyle} />
														<Label data={item.lblDiscCO} parentStyle={parentStyle} />
														<Label data={item.lblReasonCode_data} parentStyle={parentStyle} />
													</Panel>
												</Panel>
											</Panel>
											<Panel data={item.Panel233} parentStyle={parentStyle}>
												<Panel data={item.Panel489} action={actions.handlePanel489Click} parentStyle={parentStyle}>
													<Label data={item.Label335} parentStyle={parentStyle} />
												</Panel>
												<Panel data={item.Panel489504} action={actions.handlePanel489504Click} parentStyle={parentStyle}>
													<Label data={item.Label335234361} parentStyle={parentStyle} />
												</Panel>
												<Panel data={item.Panel489850} action={actions.handlePanel489850Click} parentStyle={parentStyle}>
													<Label data={item.Label335785391} parentStyle={parentStyle} />
												</Panel>
											</Panel>
										</Panel>
									)}
								</DataList>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainBodyButtons510}>
							<Panel data={props.pnlMainBodyButtonsDrafft886}>
								<Button data={props.btnSAVE691} action={actions.handleBtnSAVE691Click} />
							</Panel>
							<Panel data={props.pnlMainBodyButtonsINV65}>
								<Button data={props.btnOrder743881} action={actions.handleBtnOrder743881Click} />
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBodyDetails}>
						<Panel data={props.pnlMainBodyDetails1872}>
							<Label data={props.lblTotalAmountExclGST237} />
							<Label data={props.lblDataTotalAmountExclGST363} />
						</Panel>
					</Panel>
					<Panel data={props.pnlMainFooter}>
						<Panel data={props.pnlMainFooterTbl361}>
							<Panel data={props.pnlMainFooterTblCell1865} action={actions.handlePnlMainFooterTblCell1865Click}>
								<Label data={props.lblBtnOrder72} />
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal}>
					<Panel data={props.pnlModalTable}>
						<Panel data={props.pnlModalCell}>
							<Panel data={props.pnlModalBodyMain}>
								<Panel data={props.pnlModalBodyTitle}>
									<Label data={props.lblModalTitle} />
								</Panel>
								<Panel data={props.pnlModalBodyMessage}>
									<Label data={props.lblModalMessage} />
								</Panel>
								<Panel data={props.pnlModalBodyLoading}>
									<Image data={props.imgModalLoading} />
								</Panel>
							</Panel>
							<Panel data={props.pnlModalBodyButtons}>
								<Panel data={props.pnlModalBodyButtonPositive} action={actions.handlePnlModalBodyButtonPositiveClick}>
									<Label data={props.lblModalPositive} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonNegative} action={actions.handlePnlModalBodyButtonNegativeClick}>
									<Label data={props.lblModalNegative} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlCSS}>
					<Label data={props.lblStyle744} />
				</Panel>
				<Panel data={props.pnlUpdateQty}>
					<Panel data={props.pnlUpdateQtyTable}>
						<Panel data={props.pnlUpdateQtyTableCell}>
							<Panel data={props.pnlUpdateQtyContent}>
								<Panel data={props.Panel513}>
									<Image data={props.imgUpdateQtyClose} action={actions.handleImgUpdateQtyCloseClick} />
									<Panel data={props.pnlUpdateQtyContentHeader}>
										<Panel data={props.pnlUpdateQtyItemImage}>
											<Image data={props.imgUpdateQtyItemImage} />
										</Panel>
										<Panel data={props.pnlUpdateQtyItemNo}>
											<Label data={props.lblUpdateQtyItemNo} />
										</Panel>
										<Panel data={props.pnlUpdateQtyItemName}>
											<Label data={props.lblUpdateQtyItemName} />
										</Panel>
										<Panel data={props.pnlUpdateQtyItemUnitPrice}>
											<Label data={props.lblUpdateQtyItemUnitPrice} />
										</Panel>
									</Panel>
									<Panel data={props.pnlUpdateQtyContentMainInput}>
										<Panel data={props.pnlUpdateQtyContentInput}>
											<Panel data={props.pnlUpdateQtyContentInputQty}>
												<Panel data={props.pnlUpdateQtyContentInputQtyTable}>
													<Panel data={props.pnlUpdateQtyContentInputQtyTableCell1}>
														<Image data={props.imgMinus} action={actions.handleImgMinusClick} />
													</Panel>
													<Panel data={props.pnlUpdateQtyContentInputQtyTableCell2}>
														<Edit data={props.txtQuantity} action={actions.handleTxtQuantityClick} />
													</Panel>
													<Panel data={props.pnlUpdateQtyContentInputQtyTableCell3}>
														<Image data={props.imgPlus} action={actions.handleImgPlusClick} />
													</Panel>
												</Panel>
											</Panel>
											<Panel data={props.pnlUpdateQtyContentInputUOM}>
												<Label data={props.lblUpdateQtyContentInputUOM} />
											</Panel>
										</Panel>
									</Panel>
								</Panel>
								<Panel data={props.pnlUpdateQtyFooterBtn}>
									<Button data={props.btnUpdateQtyUpdate} action={actions.handleBtnUpdateQtyUpdateClick} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlSelectReasonCode}>
					<Panel data={props.pnlUpdateQtyTable992753}>
						<Panel data={props.pnlUpdateQtyTableCell515}>
							<Panel data={props.pnlUpdateQtyContent277}>
								<Panel data={props.Panel513530}>
									<Panel data={props.Panel401}>
										<Label data={props.Label285} />
									</Panel>
									<Panel data={props.pnlUpdateQtyContentHeader387}>
										<Panel data={props.pnlUpdateQtyItemImage874}>
											<ComboBox data={props.cmbReasonCode} functions={functions} />
										</Panel>
									</Panel>
								</Panel>
								<Panel data={props.pnlUpdateQtyFooterBtn704}>
									<Panel data={props.Panel960} action={actions.handlePanel960Click}>
										<Label data={props.Label9} />
									</Panel>
									<Panel data={props.Panel561} action={actions.handlePanel561Click}>
										<Label data={props.Label750} />
									</Panel>
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgSalesReturnDetailsUpdate);