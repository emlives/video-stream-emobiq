import React, { useEffect }  from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgPayment';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import Edit from '../framework/component/Edit';
import ComboBox from '../framework/component/ComboBox';
import Button from '../framework/component/Button';
import Signature from '../framework/component/Signature';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handleImgBack555Click: async () => {
		functions.toArray({
			"value1": functions.setVar({
				"var": "vInvoiceDate",
				"value": functions.componentValue({
					"component": "edtInvoiceDate"
				})
			}),
			"value2": functions.setVar({
				"var": "vPaymentMethod",
				"value": functions.componentValue({
					"component": "cmbPaymentMethod"
				})
			}),
			"value3": functions.setVar({
				"var": "vChequeNo",
				"value": functions.componentValue({
					"component": "edtChequeNumber"
				})
			}),
			"value4": functions.setVar({
				"var": "vPaymentAmount",
				"value": functions.componentValue({
					"component": "edtPaymentAmount"
				})
			}),
			"value5": ""
		});
		functions.setVar({
			"var": "vNewCheckout"
		});
		functions.gotoPage({
			"p": "pgSummary"
		});
	},

	handleImgBack555407Click: async () => {
		functions.showElement({
			"component": "pnlPrinter"
		});
	},

	handleBtnFullClick: async () => {
		functions.setComponentValue({
			"component": "edtPaymentAmount",
			"value": functions.getVarAttr({
				"var": "vFirstOrderLine",
				"attr": "Total_Amount_Incl_VAT"
			})
		});
	},

	handlePnlMainFooterTableCell1Click: async () => {
		functions.setVar({
			"var": "vAction",
			"value": "postInvoice"
		});
		functions.showElement({
			"component": "pnlConfirmPaymentdDetails",
			"componentId": ""
		});
	},

	handlePnlMainFooterTableCell2Click: async () => {
		functions.setVar({
			"var": "vAction",
			"value": "postInvoiceWithoutPayment"
		});
		functions.showElement({
			"component": "pnlConfirmPaymentdDetails",
			"componentId": ""
		});
	},

	handlePnlModalBodyButtonPositiveClick: async () => {
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "vAction"
				}),
				"value2": "postInvoice"
			}),
			"yesValue": "",
			"noValue": "",
			"extra": "",
			"yesCallback": async (data, extra) => {
				functions.userDefined('globalModalLoading', {});
				await functions.loadData({
					"dataset": "OK365_Order_Entry",
					"filter": functions.toArray({
						"value1": functions.toObject({
							"No": functions.getVar({
								"var": "vOrderNo"
							})
						})
					}),
					"callback": async (data, extra) => {
						functions.setVar({
							"var": "vOrderData",
							"value": functions.objectAttr({
								"object": data,
								"attr": 0
							})
						});
						await functions.navCall({
							"data": functions.toObject({
								"Key": functions.getVarAttr({
									"var": "vOrderData",
									"attr": "Key"
								}),
								"No": functions.getVarAttr({
									"var": "vOrderData",
									"attr": "No"
								}),
								"Posting_Date": null
							}),
							"callback": async (data, extra) => {
								functions.console({
									"value": "Posting Date updated"
								});
								await functions.navCall({
									"callback": async (data, extra) => {
										functions.console({
											"value": "1. Posted Sales Invoice Result"
										});
										functions.console({
											"value": data
										});
										await functions.loadData({
											"errCallback": async (data, extra) => {
												functions.setVar({
													"var": "error",
													"value": data["err"]
												});
												functions.console({
													"value": data
												});
												functions.console({
													"value": "result error"
												});
												functions.userDefined('globalModalInfo', {
													"title": "Connection Error",
													"message": "There's an error connecting to Navision Web Services (Load Posted Invoice)"
												});
												functions.userDefined('globalModalHide', {});
											},
											"dataset": "OK365_Posted_Sales_Invoice_Header",
											"filter": functions.toArray({
												"value1": functions.toObject({
													"f": "Order_No",
													"o": "=",
													"v": functions.getVar({
														"var": "vOrderNo"
													})
												})
											}),
											"callback": async (data, extra) => {
												functions.console({
													"value": "2. Retrieved Posted Sales Invoice"
												});
												functions.console({
													"value": data
												});
												functions.setVar({
													"var": "vPostedInvoice",
													"value": functions.objectAttr({
														"object": data,
														"attr": 0
													})
												});
												functions.setVar({
													"var": "vInvoiceDetails",
													"value": data[0]
												});
												functions.setVar({
													"var": "vInvoicedLines",
													"value": functions.objectAttr({
														"attr": "Posted_Sales_Invoice_Line",
														"object": functions.objectAttr({
															"attr": "SalesInvLines",
															"object": functions.getVar({
																"var": "vInvoiceDetails"
															})
														})
													})
												});
												await functions.navCall({
													"errCallback": async (data, extra) => {
														functions.console({
															"value": "no"
														});
														functions.userDefined('globalModalInfo', {
															"message": "There's an error connecting to Navision Web Services (Set Signature)",
															"title": "Connection Error"
														});
														functions.userDefined('globalModalHide', {});
													},
													"connector": "nav",
													"ent": "OK365_GetItemSalesPrice",
													"function": "SetPostedSalesInvoiceSignature",
													"data": functions.toObject({
														"signaturePicture": functions.replace({
															"string": functions.getVar({
																"var": "vSignature"
															}),
															"replace": "data:image/png;base64,"
														}),
														"postedSalesInvoiceNo": functions.getVarAttr({
															"var": "vPostedInvoice",
															"attr": "No"
														})
													}),
													"callback": async (data, extra) => {
														functions.console({
															"value": "1. Posted Sales Invoice Result"
														});
														await functions.navCall({
															"connector": "nav",
															"ent": "OK365_GetItemSalesPrice",
															"function": "GetPostedSalesInvoiceSignature",
															"data": functions.toObject({
																"postedSalesInvoiceNo": functions.getVarAttr({
																	"var": "vPostedInvoice",
																	"attr": "No"
																})
															}),
															"callback": async (data, extra) => {
																functions.setVar({
																	"var": "vSignature",
																	"value": functions.objectAttr({
																		"object": data,
																		"attr": "return_value"
																	})
																});
																functions.console({
																	"value": data
																});
																functions.console({
																	"value": "output result for signature"
																});
																await functions.loadData({
																	"dataset": "OK365_Company_Information",
																	"filter": "",
																	"callback": async (data, extra) => {
																		functions.setVar({
																			"var": "vCompany",
																			"value": data[0]
																		});
																		functions.console({
																			"value": functions.getVar({
																				"var": "vCompany"
																			})
																		});
																		await functions.navCall({
																			"data": functions.toObject({
																				"External_Document_No": null,
																				"GeneralBatchName": functions.objectAttr({
																					"object": await functions.selectBy({
																						"first": true,
																						"callback": null,
																						"errCallback": null,
																						"dataset": "l_Route_Master",
																						"by": "_id",
																						"value": 1
																					}),
																					"attr": "Cash_Rcpt_Jnl_Batch_Name"
																				}),
																				"Route_Code": functions.getVarAttr({
																					"var": "vRouteMaster",
																					"attr": "Route_Code"
																				}),
																				"Account_No": functions.getVarAttr({
																					"var": "vPostedInvoice",
																					"attr": "Bill_to_Customer_No_OkFldSls"
																				}),
																				"Document_Type": "Payment",
																				"Account_Type": "Customer",
																				"Applies_to_Doc_Type": "Invoice",
																				"Applies_to_Doc_No": functions.getVarAttr({
																					"var": "vPostedInvoice",
																					"attr": "No"
																				}),
																				"Amount": functions.multi({
																					"value1": functions.componentValue({
																						"component": "edtPaymentAmount"
																					}),
																					"value2": -1
																				}),
																				"GeneralJournalTemplate": functions.objectAttr({
																					"object": await functions.selectBy({
																						"value": 1,
																						"first": true,
																						"callback": null,
																						"errCallback": null,
																						"dataset": "l_Route_Master",
																						"by": "_id"
																					}),
																					"attr": "Cash_Rcpt_Jnl_Template"
																				}),
																				"Payment_Method_Code": null,
																				"Posting_Date": functions.getVarAttr({
																					"var": "vPostedInvoice",
																					"attr": "Posting_Date"
																				}),
																				"Mobile_User_ID": functions.getVar({
																					"var": "vUsername"
																				})
																			}),
																			"callback": async (data, extra) => {
																				functions.setVar({
																					"var": "v_current_Cash_Receipt",
																					"value": functions.objectAttr({
																						"object": data,
																						"attr": "OK365_CashReceipt"
																					})
																				});
																				await functions.loadData({
																					"dataset": "OK365_Customer_Ledger_Entries",
																					"limit": 1,
																					"filter": functions.toArray({
																						"value1": functions.toObject({
																							"f": "Document_No",
																							"v": functions.getVarAttr({
																								"var": "vPostedInvoice",
																								"attr": "No"
																							})
																						}),
																						"value2": functions.toObject({
																							"f": "Customer_No",
																							"v": functions.getVarAttr({
																								"var": "vPostedInvoice",
																								"attr": "Bill_to_Customer_No"
																							})
																						})
																					}),
																					"callback": async (data, extra) => {
																						functions.setVar({
																							"var": "v_current_Customer_Ledger_Entries",
																							"value": functions.objectAttr({
																								"object": data,
																								"attr": 0
																							})
																						});
																						await functions.navCall({
																							"errCallback": null,
																							"connector": "nav",
																							"ent": "OK365_Customer_Ledger_Entries",
																							"function": "Update",
																							"data": functions.toObject({
																								"Key": functions.getVarAttr({
																									"var": "v_current_Customer_Ledger_Entries",
																									"attr": "Key"
																								}),
																								"Select_for_Payment": 1,
																								"Applies_to_ID": functions.getVarAttr({
																									"var": "v_current_Cash_Receipt",
																									"attr": "Document_No"
																								})
																							}),
																							"callback": null
																						});
																					},
																					"errCallback": null
																				});
																				functions.setVar({
																					"var": "vTempLang",
																					"value": functions.getLanguage({})
																				});
																				functions.setLanguage({
																					"lang": "English"
																				});
																				await functions.loadData({
																					"dataset": "OK365_GST_Setup",
																					"filter": functions.toArray({
																						"value1": functions.toObject({
																							"f": "VAT_Prod_Posting_Group",
																							"v": functions.getVarAttr({
																								"var": "vItemDetails",
																								"attr": "VAT_Prod_Posting_Group"
																							})
																						}),
																						"value2": functions.toObject({
																							"f": "VAT_Bus_Posting_Group",
																							"v": functions.getVarAttr({
																								"var": "vCurrCustomerDetails",
																								"attr": "VAT_Bus_Posting_Group"
																							})
																						})
																					}),
																					"callback": async (data, extra) => {
																						functions.console({
																							"value": "gst rate"
																						});
																						functions.console({
																							"value": functions.objectAttr({
																								"object": functions.objectAttr({
																									"object": data,
																									"attr": 0
																								}),
																								"attr": "VAT_Percent"
																							})
																						});
																						functions.setVar({
																							"var": "vGstTotalRate",
																							"value": functions.objectAttr({
																								"object": functions.objectAttr({
																									"object": data,
																									"attr": 0
																								}),
																								"attr": "VAT_Percent"
																							})
																						});
																						functions.toArray({
																							"value1": await functions.loadData({
																								"dataset": "l_cart",
																								"callback": async (data, extra) => {
																									functions.setVar({
																										"var": "vTotalAmountItem",
																										"value": 0
																									});
																									functions.map({
																										"values": await functions.selectAll({
																											"errCallback": null,
																											"dataset": "l_cart",
																											"callback": ""
																										}),
																										"callback": async (data, extra) => {
																											functions.console({
																												"value": data
																											});
																											functions.setVar({
																												"var": "vItemDetails_Order",
																												"value": data
																											});
																											functions.setVar({
																												"var": "item_amount",
																												"value": functions.multi({
																													"value1": functions.getVarAttr({
																														"var": "vItemDetails_Order",
																														"attr": "Quantity"
																													}),
																													"value2": functions.getVarAttr({
																														"var": "vItemDetails_Order",
																														"attr": "Price"
																													})
																												})
																											});
																											functions.setVar({
																												"var": "vTotalAmountItem",
																												"value": functions.add({
																													"value1": functions.getVar({
																														"var": "vTotalAmountItem",
																														"default": ""
																													}),
																													"value2": functions.getVar({
																														"var": "item_amount",
																														"default": ""
																													})
																												})
																											});
																										}
																									});
																								},
																								"errCallback": ""
																							}),
																							"value2": await functions.loadData({
																								"callback": async (data, extra) => {
																									functions.console({
																										"value": data
																									});
																									functions.setVar({
																										"var": "vTotalAmountEmpty",
																										"value": 0
																									});
																									functions.map({
																										"values": await functions.selectAll({
																											"dataset": "l_empty_bottle",
																											"callback": "",
																											"errCallback": ""
																										}),
																										"callback": async (data, extra) => {
																											functions.console({
																												"value": data
																											});
																											functions.setVar({
																												"var": "vEmptyDetails_Order_Amount",
																												"value": data
																											});
																											functions.setVar({
																												"var": "item_amount_empty",
																												"value": functions.multi({
																													"value1": functions.getVarAttr({
																														"var": "vEmptyDetails_Order_Amount",
																														"attr": "quantity"
																													}),
																													"value2": functions.getVarAttr({
																														"var": "vEmptyDetails_Order_Amount",
																														"attr": "price"
																													})
																												})
																											});
																											functions.setVar({
																												"var": "vTotalAmountEmpty",
																												"value": functions.add({
																													"value1": functions.getVar({
																														"var": "vTotalAmountEmpty",
																														"default": ""
																													}),
																													"value2": functions.getVar({
																														"var": "item_amount_empty",
																														"default": ""
																													})
																												})
																											});
																											functions.console({
																												"value": functions.getVar({
																													"var": "item_amount_empty"
																												})
																											});
																											functions.console({
																												"value": functions.getVar({
																													"var": "vTotalAmountEmpty",
																													"default": ""
																												})
																											});
																										}
																									});
																								},
																								"errCallback": "",
																								"dataset": "l_empty_bottle"
																							}),
																							"value3": functions.userDefined('globalCalculateTotal', {
																								"subTotal": functions.getVar({
																									"var": "vTotalAmountItem"
																								}),
																								"emptyTotal": functions.getVar({
																									"var": "vTotalAmountEmpty"
																								}),
																								"gstRate": functions.getVar({
																									"var": "vGstTotalRate"
																								})
																							}),
																							"value4": "",
																							"value5": "",
																							"value6": "",
																							"value7": "",
																							"value8": "",
																							"value9": "",
																							"value10": ""
																						});
																						functions.setTimeout({
																							"timeout": 5000,
																							"callback": async (data, extra) => {
																								functions.setVar({
																									"var": "vAction",
																									"value": "redirectToMainPage"
																								});
																								functions.userDefined('globalModalInfo', {
																									"title": "Completed",
																									"message": functions.concat({
																										"string1": "<b><h3>",
																										"string2": functions.getVarAttr({
																											"var": "vPostedInvoice",
																											"attr": "No"
																										}),
																										"string3": "</b></h3>",
																										"string4": functions.concat({
																											"string1": "New Invoice number",
																											"string2": "<br>",
																											"string3": "Your order has been successfully posted."
																										})
																									}),
																									"btnCaption": "OK"
																								});
																							}
																						});
																					},
																					"errCallback": async (data, extra) => {
																						functions.setVar({
																							"var": "error",
																							"value": data["err"]
																						});
																					}
																				});
																				functions.console({
																					"value": "3. Cash Receipt"
																				});
																				functions.console({
																					"value": data
																				});
																				functions.console({
																					"value": "************ START RECEIPT *********"
																				});
																				functions.toArray({
																					"value1": functions.setVar({
																						"var": "vGSTValue",
																						"value": functions.getVar({
																							"var": "vGstTotalRate"
																						})
																					}),
																					"value2": functions.setVar({
																						"var": "width",
																						"value": 38
																					}),
																					"value3": functions.setVar({
																						"var": "center",
																						"value": 19
																					}),
																					"value4": functions.setVar({
																						"var": "margin_left",
																						"value": 0
																					}),
																					"value5": functions.setVar({
																						"var": "width_center",
																						"value": 10
																					})
																				});
																				functions.toArray({
																					"value1": functions.setVar({
																						"var": "vHeader",
																						"value": functions.concat({
																							"string1": functions.concat({
																								"string1": functions.padString({
																									"len": functions.getVar({
																										"var": "margin_left"
																									}),
																									"type": "left",
																									"char": " "
																								}),
																								"string2": functions.concat({
																									"string1": "[size='24pt']",
																									"string2": functions.padString({
																										"string": functions.getVarAttr({
																											"var": "vCompany",
																											"attr": "Name"
																										}),
																										"len": 30,
																										"type": "center"
																									}),
																									"string3": "[/size]",
																									"string4": ""
																								}),
																								"string3": null,
																								"string4": ""
																							}),
																							"string2": null,
																							"string3": functions.concat({
																								"string1": functions.concat({
																									"string1": functions.padString({
																										"type": "left",
																										"char": " ",
																										"len": functions.getVar({
																											"var": "left_margin"
																										})
																									}),
																									"string2": functions.padString({
																										"string": functions.concat({
																											"string1": functions.getVarAttr({
																												"var": "vCompany",
																												"attr": "Address"
																											}),
																											"string2": "",
																											"string3": functions.getVarAttr({
																												"var": "vCompany",
																												"attr": "Address2"
																											}),
																											"string4": ""
																										}),
																										"len": functions.getVar({
																											"var": "width"
																										}),
																										"type": "center"
																									}),
																									"string3": null
																								}),
																								"string2": functions.concat({
																									"string1": functions.padString({
																										"len": functions.getVar({
																											"var": "left_margin"
																										}),
																										"type": "left",
																										"char": " "
																									}),
																									"string2": functions.padString({
																										"string": functions.concat({
																											"string1": functions.getVarAttr({
																												"var": "vCompany",
																												"attr": "City"
																											}),
																											"string2": " ",
																											"string3": functions.getVarAttr({
																												"var": "vCompany",
																												"attr": "Post_Code"
																											})
																										}),
																										"len": functions.getVar({
																											"var": "width"
																										}),
																										"type": "center"
																									}),
																									"string3": null
																								}),
																								"string3": functions.concat({
																									"string1": functions.padString({
																										"len": functions.sub({
																											"value1": functions.getVar({
																												"var": "center"
																											}),
																											"value2": 16
																										}),
																										"type": "center",
																										"char": " "
																									}),
																									"string2": functions.concat({
																										"string1": "Tel",
																										"string2": ": ",
																										"string3": functions.padString({
																											"string": functions.getVarAttr({
																												"var": "vCompany",
																												"attr": "Phone_No"
																											}),
																											"len": 10,
																											"type": "left",
																											"char": ""
																										}),
																										"string4": ""
																									}),
																									"string3": "",
																									"string4": functions.concat({
																										"string1": "Fax",
																										"string2": ": ",
																										"string3": functions.padString({
																											"string": 63826045,
																											"len": 10,
																											"type": "left",
																											"char": ""
																										}),
																										"string4": functions.concat({
																											"string1": null,
																											"string2": null
																										})
																									})
																								}),
																								"string4": functions.concat({
																									"string1": functions.concat({
																										"string1": functions.padString({
																											"len": functions.sub({
																												"value1": functions.getVar({
																													"var": "center"
																												}),
																												"value2": 11
																											}),
																											"type": "center",
																											"char": " "
																										}),
																										"string2": "CO.REG.No",
																										"string3": ": ",
																										"string4": functions.padString({
																											"string": functions.getVarAttr({
																												"var": "vCompany",
																												"attr": "VAT_Registration_No"
																											}),
																											"len": functions.sub({
																												"value1": functions.getVar({
																													"var": "width_center"
																												}),
																												"value2": 10
																											}),
																											"type": "center"
																										})
																									}),
																									"string2": null,
																									"string3": functions.concat({
																										"string1": functions.padString({
																											"len": functions.sub({
																												"value1": functions.getVar({
																													"var": "center"
																												}),
																												"value2": 12
																											}),
																											"type": "center",
																											"char": " "
																										}),
																										"string2": "GST.REG.No",
																										"string3": ": ",
																										"string4": functions.padString({
																											"len": functions.sub({
																												"value1": functions.getVar({
																													"var": "width_center"
																												}),
																												"value2": 10
																											}),
																											"type": "center",
																											"string": functions.getVarAttr({
																												"var": "vCompany",
																												"attr": "VAT_Registration_No"
																											})
																										})
																									})
																								})
																							}),
																							"string4": ""
																						})
																					}),
																					"value2": functions.setVar({
																						"var": "vSubHeader",
																						"value": functions.concat({
																							"string1": functions.concat({
																								"string1": functions.concat({
																									"string1": functions.padString({
																										"len": functions.getVar({
																											"var": "left_margin"
																										}),
																										"type": "left",
																										"char": " "
																									}),
																									"string2": null,
																									"string3": ""
																								}),
																								"string2": functions.concat({
																									"string1": functions.concat({
																										"string1": functions.padString({
																											"len": functions.getVar({
																												"var": "margin_left"
																											}),
																											"type": "left",
																											"char": " "
																										}),
																										"string2": functions.padString({
																											"char": "-",
																											"len": functions.getVar({
																												"var": "width"
																											}),
																											"type": "left"
																										}),
																										"string3": null
																									}),
																									"string2": functions.concat({
																										"string1": functions.concat({
																											"string1": functions.padString({
																												"char": " ",
																												"len": functions.getVar({
																													"var": "left_margin"
																												}),
																												"type": "left"
																											}),
																											"string2": functions.padString({
																												"string": functions.concat({
																													"string1": null
																												}),
																												"len": functions.getVar({
																													"var": "width"
																												}),
																												"type": "center"
																											}),
																											"string3": null
																										}),
																										"string2": functions.concat({
																											"string1": functions.padString({
																												"len": functions.getVar({
																													"var": "margin_left"
																												}),
																												"type": "left",
																												"char": " "
																											}),
																											"string2": functions.padString({
																												"type": "left",
																												"char": "-",
																												"len": functions.getVar({
																													"var": "width"
																												})
																											}),
																											"string3": null
																										})
																									})
																								})
																							}),
																							"string2": functions.concat({
																								"string1": functions.padString({
																									"len": functions.getVar({
																										"var": "margin_left"
																									}),
																									"type": "left",
																									"char": " "
																								}),
																								"string2": functions.concat({
																									"string1": functions.concat({
																										"string1": "Invoice No.",
																										"string2": "",
																										"string3": ""
																									}),
																									"string2": functions.padString({
																										"len": functions.sub({
																											"value1": functions.getVar({
																												"var": "center"
																											}),
																											"value2": 24
																										}),
																										"char": " "
																									}),
																									"string3": ": ",
																									"string4": ""
																								}),
																								"string3": functions.getVarAttr({
																									"var": "vInvoiceDetails",
																									"attr": "No"
																								}),
																								"string4": ""
																							}),
																							"string3": functions.concat({
																								"string1": functions.concat({
																									"string1": null,
																									"string2": functions.padString({
																										"char": " ",
																										"string": "",
																										"len": functions.sub({
																											"value1": functions.getVar({
																												"var": "center"
																											}),
																											"value2": 24
																										}),
																										"type": "left"
																									})
																								}),
																								"string2": functions.concat({
																									"string1": "Date",
																									"string2": functions.padString({
																										"len": functions.sub({
																											"value1": functions.getVar({
																												"var": "center"
																											}),
																											"value2": 12
																										}),
																										"char": " "
																									}),
																									"string3": ": ",
																									"string4": ""
																								}),
																								"string3": functions.formatDate({
																									"date": functions.getVarAttr({
																										"var": "invoice",
																										"attr": "Posting_Date"
																									}),
																									"format": "d/m/Y"
																								}),
																								"string4": null
																							}),
																							"string4": functions.concat({
																								"string1": functions.concat({
																									"string1": functions.padString({
																										"char": " ",
																										"len": functions.getVar({
																											"var": "margin_left"
																										}),
																										"type": "left"
																									}),
																									"string2": functions.concat({
																										"string1": "Salesman",
																										"string2": functions.padString({
																											"len": functions.sub({
																												"value1": functions.getVar({
																													"var": "center"
																												}),
																												"value2": 16
																											}),
																											"char": " "
																										}),
																										"string3": ": ",
																										"string4": functions.concat({
																											"string1": functions.getVarAttr({
																												"var": "vInvoiceDetails",
																												"attr": "Location_Code"
																											}),
																											"string2": "",
																											"string3": null
																										})
																									}),
																									"string3": "",
																									"string4": ""
																								}),
																								"string2": functions.concat({
																									"string1": functions.padString({
																										"len": functions.getVar({
																											"var": "margin_left"
																										}),
																										"type": "left",
																										"char": " "
																									}),
																									"string2": functions.concat({
																										"string1": "Terms",
																										"string2": functions.padString({
																											"string": "",
																											"len": functions.sub({
																												"value1": functions.getVar({
																													"var": "width"
																												}),
																												"value2": 32
																											}),
																											"type": "",
																											"char": ""
																										}),
																										"string3": ": ",
																										"string4": ""
																									}),
																									"string3": functions.getVarAttr({
																										"var": "vInvoiceDetails",
																										"attr": "Payment_Terms_Code"
																									}),
																									"string4": ""
																								}),
																								"string3": null,
																								"string4": functions.concat({
																									"string1": functions.padString({
																										"len": functions.getVar({
																											"var": "margin_left"
																										}),
																										"type": "left",
																										"char": " "
																									}),
																									"string2": functions.concat({
																										"string1": "Customer",
																										"string2": functions.padString({
																											"len": functions.sub({
																												"value1": functions.getVar({
																													"var": "center"
																												}),
																												"value2": 16
																											}),
																											"char": " "
																										}),
																										"string3": functions.concat({
																											"string1": ": ",
																											"string2": functions.getVarAttr({
																												"var": "vInvoiceDetails",
																												"attr": "Bill_to_Customer_No"
																											}),
																											"string3": null,
																											"string4": functions.concat({
																												"string1": functions.concat({
																													"string1": null,
																													"string2": functions.getVarAttr({
																														"var": "vInvoiceDetails",
																														"attr": "Bill_to_Name"
																													}),
																													"string3": null,
																													"string4": ""
																												}),
																												"string3": functions.conditional({
																													"condition": functions.getVarAttr({
																														"var": "vInvoiceDetails",
																														"attr": "Sell_to_Address"
																													}),
																													"yesValue": functions.concat({
																														"string1": "",
																														"string2": functions.getVarAttr({
																															"var": "vInvoiceDetails",
																															"attr": "Bill_to_Address"
																														}),
																														"string3": null,
																														"string4": ""
																													}),
																													"noValue": "",
																													"yesCallback": ""
																												})
																											})
																										}),
																										"string4": ""
																									}),
																									"string3": "",
																									"string4": ""
																								})
																							})
																						})
																					}),
																					"value3": functions.setVar({
																						"var": "vContentHeader",
																						"value": functions.concat({
																							"string1": functions.concat({
																								"string1": functions.concat({
																									"string1": functions.padString({
																										"len": functions.getVar({
																											"var": "left_margin"
																										}),
																										"type": "left",
																										"char": " "
																									}),
																									"string2": null,
																									"string3": ""
																								}),
																								"string2": functions.concat({
																									"string1": functions.concat({
																										"string1": functions.padString({
																											"len": functions.getVar({
																												"var": "margin_left"
																											}),
																											"type": "left",
																											"char": " "
																										}),
																										"string2": functions.padString({
																											"len": functions.getVar({
																												"var": "width"
																											}),
																											"type": "left",
																											"char": "-"
																										}),
																										"string3": null
																									}),
																									"string2": functions.concat({
																										"string1": functions.concat({
																											"string1": functions.padString({
																												"len": functions.getVar({
																													"var": "left_margin"
																												}),
																												"type": "left",
																												"char": " "
																											}),
																											"string2": functions.padString({
																												"string": "No",
																												"len": 3,
																												"type": "left"
																											}),
																											"string3": functions.padString({
																												"string": "Description",
																												"len": 12,
																												"type": "left"
																											}),
																											"string4": functions.concat({
																												"string1": functions.padString({
																													"string": "Price",
																													"len": 6,
																													"type": "left",
																													"char": ""
																												}),
																												"string2": functions.padString({
																													"string": "UOM",
																													"len": 4,
																													"type": "left",
																													"char": ""
																												}),
																												"string3": functions.padString({
																													"string": "Qty.",
																													"len": 9,
																													"type": "left",
																													"char": ""
																												}),
																												"string4": functions.concat({
																													"string1": functions.padString({
																														"string": "Amount",
																														"len": 10,
																														"type": "left",
																														"char": ""
																													}),
																													"string2": null
																												})
																											})
																										}),
																										"string2": functions.concat({
																											"string1": functions.padString({
																												"len": functions.getVar({
																													"var": "margin_left"
																												}),
																												"type": "left",
																												"char": " "
																											}),
																											"string2": functions.padString({
																												"len": functions.getVar({
																													"var": "width"
																												}),
																												"type": "left",
																												"char": "-"
																											}),
																											"string3": null
																										})
																									})
																								})
																							}),
																							"string2": "",
																							"string3": "",
																							"string4": ""
																						})
																					}),
																					"value4": functions.setVar({
																						"var": "vContentBody",
																						"value": ""
																					}),
																					"value5": functions.setVar({
																						"var": "vInvoicedLineCounter",
																						"value": 1
																					}),
																					"value6": "",
																					"value7": functions.setVar({
																						"var": "vFooterText",
																						"value": functions.concat({
																							"string1": functions.concat({
																								"string1": null,
																								"string2": null,
																								"string3": null,
																								"string4": ""
																							})
																						})
																					}),
																					"value8": functions.setVar({
																						"var": "vFooterLast",
																						"value": functions.concat({
																							"string1": functions.concat({
																								"string1": functions.padString({
																									"string": "",
																									"len": functions.getVar({
																										"var": "left_Margin"
																									}),
																									"type": "left"
																								}),
																								"string2": functions.padString({
																									"string": "",
																									"len": 25,
																									"type": "left",
																									"char": "-"
																								}),
																								"string3": ""
																							}),
																							"string2": functions.concat({
																								"string1": functions.concat({
																									"string1": null,
																									"string2": functions.padString({
																										"string": "SIGNATURE & COMPANY CHOP",
																										"len": functions.getVar({
																											"var": "width"
																										}),
																										"type": "left"
																									}),
																									"string3": "",
																									"string4": ""
																								})
																							}),
																							"string3": functions.concat({
																								"string1": functions.concat({
																									"string1": null,
																									"string2": null,
																									"string3": functions.concat({
																										"string1": functions.padString({
																											"string": "THIS IS A COMPUTER GENERATED INVOICE",
																											"len": functions.getVar({
																												"var": "width"
																											}),
																											"type": "left"
																										}),
																										"string2": null,
																										"string3": functions.padString({
																											"string": "NO SIGNATURE REQUIRED.",
																											"len": functions.getVar({
																												"var": "width"
																											}),
																											"type": "left"
																										}),
																										"string4": functions.concat({
																											"string1": null,
																											"string2": null
																										})
																									}),
																									"string4": ""
																								})
																							}),
																							"string4": functions.concat({
																								"string1": functions.concat({
																									"string1": functions.padString({
																										"len": functions.getVar({
																											"var": "margin_left"
																										}),
																										"char": " "
																									}),
																									"string2": functions.padString({
																										"type": "left",
																										"string": functions.concat({
																											"string1": "Printed : ",
																											"string2": functions.formatDate({
																												"date": functions.dbDate({
																													"withTime": true
																												}),
																												"format": "d/m/Y H:i:s "
																											})
																										}),
																										"len": functions.getVar({
																											"var": "width"
																										})
																									}),
																									"string3": null,
																									"string4": functions.concat({
																										"string1": null,
																										"string2": null
																									})
																								}),
																								"string2": "",
																								"string3": ""
																							})
																						})
																					}),
																					"value9": functions.setVar({
																						"var": "vFooterSignature",
																						"value": functions.concat({
																							"string1": "[img|x=10|y=10|width=200|height=100]",
																							"string2": functions.getVar({
																								"var": "vSignature"
																							}),
																							"string3": "[/img]",
																							"string4": functions.concat({
																								"string1": null,
																								"string2": null,
																								"string3": null,
																								"string4": functions.concat({
																									"string1": null,
																									"string2": "",
																									"string3": "",
																									"string4": ""
																								})
																							})
																						})
																					}),
																					"value10": functions.setVar({
																						"var": "vInvoicedLineCount",
																						"value": functions.count({
																							"values": functions.getVar({
																								"var": "vInvoicedLines"
																							})
																						})
																					})
																				});
																				functions.setVar({
																					"var": "vInvoicedLinesCount",
																					"value": functions.count({
																						"values": functions.getVar({
																							"var": "vInvoicedLines"
																						})
																					})
																				});
																				functions.conditional({
																					"condition": functions.greater({
																						"value1": functions.getVar({
																							"var": "vInvoicedLinesCount"
																						}),
																						"value2": 1
																					}),
																					"yesCallback": async (data, extra) => {
																						functions.setVar({
																							"var": "vFooterTotal",
																							"value": functions.concat({
																								"string1": null,
																								"string2": null,
																								"string3": functions.concat({
																									"string1": functions.padString({
																										"string": functions.concat({
																											"string1": "Sub-Total",
																											"string2": ""
																										}),
																										"len": 27,
																										"type": "right",
																										"char": ""
																									}),
																									"string2": functions.padString({
																										"string": functions.concat({
																											"string1": functions.formatNumber({
																												"value": functions.objectAttr({
																													"object": functions.objectAttr({
																														"object": functions.objectAttr({
																															"object": functions.getVarAttr({
																																"var": "vInvoiceDetails",
																																"attr": "SalesInvLines"
																															}),
																															"attr": "Posted_Sales_Invoice_Line"
																														}),
																														"attr": 0
																													}),
																													"attr": "Total_Amount_Excl_VAT"
																												}),
																												"decimals": 2,
																												"decimalSep": ".",
																												"thousandSep": ","
																											}),
																											"string2": ""
																										}),
																										"len": 11,
																										"type": "right"
																									}),
																									"string3": "",
																									"string4": ""
																								}),
																								"string4": functions.concat({
																									"string1": null,
																									"string2": functions.concat({
																										"string1": functions.padString({
																											"string": functions.concat({
																												"string1": "GST ",
																												"string2": functions.padString({
																													"len": 3
																												}),
																												"string3": functions.getVar({
																													"var": "vGSTValue"
																												}),
																												"string4": "%"
																											}),
																											"len": 27,
																											"type": "right",
																											"char": ""
																										}),
																										"string2": functions.padString({
																											"string": functions.concat({
																												"string1": functions.formatNumber({
																													"decimals": 2,
																													"decimalSep": ".",
																													"thousandSep": ",",
																													"value": functions.objectAttr({
																														"object": functions.objectAttr({
																															"object": functions.objectAttr({
																																"attr": "Posted_Sales_Invoice_Line",
																																"object": functions.getVarAttr({
																																	"var": "vInvoiceDetails",
																																	"attr": "SalesInvLines"
																																})
																															}),
																															"attr": 0
																														}),
																														"attr": "Total_VAT_Amount"
																													})
																												}),
																												"string2": ""
																											}),
																											"len": 11,
																											"type": "right"
																										})
																									}),
																									"string3": null,
																									"string4": functions.concat({
																										"string1": functions.padString({
																											"string": " ",
																											"len": 18,
																											"type": "left"
																										}),
																										"string2": "[size='18pt'][b]",
																										"string3": functions.concat({
																											"string1": functions.concat({
																												"string1": "TOTAL",
																												"string2": ""
																											}),
																											"string2": functions.padString({
																												"string": functions.concat({
																													"string1": functions.formatNumber({
																														"value": functions.objectAttr({
																															"object": functions.objectAttr({
																																"object": functions.objectAttr({
																																	"object": functions.getVarAttr({
																																		"var": "vInvoiceDetails",
																																		"attr": "SalesInvLines"
																																	}),
																																	"attr": "Posted_Sales_Invoice_Line"
																																}),
																																"attr": 0
																															}),
																															"attr": "Total_Amount_Incl_VAT"
																														}),
																														"decimals": 2,
																														"decimalSep": ".",
																														"thousandSep": ","
																													}),
																													"string2": ""
																												}),
																												"len": 14,
																												"type": "right"
																											}),
																											"string3": "",
																											"string4": ""
																										}),
																										"string4": "[/b][/size]"
																									})
																								})
																							})
																						});
																						functions.map({
																							"values": functions.getVar({
																								"var": "vInvoicedLines"
																							}),
																							"callback": async (data, extra) => {
																								functions.setVar({
																									"var": "mapInvoicedLines",
																									"value": data
																								});
																								functions.setVar({
																									"var": "vContentBody",
																									"value": functions.concat({
																										"string1": functions.getVar({
																											"var": "vContentBody"
																										}),
																										"string2": functions.padString({
																											"len": functions.getVar({
																												"var": "left_margin"
																											}),
																											"type": "left",
																											"char": " "
																										}),
																										"string3": functions.concat({
																											"string1": "",
																											"string2": functions.concat({
																												"string1": null,
																												"string2": " - ",
																												"string3": functions.conditional({
																													"condition": functions.equal({
																														"value1": functions.getVarAttr({
																															"var": "mapInvoicedLines",
																															"attr": "No"
																														}),
																														"value2": "510-1015"
																													}),
																													"yesValue": "Empty Bottles",
																													"noValue": functions.getVarAttr({
																														"var": "mapInvoicedLines",
																														"attr": "Description"
																													})
																												})
																											}),
																											"string3": ""
																										}),
																										"string4": functions.concat({
																											"string1": functions.concat({
																												"string1": null,
																												"string2": functions.padString({
																													"len": 6,
																													"type": "left",
																													"char": " "
																												}),
																												"string3": functions.absolute({
																													"value": functions.getVarAttr({
																														"var": "mapInvoicedLines",
																														"attr": "Quantity"
																													})
																												}),
																												"string4": functions.concat({
																													"string1": functions.padString({
																														"string": "",
																														"len": 1,
																														"type": "left",
																														"char": " "
																													}),
																													"string2": functions.padString({
																														"char": "",
																														"string": functions.conditional({
																															"condition": functions.equal({
																																"value1": functions.getVarAttr({
																																	"var": "mapInvoicedLines",
																																	"attr": "No"
																																}),
																																"value2": "510-1015"
																															}),
																															"yesValue": "PCS",
																															"noValue": functions.getVarAttr({
																																"var": "mapInvoicedLines",
																																"attr": "Unit_of_Measure"
																															})
																														}),
																														"len": 8,
																														"type": "left"
																													}),
																													"string3": functions.padString({
																														"len": 12,
																														"type": "right",
																														"char": " ",
																														"string": "$ "
																													}),
																													"string4": ""
																												})
																											}),
																											"string2": functions.concat({
																												"string1": functions.padString({
																													"char": " ",
																													"string": functions.formatNumber({
																														"value": functions.getVarAttr({
																															"var": "mapInvoicedLines",
																															"attr": "Line_Amount"
																														}),
																														"decimals": 2,
																														"decimalSep": ".",
																														"thousandSep": ","
																													}),
																													"len": 10,
																													"type": "right"
																												})
																											}),
																											"string3": null,
																											"string4": null
																										})
																									})
																								});
																								functions.setVar({
																									"var": "vInvoicedLineCounter",
																									"value": functions.add({
																										"value1": functions.getVar({
																											"var": "vInvoicedLineCounter"
																										}),
																										"value2": 1
																									})
																								});
																								functions.conditional({
																									"condition": functions.greater({
																										"value1": functions.getVar({
																											"var": "vInvoicedLineCounter"
																										}),
																										"value2": functions.getVar({
																											"var": "vInvoicedLineCount"
																										})
																									}),
																									"yesCallback": async (data, extra) => {
																										functions.setVar({
																											"var": "vOutput",
																											"value": functions.concat({
																												"string1": functions.getVar({
																													"var": "vHeader"
																												}),
																												"string2": functions.getVar({
																													"var": "vSubHeader"
																												}),
																												"string3": functions.concat({
																													"string1": functions.concat({
																														"string1": null,
																														"string2": functions.padString({
																															"len": functions.getVar({
																																"var": "width"
																															}),
																															"type": "left",
																															"char": "-"
																														}),
																														"string3": null
																													}),
																													"string2": null,
																													"string3": functions.concat({
																														"string1": null,
																														"string2": functions.padString({
																															"len": functions.getVar({
																																"var": "width"
																															}),
																															"type": "left",
																															"char": "-"
																														}),
																														"string3": null
																													})
																												}),
																												"string4": functions.concat({
																													"string1": functions.getVar({
																														"var": "vContentBody"
																													}),
																													"string2": "",
																													"string3": functions.getVar({
																														"var": "vFooterTotal"
																													}),
																													"string4": functions.concat({
																														"string1": functions.getVar({
																															"var": "vFooterText"
																														}),
																														"string2": null,
																														"string3": "",
																														"string4": ""
																													})
																												})
																											})
																										});
																										functions.console({
																											"value": functions.getVar({
																												"var": "vOutput"
																											})
																										});
																									},
																									"noCallback": ""
																								});
																							}
																						});
																					},
																					"noCallback": async (data, extra) => {
																						functions.setVar({
																							"var": "vFooterTotal",
																							"value": functions.concat({
																								"string1": null,
																								"string2": null,
																								"string3": functions.concat({
																									"string1": functions.padString({
																										"string": functions.concat({
																											"string1": "Sub-Total",
																											"string2": ""
																										}),
																										"len": 27,
																										"type": "right",
																										"char": ""
																									}),
																									"string2": functions.padString({
																										"string": functions.concat({
																											"string1": functions.formatNumber({
																												"thousandSep": ",",
																												"value": functions.objectAttr({
																													"object": functions.objectAttr({
																														"object": functions.getVarAttr({
																															"var": "vInvoiceDetails",
																															"attr": "SalesInvLines"
																														}),
																														"attr": "Posted_Sales_Invoice_Line"
																													}),
																													"attr": "Total_Amount_Excl_VAT"
																												}),
																												"decimals": 2,
																												"decimalSep": "."
																											}),
																											"string2": ""
																										}),
																										"len": 11,
																										"type": "right"
																									}),
																									"string3": "",
																									"string4": ""
																								}),
																								"string4": functions.concat({
																									"string1": null,
																									"string2": functions.concat({
																										"string1": functions.padString({
																											"string": functions.concat({
																												"string1": "GST ",
																												"string2": functions.padString({
																													"len": 3
																												}),
																												"string3": functions.getVar({
																													"var": "vGSTValue"
																												}),
																												"string4": "%"
																											}),
																											"len": 27,
																											"type": "right",
																											"char": ""
																										}),
																										"string2": functions.padString({
																											"string": functions.concat({
																												"string1": functions.formatNumber({
																													"value": functions.objectAttr({
																														"object": functions.objectAttr({
																															"object": functions.getVarAttr({
																																"var": "vInvoiceDetails",
																																"attr": "SalesInvLines"
																															}),
																															"attr": "Posted_Sales_Invoice_Line"
																														}),
																														"attr": "Total_VAT_Amount"
																													}),
																													"decimals": 2,
																													"decimalSep": ".",
																													"thousandSep": ","
																												}),
																												"string2": ""
																											}),
																											"len": 11,
																											"type": "right"
																										})
																									}),
																									"string3": null,
																									"string4": functions.concat({
																										"string1": functions.padString({
																											"string": " ",
																											"len": 18,
																											"type": "left"
																										}),
																										"string2": "[size='18pt'][b]",
																										"string3": functions.concat({
																											"string1": functions.concat({
																												"string1": "TOTAL",
																												"string2": ""
																											}),
																											"string2": functions.padString({
																												"string": functions.concat({
																													"string1": functions.formatNumber({
																														"value": functions.objectAttr({
																															"object": functions.objectAttr({
																																"object": functions.getVarAttr({
																																	"var": "vInvoiceDetails",
																																	"attr": "SalesInvLines"
																																}),
																																"attr": "Posted_Sales_Invoice_Line"
																															}),
																															"attr": "Total_Amount_Incl_VAT"
																														}),
																														"decimals": 2,
																														"decimalSep": ".",
																														"thousandSep": ","
																													}),
																													"string2": ""
																												}),
																												"len": 14,
																												"type": "right"
																											}),
																											"string3": "",
																											"string4": ""
																										}),
																										"string4": "[/b][/size]"
																									})
																								})
																							})
																						});
																						functions.setVar({
																							"var": "mapInvoicedLines",
																							"value": functions.getVar({
																								"var": "vInvoicedLines"
																							})
																						});
																						functions.setVar({
																							"var": "vContentBody",
																							"value": functions.concat({
																								"string1": functions.getVar({
																									"var": "vContentBody"
																								}),
																								"string2": functions.padString({
																									"type": "left",
																									"char": " ",
																									"len": functions.getVar({
																										"var": "left_margin"
																									})
																								}),
																								"string3": functions.concat({
																									"string1": "",
																									"string2": functions.concat({
																										"string1": null,
																										"string2": " - ",
																										"string3": functions.conditional({
																											"condition": functions.equal({
																												"value1": functions.getVarAttr({
																													"var": "mapInvoicedLines",
																													"attr": "No"
																												}),
																												"value2": "510-1015"
																											}),
																											"yesValue": "Empty Bottles",
																											"noValue": functions.getVarAttr({
																												"var": "mapInvoicedLines",
																												"attr": "Description"
																											})
																										})
																									}),
																									"string3": ""
																								}),
																								"string4": functions.concat({
																									"string1": functions.concat({
																										"string1": null,
																										"string2": functions.padString({
																											"len": 6,
																											"type": "left",
																											"char": " "
																										}),
																										"string3": functions.absolute({
																											"value": functions.getVarAttr({
																												"var": "mapInvoicedLines",
																												"attr": "Quantity"
																											})
																										}),
																										"string4": functions.concat({
																											"string1": functions.padString({
																												"string": "",
																												"len": 1,
																												"type": "left",
																												"char": " "
																											}),
																											"string2": functions.padString({
																												"string": functions.conditional({
																													"condition": functions.equal({
																														"value1": functions.getVarAttr({
																															"var": "mapInvoicedLines",
																															"attr": "No"
																														}),
																														"value2": "510-1015"
																													}),
																													"yesValue": "PCS",
																													"noValue": functions.getVarAttr({
																														"var": "mapInvoicedLines",
																														"attr": "Unit_of_Measure"
																													})
																												}),
																												"len": 8,
																												"type": "left",
																												"char": ""
																											}),
																											"string3": functions.padString({
																												"string": "$ ",
																												"len": 12,
																												"type": "right",
																												"char": " "
																											}),
																											"string4": ""
																										})
																									}),
																									"string2": functions.concat({
																										"string1": functions.padString({
																											"string": functions.formatNumber({
																												"value": functions.getVarAttr({
																													"var": "mapInvoicedLines",
																													"attr": "Line_Amount"
																												}),
																												"decimals": 2,
																												"decimalSep": ".",
																												"thousandSep": ","
																											}),
																											"len": 10,
																											"type": "right",
																											"char": " "
																										})
																									}),
																									"string3": null,
																									"string4": null
																								})
																							})
																						});
																						functions.setVar({
																							"var": "vOutput",
																							"value": functions.concat({
																								"string1": functions.getVar({
																									"var": "vHeader"
																								}),
																								"string2": functions.getVar({
																									"var": "vSubHeader"
																								}),
																								"string3": functions.concat({
																									"string1": functions.concat({
																										"string1": null,
																										"string2": functions.padString({
																											"char": "-",
																											"len": functions.getVar({
																												"var": "width"
																											}),
																											"type": "left"
																										}),
																										"string3": null
																									}),
																									"string2": null,
																									"string3": functions.concat({
																										"string1": null,
																										"string2": functions.padString({
																											"len": functions.getVar({
																												"var": "width"
																											}),
																											"type": "left",
																											"char": "-"
																										}),
																										"string3": null
																									})
																								}),
																								"string4": functions.concat({
																									"string1": functions.getVar({
																										"var": "vContentBody"
																									}),
																									"string2": "",
																									"string3": functions.getVar({
																										"var": "vFooterTotal"
																									}),
																									"string4": functions.concat({
																										"string1": functions.getVar({
																											"var": "vFooterText"
																										}),
																										"string2": null,
																										"string3": "",
																										"string4": ""
																									})
																								})
																							})
																						});
																						functions.console({
																							"value": functions.getVar({
																								"var": "vOutput"
																							})
																						});
																					}
																				});
																				functions.toArray({
																					"value1": "",
																					"value2": functions.setVar({
																						"var": "vOutput2",
																						"value": functions.concat({
																							"string1": functions.getVar({
																								"var": "vFooterSignature"
																							}),
																							"string2": functions.getVar({
																								"var": "vFooterLast"
																							}),
																							"string3": "",
																							"string4": ""
																						})
																					}),
																					"value3": functions.console({
																						"value": functions.getVar({
																							"var": "vOutput2"
																						})
																					})
																				});
																				functions.setLanguage({
																					"lang": functions.getVar({
																						"var": "vTempLang"
																					})
																				});
																				functions.console({
																					"value": "************ END RECEIPT *********"
																				});
																				functions.setVar({
																					"var": "vInvoicedLinesCount"
																				});
																			},
																			"errCallback": async (data, extra) => {
																				functions.console({
																					"value": data["err"]
																				});
																				functions.userDefined('globalModalInfo', {
																					"title": "Connection Error",
																					"message": "There's an error connecting to Navision Web Services (Create Cash Receipt)"
																				});
																				functions.userDefined('globalModalHide', {});
																			},
																			"connector": "nav",
																			"ent": "OK365_CashReceipt",
																			"function": "Create",
																			"subfunction": ""
																		});
																	},
																	"errCallback": async (data, extra) => {
																		functions.userDefined('globalModalInfo', {
																			"title": "Connection Error",
																			"message": "There's an error connecting to Navision Web Services (Load Company Information)"
																		});
																		functions.userDefined('globalModalHide', {});
																	}
																});
															},
															"errCallback": async (data, extra) => {
																functions.userDefined('globalModalInfo', {
																	"message": "There's an error connecting to Navision Web Services (Get Signature)",
																	"title": "Connection Error"
																});
																functions.userDefined('globalModalHide', {});
															}
														});
														functions.console({
															"value": functions.getVar({
																"var": "vSignature"
															})
														});
													}
												});
											}
										});
									},
									"errCallback": async (data, extra) => {
										functions.console({
											"value": "no"
										});
										functions.userDefined('globalModalInfo', {
											"title": "Connection Error",
											"message": "There's an error connecting to Navision Web Services (Posting Invoice)"
										});
										functions.userDefined('globalModalHide', {});
									},
									"connector": "nav",
									"type": "",
									"ent": "OK365_GetItemSalesPrice",
									"function": "g_fnSalesPostOrder",
									"subfunction": "",
									"data": functions.toObject({
										"blnInvoice": "true",
										"optDocType": "Order",
										"cdeSalesOrdNo": functions.getVar({
											"var": "vOrderNo"
										}),
										"blnShip": "true"
									})
								});
							},
							"errCallback": async (data, extra) => {
								functions.userDefined('globalModalInfo', {
									"title": "Connection Error",
									"message": "There's an error connecting to Navision Web Services (Update Order Entry)"
								});
								functions.userDefined('globalModalHide', {});
							},
							"connector": "nav",
							"type": "",
							"ent": "OK365_Order_Entry",
							"function": "Update",
							"subfunction": ""
						});
					},
					"errCallback": async (data, extra) => {
						functions.userDefined('globalModalInfo', {
							"title": "Connection Error",
							"message": "There's an error connecting to Navision Web Services (Load Order Entry)"
						});
						functions.userDefined('globalModalHide', {});
					}
				});
			},
			"noCallback": async (data, extra) => {
				functions.conditional({
					"condition": functions.equal({
						"value1": functions.getVar({
							"var": "vAction"
						}),
						"value2": "postInvoiceWithoutPayment"
					}),
					"yesCallback": async (data, extra) => {
						functions.userDefined('globalModalLoading', {});
						await functions.loadData({
							"dataset": "OK365_Order_Entry",
							"limit": 1,
							"filter": functions.toArray({
								"value1": functions.toObject({
									"No": functions.getVar({
										"var": "vOrderNo"
									})
								})
							}),
							"callback": async (data, extra) => {
								functions.setVar({
									"var": "vOrderData",
									"value": functions.objectAttr({
										"object": data,
										"attr": 0
									})
								});
								await functions.navCall({
									"function": "Update",
									"data": functions.toObject({
										"Key": functions.getVarAttr({
											"var": "vOrderData",
											"attr": "Key"
										}),
										"No": functions.getVarAttr({
											"var": "vOrderData",
											"attr": "No"
										}),
										"Posting_Date": null
									}),
									"callback": async (data, extra) => {
										functions.console({
											"value": "Invoice Only - Posting Date updated"
										});
										await functions.navCall({
											"callback": async (data, extra) => {
												functions.console({
													"value": "1. Post Sales Invoice"
												});
												functions.console({
													"value": data
												});
												await functions.loadData({
													"dataset": "OK365_Posted_Sales_Invoice_Header",
													"filter": functions.toArray({
														"value1": functions.toObject({
															"f": "Order_No",
															"o": "=",
															"v": functions.getVar({
																"var": "vOrderNo"
															})
														})
													}),
													"callback": async (data, extra) => {
														functions.console({
															"value": "2. Generate Tax Invoice (Receipt)"
														});
														functions.console({
															"value": data
														});
														functions.setVar({
															"var": "vPostedInvoice",
															"value": functions.objectAttr({
																"object": data,
																"attr": 0
															})
														});
														functions.setVar({
															"var": "vInvoiceDetails",
															"value": data[0]
														});
														functions.setVar({
															"var": "vInvoicedLines",
															"value": functions.objectAttr({
																"object": functions.objectAttr({
																	"object": functions.getVar({
																		"var": "vInvoiceDetails"
																	}),
																	"attr": "SalesInvLines"
																}),
																"attr": "Posted_Sales_Invoice_Line"
															})
														});
														functions.setVar({
															"var": "vAction",
															"value": "invoice_posted"
														});
														functions.userDefined('globalModalInfo', {
															"title": "Completed",
															"message": functions.concat({
																"string1": "<b><h3>",
																"string2": functions.getVarAttr({
																	"var": "vPostedInvoice",
																	"attr": "No"
																}),
																"string3": "</b></h3>",
																"string4": functions.concat({
																	"string1": "New Invoice number",
																	"string2": "<br>",
																	"string3": "Your order has been successfully posted."
																})
															})
														});
														await functions.navCall({
															"errCallback": async (data, extra) => {
																functions.console({
																	"value": "no"
																});
																functions.userDefined('globalModalInfo', {
																	"title": "Connection Error",
																	"message": "There's an error connecting to Navision Web Services (Set Signature)"
																});
																functions.userDefined('globalModalHide', {});
															},
															"connector": "nav",
															"ent": "OK365_GetItemSalesPrice",
															"function": "SetPostedSalesInvoiceSignature",
															"data": functions.toObject({
																"postedSalesInvoiceNo": functions.getVarAttr({
																	"var": "vPostedInvoice",
																	"attr": "No"
																}),
																"signaturePicture": functions.replace({
																	"string": functions.getVar({
																		"var": "vSignature"
																	}),
																	"replace": "data:image/png;base64,"
																})
															}),
															"callback": async (data, extra) => {
																functions.console({
																	"value": "1. Posted Sales Invoice Result"
																});
																await functions.navCall({
																	"errCallback": async (data, extra) => {
																		functions.userDefined('globalModalInfo', {
																			"title": "Connection Error",
																			"message": "There's an error connecting to Navision Web Services (Get Signature)"
																		});
																		functions.userDefined('globalModalHide', {});
																	},
																	"connector": "nav",
																	"ent": "OK365_GetItemSalesPrice",
																	"function": "GetPostedSalesInvoiceSignature",
																	"data": functions.toObject({
																		"postedSalesInvoiceNo": functions.getVarAttr({
																			"var": "vPostedInvoice",
																			"attr": "No"
																		})
																	}),
																	"callback": async (data, extra) => {
																		functions.setVar({
																			"var": "vSignature",
																			"value": functions.objectAttr({
																				"object": data,
																				"attr": "return_value"
																			})
																		});
																		functions.console({
																			"value": data
																		});
																		functions.console({
																			"value": "output result for signature"
																		});
																		await functions.loadData({
																			"errCallback": async (data, extra) => {
																				functions.userDefined('globalModalInfo', {
																					"title": "Connection Error",
																					"message": "There's an error connecting to Navision Web Services (Load Company Information)"
																				});
																				functions.userDefined('globalModalHide', {});
																			},
																			"dataset": "OK365_Company_Information",
																			"filter": "",
																			"callback": async (data, extra) => {
																				functions.setVar({
																					"var": "vTempLang",
																					"value": functions.getLanguage({})
																				});
																				functions.setLanguage({
																					"lang": "English"
																				});
																				functions.setVar({
																					"var": "vCompany",
																					"value": data[0]
																				});
																				functions.console({
																					"value": functions.getVar({
																						"var": "vCompany"
																					})
																				});
																				functions.console({
																					"value": "************ START RECEIPT *********"
																				});
																				functions.toArray({
																					"value1": functions.setVar({
																						"var": "vGSTValue",
																						"value": functions.getVar({
																							"var": "vGstTotalRate"
																						})
																					}),
																					"value2": functions.setVar({
																						"var": "width",
																						"value": 38
																					}),
																					"value3": functions.setVar({
																						"var": "center",
																						"value": 19
																					}),
																					"value4": functions.setVar({
																						"var": "margin_left",
																						"value": 0
																					}),
																					"value5": functions.setVar({
																						"var": "width_center",
																						"value": 10
																					})
																				});
																				functions.toArray({
																					"value1": functions.setVar({
																						"var": "vHeader",
																						"value": functions.concat({
																							"string1": functions.concat({
																								"string1": functions.padString({
																									"char": " ",
																									"len": functions.getVar({
																										"var": "margin_left"
																									}),
																									"type": "left"
																								}),
																								"string2": functions.concat({
																									"string1": "[size='24pt']",
																									"string2": functions.padString({
																										"string": functions.getVarAttr({
																											"var": "vCompany",
																											"attr": "Name"
																										}),
																										"len": 30,
																										"type": "center"
																									}),
																									"string3": "[/size]",
																									"string4": ""
																								}),
																								"string3": null,
																								"string4": ""
																							}),
																							"string2": null,
																							"string3": functions.concat({
																								"string1": functions.concat({
																									"string1": functions.padString({
																										"char": " ",
																										"len": functions.getVar({
																											"var": "left_margin"
																										}),
																										"type": "left"
																									}),
																									"string2": functions.padString({
																										"string": functions.concat({
																											"string1": functions.getVarAttr({
																												"var": "vCompany",
																												"attr": "Address"
																											}),
																											"string2": "",
																											"string3": functions.getVarAttr({
																												"var": "vCompany",
																												"attr": "Address2"
																											}),
																											"string4": ""
																										}),
																										"len": functions.getVar({
																											"var": "width"
																										}),
																										"type": "center"
																									}),
																									"string3": null
																								}),
																								"string2": functions.concat({
																									"string1": functions.padString({
																										"len": functions.getVar({
																											"var": "left_margin"
																										}),
																										"type": "left",
																										"char": " "
																									}),
																									"string2": functions.padString({
																										"string": functions.concat({
																											"string1": functions.getVarAttr({
																												"var": "vCompany",
																												"attr": "City"
																											}),
																											"string2": " ",
																											"string3": functions.getVarAttr({
																												"var": "vCompany",
																												"attr": "Post_Code"
																											})
																										}),
																										"len": functions.getVar({
																											"var": "width"
																										}),
																										"type": "center"
																									}),
																									"string3": null
																								}),
																								"string3": functions.concat({
																									"string1": functions.padString({
																										"len": functions.sub({
																											"value1": functions.getVar({
																												"var": "center"
																											}),
																											"value2": 16
																										}),
																										"type": "center",
																										"char": " "
																									}),
																									"string2": functions.concat({
																										"string1": "Tel",
																										"string2": ": ",
																										"string3": functions.padString({
																											"string": functions.getVarAttr({
																												"var": "vCompany",
																												"attr": "Phone_No"
																											}),
																											"len": 10,
																											"type": "left",
																											"char": ""
																										}),
																										"string4": ""
																									}),
																									"string3": "",
																									"string4": functions.concat({
																										"string1": "Fax",
																										"string2": ": ",
																										"string3": functions.padString({
																											"string": 63826045,
																											"len": 10,
																											"type": "left",
																											"char": ""
																										}),
																										"string4": functions.concat({
																											"string1": null,
																											"string2": null
																										})
																									})
																								}),
																								"string4": functions.concat({
																									"string1": functions.concat({
																										"string1": functions.padString({
																											"len": functions.sub({
																												"value1": functions.getVar({
																													"var": "center"
																												}),
																												"value2": 11
																											}),
																											"type": "center",
																											"char": " "
																										}),
																										"string2": "CO.REG.No",
																										"string3": ": ",
																										"string4": functions.padString({
																											"string": functions.getVarAttr({
																												"var": "vCompany",
																												"attr": "VAT_Registration_No"
																											}),
																											"len": functions.sub({
																												"value1": functions.getVar({
																													"var": "width_center"
																												}),
																												"value2": 10
																											}),
																											"type": "center"
																										})
																									}),
																									"string2": null,
																									"string3": functions.concat({
																										"string1": functions.padString({
																											"type": "center",
																											"char": " ",
																											"len": functions.sub({
																												"value1": functions.getVar({
																													"var": "center"
																												}),
																												"value2": 12
																											})
																										}),
																										"string2": "GST.REG.No",
																										"string3": ": ",
																										"string4": functions.padString({
																											"type": "center",
																											"string": functions.getVarAttr({
																												"var": "vCompany",
																												"attr": "VAT_Registration_No"
																											}),
																											"len": functions.sub({
																												"value1": functions.getVar({
																													"var": "width_center"
																												}),
																												"value2": 10
																											})
																										})
																									})
																								})
																							}),
																							"string4": ""
																						})
																					}),
																					"value2": functions.setVar({
																						"var": "vSubHeader",
																						"value": functions.concat({
																							"string1": functions.concat({
																								"string1": functions.concat({
																									"string1": functions.padString({
																										"len": functions.getVar({
																											"var": "left_margin"
																										}),
																										"type": "left",
																										"char": " "
																									}),
																									"string2": null,
																									"string3": ""
																								}),
																								"string2": functions.concat({
																									"string1": functions.concat({
																										"string1": functions.padString({
																											"len": functions.getVar({
																												"var": "margin_left"
																											}),
																											"type": "left",
																											"char": " "
																										}),
																										"string2": functions.padString({
																											"len": functions.getVar({
																												"var": "width"
																											}),
																											"type": "left",
																											"char": "-"
																										}),
																										"string3": null
																									}),
																									"string2": functions.concat({
																										"string1": functions.concat({
																											"string1": functions.padString({
																												"char": " ",
																												"len": functions.getVar({
																													"var": "left_margin"
																												}),
																												"type": "left"
																											}),
																											"string2": functions.padString({
																												"string": functions.concat({
																													"string1": null
																												}),
																												"len": functions.getVar({
																													"var": "width"
																												}),
																												"type": "center"
																											}),
																											"string3": null
																										}),
																										"string2": functions.concat({
																											"string1": functions.padString({
																												"len": functions.getVar({
																													"var": "margin_left"
																												}),
																												"type": "left",
																												"char": " "
																											}),
																											"string2": functions.padString({
																												"len": functions.getVar({
																													"var": "width"
																												}),
																												"type": "left",
																												"char": "-"
																											}),
																											"string3": null
																										})
																									})
																								})
																							}),
																							"string2": functions.concat({
																								"string1": functions.padString({
																									"len": functions.getVar({
																										"var": "margin_left"
																									}),
																									"type": "left",
																									"char": " "
																								}),
																								"string2": functions.concat({
																									"string1": functions.concat({
																										"string1": "Invoice No.",
																										"string2": "",
																										"string3": ""
																									}),
																									"string2": functions.padString({
																										"len": functions.sub({
																											"value1": functions.getVar({
																												"var": "center"
																											}),
																											"value2": 24
																										}),
																										"char": " "
																									}),
																									"string3": ": ",
																									"string4": ""
																								}),
																								"string3": functions.getVarAttr({
																									"var": "vInvoiceDetails",
																									"attr": "No"
																								}),
																								"string4": ""
																							}),
																							"string3": functions.concat({
																								"string1": functions.concat({
																									"string1": null,
																									"string2": functions.padString({
																										"len": functions.sub({
																											"value1": functions.getVar({
																												"var": "center"
																											}),
																											"value2": 24
																										}),
																										"type": "left",
																										"char": " ",
																										"string": ""
																									})
																								}),
																								"string2": functions.concat({
																									"string1": "Date",
																									"string2": functions.padString({
																										"len": functions.sub({
																											"value1": functions.getVar({
																												"var": "center"
																											}),
																											"value2": 12
																										}),
																										"char": " "
																									}),
																									"string3": ": ",
																									"string4": ""
																								}),
																								"string3": functions.formatDate({
																									"format": "d/m/Y",
																									"date": functions.getVarAttr({
																										"var": "invoice",
																										"attr": "Posting_Date"
																									})
																								}),
																								"string4": null
																							}),
																							"string4": functions.concat({
																								"string1": functions.concat({
																									"string1": functions.padString({
																										"len": functions.getVar({
																											"var": "margin_left"
																										}),
																										"type": "left",
																										"char": " "
																									}),
																									"string2": functions.concat({
																										"string1": "Salesman",
																										"string2": functions.padString({
																											"len": functions.sub({
																												"value1": functions.getVar({
																													"var": "center"
																												}),
																												"value2": 16
																											}),
																											"char": " "
																										}),
																										"string3": ": ",
																										"string4": functions.concat({
																											"string1": functions.getVarAttr({
																												"var": "vInvoiceDetails",
																												"attr": "Location_Code"
																											}),
																											"string2": "",
																											"string3": null
																										})
																									}),
																									"string3": "",
																									"string4": ""
																								}),
																								"string2": functions.concat({
																									"string1": functions.padString({
																										"type": "left",
																										"char": " ",
																										"len": functions.getVar({
																											"var": "margin_left"
																										})
																									}),
																									"string2": functions.concat({
																										"string1": "Terms",
																										"string2": functions.padString({
																											"string": "",
																											"len": functions.sub({
																												"value1": functions.getVar({
																													"var": "width"
																												}),
																												"value2": 32
																											}),
																											"type": "",
																											"char": ""
																										}),
																										"string3": ": ",
																										"string4": ""
																									}),
																									"string3": functions.getVarAttr({
																										"var": "vInvoiceDetails",
																										"attr": "Payment_Terms_Code"
																									}),
																									"string4": ""
																								}),
																								"string3": null,
																								"string4": functions.concat({
																									"string1": functions.padString({
																										"type": "left",
																										"char": " ",
																										"len": functions.getVar({
																											"var": "margin_left"
																										})
																									}),
																									"string2": functions.concat({
																										"string1": "Customer",
																										"string2": functions.padString({
																											"len": functions.sub({
																												"value1": functions.getVar({
																													"var": "center"
																												}),
																												"value2": 16
																											}),
																											"char": " "
																										}),
																										"string3": functions.concat({
																											"string1": ": ",
																											"string2": functions.getVarAttr({
																												"var": "vInvoiceDetails",
																												"attr": "Bill_to_Customer_No"
																											}),
																											"string3": null,
																											"string4": functions.concat({
																												"string1": functions.concat({
																													"string1": null,
																													"string2": functions.getVarAttr({
																														"var": "vInvoiceDetails",
																														"attr": "Bill_to_Name"
																													}),
																													"string3": null,
																													"string4": ""
																												}),
																												"string3": functions.conditional({
																													"condition": functions.getVarAttr({
																														"var": "vInvoiceDetails",
																														"attr": "Sell_to_Address"
																													}),
																													"yesValue": functions.concat({
																														"string1": "",
																														"string2": functions.getVarAttr({
																															"var": "vInvoiceDetails",
																															"attr": "Bill_to_Address"
																														}),
																														"string3": null,
																														"string4": ""
																													}),
																													"noValue": "",
																													"yesCallback": ""
																												})
																											})
																										}),
																										"string4": ""
																									}),
																									"string3": "",
																									"string4": ""
																								})
																							})
																						})
																					}),
																					"value3": functions.setVar({
																						"var": "vContentHeader",
																						"value": functions.concat({
																							"string1": functions.concat({
																								"string1": functions.concat({
																									"string1": functions.padString({
																										"type": "left",
																										"char": " ",
																										"len": functions.getVar({
																											"var": "left_margin"
																										})
																									}),
																									"string2": null,
																									"string3": ""
																								}),
																								"string2": functions.concat({
																									"string1": functions.concat({
																										"string1": functions.padString({
																											"len": functions.getVar({
																												"var": "margin_left"
																											}),
																											"type": "left",
																											"char": " "
																										}),
																										"string2": functions.padString({
																											"len": functions.getVar({
																												"var": "width"
																											}),
																											"type": "left",
																											"char": "-"
																										}),
																										"string3": null
																									}),
																									"string2": functions.concat({
																										"string1": functions.concat({
																											"string1": functions.padString({
																												"char": " ",
																												"len": functions.getVar({
																													"var": "left_margin"
																												}),
																												"type": "left"
																											}),
																											"string2": functions.padString({
																												"type": "left",
																												"string": "No",
																												"len": 3
																											}),
																											"string3": functions.padString({
																												"string": "Description",
																												"len": 12,
																												"type": "left"
																											}),
																											"string4": functions.concat({
																												"string1": functions.padString({
																													"len": 6,
																													"type": "left",
																													"char": "",
																													"string": "Price"
																												}),
																												"string2": functions.padString({
																													"string": "UOM",
																													"len": 4,
																													"type": "left",
																													"char": ""
																												}),
																												"string3": functions.padString({
																													"string": "Qty.",
																													"len": 9,
																													"type": "left",
																													"char": ""
																												}),
																												"string4": functions.concat({
																													"string1": functions.padString({
																														"string": "Amount",
																														"len": 10,
																														"type": "left",
																														"char": ""
																													}),
																													"string2": null
																												})
																											})
																										}),
																										"string2": functions.concat({
																											"string1": functions.padString({
																												"char": " ",
																												"len": functions.getVar({
																													"var": "margin_left"
																												}),
																												"type": "left"
																											}),
																											"string2": functions.padString({
																												"len": functions.getVar({
																													"var": "width"
																												}),
																												"type": "left",
																												"char": "-"
																											}),
																											"string3": null
																										})
																									})
																								})
																							}),
																							"string2": "",
																							"string3": "",
																							"string4": ""
																						})
																					}),
																					"value4": functions.setVar({
																						"var": "vContentBody",
																						"value": ""
																					}),
																					"value5": functions.setVar({
																						"var": "vInvoicedLineCounter",
																						"value": 1
																					}),
																					"value6": "",
																					"value7": functions.setVar({
																						"var": "vFooterText",
																						"value": functions.concat({
																							"string1": functions.concat({
																								"string1": null,
																								"string2": null,
																								"string3": null,
																								"string4": ""
																							})
																						})
																					}),
																					"value8": functions.setVar({
																						"var": "vFooterLast",
																						"value": functions.concat({
																							"string1": functions.concat({
																								"string1": functions.padString({
																									"string": "",
																									"len": functions.getVar({
																										"var": "left_Margin"
																									}),
																									"type": "left"
																								}),
																								"string2": functions.padString({
																									"string": "",
																									"len": 25,
																									"type": "left",
																									"char": "-"
																								}),
																								"string3": ""
																							}),
																							"string2": functions.concat({
																								"string1": functions.concat({
																									"string1": null,
																									"string2": functions.padString({
																										"len": functions.getVar({
																											"var": "width"
																										}),
																										"type": "left",
																										"string": "SIGNATURE & COMPANY CHOP"
																									}),
																									"string3": "",
																									"string4": ""
																								})
																							}),
																							"string3": functions.concat({
																								"string1": functions.concat({
																									"string1": null,
																									"string2": null,
																									"string3": functions.concat({
																										"string1": functions.padString({
																											"string": "THIS IS A COMPUTER GENERATED INVOICE",
																											"len": functions.getVar({
																												"var": "width"
																											}),
																											"type": "left"
																										}),
																										"string2": null,
																										"string3": functions.padString({
																											"type": "left",
																											"string": "NO SIGNATURE REQUIRED.",
																											"len": functions.getVar({
																												"var": "width"
																											})
																										}),
																										"string4": functions.concat({
																											"string1": null,
																											"string2": null
																										})
																									}),
																									"string4": ""
																								})
																							}),
																							"string4": functions.concat({
																								"string1": functions.concat({
																									"string1": functions.padString({
																										"len": functions.getVar({
																											"var": "margin_left"
																										}),
																										"char": " "
																									}),
																									"string2": functions.padString({
																										"string": functions.concat({
																											"string1": "Printed : ",
																											"string2": functions.formatDate({
																												"date": functions.dbDate({
																													"withTime": true
																												}),
																												"format": "d/m/Y H:i:s "
																											})
																										}),
																										"len": functions.getVar({
																											"var": "width"
																										}),
																										"type": "left"
																									}),
																									"string3": null,
																									"string4": functions.concat({
																										"string1": null,
																										"string2": null
																									})
																								}),
																								"string2": "",
																								"string3": ""
																							})
																						})
																					}),
																					"value9": functions.setVar({
																						"var": "vFooterSignature",
																						"value": functions.concat({
																							"string1": "[img|x=10|y=10|width=200|height=100]",
																							"string2": functions.getVar({
																								"var": "vSignature"
																							}),
																							"string3": "[/img]",
																							"string4": functions.concat({
																								"string1": null,
																								"string2": null,
																								"string3": null,
																								"string4": functions.concat({
																									"string1": null,
																									"string2": "",
																									"string3": "",
																									"string4": ""
																								})
																							})
																						})
																					}),
																					"value10": functions.setVar({
																						"var": "vInvoicedLineCount",
																						"value": functions.count({
																							"values": functions.getVar({
																								"var": "vInvoicedLines"
																							})
																						})
																					})
																				});
																				functions.setVar({
																					"var": "vInvoicedLinesCount",
																					"value": functions.count({
																						"values": functions.getVar({
																							"var": "vInvoicedLines"
																						})
																					})
																				});
																				functions.conditional({
																					"condition": functions.greater({
																						"value1": functions.getVar({
																							"var": "vInvoicedLinesCount"
																						}),
																						"value2": 1
																					}),
																					"yesCallback": async (data, extra) => {
																						functions.setVar({
																							"var": "vFooterTotal",
																							"value": functions.concat({
																								"string1": null,
																								"string2": null,
																								"string3": functions.concat({
																									"string1": functions.padString({
																										"len": 27,
																										"type": "right",
																										"char": "",
																										"string": functions.concat({
																											"string1": "Sub-Total",
																											"string2": ""
																										})
																									}),
																									"string2": functions.padString({
																										"string": functions.concat({
																											"string1": functions.formatNumber({
																												"value": functions.objectAttr({
																													"object": functions.objectAttr({
																														"object": functions.objectAttr({
																															"object": functions.getVarAttr({
																																"var": "vInvoiceDetails",
																																"attr": "SalesInvLines"
																															}),
																															"attr": "Posted_Sales_Invoice_Line"
																														}),
																														"attr": 0
																													}),
																													"attr": "Total_Amount_Excl_VAT"
																												}),
																												"decimals": 2,
																												"decimalSep": ".",
																												"thousandSep": ","
																											}),
																											"string2": ""
																										}),
																										"len": 11,
																										"type": "right"
																									}),
																									"string3": "",
																									"string4": ""
																								}),
																								"string4": functions.concat({
																									"string1": null,
																									"string2": functions.concat({
																										"string1": functions.padString({
																											"char": "",
																											"string": functions.concat({
																												"string1": "GST ",
																												"string2": functions.padString({
																													"len": 3
																												}),
																												"string3": functions.getVar({
																													"var": "vGSTValue"
																												}),
																												"string4": "%"
																											}),
																											"len": 27,
																											"type": "right"
																										}),
																										"string2": functions.padString({
																											"string": functions.concat({
																												"string1": functions.formatNumber({
																													"decimals": 2,
																													"decimalSep": ".",
																													"thousandSep": ",",
																													"value": functions.objectAttr({
																														"attr": "Total_VAT_Amount",
																														"object": functions.objectAttr({
																															"object": functions.objectAttr({
																																"object": functions.getVarAttr({
																																	"var": "vInvoiceDetails",
																																	"attr": "SalesInvLines"
																																}),
																																"attr": "Posted_Sales_Invoice_Line"
																															}),
																															"attr": 0
																														})
																													})
																												}),
																												"string2": ""
																											}),
																											"len": 11,
																											"type": "right"
																										})
																									}),
																									"string3": null,
																									"string4": functions.concat({
																										"string1": functions.padString({
																											"len": 18,
																											"type": "left",
																											"string": " "
																										}),
																										"string2": "[size='18pt'][b]",
																										"string3": functions.concat({
																											"string1": functions.concat({
																												"string1": "TOTAL",
																												"string2": ""
																											}),
																											"string2": functions.padString({
																												"string": functions.concat({
																													"string1": functions.formatNumber({
																														"decimalSep": ".",
																														"thousandSep": ",",
																														"value": functions.objectAttr({
																															"object": functions.objectAttr({
																																"object": functions.objectAttr({
																																	"object": functions.getVarAttr({
																																		"var": "vInvoiceDetails",
																																		"attr": "SalesInvLines"
																																	}),
																																	"attr": "Posted_Sales_Invoice_Line"
																																}),
																																"attr": 0
																															}),
																															"attr": "Total_Amount_Incl_VAT"
																														}),
																														"decimals": 2
																													}),
																													"string2": ""
																												}),
																												"len": 14,
																												"type": "right"
																											}),
																											"string3": "",
																											"string4": ""
																										}),
																										"string4": "[/b][/size]"
																									})
																								})
																							})
																						});
																						functions.map({
																							"values": functions.getVar({
																								"var": "vInvoicedLines"
																							}),
																							"callback": async (data, extra) => {
																								functions.setVar({
																									"var": "mapInvoicedLines",
																									"value": data
																								});
																								functions.setVar({
																									"var": "vContentBody",
																									"value": functions.concat({
																										"string1": functions.getVar({
																											"var": "vContentBody"
																										}),
																										"string2": functions.padString({
																											"len": functions.getVar({
																												"var": "left_margin"
																											}),
																											"type": "left",
																											"char": " "
																										}),
																										"string3": functions.concat({
																											"string1": "",
																											"string2": functions.concat({
																												"string1": null,
																												"string2": " - ",
																												"string3": functions.conditional({
																													"condition": functions.equal({
																														"value1": functions.getVarAttr({
																															"var": "mapInvoicedLines",
																															"attr": "No"
																														}),
																														"value2": "510-1015"
																													}),
																													"yesValue": "Empty Bottles",
																													"noValue": functions.getVarAttr({
																														"var": "mapInvoicedLines",
																														"attr": "Description"
																													})
																												})
																											}),
																											"string3": ""
																										}),
																										"string4": functions.concat({
																											"string1": functions.concat({
																												"string1": null,
																												"string2": functions.padString({
																													"len": 6,
																													"type": "left",
																													"char": " "
																												}),
																												"string3": functions.absolute({
																													"value": functions.getVarAttr({
																														"var": "mapInvoicedLines",
																														"attr": "Quantity"
																													})
																												}),
																												"string4": functions.concat({
																													"string1": functions.padString({
																														"string": "",
																														"len": 1,
																														"type": "left",
																														"char": " "
																													}),
																													"string2": functions.padString({
																														"type": "left",
																														"char": "",
																														"string": functions.conditional({
																															"condition": functions.equal({
																																"value1": functions.getVarAttr({
																																	"var": "mapInvoicedLines",
																																	"attr": "No"
																																}),
																																"value2": "510-1015"
																															}),
																															"yesValue": "PCS",
																															"noValue": functions.getVarAttr({
																																"var": "mapInvoicedLines",
																																"attr": "Unit_of_Measure"
																															})
																														}),
																														"len": 8
																													}),
																													"string3": functions.padString({
																														"string": "$ ",
																														"len": 12,
																														"type": "right",
																														"char": " "
																													}),
																													"string4": ""
																												})
																											}),
																											"string2": functions.concat({
																												"string1": functions.padString({
																													"string": functions.formatNumber({
																														"thousandSep": ",",
																														"value": functions.getVarAttr({
																															"var": "mapInvoicedLines",
																															"attr": "Line_Amount"
																														}),
																														"decimals": 2,
																														"decimalSep": "."
																													}),
																													"len": 10,
																													"type": "right",
																													"char": " "
																												})
																											}),
																											"string3": null,
																											"string4": null
																										})
																									})
																								});
																								functions.setVar({
																									"var": "vInvoicedLineCounter",
																									"value": functions.add({
																										"value1": functions.getVar({
																											"var": "vInvoicedLineCounter"
																										}),
																										"value2": 1
																									})
																								});
																								functions.conditional({
																									"condition": functions.greater({
																										"value1": functions.getVar({
																											"var": "vInvoicedLineCounter"
																										}),
																										"value2": functions.getVar({
																											"var": "vInvoicedLineCount"
																										})
																									}),
																									"yesCallback": async (data, extra) => {
																										functions.setVar({
																											"var": "vOutput",
																											"value": functions.concat({
																												"string1": functions.getVar({
																													"var": "vHeader"
																												}),
																												"string2": functions.getVar({
																													"var": "vSubHeader"
																												}),
																												"string3": functions.concat({
																													"string1": functions.concat({
																														"string1": null,
																														"string2": functions.padString({
																															"len": functions.getVar({
																																"var": "width"
																															}),
																															"type": "left",
																															"char": "-"
																														}),
																														"string3": null
																													}),
																													"string2": null,
																													"string3": functions.concat({
																														"string1": null,
																														"string2": functions.padString({
																															"len": functions.getVar({
																																"var": "width"
																															}),
																															"type": "left",
																															"char": "-"
																														}),
																														"string3": null
																													})
																												}),
																												"string4": functions.concat({
																													"string1": functions.getVar({
																														"var": "vContentBody"
																													}),
																													"string2": "",
																													"string3": functions.getVar({
																														"var": "vFooterTotal"
																													}),
																													"string4": functions.concat({
																														"string1": functions.getVar({
																															"var": "vFooterText"
																														}),
																														"string2": null,
																														"string3": "",
																														"string4": ""
																													})
																												})
																											})
																										});
																										functions.console({
																											"value": functions.getVar({
																												"var": "vOutput"
																											})
																										});
																									}
																								});
																							}
																						});
																					},
																					"noCallback": async (data, extra) => {
																						functions.setVar({
																							"var": "vFooterTotal",
																							"value": functions.concat({
																								"string1": null,
																								"string2": null,
																								"string3": functions.concat({
																									"string1": functions.padString({
																										"type": "right",
																										"char": "",
																										"string": functions.concat({
																											"string1": "Sub-Total",
																											"string2": ""
																										}),
																										"len": 27
																									}),
																									"string2": functions.padString({
																										"len": 11,
																										"type": "right",
																										"string": functions.concat({
																											"string1": functions.formatNumber({
																												"value": functions.objectAttr({
																													"object": functions.objectAttr({
																														"object": functions.getVarAttr({
																															"var": "vInvoiceDetails",
																															"attr": "SalesInvLines"
																														}),
																														"attr": "Posted_Sales_Invoice_Line"
																													}),
																													"attr": "Total_Amount_Excl_VAT"
																												}),
																												"decimals": 2,
																												"decimalSep": ".",
																												"thousandSep": ","
																											}),
																											"string2": ""
																										})
																									}),
																									"string3": "",
																									"string4": ""
																								}),
																								"string4": functions.concat({
																									"string1": null,
																									"string2": functions.concat({
																										"string1": functions.padString({
																											"type": "right",
																											"char": "",
																											"string": functions.concat({
																												"string1": "GST ",
																												"string2": functions.padString({
																													"len": 3
																												}),
																												"string3": functions.getVar({
																													"var": "vGSTValue"
																												}),
																												"string4": "%"
																											}),
																											"len": 27
																										}),
																										"string2": functions.padString({
																											"string": functions.concat({
																												"string1": functions.formatNumber({
																													"value": functions.objectAttr({
																														"attr": "Total_VAT_Amount",
																														"object": functions.objectAttr({
																															"object": functions.getVarAttr({
																																"var": "vInvoiceDetails",
																																"attr": "SalesInvLines"
																															}),
																															"attr": "Posted_Sales_Invoice_Line"
																														})
																													}),
																													"decimals": 2,
																													"decimalSep": ".",
																													"thousandSep": ","
																												}),
																												"string2": ""
																											}),
																											"len": 11,
																											"type": "right"
																										})
																									}),
																									"string3": null,
																									"string4": functions.concat({
																										"string1": functions.padString({
																											"string": " ",
																											"len": 18,
																											"type": "left"
																										}),
																										"string2": "[size='18pt'][b]",
																										"string3": functions.concat({
																											"string1": functions.concat({
																												"string1": "TOTAL",
																												"string2": ""
																											}),
																											"string2": functions.padString({
																												"string": functions.concat({
																													"string1": functions.formatNumber({
																														"value": functions.objectAttr({
																															"object": functions.objectAttr({
																																"object": functions.getVarAttr({
																																	"var": "vInvoiceDetails",
																																	"attr": "SalesInvLines"
																																}),
																																"attr": "Posted_Sales_Invoice_Line"
																															}),
																															"attr": "Total_Amount_Incl_VAT"
																														}),
																														"decimals": 2,
																														"decimalSep": ".",
																														"thousandSep": ","
																													}),
																													"string2": ""
																												}),
																												"len": 14,
																												"type": "right"
																											}),
																											"string3": "",
																											"string4": ""
																										}),
																										"string4": "[/b][/size]"
																									})
																								})
																							})
																						});
																						functions.setVar({
																							"var": "mapInvoicedLines",
																							"value": functions.getVar({
																								"var": "vInvoicedLines"
																							})
																						});
																						functions.setVar({
																							"var": "vContentBody",
																							"value": functions.concat({
																								"string1": functions.getVar({
																									"var": "vContentBody"
																								}),
																								"string2": functions.padString({
																									"type": "left",
																									"char": " ",
																									"len": functions.getVar({
																										"var": "left_margin"
																									})
																								}),
																								"string3": functions.concat({
																									"string1": "",
																									"string2": functions.concat({
																										"string1": null,
																										"string2": " - ",
																										"string3": functions.conditional({
																											"condition": functions.equal({
																												"value2": "510-1015",
																												"value1": functions.getVarAttr({
																													"var": "mapInvoicedLines",
																													"attr": "No"
																												})
																											}),
																											"yesValue": "Empty Bottles",
																											"noValue": functions.getVarAttr({
																												"var": "mapInvoicedLines",
																												"attr": "Description"
																											})
																										})
																									}),
																									"string3": ""
																								}),
																								"string4": functions.concat({
																									"string1": functions.concat({
																										"string1": null,
																										"string2": functions.padString({
																											"len": 6,
																											"type": "left",
																											"char": " "
																										}),
																										"string3": functions.absolute({
																											"value": functions.getVarAttr({
																												"var": "mapInvoicedLines",
																												"attr": "Quantity"
																											})
																										}),
																										"string4": functions.concat({
																											"string1": functions.padString({
																												"string": "",
																												"len": 1,
																												"type": "left",
																												"char": " "
																											}),
																											"string2": functions.padString({
																												"string": functions.conditional({
																													"condition": functions.equal({
																														"value1": functions.getVarAttr({
																															"var": "mapInvoicedLines",
																															"attr": "No"
																														}),
																														"value2": "510-1015"
																													}),
																													"yesValue": "PCS",
																													"noValue": functions.getVarAttr({
																														"var": "mapInvoicedLines",
																														"attr": "Unit_of_Measure"
																													})
																												}),
																												"len": 8,
																												"type": "left",
																												"char": ""
																											}),
																											"string3": functions.padString({
																												"string": "$ ",
																												"len": 12,
																												"type": "right",
																												"char": " "
																											}),
																											"string4": ""
																										})
																									}),
																									"string2": functions.concat({
																										"string1": functions.padString({
																											"type": "right",
																											"char": " ",
																											"string": functions.formatNumber({
																												"value": functions.getVarAttr({
																													"var": "mapInvoicedLines",
																													"attr": "Line_Amount"
																												}),
																												"decimals": 2,
																												"decimalSep": ".",
																												"thousandSep": ","
																											}),
																											"len": 10
																										})
																									}),
																									"string3": null,
																									"string4": null
																								})
																							})
																						});
																						functions.setVar({
																							"var": "vOutput",
																							"value": functions.concat({
																								"string1": functions.getVar({
																									"var": "vHeader"
																								}),
																								"string2": functions.getVar({
																									"var": "vSubHeader"
																								}),
																								"string3": functions.concat({
																									"string1": functions.concat({
																										"string1": null,
																										"string2": functions.padString({
																											"len": functions.getVar({
																												"var": "width"
																											}),
																											"type": "left",
																											"char": "-"
																										}),
																										"string3": null
																									}),
																									"string2": null,
																									"string3": functions.concat({
																										"string1": null,
																										"string2": functions.padString({
																											"len": functions.getVar({
																												"var": "width"
																											}),
																											"type": "left",
																											"char": "-"
																										}),
																										"string3": null
																									})
																								}),
																								"string4": functions.concat({
																									"string1": functions.getVar({
																										"var": "vContentBody"
																									}),
																									"string2": "",
																									"string3": functions.getVar({
																										"var": "vFooterTotal"
																									}),
																									"string4": functions.concat({
																										"string1": functions.getVar({
																											"var": "vFooterText"
																										}),
																										"string2": null,
																										"string3": "",
																										"string4": ""
																									})
																								})
																							})
																						});
																						functions.console({
																							"value": functions.getVar({
																								"var": "vOutput"
																							})
																						});
																					}
																				});
																				functions.toArray({
																					"value1": "",
																					"value2": functions.setVar({
																						"var": "vOutput2",
																						"value": functions.concat({
																							"string1": functions.getVar({
																								"var": "vFooterSignature"
																							}),
																							"string2": functions.getVar({
																								"var": "vFooterLast"
																							}),
																							"string3": "",
																							"string4": ""
																						})
																					}),
																					"value3": functions.console({
																						"value": functions.getVar({
																							"var": "vOutput2"
																						})
																					})
																				});
																				functions.setLanguage({
																					"lang": functions.getVar({
																						"var": "vTempLang"
																					})
																				});
																				functions.console({
																					"value": "************ END RECEIPT *********"
																				});
																			}
																		});
																	}
																});
																functions.console({
																	"value": functions.getVar({
																		"var": "vSignature"
																	})
																});
															}
														});
													},
													"errCallback": async (data, extra) => {
														functions.setVar({
															"var": "error",
															"value": data["err"]
														});
														functions.console({
															"value": data
														});
														functions.console({
															"value": "result error"
														});
													}
												});
											},
											"errCallback": async (data, extra) => {
												functions.console({
													"value": "no"
												});
											},
											"connector": "nav",
											"ent": "OK365_GetItemSalesPrice",
											"function": "g_fnSalesPostOrder",
											"data": functions.toObject({
												"optDocType": "Order",
												"cdeSalesOrdNo": functions.getVar({
													"var": "vOrderNo"
												}),
												"blnShip": "true",
												"blnInvoice": "true"
											})
										});
									},
									"errCallback": null,
									"connector": "nav",
									"ent": "OK365_Order_Entry"
								});
							},
							"errCallback": null
						});
					},
					"noCallback": async (data, extra) => {
						functions.conditional({
							"condition": functions.equal({
								"value1": functions.getVar({
									"var": "vAction"
								}),
								"value2": "redirectToMainPage"
							}),
							"yesCallback": async (data, extra) => {
								functions.gotoPage({
									"p": "pgMainMenu"
								});
								functions.setVar({
									"var": "vAction"
								});
							},
							"noCallback": async (data, extra) => {
								functions.conditional({
									"condition": functions.equal({
										"value1": functions.getVar({
											"var": "vAction"
										}),
										"value2": "invoice_posted"
									}),
									"yesCallback": async (data, extra) => {
										functions.console({
											"value": "Action = invoice_posted"
										});
										functions.gotoPage({
											"p": "pgMainMenu"
										});
									},
									"noCallback": async (data, extra) => {
										functions.userDefined('globalModalHide', {});
									}
								});
							}
						});
					}
				});
			}
		});
	},

	handleLblModalNegativeClick: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalHide', {});
	},

	handlePnlModalBodyButtonNegativeClick: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalHide', {});
	},

	handleImgUpdateCartCloseClick: async () => {
		functions.hideElement({
			"component": "pnlConfirmPaymentdDetails"
		});
	},

	handleLblSignClick: async () => {
		functions.setComponentValue({
			"component": "sgUser",
			"value": "[]"
		});
	},

	handleLblClearClick: async () => {
		functions.setComponentValue({
			"component": "sgUser",
			"value": "[]"
		});
	},

	handlePnlSignBoxTableCell1Click: async () => {
		functions.setComponentValue({
			"component": "sgUser",
			"value": "[]"
		});
	},

	handlePnlSignBoxTableCell2Click: async () => {
		functions.conditional({
			"condition": null,
			"yesValue": "",
			"noValue": "",
			"extra": "",
			"yesCallback": async (data, extra) => {
				functions.setVar({
					"var": "vSignature",
					"value": null
				});
				functions.hideElement({
					"component": "pnlConfirmPaymentdDetails",
					"componentId": ""
				});
				functions.userDefined('globalModalQuestion', {
					"title": "Post Invoice",
					"message": "Are you sure to proceed?"
				});
			},
			"noCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "lblAlertMsg",
					"componentId": "",
					"value": "Please sign to continue."
				});
				functions.showElement({
					"component": "pnlAlert",
					"componentId": ""
				});
			}
		});
	},

	handlePanel37680Click: async () => {
		functions.hideElement({
			"component": "pnlAlert"
		});
	},

	handlePanel37680600Click: async () => {
		functions.setVar({
			"var": "vSettingCount",
			"value": functions.count({
				"values": await functions.selectAll({
					"dataset": "l_settings",
					"callback": "",
					"errCallback": null
				})
			})
		});
		functions.conditional({
			"condition": functions.getVar({
				"var": "vSettingCount"
			}),
			"yesCallback": async (data, extra) => {
				functions.console({
					"value": "update..."
				});
				await functions.updateBy({
					"operator": "=",
					"value": 1,
					"data": functions.toObject({
						"Printer": null
					}),
					"callback": null,
					"errCallback": null,
					"dataset": "l_settings",
					"by": "_id"
				});
			},
			"noCallback": async (data, extra) => {
				functions.console({
					"value": "insert..."
				});
				await functions.insert({
					"dataset": "l_settings",
					"dt": functions.toObject({
						"Printer": null
					}),
					"callback": null,
					"errCallback": null
				});
			}
		});
		functions.setVar({
			"var": "vPrinter",
			"value": functions.objectAttr({
				"object": await functions.selectBy({
					"dataset": "l_settings",
					"by": "_id",
					"value": 1,
					"first": true,
					"callback": null,
					"errCallback": null
				}),
				"attr": "Printer"
			})
		});
		functions.hideElement({
			"component": "pnlPrinter"
		});
	},

	handlePgPaymentLoad: async () => {
		functions.setVar({
			"var": "vAction",
			"value": ""
		});
		functions.toArray({
			"value1": functions.setComponentValue({
				"component": "lblCodeName_data",
				"componentId": "",
				"value": functions.concat({
					"string1": functions.getVarAttr({
						"var": "vOrderEntryResult",
						"attr": "Sell_to_Customer_No",
						"default": ""
					}),
					"string2": " - ",
					"string3": functions.getVarAttr({
						"var": "vOrderEntryResult",
						"attr": "Sell_to_Customer_Name",
						"default": ""
					})
				})
			}),
			"value2": functions.setComponentValue({
				"component": "lblAddress_data",
				"componentId": "",
				"value": functions.getVarAttr({
					"var": "vOrderEntryResult",
					"attr": "Sell_to_Address",
					"default": ""
				})
			}),
			"value3": functions.setComponentValue({
				"component": "lblOrderDate_data",
				"componentId": "",
				"value": functions.formatDate({
					"format": "d-m-Y",
					"date": functions.getVarAttr({
						"var": "vOrderEntryResult",
						"attr": "Order_Date",
						"default": ""
					})
				})
			}),
			"value4": functions.setComponentValue({
				"component": "lblRemarks_data",
				"componentId": "",
				"value": functions.getVarAttr({
					"var": "vOrderEntryResult",
					"attr": "Remarks",
					"default": ""
				})
			}),
			"value5": functions.setComponentValue({
				"component": "lblOrderNo_data",
				"componentId": "",
				"value": functions.getVarAttr({
					"var": "vOrderEntryResult",
					"attr": "No",
					"default": ""
				})
			}),
			"value6": functions.setComponentValue({
				"component": "edtInvoiceDate",
				"value": functions.dbDate({})
			}),
			"value7": functions.setComponentValue({
				"component": "lblAmountToPay_data",
				"componentId": "",
				"value": functions.concat({
					"string1": "$ ",
					"string2": functions.formatNumber({
						"value": functions.getVarAttr({
							"var": "vFirstOrderLine",
							"attr": "Total_Amount_Incl_VAT"
						}),
						"decimals": 2,
						"decimalSep": ".",
						"thousandSep": ","
					})
				})
			}),
			"value8": functions.setComponentValue({
				"component": "edtPaymentAmount",
				"componentId": "",
				"value": functions.formatNumber({
					"value": functions.getVarAttr({
						"var": "vFirstOrderLine",
						"attr": "Total_Amount_Incl_VAT"
					}),
					"decimals": 2,
					"decimalSep": "."
				})
			}),
			"value9": "",
			"value10": ""
		});
		functions.setComboOptions({
			"valueField": "ID",
			"displayField": "VALUE",
			"combo": "cmbPaymentMethod",
			"comboId": "",
			"data": functions.toArray({
				"value1": functions.toObject({
					"ID": "CASH",
					"VALUE": "CASH"
				}),
				"value2": functions.toObject({
					"ID": "CHEQUE",
					"VALUE": "CHEQUE"
				})
			})
		});
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "vNewCheckout"
				}),
				"value2": "true"
			}),
			"yesValue": "",
			"noValue": "",
			"extra": "",
			"yesCallback": "",
			"noCallback": async (data, extra) => {
				functions.toArray({
					"value1": functions.setComponentValue({
						"component": "edtInvoiceDate",
						"value": functions.getVar({
							"var": "vInvoiceDate"
						})
					}),
					"value2": functions.setComponentValue({
						"component": "cmbPaymentMethod",
						"value": functions.getVar({
							"var": "vPaymentMethod"
						})
					}),
					"value3": functions.setComponentValue({
						"component": "edtChequeNumber",
						"value": functions.getVar({
							"var": "vChequeNo"
						})
					}),
					"value4": functions.setComponentValue({
						"component": "edtPaymentAmount",
						"value": functions.formatNumber({
							"value": functions.getVar({
								"var": "vPaymentAmount"
							}),
							"decimals": 2,
							"decimalSep": "."
						})
					}),
					"value5": "",
					"value6": "",
					"value7": "",
					"value8": "",
					"value9": "",
					"value10": ""
				});
				functions.conditional({
					"condition": functions.equal({
						"value2": "CASH",
						"value1": null
					}),
					"yesValue": "",
					"noValue": "",
					"extra": "",
					"yesCallback": async (data, extra) => {
						functions.hideElement({
							"component": "pnlChequeNumber",
							"componentId": ""
						});
					},
					"noCallback": async (data, extra) => {
						functions.showElement({
							"component": "pnlChequeNumber",
							"componentId": ""
						});
					}
				});
			}
		});
	}
};

const PgPayment = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;
	formatParentStyleLayout(props);

	useEffect(() => {
		actions.handlePgPaymentLoad();
	}, [currentTime]);
    
    return (
		<Page data={props.pgPayment}>
			<Panel data={props.pnlHolder}>
				<Panel data={props.pnlMain}>
					<Panel data={props.pnlMainHeader}>
						<Panel data={props.pnlMainHeaderLeft756}>
							<Panel data={props.pnlMainHeaderLeftTable326}>
								<Panel data={props.pnlMainHeaderLeftCell192}>
									<Image data={props.imgBack555} action={actions.handleImgBack555Click} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderMiddle71}>
							<Panel data={props.pnlMainHeaderMiddleTable764}>
								<Panel data={props.pnlMainHeaderMiddleCell629}>
									<Label data={props.lblMainTitle453} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderRight233}>
							<Panel data={props.pnlMainHeaderRightTable2}>
								<Panel data={props.pnlMainHeaderRightCell877}>
									<Image data={props.imgBack555407} action={actions.handleImgBack555407Click} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBody}>
						<Panel data={props.pnlBodyContentHeader}>
							<Label data={props.lblOrderNo_data} />
							<Label data={props.lblCodeName_data} />
							<Label data={props.lblAddress_data} />
						</Panel>
						<Panel data={props.pnlBodyContentBody}>
							<Panel data={props.pnlOrderDate}>
								<Label data={props.lblOrderDate} />
								<Label data={props.lblOrderDate_data} />
							</Panel>
							<Panel data={props.pnlInvoiceDate}>
								<Label data={props.lblInvoiceDate} />
								<Edit data={props.edtInvoiceDate} />
							</Panel>
							<Panel data={props.pnlAmountToPay}>
								<Label data={props.lblAmountToPay} />
								<Label data={props.lblAmountToPay_data} />
							</Panel>
							<Panel data={props.pnlPaymentMethod}>
								<Label data={props.lblPaymentMethod} />
								<ComboBox data={props.cmbPaymentMethod} functions={functions} />
							</Panel>
							<Panel data={props.pnlChequeNumber}>
								<Label data={props.lblChequeNumber} />
								<Edit data={props.edtChequeNumber} />
							</Panel>
							<Panel data={props.pnlPaymentAmount}>
								<Label data={props.lblPaymentAmount} />
								<Edit data={props.edtPaymentAmount} />
								<Button data={props.btnFull} action={actions.handleBtnFullClick} />
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainFooter}>
						<Panel data={props.pnlMainFooterTable}>
							<Panel data={props.pnlMainFooterTableCell1} action={actions.handlePnlMainFooterTableCell1Click}>
								<Label data={props.lblBtnPayPost} />
							</Panel>
							<Panel data={props.pnlMainFooterTableCell2} action={actions.handlePnlMainFooterTableCell2Click}>
								<Label data={props.lblBtnPost} />
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal}>
					<Panel data={props.pnlModalTable}>
						<Panel data={props.pnlModalCell}>
							<Panel data={props.pnlModalBodyMain}>
								<Panel data={props.pnlModalBodyTitle}>
									<Label data={props.lblModalTitle} />
								</Panel>
								<Panel data={props.pnlModalBodyMessage}>
									<Label data={props.lblModalMessage} />
								</Panel>
								<Panel data={props.pnlModalBodyLoading}>
									<Image data={props.imgModalLoading} />
								</Panel>
							</Panel>
							<Panel data={props.pnlModalBodyButtons}>
								<Panel data={props.pnlModalBodyButtonPositive} action={actions.handlePnlModalBodyButtonPositiveClick}>
									<Label data={props.lblModalPositive} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonNegative} action={actions.handlePnlModalBodyButtonNegativeClick}>
									<Label data={props.lblModalNegative} action={actions.handleLblModalNegativeClick} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlCSS}>
					<Label data={props.lblStyle} />
				</Panel>
				<Panel data={props.pnlConfirmPaymentdDetails}>
					<Panel data={props.pnlConfirmPaymentMethodDetailsTable}>
						<Panel data={props.pnlConfirmPaymentMethodDetailsCell}>
							<Panel data={props.pnlConfirmPaymentMethodDetailsBody}>
								<Panel data={props.Panel103}>
									<Panel data={props.pnlpnlConfirmPaymentMethodDetailsImages}>
										<Image data={props.imgUpdateCartClose} action={actions.handleImgUpdateCartCloseClick} />
									</Panel>
									<Panel data={props.pnlMainBodypnlConfirmPaymentMethodDetailsSign}>
										<Label data={props.lblSign} action={actions.handleLblSignClick} />
										<Signature data={props.sgUser} />
										<Panel data={props.pnlMainBodySignpnlConfirmPaymentMethodDetailsClear}>
											<Label data={props.lblClear} action={actions.handleLblClearClick} />
										</Panel>
									</Panel>
								</Panel>
								<Panel data={props.pnlUpdateCartModalBody5}>
									<Panel data={props.pnlSignBoxTable}>
										<Panel data={props.pnlSignBoxTableCell1} action={actions.handlePnlSignBoxTableCell1Click}>
											<Label data={props.lblClearBtn} />
										</Panel>
										<Panel data={props.pnlSignBoxTableCell2} action={actions.handlePnlSignBoxTableCell2Click}>
											<Label data={props.lblSubmitBtn} />
										</Panel>
									</Panel>
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlAlert}>
					<Panel data={props.pnlConfirmPaymentMethodDetailsTable695203}>
						<Panel data={props.pnlConfirmPaymentMethodDetailsCell206}>
							<Panel data={props.pnlConfirmPaymentMethodDetailsBody264}>
								<Panel data={props.Panel103885}>
									<Panel data={props.pnlMainBodypnlConfirmPaymentMethodDetailsSign105}>
										<Label data={props.lblAlertMsg} />
									</Panel>
								</Panel>
								<Panel data={props.pnlUpdateCartModalBody5896}>
									<Panel data={props.Panel37680} action={actions.handlePanel37680Click}>
										<Label data={props.Label894367} />
									</Panel>
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlPrinter}>
					<Panel data={props.pnlConfirmPaymentMethodDetailsTable695203598148}>
						<Panel data={props.pnlConfirmPaymentMethodDetailsCell206351}>
							<Panel data={props.pnlConfirmPaymentMethodDetailsBody264652}>
								<Panel data={props.Panel103885987}>
									<Panel data={props.pnlMainBodypnlConfirmPaymentMethodDetailsSign105386}>
										<Label data={props.lblSelectPrinter} />
										<ComboBox data={props.cmbPrinter} functions={functions} />
									</Panel>
								</Panel>
								<Panel data={props.pnlUpdateCartModalBody589675}>
									<Panel data={props.Panel154}>
										<Panel data={props.Panel37680600} action={actions.handlePanel37680600Click}>
											<Label data={props.Label894367403} />
										</Panel>
									</Panel>
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgPayment);