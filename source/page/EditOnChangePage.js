import React, { useEffect }  from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/EditOnChangePage';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Edit from '../framework/component/Edit';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {

	handleTxtSearchChanging: (data) => {
        console.log('changing with data ', data)
	},

    handleTxtSearchChange: () => {
        console.log('event change fired!')
    }
};

const EditOnChangePage = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;
    
    return (
		<Page data={props.pgCustomerListing}>
			<Panel data={props.pnlHolder}>
                <Panel data={props.pnlMainBodySearch}>
				    <Edit 
                        data={props.txtSearch} 
                        functions={functions} 
                        changing={actions.handleTxtSearchChanging} 
                        change={actions.handleTxtSearchChange}/>
                </Panel>
			
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(EditOnChangePage);