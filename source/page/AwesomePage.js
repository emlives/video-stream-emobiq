import React, { useEffect, useRef } from 'react';
import { connect } from 'react-redux'; 

import Page from '../framework/component/Page';
import Label from '../framework/component/Label';
import Panel from '../framework/component/Panel';
import Button from '../framework/component/Button';
import Edit from '../framework/component/Edit';

import {informationLog as log} from '../framework/core/Log'
import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/AwesomePage';

let functions = new Functions(StateName);

const AwesomePage = (props) => {
    // Append the dispatch to the function instance

     useEffect(() => {
        functions.enablePage();
      },[]);

    functions.pageProps = props;
    
    const param = {
        shouldDetect: true, 
        timeout: 20, 
        callback: () => {
            console.log('callback: logout user!')
        }
    }

    handleClick4 = () => {
    }

    handleClick = () => {
        functions.openLink({link:'http://www.google.com'})
    }

    handleClick2 = () => {
        functions.keyboardHide();
    }
   

    function testToArray() {
        const a = functions.toArray({value1:'H', value2:'A'})
        log(a)
    }

    function testSetGetObjAttr() {
        functions.clearAllVar({})

        // set object in global variable
        functions.setVar( { var: 'data', value: {process: {result:false}}} )
        // check to ensure it is in global variable
        log(functions.getVarAttr({var:'data', attr: 'process'}))

        functions.console({value: a});

        var obj = functions.getVarAttr({var:'data', attr: 'process'})

        //object, attr, value
        functions.setObjectAttr({
                object: obj,
                attr: 'count',
                value: 8
        })

        //object, attr
        var val = functions.objectAttr({object:obj, attr:'count'})
        log(val)
    }

    function testSetInterval() {
        log('testSetInterval')
        let param = {
            delay: 500,
            timeout: 3000,
            callback: function() {
                log('- set interval callback...')
            }
        }

        functions.setInterval(param)
    }

    function testCallback() {
        log('testCallback')
        let param = {
            value: 10,
            extra: 'some-extra-data',
            callback: function(extra) {
                log('timeout with extra = ' + extra)
            },
        }

        let value = functions.callback(param);
        log('return value: '+ value)
    }

    function testTimeout() {
        let param = {
            extra: 'some-extra-data',
            timeout: 5000,
        }

        functions.setTimeout(param);
    }

    function testForLoopFlow() {
        let param = {start: 1,
            end: 5,
            callback: function(i) {
                log('for loop at index: ' + i)
            },
        }

        functions.forLoop(param);
    }

    function testMapFlow() {
        const array = [1, 2, 3]

        let param = {values: array,
            extra: 'hello',
            callback: function(val, extra) {
                log('val: ' + val + ', extra: ' + extra)
            },
        }

        functions.map(param);
    }

    function testConditionalFlow() {
        let param = {condition: '',
            extra: 'hello',
            yesValue: 'yes',
            noValue: 'no',
            yesCallback: function(yesValue, extra){
                log('f1 ' + extra)
                log('f2')
            },
            noCallback: function(noValue, extra){ 
                log('f3 ' + extra)
                log('f4')
            }
        }

        functions.conditional(param);
    }

    handleClick3 = () => {
        const elementIsVisible =  functions.isElementShown({name: "btnOpenURL"});
        if (elementIsVisible) {
            functions.hideElement({ name: "btnOpenURL"});
            functions.setComponentValue({name: 'lblIsElementShown', value: 'openURL button is hidden'});
        } else {
            functions.showElement({ name: "btnOpenURL" });
            functions.setComponentValue({name: 'lblIsElementShown', value: 'openURL button is displayed'});
        }
    }

    return (
        <Page data={props.page}>
            <Label data={props.lblTitle}></Label>

            <Panel data={props.panelA}>
                <Label data={props.lblComments}></Label>
                <Edit data={props.tfComments}></Edit>
            </Panel>

            <Panel data={props.panelB}>

                <Button data={props.btnDismissKeyboard} action={handleClick4}></Button>
                <Label></Label>
                <Button data={props.btnOpenURL} action={handleClick}></Button>
            </Panel>

            {/* toggle visibility */}
            <Button data={props.btnHideOpenURL} action={handleClick3}></Button>
            <Label data={props.lblIsElementShown}></Label>

            <Label data={props.lblAppleURL}></Label>

        </Page>
    )
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] }
};

// const AwesomePageWithInactivityTimeout = functions.inactivityTimeout(AwesomePage)
export default connect(mapStateToProps)(AwesomePage);