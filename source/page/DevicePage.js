import React from 'react';
import { connect } from 'react-redux';

import Page from '../framework/component/Page';
import Label from '../framework/component/Label';
import Button from '../framework/component/Button';

import Functions from '../framework/core/Function';

import { StateName } from '../reducer/data/DevicePage';

let functions = new Functions(StateName);

const DevicePage = (props) => {
    // Append the dispatch to the function instance
    functions.pageProps = props;

    let handleClickOne = async () => {
        functions.setComponentValue({
            name: 'lblResult',
            value: await functions.deviceManufacturer()
        });
    }

    let handleClickTwo = async () => {
        functions.setComponentValue({
            name: 'lblResult',
            value: await functions.deviceSerial()
        });
    }

    let handleClickThree = () => {
        functions.onBackButton({
            callback: function() {
                alert('HAHAHAHAHA');
            }
        })
    }

    let handleClickFour = async () => {
        functions.onBackButton({
            callback: async function() {
                functions.setComponentValue({
                    name: 'lblResult',
                    value: await functions.deviceManufacturer()
                });
            }
        })
    }

    return (
        <Page>
            <Label data={props.lblResult}/>
            <Button data={props.btnDeviceManufacturer} action={handleClickOne}/>
            <Button data={props.btnDeviceSerial} action={handleClickTwo}/>
            <Button data={props.btnOnBackButtonFirst} action={handleClickThree}/>
            <Button data={props.btnOnBackButtonSecond} action={handleClickFour}/>
        </Page>
    )
}

// Bind the state to the props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
}

export default connect(mapStateToProps)(DevicePage);