import React, { useEffect }  from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgCashCollection';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import ComboBox from '../framework/component/ComboBox';
import Edit from '../framework/component/Edit';
import DataList from '../framework/component/DataList';
import CheckBox from '../framework/component/CheckBox';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handleImgBackClick: async () => {
		functions.gotoPage({
			"p": "pgMainMenu"
		});
	},

	handleImgPrintClick: async () => {
		functions.showElement({
			"component": "pnlPrinter"
		});
	},

	handlePanel609Click: async () => {
		functions.setComponentValue({
			"component": "edtPaymentAmount",
			"value": functions.getVarAttr({
				"var": "vCurrCustomerDetails",
				"attr": "Balance_LCY",
				"default": ""
			})
		});
	},

	handlePanel434474168553131796Click: async () => {
		functions.userDefined('globalModalLoading', {});
		await functions.loadData({
			"dataset": "OK365_Customer_Ledger_Entries",
			"limit": 9999999,
			"filter": functions.toArray({
				"value1": functions.toObject({
					"f": "Customer_No",
					"o": "=",
					"v": functions.getVarAttr({
						"var": "vCurrCustomerDetails",
						"attr": "No",
						"default": ""
					})
				}),
				"value2": functions.toObject({
					"f": "Select_for_Payment",
					"o": "=",
					"v": "false"
				}),
				"value3": functions.toObject({
					"f": "Remaining_Amount",
					"o": ">",
					"v": 0
				})
			}),
			"callback": async (data, extra) => {
				functions.userDefined('globalModalHide', {});
				functions.showElement({
					"component": "pnlModalOutstandingInvoices"
				});
				functions.setVar({
					"var": "list_invoice",
					"value": data
				});
				functions.console({
					"value": data
				});
				functions.console({
					"value": "Retrieve OK365_Customer_Ledger_Entries"
				});
			},
			"errCallback": async (data, extra) => {
				functions.userDefined('globalModalHide', {});
				functions.userDefined('globalModalInfo', {
					"message": functions.objectAttr({
						"object": functions.objectAttr({
							"object": data,
							"attr": "err"
						}),
						"attr": "global"
					}),
					"btnCaption": "OK",
					"title": "Navision Server Error"
				});
			}
		});
	},

	handlePnlMainFooterTableCellSubmitClick: async () => {
		functions.conditional({
			"condition": functions.getVar({
				"var": "v_invoice_amount"
			}),
			"yesCallback": async (data, extra) => {
				functions.conditional({
					"condition": null,
					"yesCallback": async (data, extra) => {
						functions.conditional({
							"condition": functions.greater({
								"value1": null,
								"value2": 0
							}),
							"yesCallback": async (data, extra) => {
								functions.userDefined('globalModalLoading', {});
								await functions.navCall({
									"ent": "OK365_CashReceipt",
									"function": "Create",
									"subfunction": "",
									"data": functions.toObject({
										"External_Document_No": null,
										"GeneralJournalTemplate": functions.objectAttr({
											"object": await functions.selectBy({
												"value": 1,
												"first": true,
												"callback": null,
												"errCallback": null,
												"dataset": "l_Route_Master",
												"by": "_id"
											}),
											"attr": "Cash_Rcpt_Jnl_Template"
										}),
										"GeneralBatchName": functions.objectAttr({
											"object": await functions.selectBy({
												"errCallback": null,
												"dataset": "l_Route_Master",
												"by": "_id",
												"value": 1,
												"first": true,
												"callback": null
											}),
											"attr": "Cash_Rcpt_Jnl_Batch_Name"
										}),
										"Payment_Method_Code": null,
										"Document_Type": "Payment",
										"Account_No": functions.getVarAttr({
											"var": "vCurrCustomer",
											"attr": "Customer_Code"
										}),
										"Applies_to_Doc_Type": "Invoice",
										"Amount": functions.multi({
											"value1": functions.toFloat({
												"value": null
											}),
											"value2": -1
										}),
										"Route_Code": functions.getVarAttr({
											"var": "vRouteMaster",
											"attr": "Route_Code"
										}),
										"Posting_Date": functions.formatDate({
											"date": functions.dbDate({}),
											"format": "Y-m-d"
										}),
										"Account_Type": "Customer",
										"Mobile_User_ID": functions.getVar({
											"var": "vUsername"
										})
									}),
									"callback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.setVar({
											"var": "v_cash_receipt_no",
											"value": functions.objectAttr({
												"object": data,
												"attr": "OK365_CashReceipt"
											})
										});
										functions.map({
											"values": await functions.selectAll({
												"dataset": "l_cash_collection_invoice",
												"callback": "",
												"errCallback": null
											}),
											"callback": async (data, extra) => {
												functions.setVar({
													"var": "v_loop_invoice",
													"value": data
												});
												await functions.navCall({
													"connector": "nav",
													"ent": "OK365_Customer_Ledger_Entries",
													"function": "Update",
													"data": functions.toObject({
														"Key": functions.getVarAttr({
															"var": "v_loop_invoice",
															"attr": "Key"
														}),
														"Applies_to_ID": functions.getVarAttr({
															"var": "v_cash_receipt_no",
															"attr": "Document_No"
														}),
														"Amount_to_Apply": functions.getVarAttr({
															"var": "v_loop_invoice",
															"attr": "Remaining_Amount"
														}),
														"Select_for_Payment": 1
													}),
													"callback": "",
													"errCallback": ""
												});
											}
										});
										await functions.clearData({
											"dataset": "l_cash_collection_invoice",
											"callback": null,
											"errCallback": null
										});
										functions.userDefined('globalModalHide', {});
										functions.setVar({
											"var": "vAction",
											"value": "Payment submitted"
										});
										functions.userDefined('globalModalInfo', {
											"title": "Cash Collection",
											"message": "Payment successfully submitted.",
											"btnCaption": "OK"
										});
									},
									"errCallback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.userDefined('globalModalInfo', {
											"title": "Connection Error",
											"message": "There's an error connecting to Navision Web Services (Create Cash Receipt)"
										});
										functions.userDefined('globalModalHide', {});
									},
									"connector": "nav"
								});
							},
							"noCallback": ""
						});
					},
					"noCallback": async (data, extra) => {
						functions.userDefined('globalModalInfo', {
							"title": "Cash Collection",
							"message": "Please input payment amount to continue"
						});
					}
				});
			},
			"noCallback": async (data, extra) => {
				functions.userDefined('globalModalInfo', {
					"message": "Please select invoices to continue.",
					"btnCaption": "OK",
					"title": "Cash Collection"
				});
			}
		});
	},

	handlePnlMainFooterTableCellSubmitPrintClick: async () => {
		functions.conditional({
			"condition": functions.getVar({
				"var": "v_invoice_amount"
			}),
			"yesCallback": async (data, extra) => {
				functions.conditional({
					"condition": null,
					"yesCallback": async (data, extra) => {
						functions.conditional({
							"condition": functions.greater({
								"value2": 0,
								"value1": null
							}),
							"yesCallback": async (data, extra) => {
								functions.userDefined('globalModalLoading', {});
								await functions.navCall({
									"connector": "nav",
									"ent": "OK365_CashReceipt",
									"function": "Create",
									"subfunction": "",
									"data": functions.toObject({
										"Document_Type": "Payment",
										"Applies_to_Doc_Type": "Invoice",
										"Amount": functions.multi({
											"value1": functions.toFloat({
												"value": null
											}),
											"value2": -1
										}),
										"Mobile_User_ID": functions.getVar({
											"var": "vUsername"
										}),
										"Posting_Date": functions.formatDate({
											"date": functions.dbDate({}),
											"format": "Y-m-d"
										}),
										"Account_Type": "Customer",
										"Account_No": functions.getVarAttr({
											"var": "vCurrCustomer",
											"attr": "Customer_Code"
										}),
										"External_Document_No": null,
										"GeneralJournalTemplate": functions.objectAttr({
											"object": await functions.selectBy({
												"callback": null,
												"errCallback": null,
												"dataset": "l_Route_Master",
												"by": "_id",
												"value": 1,
												"first": true
											}),
											"attr": "Cash_Rcpt_Jnl_Template"
										}),
										"GeneralBatchName": functions.objectAttr({
											"object": await functions.selectBy({
												"first": true,
												"callback": null,
												"errCallback": null,
												"dataset": "l_Route_Master",
												"by": "_id",
												"value": 1
											}),
											"attr": "Cash_Rcpt_Jnl_Batch_Name"
										}),
										"Payment_Method_Code": null,
										"Route_Code": functions.getVarAttr({
											"var": "vRouteMaster",
											"attr": "Route_Code"
										})
									}),
									"callback": async (data, extra) => {
										functions.userDefined('globalModalHide', {});
										functions.setVar({
											"var": "v_cash_receipt_no",
											"value": functions.objectAttr({
												"object": data,
												"attr": "OK365_CashReceipt"
											})
										});
										functions.console({
											"value": data
										});
										functions.map({
											"values": await functions.selectAll({
												"dataset": "l_cash_collection_invoice",
												"callback": "",
												"errCallback": null
											}),
											"callback": async (data, extra) => {
												functions.setVar({
													"var": "v_loop_invoice",
													"value": data
												});
												await functions.navCall({
													"data": functions.toObject({
														"Key": functions.getVarAttr({
															"var": "v_loop_invoice",
															"attr": "Key"
														}),
														"Select_for_Payment": 1,
														"Applies_to_ID": functions.getVarAttr({
															"var": "v_cash_receipt_no",
															"attr": "Document_No"
														}),
														"Amount_to_Apply": functions.getVarAttr({
															"var": "v_loop_invoice",
															"attr": "Remaining_Amount"
														})
													}),
													"callback": "",
													"errCallback": "",
													"connector": "nav",
													"ent": "OK365_Customer_Ledger_Entries",
													"function": "Update"
												});
											}
										});
										await functions.loadData({
											"dataset": "OK365_Company_Information",
											"filter": "",
											"callback": async (data, extra) => {
												functions.setVar({
													"var": "vCompany",
													"value": data[0]
												});
												functions.setVar({
													"var": "customer_obj",
													"value": await functions.selectBy({
														"by": "No",
														"value": null,
														"first": true,
														"callback": null,
														"errCallback": null,
														"dataset": "l_customer_list"
													})
												});
												functions.setVar({
													"var": "amount",
													"value": null
												});
												functions.setVar({
													"var": "outstanding_amount",
													"value": functions.formatNumber({
														"decimals": 2,
														"decimalSep": ".",
														"thousandSep": ",",
														"value": functions.sub({
															"value1": functions.toFloat({
																"value": functions.getVarAttr({
																	"var": "vCurrCustomerDetails",
																	"attr": "Balance_LCY"
																})
															}),
															"value2": functions.toFloat({
																"value": null
															})
														})
													})
												});
												functions.setVar({
													"var": "width",
													"value": 38
												});
												functions.setVar({
													"var": "margin_left",
													"value": 0
												});
												functions.setVar({
													"var": "width_center",
													"value": 15
												});
												functions.setVar({
													"var": "item_name_length",
													"value": 38
												});
												functions.setVar({
													"var": "amount_length",
													"value": 9
												});
												functions.setVar({
													"var": "header",
													"value": functions.concat({
														"string1": functions.concat({
															"string1": functions.concat({
																"string1": functions.padString({
																	"len": functions.getVar({
																		"var": "left_margin"
																	}),
																	"type": "left",
																	"char": " "
																}),
																"string2": functions.concat({
																	"string1": "[size='24pt']",
																	"string2": functions.padString({
																		"string": functions.getVarAttr({
																			"var": "vCompany",
																			"attr": "Name"
																		}),
																		"len": 27,
																		"type": "center"
																	}),
																	"string3": "[/size]"
																}),
																"string3": null
															}),
															"string2": functions.concat({
																"string1": functions.concat({
																	"string1": functions.padString({
																		"len": functions.getVar({
																			"var": "left_margin"
																		}),
																		"type": "left",
																		"char": " "
																	}),
																	"string2": functions.padString({
																		"string": functions.concat({
																			"string1": functions.getVarAttr({
																				"var": "vCompany",
																				"attr": "Address"
																			}),
																			"string2": "",
																			"string3": ""
																		}),
																		"len": functions.getVar({
																			"var": "width"
																		}),
																		"type": "center"
																	}),
																	"string3": null
																}),
																"string2": functions.concat({
																	"string1": functions.padString({
																		"len": functions.getVar({
																			"var": "left_margin"
																		}),
																		"type": "left",
																		"char": " "
																	}),
																	"string2": functions.padString({
																		"string": functions.concat({
																			"string1": functions.getVarAttr({
																				"var": "vCompany",
																				"attr": "Address_2"
																			}),
																			"string2": "",
																			"string3": ""
																		}),
																		"len": functions.getVar({
																			"var": "width"
																		}),
																		"type": "center"
																	}),
																	"string3": null
																}),
																"string3": functions.concat({
																	"string1": functions.concat({
																		"string1": functions.concat({
																			"string1": functions.padString({
																				"len": functions.getVar({
																					"var": "margin_left"
																				}),
																				"type": "left",
																				"char": " "
																			}),
																			"string2": functions.padString({
																				"len": functions.getVar({
																					"var": "width"
																				}),
																				"type": "left",
																				"char": "-"
																			}),
																			"string3": null
																		}),
																		"string2": functions.concat({
																			"string1": functions.concat({
																				"string1": functions.padString({
																					"len": functions.getVar({
																						"var": "left_margin"
																					}),
																					"type": "left",
																					"char": " "
																				}),
																				"string2": functions.padString({
																					"string": "RECEIPT",
																					"len": functions.getVar({
																						"var": "width"
																					}),
																					"type": "center"
																				}),
																				"string3": null
																			}),
																			"string2": functions.concat({
																				"string1": functions.padString({
																					"char": " ",
																					"len": functions.getVar({
																						"var": "margin_left"
																					}),
																					"type": "left"
																				}),
																				"string2": functions.padString({
																					"len": functions.getVar({
																						"var": "width"
																					}),
																					"type": "left",
																					"char": "-"
																				}),
																				"string3": null
																			})
																		})
																	}),
																	"string2": ""
																})
															})
														})
													})
												});
												functions.setVar({
													"var": "header_invoice",
													"value": functions.concat({
														"string1": functions.concat({
															"string1": functions.padString({
																"len": functions.getVar({
																	"var": "margin_left"
																}),
																"type": "left",
																"char": " "
															}),
															"string2": functions.concat({
																"string1": "Receipt No",
																"string2": functions.padString({
																	"len": functions.sub({
																		"value1": functions.getVar({
																			"var": "width_center"
																		}),
																		"value2": functions.length({
																			"string": "Receipt No"
																		})
																	}),
																	"char": " "
																}),
																"string3": ": "
															}),
															"string3": functions.getVarAttr({
																"var": "v_cash_receipt_no",
																"attr": "Document_No"
															}),
															"string4": null
														}),
														"string2": functions.concat({
															"string1": functions.padString({
																"type": "left",
																"char": " ",
																"len": functions.getVar({
																	"var": "margin_left"
																})
															}),
															"string2": functions.concat({
																"string1": "Date",
																"string2": functions.padString({
																	"len": functions.sub({
																		"value1": functions.getVar({
																			"var": "width_center"
																		}),
																		"value2": functions.length({
																			"string": "Date"
																		})
																	}),
																	"char": " "
																}),
																"string3": ": "
															}),
															"string3": functions.formatDate({
																"date": functions.getVarAttr({
																	"var": "v_cash_receipt_no",
																	"attr": "Posting_Date"
																}),
																"format": "d/m/Y"
															}),
															"string4": null
														}),
														"string3": functions.concat({
															"string1": functions.concat({
																"string1": functions.padString({
																	"len": functions.getVar({
																		"var": "margin_left"
																	}),
																	"type": "left",
																	"char": " "
																}),
																"string2": functions.concat({
																	"string1": "Customer",
																	"string2": functions.padString({
																		"len": functions.sub({
																			"value1": functions.getVar({
																				"var": "width_center"
																			}),
																			"value2": functions.length({
																				"string": "Customer"
																			})
																		}),
																		"char": " "
																	}),
																	"string3": ": ",
																	"string4": functions.concat({
																		"string1": functions.getVarAttr({
																			"var": "vCurrCustomerDetails",
																			"attr": "No"
																		}),
																		"string2": null
																	})
																}),
																"string3": functions.getVarAttr({
																	"var": "vCurrCustomerDetails",
																	"attr": "Name"
																}),
																"string4": null
															}),
															"string2": ""
														})
													})
												});
												functions.setVar({
													"var": "header_table",
													"value": functions.concat({
														"string1": functions.concat({
															"string1": functions.padString({
																"len": functions.getVar({
																	"var": "margin_left"
																}),
																"char": " "
															}),
															"string2": functions.padString({
																"len": functions.getVar({
																	"var": "width"
																}),
																"char": "-"
															}),
															"string3": null
														}),
														"string2": functions.concat({
															"string1": functions.padString({
																"len": functions.getVar({
																	"var": "margin_left"
																}),
																"char": " "
															}),
															"string2": "Description",
															"string3": functions.concat({
																"string1": "",
																"string2": functions.padString({
																	"len": functions.sub({
																		"value1": functions.getVar({
																			"var": "width"
																		}),
																		"value2": functions.length({
																			"string": "Description"
																		})
																	}),
																	"type": "right",
																	"string": "Amount"
																})
															}),
															"string4": null
														}),
														"string3": functions.concat({
															"string1": functions.concat({
																"string1": functions.padString({
																	"len": functions.getVar({
																		"var": "margin_left"
																	}),
																	"char": " "
																}),
																"string2": functions.padString({
																	"len": functions.getVar({
																		"var": "width"
																	}),
																	"char": "-"
																}),
																"string3": null
															})
														})
													})
												});
												functions.setVar({
													"var": "content_table",
													"value": functions.concat({
														"string1": functions.concat({
															"string1": functions.padString({
																"len": functions.getVar({
																	"var": "margin_left"
																}),
																"char": " "
															}),
															"string2": "Payment"
														}),
														"string2": functions.padString({
															"string": functions.concat({
																"string1": "$ ",
																"string2": functions.formatNumber({
																	"value": functions.getVar({
																		"var": "amount"
																	}),
																	"decimals": 2
																})
															}),
															"len": functions.sub({
																"value1": functions.getVar({
																	"var": "width"
																}),
																"value2": functions.length({
																	"string": "Payment"
																})
															}),
															"type": "right"
														}),
														"string3": functions.concat({
															"string1": functions.padString({
																"len": 3,
																"char": null
															}),
															"string2": functions.concat({
																"string1": functions.padString({
																	"len": functions.getVar({
																		"var": "margin_left"
																	}),
																	"char": " "
																}),
																"string2": "Invoice :",
																"string3": null
															}),
															"string3": ""
														})
													})
												});
												functions.setVar({
													"var": "footer"
												});
												functions.map({
													"values": await functions.selectAll({
														"dataset": "l_cash_collection_invoice",
														"callback": "",
														"errCallback": null
													}),
													"callback": async (data, extra) => {
														functions.setVar({
															"var": "v_loop_invoice",
															"value": data
														});
														functions.setVar({
															"var": "content_table",
															"value": functions.concat({
																"string1": functions.getVar({
																	"var": "content_table"
																}),
																"string2": functions.concat({
																	"string1": functions.padString({
																		"char": " ",
																		"len": functions.getVar({
																			"var": "margin_left"
																		})
																	}),
																	"string2": functions.getVarAttr({
																		"var": "v_loop_invoice",
																		"attr": "Document_No"
																	}),
																	"string3": functions.concat({
																		"string1": " (",
																		"string2": functions.formatDate({
																			"date": functions.getVarAttr({
																				"var": "v_loop_invoice",
																				"attr": "Posting_Date"
																			}),
																			"format": "d M Y"
																		}),
																		"string3": ")"
																	}),
																	"string4": functions.concat({
																		"string1": " $ ",
																		"string2": functions.formatNumber({
																			"value": functions.getVarAttr({
																				"var": "v_loop_invoice",
																				"attr": "Amount"
																			}),
																			"decimals": 2,
																			"decimalSep": ".",
																			"thousandSep": ","
																		})
																	})
																}),
																"string3": null
															})
														});
													}
												});
												functions.setVar({
													"var": "footer",
													"value": functions.concat({
														"string1": functions.getVar({
															"var": "footer"
														}),
														"string2": functions.padString({
															"len": 4,
															"char": null
														}),
														"string3": functions.concat({
															"string1": functions.concat({
																"string1": functions.padString({
																	"len": functions.getVar({
																		"var": "margin_left"
																	}),
																	"char": " "
																}),
																"string2": functions.concat({
																	"string1": "Paid by ",
																	"string2": functions.conditional({
																		"condition": functions.notEqual({
																			"value1": null,
																			"value2": "Cash"
																		}),
																		"yesValue": "Cheque",
																		"noValue": "Cash",
																		"noCallback": ""
																	}),
																	"string3": ""
																}),
																"string3": null
															}),
															"string2": functions.concat({
																"string1": functions.padString({
																	"len": functions.getVar({
																		"var": "margin_left"
																	}),
																	"char": " "
																}),
																"string2": null,
																"string3": null
															}),
															"string3": functions.concat({
																"string1": functions.concat({
																	"string1": functions.concat({
																		"string1": functions.padString({
																			"len": functions.getVar({
																				"var": "margin_left"
																			}),
																			"char": " "
																		}),
																		"string2": functions.padString({
																			"len": functions.getVar({
																				"var": "width"
																			}),
																			"char": "-"
																		}),
																		"string3": null
																	}),
																	"string2": functions.concat({
																		"string1": functions.padString({
																			"len": functions.getVar({
																				"var": "margin_left"
																			}),
																			"char": " "
																		}),
																		"string2": "Total outstanding amount : ",
																		"string3": "$ ",
																		"string4": functions.getVar({
																			"var": "outstanding_amount"
																		})
																	}),
																	"string3": null,
																	"string4": ""
																}),
																"string2": functions.concat({
																	"string1": functions.padString({
																		"len": functions.getVar({
																			"var": "margin_left"
																		}),
																		"char": " "
																	}),
																	"string2": functions.padString({
																		"char": " ",
																		"string": functions.concat({
																			"string1": "Printed : ",
																			"string2": functions.formatDate({
																				"format": "H:i:s d/m/Y",
																				"date": functions.dbDate({
																					"withTime": true
																				})
																			})
																		}),
																		"len": functions.getVar({
																			"var": "width"
																		}),
																		"type": "center"
																	}),
																	"string3": null
																}),
																"string3": functions.concat({
																	"string1": functions.concat({
																		"string1": functions.padString({
																			"string": "",
																			"len": functions.getVar({
																				"var": "margin_left"
																			}),
																			"type": "",
																			"char": " "
																		}),
																		"string2": functions.padString({
																			"string": functions.concat({
																				"string1": "Tel : ",
																				"string2": functions.getVarAttr({
																					"var": "vCompany",
																					"attr": "Company_Phone"
																				})
																			}),
																			"len": functions.getVar({
																				"var": "width"
																			}),
																			"type": "center"
																		}),
																		"string3": null
																	}),
																	"string2": functions.concat({
																		"string1": functions.padString({
																			"len": functions.getVar({
																				"var": "margin_left"
																			})
																		}),
																		"string2": functions.padString({
																			"string": functions.concat({
																				"string1": "Fax : ",
																				"string2": functions.getVarAttr({
																					"var": "vCompany",
																					"attr": "Company_Fax"
																				})
																			}),
																			"len": functions.getVar({
																				"var": "width"
																			}),
																			"type": "center"
																		}),
																		"string3": null
																	}),
																	"string3": functions.concat({
																		"string1": functions.concat({
																			"string1": functions.padString({
																				"len": functions.getVar({
																					"var": "margin_left"
																				}),
																				"char": " "
																			}),
																			"string2": functions.padString({
																				"string": functions.concat({
																					"string1": "Co Reg No ",
																					"string2": functions.getVarAttr({
																						"var": "vCompany",
																						"attr": "Company_Registration"
																					})
																				}),
																				"len": functions.getVar({
																					"var": "width"
																				}),
																				"type": "center"
																			}),
																			"string3": null
																		}),
																		"string2": functions.concat({
																			"string1": functions.padString({
																				"len": functions.getVar({
																					"var": "margin_left"
																				}),
																				"char": " "
																			}),
																			"string2": functions.padString({
																				"string": functions.concat({
																					"string1": "GST No ",
																					"string2": functions.getVarAttr({
																						"var": "vCompany",
																						"attr": "Company_Gst"
																					})
																				}),
																				"len": functions.getVar({
																					"var": "width"
																				}),
																				"type": "center"
																			}),
																			"string3": functions.padString({
																				"len": 3,
																				"char": null
																			})
																		})
																	})
																})
															})
														})
													})
												});
												functions.setVar({
													"var": "footer",
													"value": functions.concat({
														"string1": functions.getVar({
															"var": "footer"
														}),
														"string2": ""
													})
												});
												functions.setVar({
													"var": "footer",
													"value": functions.concat({
														"string1": functions.getVar({
															"var": "footer"
														}),
														"string2": functions.padString({
															"len": 4,
															"char": null
														})
													})
												});
												functions.setVar({
													"var": "cash_receipt",
													"value": functions.concat({
														"string1": functions.getVar({
															"var": "header"
														}),
														"string2": functions.getVar({
															"var": "header_invoice"
														}),
														"string3": functions.getVar({
															"var": "header_table"
														}),
														"string4": functions.concat({
															"string1": functions.getVar({
																"var": "content_table"
															}),
															"string2": functions.getVar({
																"var": "footer"
															})
														})
													})
												});
												functions.console({
													"value": functions.getVar({
														"var": "cash_receipt"
													})
												});
												await functions.clearData({
													"dataset": "l_cash_collection_invoice",
													"callback": null,
													"errCallback": null
												});
												await functions.clearData({
													"errCallback": null,
													"dataset": "l_cash_collection",
													"callback": null
												});
												functions.userDefined('globalModalHide', {});
												functions.setVar({
													"var": "vAction",
													"value": "Payment submitted"
												});
												functions.userDefined('globalModalInfo', {
													"title": "Cash Collection",
													"message": "Payment successfully submitted.",
													"btnCaption": "OK"
												});
											},
											"errCallback": async (data, extra) => {
												functions.userDefined('globalModalInfo', {
													"title": "Connection Error",
													"message": "There's an error connecting to Navision Web Services (Load Company Information)"
												});
												functions.userDefined('globalModalHide', {});
											}
										});
									},
									"errCallback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.userDefined('globalModalInfo', {
											"title": "Connection Error",
											"message": "There's an error connecting to Navision Web Services (Create Cash Receipt)"
										});
										functions.userDefined('globalModalHide', {});
									}
								});
							},
							"noCallback": ""
						});
					},
					"noCallback": async (data, extra) => {
						functions.userDefined('globalModalInfo', {
							"title": "Cash Collection",
							"message": "Please input payment amount to continue"
						});
					}
				});
			},
			"noCallback": async (data, extra) => {
				functions.userDefined('globalModalInfo', {
					"title": "Cash Collection",
					"message": "Please select invoices to continue.",
					"btnCaption": "OK"
				});
			}
		});
	},

	handlePanel250Click: async () => {
		await functions.clearData({
			"dataset": "l_cash_collection_invoice",
			"callback": "",
			"errCallback": null
		});
		functions.setVar({
			"var": "v_invoice_amount",
			"value": 0
		});
		functions.map({
			"values": functions.getVar({
				"var": "list_invoice"
			}),
			"callback": async (data, extra) => {
				functions.setVar({
					"var": "loop_invoice",
					"value": data
				});
				functions.conditional({
					"condition": null,
					"yesCallback": async (data, extra) => {
						await functions.insert({
							"dataset": "l_cash_collection_invoice",
							"dt": functions.toObject({
								"Document_No": functions.getVarAttr({
									"var": "loop_invoice",
									"attr": "Document_No"
								}),
								"Amount": functions.getVarAttr({
									"var": "loop_invoice",
									"attr": "Remaining_Amt_LCY"
								}),
								"Key": functions.getVarAttr({
									"var": "loop_invoice",
									"attr": "Key"
								}),
								"Posting_Date": functions.getVarAttr({
									"var": "loop_invoice",
									"attr": "Posting_Date"
								}),
								"Remaining_Amount": functions.getVarAttr({
									"var": "loop_invoice",
									"attr": "Remaining_Amount"
								})
							}),
							"callback": "",
							"errCallback": ""
						});
						functions.console({
							"value": "Add Invoice"
						});
						functions.console({
							"value": functions.getVarAttr({
								"var": "loop_invoice",
								"attr": "Remaining_Amt_LCY"
							})
						});
						functions.setVar({
							"var": "v_invoice_amount",
							"value": functions.add({
								"value1": functions.getVar({
									"var": "v_invoice_amount",
									"default": ""
								}),
								"value2": functions.getVarAttr({
									"var": "loop_invoice",
									"attr": "Remaining_Amt_LCY"
								})
							})
						});
					},
					"noCallback": ""
				});
			}
		});
		functions.setComponentValue({
			"component": "lblInvoiceAmount_data",
			"value": functions.formatNumber({
				"value": functions.getVar({
					"var": "v_invoice_amount"
				}),
				"decimals": 2,
				"decimalSep": ".",
				"thousandSep": ","
			})
		});
		functions.setComponentValue({
			"component": "edtPaymentAmount",
			"value": functions.formatNumber({
				"decimals": 2,
				"decimalSep": "",
				"thousandSep": "",
				"value": functions.getVar({
					"var": "v_invoice_amount"
				})
			})
		});
		functions.showElement({
			"component": "pnlInvoiceAmount"
		});
		functions.showElement({
			"component": "pnllblRemark"
		});
		functions.hideElement({
			"component": "pnlModalOutstandingInvoices"
		});
	},

	handlePanel250797Click: async () => {
		functions.hideElement({
			"component": "pnlModalOutstandingInvoices"
		});
	},

	handlePnlModalBodyButtonPositiveClick: async () => {
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "vAction"
				}),
				"value2": "delete_item"
			}),
			"yesCallback": async (data, extra) => {
				await functions.deleteBy({
					"dataset": "l_cart",
					"by": "ItemCode",
					"value": functions.getVarAttr({
						"var": "vItemDetails",
						"attr": "ItemCode"
					}),
					"callback": async (data, extra) => {
						functions.hideElement({
							"component": "pnlModal"
						});
					},
					"errCallback": ""
				});
				functions.toArray({
					"value1": await functions.loadData({
						"dataset": "l_cart",
						"callback": async (data, extra) => {
							functions.setVar({
								"var": "vTotalAmountItem",
								"value": 0
							});
							functions.map({
								"values": await functions.selectAll({
									"dataset": "l_cart",
									"callback": "",
									"errCallback": null
								}),
								"callback": async (data, extra) => {
									functions.console({
										"value": data
									});
									functions.setVar({
										"var": "vItemDetails_Order",
										"value": data
									});
									functions.setVar({
										"var": "item_amount",
										"value": functions.multi({
											"value2": functions.getVarAttr({
												"var": "vItemDetails_Order",
												"attr": "Price"
											}),
											"value1": functions.getVarAttr({
												"var": "vItemDetails_Order",
												"attr": "Quantity"
											})
										})
									});
									functions.setVar({
										"var": "vTotalAmountItem",
										"value": functions.add({
											"value1": functions.getVar({
												"var": "vTotalAmountItem",
												"default": ""
											}),
											"value2": functions.getVar({
												"var": "item_amount",
												"default": ""
											})
										})
									});
								}
							});
						},
						"errCallback": ""
					}),
					"value2": await functions.loadData({
						"dataset": "l_empty_bottle",
						"callback": async (data, extra) => {
							functions.console({
								"value": data
							});
							functions.setVar({
								"var": "vTotalAmountEmpty",
								"value": 0
							});
							functions.map({
								"values": await functions.selectAll({
									"dataset": "l_empty_bottle",
									"callback": "",
									"errCallback": ""
								}),
								"callback": async (data, extra) => {
									functions.console({
										"value": data
									});
									functions.setVar({
										"var": "vEmptyDetails_Order_Amount",
										"value": data
									});
									functions.setVar({
										"var": "item_amount_empty",
										"value": functions.multi({
											"value2": functions.getVarAttr({
												"var": "vEmptyDetails_Order_Amount",
												"attr": "price"
											}),
											"value1": functions.getVarAttr({
												"var": "vEmptyDetails_Order_Amount",
												"attr": "quantity"
											})
										})
									});
									functions.setVar({
										"var": "vTotalAmountEmpty",
										"value": functions.add({
											"value1": functions.getVar({
												"var": "vTotalAmountEmpty",
												"default": ""
											}),
											"value2": functions.getVar({
												"var": "item_amount_empty",
												"default": ""
											})
										})
									});
									functions.console({
										"value": functions.getVar({
											"var": "item_amount_empty"
										})
									});
									functions.console({
										"value": functions.getVar({
											"var": "vTotalAmountEmpty",
											"default": ""
										})
									});
								}
							});
						},
						"errCallback": ""
					}),
					"value3": functions.userDefined('globalCalculateTotal', {
						"subTotal": functions.getVar({
							"var": "vTotalAmountItem"
						}),
						"emptyTotal": functions.getVar({
							"var": "vTotalAmountEmpty"
						}),
						"gstRate": functions.getVar({
							"var": "vGstTotalRate"
						})
					}),
					"value4": "",
					"value5": "",
					"value6": "",
					"value7": "",
					"value8": "",
					"value9": "",
					"value10": ""
				});
			},
			"noCallback": async (data, extra) => {
				functions.conditional({
					"condition": functions.equal({
						"value1": functions.getVar({
							"var": "vAction"
						}),
						"value2": "Payment submitted"
					}),
					"yesCallback": async (data, extra) => {
						functions.setVar({
							"var": "vAction"
						});
						functions.gotoPage({
							"p": "pgMainMenu"
						});
					},
					"noCallback": async (data, extra) => {
						functions.userDefined('globalModalHide', {});
					}
				});
			}
		});
	},

	handleLblModalNegativeClick: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalHide', {});
	},

	handlePnlModalBodyButtonNegativeClick: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalHide', {});
	},

	handlePnlBtnPrintOKClick: async () => {
		functions.setVar({
			"var": "vSettingCount",
			"value": functions.count({
				"values": await functions.selectAll({
					"dataset": "l_settings",
					"callback": "",
					"errCallback": null
				})
			})
		});
		functions.conditional({
			"condition": functions.getVar({
				"var": "vSettingCount"
			}),
			"yesCallback": async (data, extra) => {
				functions.console({
					"value": "update..."
				});
				await functions.updateBy({
					"value": 1,
					"data": functions.toObject({
						"Printer": null
					}),
					"callback": null,
					"errCallback": null,
					"dataset": "l_settings",
					"by": "_id",
					"operator": "="
				});
			},
			"noCallback": async (data, extra) => {
				functions.console({
					"value": "insert..."
				});
				await functions.insert({
					"errCallback": null,
					"dataset": "l_settings",
					"dt": functions.toObject({
						"Printer": null
					}),
					"callback": null
				});
			}
		});
		functions.setVar({
			"var": "vPrinter",
			"value": functions.objectAttr({
				"object": await functions.selectBy({
					"by": "_id",
					"value": 1,
					"first": true,
					"callback": null,
					"errCallback": null,
					"dataset": "l_settings"
				}),
				"attr": "Printer"
			})
		});
		functions.hideElement({
			"component": "pnlPrinter"
		});
	},

	handlePgCashCollectionLoad: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.toArray({
			"value1": functions.setComponentValue({
				"component": "lblCodeName_data",
				"value": functions.concat({
					"string1": functions.getVarAttr({
						"var": "vCurrCustomerDetails",
						"attr": "No",
						"default": ""
					}),
					"string2": " - ",
					"string3": functions.getVarAttr({
						"var": "vCurrCustomerDetails",
						"attr": "Name",
						"default": ""
					})
				})
			}),
			"value2": functions.setComponentValue({
				"component": "lblAddress_data",
				"value": functions.getVarAttr({
					"var": "vCurrCustomerDetails",
					"attr": "Address",
					"default": ""
				})
			}),
			"value3": functions.setComponentValue({
				"component": "lblTotalOutstanding_data",
				"value": functions.formatNumber({
					"value": functions.getVarAttr({
						"var": "vCurrCustomerDetails",
						"attr": "Balance_LCY",
						"default": ""
					}),
					"decimals": 2,
					"decimalSep": ".",
					"thousandSep": ","
				})
			}),
			"value4": functions.setComponentValue({
				"component": "lblRemarks_data",
				"value": functions.getVarAttr({
					"var": "vCurrOder",
					"attr": "Remarks",
					"default": ""
				})
			})
		});
		functions.setComboOptions({
			"displayField": "Name",
			"combo": "cmbPaymentMethod",
			"data": functions.toArray({
				"value1": functions.toObject({
					"Code": "Cash",
					"Name": "Cash"
				}),
				"value2": functions.toObject({
					"Code": "Cheque",
					"Name": "Cheque"
				})
			}),
			"valueField": "Code"
		});
	}
};

const PgCashCollection = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;
	formatParentStyleLayout(props);

	useEffect(() => {
		actions.handlePgCashCollectionLoad();
	}, [currentTime]);
    
    return (
		<Page data={props.pgCashCollection}>
			<Panel data={props.pnlHolder}>
				<Panel data={props.pnlMain}>
					<Panel data={props.pnlMainHeader}>
						<Panel data={props.pnlMainHeaderLeft}>
							<Panel data={props.pnlMainHeaderLeftTable}>
								<Panel data={props.pnlMainHeaderLeftCell}>
									<Image data={props.imgBack} action={actions.handleImgBackClick} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderMiddle}>
							<Panel data={props.pnlMainHeaderMiddleTable}>
								<Panel data={props.pnlMainHeaderMiddleCell}>
									<Label data={props.lblMainTitle} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderRight}>
							<Panel data={props.pnlMainHeaderRightTable}>
								<Panel data={props.pnlMainHeaderRightCell}>
									<Image data={props.imgPrint} action={actions.handleImgPrintClick} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBody}>
						<Panel data={props.pnlBodyContentHeader}>
							<Label data={props.lblCodeName_data} />
							<Label data={props.lblAddress_data} />
						</Panel>
						<Panel data={props.Panel916681385}>
							<Panel data={props.pnlTotalOutstanding}>
								<Label data={props.lblTotalOutstanding} />
								<Label data={props.lblTotalOutstanding_data} />
							</Panel>
							<Panel data={props.pnlPaymentMethod}>
								<Label data={props.lblPaymentMethod} />
								<ComboBox data={props.cmbPaymentMethod} functions={functions} />
							</Panel>
							<Panel data={props.pnlChequeNumber}>
								<Label data={props.lblChequeNumber} />
								<Edit data={props.edtChequeNumber} />
							</Panel>
							<Panel data={props.pnlPaymentAmount}>
								<Label data={props.lblPaymentAmount} />
								<Edit data={props.edtPaymentAmount} />
								<Panel data={props.Panel609} action={actions.handlePanel609Click}>
									<Label data={props.Label103} />
								</Panel>
							</Panel>
							<Panel data={props.Panel434474168553131796} action={actions.handlePanel434474168553131796Click}>
								<Label data={props.lblRemarks4921680694128548} />
							</Panel>
							<Panel data={props.pnlInvoiceAmount}>
								<Label data={props.lblInvoiceAmount} />
								<Label data={props.lblInvoiceAmount_data} />
							</Panel>
							<Panel data={props.pnllblRemark}>
								<Label data={props.lblRemark} />
								<Edit data={props.edtRemark} />
							</Panel>
						</Panel>
						<Panel data={props.pnlContentMainLines} />
					</Panel>
					<Panel data={props.pnlMainFooter}>
						<Panel data={props.pnlMainFooterTable}>
							<Panel data={props.pnlMainFooterTableCellSubmit} action={actions.handlePnlMainFooterTableCellSubmitClick}>
								<Label data={props.lblBtnSubmit} />
							</Panel>
							<Panel data={props.pnlMainFooterTableCellSubmitPrint} action={actions.handlePnlMainFooterTableCellSubmitPrintClick}>
								<Label data={props.lblBtnSubmitPrint} />
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlCSS5}>
					<Label data={props.lblStyle174626795} />
				</Panel>
				<Panel data={props.pnlModalOutstandingInvoices}>
					<Panel data={props.pnlUpdateCartModalBodyItemListing}>
						<Panel data={props.Panel936}>
							<Panel data={props.Panel875}>
								<Label data={props.Label152} />
							</Panel>
						</Panel>
						<Panel data={props.Panel845}>
							<DataList data={props.dtOK365_Customer_Ledger_Entries} functions={functions} currentTime={currentTime}>
								{({ item, parentStyle }) => (							
									<Panel data={item.Panel803} parentStyle={parentStyle}>
										<Panel data={item.Panel806} parentStyle={parentStyle}>
											<CheckBox data={item.Checkbox892} parentStyle={parentStyle}/>
										</Panel>
										<Panel data={item.Panel806931} parentStyle={parentStyle}>
											<Panel data={item.Panel172} parentStyle={parentStyle}>
												<Label data={item.Label80} parentStyle={parentStyle} />
												<Label data={item.Label566} parentStyle={parentStyle} />
												<Label data={item.Label432} parentStyle={parentStyle} />
											</Panel>
											<Panel data={item.Panel172940} parentStyle={parentStyle}>
												<Label data={item.Label80375663} parentStyle={parentStyle} />
												<Label data={item.Label56676543} parentStyle={parentStyle} />
												<Label data={item.Label432443510} parentStyle={parentStyle} />
											</Panel>
											<Panel data={item.Panel172954} parentStyle={parentStyle}>
												<Label data={item.Label80535282} parentStyle={parentStyle} />
												<Label data={item.Label566587457} parentStyle={parentStyle} />
												<Label data={item.Label432206691} parentStyle={parentStyle} />
											</Panel>
										</Panel>
									</Panel>
								)}
							</DataList>
						</Panel>
						<Panel data={props.Panel63}>
							<Panel data={props.Panel250} action={actions.handlePanel250Click}>
								<Label data={props.Label326} />
							</Panel>
							<Panel data={props.Panel250797} action={actions.handlePanel250797Click}>
								<Label data={props.Label326355612} />
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal}>
					<Panel data={props.pnlModalTable}>
						<Panel data={props.pnlModalCell}>
							<Panel data={props.pnlModalBodyMain}>
								<Panel data={props.pnlModalBodyTitle}>
									<Label data={props.lblModalTitle} />
								</Panel>
								<Panel data={props.pnlModalBodyMessage}>
									<Label data={props.lblModalMessage} />
								</Panel>
								<Panel data={props.pnlModalBodyLoading}>
									<Image data={props.imgModalLoading} />
								</Panel>
							</Panel>
							<Panel data={props.pnlModalBodyButtons}>
								<Panel data={props.pnlModalBodyButtonPositive} action={actions.handlePnlModalBodyButtonPositiveClick}>
									<Label data={props.lblModalPositive} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonNegative} action={actions.handlePnlModalBodyButtonNegativeClick}>
									<Label data={props.lblModalNegative} action={actions.handleLblModalNegativeClick} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlPrinter}>
					<Panel data={props.pnlPrinterTable}>
						<Panel data={props.pnlPrinterTableCell}>
							<Panel data={props.pnlPrinterTableCellContent}>
								<Panel data={props.pnlMainBodyPrintContent}>
									<Panel data={props.pnlBodyPrintContent}>
										<Label data={props.lblSelectPrinter} />
										<ComboBox data={props.cmbPrinter} functions={functions} />
									</Panel>
								</Panel>
								<Panel data={props.pnlMainFooterPrintOK}>
									<Panel data={props.Panel896}>
										<Panel data={props.pnlBtnPrintOK} action={actions.handlePnlBtnPrintOKClick}>
											<Label data={props.lblPrintOK} />
										</Panel>
									</Panel>
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgCashCollection);