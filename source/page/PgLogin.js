import React from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgLogin';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import Edit from '../framework/component/Edit';
import ComboBox from '../framework/component/ComboBox';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	getLabel371Value: () => {
		return "Test"
	},
	gettxtUsernameValue: () => {
		return "testUsername"
	},
	handlePnlLoginButtonAction: () => {
		functions.conditional({
			"condition": functions.and({
				"value1": functions.componentElAttr({
					"component": "txtUsername",
					"componentId": "",
					"attr": "value"
				}),
				"value2": functions.componentElAttr({
					"component": "txtPassword",
					"componentId": "",
					"attr": "value"
				})
			}),
			"yesValue": "",
			"noValue": "",
			"extra": "",
			"yesCallback": (input, extra) => {
				functions.setComponentAttr({
					"component": "nav",
					"componentId": "",
					"attr": "Mobile_Login_User_ID",
					"value": functions.componentElAttr({
						"component": "txtUsername",
						"componentId": "",
						"attr": "value"
					})
				});
				functions.setComponentAttr({
					"component": "nav",
					"componentId": "",
					"attr": "Password",
					"value": functions.componentElAttr({
						"component": "txtPassword",
						"componentId": "",
						"attr": "value"
					})
				});
				functions.userDefined('globalModalLoading', {});
				functions.loadData({
					"dataset": "OK365_Mobile_User_Master",
					"filter": functions.toArray({
						"value1": functions.toObject({
							"f": "Mobile_Login_User_ID",
							"o": "=",
							"v": functions.componentElAttr({
								"component": "txtUsername",
								"attr": "value"
							})
						}),
						"value2": functions.toObject({
							"f": "Password",
							"o": "=",
							"v": functions.componentElAttr({
								"component": "txtPassword",
								"attr": "value"
							})
						}),
						"value3": "",
						"value4": "",
						"value5": "",
						"value6": "",
						"value7": "",
						"value8": "",
						"value9": "",
						"value10": ""
					}),
					"callback": (input, extra) => {
						functions.setVar({
							"var": "var_loginCnt",
							"value": input
						});
						// functions.console({
						// 	"value": "users"
						// });
						// functions.console({
						// 	"value": functions.getVar({
						// 		"var": "var_loginCnt"
						// 	})
						// });
						functions.conditional({
							"condition": functions.greater({
								"value1": functions.count({
									"values": functions.getVar({
										"var": "var_loginCnt",
										"default": ""
									})
								}),
								"value2": 0
							}),
							"yesValue": "",
							"noValue": "",
							"extra": "",
							"yesCallback": (input, extra) => {
								// functions.console({
								// 	"value": "Login success"
								// });
								functions.clearData({
									"dataset": "l_cart",
									"callback": null,
									"errCallback": null
								});
								functions.clearData({
									"dataset": "l_empty_bottle",
									"callback": null,
									"errCallback": null
								});
								functions.clearData({
									"dataset": "l_orderlist",
									"callback": null,
									"errCallback": null
								});
								functions.clearData({
									"errCallback": null,
									"dataset": "l_orderlist_details",
									"callback": null
								});
								functions.clearData({
									"dataset": "l_orderlist_empty_details",
									"callback": null,
									"errCallback": null
								});
								functions.clearData({
									"dataset": "l_Item_List",
									"callback": null,
									"errCallback": null
								});
								functions.setVar({
									"var": "v_item_list_count",
									"value": 0
								});
								functions.gotoPage({
									"p": "pgMainMenu"
								});
							},
							"noCallback": (input, extra) => {
								functions.userDefined('globalModalInfo', {
									"message": "Invalid username/password",
									"title": "Invalid Login"
								});
							}
						});
					},
					"errCallback": (input, extra) => {
						functions.userDefined('globalModalInfo', {
							"title": "Error",
							"message": functions.conditional({
								"condition": functions.getVarAttr({
									"var": "error",
									"attr": "global"
								}),
								"yesValue": functions.getVarAttr({
									"var": "error",
									"attr": "global"
								}),
								"noValue": "Cannot connect to Navision Server."
							})
						});
					}
				});
			},
			"noCallback": (input, extra) => {
				functions.userDefined('globalModalInfo', {
					"title": "Missing Fields",
					"message": "Please enter username and password."
				});
				functions.console({
					"value": "bb"
				});
			}
		});
		functions.setVar({
			"var": "vUsername",
			"value": functions.componentElAttr({
				"component": "txtUsername",
				"componentId": "",
				"attr": "value"
			})
		});
	},

	handlePnlMainSettingsAction: () => {
		functions.btPrinterPortList({
			"callback": (input, extra) => {
				functions.setComboOptions({
					"combo": "cmbPrinter",
					"comboId": "",
					"data": input,
					"valueField": "id",
					"valueSeparator": "",
					"displayField": "name",
					"displaySeparator": ""
				});
				functions.setVar({
					"var": "vPrinter",
					"value": functions.componentValue({
						"component": "cmbPrinter"
					})
				});
				functions.showElement({
					"component": "pnlPrinter",
					"componentId": ""
				});
			},
			"errorCallback": (input, extra) => {
				functions.userDefined('globalModalInfo', {
					"btnCaption": "OK",
					"title": "Printer",
					"message": "Failed to retrieved printer, please try it again."
				});
			}
		});
		functions.setVar({
			"var": "vPrinter",
			"value": functions.toObject({})
		});
	},

	handleLabel718Action: () => {
		functions.toArray({
			"value1": functions.setVar({
				"var": "vSettingCount",
				"value": functions.count({
					"values": functions.selectAll({
						"dataset": "l_settings",
						"callback": null
					})
				})
			}),
			"value2": functions.conditional({
				"condition": functions.getVar({
					"var": "vSettingCount"
				}),
				"yesCallback": (input, extra) => {
					functions.console({
						"value": "update..."
					});
					functions.updateBy({
						"operator": "=",
						"value": 1,
						"data": functions.toObject({
							"lang": "English"
						}),
						"callback": null,
						"errCallback": null,
						"dataset": "l_settings",
						"by": "_id"
					});
				},
				"noCallback": (input, extra) => {
					functions.console({
						"value": "insert..."
					});
					functions.insert({
						"dataset": "l_settings",
						"dt": functions.toObject({
							"lang": "English"
						})
					});
				}
			}),
			"value3": functions.setLanguage({
				"lang": "English"
			}),
			"value4": null,
			"value5": functions.setVar({
				"var": "vLanguage",
				"value": functions.selectAll({
					"dataset": "l_settings",
					"callback": null
				})
			})
		});
	},

	handleLabel81Action: () => {
		functions.toArray({
			"value1": functions.setVar({
				"var": "vSettingCount",
				"value": functions.count({
					"values": functions.selectAll({
						"dataset": "l_settings",
						"callback": null
					})
				})
			}),
			"value2": functions.conditional({
				"condition": functions.getVar({
					"var": "vSettingCount"
				}),
				"yesCallback": (input, extra) => {
					functions.console({
						"value": "update..."
					});
					functions.updateBy({
						"value": 1,
						"data": functions.toObject({
							"lang": "Chinese"
						}),
						"callback": null,
						"errCallback": null,
						"dataset": "l_settings",
						"by": "_id",
						"operator": "="
					});
				},
				"noCallback": (input, extra) => {
					functions.console({
						"value": "insert..."
					});
					functions.insert({
						"dataset": "l_settings",
						"dt": functions.toObject({
							"lang": "Chinese"
						})
					});
				}
			}),
			"value3": functions.setLanguage({
				"lang": "Chinese"
			}),
			"value4": null,
			"value5": functions.setVar({
				"var": "vLanguage",
				"value": functions.selectAll({
					"dataset": "l_settings",
					"callback": null,
					"errCallback": null
				})
			})
		});
	},

	handlePnlModalBodyButtonPositiveAction: () => {
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "vAction",
					"default": ""
				}),
				"value2": "NoCustomer"
			}),
			"yesValue": "",
			"noValue": "",
			"extra": "",
			"yesCallback": (input, extra) => {
				functions.userDefined('globalModalHide', {});
			},
			"noCallback": (input, extra) => {
				functions.conditional({
					"condition": functions.equal({
						"value1": functions.getVar({
							"var": "vAction",
							"default": ""
						}),
						"value2": "logout"
					}),
					"yesValue": "",
					"noValue": "",
					"extra": "",
					"yesCallback": (input, extra) => {
						functions.clearAllVar({});
						functions.setVar({
							"var": "vAction",
							"value": ""
						});
						functions.gotoPage({
							"p": "pgLogin"
						});
					},
					"noCallback": (input, extra) => {
						functions.setVar({
							"var": "vAction",
							"value": ""
						});
						functions.userDefined('globalModalHide', {});
					}
				});
			}
		});
	},

	handlePnlModalBodyButtonNegativeAction: () => {
		functions.setVar({
			"var": "vAction",
			"value": ""
		});
		functions.userDefined('globalModalHide', {});
	},

	handleCmbPrinterAction: () => {
		functions.setVar({
			"var": "vPrinter",
			"value": functions.componentValue({
				"component": "cmbPrinter"
			})
		});
	},

	handlePnlPrinterFooterNavBtnAction: () => {
		functions.selectAll({
			"dataset": "l_settings",
			"callback": (input, extra) => {
				functions.setVar({
					"var": "vSettingCount",
					"value": input
				});
				functions.conditional({
					"condition": functions.getVar({
						"var": "vSettingCount",
						"default": ""
					}),
					"yesValue": "",
					"noValue": "",
					"extra": "",
					"yesCallback": (input, extra) => {
						functions.console({
							"value": "update..."
						});
						functions.updateBy({
							"callback": (input, extra) => {
								functions.selectBy({
									"dataset": "l_settings",
									"by": "_id",
									"operator": "",
									"value": 1,
									"first": true,
									"callback": (input, extra) => {
										functions.setVar({
											"var": "vPrinter",
											"value": functions.objectAttr({
												"object": input,
												"attr": "Printer"
											})
										});
										functions.hideElement({
											"component": "pnlPrinter"
										});
									}
								});
							},
							"errCallback": null,
							"dataset": "l_settings",
							"by": "_id",
							"operator": "=",
							"value": 1,
							"data": functions.toObject({
								"Printer": functions.componentElAttr({
									"component": "cmbPrinter",
									"attr": "value"
								})
							})
						});
					},
					"noCallback": (input, extra) => {
						functions.console({
							"value": "insert..."
						});
						functions.insert({
							"dataset": "l_settings",
							"dt": functions.toObject({
								"Printer": functions.componentElAttr({
									"component": "cmbPrinter",
									"attr": "value"
								})
							}),
							"callback": (input, extra) => {
								functions.selectBy({
									"dataset": "l_settings",
									"by": "_id",
									"operator": "",
									"value": 1,
									"first": true,
									"callback": (input, extra) => {
										functions.setVar({
											"var": "vPrinter",
											"value": functions.objectAttr({
												"object": input,
												"attr": "Printer"
											})
										});
										functions.hideElement({
											"component": "pnlPrinter"
										});
									}
								});
							}
						});
					}
				});
			}
		});
	},

	handlePgLoginLoad: () => {
		functions.onBackButton({
			"callback": (input, extra) => {
				functions.gotoPage({
					"p": "pgMainMenu"
				});
			}
		});
		functions.setVar({
			"var": "vAction",
			"value": ""
		});
		functions.userDefined('globalModalLoading', {});
		functions.conditional({
			"condition": functions.getVar({
				"var": "vCurrCustomer",
				"default": ""
			}),
			"yesValue": "",
			"noValue": "",
			"extra": "",
			"yesCallback": (input, extra) => {
				functions.setComponentValue({
					"component": "txtSearch",
					"componentId": "",
					"value": functions.getVarAttr({
						"var": "vCurrCustomer",
						"attr": "No",
						"default": ""
					})
				});
			},
			"noCallback": (input, extra) => {
				functions.setComponentValue({
					"component": "txtSearch",
					"componentId": "",
					"value": ""
				});
			}
		});
		functions.clearData({
			"callback": (input, extra) => {
				functions.loadData({
					"limit": 10,
					"filter": functions.toArray({
						"value1": functions.toObject({
							"f": "Location_Code",
							"v": functions.getVarAttr({
								"var": "vCurrCustomerDetails",
								"attr": "Location_Code"
							})
						}),
						"value2": functions.toObject({
							"f": "Status",
							"v": "Open"
						}),
						"value3": functions.toObject({
							"o": functions.conditional({
								"condition": functions.getVar({
									"var": "vCurrCustomer",
									"default": ""
								}),
								"yesValue": "=",
								"noValue": "<>",
								"extra": ""
							}),
							"f": "Sell_to_Customer_No",
							"v": functions.getVarAttr({
								"var": "vCurrCustomer",
								"attr": "Customer_Code",
								"default": ""
							})
						}),
						"value4": "",
						"value5": "",
						"value6": "",
						"value7": "",
						"value8": "",
						"value9": "",
						"value10": ""
					}),
					"order": functions.toArray({
						"value1": functions.toObject({
							"f": "No",
							"v": "desc"
						}),
						"value2": "",
						"value3": "",
						"value4": "",
						"value5": "",
						"value6": "",
						"value7": "",
						"value8": "",
						"value9": "",
						"value10": ""
					}),
					"callback": (input, extra) => {
						functions.userDefined('globalModalHide', {});
						// functions.console({
						// 	"value": input
						// });
						functions.setVar({
							"var": "vTotalCount",
							"value": functions.count({
								"values": input
							})
						});
						functions.setVar({
							"var": "vLoopCount",
							"value": 0
						});
						functions.map({
							"values": input,
							"extra": "",
							"callback": (input, extra) => {
								functions.setVar({
									"var": "vLoopCount",
									"value": functions.add({
										"value2": 1,
										"value1": functions.getVar({
											"var": "vLoopCount",
											"default": ""
										})
									})
								});
								functions.setVar({
									"var": "vOrderListDetails",
									"value": input
								});
								functions.selectBy({
									"dataset": "l_orderlist",
									"by": "No",
									"operator": "=",
									"value": input["No"],
									"callback": (input, extra) => {
										functions.conditional({
											"condition": functions.equal({
												"value1": functions.count({
													"values": input
												}),
												"value2": 0
											}),
											"yesValue": "",
											"noValue": "",
											"extra": functions.getVar({
												"var": "vOrderListDetails",
												"default": ""
											}),
											"yesCallback": (input, extra) => {
												functions.insert({
													"dataset": "l_orderlist",
													"dt": extra,
													"callback": (input, extra) => {
														functions.conditional({
															"condition": functions.equal({
																"value1": functions.getVar({
																	"var": "vTotalCount",
																	"default": ""
																}),
																"value2": functions.getVar({
																	"var": "vLoopCount",
																	"default": ""
																})
															}),
															"yesValue": "",
															"noValue": "",
															"extra": "",
															"yesCallback": (input, extra) => {
																functions.loadData({
																	"dataset": "l_orderlist",
																	"callback": null,
																	"errCallback": null
																});
															},
															"noCallback": null
														});
													},
													"errCallback": null
												});
											},
											"noCallback": (input, extra) => {
												functions.conditional({
													"condition": functions.equal({
														"value1": functions.getVar({
															"var": "vTotalCount",
															"default": ""
														}),
														"value2": functions.getVar({
															"var": "vLoopCount",
															"default": ""
														})
													}),
													"yesValue": "",
													"noValue": "",
													"extra": "",
													"yesCallback": (input, extra) => {
														functions.loadData({
															"callback": null,
															"errCallback": null,
															"dataset": "l_orderlist"
														});
													},
													"noCallback": null
												});
											}
										});
									}
								});
							}
						});
					},
					"errCallback": (input, extra) => {
						functions.setVar({
							"var": "error",
							"value": input["err"]
						});
						functions.userDefined('globalModalInfo', {
							"title": "Error",
							"message": functions.conditional({
								"condition": functions.getVarAttr({
									"var": "error",
									"attr": "global"
								}),
								"yesValue": functions.getVarAttr({
									"var": "error",
									"attr": "global"
								}),
								"noValue": "Cannot connect to Navision Server."
							})
						});
					},
					"dataset": "OK365_Order_List"
				});
			},
			"errCallback": null,
			"dataset": "l_orderlist"
		});
	}
};

functions.pageActions = actions

const PgLogin = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;

    // Manipulate the props to support css styles
    formatParentStyleLayout(props);

    return (
		<Page data={props.pgLogin} load={actions.handlePgLoginLoad} currentTime={currentTime} initializePage={functions.coreStateInitializePage} pageActions={functions.pageActions}>
			<Panel data={props.pnlWrapper} functions={functions} currentTime={currentTime}>
				<Panel data={props.pnlMain} functions={functions} currentTime={currentTime}>
					<Panel data={props.pnlMainBody} functions={functions} currentTime={currentTime}>
						<Panel data={props.pnlHeader} functions={functions} currentTime={currentTime}>
							<Image data={props.Image63} functions={functions} currentTime={currentTime}/>
							<Label data={props.Label371} functions={functions} currentTime={currentTime} value={actions.getLabel371Value}/>
						</Panel>
						<Panel data={props.pnlUsername} functions={functions} currentTime={currentTime}>
							<Panel data={props.pnlUsernameCaption} functions={functions} currentTime={currentTime}>
								<Image data={props.Image151} functions={functions} currentTime={currentTime}/>
								<Label data={props.Label696} functions={functions} currentTime={currentTime}/>
							</Panel>
							<Panel data={props.pnlUsernameText} functions={functions} currentTime={currentTime}>
								<Edit data={props.txtUsername} currentTime={currentTime} functions={functions} value={actions.gettxtUsernameValue}/>
							</Panel>
						</Panel>
						<Panel data={props.pnlUsername771} functions={functions} currentTime={currentTime}>
							<Panel data={props.pnlPasswordCaption} functions={functions} currentTime={currentTime}>
								<Image data={props.Image151321} currentTime={currentTime} functions={functions}/>
								<Label data={props.Label696418} currentTime={currentTime} functions={functions}/>
							</Panel>
							<Panel data={props.pnlPasswordText} currentTime={currentTime} functions={functions}>
								<Edit data={props.txtPassword} functions={functions} currentTime={currentTime}/>
							</Panel>
						</Panel>
						<Panel data={props.pnlLogin} functions={functions} currentTime={currentTime}>
							<Panel data={props.pnlLoginButton} action={actions.handlePnlLoginButtonAction} functions={functions} currentTime={currentTime}>
								<Label data={props.Label317} functions={functions} currentTime={currentTime}/>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainSettings} action={actions.handlePnlMainSettingsAction} functions={functions} currentTime={currentTime}>
							<Label data={props.lblPrinter} functions={functions} currentTime={currentTime}/>
							<Image data={props.Image591} currentTime={currentTime} functions={functions}/>
						</Panel>
						<Panel data={props.pnlMainLanguage} functions={functions} currentTime={currentTime}>
							<Label data={props.Label718} action={actions.handleLabel718Action} functions={functions} currentTime={currentTime}/>
							<Label data={props.Label81} action={actions.handleLabel81Action} functions={functions} currentTime={currentTime}/>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainFooterMsg} functions={functions} currentTime={currentTime}>
						<Label data={props.Label123} functions={functions} currentTime={currentTime}/>
						<Label data={props.Label145} functions={functions} currentTime={currentTime}/>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal} functions={functions} currentTime={currentTime}>
					<Panel data={props.pnlModalBodyMain} currentTime={currentTime} functions={functions}>
						<Panel data={props.pnlModalBodyTitle} functions={functions} currentTime={currentTime}>
							<Label data={props.lblModalTitle} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlModalBodyMessage} currentTime={currentTime} functions={functions}>
							<Label data={props.lblModalMessage} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlModalBodyLoading} functions={functions} currentTime={currentTime}>
							<Image data={props.imgModalLoading} functions={functions} currentTime={currentTime}/>
						</Panel>
					</Panel>
					<Panel data={props.pnlModalBodyButtons} currentTime={currentTime} functions={functions}>
						<Panel data={props.pnlModalBodyButtonPositive} action={actions.handlePnlModalBodyButtonPositiveAction} functions={functions} currentTime={currentTime}>
							<Label data={props.lblModalPositive} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlModalBodyButtonNegative} action={actions.handlePnlModalBodyButtonNegativeAction} functions={functions} currentTime={currentTime}>
							<Label data={props.lblModalNegative} functions={functions} currentTime={currentTime}/>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlPrinter} currentTime={currentTime} functions={functions}>
					<Panel data={props.pnlPrinterMainBody} functions={functions} currentTime={currentTime}>
						<Panel data={props.pnlSelectPrinter} functions={functions} currentTime={currentTime}>
							<Label data={props.lblSelectPrinter} functions={functions} currentTime={currentTime}/>
							<ComboBox data={props.cmbPrinter} action={actions.handleCmbPrinterAction} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlPrinterFooterNavBtn} action={actions.handlePnlPrinterFooterNavBtnAction} functions={functions} currentTime={currentTime}>
							<Label data={props.lblPrinterFooterNavBtn} functions={functions} currentTime={currentTime}/>
						</Panel>
					</Panel>
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgLogin);