import React, { useEffect }  from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgStockTake';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import Edit from '../framework/component/Edit';
import DataList from '../framework/component/DataList';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handleImgBackClick: async () => {
		functions.gotoPage({
			"p": "pgMainMenu"
		});
	},

	handleImgAddNewClick: async () => {
		functions.userDefined('globalModalLoading', {});
		await functions.loadData({
			"dataset": "l_Item_List",
			"callback": async (data, extra) => {
				functions.userDefined('globalModalHide', {});
				functions.showElement({
					"component": "pnlModalAddItem"
				});
			},
			"errCallback": ""
		});
	},

	handlePanel489Click: async () => {
		functions.setVar({
			"var": "v_current_stock_line_selected",
			"value": data
		});
		functions.setComponentValue({
			"component": "edtQty",
			"value": data["Qty_Phys_Inventory"]
		});
		functions.setComponentValue({
			"component": "lblEdtQtyTitle",
			"value": functions.concat({
				"string1": data["Item_No"],
				"string2": " - ",
				"string3": data["Description"]
			})
		});
		functions.setComponentValue({
			"component": "lblEditQtyUOM",
			"value": data["Unit_of_Measure_Code"]
		});
		functions.showElement({
			"component": "pnlModalEditQty"
		});
	},

	handlePnlFooter899Click: async () => {
		functions.setVar({
			"var": "vAction",
			"value": "release_stock_take"
		});
		functions.userDefined('globalModalQuestion', {
			"title": "Release Stock Take",
			"message": "Do you want to continue release stock take?"
		});
	},

	handlePnlModalBodyButtonPositiveClick: async () => {
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "vAction"
				}),
				"value2": "calculate_error"
			}),
			"yesCallback": async (data, extra) => {
				functions.setVar({
					"var": "vAction"
				});
				functions.showElement({
					"component": "pnlModalCalculate"
				});
			},
			"noCallback": async (data, extra) => {
				functions.conditional({
					"condition": functions.equal({
						"value1": functions.getVar({
							"var": "vAction"
						}),
						"value2": "release_stock_take"
					}),
					"yesCallback": async (data, extra) => {
						functions.userDefined('globalModalLoading', {});
						functions.setVar({
							"var": "vAction"
						});
						functions.conditional({
							"condition": functions.greater({
								"value1": functions.count({
									"values": await functions.selectAll({
										"dataset": "l_Physical_Inventory_Journal",
										"callback": null,
										"errCallback": null
									})
								}),
								"value2": functions.toInteger({
									"value": 0
								})
							}),
							"yesCallback": async (data, extra) => {
								functions.console({
									"value": "Yes release"
								});
								await functions.navCall({
									"callback": async (data, extra) => {
										functions.console({
											"value": "after UpdatePhysicalJournalStatus called."
										});
										functions.setVar({
											"var": "vAction",
											"value": "release_stock_take_success"
										});
										functions.userDefined('globalModalHide', {});
										functions.userDefined('globalModalInfo', {
											"title": "Release Stock Take",
											"message": "Stock Take has been successfully released.",
											"btnCaption": "MENU"
										});
									},
									"errCallback": async (data, extra) => {
										functions.setVar({
											"var": "vAction"
										});
										functions.userDefined('globalModalHide', {});
										functions.userDefined('globalModalInfo', {
											"title": "Internal Navision Server Error",
											"message": functions.objectAttr({
												"attr": "global",
												"object": functions.objectAttr({
													"object": data,
													"attr": "err"
												})
											}),
											"btnCaption": "OK"
										});
									},
									"connector": "nav",
									"ent": "OK365_GetItemSalesPrice",
									"function": "UpdatePhysicalJournalStatus",
									"data": functions.toObject({
										"documentNo": functions.getVar({
											"var": "v_stock_count_document_no"
										})
									})
								});
							},
							"noCallback": async (data, extra) => {
								functions.console({
									"value": "No release"
								});
								functions.setVar({
									"var": "vAction",
									"value": "no_data_to_release"
								});
								functions.userDefined('globalModalHide', {});
								functions.userDefined('globalModalInfo', {
									"title": "Release Stock Take",
									"message": "There's no data to release, please recalculate or add item to release.",
									"btnCaption": "OK"
								});
							}
						});
					},
					"noCallback": async (data, extra) => {
						functions.conditional({
							"condition": functions.equal({
								"value1": functions.getVar({
									"var": "vAction"
								}),
								"value2": "release_stock_take_success"
							}),
							"yesCallback": async (data, extra) => {
								functions.setVar({
									"var": "vAction"
								});
								functions.gotoPage({
									"p": "pgMainMenu"
								});
							},
							"noCallback": async (data, extra) => {
								functions.conditional({
									"condition": functions.equal({
										"value2": "zero_qty",
										"value1": functions.getVar({
											"var": "vAction"
										})
									}),
									"yesCallback": async (data, extra) => {
										functions.setVar({
											"var": "vAction"
										});
										functions.userDefined('globalModalHide', {});
										functions.showElement({
											"component": "pnlModalAddItemQty"
										});
									},
									"noCallback": async (data, extra) => {
										functions.setVar({
											"var": "vAction"
										});
										functions.userDefined('globalModalHide', {});
									}
								});
							}
						});
					}
				});
			}
		});
	},

	handlePnlModalBodyButtonNegativeClick: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalHide', {});
	},

	handlePnlBtnCalculateYesClick: async () => {
		functions.hideElement({
			"component": "pnlModalCalculate"
		});
		functions.userDefined('globalModalLoading', {});
		functions.conditional({
			"condition": functions.getVar({
				"var": "v_selected_location_code"
			}),
			"yesCallback": async (data, extra) => {
				await functions.navCall({
					"connector": "nav",
					"ent": "OK365_GetItemSalesPrice",
					"function": "CalculateInventory",
					"data": functions.toObject({
						"locationCode": functions.getVar({
							"var": "v_selected_location_code"
						}),
						"documentNo": functions.getVar({
							"var": "v_stock_count_document_no"
						}),
						"postingDate": functions.dbDate({})
					}),
					"callback": async (data, extra) => {
						functions.console({
							"value": data
						});
						functions.console({
							"value": "Calculate Success"
						});
						await functions.loadData({
							"limit": 999999,
							"filter": functions.toArray({
								"value1": functions.toObject({
									"f": "Document_No",
									"v": functions.getVar({
										"var": "v_stock_count_document_no"
									})
								}),
								"value2": functions.toObject({
									"f": "Location_Code",
									"v": functions.getVar({
										"var": "v_selected_location_code"
									})
								}),
								"value3": functions.toObject({
									"f": "Posting_Date",
									"v": functions.dbDate({})
								})
							}),
							"order": functions.toArray({
								"value1": functions.toObject({
									"f": "Item_No",
									"v": "asc"
								})
							}),
							"callback": async (data, extra) => {
								functions.console({
									"value": data
								});
								await functions.dataFromString({
									"dataset": "l_Physical_Inventory_Journal",
									"string": data,
									"callback": "",
									"errCallback": null
								});
								await functions.loadData({
									"dataset": "l_Physical_Inventory_Journal",
									"callback": async (data, extra) => {
										functions.hideElement({
											"component": "pnlModalCalculate"
										});
										functions.userDefined('globalModalHide', {});
									},
									"errCallback": ""
								});
							},
							"errCallback": async (data, extra) => {
								functions.setVar({
									"var": "vAction",
									"value": "calculate_error"
								});
								functions.userDefined('globalModalHide', {});
								functions.userDefined('globalModalInfo', {
									"title": "Navision Server Error",
									"message": functions.objectAttr({
										"object": functions.objectAttr({
											"object": data,
											"attr": "err"
										}),
										"attr": "global"
									}),
									"btnCaption": "OK"
								});
							},
							"dataset": "OK365_Physical_Inventory_Journal"
						});
					},
					"errCallback": async (data, extra) => {
						functions.console({
							"value": "Calculate Error"
						});
						functions.setVar({
							"var": "vAction",
							"value": "calculate_error"
						});
						functions.userDefined('globalModalHide', {});
						functions.userDefined('globalModalInfo', {
							"btnCaption": "OK",
							"title": "Navision Server Error",
							"message": functions.objectAttr({
								"attr": "global",
								"object": functions.objectAttr({
									"attr": "err",
									"object": data
								})
							})
						});
					}
				});
			},
			"noCallback": async (data, extra) => {
				functions.setVar({
					"var": "vAction",
					"value": "calculate_error"
				});
				functions.userDefined('globalModalHide', {});
				functions.userDefined('globalModalInfo', {
					"title": "No Location",
					"message": "Please try re-login again.",
					"btnCaption": "OK"
				});
			}
		});
	},

	handlePnlBtnCalculateNoClick: async () => {
		functions.gotoPage({
			"p": "pgMainMenu"
		});
	},

	handlePnlBtnCalculateYes912Click: async () => {
		functions.hideElement({
			"component": "pnlModalEditQty"
		});
	},

	handlePnlBtnCalculateNo27Click: async () => {
		functions.hideElement({
			"component": "pnlModalEditQty"
		});
		functions.userDefined('globalModalLoading', {});
		functions.setVar({
			"var": "v_tmp_qty",
			"value": functions.conditional({
				"condition": null,
				"yesValue": null,
				"noValue": 0,
				"yesCallback": ""
			})
		});
		await functions.navCall({
			"function": "Update",
			"data": functions.toObject({
				"Qty_Phys_Inventory": functions.getVar({
					"var": "v_tmp_qty"
				}),
				"Key": functions.getVarAttr({
					"var": "v_current_stock_line_selected",
					"attr": "Key"
				}),
				"Item_No": functions.getVarAttr({
					"var": "v_current_stock_line_selected",
					"attr": "Item_No"
				})
			}),
			"callback": async (data, extra) => {
				await functions.loadData({
					"limit": 999999,
					"filter": functions.toArray({
						"value1": functions.toObject({
							"f": "Document_No",
							"v": functions.getVar({
								"var": "v_stock_count_document_no"
							})
						}),
						"value2": functions.toObject({
							"f": "Location_Code",
							"v": functions.getVar({
								"var": "v_selected_location_code"
							})
						}),
						"value3": functions.toObject({
							"f": "Posting_Date",
							"v": functions.dbDate({})
						})
					}),
					"order": functions.toArray({
						"value1": functions.toObject({
							"f": "Item_No",
							"v": "asc"
						})
					}),
					"callback": async (data, extra) => {
						functions.console({
							"value": data
						});
						functions.setVar({
							"var": "v_input_Physical_Inventory_Journal",
							"value": data
						});
						await functions.dataFromString({
							"dataset": "l_Physical_Inventory_Journal",
							"string": data,
							"callback": "",
							"errCallback": null
						});
						await functions.loadData({
							"dataset": "l_Physical_Inventory_Journal",
							"callback": async (data, extra) => {
								functions.setComponentValue({
									"component": "txtSearch"
								});
								functions.userDefined('globalModalHide', {});
							},
							"errCallback": ""
						});
					},
					"errCallback": async (data, extra) => {
						functions.setVar({
							"var": "vAction",
							"value": "calculate_error"
						});
						functions.userDefined('globalModalHide', {});
						functions.userDefined('globalModalInfo', {
							"title": "Navision Server Error",
							"message": functions.objectAttr({
								"object": functions.objectAttr({
									"object": data,
									"attr": "err"
								}),
								"attr": "global"
							}),
							"btnCaption": "OK"
						});
					},
					"dataset": "OK365_Physical_Inventory_Journal"
				});
			},
			"errCallback": async (data, extra) => {
				functions.userDefined('globalModalHide', {});
				functions.userDefined('globalModalInfo', {
					"title": "Navision Server Error",
					"message": functions.objectAttr({
						"object": functions.objectAttr({
							"object": data,
							"attr": "err"
						}),
						"attr": "global"
					})
				});
			},
			"connector": "nav",
			"ent": "OK365_Physical_Inventory_Journal"
		});
	},

	handlePanel435Click: async () => {
		functions.hideElement({
			"component": "pnlModalAddItem"
		});
		functions.setVar({
			"var": "v_current_item_selected",
			"value": data
		});
		functions.setComponentValue({
			"component": "lblAddItemQtyTitle",
			"value": functions.concat({
				"string1": data["No"],
				"string2": " - ",
				"string3": data["Description"]
			})
		});
		functions.setComponentValue({
			"component": "edtAddItemQty"
		});
		functions.setComponentValue({
			"component": "lblAddItemQtyTitleUOM",
			"value": data["Base_Unit_of_Measure"]
		});
		functions.showElement({
			"component": "pnlModalAddItemQty"
		});
	},

	handlePnlBtnCalculateYes912356Click: async () => {
		functions.hideElement({
			"component": "pnlModalAddItem"
		});
	},

	handlePnlBtnCalculateYes9129823Click: async () => {
		functions.hideElement({
			"component": "pnlModalAddItemQty"
		});
	},

	handlePnlBtnCalculateNo272602Click: async () => {
		functions.hideElement({
			"component": "pnlModalAddItemQty"
		});
		functions.setVar({
			"var": "v_tmp_qty",
			"value": functions.conditional({
				"condition": null,
				"yesValue": null,
				"noValue": 0,
				"yesCallback": "",
				"noCallback": ""
			})
		});
		functions.conditional({
			"condition": functions.notEqual({
				"value1": functions.getVar({
					"var": "v_tmp_qty"
				}),
				"value2": 0
			}),
			"yesCallback": async (data, extra) => {
				functions.userDefined('globalModalLoading', {});
				await functions.navCall({
					"callback": async (data, extra) => {
						await functions.loadData({
							"order": functions.toArray({
								"value1": functions.toObject({
									"v": "asc",
									"f": "Item_No"
								})
							}),
							"callback": async (data, extra) => {
								functions.console({
									"value": data
								});
								functions.setVar({
									"var": "v_input_Physical_Inventory_Journal",
									"value": data
								});
								await functions.dataFromString({
									"dataset": "l_Physical_Inventory_Journal",
									"string": data,
									"callback": "",
									"errCallback": null
								});
								await functions.loadData({
									"errCallback": "",
									"dataset": "l_Physical_Inventory_Journal",
									"callback": async (data, extra) => {
										functions.setComponentValue({
											"component": "txtSearch"
										});
										functions.userDefined('globalModalHide', {});
									}
								});
							},
							"errCallback": async (data, extra) => {
								functions.setVar({
									"var": "vAction",
									"value": "calculate_error"
								});
								functions.userDefined('globalModalHide', {});
								functions.userDefined('globalModalInfo', {
									"title": "Navision Server Error",
									"message": functions.objectAttr({
										"object": functions.objectAttr({
											"object": data,
											"attr": "err"
										}),
										"attr": "global"
									}),
									"btnCaption": "OK"
								});
							},
							"dataset": "OK365_Physical_Inventory_Journal",
							"limit": 999999,
							"filter": functions.toArray({
								"value1": functions.toObject({
									"f": "Document_No",
									"v": functions.getVar({
										"var": "v_stock_count_document_no"
									})
								}),
								"value2": functions.toObject({
									"f": "Location_Code",
									"v": functions.getVar({
										"var": "v_selected_location_code"
									})
								}),
								"value3": functions.toObject({
									"f": "Posting_Date",
									"v": functions.dbDate({})
								})
							})
						});
					},
					"errCallback": async (data, extra) => {
						functions.userDefined('globalModalHide', {});
						functions.userDefined('globalModalInfo', {
							"title": "Navision Server Error",
							"message": functions.objectAttr({
								"object": functions.objectAttr({
									"object": data,
									"attr": "err"
								}),
								"attr": "global"
							})
						});
					},
					"connector": "nav",
					"ent": "OK365_Physical_Inventory_Journal",
					"function": "Create",
					"data": functions.toObject({
						"Qty_Phys_Inventory": functions.getVar({
							"var": "v_tmp_qty"
						}),
						"Document_No": functions.objectAttr({
							"object": await functions.selectBy({
								"value": 1,
								"first": true,
								"callback": null,
								"errCallback": null,
								"dataset": "l_Physical_Inventory_Journal",
								"by": "_id"
							}),
							"attr": "Document_No"
						}),
						"Journal_Template_Name": functions.objectAttr({
							"attr": "Journal_Template_Name",
							"object": await functions.selectBy({
								"dataset": "l_Physical_Inventory_Journal",
								"by": "_id",
								"value": 1,
								"first": true,
								"callback": null,
								"errCallback": null
							})
						}),
						"Journal_Batch_Name": functions.objectAttr({
							"object": await functions.selectBy({
								"value": 1,
								"first": true,
								"callback": null,
								"errCallback": null,
								"dataset": "l_Physical_Inventory_Journal",
								"by": "_id"
							}),
							"attr": "Journal_Batch_Name"
						}),
						"Posting_Date": functions.objectAttr({
							"object": await functions.selectBy({
								"dataset": "l_Physical_Inventory_Journal",
								"by": "_id",
								"value": 1,
								"first": true,
								"callback": null,
								"errCallback": null
							}),
							"attr": "Posting_Date"
						}),
						"Line_No": 4920000,
						"Item_No": functions.getVarAttr({
							"var": "v_current_item_selected",
							"attr": "No"
						})
					})
				});
			},
			"noCallback": async (data, extra) => {
				functions.setVar({
					"var": "vAction",
					"value": "zero_qty"
				});
				functions.userDefined('globalModalInfo', {
					"title": " ",
					"message": "Please specify quantity to continue.",
					"btnCaption": "OK"
				});
			}
		});
	},

	handlePgStockTakeLoad: async () => {
		functions.onBackButton({
			"callback": async (data, extra) => {
				functions.gotoPage({
					"p": "pgMainMenu"
				});
			}
		});
		functions.userDefined('globalModalLoading', {});
		functions.setVar({
			"var": "v_selected_location_code",
			"value": functions.objectAttr({
				"object": await functions.selectBy({
					"by": "_id",
					"value": 1,
					"first": true,
					"callback": null,
					"errCallback": null,
					"dataset": "l_Route_Master"
				}),
				"attr": "Location_Code"
			})
		});
		functions.setVar({
			"var": "v_stock_count_document_no",
			"value": functions.concat({
				"string1": functions.formatDate({
					"date": functions.dbDate({}),
					"format": "dmY"
				}),
				"string2": "-",
				"string3": functions.getVar({
					"var": "v_selected_location_code"
				})
			})
		});
		functions.setTimeout({
			"timeout": 100,
			"callback": async (data, extra) => {
				await functions.loadData({
					"filter": functions.toArray({
						"value1": functions.toObject({
							"v": functions.getVar({
								"var": "v_stock_count_document_no"
							}),
							"f": "Document_No"
						}),
						"value2": functions.toObject({
							"f": "Location_Code",
							"v": functions.getVar({
								"var": "v_selected_location_code"
							})
						}),
						"value3": functions.toObject({
							"f": "Posting_Date",
							"v": functions.dbDate({})
						}),
						"value4": functions.toObject({
							"v": "Open",
							"f": "Status"
						})
					}),
					"order": functions.toArray({
						"value1": functions.toObject({
							"f": "Item_No",
							"v": "asc"
						})
					}),
					"callback": async (data, extra) => {
						functions.console({
							"value": data
						});
						functions.setVar({
							"var": "v_input_Physical_Inventory_Journal",
							"value": data
						});
						await functions.dataFromString({
							"dataset": "l_Physical_Inventory_Journal",
							"string": data,
							"callback": "",
							"errCallback": null
						});
						functions.conditional({
							"condition": functions.count({
								"values": await functions.selectAll({
									"dataset": "l_Physical_Inventory_Journal",
									"callback": null,
									"errCallback": null
								})
							}),
							"yesValue": "",
							"yesCallback": async (data, extra) => {
								functions.console({
									"value": "Yes"
								});
								await functions.loadData({
									"dataset": "l_Physical_Inventory_Journal",
									"callback": async (data, extra) => {
										functions.hideElement({
											"component": "pnlModalCalculate"
										});
										functions.userDefined('globalModalHide', {});
									},
									"errCallback": ""
								});
							},
							"noCallback": async (data, extra) => {
								functions.console({
									"value": "No"
								});
								functions.userDefined('globalModalHide', {});
								functions.showElement({
									"component": "pnlModalCalculate"
								});
							}
						});
					},
					"errCallback": async (data, extra) => {
						functions.setVar({
							"var": "vAction",
							"value": "calculate_error"
						});
						functions.userDefined('globalModalHide', {});
						functions.userDefined('globalModalInfo', {
							"title": "Navision Server Error",
							"message": functions.objectAttr({
								"object": functions.objectAttr({
									"object": data,
									"attr": "err"
								}),
								"attr": "global"
							}),
							"btnCaption": "OK"
						});
					},
					"dataset": "OK365_Physical_Inventory_Journal",
					"limit": 999999
				});
			}
		});
	}
};

const PgStockTake = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;
	formatParentStyleLayout(props);

	useEffect(() => {
		actions.handlePgStockTakeLoad();
	}, [currentTime]);
    
    return (
		<Page data={props.pgStockTake}>
			<Panel data={props.pnlHolder}>
				<Panel data={props.pnlMain}>
					<Panel data={props.pnlMainHeader}>
						<Panel data={props.pnlMainHeaderLeft}>
							<Panel data={props.pnlMainHeaderLeftTable}>
								<Panel data={props.pnlMainHeaderLeftCell}>
									<Image data={props.imgBack} action={actions.handleImgBackClick} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderMiddle}>
							<Panel data={props.pnlMainHeaderMiddleTable}>
								<Panel data={props.pnlMainHeaderMiddleCell}>
									<Label data={props.lblMainTitle} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderRight}>
							<Panel data={props.pnlMainHeaderRightTable}>
								<Panel data={props.pnlMainHeaderRightCell}>
									<Image data={props.imgAddNew} action={actions.handleImgAddNewClick} />
									<Label data={props.lblFilter54} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainSearch}>
						<Panel data={props.pnlMainBodySearch}>
							<Edit data={props.txtSearch} />
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBody}>
						<Panel data={props.pnlMainBodyItemsCO2656}>
							<DataList data={props.dtSales_Return_Lines} functions={functions} currentTime={currentTime}>
								{({ item, parentStyle }) => (							
									<Panel data={item.Panel17} parentStyle={parentStyle}>
										<Panel data={item.pnlProduct} parentStyle={parentStyle}>
											<Panel data={item.pnlProductBodyMiddle} parentStyle={parentStyle}>
												<Panel data={item.pnlProductBodyMiddle1} parentStyle={parentStyle}>
													<Label data={item.lblItemCode} parentStyle={parentStyle} />
													<Label data={item.lblQuantity} parentStyle={parentStyle} />
													<Label data={item.lblItemName} parentStyle={parentStyle} />
												</Panel>
											</Panel>
											<Panel data={item.Panel489} action={actions.handlePanel489Click} parentStyle={parentStyle}>
												<Image data={item.Image776} parentStyle={parentStyle} />
											</Panel>
										</Panel>
									</Panel>
								)}
							</DataList>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainFooter}>
						<Panel data={props.pnlFooter899} action={actions.handlePnlFooter899Click}>
							<Label data={props.lblPending35985603856} />
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal}>
					<Panel data={props.pnlModalTable}>
						<Panel data={props.pnlModalCell}>
							<Panel data={props.pnlModalBodyMain}>
								<Panel data={props.pnlModalBodyTitle}>
									<Label data={props.lblModalTitle} />
								</Panel>
								<Panel data={props.pnlModalBodyMessage}>
									<Label data={props.lblModalMessage} />
								</Panel>
								<Panel data={props.pnlModalBodyLoading}>
									<Image data={props.imgModalLoading} />
								</Panel>
							</Panel>
							<Panel data={props.pnlModalBodyButtons}>
								<Panel data={props.pnlModalBodyButtonPositive} action={actions.handlePnlModalBodyButtonPositiveClick}>
									<Label data={props.lblModalPositive} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonNegative} action={actions.handlePnlModalBodyButtonNegativeClick}>
									<Label data={props.lblModalNegative} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlCSS}>
					<Label data={props.lblStyle} />
				</Panel>
				<Panel data={props.pnlModalCalculate}>
					<Panel data={props.pnlCalculateTable}>
						<Panel data={props.pnlCalculateTableCell}>
							<Panel data={props.pnlCalculateTableCellContent}>
								<Panel data={props.pnlMainBodyCalculateContent}>
									<Panel data={props.pnlBodyCalculateContent}>
										<Label data={props.lblMsgCalculate} />
									</Panel>
								</Panel>
								<Panel data={props.pnlMainFooterCalculateBtn}>
									<Panel data={props.pnlBtnCalculateYes} action={actions.handlePnlBtnCalculateYesClick}>
										<Label data={props.lblCalculateOK} />
									</Panel>
									<Panel data={props.pnlBtnCalculateNo} action={actions.handlePnlBtnCalculateNoClick}>
										<Label data={props.lblCalculateNo} />
									</Panel>
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModalEditQty}>
					<Panel data={props.pnlCalculateTable668645}>
						<Panel data={props.pnlCalculateTableCell600}>
							<Panel data={props.pnlCalculateTableCellContent638}>
								<Label data={props.lblEdtQtyTitle} />
								<Panel data={props.pnlMainBodyCalculateContent58}>
									<Panel data={props.pnlBodyCalculateContent907}>
										<Edit data={props.edtQty} />
										<Label data={props.lblEditQtyUOM} />
									</Panel>
								</Panel>
								<Panel data={props.pnlMainFooterCalculateBtn455}>
									<Panel data={props.pnlBtnCalculateYes912} action={actions.handlePnlBtnCalculateYes912Click}>
										<Label data={props.lblCalculateOK302} />
									</Panel>
									<Panel data={props.pnlBtnCalculateNo27} action={actions.handlePnlBtnCalculateNo27Click}>
										<Label data={props.lblCalculateNo807} />
									</Panel>
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModalAddItem}>
					<Panel data={props.pnlCalculateTable668645118423}>
						<Panel data={props.pnlCalculateTableCell600941}>
							<Panel data={props.pnlCalculateTableCellContent638435}>
								<Panel data={props.Panel291}>
									<Label data={props.lblEdtQtyTitle537} />
								</Panel>
								<Panel data={props.Panel291662}>
									<Edit data={props.Edit658} />
								</Panel>
								<Panel data={props.pnlMainBodyCalculateContent58149}>
									<DataList data={props.DataList334} functions={functions} currentTime={currentTime}>
										{({ item, parentStyle }) => (									
											<Panel data={item.pnlBodyCalculateContent907237} parentStyle={parentStyle}>
												<Panel data={item.Panel816} parentStyle={parentStyle}>
													<Image data={item.Image180} parentStyle={parentStyle} />
												</Panel>
												<Panel data={item.Panel60} parentStyle={parentStyle}>
													<Label data={item.Label117} parentStyle={parentStyle} />
													<Label data={item.Label742} parentStyle={parentStyle} />
												</Panel>
												<Panel data={item.Panel435} action={actions.handlePanel435Click} parentStyle={parentStyle}>
													<Image data={item.Image794} parentStyle={parentStyle} />
												</Panel>
											</Panel>
										)}
									</DataList>
								</Panel>
								<Panel data={props.pnlMainFooterCalculateBtn455666}>
									<Panel data={props.pnlBtnCalculateYes912356} action={actions.handlePnlBtnCalculateYes912356Click}>
										<Label data={props.lblCalculateOK302338} />
									</Panel>
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModalAddItemQty}>
					<Panel data={props.pnlCalculateTable668645362665}>
						<Panel data={props.pnlCalculateTableCell60070}>
							<Panel data={props.pnlCalculateTableCellContent63886}>
								<Label data={props.lblAddItemQtyTitle} />
								<Panel data={props.pnlMainBodyCalculateContent58436}>
									<Panel data={props.pnlBodyCalculateContent907501}>
										<Edit data={props.edtAddItemQty} />
										<Label data={props.lblAddItemQtyTitleUOM} />
									</Panel>
								</Panel>
								<Panel data={props.pnlMainFooterCalculateBtn4551783}>
									<Panel data={props.pnlBtnCalculateYes9129823} action={actions.handlePnlBtnCalculateYes9129823Click}>
										<Label data={props.lblCalculateOK302794} />
									</Panel>
									<Panel data={props.pnlBtnCalculateNo272602} action={actions.handlePnlBtnCalculateNo272602Click}>
										<Label data={props.lblCalculateNo807937} />
									</Panel>
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgStockTake);