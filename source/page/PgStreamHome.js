import React from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableOpacity, SafeAreaView, FlatList, ImageBackground, StyleSheet } from 'react-native';
import get from 'lodash/get';
import SocketManager from '../dependency/stream/socketManager';
import LiveStreamCard from '../framework/component/stream/LiveStreamCard';

class PgStreamHome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listLiveStream: [],
      userName: 'anchor_'+Math.floor(Math.random() * 100) + 1
    };
    const userName = this.state.userName;
  }

  componentDidMount() {
    SocketManager.instance.emitListLiveStream();
    SocketManager.instance.listenListLiveStream((data) => {
      this.setState({ listLiveStream: data });
    });
  }

  onPressLiveStreamNow = () => {
    const { route } = this.props;
    //const userName = get(route, 'params.userName', '');
    const userName = this.state.userName;
    const {
      navigation: { navigate },
    } = this.props;
    navigate('PgStreamStreaming', { userName, roomName: userName });
  };

  onPressCardItem = (data) => {
    const { route } = this.props;
    //const userName = get(route, 'params.userName', '');
    const userName = this.state.userName;
    const {
      navigation: { navigate },
    } = this.props;
    navigate('PgStreamView', { userName, data });
  };

  render() {
    const { route } = this.props;
    //const userName = get(route, 'params.userName', '');
    const userName = this.state.userName;
    const { listLiveStream } = this.state;
    return (
      <SafeAreaView style={styles.container}>
        <ImageBackground
          source={require('../asset/public/stream/img/background.png')}
          style={styles.bgImage}
          resizeMode="cover"
        >
          <Text style={styles.welcomeText}>User: {userName}</Text>
          <Text style={styles.title}>List live stream video</Text>
          <FlatList
            contentContainerStyle={styles.flatList}
            data={listLiveStream}
            renderItem={({ item }) => <LiveStreamCard data={item} onPress={this.onPressCardItem} />}
            keyExtractor={(item) => item._id}
          />
          <TouchableOpacity style={styles.liveStreamButton} onPress={this.onPressLiveStreamNow}>
            <Text style={styles.textButton}>LiveStream Now</Text>
          </TouchableOpacity>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

PgStreamHome.propTypes = {
  route: PropTypes.shape({}),
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

PgStreamHome.defaultProps = {
  route: null,
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#3498db',
    },
    liveStreamButton: {
      backgroundColor: 'green',
      alignItems: 'center',
      paddingVertical: 15,
      borderRadius: 1,
      marginHorizontal: 25,
      marginBottom: 15,
    },
    textButton: {
      color: 'white',
      fontSize: 17,
    },
    input: {
      backgroundColor: 'white',
      borderRadius: 10,
      paddingVertical: 20,
      paddingHorizontal: 20,
      marginVertical: 20,
      marginHorizontal: 25,
      fontSize: 23,
      fontWeight: '600',
    },
    flatList: {
      marginHorizontal: 15,
    },
    welcomeText: {
      fontSize: 15,
      color: 'white',
      fontWeight: 'bold',
      marginLeft: 20,
      marginTop: 25,
    },
    title: {
      fontSize: 17,
      color: 'white',
      fontWeight: '300',
      marginLeft: 20,
      marginVertical: 5,
    },
    bgImage: {
      flex: 1,
      justifyContent: 'space-around',
    },
  });

export default PgStreamHome;
