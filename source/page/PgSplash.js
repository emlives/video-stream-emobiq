import React from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgSplash';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	getLblLicensed376Value: () => {

		console.log("///////////////////// getLblLicensed376Value invoked /////////////////////////")

		return functions.concat({
			"string1": "© ",
			"string2": functions.formatDate({
				"date": functions.dbDate({}),
				"format": "Y"
			}),
			"string3": " OrangeKloud Pte. Ltd., All Rights Reserved. <br>Licensed to ",
			"string4": "MSC Consulting Pte. Ltd."
		});
	},

	handlePgSplashLoad: () => {
		functions.selectBy({
			"dataset": "l_nav_setting",
			"by": "id",
			"operator": "",
			"value": 1,
			"first": true,
			"callback": (input, extra) => {
				functions.setVar({
					"var": "v_def_setting",
					"value": input
				});
				functions.conditional({
					"condition": functions.getVar({
						"var": "v_def_setting",
						"default": ""
					}),
					"yesValue": "",
					"noValue": "",
					"extra": "",
					"yesCallback": (input, extra) => {
						functions.setComponentAttr({
							"component": "nav",
							"componentId": "",
							"attr": "url",
							"value": functions.getVarAttr({
								"var": "v_def_setting",
								"attr": "url",
								"default": ""
							})
						});
						functions.setComponentAttr({
							"component": "nav",
							"componentId": "",
							"attr": "user",
							"value": functions.getVarAttr({
								"var": "v_def_setting",
								"attr": "username",
								"default": ""
							})
						});
						functions.setComponentAttr({
							"component": "nav",
							"componentId": "",
							"attr": "password",
							"value": functions.getVarAttr({
								"var": "v_def_setting",
								"attr": "password",
								"default": ""
							})
						});
						functions.setComponentAttr({
							"component": "nav",
							"componentId": "",
							"attr": "company",
							"value": functions.getVarAttr({
								"var": "v_def_setting",
								"attr": "company",
								"default": ""
							})
						});
						functions.setComponentAttr({
							"component": "nav_odata",
							"componentId": "",
							"attr": "url",
							"value": functions.getVarAttr({
								"var": "v_def_setting",
								"attr": "url"
							})
						});
						functions.setComponentAttr({
							"component": "nav_odata",
							"componentId": "",
							"attr": "user",
							"value": functions.getVarAttr({
								"var": "v_def_setting",
								"attr": "username"
							})
						});
						functions.setComponentAttr({
							"component": "nav_odata",
							"componentId": "",
							"attr": "password",
							"value": functions.getVarAttr({
								"var": "v_def_setting",
								"attr": "password",
								"default": ""
							})
						});
						functions.setComponentAttr({
							"component": "nav_odata",
							"componentId": "",
							"attr": "company",
							"value": functions.getVarAttr({
								"var": "v_def_setting",
								"attr": "company"
							})
						});
						functions.loadData({
							"filter": functions.toArray({
								"value1": "",
								"value2": "",
								"value3": "",
								"value4": "",
								"value5": "",
								"value6": "",
								"value7": "",
								"value8": "",
								"value9": "",
								"value10": ""
							}),
							"order": "",
							"callback": (input, extra) => {
								functions.gotoPage({
									"p": "pgLogin"
								});
							},
							"errCallback": (input, extra) => {
								functions.setVar({
									"var": "error",
									"value": input["err"]
								});
								functions.gotoPage({
									"p": "pgRegisterNAVConnectionInfo"
								});
							},
							"dataset": "OK365_Mobile_User_Master",
							"limit": 1
						});
					},
					"noCallback": (input, extra) => {
						functions.setTimeout({
							"timeout": 1000,
							"extra": "",
							"callback": (input, extra) => {
								functions.gotoPage({
									"p": "pgRegisterNAVConnectionInfo"
								});
							}
						});
					}
				});
			},
			"errCallback": null
		});
	}
};

functions.pageActions = actions

const PgSplash = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;

    // Manipulate the props to support css styles
    formatParentStyleLayout(props);

    return (
		<Page data={props.pgSplash} load={actions.handlePgSplashLoad} currentTime={currentTime} initializePage={functions.coreStateInitializePage}>
			<Panel data={props.Panel81} functions={functions} currentTime={currentTime}>
				<Panel data={props.Panel118} functions={functions} currentTime={currentTime}>
					<Image data={props.Image246} functions={functions} currentTime={currentTime}/>
					<Label data={props.Label55660} currentTime={currentTime} functions={functions}/>
				</Panel>
				<Panel data={props.Panel308} functions={functions} currentTime={currentTime}>
					<Label data={props.lblLicensed376} functions={functions} currentTime={currentTime}/>
				</Panel>
				<Panel data={props.Panel607} currentTime={currentTime} functions={functions}>
					<Image data={props.Image525} functions={functions} currentTime={currentTime}/>
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgSplash);