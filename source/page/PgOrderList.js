import React, { useEffect }  from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgOrderList';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import Edit from '../framework/component/Edit';
import DataList from '../framework/component/DataList';
import ComboBox from '../framework/component/ComboBox';
import Button from '../framework/component/Button';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handleImgBackClick: async () => {
		functions.gotoPage({
			"p": "pgMainMenu"
		});
	},

	handlePnlPendingClick: async () => {
		functions.toArray({
			"value1": functions.setObjectAttr({
				"object": null,
				"attr": "borderBottom",
				"value": functions.concat({
					"string1": "2px solid ",
					"string2": functions.getVar({
						"var": "varColor"
					})
				})
			}),
			"value2": functions.setObjectAttr({
				"object": null,
				"attr": "borderBottom",
				"value": "2px solid #CCCCCC"
			}),
			"value3": "",
			"value4": ""
		});
		functions.toArray({
			"value1": functions.setObjectAttr({
				"object": null,
				"attr": "color",
				"value": functions.getVar({
					"var": "varColor"
				})
			}),
			"value2": functions.setObjectAttr({
				"object": null,
				"attr": "color",
				"value": "#CCCCCC"
			}),
			"value3": "",
			"value4": ""
		});
		functions.toArray({
			"value1": functions.setObjectAttr({
				"object": null,
				"attr": "display",
				"value": "block"
			}),
			"value2": functions.setObjectAttr({
				"object": null,
				"attr": "display",
				"value": "none"
			}),
			"value3": "",
			"value4": "",
			"value5": ""
		});
		functions.setVar({
			"var": "vType",
			"value": "draft"
		});
		functions.toArray({
			"value1": functions.setObjectAttr({
				"object": null,
				"attr": "display",
				"value": "none"
			}),
			"value2": functions.setObjectAttr({
				"object": null,
				"attr": "display",
				"value": "block"
			})
		});
	},

	handleImgUpdateCartCloseClick: async () => {
		functions.setComponentValue({
			"component": "txtQuantity",
			"value": ""
		});
		functions.hideElement({
			"component": "pnlUpdateCartModal"
		});
	},

	handleLblSelectCustomerClick: async () => {
		functions.console({
			"value": functions.greater({
				"value1": functions.count({
					"values": await functions.selectAll({
						"dataset": "l_customer",
						"callback": null,
						"errCallback": null
					})
				}),
				"value2": 0
			})
		});
		functions.setComponentValue({
			"component": "lblAll",
			"value": "All"
		});
		functions.conditional({
			"condition": "",
			"yesCallback": async (data, extra) => {
				functions.userDefined('globalModalLoading', {});
				await functions.loadData({
					"dataset": "l_customer",
					"filter": functions.toArray({
						"value1": functions.toObject({
							"f": "CardCode",
							"o": "!=",
							"v": "[object Object]"
						})
					}),
					"orFilter": functions.toArray({
						"value1": functions.toObject({
							"f": "CardCode",
							"o": "ilike",
							"v": null
						}),
						"value2": functions.toObject({
							"f": "CardName",
							"o": "ilike",
							"v": null
						})
					}),
					"order": functions.toArray({
						"value1": functions.toObject({
							"f": "CardCode",
							"v": "asc"
						})
					}),
					"callback": async (data, extra) => {
						functions.userDefined('globalModalHide', {});
						functions.showElement({
							"component": "pnlSelectCustomerModal"
						});
					},
					"errCallback": ""
				});
			},
			"noCallback": async (data, extra) => {
				functions.userDefined('globalModalLoading', {});
			}
		});
	},

	handleLblSelectOutletCodeClick: async () => {
		functions.userDefined('globalModalLoading', {});
		functions.conditional({
			"condition": functions.equal({
				"value2": "My",
				"value1": null
			}),
			"yesCallback": "",
			"noCallback": ""
		});
		functions.toArray({
			"value1": "",
			"value2": functions.setComponentValue({
				"component": "txtShipToCode"
			}),
			"value3": functions.setComponentValue({
				"component": "etAddress"
			})
		});
	},

	handleBtnSearchClick: async () => {
		functions.conditional({
			"condition": functions.getVarAttr({
				"var": "vCurrSalesBP",
				"attr": "CardCode"
			}),
			"yesCallback": async (data, extra) => {
				functions.conditional({
					"condition": functions.equal({
						"value1": functions.getVar({
							"var": "vType"
						}),
						"value2": "draft"
					}),
					"yesCallback": async (data, extra) => {
						functions.toArray({
							"value1": functions.userDefined('globalModalLoading', {}),
							"value2": null
						});
					},
					"noCallback": async (data, extra) => {
						functions.conditional({
							"condition": functions.equal({
								"value1": functions.getVar({
									"var": "vType"
								}),
								"value2": "open"
							}),
							"yesCallback": async (data, extra) => {
								functions.toArray({
									"value1": functions.userDefined('globalModalLoading', {}),
									"value2": null
								});
							},
							"noCallback": async (data, extra) => {
								functions.toArray({
									"value1": functions.userDefined('globalModalLoading', {}),
									"value2": null
								});
							}
						});
					}
				});
			},
			"noCallback": async (data, extra) => {
				functions.userDefined('globalModalInfo', {
					"title": "Error Filter",
					"message": "Please select a customer."
				});
			}
		});
	},

	handlePnlModalBodyButtonPositiveClick: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalHide', {});
	},

	handlePnlModalBodyButtonNegativeClick: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalHide', {});
	},

	handlePgOrderListLoad: async () => {
		functions.onBackButton({
			"callback": async (data, extra) => {
				functions.gotoPage({
					"p": "pgMainMenu"
				});
			}
		});
		functions.setVar({
			"var": "vAction",
			"value": ""
		});
		functions.userDefined('globalModalLoading', {});
		functions.conditional({
			"condition": functions.getVar({
				"var": "vCurrCustomer",
				"default": ""
			}),
			"yesValue": "",
			"noValue": "",
			"extra": "",
			"yesCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtSearch",
					"componentId": "",
					"value": functions.getVarAttr({
						"var": "vCurrCustomer",
						"attr": "No",
						"default": ""
					})
				});
			},
			"noCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtSearch",
					"componentId": "",
					"value": ""
				});
			}
		});
		await functions.clearData({
			"dataset": "l_orderlist",
			"callback": null,
			"errCallback": null
		});
		await functions.loadData({
			"errCallback": async (data, extra) => {
				functions.setVar({
					"var": "error",
					"value": data["err"]
				});
				functions.userDefined('globalModalInfo', {
					"title": "Error",
					"message": functions.conditional({
						"condition": functions.getVarAttr({
							"var": "error",
							"attr": "global"
						}),
						"yesValue": functions.getVarAttr({
							"var": "error",
							"attr": "global"
						}),
						"noValue": "Cannot connect to Navision Server."
					})
				});
			},
			"dataset": "OK365_Order_List",
			"limit": 10,
			"filter": functions.toArray({
				"value1": functions.toObject({
					"f": "Location_Code",
					"v": functions.getVarAttr({
						"var": "vCurrCustomerDetails",
						"attr": "Location_Code"
					})
				}),
				"value2": functions.toObject({
					"f": "Status",
					"v": "Open"
				}),
				"value3": functions.toObject({
					"f": "Sell_to_Customer_No",
					"v": functions.getVarAttr({
						"var": "vCurrCustomer",
						"attr": "Customer_Code",
						"default": ""
					}),
					"o": functions.conditional({
						"condition": functions.getVar({
							"var": "vCurrCustomer",
							"default": ""
						}),
						"yesValue": "=",
						"noValue": "<>",
						"extra": ""
					})
				}),
				"value4": "",
				"value5": "",
				"value6": "",
				"value7": "",
				"value8": "",
				"value9": "",
				"value10": ""
			}),
			"order": functions.toArray({
				"value1": functions.toObject({
					"v": "desc",
					"f": "No"
				}),
				"value2": "",
				"value3": "",
				"value4": "",
				"value5": "",
				"value6": "",
				"value7": "",
				"value8": "",
				"value9": "",
				"value10": ""
			}),
			"callback": async (data, extra) => {
				functions.userDefined('globalModalHide', {});
				functions.console({
					"value": data
				});
				functions.setVar({
					"var": "vTotalCount",
					"value": functions.count({
						"values": data
					})
				});
				functions.setVar({
					"var": "vLoopCount",
					"value": 0
				});
				functions.map({
					"values": data,
					"extra": "",
					"callback": async (data, extra) => {
						functions.setVar({
							"var": "vLoopCount",
							"value": functions.add({
								"value1": functions.getVar({
									"var": "vLoopCount",
									"default": ""
								}),
								"value2": 1
							})
						});
						functions.conditional({
							"condition": functions.equal({
								"value1": functions.count({
									"values": await functions.selectBy({
										"dataset": "l_orderlist",
										"by": "No",
										"operator": "=",
										"value": data["No"],
										"callback": null,
										"errCallback": null
									})
								}),
								"value2": 0
							}),
							"yesValue": "",
							"noValue": "",
							"extra": data,
							"yesCallback": async (data, extra) => {
								await functions.insert({
									"callback": async (data, extra) => {
										functions.conditional({
											"condition": functions.equal({
												"value2": functions.getVar({
													"var": "vLoopCount",
													"default": ""
												}),
												"value1": functions.getVar({
													"var": "vTotalCount",
													"default": ""
												})
											}),
											"yesValue": "",
											"noValue": "",
											"extra": "",
											"yesCallback": async (data, extra) => {
												await functions.loadData({
													"dataset": "l_orderlist",
													"callback": null,
													"errCallback": null
												});
											}
										});
									},
									"errCallback": null,
									"dataset": "l_orderlist",
									"dt": extra
								});
							},
							"noCallback": async (data, extra) => {
								functions.conditional({
									"condition": functions.equal({
										"value1": functions.getVar({
											"var": "vTotalCount",
											"default": ""
										}),
										"value2": functions.getVar({
											"var": "vLoopCount",
											"default": ""
										})
									}),
									"yesValue": "",
									"noValue": "",
									"extra": "",
									"yesCallback": async (data, extra) => {
										await functions.loadData({
											"dataset": "l_orderlist",
											"callback": null,
											"errCallback": null
										});
									}
								});
							}
						});
					}
				});
			}
		});
	}
};

const PgOrderList = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;
	formatParentStyleLayout(props);

	useEffect(() => {
		actions.handlePgOrderListLoad();
	}, [currentTime]);
    
    return (
		<Page data={props.pgOrderList}>
			<Panel data={props.pnlHolder}>
				<Panel data={props.pnlMain}>
					<Panel data={props.pnlMainHeader}>
						<Panel data={props.pnlMainHeaderLeft}>
							<Panel data={props.pnlMainHeaderLeftTable}>
								<Panel data={props.pnlMainHeaderLeftCell}>
									<Image data={props.imgBack} action={actions.handleImgBackClick} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderMiddle}>
							<Panel data={props.pnlMainHeaderMiddleTable}>
								<Panel data={props.pnlMainHeaderMiddleCell}>
									<Label data={props.lblMainTitle} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderRight}>
							<Panel data={props.pnlMainHeaderRightTable}>
								<Panel data={props.pnlMainHeaderRightCell}>
									<Image data={props.imgSearchFilters} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainSearch}>
						<Panel data={props.pnlMainBodySearch}>
							<Edit data={props.txtSearch} />
						</Panel>
					</Panel>
					<Panel data={props.pnlMainTabHeader}>
						<Panel data={props.pnlPending} action={actions.handlePnlPendingClick}>
							<Label data={props.lblPending} />
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBody}>
						<Panel data={props.pnlBody}>
							<Panel data={props.pnlMainTabPending}>
								<DataList data={props.dlDraft} functions={functions} currentTime={currentTime}>
									{({ item, parentStyle }) => (								
										<Panel data={item.pnlProduct} parentStyle={parentStyle}>
											<Panel data={item.pnlProductBody} parentStyle={parentStyle}>
												<Panel data={item.pnlProductBodyMiddle} parentStyle={parentStyle}>
													<Panel data={item.pnlProductBodyMiddle1} parentStyle={parentStyle}>
														<Label data={item.lblDocNumber1} parentStyle={parentStyle} />
														<Label data={item.lblDocNumber_data} parentStyle={parentStyle} />
													</Panel>
													<Panel data={item.pnlProductBodyMiddle2} parentStyle={parentStyle}>
														<Label data={item.lblCustomer1} parentStyle={parentStyle} />
														<Label data={item.lblCustomer_data} parentStyle={parentStyle} />
													</Panel>
													<Panel data={item.pnlProductBodyMiddle3} parentStyle={parentStyle}>
														<Label data={item.lblOulet1} parentStyle={parentStyle} />
														<Label data={item.lblOulet_data} parentStyle={parentStyle} />
													</Panel>
													<Panel data={item.pnlProductBodyMiddle4141161} parentStyle={parentStyle}>
														<Label data={item.lblDocumentDateAAA} parentStyle={parentStyle} />
														<Label data={item.lblDocumentDate_data4} parentStyle={parentStyle} />
													</Panel>
													<Panel data={item.pnlProductBodyMiddle5} parentStyle={parentStyle}>
														<Label data={item.lblDocTotal1} parentStyle={parentStyle} />
														<Label data={item.lblDocTotal_data} parentStyle={parentStyle} />
													</Panel>
												</Panel>
												<Panel data={item.Panel822} parentStyle={parentStyle}>
													<Image data={item.Image9} parentStyle={parentStyle} />
												</Panel>
											</Panel>
										</Panel>
									)}
								</DataList>
							</Panel>
							<Panel data={props.pnlMainTabApproved}>
								<DataList data={props.dlDraft938} functions={functions} currentTime={currentTime}>
									{({ item, parentStyle }) => (								
										<Panel data={item.pnlProduct88} parentStyle={parentStyle}>
											<Panel data={item.pnlProductBody369} parentStyle={parentStyle}>
												<Panel data={item.pnlProductBodyMiddle331} parentStyle={parentStyle}>
													<Panel data={item.pnlProductBodyMiddle196} parentStyle={parentStyle}>
														<Label data={item.lblDocNumber1450} parentStyle={parentStyle} />
														<Label data={item.lblDocNumber_data680} parentStyle={parentStyle} />
													</Panel>
													<Panel data={item.pnlProductBodyMiddle2510} parentStyle={parentStyle}>
														<Label data={item.lblCustomer1140} parentStyle={parentStyle} />
														<Label data={item.lblCustomer_data697} parentStyle={parentStyle} />
													</Panel>
													<Panel data={item.pnlProductBodyMiddle3261} parentStyle={parentStyle}>
														<Label data={item.lblOulet1902} parentStyle={parentStyle} />
														<Label data={item.lblOulet_data91} parentStyle={parentStyle} />
													</Panel>
													<Panel data={item.pnlProductBodyMiddle4141161528} parentStyle={parentStyle}>
														<Label data={item.lblDocumentDateAAA867} parentStyle={parentStyle} />
														<Label data={item.lblDocumentDate_data451308978962567} parentStyle={parentStyle} />
													</Panel>
													<Panel data={item.pnlProductBodyMiddle59} parentStyle={parentStyle}>
														<Label data={item.lblDocTotal1390} parentStyle={parentStyle} />
														<Label data={item.lblDocTotal_data861} parentStyle={parentStyle} />
													</Panel>
												</Panel>
											</Panel>
										</Panel>
									)}
								</DataList>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlUpdateCartModal}>
					<Panel data={props.pnlUpdateCartModalTable}>
						<Panel data={props.pnlUpdateCartModalCell}>
							<Panel data={props.pnlUpdateCartModalBody}>
								<Panel data={props.pnlUpdateCartModalBody1}>
									<Image data={props.imgUpdateCartImage} />
									<Image data={props.imgUpdateCartClose} action={actions.handleImgUpdateCartCloseClick} />
								</Panel>
								<Panel data={props.pnlUpdateCartModalBody2}>
									<Panel data={props.pnlMainBodyForm1}>
										<Panel data={props.pnlMainBodyForm1Label}>
											<Label data={props.lblCustomerName} />
											<Label data={props.lblSelectCustomer} action={actions.handleLblSelectCustomerClick} />
										</Panel>
										<Panel data={props.pnlMainBodyForm1Input}>
											<Edit data={props.txtCustomerName} />
										</Panel>
									</Panel>
									<Panel data={props.pnlMainBodyForm1481}>
										<Panel data={props.pnlMainBodyForm1Label551333}>
											<Label data={props.lblOutletCode} />
											<Label data={props.lblSelectOutletCode} action={actions.handleLblSelectOutletCodeClick} />
										</Panel>
										<Panel data={props.pnlMainBodyForm1Input180830}>
											<Edit data={props.txtOutletCode} />
										</Panel>
									</Panel>
									<Panel data={props.pnlMainBodyForm2}>
										<Panel data={props.pnlMainBodyForm2Label}>
											<Label data={props.lblDeliveryDateFrom} />
										</Panel>
										<Panel data={props.pnlMainBodyForm2Input}>
											<Edit data={props.txtDateFrom} />
											<Label data={props.lblDocumentDateTo} />
											<Edit data={props.txtDateTo} />
										</Panel>
									</Panel>
									<Panel data={props.pnlMainBodyForm2498}>
										<Panel data={props.pnlMainBodyForm2Label820995}>
											<Label data={props.lblDocumentDate} />
										</Panel>
										<Panel data={props.pnlMainBodyForm2Input739198}>
											<Edit data={props.txtDocumentDateFrom} />
											<Label data={props.lblDocumentDateTo851} />
											<Edit data={props.txtDocumentDateTo} />
										</Panel>
									</Panel>
									<Panel data={props.pnlMainBodyForm3}>
										<Panel data={props.pnlMainBodyForm1Label554794}>
											<Label data={props.lblCustomerName22} />
										</Panel>
										<Panel data={props.pnlMainBodyForm1Input696752}>
											<ComboBox data={props.cmbSalesperson} functions={functions} />
										</Panel>
									</Panel>
								</Panel>
								<Panel data={props.pnlUpdateCartModalBody5}>
									<Button data={props.btnSearch} action={actions.handleBtnSearchClick} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal}>
					<Panel data={props.pnlModalTable}>
						<Panel data={props.pnlModalCell}>
							<Panel data={props.pnlModalBodyMain}>
								<Panel data={props.pnlModalBodyTitle}>
									<Label data={props.lblModalTitle} />
								</Panel>
								<Panel data={props.pnlModalBodyMessage}>
									<Label data={props.lblModalMessage} />
								</Panel>
								<Panel data={props.pnlModalBodyLoading}>
									<Image data={props.imgModalLoading} />
								</Panel>
							</Panel>
							<Panel data={props.pnlModalBodyButtons}>
								<Panel data={props.pnlModalBodyButtonPositive} action={actions.handlePnlModalBodyButtonPositiveClick}>
									<Label data={props.lblModalPositive} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonNegative} action={actions.handlePnlModalBodyButtonNegativeClick}>
									<Label data={props.lblModalNegative} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlCSS}>
					<Label data={props.lblStyle} />
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgOrderList);