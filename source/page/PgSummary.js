import React, { useEffect }  from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgSummary';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import DataList from '../framework/component/DataList';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handleImgBackClick: async () => {
		functions.gotoPage({
			"p": "pgMainMenu"
		});
	},

	handlePnlMainFooterTableCellClick: async () => {
		functions.setVar({
			"var": "vSignature"
		});
		functions.gotoPage({
			"p": "pgPayment"
		});
	},

	handlePnlModalBodyButtonPositiveClick: async () => {
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "vAction"
				}),
				"value2": "delete_item"
			}),
			"yesCallback": async (data, extra) => {
				await functions.deleteBy({
					"callback": async (data, extra) => {
						functions.hideElement({
							"component": "pnlModal"
						});
					},
					"errCallback": "",
					"dataset": "l_cart",
					"by": "ItemCode",
					"value": functions.getVarAttr({
						"var": "vItemDetails",
						"attr": "ItemCode"
					})
				});
				functions.toArray({
					"value1": await functions.loadData({
						"callback": async (data, extra) => {
							functions.setVar({
								"var": "vTotalAmountItem",
								"value": 0
							});
							functions.map({
								"values": await functions.selectAll({
									"dataset": "l_cart",
									"callback": "",
									"errCallback": null
								}),
								"callback": async (data, extra) => {
									functions.console({
										"value": data
									});
									functions.setVar({
										"var": "vItemDetails_Order",
										"value": data
									});
									functions.setVar({
										"var": "item_amount",
										"value": functions.multi({
											"value1": functions.getVarAttr({
												"var": "vItemDetails_Order",
												"attr": "Quantity"
											}),
											"value2": functions.getVarAttr({
												"var": "vItemDetails_Order",
												"attr": "Price"
											})
										})
									});
									functions.setVar({
										"var": "vTotalAmountItem",
										"value": functions.add({
											"value1": functions.getVar({
												"var": "vTotalAmountItem",
												"default": ""
											}),
											"value2": functions.getVar({
												"var": "item_amount",
												"default": ""
											})
										})
									});
								}
							});
						},
						"errCallback": "",
						"dataset": "l_cart"
					}),
					"value2": await functions.loadData({
						"dataset": "l_empty_bottle",
						"callback": async (data, extra) => {
							functions.console({
								"value": data
							});
							functions.setVar({
								"var": "vTotalAmountEmpty",
								"value": 0
							});
							functions.map({
								"values": await functions.selectAll({
									"errCallback": "",
									"dataset": "l_empty_bottle",
									"callback": ""
								}),
								"callback": async (data, extra) => {
									functions.console({
										"value": data
									});
									functions.setVar({
										"var": "vEmptyDetails_Order_Amount",
										"value": data
									});
									functions.setVar({
										"var": "item_amount_empty",
										"value": functions.multi({
											"value1": functions.getVarAttr({
												"var": "vEmptyDetails_Order_Amount",
												"attr": "quantity"
											}),
											"value2": functions.getVarAttr({
												"var": "vEmptyDetails_Order_Amount",
												"attr": "price"
											})
										})
									});
									functions.setVar({
										"var": "vTotalAmountEmpty",
										"value": functions.add({
											"value1": functions.getVar({
												"var": "vTotalAmountEmpty",
												"default": ""
											}),
											"value2": functions.getVar({
												"var": "item_amount_empty",
												"default": ""
											})
										})
									});
									functions.console({
										"value": functions.getVar({
											"var": "item_amount_empty"
										})
									});
									functions.console({
										"value": functions.getVar({
											"var": "vTotalAmountEmpty",
											"default": ""
										})
									});
								}
							});
						},
						"errCallback": ""
					}),
					"value3": functions.userDefined('globalCalculateTotal', {
						"subTotal": functions.getVar({
							"var": "vTotalAmountItem"
						}),
						"emptyTotal": functions.getVar({
							"var": "vTotalAmountEmpty"
						}),
						"gstRate": functions.getVar({
							"var": "vGstTotalRate"
						})
					}),
					"value4": "",
					"value5": "",
					"value6": "",
					"value7": "",
					"value8": "",
					"value9": "",
					"value10": ""
				});
			},
			"noCallback": async (data, extra) => {
				functions.conditional({
					"condition": functions.equal({
						"value1": functions.getVar({
							"var": "vAction"
						}),
						"value2": "clear_return_cart"
					}),
					"yesCallback": async (data, extra) => {
						await functions.deleteBy({
							"dataset": "l_empty_bottle",
							"by": "name",
							"value": functions.getVarAttr({
								"var": "vGL_Group",
								"attr": "name"
							}),
							"callback": async (data, extra) => {
								functions.hideElement({
									"component": "pnlModal"
								});
							},
							"errCallback": ""
						});
						functions.toArray({
							"value1": await functions.loadData({
								"dataset": "l_cart",
								"callback": async (data, extra) => {
									functions.setVar({
										"var": "vTotalAmountItem",
										"value": 0
									});
									functions.map({
										"values": await functions.selectAll({
											"dataset": "l_cart",
											"callback": "",
											"errCallback": null
										}),
										"callback": async (data, extra) => {
											functions.console({
												"value": data
											});
											functions.setVar({
												"var": "vItemDetails_Order",
												"value": data
											});
											functions.setVar({
												"var": "item_amount",
												"value": functions.multi({
													"value1": functions.getVarAttr({
														"var": "vItemDetails_Order",
														"attr": "Quantity"
													}),
													"value2": functions.getVarAttr({
														"var": "vItemDetails_Order",
														"attr": "Price"
													})
												})
											});
											functions.setVar({
												"var": "vTotalAmountItem",
												"value": functions.add({
													"value1": functions.getVar({
														"var": "vTotalAmountItem",
														"default": ""
													}),
													"value2": functions.getVar({
														"var": "item_amount",
														"default": ""
													})
												})
											});
										}
									});
								},
								"errCallback": ""
							}),
							"value2": await functions.loadData({
								"dataset": "l_empty_bottle",
								"callback": async (data, extra) => {
									functions.console({
										"value": data
									});
									functions.setVar({
										"var": "vTotalAmountEmpty",
										"value": 0
									});
									functions.map({
										"values": await functions.selectAll({
											"dataset": "l_empty_bottle",
											"callback": "",
											"errCallback": ""
										}),
										"callback": async (data, extra) => {
											functions.console({
												"value": data
											});
											functions.setVar({
												"var": "vEmptyDetails_Order_Amount",
												"value": data
											});
											functions.setVar({
												"var": "item_amount_empty",
												"value": functions.multi({
													"value1": functions.getVarAttr({
														"var": "vEmptyDetails_Order_Amount",
														"attr": "quantity"
													}),
													"value2": functions.getVarAttr({
														"var": "vEmptyDetails_Order_Amount",
														"attr": "price"
													})
												})
											});
											functions.setVar({
												"var": "vTotalAmountEmpty",
												"value": functions.add({
													"value1": functions.getVar({
														"var": "vTotalAmountEmpty",
														"default": ""
													}),
													"value2": functions.getVar({
														"var": "item_amount_empty",
														"default": ""
													})
												})
											});
											functions.console({
												"value": functions.getVar({
													"var": "item_amount_empty"
												})
											});
											functions.console({
												"value": functions.getVar({
													"var": "vTotalAmountEmpty",
													"default": ""
												})
											});
										}
									});
								},
								"errCallback": ""
							}),
							"value3": functions.userDefined('globalCalculateTotal', {
								"subTotal": functions.getVar({
									"var": "vTotalAmountItem"
								}),
								"emptyTotal": functions.getVar({
									"var": "vTotalAmountEmpty"
								}),
								"gstRate": functions.getVar({
									"var": "vGstTotalRate"
								})
							}),
							"value4": "",
							"value5": "",
							"value6": "",
							"value7": "",
							"value8": "",
							"value9": "",
							"value10": ""
						});
					},
					"noCallback": async (data, extra) => {
						functions.conditional({
							"condition": functions.equal({
								"value1": functions.getVar({
									"var": "vAction"
								}),
								"value2": "submit_order"
							}),
							"yesCallback": async (data, extra) => {
								functions.setVar({
									"var": "vAction",
									"value": ""
								});
								functions.newArray({
									"var": "sales_line_data"
								});
								functions.map({
									"values": await functions.selectAll({
										"dataset": "l_cart",
										"callback": "",
										"errCallback": ""
									}),
									"callback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.setVar({
											"var": "vl_cart_total",
											"value": data
										});
										functions.push({
											"var": "sales_line_data",
											"value": functions.toObject({
												"Unit_Price": functions.getVarAttr({
													"var": "vl_cart_total",
													"attr": "Price"
												}),
												"Type": "Item",
												"Unit_of_Measure": functions.getVarAttr({
													"var": "vl_cart_total",
													"attr": "UOM"
												}),
												"No": functions.getVarAttr({
													"var": "vl_cart_total",
													"attr": "ItemCode"
												}),
												"Quantity": functions.getVarAttr({
													"var": "vl_cart_total",
													"attr": "Quantity"
												})
											})
										});
									}
								});
								functions.map({
									"values": await functions.selectAll({
										"dataset": "l_empty_bottle",
										"callback": "",
										"errCallback": ""
									}),
									"callback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.setVar({
											"var": "vl_cart_total_Empty",
											"value": data
										});
										functions.push({
											"var": "sales_line_data",
											"value": functions.toObject({
												"Unit_Price": functions.getVarAttr({
													"var": "vl_cart_total_Empty",
													"attr": "price"
												}),
												"Type": "G_L_Account",
												"No": functions.getVarAttr({
													"var": "vl_cart_total_Empty",
													"attr": "name"
												}),
												"Quantity": functions.multi({
													"value1": functions.getVarAttr({
														"var": "vl_cart_total_Empty",
														"attr": "quantity"
													}),
													"value2": -1
												})
											})
										});
									}
								});
								functions.console({
									"value": functions.getVar({
										"var": "sales_line_data"
									})
								});
								functions.userDefined('globalModalLoading', {});
								await functions.navCall({
									"connector": "nav",
									"ent": "OK365_Order_Entry",
									"function": "Create",
									"data": functions.toObject({
										"Order_Date": functions.dbDate({}),
										"SalesLines": functions.toObject({
											"Sales_Order_Line": functions.getVar({
												"var": "sales_line_data"
											})
										}),
										"Shipment_Date": functions.getVarAttr({
											"var": "vCheckout",
											"attr": "InvoiceDate"
										}),
										"Location_Code": functions.objectAttr({
											"object": functions.getVarAttr({
												"var": "var_loginCnt",
												"attr": 0
											}),
											"attr": "Location_Code"
										}),
										"Mobile_User_ID": functions.objectAttr({
											"object": functions.getVarAttr({
												"var": "var_loginCnt",
												"attr": 0
											}),
											"attr": "Mobile_Login_User_ID"
										}),
										"Sell_to_Customer_No": functions.getVarAttr({
											"var": "vCurrCustomerDetails",
											"attr": "No"
										})
									}),
									"callback": async (data, extra) => {
										functions.userDefined('globalModalHide', {});
										functions.console({
											"value": data
										});
										functions.setVar({
											"var": "vOrderSubmitResult",
											"value": data
										});
										functions.setVar({
											"var": "vOrderNo",
											"value": functions.objectAttr({
												"object": functions.getVarAttr({
													"var": "vOrderSubmitResult",
													"attr": "MSC_Order_Entry"
												}),
												"attr": "No"
											})
										});
										functions.console({
											"value": functions.getVar({
												"var": "vOrderNo"
											})
										});
										functions.userDefined('globalModalInfo', {
											"title": "Message",
											"message": functions.concat({
												"string1": "Your order No: ",
												"string2": functions.getVar({
													"var": "vOrderNo"
												}),
												"string3": "  ",
												"string4": "was successfully submitted."
											})
										});
										functions.setVar({
											"var": "vAction",
											"value": "redirect"
										});
										await functions.clearData({
											"dataset": "l_cart",
											"callback": "",
											"errCallback": ""
										});
										await functions.clearData({
											"callback": "",
											"errCallback": "",
											"dataset": "l_empty_bottle"
										});
										functions.setVar({
											"var": "vCurrCustomer"
										});
										functions.setVar({
											"var": "vCurrCustomerAddress"
										});
										functions.setVar({
											"var": "vCurrCustomerDetails"
										});
									},
									"errCallback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.console({
											"value": 2
										});
									}
								});
							},
							"noCallback": async (data, extra) => {
								functions.conditional({
									"condition": functions.equal({
										"value1": functions.getVar({
											"var": "vAction"
										}),
										"value2": "redirect"
									}),
									"yesCallback": async (data, extra) => {
										functions.gotoPage({
											"p": "pgMainMenu"
										});
										functions.setVar({
											"var": "vAction"
										});
									},
									"noCallback": async (data, extra) => {
										functions.conditional({
											"condition": functions.equal({
												"value1": functions.getVar({
													"var": "vAction"
												}),
												"value2": "submitSODraft"
											}),
											"yesCallback": async (data, extra) => {
												functions.setVar({
													"var": "vAction"
												});
											},
											"noCallback": async (data, extra) => {
												functions.userDefined('globalModalHide', {});
											}
										});
									}
								});
							}
						});
					}
				});
			}
		});
	},

	handleLblModalNegativeClick: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalHide', {});
	},

	handlePnlModalBodyButtonNegativeClick: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalHide', {});
	},

	handlePgSummaryLoad: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.toArray({
			"value1": functions.setComponentValue({
				"component": "lblCodeName_data",
				"value": functions.concat({
					"string1": functions.getVarAttr({
						"var": "vOrderEntryResult",
						"attr": "Sell_to_Customer_No",
						"default": ""
					}),
					"string2": " - ",
					"string3": functions.getVarAttr({
						"var": "vOrderEntryResult",
						"attr": "Sell_to_Customer_Name",
						"default": ""
					})
				})
			}),
			"value2": functions.setComponentValue({
				"component": "lblAddress_data",
				"value": functions.getVarAttr({
					"var": "vOrderEntryResult",
					"attr": "Sell_to_Address",
					"default": ""
				})
			}),
			"value3": functions.setComponentValue({
				"component": "lblOrderDate_data",
				"value": functions.formatDate({
					"date": functions.getVarAttr({
						"var": "vOrderEntryResult",
						"attr": "Order_Date",
						"default": ""
					}),
					"format": "d-m-Y"
				})
			}),
			"value4": functions.setComponentValue({
				"component": "lblRemarks_data",
				"value": functions.getVarAttr({
					"var": "vOrderEntryResult",
					"attr": "Remarks",
					"default": ""
				})
			}),
			"value5": functions.setComponentValue({
				"component": "lblOrderNo_data",
				"value": functions.getVarAttr({
					"var": "vOrderEntryResult",
					"attr": "No",
					"default": ""
				})
			})
		});
		functions.userDefined('globalModalLoading', {});
		functions.console({
			"value": functions.getVar({
				"var": "vCurrCustomerDetails"
			})
		});
		await functions.loadData({
			"dataset": "l_OrderLines",
			"filter": functions.toArray({
				"value1": functions.toObject({
					"f": "Type",
					"v": "Item"
				}),
				"value2": "",
				"value3": ""
			}),
			"callback": async (data, extra) => {
				await functions.loadData({
					"errCallback": async (data, extra) => {
						functions.setVar({
							"var": "error",
							"value": data["err"]
						});
						functions.console({
							"value": functions.concat({
								"string1": "l_OrderEmptyLines",
								"string2": functions.getVar({
									"var": "error"
								})
							})
						});
					},
					"dataset": "l_OrderEmptyLines",
					"filter": functions.toArray({
						"value1": functions.toObject({
							"v": "G_L_Account",
							"f": "Type"
						}),
						"value2": "",
						"value3": ""
					}),
					"callback": async (data, extra) => {
						await functions.loadData({
							"callback": async (data, extra) => {
								functions.setVar({
									"var": "vGstTotalRate",
									"value": functions.objectAttr({
										"attr": "VAT_Percent",
										"object": functions.objectAttr({
											"object": data,
											"attr": 0
										})
									})
								});
								functions.setComponentValue({
									"component": "lblGST",
									"value": functions.concat({
										"string1": "GST",
										"string2": " ",
										"string3": functions.formatNumber({
											"decimalSep": ".",
											"value": functions.getVar({
												"var": "vGstTotalRate"
											}),
											"decimals": 2
										}),
										"string4": "%"
									})
								});
								functions.conditional({
									"condition": functions.getVarAttr({
										"var": "vOrderEntryResult",
										"attr": "Remarks"
									}),
									"yesCallback": async (data, extra) => {
										functions.showElement({});
									},
									"noCallback": async (data, extra) => {
										functions.hideElement({
											"component": "pnlRemarks"
										});
									}
								});
								functions.setVar({
									"var": "vFirstOrderLine",
									"value": await functions.selectBy({
										"dataset": "l_OrderLines",
										"by": "Document_No",
										"value": functions.getVar({
											"var": "vOrderNo"
										}),
										"first": true,
										"callback": null,
										"errCallback": null
									})
								});
								functions.toArray({
									"value1": functions.setComponentValue({
										"component": "lblSubTotal_data",
										"value": functions.concat({
											"string1": "$ ",
											"string2": functions.formatNumber({
												"value": functions.getVarAttr({
													"var": "vFirstOrderLine",
													"attr": "Total_Amount_Excl_VAT"
												}),
												"decimals": 2,
												"decimalSep": ".",
												"thousandSep": ","
											})
										})
									}),
									"value2": functions.setComponentValue({
										"component": "lblGST_data",
										"value": functions.concat({
											"string1": "$ ",
											"string2": functions.formatNumber({
												"value": functions.getVarAttr({
													"var": "vFirstOrderLine",
													"attr": "Total_VAT_Amount"
												}),
												"decimals": 2,
												"decimalSep": ".",
												"thousandSep": ","
											})
										})
									}),
									"value3": functions.setComponentValue({
										"component": "lblTotalAmount_data",
										"value": functions.concat({
											"string1": "$ ",
											"string2": functions.formatNumber({
												"value": functions.getVarAttr({
													"var": "vFirstOrderLine",
													"attr": "Total_Amount_Incl_VAT"
												}),
												"decimals": 2,
												"decimalSep": ".",
												"thousandSep": ","
											})
										})
									})
								});
								functions.userDefined('globalModalHide', {});
							},
							"errCallback": async (data, extra) => {
								functions.setVar({
									"var": "error",
									"value": data["err"]
								});
								functions.console({
									"value": functions.concat({
										"string1": "MSC_GST_Setup",
										"string2": functions.getVar({
											"var": "error"
										})
									})
								});
							},
							"dataset": "OK365_GST_Setup",
							"filter": functions.toArray({
								"value1": functions.toObject({
									"f": "VAT_Prod_Posting_Group",
									"v": functions.getVarAttr({
										"var": "vItemDetails",
										"attr": "VAT_Prod_Posting_Group"
									})
								}),
								"value2": functions.toObject({
									"f": "VAT_Bus_Posting_Group",
									"v": functions.getVarAttr({
										"var": "vCurrCustomerDetails",
										"attr": "VAT_Bus_Posting_Group"
									})
								})
							})
						});
					}
				});
			},
			"errCallback": async (data, extra) => {
				functions.setVar({
					"var": "error",
					"value": data["err"]
				});
				functions.console({
					"value": functions.concat({
						"string1": "l_OrderLines",
						"string2": functions.getVar({
							"var": "error"
						})
					})
				});
			}
		});
	}
};

const PgSummary = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;
	formatParentStyleLayout(props);

	useEffect(() => {
		actions.handlePgSummaryLoad();
	}, [currentTime]);
    
    return (
		<Page data={props.pgSummary}>
			<Panel data={props.pnlHolder}>
				<Panel data={props.pnlMain}>
					<Panel data={props.pnlMainHeader}>
						<Panel data={props.pnlMainHeaderLeft}>
							<Panel data={props.pnlMainHeaderLeftTable}>
								<Panel data={props.pnlMainHeaderLeftCell}>
									<Image data={props.imgBack} action={actions.handleImgBackClick} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderMiddle}>
							<Panel data={props.pnlMainHeaderMiddleTable}>
								<Panel data={props.pnlMainHeaderMiddleCell}>
									<Label data={props.lblMainTitle} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderRight}>
							<Panel data={props.pnlMainHeaderRightTable}>
								<Panel data={props.pnlMainHeaderRightCell} />
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBody}>
						<Panel data={props.pnlBodyContentHeader}>
							<Label data={props.lblOrderNo_data} />
							<Label data={props.lblCodeName_data} />
							<Label data={props.lblAddress_data} />
							<Label data={props.lblOrderDate_data} />
						</Panel>
						<Panel data={props.Panel916}>
							<Panel data={props.Panel929}>
								<Panel data={props.Panel979}>
									<Label data={props.lblSubTotal} />
									<Label data={props.lblSubTotal_data} />
								</Panel>
								<Panel data={props.Panel979909}>
									<Label data={props.lblGST} />
									<Label data={props.lblGST_data} />
								</Panel>
								<Panel data={props.Panel979155}>
									<Label data={props.lblTotalAmount} />
									<Label data={props.lblTotalAmount_data} />
								</Panel>
							</Panel>
							<Panel data={props.pnlRemarks}>
								<Label data={props.lblRemarks} />
								<Label data={props.lblRemarks_data} />
							</Panel>
						</Panel>
						<Panel data={props.pnlContentMainLines}>
							<Panel data={props.Panel99}>
								<Label data={props.lblItems} />
							</Panel>
							<DataList data={props.DataList760} currentTime={currentTime} functions={functions}>
								{({ item, parentStyle }) => (							
									<Panel data={item.Panel132} parentStyle={parentStyle}>
										<Panel data={item.Panel846} parentStyle={parentStyle}>
											<Panel data={item.Panel481} parentStyle={parentStyle}>
												<Image data={item.Image159} parentStyle={parentStyle} />
											</Panel>
											<Panel data={item.Panel481444} parentStyle={parentStyle}>
												<Panel data={item.Panel397} parentStyle={parentStyle}>
													<Label data={item.Label379} parentStyle={parentStyle} />
													<Label data={item.Label161} parentStyle={parentStyle} />
													<Label data={item.Label379297} parentStyle={parentStyle} />
													<Label data={item.Label379774} parentStyle={parentStyle} />
												</Panel>
											</Panel>
										</Panel>
									</Panel>
								)}
							</DataList>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainFooter}>
						<Panel data={props.pnlMainFooterTable}>
							<Panel data={props.pnlMainFooterTableCell} action={actions.handlePnlMainFooterTableCellClick}>
								<Label data={props.lblBtnConfirm} />
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal}>
					<Panel data={props.pnlModalTable}>
						<Panel data={props.pnlModalCell}>
							<Panel data={props.pnlModalBodyMain}>
								<Panel data={props.pnlModalBodyTitle}>
									<Label data={props.lblModalTitle} />
								</Panel>
								<Panel data={props.pnlModalBodyMessage}>
									<Label data={props.lblModalMessage} />
								</Panel>
								<Panel data={props.pnlModalBodyLoading}>
									<Image data={props.imgModalLoading} />
								</Panel>
							</Panel>
							<Panel data={props.pnlModalBodyButtons}>
								<Panel data={props.pnlModalBodyButtonPositive} action={actions.handlePnlModalBodyButtonPositiveClick}>
									<Label data={props.lblModalPositive} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonNegative} action={actions.handlePnlModalBodyButtonNegativeClick}>
									<Label data={props.lblModalNegative} action={actions.handleLblModalNegativeClick} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlCSS}>
					<Label data={props.lblStyle174} />
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgSummary);