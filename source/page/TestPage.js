import React from 'react';
import { connect } from 'react-redux';

import Page from '../framework/component/Page';
import Label from '../framework/component/Label';
import Memo from '../framework/component/Memo';
import Edit from '../framework/component/Edit';
import Image from '../framework/component/Image';
import Button from '../framework/component/Button';
import CheckBox from '../framework/component/CheckBox';
import ComboBox from '../framework/component/ComboBox';
import Panel from '../framework/component/Panel';
import Signature from '../framework/component/Signature';

import { StateName } from '../reducer/data/TestPage';


import Functions from '../framework/core/Function';
let functions = new Functions(StateName);

const actions = {

    gotoFlatListEndReachedPage: () => {

        functions.gotoPage({
			"p": "FlatListEndReachedPage"
		});
       
    },


}

const TestPage = (props) => {



    // Append the dispatch to the function instance
    functions.pageProps = props;

    return (
        <Page data={props.TestPage}>
            <Button data={props.btnTestFlatListEndReachedPage} action={actions.gotoFlatListEndReachedPage}/>
            <Label data={props.lblName}/>
            <Label data={props.lblName2}/>
            <Edit data={props.txtEdit}/>
            <Memo data={props.memDesc}/>
            <Image data={props.imgTest}/>
            <Button data={props.btnTest}/>
            <CheckBox data={props.cbTest}/>
            <ComboBox data={props.cmbTest}/>
            <Panel data={props.pnlTest}>
                <Button data={props.btnPanelTest}/>
            </Panel>
            <Signature data={props.sigTest}/>
        </Page>
    );
};

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] }
};

export default connect(mapStateToProps)(TestPage);