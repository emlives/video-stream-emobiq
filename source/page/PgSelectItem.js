import React, { useEffect }  from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgSelectItem';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import Edit from '../framework/component/Edit';
import DataList from '../framework/component/DataList';
import ComboBox from '../framework/component/ComboBox';
import Button from '../framework/component/Button';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handlePnlMainHeaderLeftTableClick: async () => {
		functions.gotoPage({
			"p": "pgSalesReturnDetailsNew"
		});
	},

	handlePnlProductBodyRightClick: async () => {
		functions.setVar({
			"var": "vItemDetails",
			"value": data
		});
		functions.userDefined('globalModalLoading', {});
		await functions.loadData({
			"dataset": "OK365_Item_Unit_of_Measure",
			"filter": functions.toArray({
				"value1": functions.toObject({
					"f": "Item_No",
					"v": functions.getVarAttr({
						"var": "vItemDetails",
						"attr": "No"
					})
				}),
				"value2": ""
			}),
			"callback": async (data, extra) => {
				functions.setVar({
					"var": "vUOM",
					"value": data
				});
				functions.setComboOptions({
					"combo": "cmbUpdateSalesReturnLinesUOM",
					"data": data,
					"valueField": "Code",
					"displayField": "Code"
				});
				functions.setComponentValue({
					"component": "cmbUpdateSalesReturnLinesUOM",
					"value": functions.getVarAttr({
						"var": "vItemDetails",
						"attr": "Base_Unit_of_Measure"
					})
				});
				await functions.navCall({
					"function": "fnGetItemSalesPrice",
					"subfunction": "",
					"data": functions.toObject({
						"currencyCode": functions.getVarAttr({
							"var": "vCurrCustomer",
							"attr": "Currency_Code",
							"default": ""
						}),
						"itemVariantCode": functions.getVarAttr({
							"var": "vItemDetails",
							"attr": "itemVariantCode",
							"default": ""
						}),
						"itemNo": functions.getVarAttr({
							"var": "vItemDetails",
							"attr": "No"
						}),
						"customerNo": functions.conditional({
							"condition": functions.getVarAttr({
								"var": "vCurrCustomer",
								"attr": "Customer_Code"
							}),
							"yesValue": functions.getVarAttr({
								"var": "vCurrCustomer",
								"attr": "Customer_Code"
							}),
							"noValue": functions.getVarAttr({
								"var": "vCurrCustomer",
								"attr": "No"
							}),
							"yesCallback": "",
							"noCallback": ""
						}),
						"orderDate": functions.dbDate({}),
						"uOM": functions.componentValue({
							"component": "cmbUpdateSalesReturnLinesUOM"
						})
					}),
					"callback": async (data, extra) => {
						functions.console({
							"value": functions.objectAttr({
								"object": data,
								"attr": "return_value"
							})
						});
						functions.console({
							"value": data
						});
						functions.setVar({
							"var": "var_itemSalesPriceSelected",
							"value": functions.objectAttr({
								"attr": "return_value",
								"object": data
							})
						});
						functions.console({
							"value": "Track 1"
						});
						await functions.loadData({
							"filter": functions.toArray({
								"value1": functions.toObject({
									"f": "No",
									"v": functions.getVarAttr({
										"var": "vItemDetails",
										"attr": "No"
									})
								})
							}),
							"callback": async (data, extra) => {
								functions.console({
									"value": "Track 2"
								});
								functions.setVar({
									"var": "vCurrCartItem",
									"value": data
								});
								functions.conditional({
									"condition": functions.greater({
										"value1": functions.count({
											"values": data
										}),
										"value2": 0
									}),
									"yesCallback": async (data, extra) => {
										functions.console({
											"value": "Track 3"
										});
										functions.console({
											"value": "yes"
										});
										functions.toArray({
											"value1": functions.setComponentValue({
												"component": "imgUpdateCartImage",
												"value": ""
											}),
											"value2": functions.setComponentValue({
												"component": "lblUpdateCartItemCodeValueIL",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "No"
												})
											}),
											"value3": functions.setComponentValue({
												"component": "lblUpdateCartItemNameValueIL",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "Description"
												})
											}),
											"value4": functions.setComponentValue({
												"component": "lblUpdateCartPriceValueIL",
												"value": functions.concat({
													"string1": "$ ",
													"string2": functions.formatNumber({
														"thousandSep": ",",
														"value": functions.getVar({
															"var": "var_itemSalesPriceSelected"
														}),
														"decimals": 2,
														"decimalSep": "."
													})
												})
											}),
											"value5": functions.setComponentValue({
												"component": "txtQuantity",
												"value": functions.objectAttr({
													"object": functions.getVarAttr({
														"var": "vCurrCartItem",
														"attr": 0
													}),
													"attr": "Quantity"
												})
											}),
											"value6": null,
											"value7": "",
											"value8": functions.setComponentValue({
												"component": "lblBtnAdd",
												"value": "UPDATE"
											}),
											"value9": functions.setComponentValue({
												"component": "imgUpdateSalesReturnLinesItemImage",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "GetPicture"
												})
											}),
											"value10": functions.showElement({
												"component": "pnlUpdateSalesReturnLines"
											})
										});
									},
									"noCallback": async (data, extra) => {
										functions.console({
											"value": "Track 4"
										});
										functions.console({
											"value": "no"
										});
										functions.toArray({
											"value1": functions.setComponentValue({
												"component": "imgUpdateCartImage",
												"value": ""
											}),
											"value2": functions.setComponentValue({
												"component": "lblUpdateCartItemCodeValueIL",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "No"
												})
											}),
											"value3": functions.setComponentValue({
												"component": "lblUpdateCartItemNameValueIL",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "Description"
												})
											}),
											"value4": functions.setComponentValue({
												"component": "lblUpdateCartPriceValueIL",
												"value": functions.concat({
													"string1": "$ ",
													"string2": functions.formatNumber({
														"value": functions.getVar({
															"var": "var_itemSalesPriceSelected"
														}),
														"decimals": 2,
														"decimalSep": ".",
														"thousandSep": ","
													}),
													"string3": ""
												})
											}),
											"value5": functions.setComponentValue({
												"component": "txtQuantity",
												"value": 0
											}),
											"value6": null,
											"value7": "",
											"value8": functions.setComponentValue({
												"component": "lblBtnAdd",
												"value": "ADD"
											}),
											"value9": functions.setComponentValue({
												"component": "imgUpdateSalesReturnLinesItemImage",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "GetPicture"
												})
											}),
											"value10": functions.showElement({
												"component": "pnlUpdateSalesReturnLines"
											})
										});
									}
								});
								functions.userDefined('globalModalHide', {});
							},
							"errCallback": async (data, extra) => {
								functions.console({
									"value": "Track 5"
								});
							},
							"dataset": "l_ Sales_Return_Lines"
						});
					},
					"errCallback": async (data, extra) => {
						functions.userDefined('globalModalHide', {});
						functions.setVar({
							"var": "vAction"
						});
						functions.userDefined('globalModalInfo', {
							"title": "Internal Navision Server Error",
							"message": functions.objectAttr({
								"object": functions.objectAttr({
									"object": data,
									"attr": "err"
								}),
								"attr": "global"
							}),
							"btnCaption": "OK"
						});
					},
					"connector": "nav",
					"type": "",
					"ent": "OK365_GetItemSalesPrice"
				});
			},
			"errCallback": async (data, extra) => {
				functions.userDefined('globalModalHide', {});
				functions.setVar({
					"var": "error",
					"value": data["err"]
				});
			}
		});
	},

	handlePnlFooterBtnClick: async () => {
		functions.gotoPage({
			"p": "pgSalesReturnDetailsNew"
		});
	},

	handleImgUpdateSalesReturnLinesCloseClick: async () => {
		functions.setComponentValue({
			"component": "txtQuantity",
			"value": ""
		});
		functions.hideElement({
			"component": "pnlUpdateSalesReturnLines"
		});
	},

	handleImgMinusClick: async () => {
		functions.conditional({
			"condition": functions.greater({
				"value1": null,
				"value2": 1
			}),
			"yesCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"value": functions.sub({
						"value1": null,
						"value2": 1
					})
				});
			},
			"noCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"value": 0
				});
			}
		});
	},

	handleTxtQuantityClick: async () => {
		functions.setComponentFocus({
			"component": "txtQuantity",
			"selectAllText": "true"
		});
	},

	handleImgPlusClick: async () => {
		functions.conditional({
			"condition": functions.greater({
				"value1": null,
				"value2": -1
			}),
			"yesCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"value": functions.add({
						"value1": null,
						"value2": 1
					})
				});
			},
			"noCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"value": 0
				});
			}
		});
	},

	handlePnlUpdateSalesReturnLinesFooterBtnClick: async () => {
		functions.hideElement({
			"component": "pnlUpdateSalesReturnLines"
		});
		functions.userDefined('globalModalLoading', {});
		functions.conditional({
			"condition": null,
			"yesCallback": async (data, extra) => {
				functions.conditional({
					"condition": functions.greater({
						"value2": functions.toFloat({
							"value": -1
						}),
						"value1": null
					}),
					"yesCallback": async (data, extra) => {
						functions.conditional({
							"condition": functions.greater({
								"value2": 0,
								"value1": null
							}),
							"yesCallback": async (data, extra) => {
								functions.setVar({
									"var": "v_current_item_uom",
									"value": functions.componentValue({
										"component": "cmbUpdateSalesReturnLinesUOM"
									})
								});
								functions.console({
									"value": "Goes here Yes"
								});
								await functions.loadData({
									"limit": 1,
									"filter": functions.toArray({
										"value1": functions.toObject({
											"f": "No",
											"o": "=",
											"v": functions.getVarAttr({
												"var": "vItemDetails",
												"attr": "No"
											})
										}),
										"value2": functions.toObject({
											"f": "Unit_of_Measure_Code",
											"o": "=",
											"v": functions.componentValue({
												"component": "cmbUpdateSalesReturnLinesUOM"
											})
										})
									}),
									"callback": async (data, extra) => {
										functions.console({
											"value": "check 1"
										});
										functions.setVar({
											"var": "vCurrCartItem",
											"value": data
										});
										functions.console({
											"value": data
										});
										functions.conditional({
											"condition": functions.greater({
												"value2": 0,
												"value1": functions.count({
													"values": data
												})
											}),
											"yesCallback": async (data, extra) => {
												functions.console({
													"value": functions.toArray({
														"value1": functions.toObject({
															"v": functions.objectAttr({
																"object": functions.getVarAttr({
																	"var": "vCurrCartItem",
																	"attr": 0
																}),
																"attr": "ItemCode"
															}),
															"f": "No",
															"o": "="
														}),
														"value2": functions.toObject({
															"f": "Unit_of_Measure_Code",
															"o": "=",
															"v": functions.componentValue({
																"component": "cmbUpdateSalesReturnLinesUOM"
															})
														})
													})
												});
												functions.console({
													"value": "check 2"
												});
												functions.console({
													"value": functions.toObject({
														"Price": functions.getVar({
															"var": "var_itemSalesPriceSelected",
															"default": ""
														}),
														"Quantity": null
													})
												});
												await functions.updateBy({
													"errCallback": null,
													"dataset": "l_Item_List",
													"by": "No",
													"operator": "=",
													"value": functions.getVarAttr({
														"var": "vItemDetails",
														"attr": "No"
													}),
													"data": functions.toObject({
														"qty_sales_return": null
													}),
													"callback": ""
												});
												await functions.loadData({
													"errCallback": "",
													"dataset": "l_Item_List",
													"callback": async (data, extra) => {
														functions.userDefined('globalModalHide', {});
													}
												});
												functions.console({
													"value": "a"
												});
											},
											"noCallback": async (data, extra) => {
												await functions.insert({
													"callback": "",
													"errCallback": "",
													"dataset": "l_ Sales_Return_Lines",
													"dt": functions.toObject({
														"Quantity": null,
														"GetPicture": functions.getVarAttr({
															"var": "vItemDetails",
															"attr": "GetPicture"
														}),
														"No": functions.getVarAttr({
															"var": "vItemDetails",
															"attr": "No"
														}),
														"Description": functions.getVarAttr({
															"var": "vItemDetails",
															"attr": "Description"
														}),
														"Unit_Price": functions.getVar({
															"var": "var_itemSalesPriceSelected",
															"default": ""
														}),
														"Unit_of_Measure_Code": null
													})
												});
												await functions.loadData({
													"dataset": "l_ Sales_Return_Lines",
													"callback": async (data, extra) => {
														await functions.updateBy({
															"value": functions.getVarAttr({
																"var": "vItemDetails",
																"attr": "No"
															}),
															"data": functions.toObject({
																"qty_sales_return": null
															}),
															"callback": null,
															"errCallback": null,
															"dataset": "l_Item_List",
															"by": "No",
															"operator": "="
														});
														await functions.loadData({
															"callback": async (data, extra) => {
																functions.userDefined('globalModalHide', {});
															},
															"errCallback": null,
															"dataset": "l_Item_List"
														});
													},
													"errCallback": null
												});
												functions.console({
													"value": "b"
												});
											}
										});
									},
									"errCallback": "",
									"dataset": "l_ Sales_Return_Lines"
								});
							},
							"noCallback": async (data, extra) => {
								functions.console({
									"value": "Goes here No"
								});
								functions.userDefined('globalModalHide', {});
								functions.userDefined('globalModalInfo', {
									"title": "Invalid Value",
									"message": "Quantity must be greater than 0."
								});
							}
						});
					},
					"noCallback": async (data, extra) => {
						functions.userDefined('globalModalHide', {});
						functions.userDefined('globalModalInfo', {
							"title": "Invalid Value",
							"message": "Please enter a valid value/amount."
						});
					}
				});
			},
			"noCallback": async (data, extra) => {
				functions.userDefined('globalModalHide', {});
				functions.userDefined('globalModalInfo', {
					"title": "No Value",
					"message": "Enter the quantity."
				});
			}
		});
	},

	handleBtnModalPositiveClick: async () => {
		functions.conditional({
			"condition": functions.equal({
				"value2": "exit_application",
				"value1": functions.getVar({
					"var": "action"
				})
			}),
			"yesCallback": async (data, extra) => {
				functions.setVar({
					"var": "action"
				});
				functions.exitApp({});
			},
			"noCallback": ""
		});
		functions.userDefined('globalModalHide', {});
	},

	handleBtnModalNegativeClick: async () => {
		functions.setVar({
			"var": "action"
		});
		functions.toArray({
			"value1": functions.setComponentValue({
				"component": "lblModalTitle",
				"value": "Loading"
			}),
			"value2": functions.hideElement({
				"component": "pnlModalBodyTitle"
			}),
			"value3": functions.setComponentValue({
				"component": "lblModalMessage",
				"value": "Loading ..."
			}),
			"value4": functions.hideElement({
				"component": "pnlModalBodyMessage"
			}),
			"value5": functions.hideElement({
				"component": "pnlModalBodyLoading"
			}),
			"value6": null,
			"value7": functions.hideElement({
				"component": "pnlModalBodyButtonPositive"
			}),
			"value8": null,
			"value9": functions.hideElement({
				"component": "pnlModalBodyButtonNegative"
			}),
			"value10": functions.toArray({
				"value1": functions.setObjectAttr({
					"object": null,
					"attr": "word-wrap",
					"value": "break-word"
				}),
				"value2": functions.setObjectAttr({
					"object": null,
					"attr": "display",
					"value": "none"
				})
			})
		});
	},

	handlePgSelectItemLoad: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalLoading', {});
		functions.onBackButton({
			"callback": async (data, extra) => {
				functions.gotoPage({
					"p": "pgMainMenu"
				});
			}
		});
		functions.setTimeout({
			"timeout": 100,
			"extra": "",
			"callback": async (data, extra) => {
				await functions.clearData({
					"dataset": "l_Item_List",
					"callback": "",
					"errCallback": null
				});
				await functions.loadData({
					"errCallback": async (data, extra) => {
						functions.setVar({
							"var": "error",
							"value": data["err"]
						});
						functions.userDefined('globalModalInfo', {
							"title": "Error",
							"message": functions.conditional({
								"condition": functions.getVarAttr({
									"var": "error",
									"attr": "global"
								}),
								"yesValue": functions.getVarAttr({
									"var": "error",
									"attr": "global"
								}),
								"noValue": "Cannot connect to Navision Server."
							})
						});
						functions.console({
							"value": data
						});
					},
					"dataset": "OK365_Item_List",
					"limit": 10,
					"filter": functions.toArray({
						"value1": functions.toObject({
							"f": "Location_Filter",
							"v": functions.objectAttr({
								"object": functions.getVarAttr({
									"var": "var_loginCnt",
									"attr": 0,
									"default": ""
								}),
								"attr": "Location_Code"
							})
						}),
						"value2": "",
						"value3": "",
						"value4": "",
						"value5": "",
						"value6": "",
						"value7": "",
						"value8": "",
						"value9": "",
						"value10": ""
					}),
					"order": "",
					"callback": async (data, extra) => {
						functions.setVar({
							"var": "vTotalCount",
							"value": functions.count({
								"values": data
							})
						});
						functions.setVar({
							"var": "vLoopCount",
							"value": 0
						});
						functions.map({
							"values": data,
							"extra": "",
							"callback": async (data, extra) => {
								functions.setVar({
									"var": "vLoopCount",
									"value": functions.add({
										"value1": functions.getVar({
											"var": "vLoopCount",
											"default": ""
										}),
										"value2": 1
									})
								});
								functions.conditional({
									"condition": functions.equal({
										"value1": functions.count({
											"values": await functions.selectBy({
												"by": "No",
												"operator": "=",
												"value": data["No"],
												"callback": null,
												"errCallback": null,
												"dataset": "l_Item_List"
											})
										}),
										"value2": 0
									}),
									"yesValue": "",
									"noValue": "",
									"extra": data,
									"yesCallback": async (data, extra) => {
										await functions.insert({
											"dataset": "l_Item_List",
											"dt": extra,
											"callback": async (data, extra) => {
												functions.conditional({
													"condition": functions.equal({
														"value2": functions.getVar({
															"var": "vLoopCount",
															"default": ""
														}),
														"value1": functions.getVar({
															"var": "vTotalCount",
															"default": ""
														})
													}),
													"yesValue": "",
													"noValue": "",
													"extra": "",
													"yesCallback": async (data, extra) => {
														await functions.loadData({
															"dataset": "l_Item_List",
															"callback": async (data, extra) => {
																functions.userDefined('globalModalHide', {});
															},
															"errCallback": null
														});
													},
													"noCallback": ""
												});
											},
											"errCallback": ""
										});
									},
									"noCallback": async (data, extra) => {
										functions.conditional({
											"condition": functions.equal({
												"value1": functions.getVar({
													"var": "vTotalCount",
													"default": ""
												}),
												"value2": functions.getVar({
													"var": "vLoopCount",
													"default": ""
												})
											}),
											"yesValue": "",
											"noValue": "",
											"extra": "",
											"yesCallback": async (data, extra) => {
												await functions.loadData({});
											},
											"noCallback": ""
										});
									}
								});
							}
						});
						functions.conditional({
							"condition": functions.equal({
								"value1": functions.count({
									"values": data
								}),
								"value2": 0
							}),
							"yesValue": "",
							"noValue": "",
							"extra": "",
							"yesCallback": async (data, extra) => {
								await functions.loadData({});
							},
							"noCallback": ""
						});
					}
				});
			}
		});
	}
};

const PgSelectItem = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;
	formatParentStyleLayout(props);

	useEffect(() => {
		actions.handlePgSelectItemLoad();
	}, [currentTime]);
    
    return (
		<Page data={props.pgSelectItem}>
			<Panel data={props.pnlHolder}>
				<Panel data={props.pnlMain}>
					<Panel data={props.pnlMainHeader}>
						<Panel data={props.pnlMainHeaderLeft}>
							<Panel data={props.pnlMainHeaderLeftTable} action={actions.handlePnlMainHeaderLeftTableClick}>
								<Panel data={props.pnlMainHeaderLeftCell}>
									<Image data={props.imgBack} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderMiddle}>
							<Panel data={props.pnlMainHeaderMiddleTable}>
								<Panel data={props.pnlMainHeaderMiddleCell}>
									<Label data={props.lblMainTitle} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderRight}>
							<Panel data={props.pnlMainHeaderRightTable} />
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBody}>
						<Panel data={props.pnlMainBodySearch}>
							<Panel data={props.pnlSearch}>
								<Edit data={props.txtSearch} />
							</Panel>
						</Panel>
						<Panel data={props.pnlMainBodyContent}>
							<DataList data={props.dtItem_List} functions={functions} currentTime={currentTime}>
								{({ item, parentStyle }) => (							
									<Panel data={item.pnlProduct} parentStyle={parentStyle}>
										<Panel data={item.pnlProductBody} parentStyle={parentStyle}>
											<Panel data={item.pnlProductBodyLeft} parentStyle={parentStyle}>
												<Image data={item.imgItem} parentStyle={parentStyle} />
											</Panel>
											<Panel data={item.pnlProductBodyMiddle} parentStyle={parentStyle}>
												<Panel data={item.pnlProductBodyMiddle1} parentStyle={parentStyle}>
													<Label data={item.lblItemCode} parentStyle={parentStyle} />
													<Label data={item.lblItemName} parentStyle={parentStyle} />
													<Label data={item.lblStockOnHand} parentStyle={parentStyle} />
													<Label data={item.lblQtyAdded} parentStyle={parentStyle} />
												</Panel>
											</Panel>
											<Panel data={item.pnlProductBodyRight} action={actions.handlePnlProductBodyRightClick} parentStyle={parentStyle}>
												<Image data={item.imgAddItem} parentStyle={parentStyle} />
											</Panel>
										</Panel>
									</Panel>
								)}
							</DataList>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainFooter}>
						<Panel data={props.pnlFooterBtn} action={actions.handlePnlFooterBtnClick}>
							<Label data={props.lblBtnOK} />
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlUpdateSalesReturnLines}>
					<Panel data={props.pnlUpdateSalesReturnLinesTable}>
						<Panel data={props.pnlUpdateSalesReturnLinesTableCell}>
							<Panel data={props.pnlUpdateSalesReturnLinesContent}>
								<Image data={props.imgUpdateSalesReturnLinesClose} action={actions.handleImgUpdateSalesReturnLinesCloseClick} />
								<Panel data={props.Panel45926}>
									<Panel data={props.pnlUpdateSalesReturnLinesContent1}>
										<Image data={props.imgUpdateSalesReturnLinesItemImage} />
									</Panel>
									<Panel data={props.pnlUpdateSalesReturnLinesContent2}>
										<Label data={props.lblUpdateSalesReturnLinesItemCode_data} />
									</Panel>
									<Panel data={props.pnlUpdateSalesReturnLinesContent3}>
										<Label data={props.lblUpdateSalesReturnLinesItemName_data} />
									</Panel>
									<Panel data={props.pnlUpdateSalesReturnLinesContent4}>
										<Label data={props.lblUpdateSalesReturnLinesUnitPrice_data} />
									</Panel>
								</Panel>
								<Panel data={props.pnlUpdateSalesReturnLinesQty}>
									<Panel data={props.pnlUpdateSalesReturnLinesQtyContent1}>
										<Panel data={props.pnlUpdateSalesReturnLinesQtyContent1Table}>
											<Panel data={props.pnlUpdateSalesReturnLinesQtyContent1TableCell}>
												<Panel data={props.pnlUpdateSalesReturnLinesUOM}>
													<Panel data={props.pnlUpdateSalesReturnLinesUOMTable}>
														<Panel data={props.pnlUpdateSalesReturnLinesUOMTableCell}>
															<Label data={props.lblUpdateSalesReturnLinesUOM} />
														</Panel>
													</Panel>
												</Panel>
												<ComboBox data={props.cmbUpdateSalesReturnLinesUOM} functions={functions} />
											</Panel>
										</Panel>
									</Panel>
									<Panel data={props.pnlUpdateSalesReturnLinesQtyContent2}>
										<Panel data={props.pnlUpdateSalesReturnLinesQtyControl}>
											<Panel data={props.pnlUpdateSalesReturnLinesQtyCtrlTableCell1}>
												<Image data={props.imgMinus} action={actions.handleImgMinusClick} />
											</Panel>
											<Panel data={props.pnlUpdateSalesReturnLinesQtyCtrlTableCell2}>
												<Edit data={props.txtQuantity} action={actions.handleTxtQuantityClick} />
											</Panel>
											<Panel data={props.pnlUpdateSalesReturnLinesQtyCtrlTableCell3}>
												<Image data={props.imgPlus} action={actions.handleImgPlusClick} />
											</Panel>
										</Panel>
									</Panel>
									<Panel data={props.pnlUpdateSalesReturnLinesQtyContent3}>
										<Label data={props.lblUpdateSalesReturnLinesQuantity} />
									</Panel>
								</Panel>
								<Panel data={props.UpdateSalesReturnLinesFooterMainBtn}>
									<Panel data={props.pnlUpdateSalesReturnLinesFooterBtn} action={actions.handlePnlUpdateSalesReturnLinesFooterBtnClick}>
										<Panel data={props.pnlUpdateSalesReturnLinesBtnAdd}>
											<Label data={props.lblUpdateSalesReturnLinesBtnAdd} />
										</Panel>
									</Panel>
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal}>
					<Panel data={props.pnlModalTable}>
						<Panel data={props.pnlModalCell}>
							<Panel data={props.pnlModalBody}>
								<Panel data={props.pnlModalBodyTitle}>
									<Label data={props.lblModalTitle} />
								</Panel>
								<Panel data={props.pnlModalBodyMessage}>
									<Label data={props.lblModalMessage} />
								</Panel>
								<Panel data={props.pnlModalBodyLoading}>
									<Image data={props.imgModalLoading} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonPositive}>
									<Button data={props.btnModalPositive} action={actions.handleBtnModalPositiveClick} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonNegative}>
									<Button data={props.btnModalNegative} action={actions.handleBtnModalNegativeClick} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlCSS}>
					<Label data={props.lblStyle} />
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgSelectItem);