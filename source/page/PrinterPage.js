import React, { useEffect, useState, useRef } from 'react';
import { StyleSheet, Text, FlatList, TouchableOpacity } from 'react-native';
import Canvas, { Image as CanvasImage } from 'react-native-canvas';
import { connect } from 'react-redux';

import Page from '../framework/component/Page';
import Button from '../framework/component/Button';
import ComboBox from '../framework/component/ComboBox';
import BuLabeltton from '../framework/component/Label';
import { StateName } from '../reducer/data/PrinterPage';

import { informationLog as log } from '../framework/core/Log';
import Functions from '../framework/core/Function';
import Label from '../framework/component/Label';

const PrinterPage = (props) => {
    let functions = new Functions(StateName);
    functions.pageProps = props;

    let canvasRef = useRef()
    const [devices, setDevices] = useState([])
    const [selectedDevice, setSelectedDevice] = useState(0)

    /*
     * The following code is to identify if device is connected, 
     * will need to spend more time on native code before uncommenting
     */

    /*
    const { BtPrinter } = NativeModules;
    const [printIsDisabled, setPrintIsDisabled] = useState(true)
    const btPrinterEventEmitter = new NativeEventEmitter(BtPrinter);

    const subscription = btPrinterEventEmitter.addListener(
        'BtPrinterConnectEvent',
        (connection) => {
            console.log(`In PrinterPage... ${connection.status}`)
            setPrintIsDisabled(false)
        }
    );
    */

    const Item = (props) => (
        <TouchableOpacity onPress={() => onConnectDevice(props.address)} style={styles.item}>
            <Text style={[styles.itemText,
            { color: selectedDevice == props.address ? 'tomato' : 'black' },
            { fontWeight: selectedDevice == props.address ? 'bold' : 'normal' }
            ]} >{props.title}</Text>
        </TouchableOpacity>
    );

    const renderItem = ({ item }) => (
        <Item title={item.name} address={item.address} />
    );

    // useEffect(() => {
    //     onSetupCanvas()
    // }, [devices])

    // useEffect(() => {
    //     log(`useEffect devices = ${devices}`)
    // }, [devices])

    useEffect(() => {

        functions.btPrinterPortList({
			"callback": (input, extra) => {

                console.log('success callback triggered with input', input)

				functions.setComboOptions({
					"combo": "cmbPrinter",
					"data": input,
					"valueField": "id",
					"displayField": "name"
				});
			},
			"errorCallback": (input, extra) => {
				console.log('error callback triggered.')
			}
		});

    },[])

    useEffect(() => {
        log(`useEffect selectedDevice = ${selectedDevice}`)
    }, [selectedDevice])

    const onConnectDevice = (deviceAddress) => {
        log(`connecting to ... ${deviceAddress}`);
        let param = {
            printerId: deviceAddress,
            extra: "some extra data",
            callback: function (extra) {
                setSelectedDevice(deviceAddress)
                log(`onConnectDevice success with deviceId: ${deviceAddress}`)
            },
            errorCallback: function (e) {
                log(`onConnectDevice error: ${e}`)
            },
        }
        functions.btPrinterConnect(param);
    }

    // const onScanForPeripherals = () => {
    //     let param = {
    //         callback: function (devices) {
    //             log(`onScanForPeripherals success with devices: ${devices}`)


    //             functions.setComboOptions({
	// 				"combo": "cmbPrinter",
	// 				"data": devices,
	// 				"valueField": "id",
	// 				"displayField": "name"
	// 			});


    //             setDevices(devices)

    //         },
    //         errorCallback: function (e) {
    //             log(`onScanForPeripherals error: ${e}`)
    //         },
    //     }
    //     functions.btPrinterPortList(param);
    // }

     const onScanForPeripherals = () => {}

    const onPrintCanvas = () => {
        const printCanvas = (cpclString) => {
            let printParam = {
                text: cpclString,
                callback: function (success) {
                    log('printCanvas success')
                },
                errorCallback: function (e) {
                    log(`printCanvas error: ${e}`)
                },
            }
            functions.btPrinterPrint(printParam);
        }

        let param = {
            canvas: canvasRef.current,
            singlePrint: true,
            blackMark: false,
            callback: printCanvas
        }
        functions.canvasToCPCL(param)
    }

    const onSetupCanvas = () => {
        handleCanvas(canvasRef.current)
    }

    const handleCanvas = (canvas) => {
        if (canvas !== null) {
            canvas.width = 573;
            canvas.height = 573;
            const context = canvas.getContext('2d');
            let image = new CanvasImage(canvas);
            image.crossOrigin = 'anonymous';
            image.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAIAAAD2HxkiAAAAA3NCSVQICAjb4U/gAAAgAElEQVR4nO2dfXBU5fXHz71337JhkxhCQhiCvAooUhHEVhSEAjrjC5apowRBRsCxUGvlRWyVKv4cyzBjiy+1Dlba6tiO09pKqcAk8mbFdyoqgqIWNUAgISEvZLN7d+99fn+c7unD3bzsJpvc3L3n80dms3v37rN3n+89z3POec6jVFRUAAAACCEURQGGYXoFRVGEEADgAYBZs2bhs6xDhullKisrVfl/VCCqk2GYHoWEptIjesDGkGF6ARKaSgNTesAwTG+igqRI1iHD9D7/nRPyWJRh7OK/ImTtMYxdqJ0fwjBMT8IiZBibYREyjM2wCBnGZliEDGMzLEKGsRkWIcPYDIuQYWyGRcgwNsMiZBibYREyjM2wCBnGZliEDGMzLEKGsRkWIcPYDIuQYWyGRcgwNsMiZBibYREyjM2wCBnGZliEDGMzLEKGsRkWIcPYDIuQYWyGRcgwNsMiZBibYREyjM2wCBnGZliEDGMzLEKGsRkWIcPYDIuQYWyGRcgwNsMiZBibYREyjM2wCBnGZliEDGMzLEKGsRkWIcPYDIuQYWyGRcgwNsMiZBibYREyjM2wCBnGZliEDGMzLEKGsRkWIcPYDIuQYWyGRch0ghDC7iZkOSxClxKPxwHANE3DMPABAAgh4vF4LBbDA0zTNE1TURQ8hukhPHY3gLETVVUhYetM01RV1ePxmKYphNA0jWygoih2tjLbYUvoUjweDwCQrYOEIFGKAKAoCj6gl5gegi+uS0Fzp6oqCoxMIg1T0QwKIXRdx8Eq00OwCN0LjUJJZoqi+Hw+ANA0LRKJ6LqOA9RoNGpzW7ManhNmOUIIHG1aHqiqiiIk4QFAc3NzJBKpqan5+uuvjxw5kp+ff+211w4aNCgnJ8fGr5D1sAizHCEE+VfkByhFpLW19cSJE++///6bb765bdu2o0eP4mFTpkyZOHHioEGDaKLI9AQswiwB/SvkxkSPCzo5VVXVdR3NHY4wVVXVNC0Wi9XW1h44cGDLli1//etfa2trAUDTNEhMEU+dOhWJROSTkzllMgiLMEto01KhYEzTpAGn1+vFJ0+ePFlZWfnSSy/t3r1b13U6CflgTNM8ceJEfX29EMLj8YgELMKMwyLMTuRZH1pCNHGapjU1NW3duvWPf/zj/v376+vrAcDr9QJALBazeEHD4XBLSwsKj/Nmeg4WYTaDUqSxKAAcOHBgw4YN27Zta2xsxGe8Xq9hGCg/TdPkBJrCwsJ+/fqhAuWwPpNZWITZCU3hotGo3+8HgJqamn/84x8bN248dOgQigqVhklqiGEY8oi0tLS0f//+Fucqk3FYhFmFbKnwsd/vb25urqmp2bx58/r161FgqDRZb22Sl5eXl5dHKTWQlL/GyswILMLsQY5GIIqimKa5f//+hx9+eO/evaqqer1emvvJCqRZH00jTdM877zzioqKaCBqccAC55RmCBZhNqMoyt69e1evXv3BBx/gfI8ifqgoSwjRYhv79+9fWFgIPBXsYViEjgcNoKqqOP1TFEXXdY/HoyjKG2+8MW/evLq6OgCg5Ugks2Rp0UuBQCAcDg8dOpRi+slmkMkUnAaRDaBIAoGAECISiaAvdM+ePUuXLm1qakp3NaCmaeFw2O/3T5gwIR6PYw53zzScAWBL6HRoHiiECIfDOTk5GFj/5JNPli9ffuTIETyMQvmprIdQVdUwjIsvvnjKlCmoZ5/PR6t+2RhmHBahs6H4gWEYwWAQnzx+/PiKFStIgQQpsOPgeywWUxRl5MiRRUVF5E0FdsP0GDwczRJ8Ph8OGsPh8DPPPLNr1y5LuCI5etExs2fPpsdkBtlD0xOwJcwSMEE0Go3u3LnzmWeeoefJVKZ+KlVVS0tLZ8+ebZomLcCXz8ZkFraE2YOu662trT/60Y8aGxtzc3Nl89WpeDDBjaIX5eXlpaWlFLEgH2kvfAsXwiLMEjAQv27dupqaGgBoaWmRX01dP3ieefPmYUQRa67hHJKLXPQQLEKHQUUKCQwSxmKxffv2vfTSS6Q3StqGDp2ieJjX69U0DVchLl26dPz48bjqgpY+kZ1kMg5fVueBBgoTr9FAmaapadqLL75YW1uLk0MM2XdqAHNzc3Vd9/v9sVgMT+jz+e644w5c8osJbjwK7WlYhA5DVVVFUTAhhp5RVfXdd9/93e9+h89ghB1SGIXSqFUIUVBQAAB33333xIkTTdP0er3olWF6Ghahw6Dca1rtjqPE5557zjTNUChEx3S6SALRNC0ajaqq2tDQMH78+GXLlgFAJBLBeSBmnPJAtEfhi+s8aIiIMXoAOHz4cEVFhaqqzc3Nfr+fnJydnopW8eLjX/ziF8OHD9d1Hcur4UuswJ6Gr6/DwOEoJovG43FcF79z586amhpUHaaYdawc1LCiKMFgEIMTpmmuXLnyxhtvNAwDp5TRaBRPznPCnoZF6DBisRhF3tGxefbs2XfeeQdzzTRNswQn2oQGsc3NzajAadOm3X777ZCwfvF4XC7c1kPfhUFYhA4DizJBYpQYjUa//fbbd999F5/EFYMYbGjz7WQDAUAI4ff7DcMoKCh46KGHLrzwQpwE4nDX5/OhFNkS9jTuEmF2pD5SIUPTNP1+/9tvv/3VV19BOwtzCaw0Q6XTKM0tFAr95je/mT59ejwe9/l85FZF3w+Ofnv167kPF4kwa/KPyZphjL62tlb+XhYFer1espkYeUcD6PV6sRDwc889V15ejuuAaZgKnCbai7hFhFmjQHldvBCiubn522+/pVeT07UxBI/SisViOTk5+fn5Qgh8/vnnn7/55psh4c4hM0jF2iCRD9BmY7LjktqOK0SYNQqERL8ntZw5c+bLL7+0vGoxYjRwBYBwONzY2Kjr+qRJk/7yl7/cdtttmFtDuzLhW+hBx9eNrWVGcEVKBPUk2ztNioksHWB5b3Nzc1VVFZw7FZSVQzHDaDSam5vb0tLi8/kmT568fv36KVOm4EuUIGo5f/JOMkxPkP2WUFagjSJMXlYrP0P2zfJ8MjQhxMPi8XhDQwO0f6NBM4iPW1paVFVdtWrVyy+/fMUVV4TDYTye6v/KhYBBWlAvG0a5zSzOjJDllpA6Sh9xtcsGh4SXVsMURTEMA/daQh8mKYeUKR9MrlQAuOGGG5YtW/b9738f9/3EchjojInH47iIydIe2tBCbi1Xmsks2SxCuUf2hU4j2xDZtlja1mlTMRJIYsA0axRk8nnwyUmTJi1evHj+/PmhUAjTuwOBAObcYH4MnYFcO5Yiv/RxJPWOC9UwqZPlIsQHHfT4FJETwfAkltQwOZdaSHviwrmagXNtMuVG48ASA/GWRqKNwsPQBsr2Si6jhoKkBYdCCFyeO3ny5Ouuu27o0KH0ReLxOEoXlyzhdoXyN8KT00dbio7K3xTfRc9gC/GvPAHGJuEaRcZC1ooweSpI9+8unM2Sikl9jjqubCLwGdoLCT+xsbGxpqamurq6vr4e1yh4vV4UTCgUKikpKSsrw5rzdJJIJKKqKp4EP4ikgsIGgGAwOHjw4NOnT+OTODQNhUJXX3319OnTx44de/XVV5umiSNPEgwF5QEAR7aQ2EFNVdX6+vrTp0+fOnXq9OnTsVgMI42GYRiG4fF4BgwYUFZWVlpaGgwG6bKgxjBvTiTWXuBjLEmMreUdf9skO0WYwamgrCt6BhI6tKxXINOHItF1/fPPP9+5c+cXX3xRVVVVXV39zTff1NXVUaZ1PB4HgLy8vOLi4gEDBgwYMGDIkCGXXnrphAkTxo0bFwgE8JzRaBQfR6NRNHdkgkpKSsrLyy+55BKv11taWjps2LCysrLi4uLi4uLCwkI0rdgewzCEEDR2lYeXuq4fPXr0ww8//Pzzzw8fPlxfX19XV1dVVYVNxe/o8Xgwi+28884rKSk5//zzR44cWVZWdtVVV02YMAG/C92P0NLi18TQCK4wppw7RkapqKiYNWuW3c3IJJapoGwGoasLc8j+0KgSB3UAgJKQ3RWmaR46dOjVV199/fXXq6qqTp48GYlESKidrjPSNG3s2LHFxcVTp06dM2fO+PHjsVujyZI3k8ARbFNTE35Bj8eTk5ODDhv8vjjctQwCaWSo6/pHH320ffv2d955p7a2trq6+vjx43RYctw/GUVRhg4dWlxcfN111916662jRo1CU4wDZhQtLfsAaXTAEJWVlVBRUSGyC7zrYyqz5Rm0BmmBb8FT4TkNw8DyR7iggV4VQkQikZ07dy5YsAD7HHW4Dqyx/FJ+fj7aDURV1YKCgqlTpz755JOfffZZNBqVP84wjEgkIjfS0hiyfnhAa2urrutCiKamplOnTv3617+eM2eOPGAGACof3HE7VVWlESa95PP5FixYsHv37nA4bLmAkUgkHA7jpzMWKioqslCEpLeMiFCGzhOPx1GHQghd1w3DiEaju3btmj9/Po24SIE4r+vAAtOsTO7llmc0Tbvpppv+8Ic/nDx5sr6+PvmLoETb/KbY+w3DqKur271796JFi+QPsmgMP7rNu4ZFe+01+Ac/+MGOHTvOnDlDDcMLFY1Gu3n9s5IsFOF/LZXZtgK73wlIhEKIhoYG7PrvvfdeeXk59kKs1AJJ4165m6KHU36JXJ3y4I2ekV0giqLMnTv3lVdeOXjwYF1d3dmzZ7ENdFMwTRPvC9jgaDRaVVX1xhtvPProoxMmTMCT4E2BztlmU5PvHUoCfJVilXRAKBTC+hqqqt59991vvPEGyY9GDYyFioqKrJoT/vdrAYACmqLCufNDpDveOarvgiEKr9dbU1PzwgsvbNiwoba2lgrRK4qSk5OD+SiqtCdu8iKj9pYdya8qiVovIimResaMGTNmzLjgggsGDBiQn58fCoVycnKwZkxTUxPazPfee2/Pnj0HDhygwjOWa2IJx6P7xLINkyUkKDcbdx31er1UXQoAgsFga2trfn7+mjVrlixZgntayBNahqisrHS8CIUUdbA4P6BLIhTnRhcjkQhuOUbdCAtAAMCePXs2bdr0yiuvYJdF42DJ/OoO1NdT8ZGUlZWVlJQUFRUJIerr69EhlHwq+modn637oJE0DOP6669fu3bt5MmTQfqx8DJikMblsswqx0zH/ph0h6OWs6GzIRaLkW/mqaeemjp1Kl7H9iZRvY9lemlj/5Y/evjw4Zs3b6apKf1ATU1NeKnRirqTioqKLIwTyokdqb9LnJtKhgAABs2x+hgG2err6zdu3PjUU09h5jSNQvsCluLcNpaHocQAIcR//vOf++67r7q6+ic/+Um/fv10XcdBbCgUOnv2bL9+/VyeSZMlIwEThAnnrBJKS4Fwrguehn/o5MA0SzxhXV3dz372s0cffbShocHr9fr9fhqLZuq7dBN0nNjdCgAAvGh+v9/n850+fXrt2rV33nnn8ePHMVsVS7kFg0G637mWbLCEQggQAjXUsQi7MGjUNC0SiaAz86uvvrrvvvu2bNkihMA+ROvW+05Jsr7Wp6PRKABgbOPPf/6zYRibN2/OycnBTAMM6Lu81HefuGVmik7NYIoipIEo+iQDgYCqqrW1tffee+/f//733NxcSNSD8Hq9+fn55D6xd1jVRwwgIbcnGo2Gw2FVVbds2XLjjTc2NDRgphv6ZmxsZF+gb/1sXUC+8f9vGAndMoNypA5v0g0NDQ888MBrr73m9/ubm5shsYtDPB7HfyGx0q9bX6Z7WPyf9vqKaHQgBxVxgf+//vWvn/70p8ePH8d8dE5kc7wIiY77XLqdUpE2zWxsbFy7du3zzz8vhMDBlSItb6c1eH3tjm7voNRMbA5lSqAbOR6Pv/jii4888siZM2fwztWnxs+9j7NFKBu97s8GRWJrB5Di8gDw2GOPbdq0yXKk/EF9sA/Z3iRcnGG5ROiqwVvbpk2bnn322dbWVk3TMBXetThbhET3FYjRP0VRMMEKu4Wqqn/605+2b9+OK1+hL/ke+zhCiAEDBuBjWvqI/+L6xptvvnnq1KmBQACj9rY1tA/gbK+UEAIUgA5FmOJAlNwqGDBEpR0+fHjjxo2ffPIJAIRCocbGxow2P5vx+Xz19fX4QB5iAEBOTs4vf/nLxYsXo4+U54QOFiHqTRHWZ5IPE4nVqx2cjTK50Gvn8Xhqa2s3btz4/vvvA4C8bA8HVH1tBtjXoAQGiqNiMu20adMef/zxiRMntra24ryRUtjsbK6tZMPIKpUAfSrRMyoPgR7RV199ddOmTRhWlvdCYlLE5/Pl5uZSIQ9VVe+5554tW7ZceumlQoicnByPx4ObSblZgZAFlhCSpnztaYWMWAcnpFc//vjjJ598EgCCwWDyZmNsBlNB13UsWhOLxUpKStauXbtw4cJ+/foJISKRiM/nww1nwPUr7p1qCf/rn1TaMoNdDY9RJYimpqYXXnjh4MGDoVCIRlN9J0vbKXi9XrSBBQUFP//5zxctWhQKhdD04eakFO9xswLB0ZYQsYhQnPuSklQPtz0wkR+Loxw5cuTxxx9XFAUD8XLcmZJp2Bh2jJIo7B0KhR5++OG77roLp38+n4/q0GBRNl7N5FQR/s8ro/zvX4CEGRQAUkidpCjat2S0alZV1UceeUReUEt6S37AyKhSvUZafrlu3bp77rmHri1I+5wCVyIFAIcORzu1bEryRDHJldrmaVVVffvttz/66COUGTtjUgclR0F5/Lt69erly5dTjB4dpIwFR4qQSJ6kKQIUYY0NpqIlesvLL7+MO/65fIyUOpZfgSZ406dPv++++zweD1ZejMfjHVRzczPO7meUsd3N4ASiqurRo0crKirwX56rpAVFWTEu7/P5Hn30UawuQxmkdrexj+LITtb5cDRNMwiJEdTevXs/++wz0h73m9Sh64wi/L//+79JkyYBgKZpHo8HYxIZWWWSfXME54kwOTxo+VU6UGAHMQZVVXVd37VrV/b9xj2NvOIEIzpXXnnlkiVLMP8W07jxVXbDtInzRNgdOg70ffvtt//+97/h3H0mmE6xrCkpLCxcunRpXl5eMBhsbm72+/2GYeBeGvaut+yzODJEIST/Z5sZ222+q2MFGoZx6NChw4cPQ9+rENHHsayrHjt27Jw5c3ADmVAoZCY2kAK2hO3gvJt9e9E+AdBBGJCcnzTNk+/KaPree+89SHQU7i4pYqmwKIRYtWpVfn4+JFJwM55plH33R0dawtShn7+DB+i+a2hoqKqqMqWtanu9sY5E9l2ZpnnJJZeMGDGipz9UZNd+3Q6zhJ16ZZKfscQM6bFll4Vjx459/PHH9Az7RVNHdrpce+21o0aN4ltYWjhMhBbkH1vpLCFGJllj33zzDS7e5Q6ULngxsS7B5Zdfjp4YuxvlJBwmwlQU0ukx8gHkPT9x4oSR2EGe6Rrjxo0bN24chuZ79IOy7Ebp1D7X8ZSg0x+JUo3xPIZh0Ca12TTZ6AUUaTvUqVOnDhkyBKTMNSYVnCrCjmkvxiCkCoUglQBraWmpqqoCngqmD15DjNGPHj26T+3M4RQcKcIU6xfS7j8I7aEJ55pK3M+5rq6OCj2l/imMnNVQWlpqmqbH4+F7WVo4KUSRimu0vbcQ2EWofiEAeL3eM2fOHD16lKrZW1JAmA4gvQ0ZMmT06NEA0DtVm7IpSuFIS9hNkpcaxmKxlpYWkdiq1qZ2ORhVVYuLi/Py8vDuxpYwLdwoQgtYdwirOZEZZCmmy8CBAwsKCiBRo8Du5jgJl4rQEqVobW2tra2FtnYpZDoFYxKBQCAnJwf9Xpz0lxYOFmF3brcWEUajUXnzCciuKUdPgxcqJycHSzmxGUwXB4uwy1jUhfsE0b/ch9IF71xo/dBZyhkzaeEkEWZcHhSvl0VIPnf2LqQFCq8X0mWyb4TiJBEiGfkN5GqImD3D2usmuq5jYV+ukpwuzhNhN+2hHMSnCQzuUykShX15RNoFGhsbcY8XrKvd02TTb+Q8EXYTulWTMUQzSCJk10LXaG1tbW5u5t1duoDrRIhYQhF+v580aV+jnE11dfWJEydS3A2SkXGdCOUpH04ChRC5ubn4mOczXQAv2rFjx77++ms4d3MrJhXceLHkYCAAKIoSCASwODSHB7uAx+MRQoTD4RMnTgBXLk8f110v9ILSmiZ84PP5zjvvPACgQtHck1KHAjzHjh3DdUwcJ0wLl3Y12TEDAH6/v6ioyHIA8BQxNWiJ5gcffFBVVcWjiXRxnQgtY1FIbFJZVFQkWz9ezZQWOTk5APDWW2/hijAeR6SFgy9W12631D/o7bFYbODAgcOGDTNNE2eGvAdouuCeZ7FYbP/+/ZbbnDw0zchVzb5lLk4SYaauu+VX9Hg8mqbhnBA3kWUDmBYYevX5fEKInTt3nj59mnIhIHGdTdPEjAi7G9sXcZIIM4jcS7ADDRw4EFiEXQIvZjweVxSlsrLy7bffpifli6xpGi9xahN3iRDTi5UE8tSlpKQkeTFhNo15egjMj0Gx4czwlVdeOXv2LOXiqqoai8V4eN8BjhRhdywVBegtrpeioiLcQYEOyEBDXQDGJzRNUxQFZ4Zbt2596623MGSPdzrTNHVdl5eqMDKOFGGXoX0mkrOrBg0aNHToUMvxLMVOocxbrDWqaVpzc/OmTZswBRfDhl6vF7cKtbuxfRQXiTB5KxiZwYMHDx8+HCThsZ89dSjJwe/3A8Df/va3Xbt2KYqCG6SpqopZNRzEbxMn9bMMztAsvjsAyMvL69+/P7D1Sx/TNP1+P/q0cESqKMoDDzxQVVWF8kOryLVn2sMxIrRUhcEHqcsyeW8mi6ETQowfP556CYcK0yIajcoDDSHE+++//+yzz2LxHgzf4zpDy1WltZ3pfmI2+cwcI8J03ZXquXT6RiHERRddNGjQIL5bdw0SEj3YvHnz7t274/G4pmnxeBxHqnDu/RR/GpePPhwjwnRJd22uaZoXXXTRwIEDUYRsBruJqqonT5584IEHvvzyS0VRcLBK8SGMK9JvxCJ0JKnkLqU1zvF4PAUFBUOHDtV1nUaq7JvpJh9++OH9999fW1ubk5MTDodxkE/Cw5GqYRjpjj6yaSwKzhUhkUEder3eyy+/HBKbrYPr79DdAeeBQogdO3bce++9p06dCgaDkUiEEko9Ho/H4+GoLDhOhKlvBUOkqEMcf1511VWhUIg293J55+gOWAgYAAzDeOmll5YtW1ZbWxsIBDCsD4koP7pMs8yypYvDRNgFUq96oijKmDFjvve97/V0k9yAruuY0YZS3LFjx+LFi0+cOBGNRoUQGLewVD1PnSwTreNFmKnfA8dFeXl5Y8aMgURKJNMdYrEYLQ2LRqNbt25dunTp0aNHMVbh9/tpyu3yEYfzRJg8Ik1RhxSgbxN8KRKJ3HLLLaqq4mCJHTPdBGP36H0BgO3bt990001btmzBq93a2grSTliQ2PEXH7T3e2Xfj+K875OW6aNk0U4jFriDgt/vHzdu3ODBgzElkgMVGQR/iyNHjixcuHDZsmVffPEF7uJEg1JMQDUMA6v+JCdUZCsO/pJp+WY6PYaW24RCoVtvvRWfzLK5h73IK1c2b9587bXXPvPMM5FIRNM0ufqWvB+BS4apjhRh6j7S1EMUXq+X4shz5szBlYcu6QS9CebNxGKxr7/+evny5bNnz962bVtNTU0sFkMd0ujUJWYQHCpCC5myV1h/QVXVkSNHXnzxxRk5JyOjKEo0GkWHDY479u3bd9111y1atOi3v/3t2bNnaR5IOd92N7k3cKQIu+yb6QBd12kpQGFh4S233AJuuhn3DkIItIThcDg3NxefzM3N3b59+/r16xsbGzF0AVJlGhtb22s4tZNZVNf9LRAwS0ZRFF3XPR7P9ddfX1pa6pJO0MtgvZlwOJyfn19QUNDS0qKq6m233YZlfiil2z3bWjhVhESmpm2qqra0tHi9XhTeBRdcMGPGjIycmZHBICHO1RsbGxsaGoLBYCAQWL58Oa7Kp+0iwTWOMaeKsGPfjOjSb4cB+kAgAACqqi5atIg+iBZD8UKnriGvNrTsNRAOhx988MHzzz/fNM3c3FzZH+YSx5hTRQhJOjxnzW76vx3GpiBRrNbr9Y4YMWLKlCk4KKKq0u7xFmQWefZOM22U4oABA6ZPnw7n/oLsmHEYGXTPAICmabjs7fzzz7/55pvp5IZhYPjeJT2jh0ieZs+dO/fSSy+lfyk+4Z7r7GARyj9SKssLO0besJ7ENnv27NGjR9MtWSS28u1my91Dm7+I7P8sLS0tLy/HMYhra706WITQ1g/WnZ8Q64KBJOkLLrgAs2dwECV4+8s0SZ7U4QX0+/340vz587/73e9SuW48xm0FL5zdpbqwvLA9TNPEKAWWc8dYhaZpc+fOHTlypGma6LBhM9gdcHYNie0GQqHQD3/4Q3k9oSJhZ0N7F2eLENpyz3Tt90NDBwAYL1YUBZ2lY8aMue2221RVxZR/pjvQvRKluHjx4jFjxqAnDC0hDfu7VoLNoThehF2LRrR9Kmn3EirFh4H7IUOGCCEoyYPpMrRBRTAYvOWWW/Lz83ENviw/toQOQ4U2Uti6bAwtJZ6EEIZhTJgwYcGCBQDQ0tICiVChfDDTMXih0PtCXpm77rpr8uTJdADOuuUAhnt0mA3dqCdSSelUOFGcN29eWVkZFkQxDAMTa3h+mCJ4oWiyDQBDhgy58847adNCnCK6lmwTYaZ0SN0FxTZ27Ni7775bCBEKhYDdM2lCV5LcyytWrBg9erSmaTTmt7uNdpINIgR0aivWZ7p/TkVRYrEYumpuv/32K664oqGhAdfgd/PkroLujHjpZs6cuXDhQl3XFUUhN5i9LbSX7BGhCopQMmkMIbH0xjAMwzCKi4tXr15NfoXuttg10LVCB4zf71+zZk1eXh5u2ARSlVfXklWdCVNGu++hQeSENY/Ho+v6jTfeuGTJEr5zp4i8oYCqqljEafXq1dOnT8drSAJoJY4AAAv4SURBVLNB90Qj2iR7RChLLlM/Kp4Q3XpYemjp0qXDhw/HPFJwkwevC9CgnZaeXHPNNYsXL8aiMliYVA4PupbsESFk2k2K/QN7Eu1nMmnSpBUrVlCWo9siWumC9y8MwAaDwXvvvXfo0KFYTw2X2OOtzeWOrqwSIbSlwy7P32hbNUh0Jjz5vHnz5s+fjxKVF7/xRFEGrxXevDDJYdWqVdOmTQPJQoI0WBWJ/WFIkO5RZrb1m4yHK/C92G/wcWFh4V133TV48GB5R1FIbIHSjbZnFVTd0OfzNTc3z5o164477ggEAuiMsegNke96+K9L4odZ2GnaHJR2P2yID2KxmBBiypQpq1atMgxDzgIBN928UwdVt3LlyrKyMtM0NU3z+Xy4H5McjMWoPdlDfK9LbmpZ+CXb9NB0WYRyiBnPjF1k4cKFc+fO1XUddchYwCtWUFBgmuaGDRtmzpyJVg73BtV1HQvd08Eej0eR6jthLWYWoYNpc1Da5VPRiBSXO6EOQ6HQgw8+OGTIEPS8o5vBJZ0mdRoaGubNm1deXo5Dd8z4AwCPx4MuGTktCVWHT3q9Xvcsrs/aTpOswy7/qNg/5BJPKLYJEyY88sgj9CnAw1EAOHeBfFlZ2UMPPVRaWgoAOKCQ74mkQPrXnQHDbBZhsuS6IMLkt8TjcSGEpmnxeHzBggUrV64MBoO4Ari9z3UP+PUxPS0/P/+xxx4bPny4qqqRSIRG8vJSCYsmZR26R5BZK0KgLZkUMKFbg1LargRPgrtW0NYl999//2WXXdbep7sKeRWSpmmLFy8uLy/3er2xWAyH65go0+ZkwbKCyVUR/GwWIeAQVIAiMjM5JFsnEvsHRaPRoqKiX/3qV6NGjcKd1fCDfD5fL9zIU+ym8kw14z2bThgMBin9xTTNhQsXrlmzBjNjcLUEzqhp8EmL54WS4VpBjiPLRQgZddLAuesMMUQRi8UuueSS9evXDx8+PBqNYo9Hbw3mZ3XzEy3IWwh32lMtky7ogTEentDn84XD4by8PAAwDGPKlCkrVqwoLi4GACwhQ8UKLANRBUAFVw/gwSUi7KGcUrzBA4CqqjfccMO6detwd2hIZNigQzWzPUyOX3fqBxK9Uh6OVpY0NTUBQGlp6dNPP33RRRcBQDgcpriOHIiXnTEuVyC4QYTQM780xqDj8bjX68URV3l5+bp165L9qJk1PsllNXB/lfYSWXvaYYsLlDD6J4QYNmzY73//++985zuYdhsMBmn5JU32kh2kLscVIoSM6lBeDE6Rehxx/fjHP96wYQOue8I9hqi6fqYskpxQkpxu0oHgySeZ2eo4uq6T/R81atTmzZuvueYaXGUibxyA8SEqn4XHc1gVcdFVyLg9JDFgBD8SiQQCgdWrV69du1ZV1ZMnTwIA7nko5393E+zQcpFiaGubGirmKR9MOSiZNY/hcBgrgGzevPnqq6/GsQB9hGEYFJyQQ7WsQMJdi5ozaAyxQ9PCQiFEIBAIh8Mej2fNmjV+v3/16tUAUFhYWF1dnZOT09zcTOOxdNssv0Ue3WG4ctCgQYMHD8ZwnBACR4aqqra2tjY3N+/atQsSRRxBChJkapCMNa+KioqefPLJK6+8EvP4cI9HPMDj8eDQtL0BM+MuEWYQ5VyEEJFIJBgMRqNRj8dzzz33lJWV3XHHHdXV1cFgsLGxMVOTQ8MwNE27/PLLZ86cOWnSpMGDB+fl5eXm5no8HnIUYTQlGo1GIpGWlpaWlpbPP/98x44dr7/+enV1dWZDcLFYbPLkyU8//fRll12GSkMdkkeKXDKKosTjcRw1ZOrTs4SKigrBdAOcpGEdmng8jtMz/Ltv376ZM2fSpfb5fPI6HXq+00kavqpp2vjx45944ol9+/adPn06Eokkt4Tag22g6aJhGK2trceOHXvttdewhqqMZU1WKtCoeNmyZQcPHpQvAtLB5eq538KJVFRUsAgzgNz/cHFANBrFlw4dOoRTRFlRKZoC8voUFhaWl5dv3bo1HA63tLTgmTF7zjCMaDSKepNFKDeJ/sU3NjU1HT16dO3atRdffDF9ltfrLSkpSZaiem5BXrodKIoydOjQ55577vTp03jaWCyG9yD8xPauFa4FYwgWYcaQO70QIhKJtLa2ok7Onj1bUVExe/bswsJCizFpz/6QT2Xs2LErV67cv38/fgSeUAih6zo+tmhPbg9pD//GYjFd12XzeOrUqSeeeGL8+PHk3iSNySK0PAgGgz6fb8mSJV988QV+FppZMrl0J0rlorFhrKioUCoqKmbNmpXKjZlpD7yaIDlgRSJ7SwihaVosFmttbd2zZ8+2bdvefPPNw4cPowuxzfqliqKUlJRMnjz5+uuvnzFjxrBhw1AAsVgMvSCKouAbMQssHo93YU2jEKK1tTUnJ+fs2bOvvfbaP//5z08//fTAgQPyMeTOwVZ5PJ7BgwfPmjVryZIll112GX40AGDmEHlEsXntFTLEDwUpO1S4KU00mcrKShZhxkApUgwAHyuJhap4TDweP3LkyN69ez/99NPjx48fP37c6/Wi9dA0rbi4eMSIEaNGjRoxYsS0adMwU5wcP3RyPGHqfZfuEYgcpcASEl6vNxqNHjx48KOPPjpw4EBtbe2JEyei0SjeJoLBYFFR0fDhw8vKynBvnOTPxY9QEouYUl+P63IFAkBlZSV7RzMGqgUrdlMgQc5XxvXjF1544YUXXggAjY2NVVVVmqahE9/n8/Xr1y83NzcQCMj9UiScGUqidFKykUGvY5utQssp54uRvRJCYMYPAHi93okTJ06cOBEAGhoa6urqMP0Vs9ILCwv79+8PiWVcmCLr8/mi0SitviXh4Q2lvatk0afLFYiwCDMMrRigJRf4vNzbsCPm5+fn5uZSVF2OpOPMTdM0ykej96Jm8BhKEO+4iLX8drKlciOxBCgAYL5Bfn5+Xl4exlRkdVHsAd28LS0tubm51BI8legsDEODZx6OEpy1kGHURDUaIS1OpUA5DQLj8TiaL5FYkk/pl/gunGXBudktOAfDM5AaO+73croMnQo3BqeQHVakN00zEAiIRJYZeVMhIVpL3g8qkKa+eH4lsc6rvfbQ/aLN25M7YUuYeSzTITJl8vPUF0m0WI4aEi6QNs9msXhphfWSzal8HssKI5EouEQKT35MrcWz0fl5b4l04etlM9TpLU/aZR9kuVqmppa0O7ZgmYJF2CfoWlppz7Wkg7EiPdMXWpsdsAhtRu7lzrItXWuts75j78COGYaxGRYh06uwJUyGRcgwNsMiZBibYREyjM2wd7SLJMf32qzdYEme7oNYYhJIm98u+TAmI7AIu0hyuMyykoAOk4/sO4JsT1RtJg+0eX9hMgWLsFvI64zwGUvaV/LB0E5H753WQjvWrL0bBMuvF2ARZgA5zdJiGOUD5OMhKUumhzTZ8TCyvdtB8rt4rUPPwSLMGNRxk61K8tJ7y7sgaXybriZTz7zpWPbtiZYV2HOwCDNPm4YRkdUI7fTsnnCBpGJv2fViFyzCnkI2jNCWGqHH3I/yadtTnVBAESy8PgGLsMdJngG2KQzLS10QRlojWE1RgaXXN2AR9ioW8wgdWKqM+ml4GXtfhkVoD+35Y4juiJAl5yxYhH2F9rymSHtRBCYLYBE6A5ZcFsMJ3AxjMyxChrEZFiHD2AyLkGFshkXIMDbDImQYm2ERMozNsAgZxmZYhAxjMyxChrEZFiHD2AyLkGFshkXIMDbDImQYm2ERMozNsAgZxmZYhAxjMyxChrEZFiHD2AyLkGFshkXIMDbDImQYm2ERMozNsAgZxmZYhAxjMyxChrEZFiHD2AyLkGFshkXIMDbDImQYm2ERMozNsAgZxmZYhAxjMyxChrEZFiHD2AyLkGFshkXIMDbDImQYm2ERMozNsAgZxmZYhAxjMyxChrEZFiHD2AyLkGFshkXIMDbDImQYm2ERMozNsAgZxmZYhAxjMyxChrEZFiHD2AyLkGFshkXIMDbDImQYm2ERMozNsAgZxmZYhAxjMyxChrEZFiHD2AyLkGFsRlUUBQCEEHa3hGFcioryQykyDNP7eACgsrLS7mYwjHv5f9M8bYutXHbkAAAAAElFTkSuQmCC';
            // image.src = "https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/pie.png";
            image.addEventListener('load', () => {
                context.drawImage(image, 0, 0, 573, 573);
            });
        }
    }

    return (

        <Page data={props.page}>
            <Label data={props.pgTitle}/>
            <Label data={props.canvasMessage}/>
            <Button data={props.btnScanPeripherals} action={onScanForPeripherals}></Button>
            

            <ComboBox data={props.cmbPrinter} functions={functions} />

 


            {/* <FlatList
                persistentScrollbar={true}
                style={styles.flatList}
                data={devices}
                renderItem={renderItem}
                keyExtractor={(item, index) => index.toString()}
            /> */}
            <Button data={props.btnPrint} action={onPrintCanvas}></Button>
            <Canvas ref={canvasRef} style={{display: 'none'}}/>
        </Page>

    );
};

// Styles for Flatlist
const styles = StyleSheet.create({
    flatList: {
        alignSelf: 'center',
        maxHeight: 80,
        width: '90%',
        flexGrow: 0,
        backgroundColor: 'white',
    },
    item: {
        height: 44,
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemText: {
        fontSize: 15,
        color: 'black',
    },
});

const mapStateToProps = (state) => {
    return { ...state[StateName] }
};

export default connect(mapStateToProps)(PrinterPage);