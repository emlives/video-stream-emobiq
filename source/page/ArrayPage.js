import React from 'react';
import { connect } from 'react-redux';

import Page from '../framework/component/Page';
import Label from '../framework/component/Label';
import Button from '../framework/component/Button';

import Functions from '../framework/core/Function';

import { StateName } from '../reducer/data/ArrayPage';

let functions = new Functions(StateName);

const ArrayPage = (props) => {
    // Append the dispatch to the function instance
    functions.pageProps = props;

    let handleClickOne = () => {
        functions.setComponentValue({
            name: 'lblResult',
            value: functions.sum({ values: ['1', 5] })
        });
    }

    let handleClickTwo = () => {
        functions.setComponentValue({
            name: 'lblResult',
            value: functions.count({ values: ['1', '2', '3'] })
        });
    }

    let handleClickThree = () => {
        functions.setComponentValue({
            name: 'lblResult',
            value: functions.avg({ values: ['1', '2', '3'] })
        });
    }

    let handleClickFour = () => {
        functions.console(
            functions.sort({ values: ['5', '1', '3'] })
        );
    }

    let handleClickFive = () => {
        functions.console(
            functions.newArray({ variableName: 'vStoreArray' })
        );
    }

    let handleClickSix = () => {
        functions.console(
            functions.newObject({ variableName: 'vStoreObject' })
        );
    }

    let handleClickSeven = () => {
        functions.console(
            functions.push({ variableName: 'vStoreArray', value: 'haha' })
        );
    }

    let handleClickEight = () => {
        functions.pushObject({ variableName: 'vStoreObject', key: 'a', value: 'haha' })
        functions.console(
            functions.pushObject({ variableName: 'vStoreObject', key: 'b', value: 'kevin' })
        );
    }

    let handleClickNine = () => {
        functions.console(
            functions.pop({ variableName: 'vStoreArray' })
        );
    }

    let handleClickTen = () => {
        functions.console(
            functions.unshift({ variableName: 'vStoreArray', value: 'Unshift Boy!' })
        );

        functions.console(
            functions.unshift({ data: ['1', '2'], value: 'Unshift Boy!' })
        );
    }

    let handleClickEleven = () => {
        functions.console(
            functions.shift({ variableName: 'vStoreArray' })
        );
    }

    let handleClickTwelve = () => {
        functions.console(
            functions.findArrayIndex({ variableName: 'vStoreArray', value: 'haha' })
        );
    }

    return (
        <Page>
            <Label data={props.lblResult}/>
            <Button data={props.btnSum} action={handleClickOne}/>
            <Button data={props.btnCount} action={handleClickTwo}/>
            <Button data={props.btnAverage} action={handleClickThree}/>
            <Button data={props.btnSort} action={handleClickFour}/>
            <Button data={props.btnNewArray} action={handleClickFive}/>
            <Button data={props.btnNewObject} action={handleClickSix}/>
            <Button data={props.btnPush} action={handleClickSeven}/>
            <Button data={props.btnPushObject} action={handleClickEight}/>
            <Button data={props.btnPop} action={handleClickNine}/>
            <Button data={props.btnUnshift} action={handleClickTen}/>
            <Button data={props.btnShift} action={handleClickEleven}/>
            <Button data={props.btnFindArrayIndex} action={handleClickTwelve}/>
        </Page>
    )
}

// Bind the state to the props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
}

export default connect(mapStateToProps)(ArrayPage);