import React from 'react';
import { connect } from 'react-redux';
import Page from '../framework/component/Page';
import Label from '../framework/component/Label';
import Panel from '../framework/component/Panel';
import Edit from '../framework/component/Edit';
import Button from '../framework/component/Button';
import Memo from '../framework/component/Memo';
import DataList from '../framework/component/DataList';
import Signature from '../framework/component/Signature';
import CheckBox from '../framework/component/CheckBox';
import ComboBox from '../framework/component/ComboBox';
import Image from '../framework/component/Image';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

import Functions from '../framework/core/Function';

import SnippetWrapper from '../wrapper/component/SnippetWrapper';

import { StateName } from '../reducer/data/SamplePage';

import { References } from '../dependency/global/Variable';

const functions = new Functions(StateName);

const actions = {

    /**
     * START - Load event
     */

    eventLoad: async () => {
        functions.console({
            value: "SamplePage: load event"
        });
        functions.console({
            // value: await functions.loadData(
            //     {
            //         dataset: 'l_data_list',
            //         limit: 2,
            //         callback: (data) => {
            //             functions.console({ value: data });
            //         }
            //     }
            // )
        });
    },

    /**
     * END - Load event
     */

    /**
     * START - Button gotoPage
     */
     handleBtnGotoPage: () => {
        functions.gotoPage({ p: "AppPage" })
     },

    /**
     * END - Button gotoPage
     */

    /**
     * START - Component value and set component value
     */

    handleClick: () => {
        // Normal component testing
        functions.setComponentValue({ component: "lblFirst", value: "Changed!" });
        functions.console({
            value: functions.componentValue({ component: "lblFirst" })
        });
        functions.setComponentValue({ componentId: "label_2", value: "Yesssuuu Second!" });
        functions.userDefined("updateLabel", { component: "lblThird" });
        functions.setComponentValue({
            component: 'lblFirst',
            value: 'Haha'
        });

        // For datalist testing
        functions.setComponentValue({ component: "labelValue", value: "First In Datalist only through component name!" });
        functions.setComponentValue({ componentId: "id_kevin", value: "Yesssuuu Inside Datalist!" });
    },

    /**
     * End - Component value and set component value
     */

    handleClick2: async () => {
        functions.console({ value: "PreviousPage: " + functions.getPreviousPage() });
        functions.console({ value: "CurrentPage: " + functions.getCurrentPage() });
        functions.console({ value: "MakeId: " + functions.makeId() });
        functions.console({ value: "JsonEncode: " + functions.jsonEncode({ object: { name: "kent", age: "24" } }) });
        functions.console({ value: "JsonEncode: " + functions.jsonEncode({ object: { name: "kent", age: "24" }, indent: 10 }) });
        functions.console({ value: functions.jsonDecode({ string: '{"name":"kent","age":"24"}' }) });
        functions.console({ value: functions.xmlDecode({ string: "<ALEXA VER='0.9' URL='davidwalsh.name/' HOME='0' AID='='><SD TITLE='A' FLAGS='' HOST='davidwalsh.name'><TITLE TEXT='David Walsh Blog :: PHP, MySQL, CSS, Javascript, MooTools, and Everything Else'/><LINKSIN NUM='1102'/><SPEED TEXT='1421' PCT='51'/></SD><SD><POPULARITY URL='davidwalsh.name/' TEXT='7131'/><REACH RANK='5952'/><RANK DELTA='-1648'/></SD></ALEXA>" }) });
        functions.console({ value: "DefaultImage: " + functions.defaultImage() });
        functions.console({ value: "GenerateRandomNumber: " + functions.generateRandomNumber({ min: "", max: "" }) });
        functions.console({ value: "GenerateRandomAlphabet: " + functions.generateRandomAlphabet({ characterLength: 15 }) });
        functions.console({ value: "GenerateRandomAlphanumeric: " + functions.generateRandomAlphanumeric({ characterLength: 15 }) });
        functions.console({ value: "ValueFromUrl: " + functions.valueFromURL({ url: "http://localhost/EMOBIQ/kent-emobiq-v5-platform/edit/?appid=160688080962802#/design", parameter: "appid" }) });
        functions.console({ value: "ObjectKeys: " + functions.objectKeys({ object: { name: "Kent", age: 24 } }) });
        functions.console({ value: "ObjectValues: " + functions.objectValues({ object: { name: "Kent", age: 24 } }) });
        functions.console({ value: "AddDate: " + functions.addDate({ date: "", add: 2, type: "hours" }) });
        functions.console({ value: "FormatDate: " + functions.formatDate({ format: "Y-m-d" }) });
        functions.console({ value: "StrToDate: " + functions.strToDate({ date: "2018-12-30" }) });
        functions.console({ value: "formatNumber: " + functions.formatNumber({ value: "1000.567", decimals: "2", decimalSep: ".", thousandSep: "," }) });
        functions.console({ value: "dateDiff: " + functions.dateDiff({ date1: "2021-11-10", date2: "2021-12-10", interval: "months" }) });
        functions.console({ value: "ComponentValue: " + functions.componentValue({ name: "lblFirst" }) });
        functions.console({ value: "DeviceSerial: " });
        functions.console({ value: "DeviceOS: " + functions.deviceOS() });
        functions.console({ value: "DeviceOSVersion: " + functions.deviceOSVersion() });
    },

    handleClick3: () => {
        functions.captureImage({ options: {}, navigation: props.navigation });
    },

    /**
     * START - For snippet stuff
     */

    handleClick4: () => {
        functions.setComponentValue({
            name: 'snippetAChild1',
            value: 'imageSnippet'
        })
    },

    /**
     * END - For snippet stuff
     */

    /**
     * START - For data list sample implementation
     */
    handleClickInsert: () => {
        functions.insert(
            {
                dataset: 'l_data_list',
                dt: {
                    'key': 'name' + Math.random(),
                    'value': 'kevin ' + Math.random()
                },
                extra: 'Extra data here...',
                callback: function (input, extra) {
                    functions.console({ value: 'Success:' });
                    functions.console({ value: input });
                    functions.console({ value: extra });
                },
                errCallback: function (error, extra) {
                    functions.console({ value: 'Fail:' });
                    functions.console({ value: error });
                    functions.console({ value: extra });
                }
            }
        );
    },

    handleClickClearData: () => {
        functions.clearData(
            {
                dataset: 'l_data_list',
                extra: 'Extra data here...',
                callback: function (input, extra) {
                    functions.console({ value: 'Success:' });
                    functions.console({ value: input });
                    functions.console({ value: extra });
                },
                errCallback: function (error, extra) {
                    functions.console({ value: 'Fail:' });
                    functions.console({ value: error });
                    functions.console({ value: extra });
                }
            }
        );
    },

    handleClickDataFromString: () => {
        functions.dataFromString(
            {
                dataset: 'l_data_list',
                // string: [{key: 'a', value: 'A'}, {key: '"b', value: 'B'}],
                string: '[{"key": "a", "value": "A"}, {"key": "b", "value": "B"}, {"key": "c", "value": "C"}, {"key": "d", "value": "D"}, {"key": "e", "value": "E"}]',
                append: true,
                extra: 'Extra data here...',
                callback: function (input, extra) {
                    functions.console({ value: 'Success:' });
                    functions.console({ value: input });
                    functions.console({ value: extra });
                },
                errCallback: function (error, extra) {
                    functions.console({ value: 'Fail:' });
                    functions.console({ value: error });
                    functions.console({ value: extra });
                }
            }
        );
    },

    handleClickSelectAll: () => {
        functions.selectAll(
            {
                dataset: 'l_data_list',
                extra: 'Extra data here...',
                callback: function (input, extra) {
                    functions.console({ value: 'Success:' });
                    functions.console({ value: input });
                    functions.console({ value: extra });
                },
                errCallback: function (error, extra) {
                    functions.console({ value: 'Fail:' });
                    functions.console({ value: error });
                    functions.console({ value: extra });
                }
            }
        );
    },

    handleClickSelectBy: () => {
        functions.selectBy(
            {
                dataset: 'l_data_list',
                by: 'value',
                operator: '=',
                value: 'kevin',
                first: true,
                extra: 'Extra data here...',
                callback: function (input, extra) {
                    functions.console({ value: 'Success:' });
                    functions.console({ value: input });
                    functions.console({ value: extra });
                },
                errCallback: function (error, extra) {
                    functions.console({ value: 'Fail:' });
                    functions.console({ value: error });
                    functions.console({ value: extra });
                }
            }
        );
    },

    handleClickUpdateBy: () => {
        functions.updateBy(
            {
                dataset: 'l_data_list',
                by: 'value',
                operator: '=',
                value: 'kevin',
                data: {
                    key: 'updated :3'
                },
                first: false,
                extra: 'Extra data here...',
                callback: function (input, extra) {
                    functions.console({ value: 'Success:' });
                    functions.console({ value: input });
                    functions.console({ value: extra });
                },
                errCallback: function (error, extra) {
                    functions.console({ value: 'Fail:' });
                    functions.console({ value: error });
                    functions.console({ value: extra });
                }
            }
        )
    },

    handleClickDeleteBy: () => {
        functions.deleteBy(
            {
                dataset: 'l_data_list',
                by: 'value',
                operator: '=',
                value: 'kevin',
                first: false,
                extra: 'Extra data here...',
                callback: function (input, extra) {
                    functions.console({ value: 'Success:' });
                    functions.console({ value: input });
                    functions.console({ value: extra });
                },
                errCallback: function (error, extra) {
                    functions.console({ value: 'Fail:' });
                    functions.console({ value: error });
                    functions.console({ value: extra });
                }
            }
        );
    },

    handleClickLoadData: () => {
       functions.loadData(
            {
                dataset: "l_data_list",
                // order: [
                //     {
                //         "field": "value",
                //         "order": "asc"
                //     }
                // ],
                limit: 2,
                // page: 1,
                filter: [
                    // {
                    //     "field": "value",
                    //     "o": "=",
                    //     "value": "kevin"
                    // }
                ],
                callback: (data) => {
                    functions.console({ value: data });
                }
            }
        );
    },

    handleClickLoadNext: () => {
        functions.loadNext(
            {
                dataset: 'l_data_list',
                datasetDisplay: '',
                beforeCallback: () => {
                    functions.console({value: "Before loading the next data..."});
                }
            }
        );
    },

    /**
     * END - For data list sample implementation
     */

    /**
    * START - For combobox sample implementation
    */
    handleNextPage: () => {
        functions.gotoPage({ navigation: props.navigation, targetPage: "NextPage" })
    },

    handleSaveSignature: async () => {
        References[StateName].signature1.saveImage();
    },

    handleComboOptions: () => {
        functions.setComboOptions({
            name: 'comboBox1',
            data: [
                {
                    language: 'React Native',
                    id: 'rn'
                },
                {
                    language: 'PHP',
                    id: 'php'
                },
            ],
            valueField: 'id',
            valueSeparator: '',
            displayField: '["language","id"]',
            displaySeparator: ''
        })
    },

    /**
     * END - For combobox sample implementation
     */

    /**
     * START - Set value or default value with function to a component
     */

    setLblFirstValue: async () => {
        return await functions.concat({
            string1: 'Hello'
        });
    },

    setLabelKeyValue: record => async () => {
        return await functions.concat({
            string1: 'Hi There ',
            string2: record?.['value']
        });
    },

    /**
     * End - Set value or default value with function to a component
     */

    /**
     * START - Datalist actions
     */

    handleClickDataListButton: record => () => {
        functions.console({
            value: record
        });
    },
    
    handleClickDataListButtonInside: record => () => {
        functions.console({
            value: 'inside datalist wohoo'
        });
        functions.console({
            value: record
        });
    },

    /**
     * END - Datalist actions
     */

     handleBtnSetComponentValueClick: async () => {
        functions.setComponentValue({
            component: "lblSecond",
            value: functions.generateRandomNumber({
                min: 1,
                max: 10000
            })
        });
        functions.console({
            value: "SetComponentValue clicked!"
        });
     }
}

const SamplePage = (props) => {

    // For datalist autoload handling when to render
    const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;

    return (
        <Page load={actions.eventLoad} currentTime={currentTime}>
            {/* <Panel>
                <Button action={actions.handleClick} data={props.btnCaption}></Button>
                <Label data={props.lblFirst} value={actions.setLblFirstValue} functions={functions} currentTime={currentTime}></Label>
                <Label data={props.lblSecond}></Label>
                <Label data={props.lblThird}></Label>
                <Memo data={props.txtMemo} functions={functions}/>
                <Image/>
                <Button data={props.btnGotoPage} action={actions.handleBtnGotoPage}/>
                <Label/>
                <Button data={props.btnSetComponent} action={actions.handleBtnSetComponentValueClick}/>
                <Label/>
            </Panel> */}
            <Panel>
                <Button action={actions.handleClickInsert} data={props.btnInsert}></Button>
                <Label></Label>
                <Button action={actions.handleClickClearData} data={props.btnClearData}></Button>
                <Label></Label>
                <Button action={actions.handleClickDataFromString} data={props.btnDataFromString}></Button>
                <Label></Label>
                <Button action={actions.handleClickSelectAll} data={props.btnSelectAll}></Button>
                <Label></Label>
                <Button action={actions.handleClickSelectBy} data={props.btnSelectBy}></Button>
                <Label></Label>
                <Button action={actions.handleClickUpdateBy} data={props.btnUpdateBy}></Button>
                <Label></Label>
                <Button action={actions.handleClickDeleteBy} data={props.btnDeleteBy}></Button>
                <Label></Label>
                <Button action={actions.handleClickLoadData} data={props.btnLoadData}></Button>
                <Button action={actions.handleClickLoadNext} data={props.btnLoadNext}/>
                <DataList actionItem={actions.handleClickDataListButton} data={props.dataList1} functions={functions} currentTime={currentTime}>
                    {({ item, parentStyle }) => (
                        <Panel>
                            <Label data={item.labelKey} value={actions.setLabelKeyValue} value_data={item._data} functions={functions} currentTime={currentTime} parentStyle={parentStyle} />
                            <Label data={item.labelValue} parentStyle={parentStyle} />
                            <Button action={actions.handleClickDataListButtonInside(item._data)} data={item.buttonValue} parentStyle={parentStyle}></Button>
                        </Panel>
                    )}
                </DataList>
            </Panel>
            {/* <Panel>
                <ComboBox
                    functions={functions}
                    data={props.comboBox1}
                />
                <Button action={actions.handleComboOptions} data={props.btnComboOptions}></Button>
            </Panel>
            <Panel>
                <Edit data={props.input1} functions={functions}/>
                <Button action={actions.handleClick} data={props.btnCaption}></Button>
                <Label></Label>
                <Button action={actions.handleClick2} data={props.btnConsole}></Button>
                <Label></Label>
                <Button action={actions.handleClick3} data={props.btnCapture}></Button>
                <Label></Label>
                <Button action={actions.handleNextPage} data={props.btnNextPage}></Button>
                <Memo data={props.memo1} functions={functions}></Memo>
                <Signature data={props.signature1} inputRef={el => { References[StateName].signature1 = el }}></Signature>
                <Button action={actions.handleSaveSignature} data={props.btnSaveSign}></Button>
                <CheckBox data={props.checkbox1} functions={functions} />
            </Panel>
            <Panel>
                <Label data={props.lblSnippetInformation}></Label>
                <Button action={actions.handleClick4} data={props.btnChangeSnippet}></Button>
                <SnippetWrapper data={props.snippetA} functions={functions}></SnippetWrapper>
            </Panel> */}
        </Page>
    )
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] }
};

export default connect(mapStateToProps)(SamplePage);