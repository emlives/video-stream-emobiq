import React from 'react';
import { connect } from 'react-redux'; 
import { View } from 'react-native';
import Page from '../framework/component/Page';
import Label from '../framework/component/Label';
import Button from '../framework/component/Button';

import Functions from '../framework/core/Function';

import { StateName } from '../reducer/data/NextPage';

let functions = new Functions(StateName);

// list template to be rendered for datalists
const template = {
    datalist1: ( {item} ) => (
        <View>
            <Label data={item.LabelKey} />
            <Label data={item.LabelValue} />
        </View>
    )
}

const NextPage = (props) => {
    console.log(props.route.params)
    // Append the dispatch to the function instance
    functions.pageProps = props;
    
    let handleSamplePage = () => {
        functions.gotoPage({navigation:props.navigation, targetPage:"SamplePage"})
    };

    return (
        <Page>
                <Button action={handleSamplePage} data={props.btnSamplePage}></Button>
        </Page>
    )
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] }
};

export default connect(mapStateToProps)(NextPage);