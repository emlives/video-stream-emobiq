import React from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgItemListing';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import Edit from '../framework/component/Edit';
import DataList from '../framework/component/DataList';
import ComboBox from '../framework/component/ComboBox';
import Button from '../framework/component/Button';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handlePnlHeaderLeftAction: () => {
		functions.gotoPage({
			"p": "pgMainMenu"
		});
	},

	handleTxtSearchChange: () => {
		functions.userDefined('globalModalLoading', {});
		functions.clearData({
			"dataset": "l_Item_List",
			"callback": (input, extra) => {
				functions.conditional({
					"condition": functions.componentValue({
						"component": "txtSearch",
						"componentId": ""
					}),
					"yesValue": "",
					"noValue": "",
					"extra": "",
					"yesCallback": (input, extra) => {
						functions.loadData({
							"dataset": "OK365_Item_List",
							"limit": 10,
							"filter": functions.toArray({
								"value1": functions.toObject({
									"f": "Location_Filter",
									"v": functions.objectAttr({
										"object": functions.getVarAttr({
											"var": "var_loginCnt",
											"attr": 0,
											"default": ""
										}),
										"attr": "Location_Code"
									})
								}),
								"value2": functions.toObject({
									"f": "Description",
									"o": "=",
									"v": functions.concat({
										"string1": "'@*",
										"string2": functions.componentElAttr({
											"component": "txtSearch",
											"componentId": "",
											"attr": "value"
										}),
										"string3": "*'",
										"string4": ""
									})
								}),
								"value3": "",
								"value4": "",
								"value5": "",
								"value6": "",
								"value7": "",
								"value8": "",
								"value9": "",
								"value10": ""
							}),
							"orFilter": "",
							"order": "",
							"callback": (input, extra) => {
								functions.console({
									"value": input
								});
								functions.setVar({
									"var": "vTotalCount",
									"value": functions.count({
										"values": input
									})
								});
								functions.setVar({
									"var": "vLoopCount",
									"value": 0
								});
								functions.map({
									"values": input,
									"extra": "",
									"callback": (input, extra) => {
										functions.setVar({
											"var": "vLoopCount",
											"value": functions.add({
												"value1": functions.getVar({
													"var": "vLoopCount",
													"default": ""
												}),
												"value2": 1
											})
										});
										functions.selectBy({
											"dataset": "l_Item_List",
											"by": "No",
											"operator": "=",
											"value": input["No"],
											"callback": (input, extra) => {
												functions.conditional({
													"condition": functions.equal({
														"value1": functions.count({
															"values": input
														}),
														"value2": 0
													}),
													"yesValue": "",
													"noValue": "",
													"extra": input,
													"yesCallback": (input, extra) => {
														functions.insert({
															"dataset": "l_Item_List",
															"dt": extra,
															"callback": (input, extra) => {
																functions.conditional({
																	"condition": functions.equal({
																		"value1": functions.getVar({
																			"var": "vTotalCount",
																			"default": ""
																		}),
																		"value2": functions.getVar({
																			"var": "vLoopCount",
																			"default": ""
																		})
																	}),
																	"yesValue": "",
																	"noValue": "",
																	"extra": "",
																	"yesCallback": (input, extra) => {
																		functions.loadData({
																			"dataset": "l_Item_List",
																			"callback": (input, extra) => {
																				functions.userDefined('globalModalHide', {});
																			},
																			"errCallback": null
																		});
																	},
																	"noCallback": null
																});
															},
															"errCallback": null
														});
													},
													"noCallback": (input, extra) => {
														functions.conditional({
															"condition": functions.equal({
																"value1": functions.getVar({
																	"var": "vTotalCount",
																	"default": ""
																}),
																"value2": functions.getVar({
																	"var": "vLoopCount",
																	"default": ""
																})
															}),
															"yesValue": "",
															"noValue": "",
															"extra": "",
															"yesCallback": (input, extra) => {
																functions.loadData({});
															},
															"noCallback": null
														});
													}
												});
											}
										});
									}
								});
								functions.conditional({
									"condition": functions.equal({
										"value1": functions.count({
											"values": input
										}),
										"value2": 0
									}),
									"yesValue": "",
									"noValue": "",
									"extra": "",
									"yesCallback": (input, extra) => {
										functions.userDefined('globalModalHide', {});
									},
									"noCallback": null
								});
							},
							"errCallback": (input, extra) => {
								functions.setVar({
									"var": "error",
									"value": input["err"]
								});
								functions.userDefined('globalModalInfo', {
									"title": "Error",
									"message": functions.conditional({
										"condition": functions.getVarAttr({
											"var": "error",
											"attr": "global"
										}),
										"yesValue": functions.getVarAttr({
											"var": "error",
											"attr": "global"
										}),
										"noValue": "Cannot connect to Navision Server."
									})
								});
								functions.console({
									"value": input
								});
							}
						});
					},
					"noCallback": (input, extra) => {
						functions.loadData({
							"limit": 10,
							"filter": functions.toArray({
								"value1": functions.toObject({
									"f": "Location_Filter",
									"v": functions.objectAttr({
										"object": functions.getVarAttr({
											"var": "var_loginCnt",
											"attr": 0,
											"default": ""
										}),
										"attr": "Location_Code"
									})
								}),
								"value2": "",
								"value3": "",
								"value4": "",
								"value5": "",
								"value6": "",
								"value7": "",
								"value8": "",
								"value9": "",
								"value10": ""
							}),
							"orFilter": "",
							"order": "",
							"callback": (input, extra) => {
								functions.console({
									"value": input
								});
								functions.setVar({
									"var": "vTotalCount",
									"value": functions.count({
										"values": input
									})
								});
								functions.setVar({
									"var": "vLoopCount",
									"value": 0
								});
								functions.map({
									"values": input,
									"extra": "",
									"callback": (input, extra) => {
										functions.setVar({
											"var": "vLoopCount",
											"value": functions.add({
												"value1": functions.getVar({
													"var": "vLoopCount",
													"default": ""
												}),
												"value2": 1
											})
										});
										functions.conditional({
											"condition": functions.equal({
												"value1": functions.count({
													"values": functions.selectBy({
														"dataset": "l_Item_List",
														"by": "No",
														"operator": "=",
														"value": input["No"]
													})
												}),
												"value2": 0
											}),
											"yesValue": "",
											"noValue": "",
											"extra": input,
											"yesCallback": (input, extra) => {
												functions.insert({
													"dataset": "l_Item_List",
													"dt": extra,
													"callback": (input, extra) => {
														functions.conditional({
															"condition": functions.equal({
																"value1": functions.getVar({
																	"var": "vTotalCount",
																	"default": ""
																}),
																"value2": functions.getVar({
																	"var": "vLoopCount",
																	"default": ""
																})
															}),
															"yesValue": "",
															"noValue": "",
															"extra": "",
															"yesCallback": (input, extra) => {
																functions.loadData({
																	"dataset": "l_Item_List",
																	"callback": (input, extra) => {
																		functions.userDefined('globalModalHide', {});
																	},
																	"errCallback": null
																});
															},
															"noCallback": null
														});
													},
													"errCallback": null
												});
											},
											"noCallback": (input, extra) => {
												functions.conditional({
													"condition": functions.equal({
														"value1": functions.getVar({
															"var": "vTotalCount",
															"default": ""
														}),
														"value2": functions.getVar({
															"var": "vLoopCount",
															"default": ""
														})
													}),
													"yesValue": "",
													"noValue": "",
													"extra": "",
													"yesCallback": (input, extra) => {
														functions.loadData({});
													},
													"noCallback": null
												});
											}
										});
									}
								});
								functions.conditional({
									"condition": functions.equal({
										"value1": functions.count({
											"values": input
										}),
										"value2": 0
									}),
									"yesValue": "",
									"noValue": "",
									"extra": "",
									"yesCallback": (input, extra) => {
										functions.userDefined('globalModalHide', {});
									},
									"noCallback": null
								});
							},
							"errCallback": (input, extra) => {
								functions.setVar({
									"var": "error",
									"value": input["err"]
								});
								functions.userDefined('globalModalInfo', {
									"title": "Error",
									"message": functions.conditional({
										"condition": functions.getVarAttr({
											"var": "error",
											"attr": "global"
										}),
										"yesValue": functions.getVarAttr({
											"var": "error",
											"attr": "global"
										}),
										"noValue": "Cannot connect to Navision Server."
									})
								});
								functions.console({
									"value": input
								});
							},
							"dataset": "OK365_Item_List"
						});
					}
				});
			},
			"errCallback": null
		});
		functions.setTimeout({
			"timeout": 100,
			"extra": "",
			"callback": (input, extra) => {
				functions.clearData({
					"dataset": "l_Item_List",
					"callback": (input, extra) => {
						functions.loadData({
							"limit": 10,
							"filter": functions.toArray({
								"value1": functions.toObject({
									"f": "Location_Filter",
									"v": functions.objectAttr({
										"object": functions.getVarAttr({
											"var": "var_loginCnt",
											"attr": 0,
											"default": ""
										}),
										"attr": "Location_Code"
									})
								}),
								"value2": "",
								"value3": "",
								"value4": "",
								"value5": "",
								"value6": "",
								"value7": "",
								"value8": "",
								"value9": "",
								"value10": ""
							}),
							"order": "",
							"callback": (input, extra) => {
								functions.setVar({
									"var": "vTotalCount",
									"value": functions.count({
										"values": input
									})
								});
								functions.setVar({
									"var": "vLoopCount",
									"value": 0
								});
								functions.map({
									"values": input,
									"extra": "",
									"callback": (input, extra) => {
										functions.setVar({
											"var": "vLoopCount",
											"value": functions.add({
												"value1": functions.getVar({
													"var": "vLoopCount",
													"default": ""
												}),
												"value2": 1
											})
										});
										functions.selectBy({
											"dataset": "l_Item_List",
											"by": "No",
											"operator": "=",
											"value": input["No"],
											"extra": input,
											"callback": (input, extra) => {
												functions.conditional({
													"condition": functions.equal({
														"value1": functions.count({
															"values": input
														}),
														"value2": 0
													}),
													"yesValue": "",
													"noValue": "",
													"extra": extra,
													"yesCallback": (input, extra) => {
														functions.insert({
															"dataset": "l_Item_List",
															"dt": extra,
															"callback": (input, extra) => {
																functions.conditional({
																	"condition": functions.equal({
																		"value1": functions.getVar({
																			"var": "vTotalCount",
																			"default": ""
																		}),
																		"value2": functions.getVar({
																			"var": "vLoopCount",
																			"default": ""
																		})
																	}),
																	"yesValue": "",
																	"noValue": "",
																	"extra": "",
																	"yesCallback": (input, extra) => {
																		functions.loadData({
																			"dataset": "l_Item_List",
																			"callback": (input, extra) => {
																				functions.userDefined('globalModalHide', {});
																			},
																			"errCallback": null
																		});
																	},
																	"noCallback": null
																});
															},
															"errCallback": null
														});
													},
													"noCallback": (input, extra) => {
														functions.conditional({
															"condition": functions.equal({
																"value1": functions.getVar({
																	"var": "vTotalCount",
																	"default": ""
																}),
																"value2": functions.getVar({
																	"var": "vLoopCount",
																	"default": ""
																})
															}),
															"yesValue": "",
															"noValue": "",
															"extra": "",
															"yesCallback": (input, extra) => {
																functions.loadData({});
															},
															"noCallback": null
														});
													}
												});
											},
											"errCallback": null
										});
									}
								});
								functions.conditional({
									"condition": functions.equal({
										"value1": functions.count({
											"values": input
										}),
										"value2": 0
									}),
									"yesValue": "",
									"noValue": "",
									"extra": "",
									"yesCallback": (input, extra) => {
										functions.userDefined('globalModalHide', {});
									},
									"noCallback": null
								});
							},
							"errCallback": (input, extra) => {
								functions.setVar({
									"var": "error",
									"value": input["err"]
								});
								functions.userDefined('globalModalInfo', {
									"title": "Error",
									"message": functions.conditional({
										"condition": functions.getVarAttr({
											"var": "error",
											"attr": "global"
										}),
										"yesValue": functions.getVarAttr({
											"var": "error",
											"attr": "global"
										}),
										"noValue": "Cannot connect to Navision Server."
									})
								});
								functions.console({
									"value": input
								});
							},
							"dataset": "OK365_Item_List"
						});
					},
					"errCallback": null
				});
			}
		});
	},

	getImgItemValue: data => () => {
		return functions.objectAttr({
			"object": data,
			"attr": "GetPicture"
		});
	},

	getLblItemNameValue: data => () => {
		return functions.concat({
			"string1": data["Description"],
			"string2": "",
			"string3": data["Description_2"],
			"string4": ""
		});
	},

	getLblStockOnHandValue: data => () => {
		return functions.concat({
			"string1": "Avail. stocks: ",
			"string2": functions.formatNumber({
				"value": data["Inventory"],
				"thousandSep": ","
			}),
			"string3": "",
			"string4": ""
		});
	},

	getLblCurrentQtyOrderValue: data => () => {
		return functions.conditional({
			"condition": functions.objectAttr({
				"object": functions.selectBy({
					"dataset": "l_cart",
					"by": "ItemCode",
					"operator": "",
					"value": data["No"],
					"first": true,
					"callback": (input, extra) => {
						return 
					},
					"errCallback": (input, extra) => {
						return 
					}
				}),
				"attr": "Quantity"
			}),
			"yesValue": functions.concat({
				"string1": functions.concat({
					"string1": "<b>",
					"string2": functions.objectAttr({
						"attr": "Quantity",
						"object": functions.selectBy({
							"dataset": "l_cart",
							"by": "ItemCode",
							"value": data["No"],
							"first": true
						})
					}),
					"string3": "</b> "
				}),
				"string2": data["UOM "],
				"string3": "currently added to order.",
				"string4": ""
			}),
			"noValue": "",
			"extra": "",
			"yesCallback": (input, extra) => {
				return 
			},
			"noCallback": (input, extra) => {
				return 
			}
		});
	},

	handlePnlProductBodyRightAction: data => () => {
		functions.setVar({
			"var": "vItemDetails",
			"value": data
		});
		functions.userDefined('globalModalLoading', {});
		functions.loadData({
			"filter": functions.toArray({
				"value1": functions.toObject({
					"f": "Item_No",
					"v": functions.getVarAttr({
						"var": "vItemDetails",
						"attr": "No",
						"default": ""
					})
				}),
				"value2": "",
				"value3": "",
				"value4": "",
				"value5": "",
				"value6": "",
				"value7": "",
				"value8": "",
				"value9": "",
				"value10": ""
			}),
			"callback": (input, extra) => {
				functions.setVar({
					"var": "vUOM",
					"value": input
				});
				functions.setComboOptions({
					"combo": "ComboUOMItemList",
					"comboId": "",
					"data": input,
					"valueField": "Code",
					"valueSeparator": "",
					"displayField": "Code",
					"displaySeparator": ""
				});
				functions.setComponentValue({
					"component": "ComboUOMItemList",
					"componentId": "",
					"value": functions.getVarAttr({
						"var": "vItemDetails",
						"attr": "Base_Unit_of_Measure"
					})
				});
				functions.navCall({
					"ent": "OK365_GetItemSalesPrice",
					"function": "fnGetItemSalesPrice",
					"subfunction": "",
					"data": functions.toObject({
						"currencyCode": functions.getVarAttr({
							"var": "vCurrCustomer",
							"attr": "Currency_Code",
							"default": ""
						}),
						"itemVariantCode": functions.getVarAttr({
							"var": "vItemDetails",
							"attr": "itemVariantCode",
							"default": ""
						}),
						"itemNo": functions.getVarAttr({
							"var": "vItemDetails",
							"attr": "No",
							"default": ""
						}),
						"customerNo": functions.conditional({
							"condition": functions.getVarAttr({
								"var": "vCurrCustomer",
								"attr": "Customer_Code",
								"default": ""
							}),
							"yesValue": functions.getVarAttr({
								"var": "vCurrCustomer",
								"attr": "Customer_Code",
								"default": ""
							}),
							"noValue": functions.getVarAttr({
								"var": "vCurrCustomer",
								"attr": "No",
								"default": ""
							}),
							"extra": "",
							"yesCallback": null,
							"noCallback": null
						}),
						"orderDate": functions.dbDate({}),
						"uOM": functions.componentValue({
							"component": "ComboUOMItemList",
							"componentId": ""
						})
					}),
					"callback": (input, extra) => {
						functions.console({
							"value": functions.objectAttr({
								"object": input,
								"attr": "return_value"
							})
						});
						functions.console({
							"value": input
						});
						functions.setVar({
							"var": "var_itemSalesPriceSelected",
							"value": functions.objectAttr({
								"object": input,
								"attr": "return_value"
							})
						});
						functions.loadData({
							"dataset": "l_cart",
							"filter": functions.toArray({
								"value1": functions.toObject({
									"f": "ItemCode",
									"v": functions.getVarAttr({
										"var": "vItemDetails",
										"attr": "No"
									})
								})
							}),
							"callback": (input, extra) => {
								functions.setVar({
									"var": "vCurrCartItem",
									"value": input
								});
								functions.conditional({
									"condition": functions.greater({
										"value1": functions.count({
											"values": input
										}),
										"value2": 0
									}),
									"yesValue": "",
									"noValue": "",
									"extra": "",
									"yesCallback": (input, extra) => {
										functions.console({
											"value": "yes"
										});
										functions.toArray({
											"value1": functions.setComponentValue({
												"component": "imgUpdateCartImage",
												"value": ""
											}),
											"value2": functions.setComponentValue({
												"component": "lblUpdateCartItemCodeValueIL",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "No"
												})
											}),
											"value3": functions.setComponentValue({
												"component": "lblUpdateCartItemNameValueIL",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "Description"
												})
											}),
											"value4": functions.setComponentValue({
												"component": "lblUpdateCartPriceValueIL",
												"value": functions.concat({
													"string1": "$ ",
													"string2": functions.formatNumber({
														"value": functions.getVar({
															"var": "var_itemSalesPriceSelected"
														}),
														"decimals": 2,
														"decimalSep": ".",
														"thousandSep": ","
													})
												})
											}),
											"value5": functions.setComponentValue({
												"component": "txtQuantity",
												"value": functions.objectAttr({
													"object": functions.getVarAttr({
														"var": "vCurrCartItem",
														"attr": 0
													}),
													"attr": "Quantity"
												})
											}),
											"value6": functions.setComponentElAttr({
												"component": "btnUpdate",
												"attr": "innerText",
												"value": "UPDATE"
											}),
											"value7": "",
											"value8": functions.setComponentValue({
												"component": "lblBtnAdd",
												"value": "UPDATE"
											}),
											"value9": functions.setComponentValue({
												"component": "imgUpdateCartImage",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "GetPicture"
												})
											}),
											"value10": functions.showElement({
												"component": "pnlUpdateCartModalItemListing"
											})
										});
									},
									"noCallback": (input, extra) => {
										functions.console({
											"value": "no"
										});
										functions.toArray({
											"value1": functions.setComponentValue({
												"component": "imgUpdateCartImage",
												"value": ""
											}),
											"value2": functions.setComponentValue({
												"component": "lblUpdateCartItemCodeValueIL",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "No"
												})
											}),
											"value3": functions.setComponentValue({
												"component": "lblUpdateCartItemNameValueIL",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "Description"
												})
											}),
											"value4": functions.setComponentValue({
												"component": "lblUpdateCartPriceValueIL",
												"value": functions.concat({
													"string1": "$ ",
													"string2": functions.formatNumber({
														"decimalSep": ".",
														"thousandSep": ",",
														"value": functions.getVar({
															"var": "var_itemSalesPriceSelected"
														}),
														"decimals": 2
													}),
													"string3": ""
												})
											}),
											"value5": functions.setComponentValue({
												"component": "txtQuantity",
												"value": 0
											}),
											"value6": functions.setComponentElAttr({
												"component": "btnUpdate",
												"attr": "innerText",
												"value": "Add"
											}),
											"value7": "",
											"value8": functions.setComponentValue({
												"component": "lblBtnAdd",
												"value": "ADD"
											}),
											"value9": functions.setComponentValue({
												"component": "imgUpdateCartImage",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "GetPicture"
												})
											}),
											"value10": functions.showElement({
												"component": "pnlUpdateCartModalItemListing"
											})
										});
									}
								});
								functions.userDefined('globalModalHide', {});
							},
							"errCallback": null
						});
					},
					"errCallback": (input, extra) => {
						functions.userDefined('globalModalHide', {});
						functions.setVar({
							"var": "vAction",
							"value": ""
						});
						functions.userDefined('globalModalInfo', {
							"title": "Internal Navision Server Error",
							"message": functions.objectAttr({
								"object": functions.objectAttr({
									"object": input,
									"attr": "err"
								}),
								"attr": "global"
							}),
							"btnCaption": "OK"
						});
						functions.console({
							"value": functions.objectAttr({
								"object": functions.objectAttr({
									"object": input,
									"attr": "err"
								}),
								"attr": "global"
							})
						});
					},
					"connector": "nav",
					"type": ""
				});
			},
			"errCallback": (input, extra) => {
				functions.userDefined('globalModalHide', {});
				functions.setVar({
					"var": "error",
					"value": input["err"]
				});
			},
			"dataset": "OK365_Item_Unit_of_Measure"
		});
	},

	handlePnlMainBody2ScrollBottom: () => {
		functions.userDefined('globalModalLoading', {});
		functions.loadNext({
			"dataset": "OK365_Item_List",
			"datasetDisplay": "",
			"beforeCallback": null
		});
	},

	handlePnlMainBodyButtonsCheckoutAction: () => {
		functions.selectAll({
			"dataset": "l_cart",
			"callback": (input, extra) => {
				functions.conditional({
					"condition": functions.greater({
						"value1": functions.count({
							"values": input
						}),
						"value2": 0
					}),
					"yesValue": "",
					"noValue": "",
					"extra": "",
					"yesCallback": (input, extra) => {
						functions.gotoPage({
							"p": "pgCheckout"
						});
					},
					"noCallback": (input, extra) => {
						functions.userDefined('globalModalInfo', {
							"title": "No Item(s)",
							"message": "There are no item(s) in the cart."
						});
					}
				});
			}
		});
	},

	handleImgUpdateCartCloseAction: () => {
		functions.setComponentValue({
			"component": "txtQuantity",
			"value": ""
		});
		functions.hideElement({
			"component": "pnlUpdateCartModalItemListing"
		});
	},

	handleComboUOMItemListChange: () => {
		functions.loadData({
			"errCallback": (input, extra) => {
				functions.setVar({
					"var": "error",
					"value": input["err"]
				});
				functions.userDefined('globalModalInfo', {
					"title": "Error",
					"message": functions.conditional({
						"condition": functions.getVarAttr({
							"var": "error",
							"attr": "global"
						}),
						"yesValue": functions.getVarAttr({
							"var": "error",
							"attr": "global"
						}),
						"noValue": "Cannot connect to Navision Server."
					})
				});
			},
			"dataset": "MSC_Item_Unit_of_Measure",
			"filter": functions.toArray({
				"value1": functions.toObject({
					"f": "Item_No",
					"o": "=",
					"v": functions.getVarAttr({
						"var": "vItemDetails",
						"attr": "No"
					})
				}),
				"value2": "",
				"value3": "",
				"value4": "",
				"value5": "",
				"value6": "",
				"value7": "",
				"value8": "",
				"value9": "",
				"value10": ""
			}),
			"callback": (input, extra) => {
				functions.setComboOptions({
					"displayField": "Code",
					"displaySeparator": "",
					"combo": "ComboUOMItemList",
					"comboId": "",
					"data": input,
					"valueField": "Code",
					"valueSeparator": ""
				});
				functions.userDefined('globalModalLoading', {});
				functions.navCall({
					"callback": (input, extra) => {
						functions.userDefined('globalModalHide', {});
						functions.console({
							"value": functions.objectAttr({
								"object": input,
								"attr": "return_value"
							})
						});
						functions.console({
							"value": input
						});
						functions.setVar({
							"var": "var_itemSalesPriceSelected",
							"value": functions.objectAttr({
								"object": input,
								"attr": "return_value"
							})
						});
						functions.loadData({
							"dataset": "l_cart",
							"filter": functions.toArray({
								"value1": functions.toObject({
									"f": "ItemCode",
									"o": "=",
									"v": functions.getVarAttr({
										"var": "vItemDetails",
										"attr": "No"
									})
								}),
								"value2": functions.toObject({
									"v": functions.componentValue({
										"component": "ComboUOMItemList"
									}),
									"f": "UOM",
									"o": "="
								})
							}),
							"callback": (input, extra) => {
								functions.setVar({
									"var": "vCurrCartItem",
									"value": input
								});
								functions.conditional({
									"condition": functions.greater({
										"value1": functions.count({
											"values": input
										}),
										"value2": 0
									}),
									"yesValue": "",
									"noValue": "",
									"extra": "",
									"yesCallback": (input, extra) => {
										functions.toArray({
											"value1": functions.setComponentValue({
												"component": "imgUpdateCartImage",
												"value": ""
											}),
											"value2": functions.setComponentValue({
												"component": "lblUpdateCartItemCodeValueIL",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "No"
												})
											}),
											"value3": functions.setComponentValue({
												"component": "lblUpdateCartItemNameValueIL",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "Description"
												})
											}),
											"value4": functions.setComponentValue({
												"component": "lblUpdateCartPriceValueIL",
												"value": functions.concat({
													"string1": "$ ",
													"string2": functions.formatNumber({
														"value": functions.getVar({
															"var": "var_itemSalesPriceSelected"
														}),
														"decimals": 2
													})
												})
											}),
											"value5": functions.setComponentValue({
												"component": "txtQuantity",
												"value": functions.objectAttr({
													"object": functions.getVarAttr({
														"var": "vCurrCartItem",
														"attr": 0
													}),
													"attr": "Quantity"
												})
											}),
											"value6": functions.setComponentElAttr({
												"component": "btnUpdate",
												"attr": "innerText",
												"value": "UPDATE"
											}),
											"value7": functions.showElement({
												"component": "pnlUpdateCartModalItemListing"
											}),
											"value8": "",
											"value9": "",
											"value10": ""
										});
									},
									"noCallback": (input, extra) => {
										functions.toArray({
											"value1": functions.setComponentValue({
												"component": "imgUpdateCartImage",
												"value": ""
											}),
											"value2": functions.setComponentValue({
												"component": "lblUpdateCartItemCodeValueIL",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "No"
												})
											}),
											"value3": functions.setComponentValue({
												"component": "lblUpdateCartItemNameValueIL",
												"value": functions.getVarAttr({
													"var": "vItemDetails",
													"attr": "Description"
												})
											}),
											"value4": functions.setComponentValue({
												"component": "lblUpdateCartPriceValueIL",
												"value": functions.concat({
													"string1": "$ ",
													"string2": functions.formatNumber({
														"decimals": 2,
														"value": functions.getVar({
															"var": "var_itemSalesPriceSelected"
														})
													})
												})
											}),
											"value5": functions.setComponentValue({
												"component": "txtQuantity",
												"value": 0
											}),
											"value6": functions.setComponentElAttr({
												"component": "btnUpdate",
												"attr": "innerText",
												"value": "Add"
											}),
											"value7": functions.showElement({
												"component": "pnlUpdateCartModalItemListing"
											}),
											"value8": "",
											"value9": "",
											"value10": ""
										});
									}
								});
							},
							"errCallback": null
						});
					},
					"errCallback": (input, extra) => {
						functions.userDefined('globalModalInfo', {
							"title": "Error",
							"message": functions.objectAttr({
								"object": input["err"],
								"attr": "global"
							})
						});
					},
					"connector": "nav",
					"type": "",
					"ent": "GetItemSalesPrice",
					"function": "fnGetItemSalesPrice",
					"subfunction": "",
					"data": functions.toObject({
						"uOM": functions.componentValue({
							"component": "ComboUOMItemList",
							"componentId": ""
						}),
						"currencyCode": functions.getVarAttr({
							"var": "vCurrCustomer",
							"attr": "Currency_Code"
						}),
						"itemVariantCode": functions.getVarAttr({
							"var": "vItemDetails",
							"attr": "itemVariantCode",
							"default": ""
						}),
						"itemNo": functions.getVarAttr({
							"var": "vItemDetails",
							"attr": "No"
						}),
						"customerNo": functions.getVarAttr({
							"var": "vCurrCustomer",
							"attr": "No"
						}),
						"orderDate": functions.dbDate({
							"type": ""
						})
					})
				});
			}
		});
	},

	handleImgMinusAction: () => {
		functions.conditional({
			"condition": functions.greater({
				"value1": functions.componentElAttr({
					"component": "txtQuantity",
					"componentId": "",
					"attr": "value"
				}),
				"value2": 1
			}),
			"yesValue": "",
			"noValue": "",
			"extra": "",
			"yesCallback": (input, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"componentId": "",
					"value": functions.sub({
						"value1": functions.componentElAttr({
							"component": "txtQuantity",
							"attr": "value"
						}),
						"value2": 1
					})
				});
			},
			"noCallback": (input, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"value": 0
				});
			}
		});
	},

	handleTxtQuantityAction: () => {
		functions.setComponentFocus({
			"component": "txtQuantity",
			"selectAllText": "true"
		});
	},

	handleImgPlusAction: () => {
		functions.conditional({
			"condition": functions.greater({
				"value1": functions.componentElAttr({
					"component": "txtQuantity",
					"attr": "value"
				}),
				"value2": -1
			}),
			"yesCallback": (input, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"value": functions.add({
						"value1": functions.componentElAttr({
							"component": "txtQuantity",
							"attr": "value"
						}),
						"value2": 1
					})
				});
			},
			"noCallback": (input, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"value": 0
				});
			}
		});
	},

	handlePnlUpdateCartModalBodyButtonItemListing1Action: () => {
		functions.conditional({
			"condition": functions.componentElAttr({
				"component": "txtQuantity",
				"componentId": "",
				"attr": "value"
			}),
			"yesValue": "",
			"noValue": "",
			"extra": "",
			"yesCallback": (input, extra) => {
				functions.conditional({
					"condition": functions.greater({
						"value1": functions.componentElAttr({
							"component": "txtQuantity",
							"componentId": "",
							"attr": "value"
						}),
						"value2": functions.toFloat({
							"value": -1
						})
					}),
					"yesValue": "",
					"noValue": "",
					"extra": "",
					"yesCallback": (input, extra) => {
						functions.conditional({
							"condition": functions.greater({
								"value1": functions.componentElAttr({
									"component": "txtQuantity",
									"attr": "value"
								}),
								"value2": 0
							}),
							"yesValue": "",
							"noValue": "",
							"extra": "",
							"yesCallback": (input, extra) => {
								functions.userDefined('globalModalLoading', {});
								functions.loadData({
									"dataset": "l_cart",
									"filter": functions.toArray({
										"value1": functions.toObject({
											"f": "ItemCode",
											"o": "=",
											"v": functions.getVarAttr({
												"var": "vItemDetails",
												"attr": "No"
											})
										}),
										"value2": functions.toObject({
											"f": "UOM",
											"o": "=",
											"v": functions.componentValue({
												"component": "ComboUOMItemList"
											})
										}),
										"value3": "",
										"value4": "",
										"value5": "",
										"value6": "",
										"value7": "",
										"value8": "",
										"value9": "",
										"value10": ""
									}),
									"callback": (input, extra) => {
										functions.setVar({
											"var": "vCurrCartItem",
											"value": input
										});
										functions.console({
											"value": input
										});
										functions.conditional({
											"condition": functions.greater({
												"value1": functions.count({
													"values": input
												}),
												"value2": 0
											}),
											"yesValue": "",
											"noValue": "",
											"extra": "",
											"yesCallback": (input, extra) => {
												functions.console({
													"value": functions.toArray({
														"value1": functions.toObject({
															"v": functions.objectAttr({
																"object": functions.getVarAttr({
																	"var": "vCurrCartItem",
																	"attr": 0
																}),
																"attr": "ItemCode"
															}),
															"f": "ItemCode",
															"o": "="
														}),
														"value2": functions.toObject({
															"f": "UOM",
															"o": "=",
															"v": functions.componentValue({
																"component": "ComboUOMItemList"
															})
														})
													})
												});
												functions.console({
													"value": functions.toObject({
														"Price": functions.getVar({
															"var": "var_itemSalesPriceSelected",
															"default": ""
														}),
														"Quantity": functions.componentElAttr({
															"component": "txtQuantity",
															"attr": "value"
														})
													})
												});
												functions.updateByMulti({
													"callback": (input, extra) => {
														functions.loadData({
															"callback": (input, extra) => {
																functions.loadData({
																	"dataset": "l_Item_List",
																	"callback": (input, extra) => {
																		functions.userDefined('globalModalHide', {});
																		functions.hideElement({
																			"component": "pnlUpdateCartModalItemListing"
																		});
																	},
																	"errCallback": null
																});
															},
															"errCallback": null,
															"dataset": "l_cart"
														});
													},
													"errCallback": null,
													"dataset": "l_cart",
													"filter": functions.toArray({
														"value1": functions.toObject({
															"f": "ItemCode",
															"o": "=",
															"v": functions.objectAttr({
																"object": functions.getVarAttr({
																	"var": "vCurrCartItem",
																	"attr": 0
																}),
																"attr": "ItemCode"
															})
														}),
														"value2": functions.toObject({
															"o": "=",
															"v": functions.componentValue({
																"component": "ComboUOMItemList"
															}),
															"f": "UOM"
														})
													}),
													"data": functions.toObject({
														"Price": functions.getVar({
															"var": "var_itemSalesPriceSelected",
															"default": ""
														}),
														"Quantity": functions.componentElAttr({
															"component": "txtQuantity",
															"attr": "value"
														})
													})
												});
											},
											"noCallback": (input, extra) => {
												functions.insert({
													"dataset": "l_cart",
													"dt": functions.toObject({
														"Price": functions.getVar({
															"var": "var_itemSalesPriceSelected",
															"default": ""
														}),
														"Quantity": functions.componentElAttr({
															"component": "txtQuantity",
															"attr": "value"
														}),
														"UOM": functions.componentValue({
															"component": "ComboUOMItemList"
														}),
														"VAT_Prod_Posting_Group": functions.getVarAttr({
															"var": "vItemDetails",
															"attr": "VAT_Prod_Posting_Group"
														}),
														"GetPicture": functions.getVarAttr({
															"var": "vItemDetails",
															"attr": "GetPicture"
														}),
														"ItemCode": functions.getVarAttr({
															"var": "vItemDetails",
															"attr": "No"
														}),
														"ItemName": functions.getVarAttr({
															"var": "vItemDetails",
															"attr": "Description"
														})
													}),
													"callback": null,
													"errCallback": null
												});
												functions.loadData({
													"errCallback": null,
													"dataset": "l_cart",
													"callback": null
												});
												functions.console({
													"value": "b"
												});
												functions.loadData({
													"callback": (input, extra) => {
														functions.userDefined('globalModalHide', {});
														functions.hideElement({
															"component": "pnlUpdateCartModalItemListing"
														});
													},
													"errCallback": null,
													"dataset": "l_Item_List"
												});
											}
										});
									},
									"errCallback": null
								});
							},
							"noCallback": null
						});
					},
					"noCallback": (input, extra) => {
						functions.userDefined('globalModalInfo', {
							"title": "Invalid Value",
							"message": "Please enter a valid value/amount."
						});
					}
				});
			},
			"noCallback": (input, extra) => {
				functions.userDefined('globalModalInfo', {
					"title": "No Value",
					"message": "Enter the quantity."
				});
			}
		});
	},

	handleBtnModalPositiveAction: () => {
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "action",
					"default": ""
				}),
				"value2": "exit_application"
			}),
			"yesValue": "",
			"noValue": "",
			"extra": "",
			"yesCallback": (input, extra) => {
				functions.setVar({
					"var": "action",
					"value": ""
				});
				functions.exitApp({});
			},
			"noCallback": null
		});
		functions.userDefined('globalModalHide', {});
	},

	handleBtnModalNegativeAction: () => {
		functions.setVar({
			"var": "action",
			"value": ""
		});
		functions.toArray({
			"value1": functions.setComponentValue({
				"component": "lblModalTitle",
				"componentId": "",
				"value": "Loading"
			}),
			"value2": functions.hideElement({
				"component": "pnlModalBodyTitle",
				"componentId": ""
			}),
			"value3": functions.setComponentValue({
				"component": "lblModalMessage",
				"componentId": "",
				"value": "Loading ..."
			}),
			"value4": functions.hideElement({
				"component": "pnlModalBodyMessage",
				"componentId": ""
			}),
			"value5": functions.hideElement({
				"component": "pnlModalBodyLoading",
				"componentId": ""
			}),
			"value6": functions.setComponentElAttr({
				"component": "btnModalPositive",
				"componentId": "",
				"attr": "innerText",
				"value": "POSITIVE"
			}),
			"value7": functions.hideElement({
				"component": "pnlModalBodyButtonPositive",
				"componentId": ""
			}),
			"value8": functions.setComponentElAttr({
				"component": "btnModalNegative",
				"componentId": "",
				"attr": "innerText",
				"value": "NEGATIVE"
			}),
			"value9": functions.hideElement({
				"component": "pnlModalBodyButtonNegative",
				"componentId": ""
			}),
			"value10": functions.toArray({
				"value1": functions.setObjectAttr({
					"object": functions.componentElAttr({
						"component": "lblModalMessage",
						"attr": "style"
					}),
					"attr": "word-wrap",
					"value": "break-word"
				}),
				"value2": functions.setObjectAttr({
					"object": functions.componentElAttr({
						"component": "pnlModal",
						"componentId": "",
						"attr": "style"
					}),
					"attr": "display",
					"value": "none"
				}),
				"value3": "",
				"value4": "",
				"value5": "",
				"value6": "",
				"value7": "",
				"value8": "",
				"value9": "",
				"value10": ""
			})
		});
	},

	handlePgItemListingLoad: () => {
		functions.onBackButton({
			"callback": (input, extra) => {
				functions.gotoPage({
					"p": "pgMainMenu"
				});
			}
		});
		functions.setVar({
			"var": "vAction",
			"value": ""
		});
		functions.userDefined('globalModalLoading', {});
		functions.setTimeout({
			"timeout": 100,
			"extra": "",
			"callback": (input, extra) => {
				functions.clearData({
					"dataset": "l_Item_List",
					"callback": (input, extra) => {
						functions.loadData({
							"filter": functions.toArray({
								"value1": functions.toObject({
									"v": functions.objectAttr({
										"object": functions.getVarAttr({
											"var": "var_loginCnt",
											"attr": 0,
											"default": ""
										}),
										"attr": "Location_Code"
									}),
									"f": "Location_Filter"
								}),
								"value2": "",
								"value3": "",
								"value4": "",
								"value5": "",
								"value6": "",
								"value7": "",
								"value8": "",
								"value9": "",
								"value10": ""
							}),
							"order": "",
							"callback": (input, extra) => {
								functions.setVar({
									"var": "vTotalCount",
									"value": functions.count({
										"values": input
									})
								});
								functions.setVar({
									"var": "vLoopCount",
									"value": 0
								});
								functions.map({
									"values": input,
									"extra": "",
									"callback": (input, extra) => {
										functions.setVar({
											"var": "vLoopCount",
											"value": functions.add({
												"value1": functions.getVar({
													"var": "vLoopCount",
													"default": ""
												}),
												"value2": 1
											})
										});
										functions.selectBy({
											"dataset": "l_Item_List",
											"by": "No",
											"operator": "=",
											"value": input["No"],
											"extra": input,
											"callback": (input, extra) => {
												functions.conditional({
													"condition": functions.equal({
														"value1": functions.count({
															"values": input
														}),
														"value2": 0
													}),
													"yesValue": "",
													"noValue": "",
													"extra": extra,
													"yesCallback": (input, extra) => {
														functions.insert({
															"dataset": "l_Item_List",
															"dt": extra,
															"callback": (input, extra) => {
																functions.conditional({
																	"condition": functions.equal({
																		"value1": functions.getVar({
																			"var": "vTotalCount",
																			"default": ""
																		}),
																		"value2": functions.getVar({
																			"var": "vLoopCount",
																			"default": ""
																		})
																	}),
																	"yesValue": "",
																	"noValue": "",
																	"extra": "",
																	"yesCallback": (input, extra) => {
																		functions.loadData({
																			"dataset": "l_Item_List",
																			"callback": (input, extra) => {
																				functions.userDefined('globalModalHide', {});
																			},
																			"errCallback": null
																		});
																	},
																	"noCallback": null
																});
															},
															"errCallback": null
														});
													},
													"noCallback": (input, extra) => {
														functions.conditional({
															"condition": functions.equal({
																"value1": functions.getVar({
																	"var": "vTotalCount",
																	"default": ""
																}),
																"value2": functions.getVar({
																	"var": "vLoopCount",
																	"default": ""
																})
															}),
															"yesValue": "",
															"noValue": "",
															"extra": "",
															"yesCallback": (input, extra) => {
																functions.loadData({});
															},
															"noCallback": null
														});
													}
												});
											},
											"errCallback": null
										});
									}
								});
								functions.conditional({
									"condition": functions.equal({
										"value1": functions.count({
											"values": input
										}),
										"value2": 0
									}),
									"yesValue": "",
									"noValue": "",
									"extra": "",
									"yesCallback": (input, extra) => {
										functions.userDefined('globalModalHide', {});
									},
									"noCallback": null
								});
							},
							"errCallback": (input, extra) => {
								functions.setVar({
									"var": "error",
									"value": input["err"]
								});
								functions.userDefined('globalModalInfo', {
									"title": "Error",
									"message": functions.conditional({
										"condition": functions.getVarAttr({
											"var": "error",
											"attr": "global"
										}),
										"yesValue": functions.getVarAttr({
											"var": "error",
											"attr": "global"
										}),
										"noValue": "Cannot connect to Navision Server."
									})
								});
								functions.console({
									"value": input
								});
							},
							"dataset": "OK365_Item_List",
							"limit": 10
						});
					},
					"errCallback": null
				});
			}
		});
	}
};
functions.pageActions = actions
// list template to be rendered for datalists
const templates = {
    dlProducts: ({ item, parentStyle, currentTime }) => (													
		<Panel data={item.pnlProduct} functions={functions} currentTime={currentTime} parentStyle={parentStyle}>
			<Panel data={item.pnlProductBody} currentTime={currentTime} functions={functions} parentStyle={parentStyle}>
				<Panel data={item.pnlProductBodyLeft} functions={functions} currentTime={currentTime} parentStyle={parentStyle}>
					<Image data={item.imgItem} functions={functions} currentTime={currentTime} value={actions.getImgItemValue} value_data={item._data} parentStyle={parentStyle}/>
				</Panel>
				<Panel data={item.pnlProductBodyMiddle} currentTime={currentTime} functions={functions} parentStyle={parentStyle}>
					<Label data={item.lblItemCode} functions={functions} currentTime={currentTime} parentStyle={parentStyle}/>
					<Label data={item.lblItemName} functions={functions} currentTime={currentTime} value={actions.getLblItemNameValue} value_data={item._data} parentStyle={parentStyle}/>
					<Label data={item.lblStockOnHand} functions={functions} currentTime={currentTime} value={actions.getLblStockOnHandValue} value_data={item._data} parentStyle={parentStyle}/>
					<Label data={item.lblCurrentQtyOrder} currentTime={currentTime} value={actions.getLblCurrentQtyOrderValue} value_data={item._data} functions={functions} parentStyle={parentStyle}/>
				</Panel>
				<Panel data={item.pnlProductBodyRight} action={actions.handlePnlProductBodyRightAction(item._data)} functions={functions} currentTime={currentTime} parentStyle={parentStyle}>
					<Image data={item.imgAddItem} functions={functions} currentTime={currentTime} parentStyle={parentStyle}/>
				</Panel>
			</Panel>
		</Panel>
	)
}

const PgItemListing = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;

    // Manipulate the props to support css styles
    formatParentStyleLayout(props);

    return (
		<Page data={props.pgItemListing} load={actions.handlePgItemListingLoad} currentTime={currentTime}>
			<Panel data={props.pnlHolder} functions={functions} currentTime={currentTime}>
				<Panel data={props.pnlMain} functions={functions} currentTime={currentTime}>
					<Panel data={props.pnlMainHeader} functions={functions} currentTime={currentTime}>
						<Panel data={props.pnlHeaderLeft} action={actions.handlePnlHeaderLeftAction} currentTime={currentTime} functions={functions}>
							<Image data={props.imgHeader} currentTime={currentTime} functions={functions}/>
						</Panel>
						<Panel data={props.pnlHeaderMiddle} functions={functions} currentTime={currentTime}>
							<Label data={props.lblMainTitle} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlHeaderRight} functions={functions} currentTime={currentTime}/>
					</Panel>
					<Panel data={props.pnlMainBody} functions={functions} currentTime={currentTime}>
						<Panel data={props.pnlMainBodySearch} functions={functions} currentTime={currentTime}>
							<Edit data={props.txtSearch} change={actions.handleTxtSearchChange} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlMainBody2} scrollBottom={actions.handlePnlMainBody2ScrollBottom} functions={functions} currentTime={currentTime}>
							<DataList data={props.dlProducts} functions={functions} currentTime={currentTime} template={templates.dlProducts} />
								{/* {({ item, parentStyle }) => (							
									<Panel data={item.pnlProduct} functions={functions} currentTime={currentTime} parentStyle={parentStyle}>
										<Panel data={item.pnlProductBody} currentTime={currentTime} functions={functions} parentStyle={parentStyle}>
											<Panel data={item.pnlProductBodyLeft} functions={functions} currentTime={currentTime} parentStyle={parentStyle}>
												<Image data={item.imgItem} functions={functions} currentTime={currentTime} value={actions.getImgItemValue} value_data={item._data} parentStyle={parentStyle}/>
											</Panel>
											<Panel data={item.pnlProductBodyMiddle} currentTime={currentTime} functions={functions} parentStyle={parentStyle}>
												<Label data={item.lblItemCode} functions={functions} currentTime={currentTime} parentStyle={parentStyle}/>
												<Label data={item.lblItemName} functions={functions} currentTime={currentTime} value={actions.getLblItemNameValue} value_data={item._data} parentStyle={parentStyle}/>
												<Label data={item.lblStockOnHand} functions={functions} currentTime={currentTime} value={actions.getLblStockOnHandValue} value_data={item._data} parentStyle={parentStyle}/>
												<Label data={item.lblCurrentQtyOrder} currentTime={currentTime} value={actions.getLblCurrentQtyOrderValue} value_data={item._data} functions={functions} parentStyle={parentStyle}/>
											</Panel>
											<Panel data={item.pnlProductBodyRight} action={actions.handlePnlProductBodyRightAction(item._data)} functions={functions} currentTime={currentTime} parentStyle={parentStyle}>
												<Image data={item.imgAddItem} functions={functions} currentTime={currentTime} parentStyle={parentStyle}/>
											</Panel>
										</Panel>
									</Panel>
								)}
							</DataList> */}
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBodyButtonsCheckout} action={actions.handlePnlMainBodyButtonsCheckoutAction} functions={functions} currentTime={currentTime}>
						<Label data={props.lblBtnCheckout} functions={functions} currentTime={currentTime}/>
					</Panel>
				</Panel>
				<Panel data={props.pnlUpdateCartModalItemListing} functions={functions} currentTime={currentTime}>
					<Panel data={props.pnlUpdateCartModalTableItemListing} functions={functions} currentTime={currentTime}>
						<Panel data={props.pnlUpdateCartModalCellItemListing} functions={functions} currentTime={currentTime}>
							<Panel data={props.pnlUpdateCartModalBodyItemListing} currentTime={currentTime} functions={functions}>
								<Panel data={props.Panel459} functions={functions} currentTime={currentTime}>
									<Panel data={props.pnlUpdateCartModalBodyItemListingImage} functions={functions} currentTime={currentTime}>
										<Image data={props.imgUpdateCartImage} functions={functions} currentTime={currentTime}/>
									</Panel>
									<Image data={props.imgUpdateCartClose} action={actions.handleImgUpdateCartCloseAction} functions={functions} currentTime={currentTime}/>
									<Label data={props.lblUpdateCartItemCodeValueIL} functions={functions} currentTime={currentTime}/>
									<Label data={props.lblUpdateCartItemNameValueIL} functions={functions} currentTime={currentTime}/>
									<Label data={props.lblUpdateCartPriceValueIL} functions={functions} currentTime={currentTime}/>
								</Panel>
								<Panel data={props.Panel789} currentTime={currentTime} functions={functions}>
									<Panel data={props.pnlUpdateCartModalBodyItemListingUOM} functions={functions} currentTime={currentTime}>
										<Panel data={props.Panel375} functions={functions} currentTime={currentTime}>
											<Label data={props.lblUpdateCartQuantityIL386S} functions={functions} currentTime={currentTime}/>
										</Panel>
										<ComboBox data={props.ComboUOMItemList} change={actions.handleComboUOMItemListChange} currentTime={currentTime} functions={functions}/>
									</Panel>
									<Panel data={props.pnlUpdateCartModalBodyItemListingQty} functions={functions} currentTime={currentTime}>
										<Panel data={props.pnlUpdateCartModalBodyQtyValueOD} functions={functions} currentTime={currentTime}>
											<Panel data={props.pnlUpdateCartModalBodyLeft} currentTime={currentTime} functions={functions}>
												<Image data={props.imgMinus} action={actions.handleImgMinusAction} functions={functions} currentTime={currentTime}/>
											</Panel>
											<Panel data={props.pnlUpdateCartModalBodyMiddle} functions={functions} currentTime={currentTime}>
												<Edit data={props.txtQuantity} action={actions.handleTxtQuantityAction} functions={functions} currentTime={currentTime}/>
											</Panel>
											<Panel data={props.pnlUpdateCartModalBodyRight} functions={functions} currentTime={currentTime}>
												<Image data={props.imgPlus} action={actions.handleImgPlusAction} currentTime={currentTime} functions={functions}/>
											</Panel>
										</Panel>
									</Panel>
									<Panel data={props.pnlUpdateCartModalBodyQtyValueIL} functions={functions} currentTime={currentTime}>
										<Label data={props.lblUpdateCartQuantityIL} functions={functions} currentTime={currentTime}/>
									</Panel>
								</Panel>
								<Panel data={props.pnlUpdateCartModalBodyButtonItemListing1} action={actions.handlePnlUpdateCartModalBodyButtonItemListing1Action} currentTime={currentTime} functions={functions}>
									<Label data={props.lblBtnAdd} functions={functions} currentTime={currentTime}/>
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal} functions={functions} currentTime={currentTime}>
					<Panel data={props.pnlModalBody} functions={functions} currentTime={currentTime}>
						<Panel data={props.pnlModalBodyTitle} functions={functions} currentTime={currentTime}>
							<Label data={props.lblModalTitle} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlModalBodyMessage} currentTime={currentTime} functions={functions}>
							<Label data={props.lblModalMessage} currentTime={currentTime} functions={functions}/>
						</Panel>
						<Panel data={props.pnlModalBodyLoading} functions={functions} currentTime={currentTime}>
							<Image data={props.imgModalLoading} currentTime={currentTime} functions={functions}/>
						</Panel>
						<Panel data={props.pnlModalBodyButtonPositive} functions={functions} currentTime={currentTime}>
							<Button data={props.btnModalPositive} action={actions.handleBtnModalPositiveAction} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlModalBodyButtonNegative} functions={functions} currentTime={currentTime}>
							<Button data={props.btnModalNegative} action={actions.handleBtnModalNegativeAction} functions={functions} currentTime={currentTime}/>
						</Panel>
					</Panel>
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgItemListing);