import * as React from 'react';
import { Text, View, Image, StyleSheet, Platform } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Theme } from '../dependency/style/Flat'; 
import PgHome from './PgStreamHome'; 
import PgExplore from './PgStreamExplore';
import PgVideo from './PgStreamVideo';
import PgFavorites from './PgStreamFavorites';
import PgProfile from './PgStreamProfile';
 

const iconHome = require('../asset/public/stream/tabbar/home.png');
const iconExplore = require('../asset/public/stream/tabbar/explore.png');
const iconVideo = require('../asset/public/stream/tabbar/video.png');
const iconFavorites = require('../asset/public/stream/tabbar/favorites.png');
const iconProfile = require('../asset/public/stream/tabbar/profile.png');

const NavTabData = [
  {
    name: 'Home',
    component: PgHome,
    icon: iconHome,
  },
  {
    name: 'Explore',
    component: PgExplore,
    icon: iconExplore,
  },
  {
    name: 'Video',
    component: PgVideo,
    icon: iconVideo,
  },
  {
    name: 'Favs',
    component: PgFavorites,
    icon: iconFavorites,
  },
  {
    name: 'Profile',
    component: PgProfile,
    icon: iconProfile,
  },
];

const Coloring = {
  primary: '#555CC4',
  grey: '#acacac',
};

const Tab = createBottomTabNavigator();

export default function BottomTabs() {
    return (
      <Tab.Navigator screenOptions={{tabBarStyle: { display: 'flex', height: Platform.OS === 'ios' ? 90 : 50 },}}>
        {NavTabData.map((item, idx) => (
          <Tab.Screen 
            key={`tab_item${idx+1}`}
            name={item.name}
            component={item.component}
            options={{
            tabBarIcon: ({ focused }) => (
              <View style={styles.tabBarItemContainer}>
                <Image
                  resizeMode="contain"
                  source={item.icon}
                  style={[styles.tabBarIcon, focused && styles.tabBarIconFocused]}
                />
              </View>
            ),
            tabBarLabel: ({ focused }) => <Text style={{ fontSize: 12, color: focused ? Coloring.primary : Coloring.grey}}>{item.name}</Text>,
          }}
          />        
        ))}
      </Tab.Navigator>
    );
  };
  
  const styles = StyleSheet.create({
    tabBarItemContainer: {
      alignItems: 'center',
      justifyContent: 'center',
      borderBottomWidth: 2,
      borderBottomColor: '#ffffff',
      paddingHorizontal: 10,
      bottom: Platform.OS === 'ios' ? -5 : 0,
    },
    tabBarIcon: {
      width: 23,
      height: 23,
    },
    tabBarIconFocused: {
      tintColor: Coloring.primary,
    },
  });