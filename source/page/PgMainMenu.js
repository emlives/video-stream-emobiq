import React from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgMainMenu';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import ComboBox from '../framework/component/ComboBox';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handlePnlHeaderRightAction: () => {
		functions.userDefined('globalLogout', {});
	},

	handlePnlMainBody1LeftAction: () => {
		functions.setVar({
			"var": "v_customer_list_action",
			"value": "Item Listing"
		});
		functions.setVar({
			"var": "var_route_selected",
			"value": functions.componentElAttr({
				"component": "cmbRoute",
				"componentId": "",
				"attr": "value"
			})
		});
		functions.gotoPage({
			"p": "pgCustomerListing"
		});
	},

	handlePnlMainBody1Left817Action: () => {
		functions.gotoPage({
			"p": "pgOrderList"
		});
	},

	handlePnlMainBody1Right3Action: () => {
		functions.conditional({
			"condition": functions.getVar({
				"var": "vCurrCustomer"
			}),
			"yesCallback": (input, extra) => {
				functions.setVar({
					"var": "var_route_selected",
					"value": functions.componentElAttr({
						"component": "cmbRoute",
						"attr": "value"
					})
				});
				functions.gotoPage({
					"p": "pgSalesReturnLists"
				});
			},
			"noCallback": (input, extra) => {
				functions.userDefined('globalModalInfo', {
					"message": "Please select a customer to continue.",
					"title": "No Customer"
				});
				functions.setVar({
					"var": "vAction",
					"value": "NoCustomer"
				});
			}
		});
	},

	handlePnlMainBody1Right3553423Action: () => {
		functions.gotoPage({
			"p": "pgSettings"
		});
	},

	handlePnlMainBody1Right3553423898Action: () => {
		functions.gotoPage({
			"p": "pgTest"
		});
	},

	handlePnlModalBodyButtonPositiveAction: () => {
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "vAction",
					"default": ""
				}),
				"value2": "NoCustomer"
			}),
			"yesValue": "",
			"noValue": "",
			"extra": "",
			"yesCallback": (input, extra) => {
				functions.userDefined('globalModalHide', {});
			},
			"noCallback": (input, extra) => {
				functions.conditional({
					"condition": functions.equal({
						"value1": functions.getVar({
							"var": "vAction",
							"default": ""
						}),
						"value2": "logout"
					}),
					"yesValue": "",
					"noValue": "",
					"extra": "",
					"yesCallback": (input, extra) => {
						functions.clearAllVar({});
						functions.setVar({
							"var": "vAction",
							"value": ""
						});
						functions.gotoPage({
							"p": "pgLogin"
						});
					},
					"noCallback": (input, extra) => {
						functions.setVar({
							"var": "vAction",
							"value": ""
						});
						functions.userDefined('globalModalHide', {});
					}
				});
			}
		});
	},

	handlePnlModalBodyButtonNegativeAction: () => {
		functions.setVar({
			"var": "vAction",
			"value": ""
		});
		functions.userDefined('globalModalHide', {});
	},

	handlePgMainMenuLoad: () => {
		functions.onBackButton({
			"callback": (input, extra) => {
				functions.userDefined('globalLogout', {});
			}
		});
		functions.setVar({
			"var": "vAction",
			"value": ""
		});
		functions.userDefined('LoginAs', {});
		functions.setComponentValue({
			"component": "lblCustomer",
			"componentId": "",
			"value": functions.getVarAttr({
				"var": "vCurrCustomerDetails",
				"attr": "Name",
				"default": ""
			})
		});
		functions.conditional({
			"condition": functions.getVarAttr({
				"var": "vCurrCustomerDetails",
				"attr": "No",
				"default": ""
			}),
			"yesValue": "",
			"noValue": "",
			"extra": "",
			"yesCallback": (input, extra) => {
				functions.toArray({
					"value1": functions.setComponentValue({
						"component": "lblMainCustCode",
						"componentId": "",
						"value": functions.getVarAttr({
							"var": "vCurrCustomerDetails",
							"attr": "No",
							"default": ""
						})
					}),
					"value2": functions.setComponentValue({
						"component": "lblMainCustName",
						"componentId": "",
						"value": functions.getVarAttr({
							"var": "vCurrCustomerDetails",
							"attr": "Name",
							"default": ""
						})
					}),
					"value3": functions.setComponentValue({
						"component": "lblMainCustAddress",
						"componentId": "",
						"value": functions.getVarAttr({
							"var": "vCurrCustomerDetails",
							"attr": "Address",
							"default": ""
						})
					}),
					"value4": "",
					"value5": "",
					"value6": "",
					"value7": "",
					"value8": "",
					"value9": "",
					"value10": ""
				});
			},
			"noCallback": (input, extra) => {
				functions.setComponentValue({
					"component": "lblMainCustCode",
					"componentId": "",
					"value": "No customer selected."
				});
			}
		});
		functions.userDefined('globalModalLoading', {});
		functions.selectBy({
			"dataset": "l_Route_Master",
			"by": "_id",
			"operator": "",
			"value": 1,
			"first": true,
			"callback": (input, extra) => {
				functions.setVar({
					"var": "v_current_route_code",
					"value": functions.objectAttr({
						"object": input,
						"attr": "Route_Code"
					})
				});
				functions.loadData({
					"dataset": "OK365_Location_List",
					"callback": (input, extra) => {
						functions.setComboOptions({
							"combo": "cmbRoute",
							"comboId": "",
							"data": input,
							"valueField": "Code",
							"valueSeparator": "",
							"displayField": "Name",
							"displaySeparator": ""
						});
						functions.setObjectAttr({
							"object": null,
							"attr": "disabled",
							"value": "true"
						});
					},
					"errCallback": (input, extra) => {
						functions.setVar({
							"var": "error",
							"value": input["err"]
						});
					}
				});
				functions.loadData({
					"dataset": "OK365_Route_Master",
					"filter": functions.toArray({
						"value1": functions.toObject({
							"f": "Route_Code",
							"v": functions.objectAttr({
								"attr": "Route_Card_no",
								"object": functions.getVarAttr({
									"var": "var_loginCnt",
									"attr": 0,
									"default": ""
								})
							})
						}),
						"value2": "",
						"value3": "",
						"value4": "",
						"value5": "",
						"value6": "",
						"value7": "",
						"value8": "",
						"value9": "",
						"value10": ""
					}),
					"callback": (input, extra) => {
						functions.setVar({
							"var": "vRouteMasterData",
							"value": input
						});
						functions.setVar({
							"var": "vRouteMaster",
							"value": functions.objectAttr({
								"object": input,
								"attr": 0
							})
						});
						functions.dataFromString({
							"dataset": "l_Route_Master",
							"string": functions.getVar({
								"var": "vRouteMasterData"
							}),
							"callback": null,
							"errCallback": null
						});
						functions.setComponentValue({
							"component": "cmbRoute",
							"componentId": "",
							"value": functions.objectAttr({
								"object": functions.objectAttr({
									"object": input,
									"attr": 0
								}),
								"attr": "Location_Code"
							})
						});
						functions.setObjectAttr({
							"object": functions.getVarAttr({
								"var": "var_loginCnt",
								"attr": 0,
								"default": ""
							}),
							"attr": "Location_Code",
							"value": functions.objectAttr({
								"object": functions.objectAttr({
									"object": input,
									"attr": 0
								}),
								"attr": "Location_Code"
							})
						});
						functions.setComponentValue({
							"component": "lblTitle",
							"componentId": "",
							"value": functions.concat({
								"string1": functions.getVarAttr({
									"var": "vRouteMaster",
									"attr": "Location_Code",
									"default": ""
								}),
								"string2": " - ",
								"string3": functions.getVarAttr({
									"var": "vRouteMaster",
									"attr": "Description",
									"default": ""
								}),
								"string4": ""
							})
						});
						functions.userDefined('globalModalHide', {});
					},
					"errCallback": (input, extra) => {
						functions.userDefined('globalModalHide', {});
					}
				});
			},
			"errCallback": null
		});
	}
};

const PgMainMenu = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;

    // Manipulate the props to support css styles
    formatParentStyleLayout(props);

    return (
		<Page data={props.pgMainMenu} load={actions.handlePgMainMenuLoad} currentTime={currentTime}>
			<Panel data={props.pnlHolder} functions={functions} currentTime={currentTime}>
				<Panel data={props.pnlMain} functions={functions} currentTime={currentTime}>
					<Panel data={props.pnlMainHeader} functions={functions} currentTime={currentTime}>
						<Panel data={props.pnlHeaderLeft} currentTime={currentTime} functions={functions}>
							<Image data={props.Image484} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlHeaderMiddle} functions={functions} currentTime={currentTime}>
							<Label data={props.lblTitle} functions={functions} currentTime={currentTime}/>
							<Label data={props.lblLoginAs} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlHeaderRight} action={actions.handlePnlHeaderRightAction} functions={functions} currentTime={currentTime}>
							<Image data={props.Image215} functions={functions} currentTime={currentTime}/>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBody} functions={functions} currentTime={currentTime}>
						<Panel data={props.pnlMainBodyCustomer} currentTime={currentTime} functions={functions}>
							<Label data={props.lblPONumber383} functions={functions} currentTime={currentTime}/>
							<Label data={props.lblMainCustCode} currentTime={currentTime} functions={functions}/>
							<ComboBox data={props.cmbRoute} functions={functions} currentTime={currentTime}/>
							<Label data={props.lblMainCustName} currentTime={currentTime} functions={functions}/>
							<Label data={props.lblMainCustAddress} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlMainBody1} functions={functions} currentTime={currentTime}>
							<Panel data={props.pnlMainBody1Left} action={actions.handlePnlMainBody1LeftAction} functions={functions} currentTime={currentTime}>
								<Image data={props.Image834} functions={functions} currentTime={currentTime}/>
								<Label data={props.Label869} functions={functions} currentTime={currentTime}/>
							</Panel>
							<Panel data={props.pnlMainBody1Right} functions={functions} currentTime={currentTime}>
								<Image data={props.Image685} functions={functions} currentTime={currentTime}/>
								<Label data={props.Label856} functions={functions} currentTime={currentTime}/>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainBody2} functions={functions} currentTime={currentTime}>
							<Panel data={props.pnlMainBody1Left817} action={actions.handlePnlMainBody1Left817Action} functions={functions} currentTime={currentTime}>
								<Image data={props.Image834936} functions={functions} currentTime={currentTime}/>
								<Label data={props.Label869825} functions={functions} currentTime={currentTime}/>
							</Panel>
							<Panel data={props.pnlMainBody1Right3} action={actions.handlePnlMainBody1Right3Action} functions={functions} currentTime={currentTime}>
								<Image data={props.Image685644} functions={functions} currentTime={currentTime}/>
								<Label data={props.Label856312} currentTime={currentTime} functions={functions}/>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainBody3} functions={functions} currentTime={currentTime}>
							<Panel data={props.pnlMainBody1Left817519} functions={functions} currentTime={currentTime}>
								<Image data={props.Image834936852} functions={functions} currentTime={currentTime}/>
								<Label data={props.Label8698259} functions={functions} currentTime={currentTime}/>
							</Panel>
							<Panel data={props.pnlMainBody1Right355} currentTime={currentTime} functions={functions}>
								<Image data={props.Image685644520} functions={functions} currentTime={currentTime}/>
								<Label data={props.Label856312352} functions={functions} currentTime={currentTime}/>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainBody4} functions={functions} currentTime={currentTime}>
							<Panel data={props.pnlMainBody1Left817519933} functions={functions} currentTime={currentTime}>
								<Image data={props.Image834936852715} functions={functions} currentTime={currentTime}/>
								<Label data={props.Label8698259147} functions={functions} currentTime={currentTime}/>
							</Panel>
							<Panel data={props.pnlMainBody1Right35534} functions={functions} currentTime={currentTime}>
								<Image data={props.Image685644520284} functions={functions} currentTime={currentTime}/>
								<Label data={props.Label856312352601} functions={functions} currentTime={currentTime}/>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainBody5} functions={functions} currentTime={currentTime}>
							<Panel data={props.pnlMainBody1Left817519933542} functions={functions} currentTime={currentTime}>
								<Image data={props.Image834936852715440} functions={functions} currentTime={currentTime}/>
								<Label data={props.Label8698259147853} functions={functions} currentTime={currentTime}/>
							</Panel>
							<Panel data={props.pnlMainBody1Right3553423} action={actions.handlePnlMainBody1Right3553423Action} functions={functions} currentTime={currentTime}>
								<Image data={props.Image685644520284730} functions={functions} currentTime={currentTime}/>
								<Label data={props.Label856312352601777} functions={functions} currentTime={currentTime}/>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainBody6} functions={functions} currentTime={currentTime}>
							<Panel data={props.pnlMainBody1Left817519933542897} currentTime={currentTime} functions={functions}>
								<Image data={props.Image834936852715440208} currentTime={currentTime} functions={functions}/>
								<Label data={props.Label8698259147853876} functions={functions} currentTime={currentTime}/>
							</Panel>
							<Panel data={props.pnlMainBody1Right3553423898} action={actions.handlePnlMainBody1Right3553423898Action} functions={functions} currentTime={currentTime}>
								<Panel data={props.Panel231} currentTime={currentTime} functions={functions}/>
								<Label data={props.Label856312352601777707} currentTime={currentTime} functions={functions}/>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal} currentTime={currentTime} functions={functions}>
					<Panel data={props.pnlModalBodyMain} functions={functions} currentTime={currentTime}>
						<Panel data={props.pnlModalBodyTitle} functions={functions} currentTime={currentTime}>
							<Label data={props.lblModalTitle} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlModalBodyMessage} functions={functions} currentTime={currentTime}>
							<Label data={props.lblModalMessage} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlModalBodyLoading} functions={functions} currentTime={currentTime}>
							<Image data={props.imgModalLoading} functions={functions} currentTime={currentTime}/>
						</Panel>
					</Panel>
					<Panel data={props.pnlModalBodyButtons} functions={functions} currentTime={currentTime}>
						<Panel data={props.pnlModalBodyButtonPositive} action={actions.handlePnlModalBodyButtonPositiveAction} currentTime={currentTime} functions={functions}>
							<Label data={props.lblModalPositive} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlModalBodyButtonNegative} action={actions.handlePnlModalBodyButtonNegativeAction} functions={functions} currentTime={currentTime}>
							<Label data={props.lblModalNegative} functions={functions} currentTime={currentTime}/>
						</Panel>
					</Panel>
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgMainMenu);