import React from 'react';
import { connect } from 'react-redux';

import Page from '../framework/component/Page';
import Label from '../framework/component/Label';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import ComboBox from '../framework/component/ComboBox';

import { StateName } from '../reducer/data/MainMenuPage';

import { formatParentStyleLayout } from '../framework/core/Style';

const MainMenuPage = (props) => {
    const cmbRouteAction = () => {console.log('ComboBox action')}

    formatParentStyleLayout(props)

    return (
        <Page data={props.page} allProps={props}>
             <Panel data={props.pnlHolder}>


                <Panel data={props.pnlMain}>
                    <Panel data={props.pnlMainHeader}>
                        <Panel data={props.pnlMainHeaderLeft}>
                            <Panel data={props.pnlMainHeaderLeftTable}>
                                <Panel data={props.pnlMainHeaderLeftCell}>
                                    <Image data={props.imgBack}></Image>
                                </Panel>
                            </Panel>
                        </Panel>
     
                        <Panel data={props.pnlMainHeaderMiddle}>
                            <Panel data={props.pnlMainHeaderMiddleTable}>
                                <Panel data={props.pnlMainHeaderMiddleCell}>
                                    <Label data={props.lblTitle}></Label>
                                    <Label data={props.lblLoginAs}></Label>
                                </Panel>
                            </Panel>
                        </Panel>

                        <Panel data={props.pnlMainHeaderRight}>
                            <Panel data={props.pnlMainHeaderRightTable}>
                                <Panel data={props.pnlMainHeaderRightCell}>
                                    <Label data={props.lblAll}></Label>
                                    <Image data={props.Image85}></Image>
                                </Panel>
                            </Panel>
                        </Panel>
                    </Panel>

                    <Panel data={props.pnlMainBodySales}>
                        <Panel data={props.pnlMainBodyCombo}>
                            <Label data={props.lblPONumber383}></Label>
                            <Label data={props.lblMainCustCode}></Label>
                            <ComboBox
                                functions={cmbRouteAction}
                                data={props.cmbRoute}
                            />
                            <Label data={props.lblMainCustName}></Label>
                            <Label data={props.lblMainCustAddress}></Label>
                        </Panel>
                        <Panel data={props.pnlMainBodyTitleSalesOrder}>
                            <Label data={props.Label559}></Label>
                        </Panel>
                        <Panel data={props.pnlMainBody1}>
                            <Panel data={props.pnlMainBody1Left}>
                                <Image data={props.imgHistory}></Image>
                                <Label data={props.lblHistory}></Label>
                            </Panel>
                            <Panel data={props.pnlMainBody1Middle}>
                                <Image data={props.imgHistory65501}></Image>
                                <Label data={props.lblSettings}></Label>
                            </Panel>
                        </Panel>
                        <Panel data={props.pnlMainBody2}>
                        </Panel>
                    </Panel>
                </Panel>



                {/* ************* MODAL ************* */}
                {/* <Panel data={props.pnlModal}>
                    <Panel data={props.pnlModalTable}>
                        <Panel data={props.pnlModalCell}>
                            
                            <Panel data={props.pnlModalBodyMain}>
                                <Panel data={props.pnlModalBodyTitle}>
                                    <Label data={props.lblModalTitle}></Label>
                                </Panel>
                                <Panel data={props.pnlModalBodyMessage}>
                                    <Label data={props.lblModalMessage}></Label>
                                </Panel>
                                <Panel data={props.pnlModalBodyLoading}>
                                    <Image data={props.imgModalLoading}></Image>
                                </Panel>
                            </Panel>
                            <Panel data={props.pnlModalBodyButtons}>
                                <Panel data={props.pnlModalBodyButtonPositive}>
                                    <Label data={props.lblModalPositive}></Label>
                                </Panel>
                                <Panel data={props.pnlModalBodyButtonNegative}>
                                    <Label data={props.lblModalNegative}></Label>
                                </Panel>
                            </Panel>
                        </Panel>
                    </Panel>
                </Panel> */}



            </Panel>
        </Page>
    )
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] }
};

export default connect(mapStateToProps)(MainMenuPage);