import React from 'react';
import { connect } from 'react-redux';
import Page from '../framework/component/Page';
import Button from '../framework/component/Button';
import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/RawCallPage';
const functions = new Functions(StateName);

const actions = {
    handleButtonTestRawCall: async () => {
        const rc = functions.rawCall({
            connector: 'n_rc1',
            // path: 'api/upload',
            path: 'products',
            method: 'GET',
            // file: {'file': {name: 'logo-og.png', type: 'image/png', uri: 'https://reactjs.org/logo-og.png'}},
            // data: {'userid': '123'},
            callback: (result) => {
                functions.console({value:result})
            },
            errCallback: (error) => {
                functions.console({value:error})
            }
        })
    },
}

const RawCallPage = (props) => {
    return (
        <Page data={props.page} allProps={props}>
            <Button data={props.buttonTestRawCall} action={actions.handleButtonTestRawCall}></Button>
        </Page>
    )
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] }
};

export default connect(mapStateToProps)(RawCallPage);