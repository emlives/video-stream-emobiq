import React, { useEffect }  from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgSalesReturnLists';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import Edit from '../framework/component/Edit';
import DataList from '../framework/component/DataList';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handleImgBackClick: async () => {
		functions.gotoPage({
			"p": "pgMainMenu"
		});
	},

	handleImgAddNewClick: async () => {
		await functions.clearData({
			"callback": null,
			"errCallback": null,
			"dataset": "l_ Sales_Return_Lines"
		});
		functions.setVar({
			"var": "vSalesReturnAction",
			"value": "Create"
		});
		functions.gotoPage({
			"p": "pgSalesReturnDetailsNew"
		});
	},

	handlePanel822Click: async () => {
		functions.setVar({
			"var": "v_current_sales_return_data",
			"value": data
		});
		await functions.dataFromString({
			"string": functions.objectAttr({
				"object": functions.objectAttr({
					"object": functions.getVar({
						"var": "v_current_sales_return_data"
					}),
					"attr": "SalesLines"
				}),
				"attr": "Sales_Return_Order_Line"
			}),
			"callback": "",
			"errCallback": null,
			"dataset": "l_ Sales_Return_Lines"
		});
		functions.setVar({
			"var": "vSalesReturnAction",
			"value": "Update"
		});
		functions.gotoPage({
			"p": "pgSalesReturnDetailsNew"
		});
	},

	handlePnlModalBodyButtonPositiveClick: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalHide', {});
	},

	handlePnlModalBodyButtonNegativeClick: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalHide', {});
	},

	handlePgSalesReturnListsLoad: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalLoading', {});
		functions.onBackButton({
			"callback": async (data, extra) => {
				functions.gotoPage({
					"p": "pgMainMenu"
				});
			}
		});
		functions.setTimeout({
			"callback": async (data, extra) => {
				await functions.loadData({
					"order": functions.toArray({
						"value1": functions.toObject({
							"f": "No",
							"v": "desc"
						})
					}),
					"callback": async (data, extra) => {
						functions.userDefined('globalModalHide', {});
						functions.console({
							"value": data
						});
						await functions.dataFromString({
							"errCallback": null,
							"dataset": "l_Sales_Return_Order",
							"string": data,
							"callback": ""
						});
						await functions.loadData({
							"dataset": "l_Sales_Return_Order",
							"filter": "",
							"order": "",
							"callback": async (data, extra) => {
								functions.console({
									"value": data
								});
								functions.console({
									"value": "Retrieved Sales Return Orders"
								});
							},
							"errCallback": ""
						});
					},
					"errCallback": async (data, extra) => {
						functions.setVar({
							"var": "error",
							"value": data["err"]
						});
						functions.userDefined('globalModalInfo', {
							"title": "Error",
							"message": functions.conditional({
								"condition": functions.getVarAttr({
									"var": "error",
									"attr": "global"
								}),
								"yesValue": functions.getVarAttr({
									"var": "error",
									"attr": "global"
								}),
								"noValue": "Cannot connect to Navision Server."
							})
						});
					},
					"dataset": "OK365_Sales_Return_Order",
					"filter": functions.toArray({
						"value1": functions.toObject({
							"f": "Location_Code",
							"v": functions.getVarAttr({
								"var": "vCurrCustomerDetails",
								"attr": "Location_Code"
							})
						}),
						"value2": functions.toObject({
							"f": "Status",
							"v": "Open"
						}),
						"value3": functions.toObject({
							"f": "Sell_to_Customer_No",
							"v": functions.getVarAttr({
								"var": "vCurrCustomerDetails",
								"attr": "No"
							})
						})
					})
				});
			},
			"timeout": 100
		});
	}
};

const PgSalesReturnLists = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;
	formatParentStyleLayout(props);

	useEffect(() => {
		actions.handlePgSalesReturnListsLoad();
	}, [currentTime]);
    
    return (
		<Page data={props.pgSalesReturnLists}>
			<Panel data={props.pnlHolder}>
				<Panel data={props.pnlMain}>
					<Panel data={props.pnlMainHeader}>
						<Panel data={props.pnlMainHeaderLeft}>
							<Panel data={props.pnlMainHeaderLeftTable}>
								<Panel data={props.pnlMainHeaderLeftCell}>
									<Image data={props.imgBack} action={actions.handleImgBackClick} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderMiddle}>
							<Panel data={props.pnlMainHeaderMiddleTable}>
								<Panel data={props.pnlMainHeaderMiddleCell}>
									<Label data={props.lblMainTitle} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderRight}>
							<Panel data={props.pnlMainHeaderRightTable}>
								<Panel data={props.pnlMainHeaderRightCell}>
									<Image data={props.imgAddNew} action={actions.handleImgAddNewClick} />
									<Label data={props.lblFilter} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainSearch}>
						<Panel data={props.pnlMainBodySearch}>
							<Edit data={props.txtSearch} />
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBody106}>
						<Panel data={props.pnlBody573}>
							<Panel data={props.pnlMainTabPending548}>
								<DataList data={props.dtSales_Return_Order} functions={functions} currentTime={currentTime}>
									{({ item, parentStyle }) => (								
										<Panel data={item.pnlProduct} parentStyle={parentStyle}>
											<Panel data={item.pnlProductBody} parentStyle={parentStyle}>
												<Panel data={item.pnlProductBodyMiddle} parentStyle={parentStyle}>
													<Panel data={item.pnlProductBodyMiddle1} parentStyle={parentStyle}>
														<Label data={item.lblDocNumber_data} parentStyle={parentStyle} />
													</Panel>
													<Panel data={item.pnlProductBodyMiddle2} parentStyle={parentStyle}>
														<Label data={item.lblCustomer_data} parentStyle={parentStyle} />
													</Panel>
													<Panel data={item.pnlProductBodyMiddle4141161} parentStyle={parentStyle}>
														<Label data={item.lblDocumentDate_data4} parentStyle={parentStyle} />
													</Panel>
													<Panel data={item.pnlProductBodyMiddle5} parentStyle={parentStyle}>
														<Label data={item.lblDocTotal_data} parentStyle={parentStyle} />
													</Panel>
												</Panel>
												<Panel data={item.Panel822} action={actions.handlePanel822Click} parentStyle={parentStyle}>
													<Image data={item.Image9} parentStyle={parentStyle} />
												</Panel>
											</Panel>
										</Panel>
									)}
								</DataList>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal}>
					<Panel data={props.pnlModalTable}>
						<Panel data={props.pnlModalCell}>
							<Panel data={props.pnlModalBodyMain}>
								<Panel data={props.pnlModalBodyTitle}>
									<Label data={props.lblModalTitle} />
								</Panel>
								<Panel data={props.pnlModalBodyMessage}>
									<Label data={props.lblModalMessage} />
								</Panel>
								<Panel data={props.pnlModalBodyLoading}>
									<Image data={props.imgModalLoadin} />
								</Panel>
							</Panel>
							<Panel data={props.pnlModalBodyButtons}>
								<Panel data={props.pnlModalBodyButtonPositive} action={actions.handlePnlModalBodyButtonPositiveClick}>
									<Label data={props.lblModalPositive} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonNegative} action={actions.handlePnlModalBodyButtonNegativeClick}>
									<Label data={props.lblModalNegative} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlCSS}>
					<Label data={props.lblStyle} />
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgSalesReturnLists);