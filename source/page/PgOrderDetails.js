import React, { useEffect }  from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgOrderDetails';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import Edit from '../framework/component/Edit';
import DataList from '../framework/component/DataList';
import Button from '../framework/component/Button';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handleImgBackClick: async () => {
		functions.console({
			"value": functions.getVar({
				"var": "vCurrCustomer"
			})
		});
		functions.gotoPage({
			"p": "pgOrderList"
		});
	},

	handleImage553Click: async () => {
		functions.setVar({
			"var": "vRecordUpdated",
			"value": "Yes"
		});
		functions.gotoPage({
			"p": "pgOrderItem"
		});
	},

	handlePnlProductBodyRight1CellClick: async () => {
		functions.setVar({
			"var": "vItemDetails",
			"value": data
		});
		await functions.loadData({
			"dataset": "l_orderlist_details",
			"filter": functions.toArray({
				"value1": functions.toObject({
					"o": "=",
					"v": functions.getVarAttr({
						"var": "vItemDetails",
						"attr": "No"
					}),
					"f": "No"
				}),
				"value2": functions.toObject({
					"f": "Unit_of_Measure",
					"o": "=",
					"v": functions.getVarAttr({
						"var": "vItemDetails",
						"attr": "Unit_of_Measure"
					})
				})
			}),
			"callback": async (data, extra) => {
				functions.console({
					"value": data
				});
				functions.setVar({
					"var": "vCurrCartItem",
					"value": data
				});
				functions.conditional({
					"condition": functions.greater({
						"value2": 0,
						"value1": functions.count({
							"values": data
						})
					}),
					"yesCallback": async (data, extra) => {
						functions.console({
							"value": "Yes"
						});
						functions.toArray({
							"value1": functions.setComponentValue({
								"component": "imgUpdateCartImage",
								"value": ""
							}),
							"value2": functions.setComponentValue({
								"component": "lblUpdateCartItemCodeValueOD",
								"value": functions.getVarAttr({
									"var": "vItemDetails",
									"attr": "No"
								})
							}),
							"value3": functions.setComponentValue({
								"component": "lblUpdateCartItemNameValueOD",
								"value": functions.getVarAttr({
									"var": "vItemDetails",
									"attr": "Description"
								})
							}),
							"value4": functions.setComponentValue({
								"component": "lblOrderDetailsPriceValueOD",
								"value": functions.concat({
									"string1": "$ ",
									"string2": functions.formatNumber({
										"value": functions.getVarAttr({
											"var": "vItemDetails",
											"attr": "Unit_Price"
										}),
										"decimals": 2,
										"decimalSep": ".",
										"thousandSep": ","
									})
								})
							}),
							"value5": functions.setComponentValue({
								"component": "txtQuantityOderDetails",
								"value": functions.objectAttr({
									"attr": "Quantity",
									"object": functions.getVarAttr({
										"var": "vCurrCartItem",
										"attr": 0
									})
								})
							}),
							"value6": functions.setComponentValue({
								"component": "lblUOMValue",
								"value": functions.getVarAttr({
									"var": "vItemDetails",
									"attr": "Unit_of_Measure"
								})
							}),
							"value7": null,
							"value8": functions.showElement({
								"component": "pnlUpdateCartModalOrderDetails"
							}),
							"value9": "",
							"value10": ""
						});
						await functions.loadData({
							"errCallback": "",
							"dataset": "l_orderlist_details",
							"callback": ""
						});
					},
					"noCallback": ""
				});
			},
			"errCallback": ""
		});
	},

	handlePanel22Click: async () => {
		functions.setVar({
			"var": "vAction",
			"value": "delete_item"
		});
		functions.setVar({
			"var": "vItemDetails",
			"value": data
		});
		functions.userDefined('globalModalQuestion', {
			"title": functions.concat({
				"string1": "Delete Item for ",
				"string2": functions.getVarAttr({
					"var": "vItemDetails",
					"attr": "No"
				})
			}),
			"message": "Are you sure you want to delete this item?"
		});
	},

	handlePnlMainFooterTblCell1Click: async () => {
		functions.conditional({
			"condition": functions.greater({
				"value1": functions.count({
					"values": await functions.selectAll({
						"dataset": "l_orderlist_details",
						"callback": null,
						"errCallback": null
					})
				}),
				"value2": 0
			}),
			"yesCallback": async (data, extra) => {
				functions.setVar({
					"var": "vAction",
					"value": "update_order"
				});
				functions.userDefined('globalModalQuestion', {
					"title": "Update Order",
					"message": "Are you sure to proceed?"
				});
			},
			"noCallback": async (data, extra) => {
				functions.userDefined('globalModalInfo', {
					"message": "Please add item into a cart.",
					"title": "Empty Field"
				});
			}
		});
	},

	handlePnlModalBodyButtonPositiveClick: async () => {
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "vAction"
				}),
				"value2": "delete_item"
			}),
			"yesCallback": async (data, extra) => {
				await functions.deleteBy({
					"errCallback": "",
					"dataset": "l_orderlist_details",
					"by": "_id",
					"value": functions.getVarAttr({
						"var": "vItemDetails",
						"attr": "_id"
					}),
					"callback": async (data, extra) => {
						functions.hideElement({
							"component": "pnlModal"
						});
					}
				});
				functions.toArray({
					"value1": await functions.loadData({
						"dataset": "l_orderlist_details",
						"callback": async (data, extra) => {
							functions.setVar({
								"var": "vTotalAmountItem",
								"value": 0
							});
							functions.map({
								"values": await functions.selectAll({
									"errCallback": null,
									"dataset": "l_orderlist_details",
									"callback": ""
								}),
								"callback": async (data, extra) => {
									functions.console({
										"value": data
									});
									functions.setVar({
										"var": "vItemDetails_Order",
										"value": data
									});
									functions.setVar({
										"var": "item_amount",
										"value": functions.multi({
											"value1": functions.getVarAttr({
												"var": "vItemDetails_Order",
												"attr": "Quantity"
											}),
											"value2": functions.getVarAttr({
												"var": "vItemDetails_Order",
												"attr": "Unit_Price"
											})
										})
									});
									functions.setVar({
										"var": "vTotalAmountItem",
										"value": functions.add({
											"value1": functions.getVar({
												"var": "vTotalAmountItem",
												"default": ""
											}),
											"value2": functions.getVar({
												"var": "item_amount",
												"default": ""
											})
										})
									});
								}
							});
						},
						"errCallback": ""
					}),
					"value2": await functions.loadData({
						"callback": async (data, extra) => {
							functions.console({
								"value": data
							});
							functions.setVar({
								"var": "vTotalAmountEmpty",
								"value": 0
							});
							functions.map({
								"values": await functions.selectAll({
									"dataset": "l_orderlist_empty_details",
									"callback": "",
									"errCallback": ""
								}),
								"callback": async (data, extra) => {
									functions.console({
										"value": data
									});
									functions.setVar({
										"var": "vEmptyDetails_Order_Amount",
										"value": data
									});
									functions.setVar({
										"var": "item_amount_empty",
										"value": functions.multi({
											"value2": functions.getVarAttr({
												"var": "vEmptyDetails_Order_Amount",
												"attr": "Unit_Price"
											}),
											"value1": functions.getVarAttr({
												"var": "vEmptyDetails_Order_Amount",
												"attr": "Quantity"
											})
										})
									});
									functions.setVar({
										"var": "vTotalAmountEmpty",
										"value": functions.add({
											"value1": functions.getVar({
												"var": "vTotalAmountEmpty",
												"default": ""
											}),
											"value2": functions.getVar({
												"var": "item_amount_empty",
												"default": ""
											})
										})
									});
									functions.console({
										"value": functions.getVar({
											"var": "item_amount_empty"
										})
									});
									functions.console({
										"value": functions.getVar({
											"var": "vTotalAmountEmpty",
											"default": ""
										})
									});
								}
							});
						},
						"errCallback": "",
						"dataset": "l_orderlist_empty_details"
					}),
					"value3": functions.userDefined('globalCalculateTotal', {
						"subTotal": functions.getVar({
							"var": "vTotalAmountItem"
						}),
						"emptyTotal": functions.getVar({
							"var": "vTotalAmountEmpty"
						}),
						"gstRate": functions.getVar({
							"var": "vGstTotalRate"
						})
					}),
					"value4": "",
					"value5": "",
					"value6": "",
					"value7": "",
					"value8": "",
					"value9": "",
					"value10": ""
				});
			},
			"noCallback": async (data, extra) => {
				functions.conditional({
					"condition": functions.equal({
						"value1": functions.getVar({
							"var": "vAction"
						}),
						"value2": "clear_return_cart"
					}),
					"yesCallback": async (data, extra) => {
						await functions.deleteBy({
							"dataset": "l_orderlist_empty_details",
							"by": "No",
							"value": functions.getVarAttr({
								"var": "vEmptyOrderDetails",
								"attr": "No"
							}),
							"callback": async (data, extra) => {
								functions.hideElement({
									"component": "pnlModal"
								});
							},
							"errCallback": ""
						});
						functions.toArray({
							"value1": await functions.loadData({
								"errCallback": "",
								"dataset": "l_orderlist_details",
								"callback": async (data, extra) => {
									functions.setVar({
										"var": "vTotalAmountItem",
										"value": 0
									});
									functions.map({
										"values": await functions.selectAll({
											"callback": "",
											"errCallback": null,
											"dataset": "l_orderlist_details"
										}),
										"callback": async (data, extra) => {
											functions.console({
												"value": data
											});
											functions.setVar({
												"var": "vItemDetails_Order",
												"value": data
											});
											functions.setVar({
												"var": "item_amount",
												"value": functions.multi({
													"value1": functions.getVarAttr({
														"var": "vItemDetails_Order",
														"attr": "Quantity"
													}),
													"value2": functions.getVarAttr({
														"var": "vItemDetails_Order",
														"attr": "Unit_Price"
													})
												})
											});
											functions.setVar({
												"var": "vTotalAmountItem",
												"value": functions.add({
													"value1": functions.getVar({
														"var": "vTotalAmountItem",
														"default": ""
													}),
													"value2": functions.getVar({
														"var": "item_amount",
														"default": ""
													})
												})
											});
										}
									});
								}
							}),
							"value2": await functions.loadData({
								"dataset": "l_orderlist_empty_details",
								"callback": async (data, extra) => {
									functions.console({
										"value": data
									});
									functions.setVar({
										"var": "vTotalAmountEmpty",
										"value": 0
									});
									functions.map({
										"values": await functions.selectAll({
											"callback": "",
											"errCallback": "",
											"dataset": "l_orderlist_empty_details"
										}),
										"callback": async (data, extra) => {
											functions.console({
												"value": data
											});
											functions.setVar({
												"var": "vEmptyDetails_Order_Amount",
												"value": data
											});
											functions.setVar({
												"var": "item_amount_empty",
												"value": functions.multi({
													"value1": functions.getVarAttr({
														"var": "vEmptyDetails_Order_Amount",
														"attr": "Quantity"
													}),
													"value2": functions.getVarAttr({
														"var": "vEmptyDetails_Order_Amount",
														"attr": "Unit_Price"
													})
												})
											});
											functions.setVar({
												"var": "vTotalAmountEmpty",
												"value": functions.add({
													"value1": functions.getVar({
														"var": "vTotalAmountEmpty",
														"default": ""
													}),
													"value2": functions.getVar({
														"var": "item_amount_empty",
														"default": ""
													})
												})
											});
											functions.console({
												"value": functions.getVar({
													"var": "item_amount_empty"
												})
											});
											functions.console({
												"value": functions.getVar({
													"var": "vTotalAmountEmpty",
													"default": ""
												})
											});
										}
									});
								},
								"errCallback": ""
							}),
							"value3": functions.userDefined('globalCalculateTotal', {
								"subTotal": functions.getVar({
									"var": "vTotalAmountItem"
								}),
								"emptyTotal": functions.getVar({
									"var": "vTotalAmountEmpty"
								}),
								"gstRate": functions.getVar({
									"var": "vGstTotalRate"
								})
							}),
							"value4": "",
							"value5": "",
							"value6": "",
							"value7": "",
							"value8": "",
							"value9": "",
							"value10": ""
						});
					},
					"noCallback": async (data, extra) => {
						functions.conditional({
							"condition": functions.equal({
								"value1": functions.getVar({
									"var": "vAction"
								}),
								"value2": "submit_order"
							}),
							"yesCallback": async (data, extra) => {
								functions.setVar({
									"var": "vAction",
									"value": ""
								});
								functions.newArray({
									"var": "sales_line_data"
								});
								functions.map({
									"values": await functions.selectAll({
										"dataset": "l_cart",
										"callback": "",
										"errCallback": ""
									}),
									"callback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.setVar({
											"var": "vl_cart_total",
											"value": data
										});
										functions.push({
											"value": functions.toObject({
												"Unit_of_Measure": functions.getVarAttr({
													"var": "vl_cart_total",
													"attr": "UOM"
												}),
												"No": functions.getVarAttr({
													"var": "vl_cart_total",
													"attr": "ItemCode"
												}),
												"Quantity": functions.getVarAttr({
													"var": "vl_cart_total",
													"attr": "Quantity"
												}),
												"Unit_Price": functions.getVarAttr({
													"var": "vl_cart_total",
													"attr": "Price"
												}),
												"Type": "Item"
											}),
											"var": "sales_line_data"
										});
									}
								});
								functions.map({
									"values": await functions.selectAll({
										"dataset": "l_empty_bottle",
										"callback": "",
										"errCallback": ""
									}),
									"callback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.setVar({
											"var": "vl_cart_total_Empty",
											"value": data
										});
										functions.push({
											"var": "sales_line_data",
											"value": functions.toObject({
												"Quantity": functions.multi({
													"value1": functions.getVarAttr({
														"var": "vl_cart_total_Empty",
														"attr": "quantity"
													}),
													"value2": -1
												}),
												"Unit_Price": functions.getVarAttr({
													"var": "vl_cart_total_Empty",
													"attr": "price"
												}),
												"Type": "G_L_Account",
												"No": functions.getVarAttr({
													"var": "vl_cart_total_Empty",
													"attr": "name"
												})
											})
										});
									}
								});
								functions.console({
									"value": functions.getVar({
										"var": "sales_line_data"
									})
								});
								functions.userDefined('globalModalLoading', {});
								await functions.navCall({
									"data": functions.toObject({
										"Location_Code": functions.getVarAttr({
											"var": "vCurrCustomerDetails",
											"attr": "Location_Code"
										}),
										"Sell_to_Customer_No": functions.getVarAttr({
											"var": "vCurrCustomerDetails",
											"attr": "No"
										}),
										"Order_Date": functions.dbDate({}),
										"SalesLines": functions.toObject({
											"Sales_Order_Line": functions.getVar({
												"var": "sales_line_data"
											})
										}),
										"Shipment_Date": functions.getVarAttr({
											"var": "vCheckout",
											"attr": "InvoiceDate"
										})
									}),
									"callback": async (data, extra) => {
										functions.userDefined('globalModalHide', {});
										functions.console({
											"value": 1
										});
										functions.userDefined('globalModalInfo', {
											"title": "Message",
											"message": "Done"
										});
										functions.setVar({
											"var": "vAction",
											"value": "redirect"
										});
									},
									"errCallback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.console({
											"value": 2
										});
									},
									"connector": "nav",
									"ent": "OK365_Order_Entry",
									"function": "Create"
								});
							},
							"noCallback": async (data, extra) => {
								functions.conditional({
									"condition": functions.equal({
										"value1": functions.getVar({
											"var": "vAction"
										}),
										"value2": "redirect"
									}),
									"yesCallback": async (data, extra) => {
										functions.gotoPage({
											"p": "pgMainMenu"
										});
										functions.setVar({
											"var": "vAction"
										});
									},
									"noCallback": async (data, extra) => {
										functions.conditional({
											"condition": functions.equal({
												"value1": functions.getVar({
													"var": "vAction"
												}),
												"value2": "submitSODraft"
											}),
											"yesCallback": async (data, extra) => {
												functions.setVar({
													"var": "vAction"
												});
											},
											"noCallback": async (data, extra) => {
												functions.conditional({
													"condition": functions.equal({
														"value1": functions.getVar({
															"var": "vAction"
														}),
														"value2": "update_order"
													}),
													"yesCallback": async (data, extra) => {
														functions.setVar({
															"var": "vAction"
														});
														functions.userDefined('globalModalLoading', {});
														await functions.loadData({
															"dataset": "OK365_Order_Entry",
															"filter": functions.toArray({
																"value1": functions.toObject({
																	"f": "No",
																	"v": functions.getVarAttr({
																		"var": "vCurrOder",
																		"attr": "No"
																	})
																})
															}),
															"callback": async (data, extra) => {
																functions.console({
																	"value": "tst"
																});
																functions.console({
																	"value": data
																});
																functions.setVar({
																	"var": "vTmp",
																	"value": functions.objectAttr({
																		"object": data,
																		"attr": 0
																	})
																});
																await functions.navCall({
																	"data": functions.toObject({
																		"cdeSalesOrdNo": functions.getVarAttr({
																			"var": "vTmp",
																			"attr": "No"
																		})
																	}),
																	"callback": async (data, extra) => {
																		functions.console({
																			"value": data
																		});
																		functions.console({
																			"value": "Delete Callback"
																		});
																		functions.newArray({
																			"var": "sales_line_data"
																		});
																		functions.map({
																			"values": await functions.selectAll({
																				"dataset": "l_orderlist_details",
																				"callback": "",
																				"errCallback": ""
																			}),
																			"callback": async (data, extra) => {
																				functions.console({
																					"value": data
																				});
																				functions.setVar({
																					"var": "vl_cart_total",
																					"value": data
																				});
																				functions.push({
																					"var": "sales_line_data",
																					"value": functions.toObject({
																						"Unit_Price": functions.getVarAttr({
																							"var": "vl_cart_total",
																							"attr": "Unit_Price"
																						}),
																						"Type": "Item",
																						"Unit_of_Measure": functions.getVarAttr({
																							"var": "vl_cart_total",
																							"attr": "Unit_of_Measure"
																						}),
																						"No": functions.getVarAttr({
																							"var": "vl_cart_total",
																							"attr": "No"
																						}),
																						"Quantity": functions.getVarAttr({
																							"var": "vl_cart_total",
																							"attr": "Quantity"
																						})
																					})
																				});
																			}
																		});
																		functions.map({
																			"values": await functions.selectAll({
																				"dataset": "l_orderlist_empty_details",
																				"callback": "",
																				"errCallback": ""
																			}),
																			"callback": async (data, extra) => {
																				functions.console({
																					"value": data
																				});
																				functions.setVar({
																					"var": "vl_cart_total_Empty",
																					"value": data
																				});
																				functions.push({
																					"var": "sales_line_data",
																					"value": functions.toObject({
																						"No": functions.getVarAttr({
																							"var": "vl_cart_total_Empty",
																							"attr": "No"
																						}),
																						"Quantity": functions.multi({
																							"value1": functions.getVarAttr({
																								"var": "vl_cart_total_Empty",
																								"attr": "Quantity"
																							}),
																							"value2": -1
																						}),
																						"Unit_Price": functions.conditional({
																							"condition": functions.getVarAttr({
																								"var": "vl_cart_total_Empty",
																								"attr": "Unit_Price"
																							}),
																							"yesValue": functions.getVarAttr({
																								"var": "vl_cart_total_Empty",
																								"attr": "Unit_Price"
																							}),
																							"noValue": functions.getVarAttr({
																								"var": "vl_cart_total_Empty",
																								"attr": "Price"
																							})
																						}),
																						"Type": "G_L_Account"
																					})
																				});
																			}
																		});
																		await functions.navCall({
																			"connector": "nav",
																			"ent": "OK365_Order_Entry",
																			"function": "Update",
																			"data": functions.toObject({
																				"Key": functions.getVarAttr({
																					"var": "vTmp",
																					"attr": "Key"
																				}),
																				"No": functions.getVarAttr({
																					"var": "vTmp",
																					"attr": "No"
																				}),
																				"Remarks_OkFldSls": null,
																				"SalesLines": functions.toObject({
																					"Sales_Order_Line": functions.getVar({
																						"var": "sales_line_data"
																					})
																				})
																			}),
																			"callback": async (data, extra) => {
																				functions.setVar({
																					"var": "vOrderEntryResult",
																					"value": functions.objectAttr({
																						"object": data,
																						"attr": "OK365_Order_Entry"
																					})
																				});
																				functions.setVar({
																					"var": "vOrderEntryLineResult",
																					"value": functions.objectAttr({
																						"attr": "Sales_Order_Line",
																						"object": functions.objectAttr({
																							"object": functions.objectAttr({
																								"object": data,
																								"attr": "OK365_Order_Entry"
																							}),
																							"attr": "SalesLines"
																						})
																					})
																				});
																				await functions.dataFromString({
																					"dataset": "l_OrderLines",
																					"string": functions.getVar({
																						"var": "vOrderEntryLineResult"
																					}),
																					"callback": null,
																					"errCallback": null
																				});
																				await functions.dataFromString({
																					"dataset": "l_OrderEmptyLines",
																					"string": functions.getVar({
																						"var": "vOrderEntryLineResult"
																					}),
																					"callback": null,
																					"errCallback": null
																				});
																				functions.userDefined('globalModalHide', {});
																				functions.console({
																					"value": data
																				});
																				functions.setVar({
																					"var": "vOrderSubmitResult",
																					"value": data
																				});
																				functions.setVar({
																					"var": "vOrderNo",
																					"value": functions.objectAttr({
																						"attr": "No",
																						"object": functions.getVarAttr({
																							"var": "vOrderSubmitResult",
																							"attr": "OK365_Order_Entry"
																						})
																					})
																				});
																				functions.console({
																					"value": functions.getVar({
																						"var": "vOrderNo"
																					})
																				});
																				functions.setVar({
																					"var": "vAction",
																					"value": "redirect"
																				});
																				await functions.clearData({
																					"dataset": "l_cart",
																					"callback": "",
																					"errCallback": ""
																				});
																				await functions.clearData({
																					"dataset": "l_empty_bottle",
																					"callback": "",
																					"errCallback": ""
																				});
																				functions.setVar({
																					"var": "vAction",
																					"value": "goto_invoice"
																				});
																				functions.userDefined('globalModalQuestion', {
																					"title": "Order Updated",
																					"message": functions.concat({
																						"string1": "Your order No:",
																						"string2": "<b>",
																						"string3": functions.getVar({
																							"var": "vOrderNo"
																						}),
																						"string4": functions.concat({
																							"string1": functions.concat({
																								"string1": "</b><br>",
																								"string2": "was successfully updated."
																							}),
																							"string2": "<br>",
																							"string3": "Do you want to proceed to Invoice?"
																						})
																					})
																				});
																			},
																			"errCallback": async (data, extra) => {
																				functions.console({
																					"value": data
																				});
																				functions.console({
																					"value": 2
																				});
																			}
																		});
																	},
																	"errCallback": async (data, extra) => {
																		functions.console({
																			"value": "Delete Error Callback"
																		});
																	},
																	"connector": "nav",
																	"ent": "OK365_GetItemSalesPrice",
																	"function": "g_fnDeleteSalesOrdLine"
																});
															},
															"errCallback": ""
														});
													},
													"noCallback": async (data, extra) => {
														functions.conditional({
															"condition": functions.equal({
																"value1": functions.getVar({
																	"var": "vAction"
																}),
																"value2": "goto_invoice"
															}),
															"yesCallback": async (data, extra) => {
																functions.setVar({
																	"var": "vAction",
																	"value": ""
																});
																functions.setVar({
																	"var": "vNewCheckout",
																	"value": "true"
																});
																functions.gotoPage({
																	"p": "pgSummary"
																});
															},
															"noCallback": async (data, extra) => {
																functions.userDefined('globalModalHide', {});
															}
														});
													}
												});
											}
										});
									}
								});
							}
						});
					}
				});
			}
		});
	},

	handlePnlModalBodyButtonNegativeClick: async () => {
		functions.conditional({
			"condition": functions.equal({
				"value2": "goto_invoice",
				"value1": functions.getVar({
					"var": "vAction"
				})
			}),
			"yesCallback": async (data, extra) => {
				functions.setVar({
					"var": "vAction"
				});
				functions.userDefined('globalModalHide', {});
				functions.gotoPage({
					"p": "pgMainMenu"
				});
			},
			"noCallback": async (data, extra) => {
				functions.setVar({
					"var": "vAction"
				});
				functions.userDefined('globalModalHide', {});
			}
		});
	},

	handleImgOrderDetailsCloseClick: async () => {
		functions.setComponentValue({
			"component": "txtQuantity",
			"value": ""
		});
		functions.hideElement({
			"component": "pnlUpdateCartModalOrderDetails"
		});
	},

	handleImgMinusClick: async () => {
		functions.conditional({
			"condition": functions.greater({
				"value1": null,
				"value2": 1
			}),
			"yesCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantityOderDetails",
					"value": functions.sub({
						"value1": null,
						"value2": 1
					})
				});
			},
			"noCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantityOderDetails",
					"value": 0
				});
			}
		});
	},

	handleTxtQuantityOderDetailsClick: async () => {
		functions.setComponentFocus({
			"component": "txtQuantity",
			"selectAllText": "true"
		});
	},

	handleImgPlusClick: async () => {
		functions.conditional({
			"condition": functions.greater({
				"value1": null,
				"value2": -1
			}),
			"yesCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantityOderDetails",
					"value": functions.add({
						"value1": null,
						"value2": 1
					})
				});
			},
			"noCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantityOderDetails",
					"value": 0
				});
			}
		});
	},

	handleBtnUpdateClick: async () => {
		functions.conditional({
			"condition": null,
			"yesCallback": async (data, extra) => {
				functions.conditional({
					"condition": functions.greater({
						"value1": null,
						"value2": functions.toFloat({
							"value": -1
						})
					}),
					"yesCallback": async (data, extra) => {
						functions.conditional({
							"condition": functions.greater({
								"value1": null,
								"value2": 0
							}),
							"yesCallback": async (data, extra) => {
								await functions.loadData({
									"dataset": "l_orderlist_details",
									"filter": functions.toArray({
										"value1": functions.toObject({
											"f": "No",
											"o": "=",
											"v": functions.getVarAttr({
												"var": "vItemDetails",
												"attr": "No"
											})
										}),
										"value2": functions.toObject({
											"f": "Unit_of_Measure",
											"o": "=",
											"v": functions.getVarAttr({
												"var": "vItemDetails",
												"attr": "Unit_of_Measure"
											})
										})
									}),
									"callback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.setVar({
											"var": "vCurrCartItem",
											"value": data
										});
										functions.conditional({
											"condition": functions.greater({
												"value1": functions.count({
													"values": data
												}),
												"value2": 0
											}),
											"yesCallback": async (data, extra) => {
												functions.console({
													"value": "Yes"
												});
												functions.toArray({
													"value1": await functions.loadData({
														"dataset": "l_orderlist_details",
														"callback": async (data, extra) => {
															functions.setVar({
																"var": "vTotalAmountItem",
																"value": 0
															});
															functions.map({
																"values": await functions.selectAll({
																	"dataset": "l_orderlist_details",
																	"callback": "",
																	"errCallback": ""
																}),
																"callback": async (data, extra) => {
																	functions.console({
																		"value": data
																	});
																	functions.setVar({
																		"var": "vItemDetails_Order",
																		"value": data
																	});
																	functions.setVar({
																		"var": "item_amount",
																		"value": functions.multi({
																			"value1": functions.getVarAttr({
																				"var": "vItemDetails_Order",
																				"attr": "Quantity"
																			}),
																			"value2": functions.getVarAttr({
																				"var": "vItemDetails_Order",
																				"attr": "Unit_Price"
																			})
																		})
																	});
																	functions.setVar({
																		"var": "vTotalAmountItem",
																		"value": functions.add({
																			"value1": functions.getVar({
																				"var": "vTotalAmountItem",
																				"default": ""
																			}),
																			"value2": functions.getVar({
																				"var": "item_amount",
																				"default": ""
																			})
																		})
																	});
																}
															});
														},
														"errCallback": ""
													}),
													"value2": await functions.loadData({
														"dataset": "l_orderlist_empty_details",
														"callback": async (data, extra) => {
															functions.console({
																"value": data
															});
															functions.setVar({
																"var": "vTotalAmountEmpty",
																"value": 0
															});
															functions.map({
																"values": await functions.selectAll({
																	"callback": "",
																	"errCallback": "",
																	"dataset": "l_orderlist_empty_details"
																}),
																"callback": async (data, extra) => {
																	functions.console({
																		"value": data
																	});
																	functions.setVar({
																		"var": "vEmptyDetails_Order_Amount",
																		"value": data
																	});
																	functions.setVar({
																		"var": "item_amount_empty",
																		"value": functions.absolute({
																			"value": functions.multi({
																				"value1": functions.getVarAttr({
																					"var": "vEmptyDetails_Order_Amount",
																					"attr": "Quantity"
																				}),
																				"value2": functions.conditional({
																					"condition": functions.getVarAttr({
																						"var": "vEmptyDetails_Order_Amount",
																						"attr": "Unit_Price"
																					}),
																					"yesValue": functions.getVarAttr({
																						"var": "vEmptyDetails_Order_Amount",
																						"attr": "Unit_Price"
																					}),
																					"noValue": functions.getVarAttr({
																						"var": "vEmptyDetails_Order_Amount",
																						"attr": "Price"
																					})
																				})
																			})
																		})
																	});
																	functions.setVar({
																		"var": "vTotalAmountEmpty",
																		"value": functions.add({
																			"value1": functions.getVar({
																				"var": "vTotalAmountEmpty",
																				"default": ""
																			}),
																			"value2": functions.getVar({
																				"var": "item_amount_empty",
																				"default": ""
																			})
																		})
																	});
																	functions.console({
																		"value": functions.getVar({
																			"var": "item_amount_empty"
																		})
																	});
																	functions.console({
																		"value": functions.getVar({
																			"var": "vTotalAmountEmpty",
																			"default": ""
																		})
																	});
																}
															});
														},
														"errCallback": ""
													}),
													"value3": functions.userDefined('globalCalculateTotal', {
														"gstRate": functions.getVar({
															"var": "vGstTotalRate"
														}),
														"subTotal": functions.getVar({
															"var": "vTotalAmountItem"
														}),
														"emptyTotal": functions.getVar({
															"var": "vTotalAmountEmpty"
														})
													}),
													"value4": "",
													"value5": "",
													"value6": "",
													"value7": "",
													"value8": "",
													"value9": "",
													"value10": ""
												});
												functions.hideElement({
													"component": "pnlUpdateCartModalOrderDetails"
												});
											},
											"noCallback": ""
										});
									},
									"errCallback": ""
								});
							},
							"noCallback": ""
						});
					},
					"noCallback": async (data, extra) => {
						functions.userDefined('globalModalInfo', {
							"title": "Invalid Value",
							"message": "Please enter a valid value/amount."
						});
					}
				});
			},
			"noCallback": async (data, extra) => {
				functions.userDefined('globalModalInfo', {
					"message": "Enter the quantity.",
					"title": "No Value"
				});
			}
		});
	},

	handlePgOrderDetailsLoad: async () => {
		functions.onBackButton({
			"callback": async (data, extra) => {
				functions.gotoPage({
					"p": "pgOrderList"
				});
			}
		});
		functions.setVar({
			"var": "vAction"
		});
		functions.toArray({
			"value1": functions.setComponentValue({
				"component": "lblCustomerCodeName_data",
				"value": functions.concat({
					"string1": functions.getVarAttr({
						"var": "vCurrOder2",
						"attr": "Sell_to_Customer_No"
					}),
					"string2": " - ",
					"string3": functions.getVarAttr({
						"var": "vCurrOder2",
						"attr": "Sell_to_Customer_Name"
					})
				})
			}),
			"value2": functions.setComponentValue({
				"component": "txtCustomerNameOrderDetails",
				"value": functions.getVarAttr({
					"var": "vCurrOder",
					"attr": "Sell_to_Customer_Name"
				})
			}),
			"value3": functions.setComponentValue({
				"component": "txtOrderDateOrderDetails",
				"value": functions.dbDate({})
			}),
			"value4": functions.setComponentValue({
				"component": "txtInvoiceDateOrderDetails",
				"value": functions.formatDate({
					"date": functions.dbDate({}),
					"format": "Y-m-d"
				})
			}),
			"value5": functions.setComponentValue({
				"component": "txtRemarks",
				"value": functions.getVarAttr({
					"var": "vCurrOder2",
					"attr": "Remarks"
				})
			}),
			"value6": functions.setComponentValue({
				"component": "txtAddressOrderDetails",
				"value": ""
			}),
			"value7": functions.setComponentValue({
				"component": "lblOrderNo",
				"value": functions.getVarAttr({
					"var": "vCurrOder2",
					"attr": "No"
				})
			}),
			"value8": functions.setComponentValue({
				"component": "lblAddress_data",
				"value": functions.getVarAttr({
					"var": "vCurrOder2",
					"attr": "Sell_to_Address"
				})
			}),
			"value9": functions.setComponentValue({
				"component": "lblOrderDate_data",
				"value": functions.formatDate({
					"format": "d-m-Y",
					"date": functions.getVarAttr({
						"var": "vCurrOder2",
						"attr": "Order_Date"
					})
				})
			})
		});
		functions.hideElement({
			"component": "pnlConfirmPaymentMethod3"
		});
		functions.setComponentValue({
			"component": "txtRemarksOrderDetails",
			"value": functions.getVar({
				"var": "vRemarks"
			})
		});
		functions.console({
			"value": functions.getVar({
				"var": "vCurrCustomer"
			})
		});
		await functions.loadData({
			"dataset": "OK365_GST_Setup",
			"filter": functions.toArray({
				"value1": functions.toObject({
					"v": functions.getVarAttr({
						"var": "vItemDetails",
						"attr": "VAT_Prod_Posting_Group"
					}),
					"f": "VAT_Prod_Posting_Group"
				}),
				"value2": functions.toObject({
					"f": "VAT_Bus_Posting_Group",
					"v": functions.getVarAttr({
						"var": "vCurrCustomerDetails",
						"attr": "VAT_Bus_Posting_Group"
					})
				})
			}),
			"callback": async (data, extra) => {
				functions.console({
					"value": "gst rate"
				});
				functions.console({
					"value": functions.getVar({
						"var": "vCurrCustomer"
					})
				});
				functions.console({
					"value": functions.objectAttr({
						"object": functions.objectAttr({
							"object": data,
							"attr": 0
						}),
						"attr": "VAT_Percent"
					})
				});
				functions.setVar({
					"var": "vGstTotalRate",
					"value": functions.objectAttr({
						"object": functions.objectAttr({
							"object": data,
							"attr": 0
						}),
						"attr": "VAT_Percent"
					})
				});
				functions.conditional({
					"condition": functions.equal({
						"value1": functions.getVar({
							"var": "vRecordUpdated"
						}),
						"value2": "Yes"
					}),
					"yesValue": "",
					"noValue": "",
					"extra": "",
					"yesCallback": async (data, extra) => {
						functions.console({
							"value": "vRecordUpdated = Yes"
						});
						functions.toArray({
							"value1": await functions.loadData({
								"dataset": "l_orderlist_details",
								"callback": async (data, extra) => {
									functions.setVar({
										"var": "vTotalAmountItem",
										"value": 0
									});
									functions.map({
										"values": await functions.selectAll({
											"callback": "",
											"errCallback": "",
											"dataset": "l_orderlist_details"
										}),
										"callback": async (data, extra) => {
											functions.console({
												"value": data
											});
											functions.setVar({
												"var": "vItemDetails_Order",
												"value": data
											});
											functions.setVar({
												"var": "item_amount",
												"value": functions.multi({
													"value1": functions.getVarAttr({
														"var": "vItemDetails_Order",
														"attr": "Quantity"
													}),
													"value2": functions.getVarAttr({
														"var": "vItemDetails_Order",
														"attr": "Unit_Price"
													})
												})
											});
											functions.setVar({
												"var": "vTotalAmountItem",
												"value": functions.add({
													"value1": functions.getVar({
														"var": "vTotalAmountItem",
														"default": ""
													}),
													"value2": functions.getVar({
														"var": "item_amount",
														"default": ""
													})
												})
											});
										}
									});
								},
								"errCallback": ""
							}),
							"value2": functions.getVar({
								"var": "vTotalAmountItem"
							}),
							"value3": functions.userDefined('globalCalculateTotal', {
								"subTotal": functions.getVar({
									"var": "vTotalAmountItem"
								}),
								"emptyTotal": functions.absolute({
									"value": functions.getVar({
										"var": "vTotalAmountEmpty"
									})
								}),
								"gstRate": functions.getVar({
									"var": "vGstTotalRate"
								})
							}),
							"value4": "",
							"value5": "",
							"value6": "",
							"value7": "",
							"value8": "",
							"value9": "",
							"value10": ""
						});
					},
					"noCallback": async (data, extra) => {
						functions.console({
							"value": "vRecordUpdated = No"
						});
						await functions.loadData({
							"dataset": "l_orderlist_details",
							"callback": async (data, extra) => {
								functions.setVar({
									"var": "vOrderLine_item",
									"value": data
								});
								functions.setVar({
									"var": "vOrderLine_item_cnt",
									"value": functions.count({
										"values": functions.getVar({
											"var": "vOrderLine_item"
										})
									})
								});
								functions.conditional({
									"condition": functions.greater({
										"value1": functions.getVar({
											"var": "vOrderLine_item_cnt"
										}),
										"value2": 0
									}),
									"yesCallback": async (data, extra) => {
										functions.console({
											"value": "Have record in : l_orderlist_details"
										});
										functions.setVar({
											"var": "vOrderLine_item_counter",
											"value": 1
										});
										functions.setVar({
											"var": "vOrderLineSubTotal",
											"value": 0
										});
										functions.map({
											"values": functions.getVar({
												"var": "vOrderLine_item"
											}),
											"callback": async (data, extra) => {
												functions.setVar({
													"var": "vOrderLineMap",
													"value": data
												});
												functions.setVar({
													"var": "vOrderLine_item_counter",
													"value": functions.add({
														"value1": functions.getVar({
															"var": "vOrderLine_item_counter",
															"default": ""
														}),
														"value2": 1
													})
												});
												functions.setVar({
													"var": "vOrderLineSubTotal",
													"value": functions.add({
														"value1": functions.getVar({
															"var": "vOrderLineSubTotal"
														}),
														"value2": functions.getVarAttr({
															"var": "vOrderLineMap",
															"attr": "Line_Amount",
															"default": ""
														})
													})
												});
												functions.conditional({
													"condition": functions.greater({
														"value1": functions.getVar({
															"var": "vOrderLine_item_counter"
														}),
														"value2": functions.getVar({
															"var": "vOrderLine_item_cnt"
														})
													}),
													"yesCallback": async (data, extra) => {
														functions.console({
															"value": "Set the Sub Total Here"
														});
														functions.setComponentValue({
															"component": "lblDataSubtotal",
															"value": functions.concat({
																"string1": "$ ",
																"string2": functions.formatNumber({
																	"value": functions.getVar({
																		"var": "vOrderLineSubTotal"
																	}),
																	"decimals": 2,
																	"decimalSep": ".",
																	"thousandSep": ","
																})
															})
														});
														await functions.loadData({
															"dataset": "l_orderlist_empty_details",
															"callback": async (data, extra) => {
																functions.setVar({
																	"var": "vOrderLine_gl",
																	"value": data
																});
																functions.setVar({
																	"var": "vOrderLine_gl_cnt",
																	"value": functions.count({
																		"values": functions.getVar({
																			"var": "vOrderLine_gl"
																		})
																	})
																});
																functions.setComponentValue({
																	"component": "lblDataTotalAmountExclGST",
																	"value": functions.concat({
																		"string1": "$ ",
																		"string2": functions.formatNumber({
																			"thousandSep": ",",
																			"value": functions.absolute({
																				"value": functions.getVarAttr({
																					"var": "vOrderLineMap",
																					"attr": "Total_Amount_Excl_VAT"
																				})
																			}),
																			"decimals": 2,
																			"decimalSep": "."
																		})
																	})
																});
															},
															"errCallback": ""
														});
													},
													"noCallback": ""
												});
											}
										});
									},
									"noCallback": async (data, extra) => {
										functions.console({
											"value": "No record in : l_orderlist_details"
										});
										functions.setComponentValue({
											"component": "lblDataTotalAmountExclGST",
											"value": functions.concat({
												"string1": "$ ",
												"string2": functions.formatNumber({
													"decimalSep": ".",
													"thousandSep": ",",
													"value": 0,
													"decimals": 2
												})
											})
										});
									}
								});
							},
							"errCallback": null
						});
					}
				});
			},
			"errCallback": async (data, extra) => {
				functions.setVar({
					"var": "error",
					"value": data["err"]
				});
			}
		});
	}
};

const PgOrderDetails = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;
	formatParentStyleLayout(props);

	useEffect(() => {
		actions.handlePgOrderDetailsLoad();
	}, [currentTime]);
    
    return (
		<Page data={props.pgOrderDetails}>
			<Panel data={props.pnlHolder}>
				<Panel data={props.pnlMain}>
					<Panel data={props.pnlMainHeader}>
						<Panel data={props.pnlMainHeaderLeft}>
							<Panel data={props.pnlMainHeaderLeftTable}>
								<Panel data={props.pnlMainHeaderLeftCell}>
									<Image data={props.imgBack} action={actions.handleImgBackClick} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderMiddle}>
							<Panel data={props.pnlMainHeaderMiddleTable}>
								<Panel data={props.pnlMainHeaderMiddleCell}>
									<Label data={props.lblMainTitle} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderRight}>
							<Panel data={props.pnlMainHeaderRightTable}>
								<Panel data={props.pnlMainHeaderRightCell} />
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBody1}>
						<Panel data={props.pnlMainBodyForm}>
							<Label data={props.lblOrderNo} />
							<Label data={props.lblCustomerCodeName_data} />
							<Label data={props.lblAddress_data} />
							<Label data={props.lblOrderDate_data} />
							<Panel data={props.pnlMainBodyFormCustomerCodeOrderDetails}>
								<Panel data={props.pnlMainBodyFormLabelCustomerCode} />
								<Panel data={props.pnlMainBodyFormInputValueOrderDetails}>
									<Edit data={props.txtCustomerCodeOrderDetails} />
								</Panel>
							</Panel>
							<Panel data={props.pnlMainBodyFormCustomerNameOrderDetails}>
								<Panel data={props.pnlMainBodyForm3LabelCustomerNameOrderDetails} />
								<Panel data={props.pnlMainBodyForm3InputValueOrderDetails}>
									<Edit data={props.txtCustomerNameOrderDetails} />
								</Panel>
							</Panel>
							<Panel data={props.pnlMainBodyAddressOrderDetails}>
								<Panel data={props.pnlMainBodyForm3LabelAddressOrderDetails} />
								<Panel data={props.pnlMainBodyFormInputValue}>
									<Edit data={props.txtAddressOrderDetails} />
								</Panel>
							</Panel>
							<Panel data={props.pnlMainBodyFormOrderDateOrderDetails}>
								<Panel data={props.pnlMainBodyForm3LabelOrderDateOrderDetails} />
								<Panel data={props.pnlMainBodyForm3Input77725}>
									<Edit data={props.txtOrderDateOrderDetails} />
								</Panel>
							</Panel>
							<Panel data={props.pnlMainBodyFormInvoiceDateOrderDetails}>
								<Panel data={props.pnlMainBodyForm3LabelInvoiceDateOrderDetails}>
									<Label data={props.lblInvoiceDateOrderDetails} />
								</Panel>
								<Panel data={props.pnlMainBodyForm3Input}>
									<Edit data={props.txtInvoiceDateOrderDetails} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainBodyItems1}>
							<Label data={props.lblItems} />
							<Image data={props.Image553} action={actions.handleImage553Click} />
						</Panel>
						<Panel data={props.pnlMainBodyItems}>
							<Panel data={props.pnlMainBodyItemsTable}>
								<DataList data={props.dlItem} functions={functions} currentTime={currentTime}>
									{({ item, parentStyle }) => (								
										<Panel data={item.Panel293xx} parentStyle={parentStyle}>
											<Panel data={item.pnlProductItem} parentStyle={parentStyle}>
												<Panel data={item.pnlProductBodyItems1} parentStyle={parentStyle}>
													<Panel data={item.pnlProductBodyLeft} parentStyle={parentStyle}>
														<Image data={item.imgItem} parentStyle={parentStyle} />
													</Panel>
													<Panel data={item.pnlProductBodyMiddle} parentStyle={parentStyle}>
														<Panel data={item.pnlProductBodyMiddle1} parentStyle={parentStyle}>
															<Label data={item.lblItemCode} parentStyle={parentStyle} />
															<Label data={item.lblItemName} parentStyle={parentStyle} />
															<Label data={item.lblPrice} parentStyle={parentStyle} />
														</Panel>
													</Panel>
												</Panel>
											</Panel>
											<Panel data={item.pnlProductBodyRight} parentStyle={parentStyle}>
												<Panel data={item.pnlProductBodyRight1Cell} action={actions.handlePnlProductBodyRight1CellClick} parentStyle={parentStyle}>
													<Label data={item.Label235} parentStyle={parentStyle} />
												</Panel>
												<Panel data={item.Panel22} action={actions.handlePanel22Click} parentStyle={parentStyle}>
													<Label data={item.Label401} parentStyle={parentStyle} />
												</Panel>
											</Panel>
										</Panel>
									)}
								</DataList>
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBodyDetailsTotalAmt}>
						<Panel data={props.pnlTotalAmountTable}>
							<Panel data={props.pnlTotalAmountTableCell1}>
								<Label data={props.lblTotalAmountExclGST} />
							</Panel>
							<Panel data={props.pnlTotalAmountTableCell2}>
								<Label data={props.lblDataTotalAmountExclGST} />
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainFooter}>
						<Panel data={props.pnlMainFooterTbl}>
							<Panel data={props.pnlMainFooterTblCell1} action={actions.handlePnlMainFooterTblCell1Click}>
								<Label data={props.lblBtnUpdate} />
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal}>
					<Panel data={props.pnlModalTable}>
						<Panel data={props.pnlModalCell}>
							<Panel data={props.pnlModalBodyMain}>
								<Panel data={props.pnlModalBodyTitle}>
									<Label data={props.lblModalTitle} />
								</Panel>
								<Panel data={props.pnlModalBodyMessage}>
									<Label data={props.lblModalMessage} />
								</Panel>
								<Panel data={props.pnlModalBodyLoading}>
									<Image data={props.imgModalLoading} />
								</Panel>
							</Panel>
							<Panel data={props.pnlModalBodyButtons}>
								<Panel data={props.pnlModalBodyButtonPositive} action={actions.handlePnlModalBodyButtonPositiveClick}>
									<Label data={props.lblModalPositive} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonNegative} action={actions.handlePnlModalBodyButtonNegativeClick}>
									<Label data={props.lblModalNegative} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlUpdateCartModalOrderDetails}>
					<Panel data={props.pnlUpdateCartModalTableOrderDetails}>
						<Panel data={props.pnlUpdateCartModalCellOrderDetails}>
							<Panel data={props.pnlUpdateCartModalBodyOrderDetails}>
								<Panel data={props.pnlUpdateCartModalBodyOrderDetailsImage}>
									<Image data={props.imgOrderDetailsImage} />
									<Image data={props.imgOrderDetailsClose} action={actions.handleImgOrderDetailsCloseClick} />
								</Panel>
								<Panel data={props.Panel964}>
									<Panel data={props.pnlUpdateCartModalBodyOrderDetailsCode}>
										<Label data={props.lblUpdateCartItemCodeValueOD} />
									</Panel>
									<Panel data={props.pnlUpdateCartModalBodyOrderDetailsName}>
										<Label data={props.lblUpdateCartItemNameValueOD} />
									</Panel>
									<Panel data={props.pnlUpdateCartModalBodyOrderDetailsPrice}>
										<Label data={props.lblOrderDetailsPriceValueOD} />
									</Panel>
								</Panel>
								<Panel data={props.Panel293}>
									<Panel data={props.pnlUpdateCartModalBodyOrderDetailsQty}>
										<Panel data={props.pnlUpdateCartModalBodyQtyValueOD}>
											<Panel data={props.pnlUpdateCartModalBodyLeft}>
												<Image data={props.imgMinus} action={actions.handleImgMinusClick} />
											</Panel>
											<Panel data={props.pnlUpdateCartModalBodyMiddle}>
												<Edit data={props.txtQuantityOderDetails} action={actions.handleTxtQuantityOderDetailsClick} />
											</Panel>
											<Panel data={props.pnlUpdateCartModalBodyRight}>
												<Image data={props.imgPlus} action={actions.handleImgPlusClick} />
											</Panel>
										</Panel>
										<Label data={props.lblUOMValue} />
									</Panel>
								</Panel>
								<Panel data={props.pnlMainBodyOrderDetailsDiscount}>
									<Panel data={props.pnlUpdateCartModalBodyDiscountOD}>
										<Label data={props.lblDiscountIL} />
									</Panel>
								</Panel>
								<Panel data={props.pnlUpdateCartModalBodyButtonOrderDetails}>
									<Button data={props.btnUpdate} action={actions.handleBtnUpdateClick} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlCSS}>
					<Label data={props.lblStyle} />
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgOrderDetails);