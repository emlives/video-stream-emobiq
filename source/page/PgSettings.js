import React, { useEffect }  from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgSettings';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import ComboBox from '../framework/component/ComboBox';
import Edit from '../framework/component/Edit';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handleImgBackClick: async () => {
		functions.gotoPage({
			"p": "pgMainMenu"
		});
	},

	handlePnlMainHeaderRightClick: async () => {
		functions.userDefined('globalModalLoading', {});
		functions.setVar({
			"var": "vSettingCount",
			"value": functions.count({
				"values": await functions.selectAll({
					"dataset": "l_settings",
					"callback": "",
					"errCallback": null
				})
			})
		});
		functions.conditional({
			"condition": functions.getVar({
				"var": "vSettingCount",
				"default": ""
			}),
			"yesValue": "",
			"noValue": "",
			"extra": "",
			"yesCallback": async (data, extra) => {
				functions.console({
					"value": "update..."
				});
				await functions.updateBy({
					"callback": "",
					"errCallback": null,
					"dataset": "l_settings",
					"by": "_id",
					"operator": "=",
					"value": 1,
					"data": functions.toObject({
						"lang": null,
						"Printer": null
					})
				});
			},
			"noCallback": async (data, extra) => {
				functions.console({
					"value": "insert..."
				});
				await functions.insert({
					"dataset": "l_settings",
					"dt": functions.toObject({
						"Printer": null,
						"lang": null
					}),
					"callback": "",
					"errCallback": null
				});
			}
		});
		functions.setVar({
			"var": "vPrinter",
			"value": functions.objectAttr({
				"object": await functions.selectBy({
					"by": "_id",
					"value": 1,
					"first": true,
					"callback": null,
					"errCallback": null,
					"dataset": "l_settings"
				}),
				"attr": "Printer"
			})
		});
		functions.setLanguage({
			"lang": null
		});
		await functions.loadData({
			"order": "",
			"callback": async (data, extra) => {
				await functions.clearData({
					"dataset": "l_nav_setting",
					"callback": null,
					"errCallback": null
				});
				await functions.insert({
					"dataset": "l_nav_setting",
					"dt": functions.toObject({
						"company": null,
						"id": 1,
						"url": null,
						"username": null,
						"password": null
					}),
					"callback": "",
					"errCallback": ""
				});
				functions.userDefined('globalModalHide', {});
				functions.gotoPage({
					"p": "pgLogin"
				});
			},
			"errCallback": async (data, extra) => {
				functions.setVar({
					"var": "error",
					"value": data["err"]
				});
				functions.setVar({
					"var": "v_def_setting",
					"value": await functions.selectBy({
						"by": "id",
						"value": 1,
						"first": true,
						"callback": null,
						"errCallback": null,
						"dataset": "l_nav_setting"
					})
				});
				functions.userDefined('globalModalInfo', {
					"title": "Connection Failed",
					"message": "Please check Service URL and Connection.",
					"btnCaption": "Ok"
				});
			},
			"dataset": "OK365_Item_List",
			"limit": 1,
			"filter": functions.toArray({
				"value1": "",
				"value2": "",
				"value3": "",
				"value4": "",
				"value5": "",
				"value6": "",
				"value7": "",
				"value8": "",
				"value9": "",
				"value10": ""
			})
		});
	},

	handleLblSubTitleClick: async () => {
		await functions.navCall({
			"connector": "nav",
			"ent": "OK365_ExportImportPicture",
			"function": "GetItemPicture",
			"data": functions.toObject({
				"itemNo": "BRKAYA"
			}),
			"callback": async (data, extra) => {
				functions.console({
					"value": data
				});
			},
			"errCallback": async (data, extra) => {
				functions.console({
					"value": functions.objectAttr({
						"object": functions.objectAttr({
							"attr": "err",
							"object": data
						}),
						"attr": "global"
					})
				});
			}
		});
	},

	handleImgFingerprint609317Click: async () => {

	},

	handleLblTouchSensor270401Click: async () => {

	},

	handlePnlModalBodyButtonPositiveClick: async () => {
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "vAction"
				}),
				"value2": "delete_item"
			}),
			"yesCallback": async (data, extra) => {
				await functions.deleteBy({
					"dataset": "l_cart",
					"by": "ItemCode",
					"value": functions.getVarAttr({
						"var": "vItemDetails",
						"attr": "ItemCode"
					}),
					"callback": async (data, extra) => {
						functions.hideElement({
							"component": "pnlModal"
						});
					},
					"errCallback": ""
				});
				functions.toArray({
					"value1": await functions.loadData({
						"dataset": "l_cart",
						"callback": async (data, extra) => {
							functions.setVar({
								"var": "vTotalAmountItem",
								"value": 0
							});
							functions.map({
								"values": await functions.selectAll({
									"dataset": "l_cart",
									"callback": "",
									"errCallback": null
								}),
								"callback": async (data, extra) => {
									functions.console({
										"value": data
									});
									functions.setVar({
										"var": "vItemDetails_Order",
										"value": data
									});
									functions.setVar({
										"var": "item_amount",
										"value": functions.multi({
											"value1": functions.getVarAttr({
												"var": "vItemDetails_Order",
												"attr": "Quantity"
											}),
											"value2": functions.getVarAttr({
												"var": "vItemDetails_Order",
												"attr": "Price"
											})
										})
									});
									functions.setVar({
										"var": "vTotalAmountItem",
										"value": functions.add({
											"value1": functions.getVar({
												"var": "vTotalAmountItem",
												"default": ""
											}),
											"value2": functions.getVar({
												"var": "item_amount",
												"default": ""
											})
										})
									});
								}
							});
						},
						"errCallback": ""
					}),
					"value2": await functions.loadData({
						"dataset": "l_empty_bottle",
						"callback": async (data, extra) => {
							functions.console({
								"value": data
							});
							functions.setVar({
								"var": "vTotalAmountEmpty",
								"value": 0
							});
							functions.map({
								"values": await functions.selectAll({
									"dataset": "l_empty_bottle",
									"callback": "",
									"errCallback": ""
								}),
								"callback": async (data, extra) => {
									functions.console({
										"value": data
									});
									functions.setVar({
										"var": "vEmptyDetails_Order_Amount",
										"value": data
									});
									functions.setVar({
										"var": "item_amount_empty",
										"value": functions.multi({
											"value1": functions.getVarAttr({
												"var": "vEmptyDetails_Order_Amount",
												"attr": "quantity"
											}),
											"value2": functions.getVarAttr({
												"var": "vEmptyDetails_Order_Amount",
												"attr": "price"
											})
										})
									});
									functions.setVar({
										"var": "vTotalAmountEmpty",
										"value": functions.add({
											"value1": functions.getVar({
												"var": "vTotalAmountEmpty",
												"default": ""
											}),
											"value2": functions.getVar({
												"var": "item_amount_empty",
												"default": ""
											})
										})
									});
									functions.console({
										"value": functions.getVar({
											"var": "item_amount_empty"
										})
									});
									functions.console({
										"value": functions.getVar({
											"var": "vTotalAmountEmpty",
											"default": ""
										})
									});
								}
							});
						},
						"errCallback": ""
					}),
					"value3": functions.userDefined('globalCalculateTotal', {
						"subTotal": functions.getVar({
							"var": "vTotalAmountItem"
						}),
						"emptyTotal": functions.getVar({
							"var": "vTotalAmountEmpty"
						}),
						"gstRate": functions.getVar({
							"var": "vGstTotalRate"
						})
					}),
					"value4": "",
					"value5": "",
					"value6": "",
					"value7": "",
					"value8": "",
					"value9": "",
					"value10": ""
				});
			},
			"noCallback": async (data, extra) => {
				functions.conditional({
					"condition": functions.equal({
						"value1": functions.getVar({
							"var": "vAction"
						}),
						"value2": "clear_return_cart"
					}),
					"yesCallback": async (data, extra) => {
						await functions.deleteBy({
							"by": "name",
							"value": functions.getVarAttr({
								"var": "vGL_Group",
								"attr": "name"
							}),
							"callback": async (data, extra) => {
								functions.hideElement({
									"component": "pnlModal"
								});
							},
							"errCallback": "",
							"dataset": "l_empty_bottle"
						});
						functions.toArray({
							"value1": await functions.loadData({
								"dataset": "l_cart",
								"callback": async (data, extra) => {
									functions.setVar({
										"var": "vTotalAmountItem",
										"value": 0
									});
									functions.map({
										"values": await functions.selectAll({
											"dataset": "l_cart",
											"callback": "",
											"errCallback": null
										}),
										"callback": async (data, extra) => {
											functions.console({
												"value": data
											});
											functions.setVar({
												"var": "vItemDetails_Order",
												"value": data
											});
											functions.setVar({
												"var": "item_amount",
												"value": functions.multi({
													"value1": functions.getVarAttr({
														"var": "vItemDetails_Order",
														"attr": "Quantity"
													}),
													"value2": functions.getVarAttr({
														"var": "vItemDetails_Order",
														"attr": "Price"
													})
												})
											});
											functions.setVar({
												"var": "vTotalAmountItem",
												"value": functions.add({
													"value2": functions.getVar({
														"var": "item_amount",
														"default": ""
													}),
													"value1": functions.getVar({
														"var": "vTotalAmountItem",
														"default": ""
													})
												})
											});
										}
									});
								},
								"errCallback": ""
							}),
							"value2": await functions.loadData({
								"dataset": "l_empty_bottle",
								"callback": async (data, extra) => {
									functions.console({
										"value": data
									});
									functions.setVar({
										"var": "vTotalAmountEmpty",
										"value": 0
									});
									functions.map({
										"values": await functions.selectAll({
											"dataset": "l_empty_bottle",
											"callback": "",
											"errCallback": ""
										}),
										"callback": async (data, extra) => {
											functions.console({
												"value": data
											});
											functions.setVar({
												"var": "vEmptyDetails_Order_Amount",
												"value": data
											});
											functions.setVar({
												"var": "item_amount_empty",
												"value": functions.multi({
													"value1": functions.getVarAttr({
														"var": "vEmptyDetails_Order_Amount",
														"attr": "quantity"
													}),
													"value2": functions.getVarAttr({
														"var": "vEmptyDetails_Order_Amount",
														"attr": "price"
													})
												})
											});
											functions.setVar({
												"var": "vTotalAmountEmpty",
												"value": functions.add({
													"value1": functions.getVar({
														"var": "vTotalAmountEmpty",
														"default": ""
													}),
													"value2": functions.getVar({
														"var": "item_amount_empty",
														"default": ""
													})
												})
											});
											functions.console({
												"value": functions.getVar({
													"var": "item_amount_empty"
												})
											});
											functions.console({
												"value": functions.getVar({
													"var": "vTotalAmountEmpty",
													"default": ""
												})
											});
										}
									});
								},
								"errCallback": ""
							}),
							"value3": functions.userDefined('globalCalculateTotal', {
								"subTotal": functions.getVar({
									"var": "vTotalAmountItem"
								}),
								"emptyTotal": functions.getVar({
									"var": "vTotalAmountEmpty"
								}),
								"gstRate": functions.getVar({
									"var": "vGstTotalRate"
								})
							}),
							"value4": "",
							"value5": "",
							"value6": "",
							"value7": "",
							"value8": "",
							"value9": "",
							"value10": ""
						});
					},
					"noCallback": async (data, extra) => {
						functions.conditional({
							"condition": functions.equal({
								"value1": functions.getVar({
									"var": "vAction"
								}),
								"value2": "submit_order"
							}),
							"yesCallback": async (data, extra) => {
								functions.setVar({
									"var": "vAction",
									"value": ""
								});
								functions.newArray({
									"var": "sales_line_data"
								});
								functions.map({
									"values": await functions.selectAll({
										"dataset": "l_cart",
										"callback": "",
										"errCallback": ""
									}),
									"callback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.setVar({
											"var": "vl_cart_total",
											"value": data
										});
										functions.push({
											"var": "sales_line_data",
											"value": functions.toObject({
												"No": functions.getVarAttr({
													"var": "vl_cart_total",
													"attr": "ItemCode"
												}),
												"Quantity": functions.getVarAttr({
													"var": "vl_cart_total",
													"attr": "Quantity"
												}),
												"Unit_Price": functions.getVarAttr({
													"var": "vl_cart_total",
													"attr": "Price"
												}),
												"Type": "Item",
												"Unit_of_Measure": functions.getVarAttr({
													"var": "vl_cart_total",
													"attr": "UOM"
												})
											})
										});
									}
								});
								functions.map({
									"values": await functions.selectAll({
										"dataset": "l_empty_bottle",
										"callback": "",
										"errCallback": ""
									}),
									"callback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.setVar({
											"var": "vl_cart_total_Empty",
											"value": data
										});
										functions.push({
											"var": "sales_line_data",
											"value": functions.toObject({
												"Unit_Price": functions.getVarAttr({
													"var": "vl_cart_total_Empty",
													"attr": "price"
												}),
												"Type": "G_L_Account",
												"No": functions.getVarAttr({
													"var": "vl_cart_total_Empty",
													"attr": "name"
												}),
												"Quantity": functions.multi({
													"value1": functions.getVarAttr({
														"var": "vl_cart_total_Empty",
														"attr": "quantity"
													}),
													"value2": -1
												})
											})
										});
									}
								});
								functions.console({
									"value": functions.getVar({
										"var": "sales_line_data"
									})
								});
								functions.userDefined('globalModalLoading', {});
								await functions.navCall({
									"errCallback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.console({
											"value": 2
										});
									},
									"connector": "nav",
									"ent": "MSC_Order_Entry",
									"function": "Create",
									"data": functions.toObject({
										"SalesLines": functions.toObject({
											"Sales_Order_Line": functions.getVar({
												"var": "sales_line_data"
											})
										}),
										"Shipment_Date": functions.getVarAttr({
											"var": "vCheckout",
											"attr": "InvoiceDate"
										}),
										"Location_Code": functions.objectAttr({
											"attr": "Location_Code",
											"object": functions.getVarAttr({
												"var": "var_loginCnt",
												"attr": 0
											})
										}),
										"Mobile_User_ID": functions.objectAttr({
											"object": functions.getVarAttr({
												"var": "var_loginCnt",
												"attr": 0
											}),
											"attr": "Mobile_Login_User_ID"
										}),
										"Sell_to_Customer_No": functions.getVarAttr({
											"var": "vCurrCustomerDetails",
											"attr": "No"
										}),
										"Order_Date": functions.dbDate({})
									}),
									"callback": async (data, extra) => {
										functions.userDefined('globalModalHide', {});
										functions.console({
											"value": data
										});
										functions.setVar({
											"var": "vOrderSubmitResult",
											"value": data
										});
										functions.setVar({
											"var": "vOrderNo",
											"value": functions.objectAttr({
												"object": functions.getVarAttr({
													"var": "vOrderSubmitResult",
													"attr": "MSC_Order_Entry"
												}),
												"attr": "No"
											})
										});
										functions.console({
											"value": functions.getVar({
												"var": "vOrderNo"
											})
										});
										functions.userDefined('globalModalInfo', {
											"title": "Message",
											"message": functions.concat({
												"string1": "Your order No: ",
												"string2": functions.getVar({
													"var": "vOrderNo"
												}),
												"string3": "  ",
												"string4": "was successfully submitted."
											})
										});
										functions.setVar({
											"var": "vAction",
											"value": "redirect"
										});
										await functions.clearData({
											"dataset": "l_cart",
											"callback": "",
											"errCallback": ""
										});
										await functions.clearData({
											"dataset": "l_empty_bottle",
											"callback": "",
											"errCallback": ""
										});
										functions.setVar({
											"var": "vCurrCustomer"
										});
										functions.setVar({
											"var": "vCurrCustomerAddress"
										});
										functions.setVar({
											"var": "vCurrCustomerDetails"
										});
									}
								});
							},
							"noCallback": async (data, extra) => {
								functions.conditional({
									"condition": functions.equal({
										"value1": functions.getVar({
											"var": "vAction"
										}),
										"value2": "redirect"
									}),
									"yesCallback": async (data, extra) => {
										functions.gotoPage({
											"p": "pgMainMenu"
										});
										functions.setVar({
											"var": "vAction"
										});
									},
									"noCallback": async (data, extra) => {
										functions.conditional({
											"condition": functions.equal({
												"value1": functions.getVar({
													"var": "vAction"
												}),
												"value2": "submitSODraft"
											}),
											"yesCallback": async (data, extra) => {
												functions.setVar({
													"var": "vAction"
												});
											},
											"noCallback": async (data, extra) => {
												functions.userDefined('globalModalHide', {});
											}
										});
									}
								});
							}
						});
					}
				});
			}
		});
	},

	handleLblModalNegativeClick: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalHide', {});
	},

	handlePnlModalBodyButtonNegativeClick: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalHide', {});
	},

	handlePgSettingsLoad: async () => {
		functions.toArray({
			"value1": functions.setComboOptions({
				"combo": "cmbLanguage",
				"comboId": "",
				"data": functions.toArray({
					"value1": functions.toObject({
						"name": "English",
						"id": "English"
					}),
					"value2": functions.toObject({
						"id": "Chinese",
						"name": "Chinese"
					})
				}),
				"valueField": "id",
				"displayField": "name"
			}),
			"value2": "",
			"value3": functions.setComponentValue({
				"component": "cmbLanguage",
				"componentId": "",
				"value": functions.objectAttr({
					"object": await functions.selectBy({
						"dataset": "l_settings",
						"by": "_id",
						"value": 1,
						"first": true,
						"callback": null,
						"errCallback": null
					}),
					"attr": "lang"
				})
			}),
			"value4": "",
			"value5": "",
			"value6": "",
			"value7": "",
			"value8": "",
			"value9": "",
			"value10": ""
		});
		functions.setVar({
			"var": "v_def_setting",
			"value": await functions.selectBy({
				"operator": "",
				"value": 1,
				"first": true,
				"callback": "",
				"errCallback": "",
				"dataset": "l_nav_setting",
				"by": "id"
			})
		});
		functions.conditional({
			"condition": functions.getVar({
				"var": "v_def_setting",
				"default": ""
			}),
			"yesValue": "",
			"noValue": "",
			"extra": "",
			"yesCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtURL",
					"componentId": "",
					"value": functions.getVarAttr({
						"var": "v_def_setting",
						"attr": "url",
						"default": ""
					})
				});
				functions.setComponentValue({
					"component": "txtUsername",
					"componentId": "",
					"value": functions.getVarAttr({
						"var": "v_def_setting",
						"attr": "username"
					})
				});
				functions.setComponentValue({
					"component": "txtPassword",
					"componentId": "",
					"value": functions.getVarAttr({
						"var": "v_def_setting",
						"attr": "password"
					})
				});
				functions.setComponentValue({
					"component": "txtCompany",
					"componentId": "",
					"value": functions.getVarAttr({
						"var": "v_def_setting",
						"attr": "company"
					})
				});
			},
			"noCallback": ""
		});
	}
};

const PgSettings = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;
	formatParentStyleLayout(props);

	useEffect(() => {
		actions.handlePgSettingsLoad();
	}, [currentTime]);
    
    return (
		<Page data={props.pgSettings}>
			<Panel data={props.pnlMain}>
				<Panel data={props.pnlMainHeader}>
					<Panel data={props.pnlMainHeaderLeft}>
						<Panel data={props.pnlMainHeaderLeftTable}>
							<Panel data={props.pnlMainHeaderLeftCell}>
								<Image data={props.imgBack} action={actions.handleImgBackClick} />
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainHeaderMiddle}>
						<Panel data={props.pnlMainHeaderMiddleTable}>
							<Panel data={props.pnlMainHeaderMiddleCell}>
								<Label data={props.lblMainTitle} />
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainHeaderRight} action={actions.handlePnlMainHeaderRightClick}>
						<Panel data={props.pnlMainHeaderRightTable}>
							<Panel data={props.pnlMainHeaderRightCell}>
								<Label data={props.lblSave} />
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.Panel92}>
					<Panel data={props.Panel597}>
						<Label data={props.PrinterOption} />
						<ComboBox data={props.cmbPrinter} functions={functions} />
					</Panel>
					<Panel data={props.Panel597690}>
						<Label data={props.Language938127} />
						<ComboBox data={props.cmbLanguage270628} functions={functions} />
					</Panel>
					<Panel data={props.pnlMainBody}>
						<Panel data={props.pnlHeader}>
							<Panel data={props.pnlHeaderTitle}>
								<Label data={props.lblSubTitle} action={actions.handleLblSubTitleClick} />
							</Panel>
						</Panel>
						<Panel data={props.pnlMainUsername}>
							<Panel data={props.pnlUsernameCaption}>
								<Label data={props.lblUsername} />
							</Panel>
							<Panel data={props.pnlUsernameText}>
								<Edit data={props.txtURL} />
							</Panel>
						</Panel>
						<Panel data={props.pnlMainUsername462}>
							<Panel data={props.pnlUsernameCaption442310}>
								<Label data={props.lblUsername898} />
							</Panel>
							<Panel data={props.pnlUsernameText24294}>
								<Edit data={props.txtUsername} />
							</Panel>
						</Panel>
						<Panel data={props.pnlMainPassword}>
							<Panel data={props.pnlPasswordCaption}>
								<Label data={props.lblPassword} />
							</Panel>
							<Panel data={props.pnlPasswordText}>
								<Edit data={props.txtPassword} />
							</Panel>
						</Panel>
						<Panel data={props.pnlMain4444792}>
							<Image data={props.imgFingerprint609317} action={actions.handleImgFingerprint609317Click} />
							<Label data={props.lblTouchSensor270401} action={actions.handleLblTouchSensor270401Click} />
						</Panel>
						<Panel data={props.pnlMainPassword387}>
							<Panel data={props.pnlPasswordCaption934195}>
								<Label data={props.lblPassword529} />
							</Panel>
							<Panel data={props.pnlPasswordText420574}>
								<Edit data={props.txtCompany} />
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal}>
					<Panel data={props.pnlModalTable}>
						<Panel data={props.pnlModalCell}>
							<Panel data={props.pnlModalBodyMain}>
								<Panel data={props.pnlModalBodyTitle}>
									<Label data={props.lblModalTitle} />
								</Panel>
								<Panel data={props.pnlModalBodyMessage}>
									<Label data={props.lblModalMessage} />
								</Panel>
								<Panel data={props.pnlModalBodyLoading}>
									<Image data={props.imgModalLoading} />
								</Panel>
							</Panel>
							<Panel data={props.pnlModalBodyButtons}>
								<Panel data={props.pnlModalBodyButtonPositive} action={actions.handlePnlModalBodyButtonPositiveClick}>
									<Label data={props.lblModalPositive} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonNegative} action={actions.handlePnlModalBodyButtonNegativeClick}>
									<Label data={props.lblModalNegative} action={actions.handleLblModalNegativeClick} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgSettings);