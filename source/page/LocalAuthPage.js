import React from 'react';
import { connect } from 'react-redux'; 

import Page from '../framework/component/Page';
import Button from '../framework/component/Button';
import Label from '../framework/component/Label';

import { StateName } from '../reducer/data/LocalAuthPage';
import Functions from '../framework/core/Function';

let functions = new Functions(StateName);

const LocalAuthPage = (props) => {
    // Append the dispatch to the function instance
    functions.pageProps = props;

    checkIfDeviceHasTouchSensor = async () => {
        functions.console({
            value: functions.deviceHasTouchSensor({
                callback: (input) => {
                    functions.console({value: input});
                },
                errorCallback: (input) => {
                    functions.console({value: input});
                }
            })
        });
    }

    authTouchSensor = async () => {
        functions.console({
            value: functions.deviceVerifyTouchSensor({
                key: 'Please scan your fingerprint',
                message: 'Use PIN?',
                callback: (input) => {
                    functions.console({
                        value: input
                    });
                }
            })
        });
    };
    
    return (
        <Page data={props.page}>
            <Button data={props.btnHasLocalAuth} action={checkIfDeviceHasTouchSensor}></Button>
            <Label></Label>
            <Button data={props.btnLocalAuth} action={authTouchSensor}></Button>
        </Page>
    )
}

const mapStateToProps = (state) => {
    return { ...state[StateName] }
};

export default connect(mapStateToProps)(LocalAuthPage);