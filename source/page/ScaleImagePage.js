import React from 'react';
import { connect } from 'react-redux';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';

import { StateName } from '../reducer/data/ScaleImagePage';

const ScaleImagePage = (props) => {
    return (
        <Page data={props.page} allProps={props}>
            {/* <Panel data={props.pnlHolder}> */}
                <Image data={props.Image387}></Image>
            {/* </Panel> */}
        </Page>
    )
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] }
};

export default connect(mapStateToProps)(ScaleImagePage);