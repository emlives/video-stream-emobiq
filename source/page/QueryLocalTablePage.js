import React, { useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import Page from '../framework/component/Page';
import Label from '../framework/component/Label';
import Button from '../framework/component/Button';
import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/QueryLocalTablePage';

let functions = new Functions(StateName);

const QueryLocalTablePage = (props) => {

    const prepareData = async () => {
        let todos = [
            {title: "Learn SwiftUI", category:'work', priority:'1', done: false,},
            {title: "Learn Kotlin", category:'work', priority:'2', done: false,},
            {title: "Learn Android", category:'work', priority:'2', done: false,},
            {title: "Go to Gym", category:'wellbeing', priority:'1', done: false,},
            {title: "Clean House", category:'wellbeing', priority:'1', done: false,},
            {title: "Eat on time", category:'wellbeing', priority:'1', done: true,},
            {title: "Watch Mortal Kombat", category:'entertainment', priority:'1', done: false,},
            {title: "Watch Cecil Hotel on Netflix", category:'wellbeing', priority:'3', done: false,},
        ]

        await functions.clearData({
            "dataset": "l_todo",
            "callback": "",
            "errCallback": ""
        });

		await functions.insert({
            "dataset": "l_todo",
            "dt": todos,
            "callback": console.log('success'),
            "errCallback": (error) => console.log(error)
        });
	}

    const onSelectAll = async () => {
        let result = await functions.selectAll({
            "dataset": "l_todo",
            "callback": null,
            "errCallback": null
        })
        console.log('/////////////// selectAll ///////////////', result)
    }

    const onSelectByMulti = async () => {
        const filterArray = [
            { f: 'priority', v: '1', o: '=' },
            // { f: 'category', v: 'work', o: '=' },
        ];
        const orFilterArray = [
            { f: 'done', v: false, o: '=' },
        ];
        let result = await functions.selectByMulti({
            "dataset": "l_todo",
            "first": false,
            "filter": filterArray,
            "orFilter": orFilterArray,
            "callback": null,
            "errCallback": null
        })
        console.log('/////////////// selectByMulti ///////////////', result)
        /*
        {"_id": 4, "category": "wellbeing", "done": false, "priority": "1", "title": "Go to Gym"}
        */
    }

    const onDeleteByMulti = async () => {
        const filterArray = [
            { f: 'category', v: 'wellbeing', o: '=' },
            // { f: 'category', v: 'work', o: '=' },
        ];
        const orFilterArray = [
            { f: 'priority', v: '1', o: '=' },
        ];
        let result = await functions.deleteByMulti({
            "dataset": "l_todo",
            "first": true,
            "filter": filterArray,
            "orFilter": orFilterArray,
            "callback": null,
            "errCallback": null
        })
        console.log('/////////////// deleteByMulti ///////////////', result)
    }

    const onUpdateByMulti = async () => {
        const filterArray = [
            { f: 'priority', v: '1', o: '=' },
            // { f: 'category', v: 'work', o: '=' },
        ];
        const orFilterArray = [
            { f: 'done', v: false, o: '=' },
        ];
        let result = await functions.updateByMulti({
            "dataset": "l_todo",
            "first": false,
            "filter": filterArray,
            "orFilter": orFilterArray,
            "data": {'done': true},
            "callback": null,
            "errCallback": null
        })
        console.log('/////////////// updateByMulti ///////////////', result)

    }

    useEffect(() => {
        prepareData()
    },[]);

    return (
        <Page data={props.page} allProps={props}>
            <Label data={props.lblTitle} />
            <Button data={props.btnSelectByMulti} action={onSelectByMulti}></Button>
            <Button data={props.btnDeleteByMulti} action={onDeleteByMulti}></Button>
            <Button data={props.btnUpdateByMulti} action={onUpdateByMulti}></Button>
            <Button data={props.btnSelectAll} action={onSelectAll}></Button>
        </Page>
    )
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] }
};

export default connect(mapStateToProps)(QueryLocalTablePage);