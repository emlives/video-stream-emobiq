import React, { useEffect }  from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgCheckout';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import DataList from '../framework/component/DataList';
import Edit from '../framework/component/Edit';
import Button from '../framework/component/Button';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handleImgBackClick: async () => {
		functions.gotoPage({
			"p": "pgItemListing"
		});
	},

	handleImage553Click: async () => {
		functions.gotoPage({
			"p": "pgItemListing"
		});
	},

	handlePnlLineFooterEditQtyClick: async () => {
		functions.setVar({
			"var": "vItemDetails",
			"value": data
		});
		await functions.loadData({
			"errCallback": "",
			"dataset": "l_cart",
			"filter": functions.toArray({
				"value1": functions.toObject({
					"f": "ItemCode",
					"o": "=",
					"v": functions.getVarAttr({
						"var": "vItemDetails",
						"attr": "ItemCode"
					})
				}),
				"value2": functions.toObject({
					"f": "UOM",
					"o": "=",
					"v": functions.getVarAttr({
						"var": "vItemDetails",
						"attr": "UOM"
					})
				})
			}),
			"callback": async (data, extra) => {
				functions.console({
					"value": data
				});
				functions.setVar({
					"var": "vCurrCartItem",
					"value": data
				});
				functions.conditional({
					"condition": functions.greater({
						"value2": 0,
						"value1": functions.count({
							"values": data
						})
					}),
					"yesCallback": async (data, extra) => {
						functions.console({
							"value": "Yes"
						});
						functions.toArray({
							"value1": functions.setComponentValue({
								"component": "imgUpdateCartImage",
								"value": functions.getVarAttr({
									"var": "vItemDetails",
									"attr": "GetPicture"
								})
							}),
							"value2": functions.setComponentValue({
								"component": "lblUpdateCartItemCodeValueCO",
								"value": functions.getVarAttr({
									"var": "vItemDetails",
									"attr": "ItemCode"
								})
							}),
							"value3": functions.setComponentValue({
								"component": "lblUpdateCartItemNameValueCO",
								"value": functions.getVarAttr({
									"var": "vItemDetails",
									"attr": "ItemName"
								})
							}),
							"value4": functions.setComponentValue({
								"component": "lblUpdateCartPriceValueCO",
								"value": functions.concat({
									"string1": "$ ",
									"string2": functions.formatNumber({
										"value": functions.getVar({
											"var": "var_itemSalesPriceSelected"
										}),
										"decimals": 2
									})
								})
							}),
							"value5": functions.setComponentValue({
								"component": "txtQuantity",
								"value": functions.objectAttr({
									"object": functions.getVarAttr({
										"var": "vCurrCartItem",
										"attr": 0
									}),
									"attr": "Quantity"
								})
							}),
							"value6": functions.setComponentValue({
								"component": "lblUOMValue",
								"value": functions.getVarAttr({
									"var": "vItemDetails",
									"attr": "UOM"
								})
							}),
							"value7": null,
							"value8": functions.showElement({
								"component": "pnlUpdateCartModalCheckout"
							}),
							"value9": "",
							"value10": ""
						});
					},
					"noCallback": ""
				});
			}
		});
	},

	handlePnlLineFooterRemoveClick: async () => {
		functions.setVar({
			"var": "vAction",
			"value": "delete_item"
		});
		functions.setVar({
			"var": "vItemDetails",
			"value": data
		});
		functions.userDefined('globalModalQuestion', {
			"message": "Are you sure you want to delete this item?",
			"title": functions.concat({
				"string1": "Delete Item for ",
				"string2": functions.getVarAttr({
					"var": "vItemDetails",
					"attr": "ItemCode"
				})
			})
		});
	},

	handlePnlMainFooterTblCell1Click: async () => {
		functions.conditional({
			"condition": functions.greater({
				"value1": functions.count({
					"values": await functions.selectAll({
						"dataset": "l_cart",
						"callback": null,
						"errCallback": null
					})
				}),
				"value2": 0
			}),
			"yesCallback": async (data, extra) => {
				functions.setVar({
					"var": "vAction",
					"value": "submit_order"
				});
				functions.userDefined('globalModalQuestion', {
					"title": "Submit Order",
					"message": "Are you sure to proceed?"
				});
			},
			"noCallback": async (data, extra) => {
				functions.userDefined('globalModalInfo', {
					"title": "Empty Field",
					"message": "Please add item into a cart."
				});
			}
		});
	},

	handlePnlModalBodyButtonPositiveClick: async () => {
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "vAction"
				}),
				"value2": "delete_item"
			}),
			"yesCallback": async (data, extra) => {
				await functions.deleteBy({
					"callback": async (data, extra) => {
						functions.hideElement({
							"component": "pnlModal"
						});
					},
					"errCallback": "",
					"dataset": "l_cart",
					"by": "_id",
					"value": functions.getVarAttr({
						"var": "vItemDetails",
						"attr": "_id"
					})
				});
				functions.toArray({
					"value1": await functions.loadData({
						"dataset": "l_cart",
						"callback": async (data, extra) => {
							functions.setVar({
								"var": "vTotalAmountItem",
								"value": 0
							});
							functions.map({
								"values": await functions.selectAll({
									"dataset": "l_cart",
									"callback": "",
									"errCallback": null
								}),
								"callback": async (data, extra) => {
									functions.console({
										"value": data
									});
									functions.setVar({
										"var": "vItemDetails_Order",
										"value": data
									});
									functions.setVar({
										"var": "item_amount",
										"value": functions.multi({
											"value1": functions.getVarAttr({
												"var": "vItemDetails_Order",
												"attr": "Quantity"
											}),
											"value2": functions.getVarAttr({
												"var": "vItemDetails_Order",
												"attr": "Price"
											})
										})
									});
									functions.setVar({
										"var": "vTotalAmountItem",
										"value": functions.add({
											"value1": functions.getVar({
												"var": "vTotalAmountItem",
												"default": ""
											}),
											"value2": functions.getVar({
												"var": "item_amount",
												"default": ""
											})
										})
									});
								}
							});
						},
						"errCallback": ""
					}),
					"value2": await functions.loadData({
						"dataset": "l_empty_bottle",
						"callback": async (data, extra) => {
							functions.console({
								"value": data
							});
							functions.setVar({
								"var": "vTotalAmountEmpty",
								"value": 0
							});
							functions.map({
								"values": await functions.selectAll({
									"dataset": "l_empty_bottle",
									"callback": "",
									"errCallback": ""
								}),
								"callback": async (data, extra) => {
									functions.console({
										"value": data
									});
									functions.setVar({
										"var": "vEmptyDetails_Order_Amount",
										"value": data
									});
									functions.setVar({
										"var": "item_amount_empty",
										"value": functions.multi({
											"value1": functions.getVarAttr({
												"var": "vEmptyDetails_Order_Amount",
												"attr": "quantity"
											}),
											"value2": functions.getVarAttr({
												"var": "vEmptyDetails_Order_Amount",
												"attr": "price"
											})
										})
									});
									functions.setVar({
										"var": "vTotalAmountEmpty",
										"value": functions.add({
											"value1": functions.getVar({
												"var": "vTotalAmountEmpty",
												"default": ""
											}),
											"value2": functions.getVar({
												"var": "item_amount_empty",
												"default": ""
											})
										})
									});
									functions.console({
										"value": functions.getVar({
											"var": "item_amount_empty"
										})
									});
									functions.console({
										"value": functions.getVar({
											"var": "vTotalAmountEmpty",
											"default": ""
										})
									});
								}
							});
						},
						"errCallback": ""
					}),
					"value3": functions.userDefined('globalCalculateTotal', {
						"subTotal": functions.getVar({
							"var": "vTotalAmountItem"
						}),
						"emptyTotal": functions.getVar({
							"var": "vTotalAmountEmpty"
						}),
						"gstRate": functions.getVar({
							"var": "vGstTotalRate"
						})
					}),
					"value4": "",
					"value5": "",
					"value6": "",
					"value7": "",
					"value8": "",
					"value9": "",
					"value10": ""
				});
			},
			"noCallback": async (data, extra) => {
				functions.conditional({
					"condition": functions.equal({
						"value1": functions.getVar({
							"var": "vAction"
						}),
						"value2": "clear_return_cart"
					}),
					"yesCallback": async (data, extra) => {
						await functions.deleteBy({
							"dataset": "l_empty_bottle",
							"by": "name",
							"value": functions.getVarAttr({
								"var": "vGL_Group",
								"attr": "name"
							}),
							"callback": async (data, extra) => {
								functions.hideElement({
									"component": "pnlModal"
								});
							},
							"errCallback": ""
						});
						functions.toArray({
							"value1": await functions.loadData({
								"dataset": "l_cart",
								"callback": async (data, extra) => {
									functions.setVar({
										"var": "vTotalAmountItem",
										"value": 0
									});
									functions.map({
										"values": await functions.selectAll({
											"callback": "",
											"errCallback": null,
											"dataset": "l_cart"
										}),
										"callback": async (data, extra) => {
											functions.console({
												"value": data
											});
											functions.setVar({
												"var": "vItemDetails_Order",
												"value": data
											});
											functions.setVar({
												"var": "item_amount",
												"value": functions.multi({
													"value1": functions.getVarAttr({
														"var": "vItemDetails_Order",
														"attr": "Quantity"
													}),
													"value2": functions.getVarAttr({
														"var": "vItemDetails_Order",
														"attr": "Price"
													})
												})
											});
											functions.setVar({
												"var": "vTotalAmountItem",
												"value": functions.add({
													"value1": functions.getVar({
														"var": "vTotalAmountItem",
														"default": ""
													}),
													"value2": functions.getVar({
														"var": "item_amount",
														"default": ""
													})
												})
											});
										}
									});
								},
								"errCallback": ""
							}),
							"value2": await functions.loadData({
								"dataset": "l_empty_bottle",
								"callback": async (data, extra) => {
									functions.console({
										"value": data
									});
									functions.setVar({
										"var": "vTotalAmountEmpty",
										"value": 0
									});
									functions.map({
										"values": await functions.selectAll({
											"dataset": "l_empty_bottle",
											"callback": "",
											"errCallback": ""
										}),
										"callback": async (data, extra) => {
											functions.console({
												"value": data
											});
											functions.setVar({
												"var": "vEmptyDetails_Order_Amount",
												"value": data
											});
											functions.setVar({
												"var": "item_amount_empty",
												"value": functions.multi({
													"value1": functions.getVarAttr({
														"var": "vEmptyDetails_Order_Amount",
														"attr": "quantity"
													}),
													"value2": functions.getVarAttr({
														"var": "vEmptyDetails_Order_Amount",
														"attr": "price"
													})
												})
											});
											functions.setVar({
												"var": "vTotalAmountEmpty",
												"value": functions.add({
													"value1": functions.getVar({
														"var": "vTotalAmountEmpty",
														"default": ""
													}),
													"value2": functions.getVar({
														"var": "item_amount_empty",
														"default": ""
													})
												})
											});
											functions.console({
												"value": functions.getVar({
													"var": "item_amount_empty"
												})
											});
											functions.console({
												"value": functions.getVar({
													"var": "vTotalAmountEmpty",
													"default": ""
												})
											});
										}
									});
								},
								"errCallback": ""
							}),
							"value3": functions.userDefined('globalCalculateTotal', {
								"subTotal": functions.getVar({
									"var": "vTotalAmountItem"
								}),
								"emptyTotal": functions.getVar({
									"var": "vTotalAmountEmpty"
								}),
								"gstRate": functions.getVar({
									"var": "vGstTotalRate"
								})
							}),
							"value4": "",
							"value5": "",
							"value6": "",
							"value7": "",
							"value8": "",
							"value9": "",
							"value10": ""
						});
					},
					"noCallback": async (data, extra) => {
						functions.conditional({
							"condition": functions.equal({
								"value1": functions.getVar({
									"var": "vAction"
								}),
								"value2": "submit_order"
							}),
							"yesCallback": async (data, extra) => {
								functions.setVar({
									"var": "vAction",
									"value": ""
								});
								functions.newArray({
									"var": "sales_line_data"
								});
								functions.map({
									"values": await functions.selectAll({
										"dataset": "l_cart",
										"callback": "",
										"errCallback": ""
									}),
									"callback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.setVar({
											"var": "vl_cart_total",
											"value": data
										});
										functions.push({
											"var": "sales_line_data",
											"value": functions.toObject({
												"No": functions.getVarAttr({
													"var": "vl_cart_total",
													"attr": "ItemCode"
												}),
												"Quantity": functions.getVarAttr({
													"var": "vl_cart_total",
													"attr": "Quantity"
												}),
												"Unit_Price": functions.getVarAttr({
													"var": "vl_cart_total",
													"attr": "Price"
												}),
												"Type": "Item",
												"Unit_of_Measure": functions.getVarAttr({
													"var": "vl_cart_total",
													"attr": "UOM"
												})
											})
										});
									}
								});
								functions.map({
									"values": await functions.selectAll({
										"dataset": "l_empty_bottle",
										"callback": "",
										"errCallback": ""
									}),
									"callback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.setVar({
											"var": "vl_cart_total_Empty",
											"value": data
										});
										functions.push({
											"var": "sales_line_data",
											"value": functions.toObject({
												"Type": "G_L_Account",
												"No": functions.getVarAttr({
													"var": "vl_cart_total_Empty",
													"attr": "name"
												}),
												"Quantity": functions.multi({
													"value1": functions.getVarAttr({
														"var": "vl_cart_total_Empty",
														"attr": "quantity"
													}),
													"value2": -1
												}),
												"Unit_Price": functions.getVarAttr({
													"var": "vl_cart_total_Empty",
													"attr": "price"
												})
											})
										});
									}
								});
								functions.console({
									"value": functions.getVar({
										"var": "sales_line_data"
									})
								});
								functions.userDefined('globalModalLoading', {});
								await functions.navCall({
									"connector": "nav",
									"ent": "OK365_Order_Entry",
									"function": "Create",
									"data": functions.toObject({
										"Shipment_Date": functions.getVarAttr({
											"var": "vCheckout",
											"attr": "InvoiceDate"
										}),
										"Location_Code": functions.objectAttr({
											"object": functions.getVarAttr({
												"var": "var_loginCnt",
												"attr": 0
											}),
											"attr": "Location_Code"
										}),
										"Mobile_User_ID_OkFldSls": functions.objectAttr({
											"object": functions.getVarAttr({
												"var": "var_loginCnt",
												"attr": 0
											}),
											"attr": "Mobile_Login_User_ID"
										}),
										"Sell_to_Customer_No": functions.getVarAttr({
											"var": "vCurrCustomerDetails",
											"attr": "No"
										}),
										"Order_Date": functions.dbDate({}),
										"SalesLines": functions.toObject({
											"Sales_Order_Line": functions.getVar({
												"var": "sales_line_data"
											})
										})
									}),
									"callback": async (data, extra) => {
										functions.setVar({
											"var": "vOrderEntryResult",
											"value": functions.objectAttr({
												"object": data,
												"attr": "OK365_Order_Entry"
											})
										});
										functions.setVar({
											"var": "vOrderEntryLineResult",
											"value": functions.objectAttr({
												"object": functions.objectAttr({
													"object": functions.objectAttr({
														"object": data,
														"attr": "OK365_Order_Entry"
													}),
													"attr": "SalesLines"
												}),
												"attr": "Sales_Order_Line"
											})
										});
										await functions.dataFromString({
											"string": functions.getVar({
												"var": "vOrderEntryLineResult"
											}),
											"callback": "",
											"errCallback": "",
											"dataset": "l_OrderLines"
										});
										await functions.dataFromString({
											"errCallback": null,
											"dataset": "l_OrderEmptyLines",
											"string": functions.getVar({
												"var": "vOrderEntryLineResult"
											}),
											"callback": null
										});
										functions.userDefined('globalModalHide', {});
										functions.console({
											"value": data
										});
										functions.setVar({
											"var": "vOrderSubmitResult",
											"value": data
										});
										functions.setVar({
											"var": "vOrderNo",
											"value": functions.objectAttr({
												"object": functions.getVarAttr({
													"var": "vOrderSubmitResult",
													"attr": "OK365_Order_Entry"
												}),
												"attr": "No"
											})
										});
										functions.console({
											"value": functions.getVar({
												"var": "vOrderNo"
											})
										});
										functions.userDefined('globalModalInfo', {
											"title": "Message",
											"message": functions.concat({
												"string1": "Your order No",
												"string2": ": ",
												"string3": functions.getVar({
													"var": "vOrderNo"
												}),
												"string4": "was successfully submitted."
											})
										});
										functions.setVar({
											"var": "vAction",
											"value": "redirect"
										});
										await functions.clearData({
											"dataset": "l_cart",
											"callback": "",
											"errCallback": ""
										});
										await functions.clearData({
											"dataset": "l_empty_bottle",
											"callback": "",
											"errCallback": ""
										});
										functions.setVar({
											"var": "vAction",
											"value": "goto_invoice"
										});
										functions.userDefined('globalModalQuestion', {
											"title": "Order Created",
											"message": functions.concat({
												"string1": "Your order No",
												"string2": ":<br><b>",
												"string3": functions.getVar({
													"var": "vOrderNo"
												}),
												"string4": functions.concat({
													"string1": "</b><br>",
													"string2": "was successfully created.",
													"string3": "<br>",
													"string4": "Do you want to proceed to Invoice?"
												})
											})
										});
									},
									"errCallback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.console({
											"value": 2
										});
										functions.setVar({
											"var": "vAction",
											"value": ""
										});
										functions.userDefined('globalModalInfo', {
											"title": "Connection Error",
											"message": "Connection to server failed, please try it again."
										});
									}
								});
							},
							"noCallback": async (data, extra) => {
								functions.conditional({
									"condition": functions.equal({
										"value1": functions.getVar({
											"var": "vAction"
										}),
										"value2": "redirect"
									}),
									"yesCallback": async (data, extra) => {
										functions.gotoPage({
											"p": "pgMainMenu"
										});
										functions.setVar({
											"var": "vAction"
										});
									},
									"noCallback": async (data, extra) => {
										functions.conditional({
											"condition": functions.equal({
												"value1": functions.getVar({
													"var": "vAction"
												}),
												"value2": "submitSODraft"
											}),
											"yesCallback": async (data, extra) => {
												functions.setVar({
													"var": "vAction"
												});
											},
											"noCallback": async (data, extra) => {
												functions.conditional({
													"condition": functions.equal({
														"value1": functions.getVar({
															"var": "vAction"
														}),
														"value2": "goto_invoice"
													}),
													"yesCallback": async (data, extra) => {
														functions.setVar({
															"var": "vAction",
															"value": ""
														});
														functions.setVar({
															"var": "vNewCheckout",
															"value": "true"
														});
														functions.gotoPage({
															"p": "pgSummary"
														});
													},
													"noCallback": async (data, extra) => {
														functions.conditional({
															"condition": functions.equal({
																"value1": functions.getVar({
																	"var": "vAction"
																}),
																"value2": "submit_order_TEST"
															}),
															"yesCallback": async (data, extra) => {
																functions.setVar({
																	"var": "vAction",
																	"value": ""
																});
																functions.newArray({
																	"var": "sales_line_data"
																});
																functions.map({
																	"values": await functions.selectAll({
																		"callback": "",
																		"errCallback": "",
																		"dataset": "l_cart"
																	}),
																	"callback": async (data, extra) => {
																		functions.console({
																			"value": data
																		});
																		functions.setVar({
																			"var": "vl_cart_total",
																			"value": data
																		});
																		functions.push({
																			"var": "sales_line_data",
																			"value": functions.toObject({
																				"Unit_of_Measure": functions.getVarAttr({
																					"var": "vl_cart_total",
																					"attr": "UOM"
																				}),
																				"Quantity": functions.toFloat({
																					"value": functions.getVarAttr({
																						"var": "vl_cart_total",
																						"attr": "Quantity"
																					})
																				}),
																				"No": functions.getVarAttr({
																					"var": "vl_cart_total",
																					"attr": "ItemCode"
																				}),
																				"Type": "Item"
																			})
																		});
																	}
																});
																functions.map({
																	"values": await functions.selectAll({
																		"dataset": "l_empty_bottle",
																		"callback": "",
																		"errCallback": ""
																	}),
																	"callback": async (data, extra) => {
																		functions.console({
																			"value": data
																		});
																		functions.setVar({
																			"var": "vl_cart_total_Empty",
																			"value": data
																		});
																		functions.push({
																			"var": "sales_line_data",
																			"value": functions.toObject({
																				"No": functions.getVarAttr({
																					"var": "vl_cart_total_Empty",
																					"attr": "name"
																				}),
																				"Quantity": functions.multi({
																					"value1": functions.getVarAttr({
																						"var": "vl_cart_total_Empty",
																						"attr": "quantity"
																					}),
																					"value2": -1
																				}),
																				"Unit_Price": functions.getVarAttr({
																					"var": "vl_cart_total_Empty",
																					"attr": "price"
																				}),
																				"Type": "G_L_Account"
																			})
																		});
																	}
																});
																functions.console({
																	"value": functions.getVar({
																		"var": "sales_line_data"
																	})
																});
																functions.userDefined('globalModalLoading', {});
																await functions.navCall({
																	"errCallback": async (data, extra) => {
																		functions.console({
																			"value": data
																		});
																		functions.console({
																			"value": 2
																		});
																		functions.setVar({
																			"var": "vAction",
																			"value": ""
																		});
																		functions.userDefined('globalModalInfo', {
																			"title": "Connection Error",
																			"message": "Connection to server failed, please try it again."
																		});
																	},
																	"connector": "nav",
																	"ent": "OK365_Order_Entry",
																	"function": "Create",
																	"data": functions.toObject({
																		"Sell_to_Customer_No": functions.getVarAttr({
																			"var": "vCurrCustomerDetails",
																			"attr": "No"
																		}),
																		"Order_Date": functions.dbDate({}),
																		"SalesLines": functions.toObject({
																			"Sales_Order_Line": functions.getVar({
																				"var": "sales_line_data"
																			})
																		}),
																		"Location_Code": functions.objectAttr({
																			"object": functions.getVarAttr({
																				"var": "var_loginCnt",
																				"attr": 0
																			}),
																			"attr": "Location_Code"
																		}),
																		"Mobile_User_ID_OkFldSls": functions.objectAttr({
																			"object": functions.getVarAttr({
																				"var": "var_loginCnt",
																				"attr": 0
																			}),
																			"attr": "Mobile_Login_User_ID"
																		})
																	}),
																	"callback": async (data, extra) => {
																		functions.setVar({
																			"var": "vOrderEntryResult",
																			"value": functions.objectAttr({
																				"object": data,
																				"attr": "OK365_Order_Entry"
																			})
																		});
																		functions.setVar({
																			"var": "vOrderEntryLineResult",
																			"value": functions.objectAttr({
																				"object": functions.objectAttr({
																					"object": functions.objectAttr({
																						"object": data,
																						"attr": "OK365_Order_Entry"
																					}),
																					"attr": "SalesLines"
																				}),
																				"attr": "Sales_Order_Line"
																			})
																		});
																		await functions.dataFromString({
																			"dataset": "l_OrderLines",
																			"string": functions.getVar({
																				"var": "vOrderEntryLineResult"
																			}),
																			"callback": "",
																			"errCallback": ""
																		});
																		await functions.dataFromString({
																			"dataset": "l_OrderEmptyLines",
																			"string": functions.getVar({
																				"var": "vOrderEntryLineResult"
																			}),
																			"callback": null,
																			"errCallback": null
																		});
																		functions.userDefined('globalModalHide', {});
																		functions.console({
																			"value": data
																		});
																		functions.setVar({
																			"var": "vOrderSubmitResult",
																			"value": data
																		});
																		functions.setVar({
																			"var": "vOrderNo",
																			"value": functions.objectAttr({
																				"object": functions.getVarAttr({
																					"var": "vOrderSubmitResult",
																					"attr": "OK365_Order_Entry"
																				}),
																				"attr": "No"
																			})
																		});
																		functions.console({
																			"value": functions.getVar({
																				"var": "vOrderNo"
																			})
																		});
																		functions.userDefined('globalModalInfo', {
																			"message": functions.concat({
																				"string1": "Your order No",
																				"string2": ": ",
																				"string3": functions.getVar({
																					"var": "vOrderNo"
																				}),
																				"string4": "was successfully submitted."
																			}),
																			"title": "Message"
																		});
																		functions.setVar({
																			"var": "vAction",
																			"value": "redirect"
																		});
																		await functions.clearData({
																			"dataset": "l_cart",
																			"callback": "",
																			"errCallback": ""
																		});
																		await functions.clearData({
																			"dataset": "l_empty_bottle",
																			"callback": "",
																			"errCallback": ""
																		});
																		functions.setVar({
																			"var": "vAction",
																			"value": "goto_invoice"
																		});
																		functions.userDefined('globalModalQuestion', {
																			"title": "Order Created",
																			"message": functions.concat({
																				"string1": "Your order No",
																				"string2": ":<br><b>",
																				"string3": functions.getVar({
																					"var": "vOrderNo"
																				}),
																				"string4": functions.concat({
																					"string1": "</b><br>",
																					"string2": "was successfully created.",
																					"string3": "<br>",
																					"string4": "Do you want to proceed to Invoice?"
																				})
																			})
																		});
																	}
																});
															},
															"noCallback": async (data, extra) => {
																functions.userDefined('globalModalHide', {});
															}
														});
													}
												});
											}
										});
									}
								});
							}
						});
					}
				});
			}
		});
	},

	handlePnlModalBodyButtonNegativeClick: async () => {
		functions.console({
			"value": "Hello"
		});
		functions.console({
			"value": functions.getVar({
				"var": "vAction"
			})
		});
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "vAction"
				}),
				"value2": "goto_invoice"
			}),
			"yesCallback": async (data, extra) => {
				functions.console({
					"value": "Hello yes"
				});
				functions.toArray({
					"value1": functions.setVar({
						"var": "vAction",
						"value": ""
					}),
					"value2": "",
					"value3": "",
					"value4": ""
				});
				functions.gotoPage({
					"p": "pgMainMenu"
				});
			},
			"noCallback": async (data, extra) => {
				functions.console({
					"value": "Hello Cancel"
				});
				functions.setVar({
					"var": "vAction"
				});
				functions.toArray({
					"value1": functions.setVar({
						"var": "vAction",
						"value": ""
					}),
					"value2": functions.setVar({
						"var": "vCurrCustomer"
					}),
					"value3": functions.setVar({
						"var": "vCurrCustomerAddress"
					}),
					"value4": functions.setVar({
						"var": "vCurrCustomerDetails"
					})
				});
				functions.console({
					"value": "Hello yes oh mo"
				});
				functions.userDefined('globalModalHide', {});
			}
		});
	},

	handleImgUpdateCartCloseClick: async () => {
		functions.setComponentValue({
			"component": "txtQuantity",
			"value": ""
		});
		functions.hideElement({
			"component": "pnlUpdateCartModalCheckout"
		});
	},

	handleImgMinusClick: async () => {
		functions.conditional({
			"condition": functions.greater({
				"value1": null,
				"value2": 1
			}),
			"yesCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"value": functions.sub({
						"value1": null,
						"value2": 1
					})
				});
			},
			"noCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"value": 0
				});
			}
		});
	},

	handleTxtQuantityClick: async () => {
		functions.setComponentFocus({
			"component": "txtQuantity",
			"selectAllText": "true"
		});
	},

	handleImgPlusClick: async () => {
		functions.conditional({
			"condition": functions.greater({
				"value1": null,
				"value2": -1
			}),
			"yesCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"value": functions.add({
						"value1": null,
						"value2": 1
					})
				});
			},
			"noCallback": async (data, extra) => {
				functions.setComponentValue({
					"component": "txtQuantity",
					"value": 0
				});
			}
		});
	},

	handleBtnUpdateClick: async () => {
		functions.conditional({
			"condition": null,
			"yesCallback": async (data, extra) => {
				functions.conditional({
					"condition": functions.greater({
						"value1": null,
						"value2": functions.toFloat({
							"value": -1
						})
					}),
					"yesCallback": async (data, extra) => {
						functions.conditional({
							"condition": functions.greater({
								"value1": null,
								"value2": 0
							}),
							"yesCallback": async (data, extra) => {
								await functions.loadData({
									"errCallback": "",
									"dataset": "l_cart",
									"filter": functions.toArray({
										"value1": functions.toObject({
											"o": "=",
											"v": functions.getVarAttr({
												"var": "vItemDetails",
												"attr": "ItemCode"
											}),
											"f": "ItemCode"
										}),
										"value2": functions.toObject({
											"f": "UOM",
											"o": "=",
											"v": functions.getVarAttr({
												"var": "vItemDetails",
												"attr": "UOM"
											})
										})
									}),
									"callback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.setVar({
											"var": "vCurrCartItem",
											"value": data
										});
										functions.conditional({
											"condition": functions.greater({
												"value1": functions.count({
													"values": data
												}),
												"value2": 0
											}),
											"yesCallback": async (data, extra) => {
												functions.console({
													"value": "Yes"
												});
												functions.toArray({
													"value1": await functions.loadData({
														"dataset": "l_cart",
														"callback": async (data, extra) => {
															functions.setVar({
																"var": "vTotalAmountItem",
																"value": 0
															});
															functions.map({
																"values": await functions.selectAll({
																	"dataset": "l_cart",
																	"callback": "",
																	"errCallback": null
																}),
																"callback": async (data, extra) => {
																	functions.console({
																		"value": data
																	});
																	functions.setVar({
																		"var": "vItemDetails_Order",
																		"value": data
																	});
																	functions.setVar({
																		"var": "item_amount",
																		"value": functions.multi({
																			"value1": functions.getVarAttr({
																				"var": "vItemDetails_Order",
																				"attr": "Quantity"
																			}),
																			"value2": functions.getVarAttr({
																				"var": "vItemDetails_Order",
																				"attr": "Price"
																			})
																		})
																	});
																	functions.setVar({
																		"var": "vTotalAmountItem",
																		"value": functions.add({
																			"value1": functions.getVar({
																				"var": "vTotalAmountItem",
																				"default": ""
																			}),
																			"value2": functions.getVar({
																				"var": "item_amount",
																				"default": ""
																			})
																		})
																	});
																}
															});
															functions.setComponentValue({
																"component": "lblTotalAmount_data",
																"value": functions.concat({
																	"string1": "$ ",
																	"string2": functions.formatNumber({
																		"decimals": 2,
																		"decimalSep": ".",
																		"thousandSep": ",",
																		"value": functions.getVar({
																			"var": "vTotalAmountItem"
																		})
																	})
																})
															});
														},
														"errCallback": ""
													}),
													"value2": "",
													"value3": "",
													"value4": "",
													"value5": "",
													"value6": "",
													"value7": "",
													"value8": "",
													"value9": "",
													"value10": ""
												});
												functions.hideElement({
													"component": "pnlUpdateCartModalCheckout"
												});
											},
											"noCallback": ""
										});
									}
								});
							},
							"noCallback": ""
						});
					},
					"noCallback": async (data, extra) => {
						functions.userDefined('globalModalInfo', {
							"title": "Invalid Value",
							"message": "Please enter a valid value/amount."
						});
					}
				});
			},
			"noCallback": async (data, extra) => {
				functions.userDefined('globalModalInfo', {
					"title": "No Value",
					"message": "Enter the quantity."
				});
			}
		});
	},

	handlePnlUpdateCartModalCheckoutFooterNavTableClick: async () => {
		functions.conditional({
			"condition": null,
			"yesCallback": async (data, extra) => {
				functions.conditional({
					"condition": functions.greater({
						"value1": null,
						"value2": functions.toFloat({
							"value": -1
						})
					}),
					"yesCallback": async (data, extra) => {
						functions.conditional({
							"condition": functions.greater({
								"value2": 0,
								"value1": null
							}),
							"yesCallback": async (data, extra) => {
								await functions.loadData({
									"callback": async (data, extra) => {
										functions.console({
											"value": data
										});
										functions.setVar({
											"var": "vCurrCartItem",
											"value": data
										});
										functions.conditional({
											"condition": functions.greater({
												"value1": functions.count({
													"values": data
												}),
												"value2": 0
											}),
											"yesCallback": async (data, extra) => {
												functions.console({
													"value": "Yes"
												});
												functions.toArray({
													"value1": await functions.loadData({
														"dataset": "l_cart",
														"callback": async (data, extra) => {
															functions.setVar({
																"var": "vTotalAmountItem",
																"value": 0
															});
															functions.map({
																"values": await functions.selectAll({
																	"dataset": "l_cart",
																	"callback": "",
																	"errCallback": null
																}),
																"callback": async (data, extra) => {
																	functions.console({
																		"value": data
																	});
																	functions.setVar({
																		"var": "vItemDetails_Order",
																		"value": data
																	});
																	functions.setVar({
																		"var": "item_amount",
																		"value": functions.multi({
																			"value1": functions.getVarAttr({
																				"var": "vItemDetails_Order",
																				"attr": "Quantity"
																			}),
																			"value2": functions.getVarAttr({
																				"var": "vItemDetails_Order",
																				"attr": "Price"
																			})
																		})
																	});
																	functions.setVar({
																		"var": "vTotalAmountItem",
																		"value": functions.add({
																			"value2": functions.getVar({
																				"var": "item_amount",
																				"default": ""
																			}),
																			"value1": functions.getVar({
																				"var": "vTotalAmountItem",
																				"default": ""
																			})
																		})
																	});
																}
															});
															functions.setComponentValue({
																"component": "lblTotalAmount_data",
																"value": functions.concat({
																	"string1": "$ ",
																	"string2": functions.formatNumber({
																		"decimalSep": ".",
																		"thousandSep": ",",
																		"value": functions.getVar({
																			"var": "vTotalAmountItem"
																		}),
																		"decimals": 2
																	})
																})
															});
														},
														"errCallback": ""
													}),
													"value2": "",
													"value3": "",
													"value4": "",
													"value5": "",
													"value6": "",
													"value7": "",
													"value8": "",
													"value9": "",
													"value10": ""
												});
												functions.hideElement({
													"component": "pnlUpdateCartModalCheckout"
												});
											},
											"noCallback": ""
										});
									},
									"errCallback": "",
									"dataset": "l_cart",
									"filter": functions.toArray({
										"value1": functions.toObject({
											"f": "ItemCode",
											"o": "=",
											"v": functions.getVarAttr({
												"var": "vItemDetails",
												"attr": "ItemCode"
											})
										}),
										"value2": functions.toObject({
											"f": "UOM",
											"o": "=",
											"v": functions.getVarAttr({
												"var": "vItemDetails",
												"attr": "UOM"
											})
										})
									})
								});
							},
							"noCallback": ""
						});
					},
					"noCallback": async (data, extra) => {
						functions.userDefined('globalModalInfo', {
							"title": "Invalid Value",
							"message": "Please enter a valid value/amount."
						});
					}
				});
			},
			"noCallback": async (data, extra) => {
				functions.userDefined('globalModalInfo', {
					"message": "Enter the quantity.",
					"title": "No Value"
				});
			}
		});
	},

	handlePgCheckoutLoad: async () => {
		functions.onBackButton({
			"callback": async (data, extra) => {
				functions.gotoPage({
					"p": "pgCart"
				});
			}
		});
		functions.setVar({
			"var": "vAction"
		});
		functions.setVar({
			"var": "vCheckout",
			"value": functions.toObject({})
		});
		functions.userDefined('globalModalLoading', {});
		await functions.loadData({
			"dataset": "OK365_Customer_Card",
			"filter": functions.toArray({
				"value1": functions.toObject({
					"f": "No",
					"o": "=",
					"v": functions.getVarAttr({
						"var": "vCurrCustomerDetails",
						"attr": "No"
					})
				})
			}),
			"callback": async (data, extra) => {
				functions.userDefined('globalModalHide', {});
				functions.console({
					"value": "item details"
				});
				functions.console({
					"value": functions.getVar({
						"var": "vItemDetails"
					})
				});
				functions.setVar({
					"var": "vCurrCustomerAddress",
					"value": functions.objectAttr({
						"attr": "Address",
						"object": functions.objectAttr({
							"object": data,
							"attr": 0
						})
					})
				});
				functions.setComponentValue({
					"component": "txtAddress",
					"value": functions.objectAttr({
						"object": functions.objectAttr({
							"object": data,
							"attr": 0
						}),
						"attr": "Address"
					})
				});
				await functions.loadData({
					"callback": async (data, extra) => {
						functions.console({
							"value": "gst rate"
						});
						functions.console({
							"value": functions.objectAttr({
								"object": functions.objectAttr({
									"object": data,
									"attr": 0
								}),
								"attr": "VAT_Percent"
							})
						});
						functions.setVar({
							"var": "vGstTotalRate",
							"value": functions.objectAttr({
								"object": functions.objectAttr({
									"object": data,
									"attr": 0
								}),
								"attr": "VAT_Percent"
							})
						});
						functions.toArray({
							"value1": await functions.loadData({
								"dataset": "l_cart",
								"callback": async (data, extra) => {
									functions.setVar({
										"var": "vTotalAmountItem",
										"value": 0
									});
									functions.map({
										"values": await functions.selectAll({
											"callback": "",
											"errCallback": null,
											"dataset": "l_cart"
										}),
										"callback": async (data, extra) => {
											functions.console({
												"value": data
											});
											functions.setVar({
												"var": "vItemDetails_Order",
												"value": data
											});
											functions.setVar({
												"var": "item_amount",
												"value": functions.multi({
													"value2": functions.getVarAttr({
														"var": "vItemDetails_Order",
														"attr": "Price"
													}),
													"value1": functions.getVarAttr({
														"var": "vItemDetails_Order",
														"attr": "Quantity"
													})
												})
											});
											functions.setVar({
												"var": "vTotalAmountItem",
												"value": functions.add({
													"value1": functions.getVar({
														"var": "vTotalAmountItem",
														"default": ""
													}),
													"value2": functions.getVar({
														"var": "item_amount",
														"default": ""
													})
												})
											});
										}
									});
								},
								"errCallback": ""
							}),
							"value2": functions.setComponentValue({
								"component": "lblTotalAmount_data",
								"value": functions.concat({
									"string1": "$ ",
									"string2": functions.formatNumber({
										"value": functions.getVar({
											"var": "vTotalAmountItem"
										}),
										"decimals": 2,
										"decimalSep": ".",
										"thousandSep": ","
									})
								})
							}),
							"value3": functions.userDefined('globalCalculateTotal', {
								"gstRate": functions.getVar({
									"var": "vGstTotalRate"
								}),
								"subTotal": functions.getVar({
									"var": "vTotalAmountItem"
								}),
								"emptyTotal": functions.getVar({
									"var": "vTotalAmountEmpty"
								})
							}),
							"value4": "",
							"value5": "",
							"value6": "",
							"value7": "",
							"value8": "",
							"value9": "",
							"value10": ""
						});
					},
					"errCallback": async (data, extra) => {
						functions.setVar({
							"var": "error",
							"value": data["err"]
						});
					},
					"dataset": "OK365_GST_Setup",
					"filter": functions.toArray({
						"value1": functions.toObject({
							"f": "VAT_Prod_Posting_Group",
							"v": functions.getVarAttr({
								"var": "vItemDetails",
								"attr": "VAT_Prod_Posting_Group"
							})
						}),
						"value2": functions.toObject({
							"f": "VAT_Bus_Posting_Group",
							"v": functions.getVarAttr({
								"var": "vCurrCustomerDetails",
								"attr": "VAT_Bus_Posting_Group"
							})
						})
					})
				});
			},
			"errCallback": async (data, extra) => {
				functions.setVar({
					"var": "error",
					"value": data["err"]
				});
				functions.userDefined('globalModalInfo', {
					"title": "Error",
					"message": functions.conditional({
						"condition": functions.getVarAttr({
							"var": "error",
							"attr": "global"
						}),
						"yesValue": functions.getVarAttr({
							"var": "error",
							"attr": "global"
						}),
						"noValue": "Cannot connect to Navision Server."
					})
				});
			}
		});
		functions.toArray({
			"value1": functions.setComponentValue({
				"component": "lblCustomerCodeName",
				"value": functions.concat({
					"string1": functions.getVarAttr({
						"var": "vCurrCustomerDetails",
						"attr": "No"
					}),
					"string2": " - ",
					"string3": functions.getVarAttr({
						"var": "vCurrCustomerDetails",
						"attr": "Name"
					})
				})
			}),
			"value2": functions.setComponentValue({
				"component": "txtCustomerName",
				"value": functions.getVarAttr({
					"var": "vCurrCustomerDetails",
					"attr": "Name"
				})
			}),
			"value3": functions.setComponentValue({
				"component": "lblOrderDate",
				"value": functions.formatDate({
					"format": "Y-m-d",
					"date": functions.dbDate({})
				})
			}),
			"value4": functions.setComponentValue({
				"component": "txtInvoiceDate",
				"value": functions.formatDate({
					"date": functions.dbDate({}),
					"format": "d-m-Y"
				})
			}),
			"value5": functions.setComponentValue({
				"component": "txtRemarks",
				"value": functions.getVarAttr({
					"var": "vCheckout",
					"attr": "Comments"
				})
			}),
			"value6": functions.setComponentValue({
				"component": "lblAddress",
				"value": functions.getVarAttr({
					"var": "vCurrCustomerDetails",
					"attr": "Address"
				})
			})
		});
		functions.hideElement({
			"component": "pnlConfirmPaymentdDetails"
		});
	}
};

const PgCheckout = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;
	formatParentStyleLayout(props);

	useEffect(() => {
		actions.handlePgCheckoutLoad();
	}, [currentTime]);
    
    return (
		<Page data={props.pgCheckout}>
			<Panel data={props.pnlHolder}>
				<Panel data={props.pnlMain}>
					<Panel data={props.pnlMainHeader}>
						<Panel data={props.pnlMainHeaderLeft}>
							<Panel data={props.pnlMainHeaderLeftTable}>
								<Panel data={props.pnlMainHeaderLeftCell}>
									<Image data={props.imgBack} action={actions.handleImgBackClick} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderMiddle458}>
							<Panel data={props.pnlMainHeaderMiddleTable161860}>
								<Panel data={props.pnlMainHeaderMiddleCell31}>
									<Label data={props.lblMainTitle667} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderRight}>
							<Panel data={props.pnlMainHeaderRightTable}>
								<Panel data={props.pnlMainHeaderRightCell} />
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBody}>
						<Panel data={props.pnlMainBodyForm}>
							<Panel data={props.pnlMainBodyFormCustomerCode}>
								<Panel data={props.pnlMainBodyForm3LabelCustomerCode}>
									<Label data={props.lblCustomerCodeName} />
									<Label data={props.lblAddress} />
									<Label data={props.lblOrderDate} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainBodyItemsCO1}>
							<Label data={props.lblItems} />
							<Image data={props.Image553} action={actions.handleImage553Click} />
						</Panel>
						<Panel data={props.pnlMainBodyItems}>
							<Panel data={props.pnlMainBodyItemsCO2}>
								<DataList data={props.dlltem} functions={functions} currentTime={currentTime}>
									{({ item, parentStyle }) => (								
										<Panel data={item.Panel764} parentStyle={parentStyle}>
											<Panel data={item.pnlProduct} parentStyle={parentStyle}>
												<Panel data={item.pnlProductBodyLeft} parentStyle={parentStyle}>
													<Image data={item.imgItem} parentStyle={parentStyle} />
												</Panel>
												<Panel data={item.pnlProductBodyMiddle} parentStyle={parentStyle}>
													<Panel data={item.pnlProductBodyMiddle1} parentStyle={parentStyle}>
														<Label data={item.lblItemCode} parentStyle={parentStyle} />
														<Label data={item.lblItemName} parentStyle={parentStyle} />
														<Label data={item.lblPrice} parentStyle={parentStyle} />
													</Panel>
												</Panel>
											</Panel>
											<Panel data={item.pnlLineFooter} parentStyle={parentStyle}>
												<Panel data={item.pnlLineFooterEditQty} action={actions.handlePnlLineFooterEditQtyClick} parentStyle={parentStyle}>
													<Label data={item.Label191} parentStyle={parentStyle} />
												</Panel>
												<Panel data={item.pnlLineFooterRemove} action={actions.handlePnlLineFooterRemoveClick} parentStyle={parentStyle}>
													<Label data={item.Label561} parentStyle={parentStyle} />
												</Panel>
											</Panel>
										</Panel>
									)}
								</DataList>
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBodyDetails}>
						<Panel data={props.pnlMainBodyDetails1}>
							<Panel data={props.Panel495}>
								<Label data={props.lblTotalAmountExclGST} />
							</Panel>
							<Panel data={props.Panel495500}>
								<Label data={props.lblTotalAmount_data} />
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainFooter}>
						<Panel data={props.pnlMainFooterTbl}>
							<Panel data={props.pnlMainFooterTblCell1} action={actions.handlePnlMainFooterTblCell1Click}>
								<Label data={props.lblBtnOrder} />
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal}>
					<Panel data={props.pnlModalTable}>
						<Panel data={props.pnlModalCell}>
							<Panel data={props.pnlModalBodyMain}>
								<Panel data={props.pnlModalBodyTitle}>
									<Label data={props.lblModalTitle} />
								</Panel>
								<Panel data={props.pnlModalBodyMessage}>
									<Label data={props.lblModalMessage} />
								</Panel>
								<Panel data={props.pnlModalBodyLoading}>
									<Image data={props.imgModalLoading} />
								</Panel>
							</Panel>
							<Panel data={props.pnlModalBodyButtons}>
								<Panel data={props.pnlModalBodyButtonPositive} action={actions.handlePnlModalBodyButtonPositiveClick}>
									<Label data={props.lblModalPositive} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonNegative} action={actions.handlePnlModalBodyButtonNegativeClick}>
									<Label data={props.lblModalNegative} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlCSS}>
					<Label data={props.lblStyle} />
				</Panel>
				<Panel data={props.pnlUpdateCartModalCheckout}>
					<Panel data={props.pnlUpdateCartModalTableCheckout}>
						<Panel data={props.pnlUpdateCartModalCellCheckout}>
							<Panel data={props.pnlUpdateCartModalBodyCheckout}>
								<Panel data={props.Panel513}>
									<Image data={props.imgUpdateCartClose} action={actions.handleImgUpdateCartCloseClick} />
									<Panel data={props.Panel514}>
										<Panel data={props.pnlUpdateCartModalBodyCheckoutImage}>
											<Image data={props.imgUpdateCartImage} />
										</Panel>
										<Panel data={props.pnlUpdateCartModalBodyCheckoutCode}>
											<Label data={props.lblUpdateCartItemCodeValueCO} />
										</Panel>
										<Panel data={props.pnlUpdateCartModalBodyCheckoutName}>
											<Label data={props.lblUpdateCartItemNameValueCO} />
										</Panel>
										<Panel data={props.pnlUpdateCartModalBodyCheckoutPrice}>
											<Label data={props.lblUpdateCartPriceValueCO} />
										</Panel>
									</Panel>
									<Panel data={props.Panel505}>
										<Panel data={props.Panel414}>
											<Panel data={props.pnlUpdateCartModalBodyCheckoutQty}>
												<Panel data={props.pnlUpdateCartModalBodyQtyValueCO}>
													<Panel data={props.pnlUpdateCartModalBodyLeft}>
														<Image data={props.imgMinus} action={actions.handleImgMinusClick} />
													</Panel>
													<Panel data={props.pnlUpdateCartModalBodyMiddle}>
														<Edit data={props.txtQuantity} action={actions.handleTxtQuantityClick} />
													</Panel>
													<Panel data={props.pnlUpdateCartModalBodyRight}>
														<Image data={props.imgPlus} action={actions.handleImgPlusClick} />
													</Panel>
												</Panel>
											</Panel>
											<Panel data={props.pnlUpdateCartModalBodyQtyCO}>
												<Label data={props.lblUpdateCartQuantityIL} />
												<Label data={props.lblUOMValue} />
											</Panel>
										</Panel>
									</Panel>
								</Panel>
								<Panel data={props.pnlUpdateCartModalBodyButtonCheckout}>
									<Button data={props.btnUpdate} action={actions.handleBtnUpdateClick} />
									<Panel data={props.pnlUpdateCartModalCheckoutFooterNavTable} action={actions.handlePnlUpdateCartModalCheckoutFooterNavTableClick}>
										<Panel data={props.pnlUpdateCartModalCheckoutFooterNavTableCell}>
											<Label data={props.pnlFooterNavUpdateBtn} />
										</Panel>
									</Panel>
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgCheckout);