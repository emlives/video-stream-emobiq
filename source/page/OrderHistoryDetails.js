import React, { useEffect, useState, useCallback, useRef } from 'react';
import { View, ScrollView } from 'react-native';
import { connect } from 'react-redux'; 
import Page from '../framework/component/Page';
import Label from '../framework/component/Label';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import DataList from '../framework/component/DataList';
import Button from '../framework/component/Button';

import Functions from '../framework/core/Function';

import { StateName } from '../reducer/data/OrderHistoryDetails';

let functions = new Functions(StateName);

const actions = {
    handleClickInsert: async () => {
        functions.console({
            value: await functions.insert(
                {
                    dataset: 'l_OrderLines',
                    dt: {
                        'No': 'A01-BADAM',
                        'Description': 'Badam Nuts',
                        'disc': 'Disc'
                    },
                    extra: 'Extra data here...',
                    callback: function (input, extra) {
                        functions.console({ value: 'Success:' });
                        functions.console({ value: input });
                        functions.console({ value: extra });
                    },
                    errCallback: function (error, extra) {
                        functions.console({ value: 'Fail:' });
                        functions.console({ value: error });
                        functions.console({ value: extra });
                    }
                }
            )
        });
    },
    handleClickLoadData: async () => {
        functions.console({
            value: await functions.loadData(
                {
                    dataset: "l_OrderLines",
                    // order: [
                    //     {
                    //         "field": "value",
                    //         "order": "asc"
                    //     }
                    // ],
                    // limit: 5,
                    // page: 1,
                    filter: [
                        // {
                        //     "field": "value",
                        //     "o": "=",
                        //     "value": "kevin"
                        // }
                    ],
                    callback: (data) => {
                        functions.console({ value: data });
                    }
                }
            )
        });
    },
}

const OrderHistoryDetails = (props) => {
    const [currentTime, setCurrentTime] = useState(Date.now());
    useEffect(() => { // to trigger render on some components every time screen changes
        const unsubscribe = props.navigation.addListener('focus', () => {
            setCurrentTime(Date.now())
        });
        // Return the function to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [props.navigation]);
    
    // Append the dispatch to the function instance
    functions.pageProps = props;

    return (
        <Page data={props.page} allProps={props}>
            <Panel data={props.pnlHolder}>
                <Panel data={props.pnlMain}>
                    <Panel data={props.pnlMainHeader}>
                        <Panel data={props.pnlMainHeaderLeft}>
                            <Panel data={props.pnlMainHeaderLeftTable}>
                                <Panel data={props.pnlMainHeaderLeftCell}>
                                    <Image data={props.imgBack}/>
                                </Panel>
                            </Panel>
                        </Panel>
                        <Panel data={props.pnlMainHeaderMiddle}>
                            <Panel data={props.pnlMainHeaderMiddleTable}>
                                <Panel data={props.pnlMainHeaderMiddleCell}>
                                    <Label data={props.lblMainTitle}/>
                                </Panel>
                            </Panel>
                        </Panel>
                        <Panel data={props.pnlMainHeaderRight233}>
                            <Panel data={props.pnlMainHeaderRightTable2}>
                                <Panel data={props.pnlMainHeaderRightCell877}>
                                    <Image data={props.imgBack555407}/>
                                </Panel>
                            </Panel>
                        </Panel>
                    </Panel>
                    <Panel data={props.pnlMainBody}>
                        <Panel data={props.pnlMainBodyForm}>
                            <Panel data={props.pnlMainBodyFormCustomerCode}>
                                <Panel data={props.pnlMainBodyForm3LabelCustomerCode}>
                                    <Label data={props.lblOrderNo_data}/>
                                    <Label data={props.lblCustomerCodeName_data}/>
                                    <Label data={props.lblAddress_data}/>
                                    <Label data={props.lblOrderDate_data}/>
                                </Panel>
                            </Panel>
                        </Panel>
                        <Panel data={props.pnlMainBodyItemsCO1}>
                            <Label data={props.lblItems}/>
                        </Panel>
                        <Panel data={props.pnlMainBodyItems}>
                            <Panel data={props.pnlMainBodyItemsC02}>
                                <DataList data={props.dlItem} functions={functions} currentTime = {currentTime}>
                                    {({ item }, parentStyle) =>{ 
                                    return (
                                        <View>
                                            <Panel data={item.pnlProduct} parentStyle={parentStyle}>
                                                <Panel data={item.pnlProductBodyLeft}>
                                                    <Image data={item.imgItem}/>
                                                </Panel>
                                                <Panel data={item.pnlProductBodyMiddle}>
                                                    <Panel data={item.pnlProductBodyMiddle1}>
                                                        <Label data={item.lblItemCode_data}/>
                                                        <Label data={item.lblItemName_data}/>
                                                        <Label data={item.lblItemsQtyUOM}/>
                                                        <Label data={item.lblPrice_data}/>
                                                        <Label data={item.lblDiscCO}/>
                                                    </Panel>
                                                </Panel>
                                            </Panel>
                                        </View>
                                        )
                                    }
                                    }
                                </DataList>
                            </Panel>
                            <Button action={actions.handleClickInsert} data={props.btnInsert}></Button>
                            <Button action={actions.handleClickLoadData} data={props.btnLoadData}></Button>
                        </Panel>
                    </Panel>
                    <Panel data={props.pnlMainBodyDetails}>
                        
                    </Panel>
                </Panel>
            </Panel>
        </Page>
    )
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] }
};

export default connect(mapStateToProps)(OrderHistoryDetails);