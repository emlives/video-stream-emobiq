import React, { useEffect }  from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgTransferStockDetails';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import DataList from '../framework/component/DataList';
import Edit from '../framework/component/Edit';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handleImgMenuClick: async () => {
		functions.gotoPage({
			"p": "pgTransferStocks"
		});
	},

	handleImgMenu236Click: async () => {
		functions.showElement({
			"component": "pnlModalAddItem"
		});
	},

	handlePnlDataContentTblCell2Click: async () => {
		functions.setVar({
			"var": "v_transfer_line_selected",
			"value": data
		});
		functions.toArray({
			"value1": functions.setComponentValue({
				"component": "lblEdtQtyTitle",
				"value": functions.concat({
					"string1": functions.getVarAttr({
						"var": "v_transfer_line_selected",
						"attr": "Item_No"
					}),
					"string2": " - ",
					"string3": functions.getVarAttr({
						"var": "v_transfer_line_selected",
						"attr": "Description"
					})
				})
			}),
			"value2": functions.setComponentValue({
				"component": "edtQty",
				"value": functions.getVarAttr({
					"var": "v_transfer_line_selected",
					"attr": "Quantity"
				})
			}),
			"value3": functions.setComponentValue({
				"component": "lblEditQtyUOM",
				"value": functions.getVarAttr({
					"var": "v_transfer_line_selected",
					"attr": "Unit_of_Measure"
				})
			})
		});
		functions.showElement({
			"component": "pnlModalEditQty"
		});
	},

	handlePnlFooterBtnReleaseClick: async () => {
		functions.setVar({
			"var": "vAction",
			"value": "transfer_order_release"
		});
		functions.userDefined('globalModalQuestion', {
			"title": "Transfer Order",
			"message": functions.concat({
				"string1": "Do you want to release Transfer Order No.",
				"string2": functions.concat({
					"string1": "<br><b>",
					"string2": functions.getVarAttr({
						"var": "v_transfer_order_data",
						"attr": "No"
					}),
					"string3": "</b>",
					"string4": ""
				}),
				"string3": "?"
			})
		});
	},

	handlePnlModalBodyButtonPositiveClick: async () => {
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "vAction"
				}),
				"value2": "transfer_order_release"
			}),
			"yesCallback": async (data, extra) => {
				functions.userDefined('globalModalLoading', {});
				functions.setVar({
					"var": "vAction"
				});
				await functions.navCall({
					"callback": async (data, extra) => {
						functions.console({
							"value": "Called : fnReleaseTransferOrder"
						});
						functions.console({
							"value": data
						});
						functions.setVar({
							"var": "vAction",
							"value": "transfer_order_release_success"
						});
						functions.userDefined('globalModalInfo', {
							"title": "Transfer Order",
							"message": functions.concat({
								"string1": "Transfer Order No. ",
								"string2": functions.concat({
									"string1": "<b>",
									"string2": functions.getVarAttr({
										"var": "v_transfer_order_data",
										"attr": "No"
									}),
									"string3": "</b>",
									"string4": " "
								}),
								"string3": "has been successfully released."
							}),
							"btnCaption": "OK"
						});
					},
					"errCallback": async (data, extra) => {
						functions.userDefined('globalModalHide', {});
						functions.userDefined('globalModalInfo', {
							"title": "Navision Server Error",
							"message": functions.objectAttr({
								"object": functions.objectAttr({
									"object": data,
									"attr": "err"
								}),
								"attr": "global"
							}),
							"btnCaption": "OK"
						});
					},
					"connector": "nav",
					"ent": "OK365_GetItemSalesPrice",
					"function": "fnReleaseTransferOrder",
					"data": functions.toObject({
						"cdeTransferOrdNo": functions.getVarAttr({
							"var": "v_transfer_order_data",
							"attr": "No"
						})
					})
				});
			},
			"noCallback": async (data, extra) => {
				functions.conditional({
					"condition": functions.equal({
						"value1": functions.getVar({
							"var": "vAction"
						}),
						"value2": "transfer_order_release_success"
					}),
					"yesCallback": async (data, extra) => {
						functions.setVar({
							"var": "vAction"
						});
						await functions.clearData({
							"dataset": "l_TransferLines",
							"callback": null,
							"errCallback": null
						});
						functions.gotoPage({
							"p": "pgTransferStocks"
						});
					},
					"noCallback": async (data, extra) => {
						functions.setVar({
							"var": "vAction"
						});
						functions.userDefined('globalModalHide', {});
					}
				});
			}
		});
	},

	handlePnlModalBodyButtonNegativeClick: async () => {
		functions.setVar({
			"var": "vAction"
		});
		functions.userDefined('globalModalHide', {});
	},

	handlePnlBtnCalculateYes912Click: async () => {
		functions.hideElement({
			"component": "pnlModalEditQty"
		});
	},

	handlePnlBtnCalculateNo27Click: async () => {
		functions.hideElement({
			"component": "pnlModalEditQty"
		});
		functions.userDefined('globalModalLoading', {});
		functions.setVar({
			"var": "v_tmp_qty",
			"value": functions.conditional({
				"condition": null,
				"yesValue": null,
				"noValue": 0,
				"yesCallback": ""
			})
		});
		await functions.navCall({
			"errCallback": async (data, extra) => {
				functions.console({
					"value": data["err"]
				});
				functions.userDefined('globalModalHide', {});
				functions.userDefined('globalModalInfo', {
					"title": "Navision Server Error",
					"message": functions.objectAttr({
						"object": functions.objectAttr({
							"object": data,
							"attr": "err"
						}),
						"attr": "global"
					})
				});
			},
			"connector": "nav",
			"ent": "OK365_Transfer_Orders",
			"function": "Update",
			"data": functions.toObject({
				"Key": functions.getVarAttr({
					"var": "v_transfer_order_data",
					"attr": "Key"
				}),
				"TransferLines": functions.toArray({
					"value1": functions.toObject({
						"Key": functions.getVarAttr({
							"var": "v_transfer_line_selected",
							"attr": "Key"
						}),
						"Quantity": functions.getVar({
							"var": "v_tmp_qty"
						})
					})
				})
			}),
			"callback": async (data, extra) => {
				await functions.loadData({
					"callback": async (data, extra) => {
						functions.setVar({
							"var": "v_transfer_order_data",
							"value": functions.objectAttr({
								"object": data,
								"attr": 0
							})
						});
						functions.setVar({
							"var": "v_transfer_order_lines_data",
							"value": functions.objectAttr({
								"object": functions.objectAttr({
									"object": functions.objectAttr({
										"object": data,
										"attr": 0
									}),
									"attr": "TransferLines"
								}),
								"attr": "OK365_Transfer_Line_OkFldSls"
							})
						});
						await functions.dataFromString({
							"errCallback": null,
							"dataset": "l_TransferLines",
							"string": functions.getVar({
								"var": "v_transfer_order_lines_data"
							}),
							"callback": ""
						});
						await functions.loadData({
							"dataset": "l_TransferLines",
							"callback": async (data, extra) => {
								functions.userDefined('globalModalHide', {});
							},
							"errCallback": null
						});
					},
					"errCallback": async (data, extra) => {
						functions.userDefined('globalModalHide', {});
						functions.userDefined('globalModalInfo', {
							"title": "Navision Server Error",
							"message": functions.objectAttr({
								"object": functions.objectAttr({
									"object": data,
									"attr": "err"
								}),
								"attr": "global"
							}),
							"btnCaption": "OK"
						});
					},
					"dataset": "OK365_Transfer_Orders",
					"filter": functions.toArray({
						"value1": functions.toObject({
							"f": "No",
							"v": functions.getVarAttr({
								"var": "v_transfer_order_selected",
								"attr": "No"
							})
						})
					})
				});
			}
		});
	},

	handlePanel435Click: async () => {
		functions.hideElement({
			"component": "pnlModalAddItem"
		});
		functions.setVar({
			"var": "v_current_item_selected",
			"value": data
		});
		functions.conditional({
			"condition": await functions.selectBy({
				"value": functions.getVarAttr({
					"var": "v_current_item_selected",
					"attr": "No"
				}),
				"first": true,
				"callback": null,
				"errCallback": null,
				"dataset": "l_TransferLines",
				"by": "Item_No"
			}),
			"yesCallback": async (data, extra) => {
				functions.console({
					"value": "exists"
				});
				functions.setVar({
					"var": "vAction",
					"value": "item_exists"
				});
				functions.userDefined('globalModalInfo', {
					"title": "Add Item",
					"message": functions.concat({
						"string1": "<b>",
						"string2": functions.concat({
							"string1": functions.concat({
								"string1": functions.getVarAttr({
									"var": "v_current_item_selected",
									"attr": "No"
								}),
								"string2": " - ",
								"string3": functions.getVarAttr({
									"var": "v_current_item_selected",
									"attr": "Description"
								})
							})
						}),
						"string3": "</b> ",
						"string4": "already exists in the line."
					}),
					"btnCaption": "OK"
				});
			},
			"noCallback": async (data, extra) => {
				functions.console({
					"value": "not exists"
				});
				functions.setComponentValue({
					"component": "lblAddItemQtyTitle",
					"value": functions.concat({
						"string1": functions.getVarAttr({
							"var": "v_current_item_selected",
							"attr": "No"
						}),
						"string2": " - ",
						"string3": functions.getVarAttr({
							"var": "v_current_item_selected",
							"attr": "Description"
						})
					})
				});
				functions.setComponentValue({
					"component": "lblAddQtyUOM",
					"value": functions.getVarAttr({
						"var": "v_current_item_selected",
						"attr": "Base_Unit_of_Measure"
					})
				});
				functions.setComponentValue({
					"component": "edtAddQty"
				});
				functions.showElement({
					"component": "pnlModalAddQty"
				});
			}
		});
	},

	handlePnlBtnCalculateYes912356Click: async () => {
		functions.hideElement({
			"component": "pnlModalAddItem"
		});
	},

	handlePnlBtnCalculateYes912158Click: async () => {
		functions.hideElement({
			"component": "pnlModalAddQty"
		});
		functions.showElement({
			"component": "pnlModalAddItem"
		});
	},

	handlePnlBtnCalculateNo27883Click: async () => {
		functions.setVar({
			"var": "v_tmp_qty",
			"value": functions.conditional({
				"condition": null,
				"yesValue": null,
				"noValue": 0,
				"yesCallback": "",
				"noCallback": ""
			})
		});
		functions.conditional({
			"condition": functions.greater({
				"value1": functions.getVar({
					"var": "v_tmp_qty",
					"default": ""
				}),
				"value2": 0
			}),
			"yesValue": "",
			"noValue": "",
			"extra": "",
			"yesCallback": async (data, extra) => {
				functions.hideElement({
					"component": "pnlModalAddQty",
					"componentId": ""
				});
				functions.userDefined('globalModalLoading', {});
				await functions.navCall({
					"errCallback": async (data, extra) => {
						functions.userDefined('globalModalHide', {});
						functions.userDefined('globalModalInfo', {
							"title": "Navision Server Error",
							"message": functions.objectAttr({
								"object": functions.objectAttr({
									"object": data,
									"attr": "err"
								}),
								"attr": "global"
							}),
							"btnCaption": "OK"
						});
					},
					"connector": "nav",
					"type": "",
					"ent": "OK365_Transfer_Orders",
					"function": "Update",
					"subfunction": "",
					"data": functions.toObject({
						"Key": functions.getVarAttr({
							"var": "v_transfer_order_data",
							"attr": "Key",
							"default": ""
						}),
						"TransferLines": functions.toArray({
							"value1": functions.toObject({
								"Quantity": functions.getVar({
									"var": "v_tmp_qty"
								}),
								"Item_No": functions.getVarAttr({
									"var": "v_current_item_selected",
									"attr": "No"
								})
							}),
							"value2": "",
							"value3": "",
							"value4": "",
							"value5": "",
							"value6": "",
							"value7": "",
							"value8": "",
							"value9": "",
							"value10": ""
						})
					}),
					"callback": async (data, extra) => {
						await functions.loadData({
							"dataset": "OK365_Transfer_Orders",
							"filter": functions.toArray({
								"value1": functions.toObject({
									"v": functions.getVarAttr({
										"var": "v_transfer_order_selected",
										"attr": "No"
									}),
									"f": "No"
								})
							}),
							"callback": async (data, extra) => {
								functions.setVar({
									"var": "v_transfer_order_data",
									"value": functions.objectAttr({
										"object": data,
										"attr": 0
									})
								});
								functions.setVar({
									"var": "v_transfer_order_lines_data",
									"value": functions.objectAttr({
										"object": functions.objectAttr({
											"object": functions.objectAttr({
												"object": data,
												"attr": 0
											}),
											"attr": "TransferLines"
										}),
										"attr": "OK365_Transfer_Line_OkFldSls"
									})
								});
								await functions.dataFromString({
									"dataset": "l_TransferLines",
									"string": functions.getVar({
										"var": "v_transfer_order_lines_data",
										"default": ""
									}),
									"callback": "",
									"errCallback": ""
								});
								await functions.loadData({
									"dataset": "l_TransferLines",
									"callback": async (data, extra) => {
										functions.userDefined('globalModalHide', {});
									},
									"errCallback": ""
								});
							},
							"errCallback": async (data, extra) => {
								functions.userDefined('globalModalHide', {});
								functions.userDefined('globalModalInfo', {
									"title": "Navision Server Error",
									"message": functions.objectAttr({
										"object": functions.objectAttr({
											"object": data,
											"attr": "err"
										}),
										"attr": "global"
									}),
									"btnCaption": "OK"
								});
							}
						});
					}
				});
			},
			"noCallback": async (data, extra) => {
				functions.setVar({
					"var": "vAction",
					"value": "qty_zero"
				});
				functions.userDefined('globalModalInfo', {
					"message": "Quantity must be greater than 0.",
					"btnCaption": "OK",
					"title": "Add Item"
				});
			}
		});
	},

	handlePgTransferStockDetailsLoad: async () => {
		functions.userDefined('globalModalLoading', {});
		functions.toArray({
			"value1": functions.setComponentValue({
				"component": "lblMainTitle_data",
				"value": functions.getVarAttr({
					"var": "v_transfer_order_data",
					"attr": "No"
				})
			}),
			"value2": functions.setComponentValue({
				"component": "lblFromLocation_data",
				"value": functions.getVarAttr({
					"var": "v_transfer_order_data",
					"attr": "Transfer_from_Code"
				})
			}),
			"value3": functions.setComponentValue({
				"component": "lblToLocation_data",
				"value": functions.getVarAttr({
					"var": "v_transfer_order_data",
					"attr": "Transfer_to_Code"
				})
			})
		});
		await functions.loadData({
			"errCallback": null,
			"dataset": "l_TransferLines",
			"callback": async (data, extra) => {
				functions.userDefined('globalModalHide', {});
			}
		});
	}
};

const PgTransferStockDetails = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;
	formatParentStyleLayout(props);

	useEffect(() => {
		actions.handlePgTransferStockDetailsLoad();
	}, [currentTime]);
    
    return (
		<Page data={props.pgTransferStockDetails}>
			<Panel data={props.pnlHolder}>
				<Panel data={props.pnlMain}>
					<Panel data={props.pnlMainHeader}>
						<Panel data={props.pnlMainHeaderLeft}>
							<Panel data={props.pnlMainHeaderLeftTbl}>
								<Panel data={props.pnlMainHeaderLeftTblCell}>
									<Image data={props.imgMenu} action={actions.handleImgMenuClick} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderMiddle}>
							<Panel data={props.pnlMainHeaderMiddleTbl}>
								<Panel data={props.pnlMainHeaderMiddleTblCell}>
									<Label data={props.lblMainTitle} />
									<Label data={props.lblMainTitle_data} />
								</Panel>
							</Panel>
						</Panel>
						<Panel data={props.pnlMainHeaderRight}>
							<Panel data={props.pnlMainHeaderRightTbl}>
								<Panel data={props.pnlMainHeaderRightTblCell}>
									<Label data={props.lblReturn} />
									<Image data={props.imgMenu236} action={actions.handleImgMenu236Click} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainSubHeader}>
						<Panel data={props.pnlBody921975}>
							<Panel data={props.pnlBodyContent680}>
								<Label data={props.lblFromLocation_data} />
							</Panel>
							<Panel data={props.pnlBodyContent680346}>
								<Image data={props.Image9985622} />
							</Panel>
							<Panel data={props.pnlBodyContent680452}>
								<Label data={props.lblToLocation_data} />
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBody}>
						<Panel data={props.pnlBody}>
							<Panel data={props.pnlBodyContent}>
								<DataList data={props.dtTransferLines} functions={functions} currentTime={currentTime}>
									{({ item, parentStyle }) => (								
										<Panel data={item.pnlMainDataContent} parentStyle={parentStyle}>
											<Panel data={item.pnlDataContentTbl} parentStyle={parentStyle}>
												<Panel data={item.pnlDataContentTblCell1} parentStyle={parentStyle}>
													<Panel data={item.pnlDataContentNo} parentStyle={parentStyle}>
														<Label data={item.lblDataContentNo_data} parentStyle={parentStyle} />
													</Panel>
													<Panel data={item.pnlDataContentToLocation} parentStyle={parentStyle}>
														<Label data={item.lblDataContentToLocation_data} parentStyle={parentStyle} />
													</Panel>
													<Panel data={item.pnlDataContentPostingDate} parentStyle={parentStyle}>
														<Label data={item.lblDataContentPostingDate_data} parentStyle={parentStyle} />
													</Panel>
													<Panel data={item.pnlDataContentStatus} parentStyle={parentStyle} />
												</Panel>
												<Panel data={item.pnlDataContentTblCell2} action={actions.handlePnlDataContentTblCell2Click} parentStyle={parentStyle}>
													<Image data={item.Image776} parentStyle={parentStyle} />
												</Panel>
											</Panel>
										</Panel>
									)}
								</DataList>
							</Panel>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainFooter}>
						<Panel data={props.pnlFooterBtnRelease} action={actions.handlePnlFooterBtnReleaseClick}>
							<Label data={props.lblFooterBtnRelease} />
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal}>
					<Panel data={props.pnlModalTable}>
						<Panel data={props.pnlModalCell}>
							<Panel data={props.pnlModalBodyMain}>
								<Panel data={props.pnlModalBodyTitle}>
									<Label data={props.lblModalTitle} />
								</Panel>
								<Panel data={props.pnlModalBodyMessage}>
									<Label data={props.lblModalMessage} />
								</Panel>
								<Panel data={props.pnlModalBodyLoading}>
									<Image data={props.imgModalLoading} />
								</Panel>
							</Panel>
							<Panel data={props.pnlModalBodyButtons}>
								<Panel data={props.pnlModalBodyButtonPositive} action={actions.handlePnlModalBodyButtonPositiveClick}>
									<Label data={props.lblModalPositive} />
								</Panel>
								<Panel data={props.pnlModalBodyButtonNegative} action={actions.handlePnlModalBodyButtonNegativeClick}>
									<Label data={props.lblModalNegative} />
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlCSS}>
					<Label data={props.lblStyle} />
				</Panel>
				<Panel data={props.pnlModalEditQty}>
					<Panel data={props.pnlCalculateTable668645}>
						<Panel data={props.pnlCalculateTableCell600}>
							<Panel data={props.pnlCalculateTableCellContent638}>
								<Label data={props.lblEdtQtyTitle} />
								<Panel data={props.pnlMainBodyCalculateContent58}>
									<Panel data={props.pnlBodyCalculateContent907}>
										<Edit data={props.edtQty} />
										<Label data={props.lblEditQtyUOM} />
									</Panel>
								</Panel>
								<Panel data={props.pnlMainFooterCalculateBtn455}>
									<Panel data={props.pnlBtnCalculateYes912} action={actions.handlePnlBtnCalculateYes912Click}>
										<Label data={props.lblCalculateOK302} />
									</Panel>
									<Panel data={props.pnlBtnCalculateNo27} action={actions.handlePnlBtnCalculateNo27Click}>
										<Label data={props.lblCalculateNo807} />
									</Panel>
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModalAddItem}>
					<Panel data={props.pnlCalculateTable668645118423}>
						<Panel data={props.pnlCalculateTableCell600941}>
							<Panel data={props.pnlCalculateTableCellContent638435}>
								<Panel data={props.Panel291}>
									<Label data={props.lblEdtQtyTitle537} />
								</Panel>
								<Panel data={props.Panel291662}>
									<Edit data={props.edtSearch} />
								</Panel>
								<Panel data={props.pnlMainBodyCalculateContent58149}>
									<DataList data={props.DataList334} functions={functions} currentTime={currentTime}>
										{({ item, parentStyle }) => (									
											<Panel data={item.pnlBodyCalculateContent907237} parentStyle={parentStyle}>
												<Panel data={item.Panel816} parentStyle={parentStyle}>
													<Image data={item.Image180} parentStyle={parentStyle} />
												</Panel>
												<Panel data={item.Panel60} parentStyle={parentStyle}>
													<Label data={item.Label117} parentStyle={parentStyle} />
													<Label data={item.Label742} parentStyle={parentStyle} />
												</Panel>
												<Panel data={item.Panel435} action={actions.handlePanel435Click} parentStyle={parentStyle}>
													<Image data={item.Image794} parentStyle={parentStyle} />
												</Panel>
											</Panel>
										)}
									</DataList>
								</Panel>
								<Panel data={props.pnlMainFooterCalculateBtn455666}>
									<Panel data={props.pnlBtnCalculateYes912356} action={actions.handlePnlBtnCalculateYes912356Click}>
										<Label data={props.lblCalculateOK302338} />
									</Panel>
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModalAddQty}>
					<Panel data={props.pnlCalculateTable66864539346}>
						<Panel data={props.pnlCalculateTableCell600671}>
							<Panel data={props.pnlCalculateTableCellContent6386}>
								<Label data={props.lblAddItemQtyTitle} />
								<Panel data={props.pnlMainBodyCalculateContent58800}>
									<Panel data={props.pnlBodyCalculateContent907564}>
										<Edit data={props.edtAddQty} />
										<Label data={props.lblAddQtyUOM} />
									</Panel>
								</Panel>
								<Panel data={props.pnlMainFooterCalculateBtn455655}>
									<Panel data={props.pnlBtnCalculateYes912158} action={actions.handlePnlBtnCalculateYes912158Click}>
										<Label data={props.lblCalculateOK302949} />
									</Panel>
									<Panel data={props.pnlBtnCalculateNo27883} action={actions.handlePnlBtnCalculateNo27883Click}>
										<Label data={props.lblCalculateNo807336} />
									</Panel>
								</Panel>
							</Panel>
						</Panel>
					</Panel>
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgTransferStockDetails);