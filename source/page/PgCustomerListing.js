import React from 'react';
import { connect } from 'react-redux';

import Functions from '../framework/core/Function';
import { StateName } from '../reducer/data/PgCustomerListing';
import { formatParentStyleLayout } from '../framework/core/Style';

import Page from '../framework/component/Page';
import Panel from '../framework/component/Panel';
import Image from '../framework/component/Image';
import Label from '../framework/component/Label';
import Edit from '../framework/component/Edit';
import DataList from '../framework/component/DataList';

import { useNavigateResponder } from '../framework/hook/NavigateResponder';

let functions = new Functions(StateName);

const actions = {
	handlePnlHeaderLeftAction: () => {
		functions.gotoPage({
			"p": "pgMainMenu"
		});
	},

	handlePnlHeaderRightAction: () => {
		functions.userDefined('globalModalLoading', {});
		functions.setComponentValue({
			"component": "lblAll",
			"componentId": "",
			"value": functions.conditional({
				"condition": functions.equal({
					"value1": functions.getVar({
						"var": "vCustomerListingType",
						"default": ""
					}),
					"value2": "All"
				}),
				"yesValue": "My",
				"noValue": "All",
				"extra": "",
				"yesCallback": null,
				"noCallback": null
			})
		});
		functions.setVar({
			"var": "vCustomerListingType",
			"value": functions.conditional({
				"condition": functions.equal({
					"value1": functions.getVar({
						"var": "vCustomerListingType",
						"default": ""
					}),
					"value2": "All"
				}),
				"yesValue": "My",
				"noValue": "All",
				"extra": "",
				"yesCallback": null,
				"noCallback": null
			})
		});
		functions.setTimeout({
			"timeout": 100,
			"extra": "",
			"callback": (input, extra) => {
				functions.clearData({
					"dataset": "l_customer",
					"callback": (input, extra) => {
						functions.loadData({
							"orFilter": "",
							"order": functions.toArray({
								"value1": functions.toObject({
									"v": "asc",
									"f": "Name"
								}),
								"value2": "",
								"value3": "",
								"value4": "",
								"value5": "",
								"value6": "",
								"value7": "",
								"value8": "",
								"value9": "",
								"value10": ""
							}),
							"callback": (input, extra) => {
								functions.setVar({
									"var": "vTotalCount",
									"value": functions.count({
										"values": input
									})
								});
								functions.setVar({
									"var": "vLoopCount",
									"value": 0
								});
								functions.map({
									"values": input,
									"extra": "",
									"callback": (input, extra) => {
										functions.setVar({
											"var": "vLoopCount",
											"value": functions.add({
												"value1": functions.getVar({
													"var": "vLoopCount",
													"default": ""
												}),
												"value2": 1
											})
										});
										functions.selectByMulti({
											"dataset": "l_customer",
											"orFilter": functions.toArray({
												"value1": functions.toObject({
													"f": "Customer_Code",
													"v": functions.conditional({
														"condition": functions.equal({
															"value1": functions.getVar({
																"var": "vCustomerListingType",
																"default": ""
															}),
															"value2": "All"
														}),
														"yesValue": input["No"],
														"noValue": input["Customer_Code"],
														"extra": "",
														"yesCallback": null
													})
												}),
												"value2": functions.toObject({
													"f": "No",
													"v": functions.conditional({
														"condition": functions.equal({
															"value1": functions.getVar({
																"var": "vCustomerListingType",
																"default": ""
															}),
															"value2": "All"
														}),
														"yesValue": "No",
														"noValue": "Customer_Code",
														"extra": "",
														"yesCallback": null
													})
												}),
												"value3": "",
												"value4": "",
												"value5": "",
												"value6": "",
												"value7": "",
												"value8": "",
												"value9": "",
												"value10": ""
											}),
											"extra": input,
											"callback": (input, extra) => {
												functions.conditional({
													"condition": functions.equal({
														"value1": functions.count({
															"values": input
														}),
														"value2": 0
													}),
													"yesValue": "",
													"noValue": "",
													"extra": extra,
													"yesCallback": (input, extra) => {
														functions.insert({
															"dataset": "l_customer",
															"dt": extra,
															"callback": (input, extra) => {
																functions.conditional({
																	"condition": functions.equal({
																		"value1": functions.getVar({
																			"var": "vTotalCount",
																			"default": ""
																		}),
																		"value2": functions.getVar({
																			"var": "vLoopCount",
																			"default": ""
																		})
																	}),
																	"yesValue": "",
																	"noValue": "",
																	"extra": "",
																	"yesCallback": (input, extra) => {
																		functions.loadData({
																			"dataset": "l_customer",
																			"callback": (input, extra) => {
																				functions.userDefined('globalModalHide', {});
																			},
																			"errCallback": null
																		});
																		functions.setVar({
																			"var": "vAction",
																			"value": ""
																		});
																	},
																	"noCallback": null
																});
															},
															"errCallback": null
														});
													},
													"noCallback": (input, extra) => {
														functions.conditional({
															"condition": functions.equal({
																"value1": functions.getVar({
																	"var": "vTotalCount",
																	"default": ""
																}),
																"value2": functions.getVar({
																	"var": "vLoopCount",
																	"default": ""
																})
															}),
															"yesValue": "",
															"noValue": "",
															"extra": "",
															"yesCallback": (input, extra) => {
																functions.loadData({
																	"dataset": "l_customer",
																	"callback": (input, extra) => {
																		functions.userDefined('globalModalHide', {});
																	},
																	"errCallback": null
																});
																functions.setVar({
																	"var": "vAction",
																	"value": ""
																});
															},
															"noCallback": null
														});
													}
												});
											},
											"errCallback": null
										});
									}
								});
								functions.conditional({
									"condition": functions.equal({
										"value1": functions.count({
											"values": input
										}),
										"value2": 0
									}),
									"yesValue": "",
									"noValue": "",
									"extra": "",
									"yesCallback": (input, extra) => {
										functions.userDefined('globalModalHide', {});
									},
									"noCallback": null
								});
							},
							"errCallback": (input, extra) => {
								functions.setVar({
									"var": "error",
									"value": input["err"]
								});
								functions.userDefined('globalModalInfo', {
									"title": "Error",
									"message": functions.conditional({
										"condition": functions.getVarAttr({
											"var": "error",
											"attr": "global",
											"default": ""
										}),
										"yesValue": functions.getVarAttr({
											"var": "error",
											"attr": "global",
											"default": ""
										}),
										"noValue": "Cannot connect to Navision Server.",
										"extra": ""
									}),
									"btnCaption": ""
								});
								functions.console({
									"value": input
								});
							},
							"dataset": functions.conditional({
								"condition": functions.equal({
									"value1": functions.getVar({
										"var": "vCustomerListingType",
										"default": ""
									}),
									"value2": "All"
								}),
								"yesValue": "OK365_Customer_List",
								"noValue": "OK365_Route_And_Customer_Priority",
								"extra": "",
								"yesCallback": null,
								"noCallback": null
							}),
							"limit": 10,
							"page": "",
							"filter": functions.conditional({
								"condition": functions.equal({
									"value1": functions.getVar({
										"var": "vCustomerListingType",
										"default": ""
									}),
									"value2": "All"
								}),
								"yesValue": "",
								"noValue": functions.toArray({
									"value1": functions.toObject({
										"f": "Route_No",
										"v": functions.objectAttr({
											"object": functions.getVarAttr({
												"var": "var_loginCnt",
												"attr": 0,
												"default": ""
											}),
											"attr": "Route_Card_no"
										})
									}),
									"value2": "",
									"value3": "",
									"value4": "",
									"value5": "",
									"value6": "",
									"value7": "",
									"value8": "",
									"value9": "",
									"value10": ""
								}),
								"extra": "",
								"yesCallback": null,
								"noCallback": null
							})
						});
					},
					"errCallback": null
				});
			}
		});
		functions.console({
			"value": 111
		});
	},

	handleTxtSearchChange: () => {
		functions.userDefined('globalModalLoading', {});
		functions.setTimeout({
			"timeout": 100,
			"extra": "",
			"callback": (input, extra) => {
				functions.clearData({
					"dataset": "l_customer",
					"callback": null,
					"errCallback": null
				});
				functions.loadData({
					"dataset": functions.conditional({
						"condition": functions.equal({
							"value1": functions.getVar({
								"var": "vCustomerListingType",
								"default": ""
							}),
							"value2": "All"
						}),
						"yesValue": "OK365_Customer_List",
						"noValue": "OK365_Route_And_Customer_Priority",
						"extra": "",
						"yesCallback": null,
						"noCallback": null
					}),
					"limit": 10,
					"page": "",
					"filter": functions.conditional({
						"condition": functions.equal({
							"value1": functions.getVar({
								"var": "vCustomerListingType",
								"default": ""
							}),
							"value2": "All"
						}),
						"yesValue": functions.toArray({
							"value1": functions.toObject({
								"f": "Name",
								"o": "=",
								"v": functions.concat({
									"string1": "'@*",
									"string2": functions.componentElAttr({
										"component": "txtSearch",
										"componentId": "",
										"attr": "value"
									}),
									"string3": "*'",
									"string4": ""
								})
							}),
							"value2": "",
							"value3": "",
							"value4": "",
							"value5": "",
							"value6": "",
							"value7": "",
							"value8": "",
							"value9": "",
							"value10": ""
						}),
						"noValue": functions.toArray({
							"value1": functions.toObject({
								"f": functions.formatDate({
									"date": functions.componentValue({
										"component": "DatePicker"
									}),
									"format": "l"
								}),
								"v": "true"
							}),
							"value2": functions.toObject({
								"f": "Route_No",
								"v": functions.objectAttr({
									"object": functions.getVarAttr({
										"var": "var_loginCnt",
										"attr": 0
									}),
									"attr": "Route_Card_no"
								})
							}),
							"value3": functions.toObject({
								"v": functions.concat({
									"string1": "'@*",
									"string2": functions.componentElAttr({
										"component": "txtSearch",
										"componentId": "",
										"attr": "value"
									}),
									"string3": "*'",
									"string4": ""
								}),
								"f": "Name",
								"o": "="
							}),
							"value4": "",
							"value5": "",
							"value6": "",
							"value7": "",
							"value8": "",
							"value9": "",
							"value10": ""
						}),
						"extra": "",
						"yesCallback": null,
						"noCallback": null
					}),
					"orFilter": "",
					"order": functions.toArray({
						"value1": functions.toObject({
							"f": "Name",
							"v": "asc"
						}),
						"value2": "",
						"value3": "",
						"value4": "",
						"value5": "",
						"value6": "",
						"value7": "",
						"value8": "",
						"value9": "",
						"value10": ""
					}),
					"callback": (input, extra) => {
						functions.setVar({
							"var": "vTotalCount",
							"value": functions.count({
								"values": input
							})
						});
						functions.setVar({
							"var": "vLoopCount",
							"value": 0
						});
						functions.map({
							"values": input,
							"extra": "",
							"callback": (input, extra) => {
								functions.setVar({
									"var": "vLoopCount",
									"value": functions.add({
										"value1": functions.getVar({
											"var": "vLoopCount",
											"default": ""
										}),
										"value2": 1
									})
								});
								functions.conditional({
									"condition": functions.equal({
										"value1": functions.count({
											"values": functions.selectByMulti({
												"dataset": "l_customer",
												"orFilter": functions.toArray({
													"value1": functions.toObject({
														"f": "Customer_Code",
														"v": functions.conditional({
															"condition": functions.equal({
																"value1": functions.getVar({
																	"var": "vCustomerListingType",
																	"default": ""
																}),
																"value2": "All"
															}),
															"yesValue": input["No"],
															"noValue": input["Customer_Code"],
															"extra": ""
														})
													}),
													"value2": functions.toObject({
														"f": "No",
														"v": functions.conditional({
															"condition": functions.equal({
																"value1": functions.getVar({
																	"var": "vCustomerListingType",
																	"default": ""
																}),
																"value2": "All"
															}),
															"yesValue": input["No"],
															"noValue": input["Customer_Code"],
															"extra": ""
														})
													}),
													"value3": "",
													"value4": "",
													"value5": "",
													"value6": "",
													"value7": "",
													"value8": "",
													"value9": "",
													"value10": ""
												}),
												"callback": null,
												"errCallback": null
											})
										}),
										"value2": 0
									}),
									"yesValue": "",
									"noValue": "",
									"extra": input,
									"yesCallback": (input, extra) => {
										functions.insert({
											"dataset": "l_customer",
											"dt": extra,
											"callback": (input, extra) => {
												functions.conditional({
													"condition": functions.equal({
														"value1": functions.getVar({
															"var": "vTotalCount",
															"default": ""
														}),
														"value2": functions.getVar({
															"var": "vLoopCount",
															"default": ""
														})
													}),
													"yesValue": "",
													"noValue": "",
													"extra": "",
													"yesCallback": (input, extra) => {
														functions.loadData({
															"dataset": "l_customer",
															"callback": (input, extra) => {
																functions.userDefined('globalModalHide', {});
															},
															"errCallback": null
														});
													},
													"noCallback": null
												});
											},
											"errCallback": null
										});
									},
									"noCallback": (input, extra) => {
										functions.conditional({
											"condition": functions.equal({
												"value1": functions.getVar({
													"var": "vTotalCount",
													"default": ""
												}),
												"value2": functions.getVar({
													"var": "vLoopCount",
													"default": ""
												})
											}),
											"yesValue": "",
											"noValue": "",
											"extra": "",
											"yesCallback": (input, extra) => {
												functions.loadData({
													"dataset": "l_customer",
													"callback": (input, extra) => {
														functions.userDefined('globalModalHide', {});
													},
													"errCallback": null
												});
											},
											"noCallback": null
										});
									}
								});
							}
						});
						functions.conditional({
							"condition": functions.equal({
								"value1": functions.count({
									"values": input
								}),
								"value2": 0
							}),
							"yesValue": "",
							"noValue": "",
							"extra": "",
							"yesCallback": (input, extra) => {
								functions.userDefined('globalModalHide', {});
							},
							"noCallback": null
						});
						functions.setVar({
							"var": "vCustomerListingType",
							"value": functions.conditional({
								"condition": functions.equal({
									"value1": functions.getVar({
										"var": "vCustomerListingType",
										"default": ""
									}),
									"value2": "All"
								}),
								"yesValue": "My",
								"noValue": "All",
								"extra": "",
								"yesCallback": null,
								"noCallback": null
							})
						});
					},
					"errCallback": (input, extra) => {
						functions.setVar({
							"var": "error",
							"value": input["err"]
						});
						functions.userDefined('globalModalInfo', {
							"title": "Error",
							"message": functions.conditional({
								"condition": functions.getVarAttr({
									"var": "error",
									"attr": "global"
								}),
								"yesValue": functions.getVarAttr({
									"var": "error",
									"attr": "global"
								}),
								"noValue": "Cannot connect to Navision Server."
							})
						});
						functions.console({
							"value": input
						});
					}
				});
			}
		});
	},

	getLblCustomerDetailsValue: data => () => {
		return functions.concat({
			"string1": functions.conditional({
				"condition": data["No"],
				"yesValue": data["No"],
				"noValue": data["Customer_Code"]
			}),
			"string2": "",
			"string3": "",
			"string4": ""
		});
	},

	getLblTermValue: data => () => {
		return functions.concat({
			"string1": "<b>Term: </b>",
			"string2": data["Payment_Terms_Code"],
			"string3": "",
			"string4": ""
		});
	},

	handleDlCustomerListActionItem: data => () => {
		functions.setVar({
			"var": "vCurrCustomer",
			"value": data
		});
		functions.userDefined('globalModalLoading', {});
		functions.loadData({
			"order": "",
			"callback": (input, extra) => {
				functions.console({
					"value": input
				});
				functions.setVar({
					"var": "vCurrCustomerDetails",
					"value": functions.objectAttr({
						"object": input,
						"attr": 0
					})
				});
				functions.setVar({
					"var": "vAction",
					"value": ""
				});
				functions.gotoPage({
					"p": "pgItemListing"
				});
			},
			"errCallback": (input, extra) => {
				functions.setVar({
					"var": "error",
					"value": input["err"]
				});
				functions.userDefined('globalModalInfo', {
					"title": "Navision Server Error",
					"message": functions.conditional({
						"condition": functions.getVarAttr({
							"var": "error",
							"attr": "global"
						}),
						"yesValue": functions.getVarAttr({
							"var": "error",
							"attr": "global"
						}),
						"noValue": "Cannot connect to Navision Server."
					})
				});
			},
			"dataset": "OK365_Customer_Card",
			"filter": functions.toArray({
				"value1": functions.toObject({
					"f": "No",
					"v": functions.conditional({
						"condition": functions.getVarAttr({
							"var": "vCurrCustomer",
							"attr": "No",
							"default": ""
						}),
						"yesValue": functions.getVarAttr({
							"var": "vCurrCustomer",
							"attr": "No",
							"default": ""
						}),
						"noValue": functions.getVarAttr({
							"var": "vCurrCustomer",
							"attr": "Customer_Code",
							"default": ""
						}),
						"extra": "",
						"yesCallback": null,
						"noCallback": null
					})
				}),
				"value2": "",
				"value3": "",
				"value4": "",
				"value5": "",
				"value6": "",
				"value7": "",
				"value8": "",
				"value9": "",
				"value10": ""
			})
		});
	},

	handlePnlModalBodyButtonPositiveAction: () => {
		functions.conditional({
			"condition": functions.equal({
				"value1": functions.getVar({
					"var": "vAction",
					"default": ""
				}),
				"value2": "NoCustomer"
			}),
			"yesValue": "",
			"noValue": "",
			"extra": "",
			"yesCallback": (input, extra) => {
				functions.userDefined('globalModalHide', {});
			},
			"noCallback": (input, extra) => {
				functions.conditional({
					"condition": functions.equal({
						"value1": functions.getVar({
							"var": "vAction",
							"default": ""
						}),
						"value2": "logout"
					}),
					"yesValue": "",
					"noValue": "",
					"extra": "",
					"yesCallback": (input, extra) => {
						functions.clearAllVar({});
						functions.setVar({
							"var": "vAction",
							"value": ""
						});
						functions.gotoPage({
							"p": "pgLogin"
						});
					},
					"noCallback": (input, extra) => {
						functions.setVar({
							"var": "vAction",
							"value": ""
						});
						functions.userDefined('globalModalHide', {});
					}
				});
			}
		});
	},

	handlePnlModalBodyButtonNegativeAction: () => {
		functions.setVar({
			"var": "vAction",
			"value": ""
		});
		functions.userDefined('globalModalHide', {});
	},

	handlePgCustomerListingLoad: () => {
		functions.console({
			"value": "pageload"
		});
		functions.onBackButton({
			"callback": (input, extra) => {
				functions.gotoPage({
					"p": "pgMainMenu"
				});
			}
		});
		functions.setVar({
			"var": "vAction",
			"value": "page_load"
		});
		functions.setVar({
			"var": "vCustomerListingType",
			"value": "My"
		});
		functions.userDefined('globalModalLoading', {});
		functions.setTimeout({
			"timeout": 100,
			"extra": "",
			"callback": (input, extra) => {
				functions.clearData({
					"errCallback": null,
					"dataset": "l_customer",
					"callback": (input, extra) => {
						functions.loadData({
							"dataset": functions.conditional({
								"condition": functions.equal({
									"value1": functions.getVar({
										"var": "vCustomerListingType",
										"default": ""
									}),
									"value2": "All"
								}),
								"yesValue": "OK365_Customer_List",
								"noValue": "OK365_Route_And_Customer_Priority",
								"extra": "",
								"yesCallback": null,
								"noCallback": null
							}),
							"limit": 10,
							"filter": functions.conditional({
								"condition": functions.equal({
									"value1": functions.getVar({
										"var": "vCustomerListingType",
										"default": ""
									}),
									"value2": "All"
								}),
								"yesValue": "",
								"noValue": functions.toArray({
									"value1": functions.toObject({
										"f": "Route_No",
										"v": functions.objectAttr({
											"object": functions.getVarAttr({
												"var": "var_loginCnt",
												"attr": 0,
												"default": ""
											}),
											"attr": "Route_Card_no"
										})
									}),
									"value2": "",
									"value3": "",
									"value4": "",
									"value5": "",
									"value6": "",
									"value7": "",
									"value8": "",
									"value9": "",
									"value10": ""
								}),
								"extra": "",
								"yesCallback": null,
								"noCallback": null
							}),
							"order": functions.toArray({
								"value1": functions.toObject({
									"f": "Name",
									"v": "asc"
								}),
								"value2": "",
								"value3": "",
								"value4": "",
								"value5": "",
								"value6": "",
								"value7": "",
								"value8": "",
								"value9": "",
								"value10": ""
							}),
							"callback": (input, extra) => {
								functions.setVar({
									"var": "vTotalCount",
									"value": functions.count({
										"values": input
									})
								});
								functions.setVar({
									"var": "vLoopCount",
									"value": 0
								});
								functions.map({
									"values": input,
									"extra": "",
									"callback": (input, extra) => {
										functions.setVar({
											"var": "vLoopCount",
											"value": functions.add({
												"value1": functions.getVar({
													"var": "vLoopCount",
													"default": ""
												}),
												"value2": 1
											})
										});
										functions.selectByMulti({
											"dataset": "l_customer",
											"orFilter": functions.toArray({
												"value1": functions.toObject({
													"f": "Customer_Code",
													"v": functions.conditional({
														"condition": functions.equal({
															"value1": functions.getVar({
																"var": "vCustomerListingType",
																"default": ""
															}),
															"value2": "All"
														}),
														"yesValue": input["No"],
														"noValue": input["Customer_Code"],
														"extra": "",
														"yesCallback": null
													})
												}),
												"value2": functions.toObject({
													"f": "No",
													"v": functions.conditional({
														"condition": functions.equal({
															"value1": functions.getVar({
																"var": "vCustomerListingType",
																"default": ""
															}),
															"value2": "All"
														}),
														"yesValue": "No",
														"noValue": "Customer_Code",
														"extra": "",
														"yesCallback": null
													})
												}),
												"value3": "",
												"value4": "",
												"value5": "",
												"value6": "",
												"value7": "",
												"value8": "",
												"value9": "",
												"value10": ""
											}),
											"extra": input,
											"callback": (input, extra) => {
												functions.conditional({
													"condition": functions.equal({
														"value1": functions.count({
															"values": input
														}),
														"value2": 0
													}),
													"yesValue": "",
													"noValue": "",
													"extra": extra,
													"yesCallback": (input, extra) => {
														functions.insert({
															"dataset": "l_customer",
															"dt": extra,
															"callback": (input, extra) => {
																functions.conditional({
																	"condition": functions.equal({
																		"value1": functions.getVar({
																			"var": "vTotalCount",
																			"default": ""
																		}),
																		"value2": functions.getVar({
																			"var": "vLoopCount",
																			"default": ""
																		})
																	}),
																	"yesValue": "",
																	"noValue": "",
																	"extra": "",
																	"yesCallback": (input, extra) => {
																		functions.loadData({
																			"dataset": "l_customer",
																			"callback": (input, extra) => {
																				functions.userDefined('globalModalHide', {});
																			},
																			"errCallback": null
																		});
																		functions.setVar({
																			"var": "vAction",
																			"value": ""
																		});
																	},
																	"noCallback": null
																});
															},
															"errCallback": null
														});
													},
													"noCallback": (input, extra) => {
														functions.conditional({
															"condition": functions.equal({
																"value1": functions.getVar({
																	"var": "vTotalCount",
																	"default": ""
																}),
																"value2": functions.getVar({
																	"var": "vLoopCount",
																	"default": ""
																})
															}),
															"yesValue": "",
															"noValue": "",
															"extra": "",
															"yesCallback": (input, extra) => {
																functions.loadData({
																	"dataset": "l_customer",
																	"callback": (input, extra) => {
																		functions.userDefined('globalModalHide', {});
																	},
																	"errCallback": null
																});
																functions.setVar({
																	"var": "vAction",
																	"value": ""
																});
															},
															"noCallback": null
														});
													}
												});
											},
											"errCallback": null
										});
									}
								});
								functions.conditional({
									"condition": functions.equal({
										"value1": functions.count({
											"values": input
										}),
										"value2": 0
									}),
									"yesValue": "",
									"noValue": "",
									"extra": "",
									"yesCallback": (input, extra) => {
										functions.userDefined('globalModalHide', {});
										functions.setVar({
											"var": "vAction",
											"value": ""
										});
									},
									"noCallback": null
								});
							},
							"errCallback": (input, extra) => {
								functions.setVar({
									"var": "error",
									"value": input["err"]
								});
								functions.userDefined('globalModalInfo', {
									"btnCaption": "",
									"title": "Error",
									"message": functions.conditional({
										"condition": functions.getVarAttr({
											"var": "error",
											"attr": "global",
											"default": ""
										}),
										"yesValue": functions.getVarAttr({
											"var": "error",
											"attr": "global",
											"default": ""
										}),
										"noValue": "Cannot connect to Navision Server.",
										"extra": ""
									})
								});
								functions.console({
									"value": input
								});
							}
						});
					}
				});
			}
		});
	}
};

functions.pageActions = actions
// list template to be rendered for datalists
const templates = {
    dlCustomer_List: ({ item, parentStyle, currentTime }) => (							
		<Panel data={item.pnlCustomer} functions={functions} currentTime={currentTime} parentStyle={parentStyle}>
			<Panel data={item.Panel483} functions={functions} currentTime={currentTime} parentStyle={parentStyle}>
				<Label data={item.lblCustomerDetails} currentTime={currentTime} value={actions.getLblCustomerDetailsValue} value_data={item._data} functions={functions} parentStyle={parentStyle}/>
				<Label data={item.lblCustomerName} functions={functions} currentTime={currentTime} parentStyle={parentStyle}/>
				<Label data={item.lblTerm} functions={functions} currentTime={currentTime} value={actions.getLblTermValue} value_data={item._data} parentStyle={parentStyle}/>
			</Panel>
			<Panel data={item.Panel656} functions={functions} currentTime={currentTime} parentStyle={parentStyle}>
				<Image data={item.imgAddItem} functions={functions} currentTime={currentTime} parentStyle={parentStyle}/>
			</Panel>
		</Panel>
	)
}

const PgCustomerListing = (props) => {

	// For datalist autoload handling when to render
	const currentTime = useNavigateResponder(props.navigation);

    // Append the dispatch to the function instance
    functions.pageProps = props;

    // Manipulate the props to support css styles
    formatParentStyleLayout(props);

    return (
		<Page data={props.pgCustomerListing} load={actions.handlePgCustomerListingLoad} currentTime={currentTime} initializePage={functions.coreStateInitializePage}>
			<Panel data={props.pnlHolder} functions={functions} currentTime={currentTime}>
				<Panel data={props.pnlMain} functions={functions} currentTime={currentTime}>
					<Panel data={props.pnlMainHeader} functions={functions} currentTime={currentTime}>
						<Panel data={props.pnlHeaderLeft} action={actions.handlePnlHeaderLeftAction} functions={functions} currentTime={currentTime}>
							<Image data={props.imgBack} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlHeaderMiddle} functions={functions} currentTime={currentTime}>
							<Label data={props.lblMainTitle} currentTime={currentTime} functions={functions}/>
						</Panel>
						<Panel data={props.pnlHeaderRight} action={actions.handlePnlHeaderRightAction} functions={functions} currentTime={currentTime}>
							<Label data={props.lblAll} functions={functions} currentTime={currentTime}/>
						</Panel>
					</Panel>
					<Panel data={props.pnlMainBody} functions={functions} currentTime={currentTime}>
						<Panel data={props.pnlSearch} functions={functions} currentTime={currentTime}>
							<Edit data={props.txtSearch} change={actions.handleTxtSearchChange} currentTime={currentTime} functions={functions}/>
						</Panel>
						<Panel data={props.pnlCustomerListing} functions={functions} currentTime={currentTime}>
							<DataList data={props.dlCustomer_List} actionItem={actions.handleDlCustomerListActionItem} functions={functions} currentTime={currentTime} template={templates.dlCustomer_List}/>
								{/* {({ item, parentStyle }) => (							
									<Panel data={item.pnlCustomer} functions={functions} currentTime={currentTime} parentStyle={parentStyle}>
										<Panel data={item.Panel483} functions={functions} currentTime={currentTime} parentStyle={parentStyle}>
											<Label data={item.lblCustomerDetails} currentTime={currentTime} value={actions.getLblCustomerDetailsValue} value_data={item._data} functions={functions} parentStyle={parentStyle}/>
											<Label data={item.lblCustomerName} functions={functions} currentTime={currentTime} parentStyle={parentStyle}/>
											<Label data={item.lblTerm} functions={functions} currentTime={currentTime} value={actions.getLblTermValue} value_data={item._data} parentStyle={parentStyle}/>
										</Panel>
										<Panel data={item.Panel656} functions={functions} currentTime={currentTime} parentStyle={parentStyle}>
											<Image data={item.imgAddItem} functions={functions} currentTime={currentTime} parentStyle={parentStyle}/>
										</Panel>
									</Panel>
								)}
							</DataList> */}
						</Panel>
					</Panel>
				</Panel>
				<Panel data={props.pnlModal} functions={functions} currentTime={currentTime}>
					<Panel data={props.pnlModalBodyMain} functions={functions} currentTime={currentTime}>
						<Panel data={props.pnlModalBodyTitle} functions={functions} currentTime={currentTime}>
							<Label data={props.lblModalTitle} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlModalBodyMessage} functions={functions} currentTime={currentTime}>
							<Label data={props.lblModalMessage} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlModalBodyLoading} functions={functions} currentTime={currentTime}>
							<Image data={props.imgModalLoading} functions={functions} currentTime={currentTime}/>
						</Panel>
					</Panel>
					<Panel data={props.pnlModalBodyButtons} functions={functions} currentTime={currentTime}>
						<Panel data={props.pnlModalBodyButtonPositive} action={actions.handlePnlModalBodyButtonPositiveAction} functions={functions} currentTime={currentTime}>
							<Label data={props.lblModalPositive} functions={functions} currentTime={currentTime}/>
						</Panel>
						<Panel data={props.pnlModalBodyButtonNegative} action={actions.handlePnlModalBodyButtonNegativeAction} functions={functions} currentTime={currentTime}>
							<Label data={props.lblModalNegative} currentTime={currentTime} functions={functions}/>
						</Panel>
					</Panel>
				</Panel>
			</Panel>
		</Page>
	);
}

// Bind the state of redux to props - redux
const mapStateToProps = (state) => {
    return { ...state[StateName] };
};

export default connect(mapStateToProps)(PgCustomerListing);