import React from 'react';
import { View } from 'react-native';
import Image from './../framework/component/Image';

const ImageSnippet = (props) => {    
    let functions = props.functions;
    return (
        <View>
            <Image data={props.data.image1} functions={functions}/>
        </View>
    )
}

export default ImageSnippet;