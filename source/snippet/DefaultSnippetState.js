import SAMPLE_SNIPPET_DATA from './defaults/SampleSnippet';
import SECOND_SNIPPET_DATA from './defaults/SecondSnippet';
import IMAGE_SNIPPET_DATA from './defaults/ImageSnippet';

const DefaultSnippetState = {
    'sampleSnippet' : SAMPLE_SNIPPET_DATA,
    'secondSnippet' : SECOND_SNIPPET_DATA,
    'imageSnippet'  : IMAGE_SNIPPET_DATA,
};

export default DefaultSnippetState;