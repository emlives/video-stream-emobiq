import React from 'react';
import { View } from 'react-native'
import Button from './../framework/component/Button';

const SecondSnippet = (props) => {
    let functions = props.functions;
    
    handleClick1 = () => {
        functions.setComponentValue({name:"btnSecond1",id:"",value:"Changed Text"})
    }

    handleClick2 = () => {
        alert('this is second snippet');
    }
    
    return (
        <View>
            <Button action={handleClick1} data={props.data.btnSecond1} functions={functions}></Button>
            <Button action={handleClick2} data={props.data.btnSecond2} functions={functions}></Button>
        </View>
    )
}

export default SecondSnippet;
