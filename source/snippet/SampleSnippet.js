import React from 'react';
import { View } from 'react-native';
import Button from './../framework/component/Button';
import SnippetWrapper from '../wrapper/component/SnippetWrapper';

const SampleSnippet = (props) => {
    let functions = props.functions;

    handleClick1 = () => {
        functions.setComponentValue({name:"btnSnippet1",id:"",value:"Changed Text"})
    }

    handleClick2 = () => {
        alert('this is sample snippet');
    }
    
    return (
        <View>
            <Button action={handleClick1} data={props.data.btnSnippet1} functions={functions}></Button>
            <Button action={handleClick2} data={props.data.btnSnippet2} functions={functions}></Button>
            <SnippetWrapper data={props.data.snippetAChild1} functions={functions}></SnippetWrapper>
        </View>
    )
}

export default SampleSnippet;