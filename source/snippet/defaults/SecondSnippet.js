const DEFAULTS = {
    btnSecond1: {
        style: {
            width: '100%',
            textAlign: 'center'
        },
        property: {
            componentValue: "Second Snippet Button 1"
        }
    },
    btnSecond2: {
        style: {
            width: '100%',
            textAlign: 'center'
        },
        property: {
            componentValue: "Second Snippet Button 2"
        }
    }
};

export default DEFAULTS;