const DEFAULTS = {
    btnSnippet1: {
        style: {
            width: '100%',
            textAlign: 'center'
        },
        property: {
            componentValue: "Sample Button Snippet 1"
        }
    },
    btnSnippet2: {
        style: {
            width: '100%',
            textAlign: 'center'
        },
        property: {
            componentValue: "Sample Button Snippet 2"
        }
    }
};

export default DEFAULTS;