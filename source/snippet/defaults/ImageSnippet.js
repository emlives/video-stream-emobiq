const DEFAULTS = {
    image1: {
        property: {
            name: "image1",
            componentValue: { uri: 'http://beta.emobiq.com/archive/app/OK365VANSALES28502/asset/images/OK365-Dark.png' }
        },
        style: {
            paddingTop: "50%",
            height: "30%"
        }
    }
};

export default DEFAULTS;