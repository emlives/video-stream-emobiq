import React from 'react';
import { getPropertyValue } from '../../framework/core/Component';
import Snippet from '../../framework/component/Snippet';
import SampleSnippet from '../../snippet/SampleSnippet';
import SecondSnippet from '../../snippet/SecondSnippet';
import ImageSnippet from '../../snippet/ImageSnippet';

const SnippetWrapper = (props) => {
    return (
        <Snippet data={props.data}>
            { prepareSnippet(props) }
        </Snippet>
    )
}

/**
 * Prepare the snippet component with it's data
 * and return the snippet component.
 * @param {object} props 
 */
const prepareSnippet = (props) => {
    // Prepare the needed information
    let result;
    let name = getPropertyValue('componentValue', props);
    let content = getPropertyValue('componentData', props);
    switch (name) {
        case 'sampleSnippet':
            result = <SampleSnippet data={content} functions={props.functions}></SampleSnippet>;
            break;
        case 'secondSnippet':
            result = <SecondSnippet data={content} functions={props.functions}></SecondSnippet>;
            break;
        case 'imageSnippet':
            result = <ImageSnippet data={content} functions={props.functions}></ImageSnippet>;
            break;
    }

    return result;
}

export default SnippetWrapper;