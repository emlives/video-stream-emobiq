//
//  BtPrinter.h
//  HelloPrinter
//
//  Created by Foo Piau Ming on 4/2/21.
//

#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
#import <Foundation/Foundation.h>
#import "BLE.h"

@interface BtPrinter : RCTEventEmitter <RCTBridgeModule, BLEDelegate> {
  BLE *_bleShield;
  
  NSString* _connectCallbackId;
  NSString* _subscribeCallbackId;
  NSString* _subscribeBytesCallbackId;
  NSString* _rssiCallbackId;
  NSMutableString *_buffer;
  NSString *_delimiter;
}

@end
