//
//  Foo.m
//  HelloPrinter
//
//  Created by Foo Piau Ming on 4/2/21.
//

#import "BtPrinter.h"

@implementation BtPrinter
{
  bool hasListeners;
}


RCT_EXPORT_MODULE();

- (id)init
{
	self = [super init];
	_bleShield = [[BLE alloc] init];
	[_bleShield controlSetup];
	[_bleShield setDelegate:self];
	_buffer = [[NSMutableString alloc] init];

	return self;
}

// Please add this one
+ (BOOL)requiresMainQueueSetup
{
	return YES;
}

- (NSArray<NSString *> *)supportedEvents{
  return @[@"BtPrinterConnectEvent"];
}

/*
 Implementing RCTEventEmitter's startObserving,stopObserving methods
 - startObserving() will be called when this module's first listener is added.
 - stopObserving() will be called when this module's last listener is removed, or on dealloc.
 */
-(void)startObserving {
    hasListeners = YES;
    // Set up any upstream listeners or background tasks as necessary
}

-(void)stopObserving {
    hasListeners = NO;
    // Remove upstream listeners, stop unnecessary background tasks
}

RCT_EXPORT_METHOD(scanForBLEPeripherals: (int)timeout withCallback:(RCTResponseSenderBlock)callback)
{
  if (_bleShield) {
    [_bleShield findBLEPeripherals:timeout];
  }
  else {
    NSLog(@"_bleShield is null!");
  }
  
  dispatch_async(dispatch_get_main_queue(), ^{
    [self performSelector:@selector(scanForBLEPeripheralsCallback:) withObject:callback afterDelay:timeout];
  });
}

- (void)scanForBLEPeripheralsCallback:(RCTResponseSenderBlock)callback
{
  NSLog(@"scanForBLEPeripheralsCallback invoked");
  
  NSMutableArray *mutableArray = [NSMutableArray array];
  NSArray *peripherals = _bleShield.peripherals;
  
  for (int i = 0; i < peripherals.count; i++)
  {
    NSMutableDictionary *deviceDict = [NSMutableDictionary new];
    CBPeripheral *peripheral = [peripherals objectAtIndex:i];

    NSString *uuid = @"NULL";
    NSString *deviceName = @"";
    if (peripheral.identifier != NULL) {
      uuid = peripheral.identifier.UUIDString;
    }
    
    if (peripheral.name != NULL) {
      deviceName = peripheral.name;
    }
    else {
      deviceName = @"UNKNOWN DEVICE";
      continue;
    }

//    [deviceDict setObject:deviceName forKey:@"name"];
//    [deviceDict setObject:uuid forKey:@"address"];
    
    [deviceDict setObject:deviceName forKey:@"name"];
    [deviceDict setObject:uuid forKey:@"id"];
    
    [mutableArray addObject:deviceDict];
  }
  
  NSArray *array = [mutableArray copy];
  callback(@[[NSNull null], array]);
}

RCT_EXPORT_METHOD(connect: (NSString*)uuid withCallback:(RCTResponseSenderBlock)callback)
{
	if (_bleShield) {
		CBPeripheral *peripheral = [self findPeripheralByUUID:uuid];
		NSLog(@"peripheral name: %@", peripheral.name);

		if (peripheral) {
			[_bleShield connectPeripheral:peripheral];
			callback(@[[NSNull null], @true]);
		}
		else {
			NSLog(@"status: %@", @"not found");
			callback(@[@"not found", [NSNull null]]);
		}
	}
	else {
		NSLog(@"_bleShield is null!");
		callback(@[@"bleShield is null", [NSNull null]]);
	}
}


RCT_EXPORT_METHOD(write: (NSString *)text withCallback:(RCTResponseSenderBlock)callback) 
{
  
  NSLog(@"write with : %@", text);
  
  NSData *data = [[NSData alloc] initWithBase64EncodedString:text options:0];
  if (data != nil) {
	  [_bleShield write:data];
		callback(@[[NSNull null], @true]);
	}
	else {
	  callback(@[@"text is null", [NSNull null]]);
	}
}

- (CBPeripheral*) findPeripheralByUUID: (NSString*)uuid 
{
  NSLog(@"...findPeripheralByUUID: %@", uuid);
  NSMutableArray *peripherals = [_bleShield peripherals];
	CBPeripheral *peripheral = nil;

	for (CBPeripheral *p in peripherals) {
		NSString *other = p.identifier.UUIDString;
		if ([uuid isEqualToString:other]) {
			peripheral = p;
			break;
		}
	}

	return peripheral;
}

- (void)bleDidConnect {
  NSLog(@"bleDidConnect!!!!!");
  if (hasListeners) { // Only send events if anyone is listening
    [self sendEventWithName:@"BtPrinterConnectEvent" body:@{@"status": @"success"}];
  }
}

@end
