package com.firstproject; 
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.WritableNativeArray;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.Promise;

import java.util.Set;
import java.util.Map;
import java.util.HashMap;

import android.Manifest;
import android.content.pm.PackageManager;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BtPrinter extends ReactContextBaseJavaModule {

    // actions
    private static final String LIST = "list";
    private static final String CONNECT = "connect";
    private static final String CONNECT_INSECURE = "connectInsecure";
    private static final String DISCONNECT = "disconnect";
    private static final String WRITE = "write";
    private static final String AVAILABLE = "available";
    private static final String READ = "read";
    private static final String READ_UNTIL = "readUntil";
    private static final String SUBSCRIBE = "subscribe";
    private static final String UNSUBSCRIBE = "unsubscribe";
    private static final String SUBSCRIBE_RAW = "subscribeRaw";
    private static final String UNSUBSCRIBE_RAW = "unsubscribeRaw";
    private static final String IS_ENABLED = "isEnabled";
    private static final String IS_CONNECTED = "isConnected";
    private static final String CLEAR = "clear";
    private static final String SETTINGS = "showBluetoothSettings";
    private static final String ENABLE = "enable";
    private static final String DISCOVER_UNPAIRED = "discoverUnpaired";
    private static final String SET_DEVICE_DISCOVERED_LISTENER = "setDeviceDiscoveredListener";
    private static final String CLEAR_DEVICE_DISCOVERED_LISTENER = "clearDeviceDiscoveredListener";
    private static final String SET_NAME = "setName";
    private static final String SET_DISCOVERABLE = "setDiscoverable";

    private BluetoothAdapter bluetoothAdapter;
    private BluetoothSerialService bluetoothSerialService;

    // Debugging
    private static final String TAG = "BluetoothSerial";
    private static final boolean D = true;

    // Message types sent from the BluetoothSerialService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_READ_RAW = 6;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    StringBuffer buffer = new StringBuffer();
    private String delimiter;
    private static final int REQUEST_ENABLE_BLUETOOTH = 1;

    // Android 23 requires user to explicitly grant permission for location to discover unpaired
    // private static final String ACCESS_COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int CHECK_PERMISSIONS_REQ_CODE = 2;
    // private CallbackContext permissionCallback;


    BtPrinter(ReactApplicationContext context) {
        super(context);
    }

    @Override
    public String getName() {
        return "BtPrinter";
    }

    @Override
    public void initialize() {
        if (bluetoothAdapter == null) {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }

        if (bluetoothSerialService == null) {
            bluetoothSerialService = new BluetoothSerialService();
        }
    }

    @ReactMethod
    public void scanForBLEPeripherals(int timeout, Callback callBack) {
        Log.d("ReactNative","scanForBLEPeripherals called");
        WritableArray array = new WritableNativeArray();
        try {
            Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();
            for (BluetoothDevice device : bondedDevices) {
                array.pushMap(deviceToMap(device));
            }
            callBack.invoke(null, array);
        } 
        catch(Exception e) {
            callBack.invoke(e, null);
        }
    }

    @ReactMethod
    public void connect(String macAddress, Callback callBack) {
        Log.d("ReactNative","connect called");
        boolean success = true;
        try {
            BluetoothDevice device = bluetoothAdapter.getRemoteDevice(macAddress);
            boolean secure = true;
    
            if (device != null) {
                bluetoothSerialService.connect(device, secure);
                buffer.setLength(0);
                callBack.invoke(null, success);
            } 
            else {
                callBack.invoke(null, success);
            }
        } catch(Exception e) {
            callBack.invoke(e, null);
        }
    }

    @ReactMethod
    public void write(String text, Callback callBack) {
        Log.d("ReactNative","write called");
        boolean success = true;
        try {
            // byte[] data = args.getArrayBuffer(0);
            // byte[] data = text.getBytes();
            bluetoothSerialService.write(text);
            callBack.invoke(null, success);
        } catch(Exception e) {
            callBack.invoke(e, null);
        }
    }

    // private methods
    // @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_ENABLE_BLUETOOTH) {

            if (resultCode == Activity.RESULT_OK) {
                Log.d(TAG, "User enabled Bluetooth");
                // if (enableBluetoothCallback != null) {
                //     enableBluetoothCallback.success();
                // }
            } else {
                Log.d(TAG, "User did *NOT* enable Bluetooth");
                // if (enableBluetoothCallback != null) {
                //     enableBluetoothCallback.error("User did not enable Bluetooth");
                // }
            }

            // enableBluetoothCallback = null;
        }
    }

    

    private WritableMap deviceToMap(BluetoothDevice device) throws JSONException {
        WritableMap map = new WritableNativeMap();
        map.putString("name", device.getName());
        map.putString("address", device.getAddress());
        map.putString("id", device.getAddress());
        if (device.getBluetoothClass() != null) {
            String deviceClassString = new Integer(device.getBluetoothClass().getDeviceClass()).toString();
            map.putString("class", deviceClassString);
        }
        return map;
    }


    private int available() {
        return buffer.length();
    }

    private String read() {
        int length = buffer.length();
        String data = buffer.substring(0, length);
        buffer.delete(0, length);
        return data;
    }

    private String readUntil(String c) {
        String data = "";
        int index = buffer.indexOf(c, 0);
        if (index > -1) {
            data = buffer.substring(0, index + c.length());
            buffer.delete(0, index + c.length());
        }
        return data;
    }

    // @Override
    public void onRequestPermissionResult(int requestCode, String[] permissions,
                                          int[] grantResults) throws JSONException {

        for(int result:grantResults) {
            if(result == PackageManager.PERMISSION_DENIED) {
                Log.d(TAG, "User *rejected* location permission");
                // this.permissionCallback.sendPluginResult(new PluginResult(
                //         PluginResult.Status.ERROR,
                //         "Location permission is required to discover unpaired devices.")
                //     );
                return;
            }
        }

        switch(requestCode) {
            case CHECK_PERMISSIONS_REQ_CODE:
            Log.d(TAG, "User granted location permission");
                // discoverUnpairedDevices(permissionCallback);
                break;
        }
    }
}